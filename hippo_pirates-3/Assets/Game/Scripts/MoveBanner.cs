﻿using UnityEngine;
using System.Collections;

public class MoveBanner : MonoBehaviour {


    public void MoeToTop ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        ManagerGoogle.Instance.RepositionSmallBanner ( AdmobAd.AdLayout.Top_Centered );
#endif
    }

    public void MoeToBottom ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        ManagerGoogle.Instance.RepositionSmallBanner ( AdmobAd.AdLayout.Bottom_Centered );
#endif
    }

}
