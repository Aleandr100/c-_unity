﻿#define BRAND_NEW_PROTOTYPE

using UnityEngine;
using System.Collections;


public class AudioToggle : ToggleButton
{
	public enum Toggles
	{
		Music,
		Sounds,
		Vibro,
	}
    
	public Toggles ToggleType;


    void OnEnable ()
    {
        SetValues ( );
    }
    
	void SetValues()
	{
		bool param = false;
		switch (ToggleType)
			{
			case Toggles.Music:
				{
#if BRAND_NEW_PROTOTYPE
                    param =	!GameSettings.IsMusicEnabled();
#else
                    param =	!AudioController.IsMusicEnabled();
#endif
                    break;
				}
			case Toggles.Sounds:
				{
#if BRAND_NEW_PROTOTYPE
                    param = !GameSettings.IsSoundsEnabled ( );
#else
                    param =	!AudioController.IsSoundsEnabled();
#endif
                    break;
				}
			case Toggles.Vibro:
				{
#if BRAND_NEW_PROTOTYPE
                    param =	!GameSettings.IsVibroEnabled();
#endif
                    break;
				}
			}

        //this stuff is done to avoid listeners from catching init changes events
        SetToggle ( param );
    }

	override protected void OnValueChanged (bool param)
	{
		//Debug.Log("Toggle action = " + ToggleType.ToString());
		switch (ToggleType)
			{
			case Toggles.Music:
				{
#if BRAND_NEW_PROTOTYPE
                    GameSettings.EnableMusic ( !param );
#else
                    AudioController.EnableMusic ( !param );
#endif
                    break;
				}
			case Toggles.Sounds:
				{
#if BRAND_NEW_PROTOTYPE
                    GameSettings.EnableSounds ( !param );
#else
                    AudioController.EnableSounds ( !param );
#endif
                    break;
				}
			case Toggles.Vibro:
				{
#if BRAND_NEW_PROTOTYPE
                    GameSettings.EnableVibro(!param);
#endif
                    break;
				}
			}
	}

}
