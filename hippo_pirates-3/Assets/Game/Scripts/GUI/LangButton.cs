﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent ( typeof ( Image ) )]
public class LangButton :MonoBehaviour
{
    [Header ( "en fr ge pt ru sp" )]
    public Sprite []
        lang_array;    //set here lang sprites in strong order of their index in lang_order


    private Dictionary<Languages.Language, int> lang_icons;


    private Image
        img;





    void Awake ()
    {
        img = GetComponent<Image> ( );

        ParseIcons ( );
    }


    void ParseIcons ()
    {
        lang_icons = new Dictionary<Languages.Language, int> ( );
        for (int i = 0; i < lang_array.Length; i++)
        {
            if (lang_array [i] != null)
            {
                Languages.Language icon_lng = (Languages.Language) System.Enum.Parse ( typeof ( Languages.Language ), lang_array [i].name );
                if (!lang_icons.ContainsKey ( icon_lng ))
                {
                    lang_icons.Add ( icon_lng, i );
                }
                else
                {
                    Debug.Log ( "Key duplicate " + icon_lng.ToString ( ) + " icon name : " + lang_array [i].name );
                }
            }
        }
    }


    void OnEnable ()
    {
        Languages.OnLangChange += SetSprite;
    }


    void OnDisable ()
    {
        Languages.OnLangChange -= SetSprite;
    }



    void SetSprite (Languages.Language current_lang)
    {
        if (lang_icons.ContainsKey ( current_lang ))
        {
            img.sprite = lang_array [lang_icons [current_lang]];
        }
        else
        {
            Debug.Log ( "No lang icon" );
        }
    }

    void Start ()
    {
        Languages.Language current_lang = Languages.GetLanguage ( );
        SetSprite ( current_lang );
    }

}
