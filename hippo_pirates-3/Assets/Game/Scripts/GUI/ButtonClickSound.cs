﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ButtonClickSound : MonoBehaviour
{

    private string ClickSound = "Button - Click";

    [HideInInspector]
    public new bool enabled = true;


    void Awake ()
    {
        Button button = GetComponent<Button> ( );
        if (button)
        {
            button.onClick.AddListener ( PlayOnCLick );
        }
        else
        {
            Toggle toggle = GetComponent<Toggle> ( );
            if (toggle)
            {
                toggle.onValueChanged.AddListener ((bool param)=> PlayOnCLick ( ) );
            }
        }
    }

    void PlayOnCLick ()
    {
        if (enabled)
        {
            AudioController.PlaySound ( ClickSound );
            Vibration.Vibrate ( 10 );
        }
    }

}
