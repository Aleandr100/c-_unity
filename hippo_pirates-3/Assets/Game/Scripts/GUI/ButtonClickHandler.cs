﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[RequireComponent(typeof(Button))]
public class ButtonClickHandler : MonoBehaviour {

    virtual protected void Awake ()
    {
        GetComponent<Button> ( ).onClick.AddListener ( OnButtonCLick );
    }

    virtual protected void OnButtonCLick ()
    {
        
    }

}
