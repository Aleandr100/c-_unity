﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class HarpoonPool : ABasePool, IHarpoonPool
	{
		// FIELDS
		// -------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;

		private GameObject[] _prefabs;

	

		// A_SERVICES
		protected override void InitAwake()
		{
			_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IHarpoonPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IHarpoonPool>();
		}

	

		// INTERFACES
		public GameObject ShowObject(Transform myHarpoon, int harpType)
		{
			GameObject go = GetPrefab(_prefabs);

			// in case the pool is empty !
			if (go == null)
			{
				go = GetBackupPrefab();
			}

			Transform goTransf = go.transform;

			SetPosition(goTransf, myHarpoon, harpType);
			SetParentToObject(goTransf);

			go.SetActive(true);

			// this place is OK - otherwise harpoon doesn't move !
			SetImpulse(go, myHarpoon, harpType);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}



		// METHODS
		private void SetPosition(Transform go, Transform myHarpoon, int harpType)
		{
			if (harpType == 0)
			{
				Vector2 tempScale = go.localScale;
				go.localScale = new Vector2(-tempScale.x, tempScale.y);
			}

			go.position = myHarpoon.GetChild(0).GetChild(2).position;
			go.localRotation = myHarpoon.localRotation;
		}

		private void SetImpulse(GameObject go, Transform myHarpoon, int harpType)
		{
			var goRigid = go.GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goRigid);
	#endif
			#endregion

			Vector2 tempPos = Vector2.zero;

			if (harpType == 0)
			{
				tempPos = -myHarpoon.right;
			}
			else if (harpType == 1)
			{
				tempPos = myHarpoon.right;
			}

			const float impulseMult = 5f;
			goRigid.velocity = tempPos.normalized * impulseMult;
		}


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}