﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class SeaweedPool : ABasePool, ISeaweedPool
	{
		// FIELDS
		// ----------------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;
	
		private GameObject[] _prefabs;
	
		// ----------------------------------------------------
		[Header("SEAWEED GREEN: UP AND DOWN")]
		[SerializeField] private GameObject _prefabGreenUp;
		[SerializeField] private GameObject _prefabGreenDown;

		// ----------------------------------------------------
		[Header("SEAWEED RED: UP AND DOWN")]
		[SerializeField] private GameObject _prefabRedUp;
		[SerializeField] private GameObject _prefabRedDown;

		// ----------------------------------------------------
		[Header("SEAWEED YELLOW: UP AND DOWN")]
		[SerializeField] private GameObject _prefabYellowUp;
		[SerializeField] private GameObject _prefabYellowDown;
	


		// A_SERVICES
		protected override void InitAwake()
		{
			_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ISeaweedPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ISeaweedPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("w"))
			{
				ShowObject();
			}
		}
	#endif
	


		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			Transform goTransf = go.transform;

			SetPosition(goTransf);
			SetParentToObject(goTransf);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}

		public void CutSeaweed(Transform go)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			GameObject upPrefab = GetUpPrefabType(go.name);
			ShowSeaweedUp(go, upPrefab);

			GameObject downPrefab = GetDownPrefabType(go.name);
			ShowSeaweedDown(go, downPrefab);
		}
	
		// METHODS

		#region SEAWEED

		private void SetName(GameObject go, string prefabName)
		{
			go.name = prefabName;
		}

		private void SetPosition(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				goTransf.position = new Vector2(17f, -4.5f);
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				goTransf.position = new Vector2(-7.2f, -12f);
				SetRotation(goTransf);
			}
		}

		private void SetRotation(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			const float minRot = 320;
			const float maxRot = 330;
			float rotZ = Random.Range(minRot, maxRot);

			goTransf.localRotation = Quaternion.Euler(0, 0, rotZ);
		}

		#endregion

		#region SEAWEED_UP

		private GameObject GetUpPrefabType(string seaweedName)
		{
			if (seaweedName == "Seaweed_Green")
			{
				return _prefabGreenUp;
			}
			if (seaweedName == "Seaweed_Red")
			{
				return _prefabRedUp;
			}
			if (seaweedName == "Seaweed_Yellow")
			{
				return _prefabYellowUp;
			}

			#region DEBUG
	#if UNITY_EDITOR
			//Debug.Log("[ERROR] Unknown seaweedName !");
	#endif
			#endregion
		
			return null;
		}

		private void ShowSeaweedUp(Transform seaweed, GameObject upPrefab)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, seaweed);
	#endif
			#endregion

			GameObject go = Instantiate(upPrefab);
			go.name = string.Format("{0}_Up", upPrefab.name);

			Transform goTransf = go.transform;

			SetSeaweedUpPosition(goTransf, seaweed);
			SetParentToObject(goTransf);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			// post-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion
		}

		private void SetSeaweedUpPosition(Transform go, Transform seaweed)
		{
			var goMove = seaweed.GetComponent<SeaweedMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			go.position = goMove.Get_PointUp().position;
		}

		#endregion

		#region SEAWEED_DOWN

		private GameObject GetDownPrefabType(string seaweedName)
		{
			if (seaweedName == "Seaweed_Green")
			{
				return _prefabGreenDown;
			}
			if (seaweedName == "Seaweed_Red")
			{
				return _prefabRedDown;
			}
			if (seaweedName == "Seaweed_Yellow")
			{
				return _prefabYellowDown;
			}

			#region DEBUG
	#if UNITY_EDITOR
			Debug.Log("[ERROR] Unknown seaweedName !");
	#endif
			#endregion

			return null;
		}

		private void ShowSeaweedDown(Transform seaweed, GameObject downPrefab)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, seaweed);
	#endif
			#endregion

			GameObject go = Instantiate(downPrefab);
			go.name = string.Format("{0}_Down", downPrefab.name);

			Transform goTransf = go.transform;

			SetParentToObject(goTransf);
			SetSeaweedDownPosition(goTransf, seaweed);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			// post-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion
		}

		private void SetSeaweedDownPosition(Transform go, Transform seaweed)
		{
			var goMove = seaweed.GetComponent<SeaweedMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			go.position = goMove.Get_PointDown().position;
		}

		#endregion


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}