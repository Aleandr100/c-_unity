﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class BonusPool : ABasePool, IBonusPool
	{
		// FIELDS
		// ------------------------------------------------
		[Header("POOL PLACES")]
		[SerializeField] private Transform _poolPlaceBonusArmor;
		private GameObject[] _prefabBonusArmor;

		// ------------------------------------------------
		[SerializeField] private Transform _poolPlaceBonusHpRestore;
		private GameObject[] _prefabBonusHpRestore;

		// ------------------------------------------------
		[SerializeField] private Transform _poolPlaceBonusRepairRate;
		private GameObject[] _prefabBonusRepairRate;

		// ------------------------------------------------
		private readonly Dictionary<Enumerators.BonusTypes, Transform> _dictPoolPlaces =
			new Dictionary<Enumerators.BonusTypes, Transform>();

		private readonly Dictionary<Enumerators.BonusTypes, GameObject[]> _dictPrefabs =
			new Dictionary<Enumerators.BonusTypes, GameObject[]>();
	

	
		// A_SERVICES
		protected override void InitAwake()
		{
			// lists must be initialized before stored in dictionary (or they will be null) !!!
			_prefabBonusArmor = InitPrefabsList(_prefabBonusArmor, _poolPlaceBonusArmor);
			_prefabBonusHpRestore = InitPrefabsList(_prefabBonusHpRestore, _poolPlaceBonusHpRestore);
			_prefabBonusRepairRate = InitPrefabsList(_prefabBonusRepairRate, _poolPlaceBonusRepairRate);

			InitDictPoolPlaces();
			InitDictPrefabs();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IBonusPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IBonusPool>();
		}



		// INTERFACES
		public GameObject ShowObject(Enumerators.BonusTypes bonusType)
		{
			GameObject go = GetPrefab( _dictPrefabs[bonusType] );

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			SetParentToObject(go.transform);
			go.SetActive(true);
			CheckLevelType(go);

			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}



		// METHODS
		#region INITS

		private void InitDictPoolPlaces()
		{
			_dictPoolPlaces.Add( Enumerators.BonusTypes.BONUS_ARMOR,		_poolPlaceBonusArmor		);
			_dictPoolPlaces.Add( Enumerators.BonusTypes.BONUS_HP_RESTORE,	_poolPlaceBonusHpRestore	);
			_dictPoolPlaces.Add( Enumerators.BonusTypes.BONUS_REPAIR_RATE,	_poolPlaceBonusRepairRate	);
		}

		private void InitDictPrefabs()
		{
			_dictPrefabs.Add( Enumerators.BonusTypes.BONUS_ARMOR,		_prefabBonusArmor		);
			_dictPrefabs.Add( Enumerators.BonusTypes.BONUS_HP_RESTORE,	_prefabBonusHpRestore	);
			_dictPrefabs.Add( Enumerators.BonusTypes.BONUS_REPAIR_RATE,	_prefabBonusRepairRate	);
		}

		#endregion

		private void CheckLevelType(GameObject go)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				SetHorizontalParams(go);
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				SetVerticalParams(go);
			}
		}

		#region HORIZONTAL

		private void SetHorizontalParams(GameObject go)
		{
			// pre-conditions
			Assert.AreNotEqual(null, go);

			const int variants = 2;
			int rnd = UnityEngine.Random.Range(0, variants);

			if (rnd == 0)
			{
				SetHorizontalBottomParams(go);
			}
			else if (rnd == 1)
			{
				SetHorizontalTopParams(go);
			}
		}

		// spawn on bottom
		private void SetHorizontalBottomParams(GameObject go)
		{
			SetHorizontalBottomPosition(go);
			SetHorizontalBottomPhysics(go);
		}

		private void SetHorizontalBottomPosition(GameObject go)
		{
			go.transform.position = new Vector2(15f, -3.5f);
		}

		private void SetHorizontalBottomPhysics(GameObject go)
		{
			Rigidbody2D goRigid = go.GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goRigid);
	#endif
			#endregion
			goRigid.gravityScale = 0;

			//goRigid.velocity = new Vector2(-1, 0);
			var goMove = go.GetComponent<BonusMove>();
			#region ASSERTS
			    #if UNITY_EDITOR
					Assert.AreNotEqual(null, goMove);
			    #endif
			#endregion
			goMove.SetVectorMoveHorizontal();
		}

		// spawn on top
		private void SetHorizontalTopParams(GameObject go)
		{
			SetHorizontalTopPosition(go);
			SetHorizontalTopPhysics(go);
		}

		private void SetHorizontalTopPosition(GameObject go)
		{
			go.transform.position = new Vector2(15f, 7f); 
		}

		private void SetHorizontalTopPhysics(GameObject go)
		{
//			Rigidbody2D goRigid = go.GetComponent<Rigidbody2D>();
//			#region ASSERTS
//	#if UNITY_EDITOR
//			Assert.AreNotEqual(null, goRigid);
//	#endif
//			#endregion

			//goRigid.velocity = new Vector2(-1, -1);
			var goMove = go.GetComponent<BonusMove>();
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
#endif
			#endregion
			goMove.SetVectorMoveVertical();
		}

		#endregion

		#region VERTICAL   

		private void SetVerticalParams(GameObject go)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			SetVertPosition(go);
			SetVertPhysics(go);	
		}

		private void SetVertPosition(GameObject go)
		{
			const float min_x = -4f;
			const float max_x = 4f;
			float pointX = UnityEngine.Random.Range(min_x, max_x);

			const float pointY = 7f;

			go.transform.position = new Vector2(pointX, pointY);
		}

		private void SetVertPhysics(GameObject go)
		{
			Rigidbody2D goRigid = go.GetComponent<Rigidbody2D>();
			Assert.AreNotEqual(null, goRigid);

			goRigid.gravityScale = 0.01f;
		}

		#endregion


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}


	}
}