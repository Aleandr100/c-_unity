﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class NetPool : ABasePool, INetPool
	{
		// FIELDS
		// -----------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;

		private GameObject[] _prefabs;



		// A_SERVICES
		protected override void InitAwake()
		{
			_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<INetPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<INetPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("n"))
			{
				ShowObject();
			}
		}
	#endif

	

		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			Transform goTransf = go.transform;

			SetRandomRotation(goTransf);
			SetPosition(goTransf);
			SetParentToObject(goTransf);
			Services.Get<IMineExplosionManager>().AddToList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}
	


		// METHODS
		private void SetRandomRotation(Transform go)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			float rndRot = Random.Range(-360, 360);
			go.GetChild(0).localRotation = Quaternion.Euler(0, 0, rndRot);
		}

		private void SetPosition(Transform go)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			go.position = new Vector2(-3f, -10f);		// -4.5f
		}


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}