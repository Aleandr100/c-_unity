﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class TrashSmallPool : ABasePool, ITrashSmallPool
	{
		// FIELDS
		// ---------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;

		private GameObject[] _prefabs;
	


		// A_SERVICES
		protected override void InitAwake()
		{
			_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ITrashSmallPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ITrashSmallPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("t"))
			{
				ShowObject();
			}
		}
	#endif



		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			Transform goTransf = go.transform;

			SetRandomPosition(goTransf);
			SetRandomRotation(goTransf);
			SetParentToObject(goTransf);
			Services.Get<IMineExplosionManager>().AddToList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}
	


		// METHODS
		private void SetRandomPosition(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			float min_x = GetMin_X();
			float max_x = GetMax_X();
			float pointX = Random.Range(min_x, max_x);

			const float pointY = 8f;

			goTransf.position = new Vector2(pointX, pointY);
		}

		private float GetMin_X()
		{
			float min_x = 0;
		
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				min_x = 13f;
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				min_x = -4f;
			}

			return min_x;
		}

		private float GetMax_X()
		{
			float max_x = 0;

			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				max_x = 15f;
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				max_x = 4f;
			}

			return max_x;
		}
	
		private void SetRandomRotation(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			float rndRot = Random.Range(-360, 360);
			goTransf.GetChild(0).localRotation = Quaternion.Euler(0, 0, rndRot);
		}


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}