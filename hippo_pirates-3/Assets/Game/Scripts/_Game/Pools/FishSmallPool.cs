﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class FishSmallPool : ABasePool, IFishSmallPool
	{
		// FIELDS
		// ---------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;

		protected GameObject[] _prefabs;
	
		// ---------------------------------------------
		protected readonly float _startLeftX = -12f;
		protected readonly float _startRightX = 15f;

		protected float _tempStartY;



		// A_SERVICES
		protected override void InitAwake()
		{
			_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IFishSmallPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IFishSmallPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("s"))
			{
				ShowObject();
			}
		}
	#endif
	


		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			FishSmallMove goMove = go.GetComponent<FishSmallMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			SetRandomDepthY(goMove);
			SetRandomSpeed(goMove);
			SetRandomDirection(goMove);

			SetParentToObject(go.transform);
			Services.Get<IMineExplosionManager>().AddToList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}



		// METHODS

		#region DEPTH

		private void SetRandomDepthY(FishSmallMove goMove)
		{
			// pre-conditions
			Assert.AreNotEqual(null, goMove);

			int rnd = Random.Range(0, 3);
			CheckRandomDepth(goMove, rnd);

			float[] startsY = { 1, -0.2f, -1.3f };        // 0, -1.2f, -2.3f
			_tempStartY = startsY[rnd];
		}

		private void CheckRandomDepth(FishSmallMove goMove, int rndDepthNum)
		{
			if (rndDepthNum == 0)
			{
				goMove.Set_CurrentFishDepth(Enumerators.FishDepths.TOP);
			}
			else if (rndDepthNum == 1)
			{
				goMove.Set_CurrentFishDepth(Enumerators.FishDepths.CENTER);
			}
			else if (rndDepthNum == 2)
			{
				goMove.Set_CurrentFishDepth(Enumerators.FishDepths.BOTTOM);
			}
		}

		#endregion

		#region SPEED

		private void SetRandomSpeed(FishSmallMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			const float minSpeed = 0.8f;
			const float maxSpeed = 1.2f;
			float rndSpeed = Random.Range(minSpeed, maxSpeed);

			goMove.Set_Speed(rndSpeed);
		}

		#endregion

		#region DIRECTIONS

		private void SetRandomDirection(FishSmallMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			int rndDir = Random.Range(0, 2);
			if (rndDir == 0)
			{
				MoveLeft(goMove);
			}
			else if (rndDir == 1)
			{
				MoveRight(goMove);
			}
		}

		private void MoveLeft(FishSmallMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.LEFT);
			goMove.Set_VectorMove(Vector2.left);
			goMove.transform.position = new Vector2(_startRightX, _tempStartY);

			CheckNeedFlip(goMove.transform, Enumerators.Directions.LEFT);
		}

		private void MoveRight(FishSmallMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.RIGHT);
			goMove.Set_VectorMove(Vector2.right);
			goMove.transform.position = new Vector2(_startLeftX, _tempStartY);

			CheckNeedFlip(goMove.transform, Enumerators.Directions.RIGHT);
		}

		#endregion

		#region FLIP

		private void CheckNeedFlip(Transform goTransf, Enumerators.Directions direction)
		{
			if (direction == Enumerators.Directions.LEFT)
			{
				if (goTransf.localScale.x < 0)
				{
					Flip(goTransf);
				}
			}
			else if (direction == Enumerators.Directions.RIGHT)
			{
				if (goTransf.localScale.x > 0)
				{
					Flip(goTransf);
				}
			}
		}

		private void Flip(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			Vector2 tempScale = goTransf.localScale;
			goTransf.localScale = new Vector2(-1 * tempScale.x, tempScale.y);
		}

		#endregion

		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}