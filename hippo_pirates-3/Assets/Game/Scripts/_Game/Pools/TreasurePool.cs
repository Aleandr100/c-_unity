﻿
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Assets.Game.Scripts._Game.Interfaces.IPools;
using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class TreasurePool : ABasePool, ITreasurePool
	{
		// FIELDS
		// -----------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlaceTreasure;

		// -----------------------------------------------
		[Header("PREFABS")]
		[SerializeField] private GameObject[] _prefabs;



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ITreasurePool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ITreasurePool>();
		}



		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetBackupPrefab();
			go.name = "Treasure";

			SetParentToObject(go.transform);

			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			// post-conditions
			#region ASSERTS
			    #if UNITY_EDITOR
					Assert.AreNotEqual(null, go);
			    #endif
			#endregion

			return go;
		}



		// METHODS
		protected override GameObject GetBackupPrefab()
		{
			int rndNumber = Random.Range(0, _prefabs.Length);
			GameObject rndPrefab = _prefabs[rndNumber];

			GameObject go = Instantiate(rndPrefab);

			return go;
		}



	}
}