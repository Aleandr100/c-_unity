﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class ChainPool : ABasePool, IChainPool
	{
		// FIELDS
		// ------------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;
		
		 private GameObject[] _prefabs;

		// ------------------------------------------------
		[Header("[CHAIN_UP AND CHAIN_DOWN]")]
		[SerializeField] private GameObject[] _chainUpPrefabs;
		[SerializeField] private GameObject[] _chainDownPrefabs;

		// ------------------------------------------------
		private readonly Dictionary<string, int> _dictCollidersNames = new Dictionary<string, int>
		{
			{ "0", 0 },
			{ "1", 1 },
			{ "2", 2 },
			{ "3", 3 },
			{ "4", 4 }
		};



		// A_SERVICES
		protected override void InitAwake()
		{
	 		_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IChainPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IChainPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("c"))
			{
				ShowObject();
			}
		}
	#endif
	



		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			SetRopePosition(go.transform);
			SetParentToObject(go.transform);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}

		public void CutChain(Transform go, Transform coll)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
			Assert.AreNotEqual(null, coll);
	#endif
			#endregion

			int numColl = _dictCollidersNames[coll.name];
			ShowRopeUp(go, numColl);
			ShowRopeDown(go, numColl);
		}
	


		// METHODS
		private void SetRopePosition(Transform go)
		{
			Vector2 startPos = new Vector2(17f, 6.4f);
			go.position = startPos;
		}

		#region ROPE_UP

		private void ShowRopeUp(Transform chain, int num)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, chain);
			Assert.IsTrue(num >= 0 && num < 5);     // 5 - number of chains
	#endif
			#endregion

			GameObject go = Instantiate(_chainUpPrefabs[num]);
			go.name = string.Format("Chain_Up_{0}", num);

			SetPositionRopeUp(go.transform, chain);
			SetParentToObject(go.transform);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);
		}

		private void SetPositionRopeUp(Transform go, Transform chain)
		{
			var chainMove = chain.GetComponent<ChainMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, chainMove);
	#endif
			#endregion

			go.position = chainMove.Get_PointUp().position;
		}

		#endregion

		#region ROPE_DOWN

		private void ShowRopeDown(Transform chain, int num)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, chain);
			Assert.IsTrue(num >= 0 && num < 5);     // 5 - number of chains
	#endif
			#endregion

			GameObject go = Instantiate(_chainDownPrefabs[num]);
			go.name = string.Format("Chain_Down_{0}", num);

			SetPositionRopeDown(go.transform, chain);
			SetParentToObject(go.transform);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);
		}

		private void SetPositionRopeDown(Transform go, Transform chain)
		{
			var chainMove = chain.GetComponent<ChainMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, chainMove);
	#endif
			#endregion

			go.position = chainMove.Get_PointDown().position;
		}

		#endregion

		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}