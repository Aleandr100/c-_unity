﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using UnityEngine.Assertions;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class RopePool : ABasePool, IRopePool
	{
		// FIELDS
		// ----------------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;

		private GameObject[] _ropePrefabs;

		// ----------------------------------------------------
		[Header("[ROPE_UP AND ROPE_DOWN]")]
		[SerializeField] protected GameObject[] _ropeUpPrefabs;
		[SerializeField] protected GameObject[] _ropeDownPrefabs;

		// ----------------------------------------------------
		private readonly Dictionary<string, int> _dictCollidersNames = new Dictionary<string, int>
		{
			{ "0", 0 },
			{ "1", 1 },
			{ "2", 2 },
			{ "3", 3 },
			{ "4", 4 }
		};



		// A_SERVICES
		protected override void InitAwake()
		{
			_ropePrefabs = InitPrefabsList(_ropePrefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IRopePool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IRopePool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("r"))
			{
				ShowObject();
			}
		}
	#endif
	


		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_ropePrefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			Transform goTransf = go.transform;
		
			SetPosition(goTransf);
			SetParentToObject(goTransf);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}

		public void CutRope(Transform go, Transform coll)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
			Assert.AreNotEqual(null, coll);
	#endif
			#endregion

			int numColl = _dictCollidersNames[coll.name];
			ShowRopeUp(go, numColl);
			ShowRopeDown(go, numColl);
		}

	

		// METHODS
		protected void SetPosition(Transform go)
		{
			go.position = new Vector2(17f, 7.5f);
		}

		#region ROPE_UP
	
		private void ShowRopeUp(Transform rope, int num)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rope);
			Assert.IsTrue(num >= 0 && num < 5);         // 6 - number of rope segmetns
	#endif
			#endregion

			GameObject go = Instantiate( _ropeUpPrefabs[num] );
			go.name = string.Format("Rope_Up_{0}", num.ToString());

			Transform goTransf = go.transform;

			SetRopeUpPosition(goTransf, rope);
			SetParentToObject(goTransf);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			// post-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion
		}

		private void SetRopeUpPosition(Transform go, Transform rope)
		{
			var goMove = rope.GetComponent<RopeMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			go.position = goMove.Get_PointUp().position;
		}

		#endregion

		#region ROPE_DOWN

		private void ShowRopeDown(Transform rope, int num)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rope);
			Assert.IsTrue(num >= 0 && num < 5);         // 5 - number of rope segmetns
	#endif
			#endregion

			GameObject go = Instantiate(_ropeDownPrefabs[num]);
			go.name = string.Format("Rope_Down_{0}", num.ToString());

			Transform goTransf = go.transform;

			SetParentToObject(goTransf);
			SetRopeDownPosition(goTransf, rope);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			// post-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion
		}

		private void SetRopeDownPosition(Transform go, Transform rope)
		{
			var goMove = rope.GetComponent<RopeMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			go.position = goMove.Get_PointDown().position;
		}

		#endregion


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}