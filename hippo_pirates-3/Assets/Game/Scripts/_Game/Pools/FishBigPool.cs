﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class FishBigPool : ABasePool, IFishBigPool
	{
		// FIELDS
		// ------------------------------------------
		[Header("POOL PLACE")]
		[SerializeField] private Transform _poolPlace;

		protected GameObject[] _prefabs;

		// ------------------------------------------
		protected const float _startLeftX = -12.4f;
		protected const float _startRightX = 23.75f;
		protected const float _startY = 0f;



		// A_SERVICES
		protected override void InitAwake()
		{
			_prefabs = InitPrefabsList(_prefabs, _poolPlace);
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IFishBigPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IFishBigPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("b"))
			{
				ShowObject();
			}
		}
	#endif
	


		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject go = GetPrefab(_prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			FishBigMove goMove = go.GetComponent<FishBigMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			SetRandomSpeed(goMove);
			SetRandomDirection(goMove);

			SetParentToObject(go.transform);
			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IAvatarsManager>().ActivateThreat(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}
	


		// METHODS
		#region SPEED

		private void SetRandomSpeed(FishBigMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			const float minSpeed = 0.4f;
			const float maxSpeed = 0.7f;
			float rndSpeed = Random.Range(minSpeed, maxSpeed);

			goMove.Set_Speed(rndSpeed);
		}


		#endregion

		#region DIRECTION

		private void SetRandomDirection(FishBigMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			int rndDir = Random.Range(0, 2);
			if (rndDir == 0)
			{
				MoveLeft(goMove);
			}
			else if (rndDir == 1)
			{
				MoveRight(goMove);
				Services.Get<ICameraManager>().MoveCameraLeft();
			}
		}

		private void MoveLeft(FishBigMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.LEFT);
			goMove.Set_VectorMove(Vector2.left);
			goMove.transform.position = new Vector2(_startRightX, _startY);
		}

		private void MoveRight(FishBigMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.RIGHT);
			goMove.Set_VectorMove(Vector2.right);
			goMove.transform.position = new Vector2(_startLeftX, _startY);
		}

		#endregion

		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}