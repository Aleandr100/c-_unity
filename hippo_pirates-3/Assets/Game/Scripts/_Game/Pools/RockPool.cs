﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using UnityEngine.Assertions;
using Pirates_3.Common.Info;
using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public class RockPool : ABasePool, IRockPool
	{
		// FIELDS
		// --------------------------------------
		[Header("POOL PLACES")]
		[SerializeField] private Transform _poolPlaceHorA;

		private GameObject[] _prefabsHorA;

		// -------------------------------
		[SerializeField] private Transform _poolPlaceVertA;

		private GameObject[] _prefabsVertA;

		// -------------------------------
		[SerializeField] private Transform _poolPlaceHorB;

		private GameObject[] _prefabsHorB;

		// -------------------------------
		[SerializeField] private Transform _poolPlaceVertB;

		private GameObject[] _prefabsVertB;

		// -------------------------------
		private readonly Dictionary<Enumerators.LevelsSubtypes, Transform> _dictPoolPlaces = 
			new Dictionary<Enumerators.LevelsSubtypes, Transform>();

		private readonly Dictionary<Enumerators.LevelsSubtypes, GameObject[]> _dictPrefabs =
			new Dictionary<Enumerators.LevelsSubtypes, GameObject[]>();
	


		// A_SERVICES
		protected override void InitAwake()
		{
			// lists must be initialized before stored in dictionary (or they will be null) !!!
			_prefabsHorA = InitPrefabsList(_prefabsHorA, _poolPlaceHorA);
			_prefabsVertA = InitPrefabsList(_prefabsVertA, _poolPlaceVertA);
			_prefabsHorB = InitPrefabsList(_prefabsHorB, _poolPlaceHorB);
			_prefabsVertB = InitPrefabsList(_prefabsVertB, _poolPlaceVertB);

			InitDictPoolPlaces();
			InitDictPrefabs();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IRockPool>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IRockPool>();
		}



		// UNITY
	#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown("q"))
			{
				ShowObject();
			}
		}
	#endif
	


		// INTERFACES
		public GameObject ShowObject()
		{
			GameObject[] prefabs = _dictPrefabs[ LevelsController.Get_CurrentLevelSubType() ];
			GameObject go = GetPrefab(prefabs);

			if (go == null)
			{
				go = GetBackupPrefab();
			}

			Transform goTransf = go.transform;

			SetPosition(goTransf);
			SetParentToObject(goTransf);

			Services.Get<IMineExplosionManager>().AddToList(go);
			Services.Get<IBottomSpeedManager>().AddToStaticBottomObjectsList(go);

			go.SetActive(true);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}



		// METHODS
		#region INITS

		private void InitDictPoolPlaces()
		{
			_dictPoolPlaces.Add( Enumerators.LevelsSubtypes.TUTORIAL,		_poolPlaceHorA	);
			_dictPoolPlaces.Add( Enumerators.LevelsSubtypes.HORIZONTAL_A,	_poolPlaceHorA	);
			_dictPoolPlaces.Add( Enumerators.LevelsSubtypes.VERTICAL_A,		_poolPlaceVertA	);
			_dictPoolPlaces.Add( Enumerators.LevelsSubtypes.HORIZONTAL_B,	_poolPlaceHorB	);
			_dictPoolPlaces.Add( Enumerators.LevelsSubtypes.VERTICAL_B,		_poolPlaceVertB	);
		}

		private void InitDictPrefabs()
		{
			_dictPrefabs.Add( Enumerators.LevelsSubtypes.TUTORIAL,		_prefabsHorA	);
			_dictPrefabs.Add( Enumerators.LevelsSubtypes.HORIZONTAL_A,	_prefabsHorA	);
			_dictPrefabs.Add( Enumerators.LevelsSubtypes.VERTICAL_A,	_prefabsVertA	);
			_dictPrefabs.Add( Enumerators.LevelsSubtypes.HORIZONTAL_B,	_prefabsHorB	);
			_dictPrefabs.Add( Enumerators.LevelsSubtypes.VERTICAL_B,	_prefabsVertB	);
		}

		#endregion

		private void SetPosition(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				goTransf.position = new Vector2(20f, -3.05f); 
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				goTransf.position = new Vector2(-3.15f, -10f);
			}
		}


		protected override GameObject GetBackupPrefab()
		{
			GameObject go = Instantiate(_backupPrefab);
			go.name = _backupPrefab.ToString();

			return go;
		}



	}
}