﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Pools
{
	public abstract class ABasePool : AMonoService
	{
		// FIELDS
		// -----------------------------------
		[Header("BACKUP PREFAB")]
		[SerializeField] protected GameObject _backupPrefab;       // when pool is over and we need fire

		// -----------------------------------
		[Header("FOR OBJECTS PLACE")]
		[SerializeField] protected Transform _forObjectsPlace;



		// METHODS
		protected GameObject[] InitPrefabsList(GameObject[] prefabs, Transform transfPlace)
		{
			int countPrefabs = transfPlace.childCount;

			prefabs = new GameObject[countPrefabs];

			for (int i = 0; i < countPrefabs; i++)
			{
				prefabs[i] = transfPlace.GetChild(i).gameObject;
			}

			return prefabs;
		}

		protected GameObject GetPrefab(GameObject[] prefabs)
		{
			for (int i = 0; i < prefabs.Length; i++)
			{
				if (prefabs[i].transform.parent != _forObjectsPlace)
				{
					if (!prefabs[i].activeSelf)
					{
						return prefabs[i];
					}
				}
			}

			return null;
		}

		protected void SetParentToObject(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			Vector2 tempScale = goTransf.localScale;
			goTransf.SetParent(_forObjectsPlace);
			goTransf.localScale = tempScale;
		}

		protected abstract GameObject GetBackupPrefab();

	

	}
}