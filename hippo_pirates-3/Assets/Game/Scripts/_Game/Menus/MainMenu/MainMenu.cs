﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Main
{
	public class MainMenu : IInitializable, IDisposable
	{
		// FIELDS
		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;
		private IMainMenuSceneData _mainMenuSceneData;



		// CONSTRUCTOR
		[Inject]
		public MainMenu(ISoundManager soundManager,
						IAnimationManager animationManager,
						ITimerManager timerManager,
						IMainMenuSceneData mainMenuSceneData)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;

			_mainMenuSceneData = mainMenuSceneData;
		}



		// ZENJECT
		public void Initialize()
		{
			AssertVariables();

			SetInitProjectSettings();
			SetInitSoundSettings();
		}

		public void Dispose()
		{
			AudioController.Release();
		}



		// METHODS
		#region INIT

		private void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}

		private void SetInitProjectSettings()
		{
			ManagerGoogle.Instance.HideSmallBanner();
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}

		private void SetInitSoundSettings()
		{
			_soundManager.SetMisicVolume();
			PlayMusic();
			PlayPromoModuleSounds();
			_timerManager.StartTimer(RaccoonStartSpeech, 1f);
		}

		#endregion

		private void PlayPromoModuleSounds()
		{
			_soundManager.PlaySoundRepeating(SoundsMainMenuFX.rotatingMechanism);
		}

		private void RaccoonStartSpeech()
		{
			_soundManager.PlaySound(SoundsSpeech.MainMenu.mainMenuRaccoon);
			float soundsLenght = _soundManager.GetSoundLenght(SoundsSpeech.MainMenu.mainMenuRaccoon);

			_animationManager.SetParameterTrigger(_mainMenuSceneData.Get_AnimatorRaccoon(), ParamTriggers.RaccoonIntro.introTavern);

			_timerManager.StartTimer<object>(
				_animationManager.SetParameterTrigger,
				(new object[] { _mainMenuSceneData.Get_AnimatorRaccoon(), ParamTriggers.RaccoonIntro.needIdle }), soundsLenght);
		}

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}



	}
}
