﻿
using Pirates_3.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Pirates_3.Menu.Main
{
	public class MainMenuSceneData : MonoBehaviour, IMainMenuSceneData
	{
		// FIELDS
		// -------------------------------------
		//
		[Header("ANIMATORS")]
		[SerializeField] private Animator _animatorRaccoon;
		[SerializeField] private Animator _animatorGi;

		// --------------------------------------
		//
		[Header("BUTTONS")]
		[SerializeField] private Button _buttonSettings;
		[SerializeField] private Button _buttonPlayTwoPlus;
		[SerializeField] private Button _buttonPlayFivePlus;

		// --------------------------------------
		//
		[Header("PROMO MODULE")]
		[SerializeField] private GameObject _promoModule;




		// UNITY
		private void Start()
		{
			AssertVariables();
		}

		private void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}



		// INTERFACES
		public Animator Get_AnimatorRaccoon()
		{
			return _animatorRaccoon;
		}

		public Animator Get_AnimatorGi()
		{
			return _animatorGi;
		}

		public Button Get_ButtonSettings()
		{
			return _buttonSettings;
		}

		public Button Get_ButtonPlayTwoPlus()
		{
			return _buttonPlayTwoPlus;
		}

		public Button Get_ButtonPlayFivePlus()
		{
			return _buttonPlayFivePlus;
		}

		public GameObject Get_PromoModule()
		{
			return _promoModule;
		}



	}
}