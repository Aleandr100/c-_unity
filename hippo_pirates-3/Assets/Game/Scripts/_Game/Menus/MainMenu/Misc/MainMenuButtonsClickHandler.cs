﻿using System;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Main
{
	public class MainMenuButtonsClickHandler : IMainMenuButtonsClickHandler, IInitializable, IDisposable
	{
		// FIELDS
		// -------------------------------------
		//
		private readonly string _strSoundClick = "Button - Click";

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IMainMenuSceneData _mainMenuSceneData;



		// CONSTRUCTOR
		[Inject]
		public MainMenuButtonsClickHandler(ISoundManager soundManager,
										IMainMenuSceneData mainMenuSceneData)
		{
		    _soundManager = soundManager;
			_mainMenuSceneData = mainMenuSceneData;
		}



		// ZENJECT
		public void Initialize()
		{
			CanvasBlocker.OnClick += OnBlockerClick;

			//
			AssertVariables();
		}

		public void Dispose()
		{
			CanvasBlocker.OnClick -= OnBlockerClick;
		}

		private void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}




		// INTERFACES
		public void ShowOrHidePromoModule()
		{
			GameObject promoModule = _mainMenuSceneData.Get_PromoModule();

			// show/hide promo-module
			bool isPromomoduleActive = promoModule.activeSelf;
			promoModule.SetActive(!isPromomoduleActive);
		}

		public void HandleClickButtonTwoPlus()
		{
			_soundManager.PlaySound(_strSoundClick);

			LevelsController.Set_AgeType(Enumerators.AgeType.TWO_PLUS);
			LevelsController.LoadIntroTavern();
		}

		public void HandleClickButtonFivePlus()
		{
			_soundManager.PlaySound(_strSoundClick);

			LevelsController.Set_AgeType(Enumerators.AgeType.FIVE_PLUS);
			LevelsController.LoadChooseDifficulty();
		}



		// METHODS
		private void OnBlockerClick()
		{
			_mainMenuSceneData.Get_PromoModule().SetActive(true);
		}



	}
}