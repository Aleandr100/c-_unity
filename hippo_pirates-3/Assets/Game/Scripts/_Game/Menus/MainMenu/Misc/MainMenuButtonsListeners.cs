﻿
using Zenject;

namespace Pirates_3.Menu.Main
{
	public class MainMenuButtonsListeners : IInitializable
	{
		// FIELDS
		// --------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IMainMenuSceneData _mainMenuSceneData;
		private IMainMenuButtonsClickHandler _mainMenuButtonsClickHandler;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IMainMenuSceneData mainMenuSceneData,
								IMainMenuButtonsClickHandler mainMenuButtonsClickHandler)
		{
		    _soundManager = soundManager;
			_mainMenuSceneData = mainMenuSceneData;
			_mainMenuButtonsClickHandler = mainMenuButtonsClickHandler;
		}



		// ZENJECT
		public void Initialize()
		{
			AddListeners();
		}



		// METHODS
		private void AddListeners()
		{
			_mainMenuSceneData.Get_ButtonSettings().
				onClick.AddListener(OnClickButtonSettings);

			_mainMenuSceneData.Get_ButtonPlayTwoPlus().
				onClick.AddListener(OnClickButtonPlayTwoPlus);

			_mainMenuSceneData.Get_ButtonPlayFivePlus().
				onClick.AddListener(OnClickButtonPlayFivePlus);
		}

		#region ON_CLICK

		private void OnClickButtonSettings()
		{
			_mainMenuButtonsClickHandler.ShowOrHidePromoModule();
		}

		private void OnClickButtonPlayTwoPlus()
		{
			_mainMenuButtonsClickHandler.HandleClickButtonTwoPlus();
		}

		private void OnClickButtonPlayFivePlus()
		{
			_mainMenuButtonsClickHandler.HandleClickButtonFivePlus();
		}

		#endregion



	}
}