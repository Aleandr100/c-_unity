﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay
{
	public class RaccoonGameplayController : AMonoService, IRaccoonGameplayController
	{
		// FIELDS
		// -------------------------------------
		//
		[SerializeField] private Transform _objRaccoon;

		// -------------------------------------
		//
		private Animator _animatorRaccoon;

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_animatorRaccoon = _objRaccoon.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorRaccoon);
#endif
			#endregion
		}

		protected override void InitStart()
		{
			_timerManager.StartTimer(StartSpeechAndAnimations, 1f);
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IRaccoonGameplayController>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IRaccoonGameplayController>();
		}



		// INTERFACES
		public void ActionsHaveDamage()
		{
			SetAnimationOnHaveDamage(true);
			string sound = PlaySoundOnHaveDamage();

			if (sound.Length > 0)
			{
				float delay = _soundManager.GetSoundLenght(sound);
				_timerManager.StartTimer(SetAnimationOnHaveDamage, false, delay);
			}
		}

		public void ActionsRepairDamage()
		{
			SetAnimationOnRepairDamage(true);
			string sound = PlaySoundOnRepair();

			if (sound.Length > 0)
			{
				float delay = _soundManager.GetSoundLenght(sound);
				_timerManager.StartTimer(SetAnimationOnRepairDamage, false, delay);
			}
		}



		// METHODS
		private void StartSpeechAndAnimations()
		{
			if (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.HORIZONTAL_A)
			{
				_animationManager.SetParameterBool(_animatorRaccoon, ParamBools.Raccoon.isSpeechOnRepair, true);
				_soundManager.PlaySound(SoundsSpeech.Gameplay.gameplayRaccoonStart);

				float delay = _soundManager.GetSoundLenght(SoundsSpeech.Gameplay.gameplayRaccoonStart);
				_timerManager.StartTimer(SetAnimationOnRepairDamage, false, delay);
			}
		}

		#region HAVE_DAMAGE

		private void SetAnimationOnHaveDamage(bool boolValue)
		{
			if (!IsTutorial())
			{
				_animationManager.SetParameterBool(_animatorRaccoon, ParamBools.Raccoon.isSpeechOnDamage, boolValue);
			}
		}

		private string PlaySoundOnHaveDamage()
		{
			if (!IsTutorial())
			{
				_soundManager.StopAllRaccoonSpeeches();

				string rndSound = _soundManager.GetRandomSound(SoundsSpeech.Gameplay.haveDmgSpeeches);
				_soundManager.PlaySound(rndSound);

				return rndSound;
			}

			return "";
		}

		#endregion

		#region REPAIR_DAMAGE

		private void SetAnimationOnRepairDamage(bool boolValue)
		{
			if (!IsTutorial())
			{
				_animationManager.SetParameterBool(_animatorRaccoon, ParamBools.Raccoon.isSpeechOnRepair, boolValue);
			}
		}

		private string PlaySoundOnRepair()
		{
			if (!IsTutorial())
			{
				_soundManager.StopAllRaccoonSpeeches();

				string rndSound = _soundManager.GetRandomSound(SoundsSpeech.Gameplay.repairedSpeeches);
				_soundManager.PlaySound(rndSound);

				return rndSound;
			}

			return "";
		}

		#endregion

		private bool IsTutorial()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}



	}
}