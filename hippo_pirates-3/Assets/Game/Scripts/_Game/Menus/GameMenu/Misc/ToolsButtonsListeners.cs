﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pirates_3.Menu.Gameplay
{
	public class ToolsButtonsListeners : AMonoScript
	{
		// FIELDS
		[Header("BUTTONS")]
		[SerializeField] private Button _buttonScissors;
		[SerializeField] private Button _buttonMagnet;
		[SerializeField] private Button _buttonHammer;
		[SerializeField] private Button _buttonWelding;

	

		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void Update()
		{
			CheckMouseClick();
		}



		// METHODS

		#region MOUSE

		private void CheckMouseClick()
		{
			if ( Input.GetMouseButtonDown(0) &&
				IsPointerOverUIObject() )
			{
				ActionsMouseClick();
			}
		}

		private void ActionsMouseClick()
		{
			if ( IsClickToolButton() )
			{
				string buttonTag = GetToolButtonTag();

				if (buttonTag == Tags.ToolButtonScissors)
				{
					OnClickButtonScissors();
				}
				else if (buttonTag == Tags.ToolButtonMagnet)
				{
					OnClickButtonMagnet();
				}
				else if (buttonTag == Tags.ToolButtonHammer)
				{
					OnClickButtonHammer();
				}
				else if (buttonTag == Tags.ToolButtonWelding)
				{
					OnClickButtonWelding();
				}
			}
		}

		protected bool IsPointerOverUIObject()
		{
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
			{
				position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
			};

			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

			return results.Count > 0;
		}

		private bool IsClickToolButton()
		{
			return (EventSystem.current.currentSelectedGameObject != null);
		}

		private string GetToolButtonTag()
		{
			return (EventSystem.current.currentSelectedGameObject.tag);
		}

		#endregion

		#region ON_CLICK

		private void OnClickButtonScissors()
		{
			if ( !IsBlocked() )
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.SCISSORS);
			}
		}

		private void OnClickButtonMagnet()
		{
			if ( !IsBlocked() )
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.MAGNET);
			}
		}

		private void OnClickButtonHammer()
		{
			if ( !IsBlocked() )
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.HAMMER);
			}
		}

		private void OnClickButtonWelding()
		{
			if ( !IsBlocked() )
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.WELDING);
			}
		}

		#endregion

		#region MISC

		private bool IsBlocked()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}

		#endregion

	

	}
}