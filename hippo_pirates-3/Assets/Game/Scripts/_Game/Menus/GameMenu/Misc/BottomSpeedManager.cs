﻿
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;

// Класс для регулировки скорости дна (создается эффект, что Субмарина плывет быстрее.медленее)

namespace Pirates_3.Menu.Gameplay
{
	public class BottomSpeedManager : AMonoService, IBottomSpeedManager
	{
		// FIELDS
		// ----------------------------------
		[Header("HORIZONTAL A")]
		[SerializeField] private ABaseBottomMovement[] _bottomsHorA;

		// ----------------------------------
		[Header("VERTICAL A")]
		[SerializeField] private ABaseBottomMovement[] _bottomsVertA;

		// ----------------------------------
		[Header("HORIZONTAL B")]
		[SerializeField] private ABaseBottomMovement[] _bottomsHorB;

		// ----------------------------------
		[Header("VERTICAL B")]
		[SerializeField] private ABaseBottomMovement[] _bottomsVertB;

		// ----------------------------------
		private readonly Dictionary<Enumerators.LevelsSubtypes, ABaseBottomMovement[]> _dictBottoms =
			new Dictionary<Enumerators.LevelsSubtypes, ABaseBottomMovement[]>();

		// ----------------------------------
		private List<GameObject> _staticBottomObjects = new List<GameObject>();

		// ----------------------------------
		private float _speedMultiplier = 2f;

		// ----------------------------------
		public enum SpeedType
		{
			NORMAL,
			SLOW
		}

		private SpeedType _currSpeedype = SpeedType.NORMAL;



		// A_SERVICES
		protected override void InitAwake()
		{
			InitDictionaryBottoms();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(this.GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IBottomSpeedManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IBottomSpeedManager>();
		}



		// UNITY
		private void OnDestroy()
		{
			ClearStaticBottomObjectsList();
		}



		// INTERFACES
		public SpeedType Get_Speedtype()
		{
			return _currSpeedype;
		}

		//
		public void SetSpeedNormal()
		{
			if (LevelsController.Get_CurrentLevelSubType() != Enumerators.LevelsSubtypes.TUTORIAL)
			{
				_currSpeedype = SpeedType.NORMAL;

				SetBottomSpeedormal();
				SetStaticBottomObjectsSpeedNormal();
			}
		}

		public void SetSpeedSlow()
		{
			if (LevelsController.Get_CurrentLevelSubType() != Enumerators.LevelsSubtypes.TUTORIAL)
			{
				_currSpeedype = SpeedType.SLOW;

				SetBottomSpeedSlow();
				SetStaticBottomObjectsSpeedSlow();
			}
		}

		//
		public void AddToStaticBottomObjectsList(GameObject go)
		{
			_staticBottomObjects.Add(go);
		}



		// METHODS
		private void InitDictionaryBottoms()
		{
			_dictBottoms.Add( Enumerators.LevelsSubtypes.HORIZONTAL_A, 	_bottomsHorA	);
			_dictBottoms.Add( Enumerators.LevelsSubtypes.VERTICAL_A, 	_bottomsVertA	);
			_dictBottoms.Add( Enumerators.LevelsSubtypes.HORIZONTAL_B, 	_bottomsHorB	);
			_dictBottoms.Add( Enumerators.LevelsSubtypes.VERTICAL_B, 	_bottomsVertB	);
		}

		private ABaseBottomMovement[] GetCurrentBottomsList()
		{
			ABaseBottomMovement[] bottoms = _dictBottoms[LevelsController.Get_CurrentLevelSubType()];

			// post-conditions
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, bottoms);
#endif
			#endregion

			return bottoms;
		}

		#region NORMAL_SPEED

		private void SetBottomSpeedormal()
		{
			ABaseBottomMovement[] bottoms = GetCurrentBottomsList();

			for (byte i = 0; i < bottoms.Length; i++)
			{
				// pre-condition
				#region ASSERTS
#if UNITY_EDITOR
				Assert.AreNotEqual(null, bottoms[i]);
#endif
				#endregion

				bottoms[i].SetSpeedNormal();
			}
		}

		private void SetStaticBottomObjectsSpeedNormal()
		{
			for (int i = 0; i < _staticBottomObjects.Count; i++)
			{
				CheckStaticBottomObjectsNormal( _staticBottomObjects[i] );
			}
		}

		private void CheckStaticBottomObjectsNormal(GameObject go)
		{
			// ----------------------------------
			if (go.CompareTag(Tags.Rope) ||
			    (go.CompareTag(Tags.CutRope)))
			{
				var goMove = go.GetComponent<RopeMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.Seaweed))
			{
				var goMove = go.GetComponent<SeaweedMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.Chain) ||
			    go.CompareTag(Tags.CutChain))
			{
				var goMove = go.GetComponent<ChainMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.Mine))
			{
				var goMove = go.GetComponent<MineMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.Rock))
			{
				var goMove = go.GetComponent<RockMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.BonusArmor) ||
				go.CompareTag(Tags.BonusHpRestore) ||
				go.CompareTag(Tags.BonusRepairRate) )
			{
				var goMove = go.GetComponent<BonusMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.Treasure))
			{
				var goMove = go.GetComponent<TreasureMove>();

				if (goMove != null) {
					goMove.SetSpeedNormal(); }
			}

			// ----------------------------------
		}

		#endregion

		#region SLOW_SPEED

		private void SetBottomSpeedSlow()
		{
			ABaseBottomMovement[] bottoms = GetCurrentBottomsList();

			for (byte i = 0; i < bottoms.Length; i++)
			{
				// pre-condition
				#region ASSERTS
#if UNITY_EDITOR
				Assert.AreNotEqual(null, bottoms[i]);
#endif
				#endregion

				bottoms[i].SetSpeedSlow();
			}
		}

		private void SetStaticBottomObjectsSpeedSlow()
		{

			for (int i = 0; i < _staticBottomObjects.Count; i++)
			{
				CheckStaticBottomObjectsSlow( _staticBottomObjects[i] );
			}
		}

		private void CheckStaticBottomObjectsSlow(GameObject go)
		{
			// ----------------------------------
			if (go.CompareTag(Tags.Rope) ||
			    (go.CompareTag(Tags.CutRope)))
			{
				var goMove = go.GetComponent<RopeMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
			if (go.CompareTag(Tags.Seaweed))
			{
				var goMove = go.GetComponent<SeaweedMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
			if (go.CompareTag(Tags.Chain) ||
			    go.CompareTag(Tags.CutChain))
			{
				var goMove = go.GetComponent<ChainMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
			if (go.CompareTag(Tags.Mine))
			{
				var goMove = go.GetComponent<MineMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
			if (go.CompareTag(Tags.Rock))
			{
				var goMove = go.GetComponent<RockMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.BonusArmor) ||
			         go.CompareTag(Tags.BonusHpRestore) ||
			         go.CompareTag(Tags.BonusRepairRate) )
			{
				var goMove = go.GetComponent<BonusMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
			else if (go.CompareTag(Tags.Treasure))
			{
				var goMove = go.GetComponent<TreasureMove>();

				if (goMove != null) {
					goMove.SetSpeedSlow(); }
			}

			// ----------------------------------
		}

		#endregion

		private void ClearStaticBottomObjectsList()
		{
			foreach (var item in _staticBottomObjects)
			{
				Destroy(item);
			}

			_staticBottomObjects.Clear();
		}



	}
}