﻿using DG.Tweening;
using Pirates_3.Common;
using UnityEngine;
using UnityEngine.UI;

// Класс, который показывает "блокер" с фэйдом экрана в черный и кнопку "Делее"
// чтобы было четко понятно, что уровень закончился

namespace Pirates_3.Menu.Gameplay
{
	public class GameplayNextLevelController : AMonoService, IGameplayNextLevelController
	{
		// FIELDS
		// --------------------------------------
		//
		[SerializeField] private GameObject _objPanelNextLevel;
		[SerializeField] private Image _imageBlocker;
		[SerializeField] private Button _buttonNextLevel;

		// --------------------------------------
		//
		private bool _isLoose;



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			AddListeners();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IGameplayNextLevelController>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IGameplayNextLevelController>();
		}



		// INTERFACES
		public void SetWeAreLoose()
		{
			_isLoose = true;
		}

		public void ShowPanel()
		{
			FadeToBlack();
		}



		// METHODS
		private void AddListeners()
		{
			_buttonNextLevel.onClick.AddListener(OnClickButtonNextLevel);
		}

		#region ON_CLICK

		private void OnClickButtonNextLevel()
		{
			if (_isLoose)
			{
				LevelsController.LoadLooseScene();
			}
			else
			{
				LevelsController.LoadNextLevel();
			}
		}

		#endregion

		private void FadeToBlack()
		{
			float targetAlpha = 0.47f;		// alpha = 120 (in Unity Editor)

			_imageBlocker.DOFade(targetAlpha, 2f).OnComplete(ShowButtonNextLevel);
		}

		private void ShowButtonNextLevel()
		{
			_objPanelNextLevel.SetActive(true);
		}


	}
}