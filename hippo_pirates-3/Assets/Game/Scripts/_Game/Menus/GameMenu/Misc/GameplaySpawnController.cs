﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Assets.Game.Scripts._Game.Interfaces.IPools;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Common.SpawnData;
using Pirates_3.Common.Info;

// Класс, который отвечает за длительность геймплея и спавнит угрозы / бонусы

namespace Pirates_3.Menu.Gameplay
{
	public class GameplaySpawnController : AMonoService, IGameplaySpawnController
	{
		// FIELDS
		// -------------------------------------------------------
		[Header("VISUAL OBJECTS")]
		[SerializeField] private GameObject _objProgressBarLevel;
		[SerializeField] private RectTransform _objSubmarineMini;

		// -------------------------------------------------------
		private readonly float _endTime = 120f;		//120s
		private float _timer;
		private bool _isEndLevelTimer;

		// -------------------------------------------------------
		// threats
		private AThreats _threats = new ThreatsDefault();

		private readonly List<Enumerators.ThreatTypes> _threatsTypes = new List<Enumerators.ThreatTypes>();
		private readonly List<int> _idThreatsTypes = new List<int>();

		private int _countThreatsSpawn;

		// -------------------------------------------------------
		// bonuses
		private ABonuses _bonuses = new BonusesDefault();

		private readonly List<Enumerators.BonusTypes> _bonusesTypes = new List<Enumerators.BonusTypes>();
		private readonly List<int> _idBonusesTypes = new List<int>();

		private int _countBonusesSpawn;

		// -------------------------------------------------------
		// treasures
		private ABaseTreasures _treasures = new TreasuresDefault();

		private int _countTreasuresSpawn;



		// A_SERVICES
		protected override void InitAwake()
		{	
		}

		protected override void InitStart()
		{
			InitVariables();

			//
			InitThreatsTypeList();
			InitIdList(_idThreatsTypes, _threatsTypes);
			ShuffleList(_idThreatsTypes);

			//
			InitBonusesTypesList();
			InitIdList(_idBonusesTypes, _bonusesTypes);
			ShuffleList(_idBonusesTypes);

			//
			CheckLevel();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IGameplaySpawnController>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IGameplaySpawnController>();
		}



		// UNITY
		private void OnDestroy()
		{
			SafeCancelInvoke("SpawnThreat");
			SafeCancelInvoke("SpawnBonus");
			SafeCancelInvoke("SpawnTreasure");
		}



		// INTERFACES
		public bool Get_IsEndLevelTimer()
		{
			return _isEndLevelTimer;
		}



		// METHODS
		#region INIT_THREATS_AND_BONUSES

		private void InitVariables()
		{
			switch ( LevelsController.Get_CurrentLevelSubType() )
			{
				case Enumerators.LevelsSubtypes.HORIZONTAL_A:
					CheckHorizontalADifficulty();
					_treasures = new TreasuresHorizontal();
					break;

				case Enumerators.LevelsSubtypes.VERTICAL_A:
					CheckVerticalADifficulty();
					_treasures = new TreasuresVertical();
					break;

				case Enumerators.LevelsSubtypes.HORIZONTAL_B:
					CheckHorizontalBDifficulty();
					_treasures = new TreasuresHorizontal();
					break;

				case Enumerators.LevelsSubtypes.VERTICAL_B:
					CheckVerticalBDifficulty();
					_treasures = new TreasuresVertical();
					break;

				default:
					_threats = new ThreatsDefault();
					_bonuses = new BonusesDefault();
					_treasures = new TreasuresDefault();
					break;
			}
		}

		private void CheckHorizontalADifficulty()
		{
			switch ( LevelsController.Get_CurrentLevelDifficulty() )
			{
				case Enumerators.LevelDifficulty.VERY_EASY:
					_threats = new ThreatsVeryEasyHorizontalA();
					_bonuses = new BonusesVeryEasyHorizontalA();
					break;

				case Enumerators.LevelDifficulty.EASY:
					_threats = new ThreatsEasyHorizontalA();
					_bonuses = new BonusesEasyHorizontalA();
					break;

				case Enumerators.LevelDifficulty.NORMAL:
					_threats = new ThreatsNormalHorizontalA();
					_bonuses = new BonusesNormalHorizontalA();
					break;
			}
		}

		private void CheckVerticalADifficulty()
		{
			switch ( LevelsController.Get_CurrentLevelDifficulty() )
			{
				case Enumerators.LevelDifficulty.VERY_EASY:
					_threats = new ThreatsVeryEasyVerticalA();
					_bonuses = new BonusesVeryEasyVerticalA();
					break;

				case Enumerators.LevelDifficulty.EASY:
					_threats = new ThreatsEasyVerticalA();
					_bonuses = new BonusesEasyVerticalA();
					break;

				case Enumerators.LevelDifficulty.NORMAL:
					_threats = new ThreatsNormalVerticalA();
					_bonuses = new BonusesNormalVerticalA();
					break;
			}
		}

		private void CheckHorizontalBDifficulty()
		{
			switch ( LevelsController.Get_CurrentLevelDifficulty() )
			{
				case Enumerators.LevelDifficulty.VERY_EASY:
					_threats = new ThreatsVeryEasyHorizontalB();
					_bonuses = new BonusesVeryEasyHorizontalB();
					break;

				case Enumerators.LevelDifficulty.EASY:
					_threats = new ThreatsEasyHorizontalB();
					_bonuses = new BonusesEasyHorizontalB();
					break;

				case Enumerators.LevelDifficulty.NORMAL:
					_threats = new ThreatsNormalHorizontalB();
					_bonuses = new BonusesNormalHorizontalB();
					break;
			}
		}

		private void CheckVerticalBDifficulty()
		{
			switch ( LevelsController.Get_CurrentLevelDifficulty() )
			{
				case Enumerators.LevelDifficulty.VERY_EASY:
					_threats = new ThreatsVeryEasyVerticalB();
					_bonuses = new BonusesVeryEasyVerticalB();
					break;

				case Enumerators.LevelDifficulty.EASY:
					_threats = new ThreatsEasyVerticalB();
					_bonuses = new BonusesEasyVerticalB();
					break;

				case Enumerators.LevelDifficulty.NORMAL:
					_threats = new ThreatsNormalVerticalB();
					_bonuses = new BonusesNormalVerticalB();
					break;
			}
		}

		#endregion

		#region INIT_LISTS

		private void InitThreatsTypeList()
		{
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.TRASH_SMALL, _threats.trashSmall	);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.TRASH_BIG, _threats.trashBig		);

			AddToList ( _threatsTypes, Enumerators.ThreatTypes.FISH_SMALL, _threats.fishSmall	);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.FISH_BIG, _threats.fishBig		);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.FISH_FAST, _threats.fishFast		);

			AddToList ( _threatsTypes, Enumerators.ThreatTypes.NET, _threats.net				);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.ROPE, _threats.rope				);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.SEAWEED, _threats.seaweed		);

			AddToList ( _threatsTypes, Enumerators.ThreatTypes.CHAIN, _threats.chain			);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.MINE, _threats.mine				);
			AddToList ( _threatsTypes, Enumerators.ThreatTypes.ROCK, _threats.rock				);
		}

		private void InitBonusesTypesList()
		{
			AddToList(_bonusesTypes, Enumerators.BonusTypes.BONUS_REPAIR_RATE, _bonuses.repairRate);
			AddToList(_bonusesTypes, Enumerators.BonusTypes.BONUS_HP_RESTORE, _bonuses.hpRestore);
			AddToList(_bonusesTypes, Enumerators.BonusTypes.BONUS_ARMOR, _bonuses.armor);
		}

		private void AddToList(List<Enumerators.ThreatTypes> myList, Enumerators.ThreatTypes myType, int num)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myList);
			Assert.IsTrue(num >= 0);
	#endif
			#endregion

			for (int i = 0; i < num; i++)
			{
				myList.Add(myType);
			}
		}

		private void AddToList(List<Enumerators.BonusTypes> mysList, Enumerators.BonusTypes myType, int num)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, mysList);
			Assert.IsTrue(num >= 0);
	#endif
			#endregion

			for (int i = 0; i < num; i++)
			{
				mysList.Add(myType);
			}
		}

		private void InitIdList(List<int> idList, List<Enumerators.ThreatTypes> someLsit)
		{
			for (int i = 0; i < someLsit.Count; i++)
			{
				idList.Add(i);
			}
		}

		private void InitIdList(List<int> idList, List<Enumerators.BonusTypes> someLsit)
		{
			for (int i = 0; i < someLsit.Count; i++)
			{
				idList.Add(i);
			}
		}

		#endregion

		#region MISC

		private void ShuffleList(List<int> someList)
		{
			for (int i = 0; i < someList.Count; i++)
			{
				int tmp = someList[i];
				int r = Random.Range(i, someList.Count);
				someList[i] = someList[r];
				someList[r] = tmp;
			}
		}

		private void CheckLevel()
		{
			if (LevelsController.Get_CurrentLevelSubType() != Enumerators.LevelsSubtypes.TUTORIAL)
			{
				InvokeRepeating("SpawnThreat", _threats.delaySpawn, _threats.timerSpawn);
				InvokeRepeating("SpawnBonus", _bonuses.delaySpawn, _bonuses.timerSpawn);
				InvokeRepeating("SpawnTreasure", _treasures.delaySpawn, _treasures.timerSpawn);

				SafeStartInvokeRepeating("CheckTimer", 0f, 1f);
			}
			else
			{
				_objProgressBarLevel.SetActive(false);
			}
		}

		#endregion

		#region TIMER

		private void CheckTimer()   // used in Invoke()
		{
			if (!_isEndLevelTimer)
			{
				_timer++;
				ShowProgresBarLevel();

				if (_timer > _endTime)
				{
					TimerRunsOut();
				}
			}
		}

		private void TimerRunsOut()
		{
			_isEndLevelTimer = true;

			SafeCancelInvoke("SpawnThreat");
			_countThreatsSpawn = 0;

			SafeCancelInvoke("SpawnBonus");
			_countBonusesSpawn = 0;

			StartWinSequence();
		}

		private void StartWinSequence()
		{
			SaveGameProgress();
			Services.Get<IGameplayNextLevelController>().ShowPanel();
		}

		private void SaveGameProgress()
		{
			int earnedStars = GetNumberOfStarsForLevelComplete();

			switch (LevelsController.Get_CurrentLevelDifficulty())
			{
				case Enumerators.LevelDifficulty.VERY_EASY:
					Services.Get<ISaveGameManager>().Set_CountStarsVeryEasy(earnedStars);
					break;

				case Enumerators.LevelDifficulty.EASY:
					Services.Get<ISaveGameManager>().Set_CountStarsEasy(earnedStars);
					break;

				case Enumerators.LevelDifficulty.NORMAL:
					Services.Get<ISaveGameManager>().Set_CountStarsNormal(earnedStars);
					break;

				default:
					#region DEBUG
					    #if UNITY_EDITOR
					    Debug.Log("[ERROR] unknown LevelDifficulty !");
					    #endif
					#endregion
					break;
			}
		}

		private int GetNumberOfStarsForLevelComplete()
		{
			var dictStarsForLevelComplete = new Dictionary <Enumerators.LevelsSubtypes, int>
			{
				{Enumerators.LevelsSubtypes.HORIZONTAL_A, 	0},
				{Enumerators.LevelsSubtypes.VERTICAL_A, 	1},
				{Enumerators.LevelsSubtypes.HORIZONTAL_B, 	2},
				{Enumerators.LevelsSubtypes.VERTICAL_B, 	3}
			};

			var currentLevel = LevelsController.Get_CurrentLevelSubType();
			int starsNumber = dictStarsForLevelComplete[currentLevel];

			return starsNumber;
		}

		#endregion

		#region SPAWN

		private void SpawnThreat()      // used in Invoke()
		{
			if (_countThreatsSpawn < _threatsTypes.Count)
			{
				int rndElement = _idThreatsTypes[_countThreatsSpawn];

				switch (_threatsTypes[rndElement])
				{
					// ===========================================================
					// TRASH
					case Enumerators.ThreatTypes.TRASH_SMALL:
						{
							Services.Get<ITrashSmallPool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.TRASH_BIG:
						{
							Services.Get<ITrashBigPool>().ShowObject();
						}
						break;

					// ===========================================================
					// FISH
					case Enumerators.ThreatTypes.FISH_SMALL:
						{
							Services.Get<IFishSmallPool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.FISH_BIG:
						{
							Services.Get<IFishBigPool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.FISH_FAST:
						{
							Services.Get<IFishFastPool>().ShowObject();
						}
						break;

					// ===========================================================
					// SCREW BREAK
					case Enumerators.ThreatTypes.NET:
						{
							Services.Get<INetPool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.ROPE:
						{
							Services.Get<IRopePool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.SEAWEED:
						{
							Services.Get<ISeaweedPool>().ShowObject();
						}
						break;

					// ===========================================================
					// OTHERS
					case Enumerators.ThreatTypes.CHAIN:
						{
							Services.Get<IChainPool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.MINE:
						{
							Services.Get<IMinePool>().ShowObject();
						}
						break;

					// ===========================================================
					case Enumerators.ThreatTypes.ROCK:
						{
							Services.Get<IRockPool>().ShowObject();
						}
						break;

						// ===========================================================
				}
				_countThreatsSpawn++;
			}

			else
			{
				CancelInvoke("SpawnThreat");
			}
		}

		private void SpawnBonus()       // used in Invoke()
		{
			if (_countBonusesSpawn < _bonusesTypes.Count)
			{
				int rndElement = _idBonusesTypes[_countBonusesSpawn];
				Services.Get<IBonusPool>().ShowObject( _bonusesTypes[rndElement] );

				_countBonusesSpawn++;
			}
			else
			{
				CancelInvoke("SpawnBonus");
			}
		}

		private void SpawnTreasure()    // used in Invoke()
		{
			if (_countTreasuresSpawn < _treasures.treasures)
			{
				Services.Get<ITreasurePool>().ShowObject();

				_countTreasuresSpawn++;
			}
			else
			{
				CancelInvoke("SpawnTreasure");
			}
		}

		#endregion
	
		#region PROGRESS_BAR

		private void ShowProgresBarLevel()
		{
			float currPosX = GetCurrentProgressBarPosX();
			_objSubmarineMini.anchoredPosition = new Vector2(currPosX, _objSubmarineMini.anchoredPosition.y);
		}

		private float GetCurrentProgressBarPosX()
		{
			const float min_x = 0f;
			const float max_x = 500f;

			float currPosX = (_timer * max_x) / _endTime;

			if (currPosX <= min_x)
			{
				currPosX = min_x;
			}
			else if (currPosX >= max_x)
			{
				currPosX = max_x;
			}

			return currPosX;
		}

		#endregion


	}
}