﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Gameplay
{
	public class DamageBubbles : AMonoService, IDamageBubbles
	{
		// FIELDS
		// ---------------------------------------
		//
		private bool _isDamaged;

		// ---------------------------------------
		//
		#region INNER CLASS: DAMAGE COUNT

		private class DamageCount
		{
			private int _counter;

			public int Get_Counter()
			{
				return _counter;
			}

			public void Increase()
			{
				_counter++;
			}

			public void Reduce()
			{
				_counter--;

				if (_counter < 0) {
					_counter = 0; }
			}
		}

		private DamageCount _damageCount = new DamageCount();

		#endregion

		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IDamageBubbles>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IDamageBubbles>();
		}



		// INTERFACES
		public void IncreaseDamageCount()
		{
			_damageCount.Increase();

			if (!_isDamaged)
			{
				_isDamaged = true;
				_soundManager.PlaySoundRepeating(SoundsGameplayFX.Misc.bubblesOnHit);
			}
		}

		public void ReduceDamageCount()
		{
			_damageCount.Reduce();

			if (_damageCount.Get_Counter() == 0)
			{
				_isDamaged = false;
				_soundManager.StopSound(SoundsGameplayFX.Misc.bubblesOnHit);
			}
		}



	}
}
