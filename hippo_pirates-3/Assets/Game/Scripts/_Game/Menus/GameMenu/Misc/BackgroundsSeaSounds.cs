﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Pirates_3.Common;
using Zenject;

namespace Pirates_3.Menu.Gameplay
{
	public class BackgroundsSeaSounds : AMonoScript
	{
		// FIELDS
		// ----------------------------------------------
		private readonly string _strSoundDolphin = "dolphin";

		private readonly string _strSoundWhaleOne = "whale-1";
		private readonly string _strSoundWhaleTwo = "whale-2";
		private readonly string _strSoundWhaleThree = "whale-3";

		private Dictionary<int, string> _dictSeaSounds;

		// ----------------------------------------------
		private readonly string _strSoundSonar = "sonar";

		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			InitDictionarySeaSounds();

			StartCoroutine(PlaySeaSoundsCicle());
			StartCoroutine(PlaySonarSoundCircle());
		}

		protected override void AssertVariables()
		{
			// no need to aasert variables here !
		}



		// METHODS
		private void InitDictionarySeaSounds()
		{
			_dictSeaSounds = new Dictionary<int, string>
		{
			{ 0, _strSoundDolphin    },

			{ 1, _strSoundWhaleOne   },
			{ 2, _strSoundWhaleTwo   },
			{ 3, _strSoundWhaleThree }
		};
		}

		private int GetRandomSeaSound()
		{
			return Random.Range(0, _dictSeaSounds.Count);
		}

		private IEnumerator PlaySeaSoundsCicle()
		{
			const float delayBeforeSoundPlay = 11f;     // in seconds

			while (true)
			{
				yield return new WaitForSeconds(delayBeforeSoundPlay);

				int rndSoundNumber = GetRandomSeaSound();

				_soundManager.PlaySound(_dictSeaSounds[rndSoundNumber]);
			}
		}

		private IEnumerator PlaySonarSoundCircle()
		{
			const float delayBetweenSonarSound = 3f;        // in seconds

			while (true)
			{
				yield return new WaitForSeconds(delayBetweenSonarSound);

				_soundManager.PlaySound(_strSoundSonar);
			}
		}



	}
}