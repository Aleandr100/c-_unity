﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay
{
	public class TutorialCheck : AMonoScript
	{
		// FIELDS
		[SerializeField] private GameObject _skipTutorialButton;
		[SerializeField] private GameObject _settingsButton;
		[SerializeField] private GameObject _objTutorial;



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			Check();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		private void Check()
		{
			bool isTutorial = (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);

			_skipTutorialButton.SetActive(isTutorial); 
			_settingsButton.SetActive(!isTutorial);
			_objTutorial.SetActive(isTutorial);
		}
	


	}
}