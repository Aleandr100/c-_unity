﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay
{
	public class GameMenu : AMonoMenu
	{
		// FIELDS
		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			//ManagerGoogle.Instance.ShowSmallBanner();
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			Application.targetFrameRate = 60;                   // FPS - frame per second
			//ManagerGoogle.Instance.RepositionSmallBanner(AdmobAd.AdLayout.Bottom_Left);

			ManagerGoogle.Instance.HideSmallBanner();
		}

		protected override void InitStart()
		{
			PlayMusic();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}



		// UNITY
		private void OnDestroy()
		{
			ManagerGoogle.Instance.HideSmallBanner();
			AudioController.Release();
		}



		// ON CLICK
		public void OnClickBackButton()
		{
			LevelsController.LoadMainMenu();
		}

		public void OnClickSkipTutorialButton()
		{
			Services.Get<ISaveGameManager>().Set_IsTutorialCompleted();
			LevelsController.LoadGameplayLevelOne();
		}



		// METHODS
		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}



	}
}