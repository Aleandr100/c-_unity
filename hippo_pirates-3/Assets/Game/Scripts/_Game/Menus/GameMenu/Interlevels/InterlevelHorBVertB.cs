﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using Pirates_3.Common.Info.AnimationParameters;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Interlevels
{
	public class InterlevelHorBVertB : AMonoMenu
	{
		// FIELDS
		// -----------------------------------------------
		[Header("ANIMATORS")]
		[SerializeField] private Animator _animatorSubmarine;
		[SerializeField] private Animator _animatorScrewWaves;
		[SerializeField] private Animator _animatorProjectors;

		// -----------------------------------------------
		[Header("MISC")]
		[SerializeField] private Transform _camera;

		// -----------------------------------------------
		[Header("POINTS")]
		[SerializeField] private Transform _middlePos;
		[SerializeField] private Transform _endPos;

		// -----------------------------------------------
		// actions
		private readonly List<Func<float>> _actions = new List<Func<float>>();

		private float _coeffMove = 0.6f;
		private float _speedAdjust = 2f;

		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;

		private IAnimationManager _animationManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			InitActionsList();
			PlayMusic();

			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgScrewRotate);
			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgUnderwater);

			//																				   
			StartCoroutine(Sequences());
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}



		// INTERFACES (CLICK)
		public void ClickButtonSkip()
		{
			StopAllCoroutines();
			LevelsController.LoadNextLevel();
		}



		// METHODS
		private void InitActionsList()
		{
			AddAction(SubmarineLightsUp);
			AddAction(SubmarineScrewWavesHorizontal);
			AddAction(SubmarineMoveMiddlePoint);
			AddAction(SubmarineScrewWavesVertical);
			AddAction(SubmarineMoveEndPoint);
			AddAction(NextLevel);
		}

		// =========================================================
		// actions
		private float SubmarineLightsUp()
		{
			_animationManager.SetParameterBool(_animatorProjectors, ParamBools.Submarine.isLightUp, true);

			return 0f;
		}

		private float SubmarineScrewWavesHorizontal()
		{
			_animationManager.SetParameterInt(_animatorScrewWaves,
				ParamInts.ScrewWaves.direction, (int)ParamInts.ScrewWaves.Directions.LEFT);

			return 0f;
		}

		private float SubmarineMoveMiddlePoint()
		{
			float moveTime = 10f / _speedAdjust;
			_camera.DOMove(_middlePos.position, moveTime).SetEase(Ease.Linear);

			return moveTime;
		}

		private float SubmarineScrewWavesVertical()
		{
			_animationManager.SetParameterInt(_animatorScrewWaves,
				ParamInts.ScrewWaves.direction, (int)ParamInts.ScrewWaves.Directions.UP);

			return 0f;
		}

		private float SubmarineMoveEndPoint()
		{
			float moveTime = 6f / _speedAdjust;
			_camera.DOMove(_endPos.position, moveTime).SetEase(Ease.Linear);

			return moveTime * _coeffMove;
		}

		private float NextLevel()
		{
			LevelsController.LoadNextLevel();

			return 0f;
		}

		// =========================================================

		#region MISC

		private void AddAction(Func<float> action)
		{
			_actions.Add(action);
		}

		private IEnumerator Sequences()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actions.Count)
				{
					break;
				}

				delay = _actions[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}



	}
}