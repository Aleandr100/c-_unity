﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Pirates_3.Common;
using Zenject;

namespace Pirates_3.Menu.Loose
{
	public class LooseMenuPanel : AMonoScript
	{
		// FIELDS
		// -------------------------------------------
		//
		[SerializeField] private RectTransform _panel;
		[SerializeField] private Text _textBacKCount;

		// -------------------------------------------
		//
		private const int _countMax = 10;
		private int _count;

		// -------------------------------------------
		// dependences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			_timerManager.StartTimer(ShowPanel, 2f);
			_timerManager.StartTimer(StartBackCount, 3f);
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void ClickReplayButton()
		{
			SafeCancelInvoke("TimerBackCount");
			LevelsController.LoadGameplayLevelOne();
		}
	


		// METHODS
		private void ShowPanel()
		{
			_panel.DOScale(new Vector3(1, 1), 0.5f);
		}

		private void StartBackCount()
		{
			_count = _countMax;
			SafeStartInvokeRepeating("TimerBackCount", 0f, 1f);
		}

		private void TimerBackCount()		// used in Invoke
		{
			_count --;
		
			if (_count <= 0)
			{
				SafeCancelInvoke("TimerBackCount");
				LevelsController.LoadMainMenu();
			}
			else
			{
				_textBacKCount.text = _count.ToString();
			}
		}
	


	}
}