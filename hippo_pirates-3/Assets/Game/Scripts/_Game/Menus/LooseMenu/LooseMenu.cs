﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Loose
{
	public class LooseMenu : AMonoMenu
	{
		// FIELDS
		// ----------------------------------------------------------------------
		[SerializeField] private Animator _animatorRaccoon;

		// ----------------------------------------------------------------------
		// actions
		private readonly List< Func<float> > _actions = new List< Func<float> >();

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			//ManagerGoogle.Instance.RepositionSmallBanner(AdmobAd.AdLayout.Bottom_Left);
			InitActionsList();

			ManagerGoogle.Instance.HideSmallBanner();
		}

		protected override void InitStart()
		{
			PlayMusic();
			_soundManager.PlaySoundRepeating(SoundsLooseFX.bgWavesLoose);

			//
			StartActionCicle();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnDestroy()
		{
			AudioController.Release();
		}

	

		// ON CLICK
		public void OnClickMainMenu()
		{
			LevelsController.LoadMainMenu();
		}
	


		// METHODS
		private void InitActionsList()
		{
			AddAction(RaccoonThrowKostil);
			AddAction(RaccoonLooseSpeeck);
			AddAction(RaccoonIdleSpeech);
		}

		// =====================================================
		// actions
		private float RaccoonThrowKostil()
		{
			_timerManager.StartTimer<object>(
				_animationManager.SetParameterTrigger,
				(new object[] { _animatorRaccoon, ParamTriggers.RaccoonLoose.needThrowKostil }), 1f);

			return 2.5f;
		}

		private float RaccoonLooseSpeeck()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonLoose.needLooseSpeech);

			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Loose.loose);
			_soundManager.PlaySound(SoundsSpeech.Loose.loose);

			return soundLenght;
		}

		private float RaccoonIdleSpeech()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonLoose.needIdleLoose);

			return 0f;
		}

		// =====================================================

		#region MISC

		private void AddAction(Func<float> action)
		{
			_actions.Add(action);
		}

		private void StartActionCicle()
		{
			StartCoroutine(ActionCicle());
		}

		private IEnumerator ActionCicle()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actions.Count)
				{
					break;
				}

				delay = _actions[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}


	}
}