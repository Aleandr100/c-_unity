﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Init
{
	public class SettingsButton : AMonoScript
	{
		// FIELDS
		[SerializeField] private GameObject _bloker;

		private SettingsPanel _sp;



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_sp = GetComponent<SettingsPanel>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _sp);
	#endif
			#endregion
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES (button click)
		public void ClickSettingsButton()
		{
			SetGamePause(true);
		}

		public void ClickResumeButton()
		{
			SetGamePause(false);
			ShowBorder(false);
			_sp.ShowPannel(false);
		}

		public void ClickMainMenu()
		{
			SetGamePause(false);
		}
	


		// METHODS
		private void SetGamePause(bool boolValue)
		{
			Time.timeScale = (boolValue) ? (0) : (1);
		}

		private void ShowBorder(bool boolValue)
		{
			_bloker.SetActive(boolValue);
		}


	
	}
}