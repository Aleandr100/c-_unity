﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Init
{
	public class InitMenu : AMonoMenu
	{



		// A_SCRIPTS
		protected override void InitAwake()
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			//ManagerGoogle.Instance.RepositionSmallBanner(AdmobAd.AdLayout.Bottom_Left);

			ManagerGoogle.Instance.HideSmallBanner();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



	}
}
