﻿// Базовый класс для всех Меню (отвечает за конкретную сцену Unity, т.е. уровень либо меню)
// Цель: обнулить список сервис-локатора перед выходом из сцены

namespace Pirates_3.Common
{
	public abstract class AMonoMenu : AMonoScript
	{



		// METHODS
		protected void OnDisable()
		{
			ClearServices();
		}
	
		private void ClearServices()
		{
			Services.Clear();
		}

	
	
	}
}