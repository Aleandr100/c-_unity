﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using Zenject;

namespace Pirates_3.Menu.ChooseDifficulty
{

	public class ChooseDifficultyMenu : IInitializable, IDisposable
	{
		// FIELDS
		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// ZENJECT
		public void Initialize()
		{
			ManagerGoogle.Instance.HideSmallBanner();
			PlayMusic();
		}

		public void Dispose()
		{
			AudioController.Release();
		}



		// METHODS
		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}


	}
}