﻿
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.ChooseDifficulty
{
	public class ChooseDifficultyButtonsClickHandler : IChooseDifficultyButtonsClickHandler
	{
		// FIELDS
		// ---------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
			_soundManager = soundManager;
		}



		// INTERFACES
		public void HandleClickButtonVeryEasy()
		{
			PlayClickSound();
			SetDifficultyLevel(Enumerators.LevelDifficulty.VERY_EASY);
			LoadIntro();
		}

		public void HandleClickButtonEasy()
		{
			PlayClickSound();
			SetDifficultyLevel(Enumerators.LevelDifficulty.EASY);
			LoadIntro();
		}

		public void HandleClickButtonNormal()
		{
			PlayClickSound();
			SetDifficultyLevel(Enumerators.LevelDifficulty.NORMAL);
			LoadIntro();
		}

		public void HandleClickButtonMainMenu()
		{
			LevelsController.LoadMainMenu();
		}



		// METHODS
		private void PlayClickSound()
		{
			_soundManager.PlaySound(SoundsLevelSelectFX.selectLevel);
		}

		private void SetDifficultyLevel(Enumerators.LevelDifficulty levelDifficulty)
		{
			LevelsController.Set_CurrentDifficultyLevel(levelDifficulty);
		}

		private void LoadIntro()
		{
			LevelsController.LoadIntroTavern();
		}


	}
}