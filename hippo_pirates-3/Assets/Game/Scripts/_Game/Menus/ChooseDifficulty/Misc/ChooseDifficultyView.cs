﻿
using Pirates_3.Common;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Pirates_3.Menu.ChooseDifficulty
{
	public class ChooseDifficultyView : IInitializable
	{
		// FIELDS
		// -----------------------------------
		// stars
		private Sprite _spriteStarEmpty;
		private Sprite _spriteStarGold;

		// very easy
		private Image[] _imagesStarsVeryEasy;

		// easy
		private Button _buttonEasy;
		private Image _imageEasy;
		private Image[] _imagesStarsEasy;

		// normal
		private Button _buttonNormal;
		private Image _imageNormal;
		private Image[] _imagesStarsNormal;

		// -----------------------------------
		//
		private enum Difficulties
		{
			EASY,
			NORMAL
		}

		// -----------------------------------
		// dependences
		private IChooseDifficultySceneData _sceneData;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(IChooseDifficultySceneData chooseDifficultySceneData)
		{
			_sceneData = chooseDifficultySceneData;
		}



		// ZENJECT
		public void Initialize()
		{
			InitVariables();
			AssertVariables();

			ShowDifficultySettingData();
		}

		private void InitVariables()
		{
			// stars
			_spriteStarEmpty = _sceneData.Get_SpriteStarEmpty();
			_spriteStarGold = _sceneData.Get_SpriteStarGold();

			// very easy
			_imagesStarsVeryEasy = _sceneData.Get_ImagesStarsVeryEasy();

			// easy
			_buttonEasy = _sceneData.Get_ButtonEasy();
			_imageEasy = _sceneData.Get_ImageEasy();
			_imagesStarsEasy = _sceneData.Get_ImagesStarsEasy();

			// normal
			_buttonNormal = _sceneData.Get_ButtonNormal();
			_imageNormal = _sceneData.Get_ImageNormal();
			_imagesStarsNormal = _sceneData.Get_ImagesStarsNormal();
		}

		private void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}



		// METHODS
		private void ShowDifficultySettingData()
		{
			ShowVeryEasy();
			ShowEasy();
			ShowNormal();
		}

		private void ShowVeryEasy()
		{
			int numberOfStarsVeryEasy = Services.Get<ISaveGameManager>().Get_CountStarsVeryEasy();

			ShowStars(_imagesStarsVeryEasy, numberOfStarsVeryEasy);
			CheckUnblockDifficulty(Difficulties.EASY, numberOfStarsVeryEasy);
		}

		private void ShowEasy()
		{
			int numberOfStarsEasy = Services.Get<ISaveGameManager>().Get_CountStarsEasy();

			ShowStars(_imagesStarsEasy, numberOfStarsEasy);
			CheckUnblockDifficulty(Difficulties.NORMAL, numberOfStarsEasy);
		}

		private void ShowNormal()
		{
			int numberOfStarNormal = Services.Get<ISaveGameManager>().Get_CountStarsNormal();

			ShowStars(_imagesStarsNormal, numberOfStarNormal);
		}

		private void ShowStars(Image[] stars, int saveGameNumberOfStars)
		{
			for (int i = 0; i < stars.Length; i++)
			{
				stars[i].sprite = (i < saveGameNumberOfStars) ? (_spriteStarGold) : (_spriteStarEmpty);
			}
		}

		private void CheckUnblockDifficulty(Difficulties difficulty, int starsNumber)
		{
			if (starsNumber == Services.Get<ISaveGameManager>().Get_MaxCountStars())
			{
				CheckUnblock(difficulty);
			}
		}

		private void CheckUnblock(Difficulties difficulty)
		{
			switch (difficulty)
			{
				case Difficulties.EASY:
					ShowUnblockedEasy();
					break;

				case Difficulties.NORMAL:
					ShowUnblockNormal();
					break;

				default:
					#region DEBUG
#if UNITY_EDITOR
					Debug.Log("[ERROR] unknown Difficulty enum !");
#endif
					#endregion
					break;
			}
		}

		private void ShowUnblockedEasy()
		{
			_buttonEasy.interactable = true;

			_imageEasy.color = Color.white;

			for (int i = 0; i < _imagesStarsEasy.Length; i++)
			{
				_imagesStarsEasy[i].color = Color.white;
			}
		}

		private void ShowUnblockNormal()
		{
			_buttonNormal.interactable = true;

			_imageNormal.color = Color.white;

			for (int i = 0; i < _imagesStarsNormal.Length; i++)
			{
				_imagesStarsNormal[i].color = Color.white;
			}
		}



	}
}