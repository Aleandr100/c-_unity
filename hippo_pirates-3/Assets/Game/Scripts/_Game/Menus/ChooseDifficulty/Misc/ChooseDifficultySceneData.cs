﻿
using Pirates_3.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Pirates_3.Menu.ChooseDifficulty
{
	public class ChooseDifficultySceneData : MonoBehaviour, IChooseDifficultySceneData
	{
		// FIELDS
		// -------------------------------------
		//
		[Header("BUTTONS")]
		[SerializeField] private Button _buttonVeryEasy;
		[SerializeField] private Button _buttonEasy;
		[SerializeField] private Button _buttonNormal;
		[SerializeField] private Button _buttonMainMenu;

		// ---------------------------------
		//
		[Header("STARS IMAGES")]
		[SerializeField] private Sprite _spriteStarEmpty;
		[SerializeField] private Sprite _spriteStarGold;

		// ---------------------------------
		//
		[Header("VERY EASY")]
		[SerializeField] private Image[] _imagesStarsVeryEasy;

		// ---------------------------------
		//
		[Header("EASY")]
		[SerializeField] private GameObject _objEasy;
		[SerializeField] private Image _imageEasy;
		[SerializeField] private Image[] _imagesStarsEasy;

		// ---------------------------------
		//
		[Header("NORMAL")]
		[SerializeField] private GameObject _objNormal;
		[SerializeField] private Image _imageNormal;
		[SerializeField] private Image[] _imagesStarsNormal;



		// UNITY
		private void Start()
		{
			AssertVariables();
		}

		private void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}



		// INTERFACE

		// =============================================
		// buttons
		public Button Get_ButtonVeryEasy()
		{
			return _buttonVeryEasy;
		}

		public Button Get_ButtonEasy()
		{
			return _buttonEasy;
		}

		public Button Get_ButtonNormal()
		{
			return _buttonNormal;
		}

		public Button Get_ButtonMainMenu()
		{
			return _buttonMainMenu;
		}

		// =============================================
		// stars
		public Sprite Get_SpriteStarEmpty()
		{
			return _spriteStarEmpty;
		}

		public Sprite Get_SpriteStarGold()
		{
			return _spriteStarGold;
		}

		// =============================================
		// very easy
		public Image[] Get_ImagesStarsVeryEasy()
		{
			return _imagesStarsVeryEasy;
		}

		// =============================================
		// easy
		public GameObject Get_ObjectEasy()
		{
			return _objEasy;
		}

		public Image[] Get_ImagesStarsEasy()
		{
			return _imagesStarsEasy;
		}

		public Image Get_ImageEasy()
		{
			return _imageEasy;
		}

		// =============================================
		// normal
		public GameObject Get_ObjectNormal()
		{
			return _objNormal;
		}

		public Image[] Get_ImagesStarsNormal()
		{
			return _imagesStarsNormal;
		}

		public Image Get_ImageNormal()
		{
			return _imageNormal;
		}



	}
}