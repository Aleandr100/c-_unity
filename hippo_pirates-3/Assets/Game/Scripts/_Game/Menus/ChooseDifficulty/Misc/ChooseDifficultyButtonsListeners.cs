﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine.UI;
using Zenject;

namespace Pirates_3.Menu.ChooseDifficulty
{
	public class ChooseDifficultyButtonsListeners : IInitializable
	{
		// FIELDS
		// ---------------------------------------
		//
		private Button _buttonVeryEasy;
		private Button _buttonEasy;
		private Button _buttonNormal;
		private Button _buttonMainMenu;

		// ---------------------------------------
		// dependences
		private IChooseDifficultySceneData _sceneData;
		private IChooseDifficultyButtonsClickHandler _clickHandler;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(IChooseDifficultySceneData sceneData,
								IChooseDifficultyButtonsClickHandler clickHandler)
		{
			_sceneData = sceneData;
			_clickHandler = clickHandler;
		}



		// ZENJECT
		public void Initialize()
		{
			InitVariables();
			AddListeners();
		}

		private void InitVariables()
		{
			_buttonVeryEasy = _sceneData.Get_ButtonVeryEasy();
			_buttonEasy = _sceneData.Get_ButtonEasy();
			_buttonNormal = _sceneData.Get_ButtonNormal();
			_buttonMainMenu = _sceneData.Get_ButtonMainMenu();
		}



		// METHODS
		private void AddListeners()
		{
			_buttonVeryEasy.onClick.AddListener(OnClickButtonVeryEasy);
			_buttonEasy.onClick.AddListener(OnClickButtonEasy);
			_buttonNormal.onClick.AddListener(OnClickButtonNormal);
			_buttonMainMenu.onClick.AddListener(OnClickButtonMainMenu);
		}

		#region ON_CLICK

		private void OnClickButtonVeryEasy()
		{
			_clickHandler.HandleClickButtonVeryEasy();
		}

		private void OnClickButtonEasy()
		{
			_clickHandler.HandleClickButtonEasy();
		}

		private void OnClickButtonNormal()
		{
			_clickHandler.HandleClickButtonNormal();
		}

		private void OnClickButtonMainMenu()
		{
			_clickHandler.HandleClickButtonMainMenu();
		}

		#endregion


	}
}