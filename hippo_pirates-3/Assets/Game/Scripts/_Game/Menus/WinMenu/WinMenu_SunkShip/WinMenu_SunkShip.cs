﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using DG.Tweening;
using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using PSV_Prototype;
using Zenject;

namespace Pirates_3.Menu.Win
{
	public class WinMenu_SunkShip : AMonoMenu
	{
		// FIELDS
		// ---------------------------------------------------
		[Header("[RACCOON]")]
		[SerializeField] private Animator _animatorRaccoon;

		// ---------------------------------------------------
		[Header("[SUBMARINE]")]
		[SerializeField] private Transform _objSubmarine;
		[SerializeField] private Animator _animatorSubmarine;
		[SerializeField] private Animator _animatorScrew;

		// ---------------------------------------------------
		[Header("TREASURES")]
		[SerializeField] private Transform _objForTreasure;
		[SerializeField] private Transform[] _treasures;

		// ---------------------------------------------------
		private int _countTreasures;

		// ---------------------------------------------------
		// points
		private readonly Vector2 _posStart =  new Vector2 ( -1.5f,  10f   );
		private readonly Vector2 _posDown_A = new Vector2 (	-1.5f,  1.15f );
		private readonly Vector2 _posDown_B = new Vector2 (	-0.74f, 1.15f );
		private readonly Vector2 _posDown_C = new Vector2 ( 0.86f,  1.15f );
		private readonly Vector2 _posDown_D = new Vector2 ( 1.78f,  1.15f );
		private readonly Vector2 _posDown_E = new Vector2 ( 3.77f,  1.15f );

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			ManagerGoogle.Instance.HideSmallBanner();
			//ManagerGoogle.Instance.RepositionSmallBanner(AdmobAd.AdLayout.Bottom_Left);
		}

		protected override void InitStart()
		{
			StartSequence();

			PlayMusic();
			PlayBackgroundSounds();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnDestroy()
		{
			AudioController.Release();
		}

	

		// INTERFACES
		public void ActionOnGrabAnimation()
		{
			SetTreasureToManipulator(_countTreasures);
		
			switch (_countTreasures)
			{
				// =====================================================
				//
				case 2:
				{
					MoveSubmarine(_posDown_B, 1f);
				}
				break;
				// =====================================================
				//
				case 3:
				{
					MoveSubmarine(_posDown_C, 1f);
				}
				break;
				// =====================================================
				//
				case 4:
				{
					_animationManager.SetParameterInt(_animatorSubmarine, ParamInts.Manipulator.direction, (int)ParamInts.Manipulator.Directions.RIGHT);
					MoveSubmarine(_posDown_D, 1f);
				}
				break;
				// =====================================================			
				//
				case 8:
				{
					MoveSubmarine(_posDown_E, 1f);
					_animationManager.SetParameterInt(_animatorSubmarine, ParamInts.Manipulator.direction, (int)ParamInts.Manipulator.Directions.NONE);
					_timerManager.StartTimer<object>( MoveSubmarine, (new object[] { _posStart, 2f }), 2f );
					_timerManager.StartTimer( LoadWinTavernScene, 4f );
				}
				break;
				// =====================================================
			}

			_countTreasures ++;
		}



		// ON CLICK
		public void OnClickMainMenu()
		{
			LevelsController.LoadMainMenu();
		}



		// METHODS
		private void StartSequence()
		{
			// =======================================================================================
			//
			Services.Get<ISubmarineVisualEffects>().StartScrewRotate();

			_timerManager.StartTimer<object>(MoveSubmarine, (new object[] { _posDown_A, 2f }), 1f );

			_timerManager.StartTimer<object>(
				_animationManager.SetParameterInt,
					(new object[] { _animatorSubmarine, ParamInts.Manipulator.direction, (int)ParamInts.Manipulator.Directions.LEFT }), 3f);

			// =======================================================================================
			//
			_timerManager.StartTimer<object>(
				_animationManager.SetParameterBool,
					(new object[] { _animatorRaccoon, ParamBools.Raccoon.isSpeechOnRepair, true }), 2.8f);
		
			_timerManager.StartTimer(
				_soundManager.PlaySound, SoundsSpeech.Win.winSunkShip, 3f);

			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Win.winSunkShip);

			_timerManager.StartTimer<object>(
				_animationManager.SetParameterBool,
					(new object[] { _animatorRaccoon, ParamBools.Raccoon.isSpeechOnRepair, false }), 2.8f + soundLenght);

			// =======================================================================================
		}

		private void MoveSubmarine(Vector2 myPos, float moveTime)
		{
			_objSubmarine.DOMove(myPos, moveTime).SetEase(Ease.Linear);
		}

		private void MoveSubmarine(object[] param)
		{
			// param[0] - Vector2 myPos
			// param[1] - float moveTime

			Vector2 myPos = (Vector2) param[0];
			float moveTime = (float) param[1];

			MoveSubmarine(myPos, moveTime);
		}

		private void SetTreasureToManipulator(int numTreasure)
		{
			Transform myTreasure = _treasures[numTreasure];

			myTreasure.SetParent(_objForTreasure);
			myTreasure.position = _objForTreasure.position;
			myTreasure.localScale = new Vector2(0.4f, 0.4f);

			_timerManager.StartTimer( HideTreasure, myTreasure, 0.5f );
		}

		private void HideTreasure(Transform go)
		{
			go.gameObject.SetActive(false);
		}

		private void LoadWinTavernScene()
		{
			SceneLoader.Instance.SwitchToScene(Scenes.WinMenu_Tavern);
		}

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}

		private void PlayBackgroundSounds()
		{
			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgScrewRotate);
			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgUnderwater);
		}


	
	}
}
