﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Win
{
	public class SubmarineManipulator : AMonoScript
	{
		// FIELDS
		// -------------------------------------
		//
		[SerializeField] private WinMenu_SunkShip _winMenuScript;

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// ANIM EVENTS
		public void GrabTreasure()
		{
			_soundManager.PlaySound(SoundsWinFX.manipulator);
			_winMenuScript.ActionOnGrabAnimation();
		}
	


	}
}