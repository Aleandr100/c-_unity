﻿using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Win
{
	public class GoldThrowTrigerEvents : MonoBehaviour
	{
		// FIELDS
		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_timerManager = timerManager;
		}



		// TRIGER EVENTS
		public void GrabGoldCoins()
		{
			_soundManager.PlaySound(SoundsWinFX.coinsOne);
		}

		public void GoldCoinsFallOnFlour()
		{
			_soundManager.PlaySound(SoundsWinFX.coinsTwo);

			_timerManager.StartTimer(
				_soundManager.PlaySound, SoundsWinFX.coinsThree, 0.2f);
		}



	}
}