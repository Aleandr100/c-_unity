﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using System;
using System.Collections;
using System.Collections.Generic;
using PSV_Prototype;
using RateMePlugin;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Win
{
	public class WinMenu_Tavern : AMonoMenu
	{
		// FIELDS
		// ------------------------------------------------
		[Header("RACCOON")]
		[SerializeField] private Transform _objRaccoon;
	
		private Animator _animatorRaccoon;

		// ------------------------------------------------
		[Header("GI")]
		[SerializeField] private Transform _objGi;

		private Animator _animatorGi;

		// ------------------------------------------------
		// actions
		private readonly List< Func<float> > _actions = new List< Func<float> >();

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			InitVariables();
			InitActionsList();

			ManagerGoogle.Instance.HideSmallBanner();
		}

		protected override void InitStart()
		{
			PlayMusic();

			//
			StartActionCicle();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnDestroy()
		{
			AudioController.Release();
		}
	


		// INTERFACES (CLICK)
		public void ClickButtonSkip()
		{
			StopAllCoroutines();
			LevelsController.LoadMainMenu();
		}
	


		// METHODS
		private void InitVariables()
		{
			_animatorRaccoon = _objRaccoon.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorRaccoon);
	#endif
			#endregion
		
			_animatorGi = _objGi.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorGi);
	#endif
			#endregion
		}

		private void InitActionsList()
		{
			AddAction(WaitFromStart);
			AddAction(RaccoonSpeech);
			AddAction(GiSpeech);
			AddAction(WinGame);
		}

		// =========================================================
		//
		private float WaitFromStart()
		{
			// --- none ---

			return 5f;
		}

		private float RaccoonSpeech()
		{
			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Win.winTavern);
			_soundManager.PlaySound(SoundsSpeech.Win.winTavern);

			_animationManager.SetParameterBool(_animatorRaccoon, ParamBools.Raccoon.isSpeechWinTavern, true);

			_timerManager.StartTimer<object>(
				_animationManager.SetParameterBool,
				(new object[] { _animatorRaccoon, ParamBools.Raccoon.isSpeechWinTavern, false }), soundLenght);

			return soundLenght;
		}

		private float GiSpeech()
		{
			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Intro.introTavernGiYeYe);
			_soundManager.PlaySound(SoundsSpeech.Intro.introTavernGiYeYe);

			_animationManager.SetParameterBool(_animatorGi, ParamBools.Diver.isSpeechWinTavern, true);

			_timerManager.StartTimer<object>(
				_animationManager.SetParameterBool,
					(new object[] { _animatorGi, ParamBools.Diver.isSpeechWinTavern, false }), soundLenght);

			return soundLenght + 10f;
		}
	
		private float WinGame()
		{
			if (RateMeModule.Instance.CanShow(Time.time))
			{
				SceneLoader.Instance.SwitchToScene(Scenes.RateUs);
			}
			else
			{
				LevelsController.LoadMainMenu();
			}


			return 0f;
		}

		// =========================================================
	
		#region MISC

		private void AddAction(Func<float> action)
		{
			_actions.Add(action);
		}

		private void StartActionCicle()
		{
			StartCoroutine(ActionCicle());
		}

		private IEnumerator ActionCicle()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actions.Count)
				{
					break;
				}

				delay = _actions[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}



	}
}