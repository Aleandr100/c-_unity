﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;

namespace Pirates_3.Common
{
	public class TimerManager : ITimerManager
	{
		// FIELDS
		// -------------------------------------
		//
		public delegate void OnTimerVoid();
		public delegate void OnTimer<in T>(T param);
		public delegate void OnTimerObjects<in T>(T[] param);




		// INTERFACE
		public void StartTimer(OnTimerVoid handler, float delay)
		{
			Services.Get<ICoroutineManagerMono>().Get_Instance()
				.StartCoroutine(DelayedMethodVoid(handler, delay));
		}

		public void StartTimer<T>(OnTimer<T> handler, T param, float delay)
		{
			Services.Get<ICoroutineManagerMono>().Get_Instance()
				.StartCoroutine(DelayedMethodT(handler, param, delay));
		}

		public void StartTimer<T>(OnTimerObjects<T> handler, T[] param, float delay)
		{
			Services.Get<ICoroutineManagerMono>().Get_Instance()
				.StartCoroutine( DelayedMethodObjects(handler, param, delay) );
		}



		// METHODS
		private IEnumerator DelayedMethodVoid(OnTimerVoid handler, float delay)
		{
			yield return new WaitForSeconds(delay);

			handler();
		}

		private IEnumerator DelayedMethodT<T>(OnTimer<T> handler, T param, float delay)
		{
			yield return new WaitForSeconds(delay);

			handler(param);
		}

		private IEnumerator DelayedMethodObjects<T>(OnTimerObjects<T> handler, T[] param, float delay)
		{
			yield return new WaitForSeconds(delay);

			handler(param);
		}

	
	
	



	}
}