﻿using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using UnityEngine;

// Класс, который используется как фэйк, чтоб из си-шарп скриптов использовать корутины

namespace Pirates_3.Common
{
	public class CoroutineManagerMono : AMonoService, ICoroutineManagerMono
	{
		// PRIVATE
		private CoroutineManagerMono _instance;




		// A_SERVICES
		protected override void InitAwake()
		{
			// cash data
			_instance = this;
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ICoroutineManagerMono>(this);
		}

		protected override void DeregisterService()
		{
			 Services.Remove<ICoroutineManagerMono>();
		}



		// INTERFACES
		public CoroutineManagerMono Get_Instance()
		{
			return _instance;
		}



	}
}