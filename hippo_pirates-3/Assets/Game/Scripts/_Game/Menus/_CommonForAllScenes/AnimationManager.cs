﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Common
{
	public class AnimationManager : IAnimationManager
	{



		// INTERFACES
		public void SetParameterBool(Animator myAnimator, string myParam, bool myBool)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myAnimator);
			Assert.IsTrue(myParam.Length > 0);
	#endif
			#endregion

			myAnimator.SetBool(myParam, myBool);
		}

		public void SetParameterBool(object[] param)
		{
			// param[0] - Animator myAnimator
			// param[1] - string myParam
			// param[2] - bool myBool

			Animator myAnimator = (Animator) param[0];
			string myParam = (string) param[1];
			bool myBool = (bool) param[2];

			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myAnimator);
			Assert.IsTrue(myParam.Length > 0);
	#endif
			#endregion

			myAnimator.SetBool(myParam, myBool);
		}

		//
		public void SetParameterInt(Animator myAnimator, string myParam, int myInt)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myAnimator);
			Assert.IsTrue(myParam.Length > 0);
	#endif
			#endregion

			myAnimator.SetInteger(myParam, myInt);
		}

		public void SetParameterInt(object[] param)
		{
			// param[0] - Animator myAnimator
			// param[1] - string myParam
			// param[2] - int myInt

			Animator myAnimator = (Animator) param[0];
			string myParam = (string) param[1];
			int myInt = (int) param[2];

			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myAnimator);
			Assert.IsTrue(myParam.Length > 0);
	#endif
			#endregion

			myAnimator.SetInteger(myParam, myInt);
		}

		//
		public void SetParameterTrigger(Animator myAnimator, string myTrig)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myAnimator);
			Assert.IsTrue(myTrig.Length > 0);
	#endif
			#endregion

			myAnimator.SetTrigger(myTrig);
		}

		public void SetParameterTrigger(object[] param)
		{
			// param[0] - Animator myAnioamtor
			// param[1] - string myTrig

			Animator myAnimator = (Animator) param[0];
			string myTrig = (string) param[1];

			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myAnimator);
			Assert.IsTrue(myTrig.Length > 0);
	#endif
			#endregion

			myAnimator.SetTrigger(myTrig);
		}
	


	}
}