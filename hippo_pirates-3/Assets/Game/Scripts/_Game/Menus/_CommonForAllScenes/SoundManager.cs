﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Common
{
	public class SoundManager : ISoundManager
	{
		// FIELDS
		private float _musicVolume = 0.5f;

	

		// INTERFACES
		public void PlaySound(string strSound)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(strSound.Length > 0);
	#endif
			#endregion

			AudioController.PlaySound(strSound);
		}

		public void PlaySoundRepeating(string strSound)
		{
			AudioController.PlayStreamCicle(strSound);
		}

		public void StopSound(string strSound)
		{
			AudioController.StopSound(strSound);
		}
	
		//
		public void PlayMusic(string strMusic)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(strMusic.Length > 0);
	#endif
			#endregion

			AudioController.PlayMusic(strMusic);
		}

		//
		public float GetSoundLenght(string strSound)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(strSound.Length > 0);
	#endif
			#endregion

			AudioClip clip = LanguageAudio.GetSoundByName(strSound);
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, clip);
	#endif
			#endregion

			return clip.length;
		}

		public string GetRandomSound(string[] someArray)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(someArray.Length > 0);
	#endif
			#endregion

			int rnd = Random.Range(0, someArray.Length);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(someArray[rnd].Length > 0);
	#endif
			#endregion

			return someArray[rnd];
		}

		public void StopAllRaccoonSpeeches()
		{
			// speech when have damage
			for (int i = 0; i < SoundsSpeech.Gameplay.haveDmgSpeeches.Length; i++)
			{
				AudioController.StopSound(SoundsSpeech.Gameplay.haveDmgSpeeches[i] );
			}

			// speech when repair damage
			for (int i = 0; i < SoundsSpeech.Gameplay.repairedSpeeches.Length; i++)
			{
				AudioController.StopSound(SoundsSpeech.Gameplay.repairedSpeeches[i] );
			}
		}

		//
		public void SetMisicVolume()
		{
			AudioController.SetMusicVolume(_musicVolume);
		}
	
	

	}
}