﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Intro
{
	public class IntroMenu_SecretCave : AMonoMenu
	{
		// FIELDS
	    #region Variables

		// -----------------------------------------------
		[Header("RACCOON")]
		[SerializeField] private Transform _objRaccoon;

		private Animator _animatorRaccoon;

		// -----------------------------------------------
		[Header("GI")]
		[SerializeField] private Transform _objGi;

		private Animator _animatorGi;

		// -----------------------------------------------
		[Header("STATIC OBJECTS")]
		[SerializeField] private Transform _objStand;
		[SerializeField] private Transform _objPier;

		// -----------------------------------------------
		[Header("SUBMARINE")]
		[SerializeField] private Transform _objSubmarine;
		[SerializeField] private Transform _objHatch;
		[SerializeField] private Transform _objProjector;
		[SerializeField] private Transform _objScrew;
		[SerializeField] private Transform _objScrewWave;

		private Animator _animatorSubmarine;
		private Animator _animatorHatch;
		private Animator _animatorProjector;
		private Animator _animatorScrew;
		private Animator _animatorScrewWave;

		// -----------------------------------------------
		[Header("CAMERA")]
		[SerializeField] private Camera _cameraMain;

		// -----------------------------------------------
		[Header("POINTS")]
		[SerializeField] private Transform _standEndPos;
		[SerializeField] private Transform _pierStartPos;
		[SerializeField] private Transform _pierEndPos;
		[SerializeField] private Transform _submarineUpPos;
		[SerializeField] private Transform _submarineInsidePos;

		// -----------------------------------------------
		// actions
		private readonly List< Func<float> > _actionsGi = new List< Func<float> >();
		private readonly List< Func<float> > _actionsRaccoon = new List< Func<float> >();

		private float _coeffTimeMove = 0.6f;		// don't wait for end of lerp in DOTween methods

		private const float _standMoveTime = 5f;

		// -----------------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;

		#endregion



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager)
		{
			_soundManager = soundManager;
			_animationManager = animationManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			InitAnimators();
			InitGiActionsList();
			InitRaccoonActionsList();

			ManagerGoogle.Instance.HideSmallBanner();
		}

		protected override void InitStart()
		{
			PlayMusic();

			//
			StartCoroutine(GiSequences());
			StartCoroutine(RaccoonSequences());
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		[SuppressMessage("ReSharper", "UnusedMember.Local")]
		private void OnDestroy()
		{
			AudioController.Release();
		}



	    // INTERFACES (CLICK)
		public void ClickButtonSkip()
		{
			StopAllCoroutines();

			if ( !Services.Get<ISaveGameManager>().Get_IsTutorialCompleted() )
			{
				LevelsController.LoadGameplayTutorial();
			}
			else
			{
				LevelsController.LoadGameplayLevelOne();
			}
		}



		// METHODS

		#region INITS

		private void InitAnimators()
		{
			// raccoon
			_animatorRaccoon = _objRaccoon.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorRaccoon);
	#endif
			#endregion

			// Gi
			_animatorGi = _objGi.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorGi);
#endif
			#endregion

			// Submarine
			_animatorSubmarine = _objSubmarine.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorSubmarine);
#endif
			#endregion

			_animatorHatch = _objHatch.GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR

			Assert.AreNotEqual(null, _animatorHatch);

	#endif
			#endregion

			_animatorProjector = _objProjector.GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR

			Assert.AreNotEqual(null, _animatorProjector);

	#endif
			#endregion

			_animatorScrew = _objScrew.GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR

			Assert.AreNotEqual(null, _animatorScrew);

	#endif
			#endregion

			_animatorScrewWave = _objScrewWave.GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR

			Assert.AreNotEqual(null, _animatorScrewWave);

	#endif
			#endregion
		}

		private void InitGiActionsList()
		{
			// Stand go down
			AddGiAction(StandGoDown);
			AddGiAction(SetGiAndRaccoonParent);

			// Camera and scale
			AddGiAction(CameraLongRange);
			AddGiAction(ScaleObjects);

			// Gi go to Submarine
			AddGiAction(GiWalkAfterStandGoDown);
			AddGiAction(GiGoToPierStart);
			AddGiAction(GiGoToPierEnd);
			AddGiAction(GiGoToSubmarineUp);
			AddGiAction(GiIdleAfterGoToSubmarineUp);
			AddGiAction(GiJumpShort);
			AddGiAction(GiIdleAfterJumpShort);
			AddGiAction(GiGoToSubmarineInside);

			AddGiAction(GiMakeSubmarineUpDown);
		}

		private void InitRaccoonActionsList()
		{
			// Wait for stand go down
			AddRaccoonAction(WaitForStandGoDown);

			// Raccoon go to Submarine
			AddRaccoonAction(RaccoonWalkAfterStandGoDown);
			AddRaccoonAction(RaccoonGoToPierStart);
			AddRaccoonAction(RaccoonGoToPierEnd);
			AddRaccoonAction(RaccoonGoToSubmarineUp);
			AddRaccoonAction(RaccoonIdleAfterGoToSubmarineUp);
			AddRaccoonAction(RaccoonSpeechOnIntroEnd);
			AddRaccoonAction(RaccoonIdleAfterSpeechOnIntroEnd);
			AddRaccoonAction(RaccoonGoToSubmarineInside);

			AddRaccoonAction(RaccoonMakeSubmarineUpDown);

			// Submarine
			AddRaccoonAction(SubmarineCloseHatch);
			AddRaccoonAction(SubmarineLightProjectors);
			AddRaccoonAction(SubmarineStartScrew);
			AddRaccoonAction(SubmarineStartScrewWaves);

			// load next level
			AddRaccoonAction(NextLevel);
		}

		#endregion

		#region GI_ACTIONS

		// =========================================================
		// Stand go down
		private float StandGoDown()
		{
			_soundManager.PlaySound(SoundsIntroFX.Misc.standMovesDown);

			_objStand.DOMove(_standEndPos.position, _standMoveTime);

			return _standMoveTime;
		}

		private float SetGiAndRaccoonParent()
		{
			_objGi.SetParent(_standEndPos);
			_objRaccoon.SetParent(_standEndPos);

			return 0f;
		}

		private float CameraLongRange()
		{
			Vector3 endPos = new Vector3(0.3f, -0.8f, -10f);
			const float endCameraSize = 4.5f;
			const float durationTween = 3f;

			// tween camera position
			_cameraMain.transform.DOMove(endPos, durationTween).SetEase(Ease.Linear);

			// tween camera size
			DOTween.To( () => _cameraMain.orthographicSize,
				x => _cameraMain.orthographicSize = x, endCameraSize, durationTween)
				.SetEase(Ease.Linear);

			return 0f;
		}

		private float ScaleObjects()
		{
			// static objects
			Vector2 endStandScale = new Vector2(1f, 1f);
			_objStand.DOScale(endStandScale, 2f).SetEase(Ease.Linear);

			Vector2 endPierScale = new Vector2(1f, 1f);
			_objPier.DOScale(endPierScale, 2f).SetEase(Ease.Linear);

			Vector2 endSubmarineScale = new Vector2(-1f, 1f);
			_objSubmarine.DOScale(endSubmarineScale, 2f).SetEase(Ease.Linear);

			// Gi and Raccoon
			Vector2 endRaccoonScale = new Vector2(-0.4f, 0.4f);
			_objRaccoon.DOScale(endRaccoonScale, 2f).SetEase(Ease.Linear);

			Vector2 endGiScale = new Vector2(0.25f, 0.25f);
			_objGi.DOScale(endGiScale, 2.5f).SetEase(Ease.Linear);

			return 0f;
		}

		// =========================================================
		// Gi go to Submarine
		private float GiWalkAfterStandGoDown()
		{
			_soundManager.StopSound(SoundsIntroFX.Misc.standMovesDown);

			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needWalk);

			return 0f;
		}

		private float GiGoToPierStart()
		{
			const float moveTime = 2f;
			_objGi.DOMove(_pierStartPos.position, moveTime);

			return moveTime * _coeffTimeMove;
		}

		private float GiGoToPierEnd()
		{
			const float moveTime = 3f;
			_objGi.DOMove(_pierEndPos.position, moveTime);

			return moveTime * _coeffTimeMove;
		}

		private float GiGoToSubmarineUp()
		{
			const float moveTime = 2f;
			_objGi.DOMove(_submarineUpPos.position, moveTime);

			return moveTime * _coeffTimeMove;
		}

		private float GiIdleAfterGoToSubmarineUp()
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needIdle);

			return 0f;
		}

		private float GiJumpShort()
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needJumpShort);

			return 0.5f;
		}

		private float GiIdleAfterJumpShort()
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needIdle);

			return 0f;
		}

		private float GiGoToSubmarineInside()
		{
			const float moveTime = 1f;
			_objGi.DOMove(_submarineInsidePos.position, moveTime);

			return 0.2f;		//moveTime * _coeffTimeMove;
		}

		private float GiMakeSubmarineUpDown()
		{
			_animationManager.SetParameterTrigger(_animatorSubmarine, ParamTriggers.SubmarineIntro.needUpDown);

			return 1f;
		}

		// =========================================================

		#endregion

		#region RACCOON_ACTIONS

		// =========================================================
		// Wait for stand go down
		private float WaitForStandGoDown()
		{
			// --- none ---

			return _standMoveTime;
		}

		// =========================================================
		// Raccoon go to Submarine
		private float RaccoonTurnLeft()
		{
			_objRaccoon.localScale = new Vector2(-_objRaccoon.localScale.x, _objRaccoon.localScale.y);

			return 0f;
		}

		private float RaccoonWalkAfterStandGoDown()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.needWalk);

			return 0f;
		}

		private float RaccoonGoToPierStart()
		{
			const float moveTime = 4f;
			_objRaccoon.DOMove(_pierStartPos.position, moveTime);

			return moveTime * _coeffTimeMove;
		}

		private float RaccoonGoToPierEnd()
		{
			const float moveTime = 5f;
			_objRaccoon.DOMove(_pierEndPos.position, moveTime);

			return moveTime * _coeffTimeMove;
		}

		private float RaccoonGoToSubmarineUp()
		{
			const float moveTime = 3f;
			_objRaccoon.DOMove(_submarineUpPos.position, moveTime);

			return moveTime * _coeffTimeMove;
		}

		private float RaccoonIdleAfterGoToSubmarineUp()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.needIdle);

			return 0f;
		}
	
		private float RaccoonSpeechOnIntroEnd()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.introCave);

			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Intro.introCaveRaccoon);
			_soundManager.PlaySound(SoundsSpeech.Intro.introCaveRaccoon);

			return soundLenght;
		}

		private float RaccoonIdleAfterSpeechOnIntroEnd()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.needIdle);

			return 0f;
		}

		private float RaccoonGoToSubmarineInside()
		{
			const float moveTime = 1.7f;
			_objRaccoon.DOMove(_submarineInsidePos.position, moveTime);

			return 0.2f;	//moveTime;
		}

		private float RaccoonMakeSubmarineUpDown()
		{
			_animationManager.SetParameterTrigger(_animatorSubmarine, ParamTriggers.SubmarineIntro.needUpDown);

			return 1f;
		}

		// =========================================================
		//
		private float SubmarineCloseHatch()
		{
			_animationManager.SetParameterTrigger(_animatorHatch, ParamTriggers.SubmarineIntro.needCloseHatch);

			_soundManager.PlaySound(SoundsGameplayFX.Misc.submarineCloseHatch);

			return 1f;
		}

		private float SubmarineLightProjectors()
		{
			_animationManager.SetParameterBool(_animatorProjector, ParamBools.Submarine.isLightUp, true);

			_soundManager.PlaySound(SoundsGameplayFX.Misc.submarineLightsUpProjectors);

			return 1.5f;
		}

		private float SubmarineStartScrew()
		{
			_animationManager.SetParameterTrigger(_animatorScrew, ParamTriggers.SubmarineIntro.needRotate);

			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgScrewRotate);

			return 0f;
		}

		private float SubmarineStartScrewWaves()
		{
			_objScrewWave.gameObject.SetActive(true);
			_animationManager.SetParameterInt(_animatorScrewWave, ParamInts.ScrewWaves.direction, (int)ParamInts.ScrewWaves.Directions.LEFT);

			return 2f;
		}
	
		// =========================================================
		//
		private float NextLevel()
		{
			if ( !Services.Get<ISaveGameManager>().Get_IsTutorialCompleted() )
			{
				LevelsController.LoadGameplayTutorial();
			}
			else
			{
				LevelsController.LoadGameplayLevelOne();
			}

			return 0f;
		}
	
		// =========================================================

		#endregion
		
		#region MISC

		// Gi
		private void AddGiAction(Func<float> action)
		{
			_actionsGi.Add(action);
		}

		private IEnumerator GiSequences()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsGi.Count)
				{
					break;
				}

				delay = _actionsGi[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay + 0.2f);
			}
		}

		// Raccoon
		private void AddRaccoonAction(Func<float> action)
		{
			_actionsRaccoon.Add(action);
		}

		private IEnumerator RaccoonSequences()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsRaccoon.Count)
				{
					break;
				}

				delay = _actionsRaccoon[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}

	

	}
}
