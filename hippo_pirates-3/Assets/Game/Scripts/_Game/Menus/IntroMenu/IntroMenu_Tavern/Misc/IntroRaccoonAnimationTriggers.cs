﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Intro
{
	public class IntroRaccoonAnimationTriggers : AMonoScript
	{
		// FIELDS
		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}


		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES (TRIGGERS)
		public void TriggerRaccoonKostilHit()
		{
			_soundManager.PlaySound(SoundsIntroFX.Raccoon.raccoonKostilHit);
		}

		public void TriggerRaccoonPullTorch()
		{
			//Services.Get<ISoundManager>().PlaySound(SoundsIntroFX.Raccoon.raccoonPullTorch);
		}

		public void TriggerRaccoonWalk()
		{
			_soundManager.PlaySound(SoundsIntroFX.Raccoon.raccoonWalk);
		}

	

	}
}
