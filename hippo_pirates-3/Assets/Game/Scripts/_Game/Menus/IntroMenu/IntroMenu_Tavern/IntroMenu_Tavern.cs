﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Intro
{
	public class IntroMenu_Tavern : AMonoMenu
	{
		// ---------------------------------------------
		[Header("[RACCOON]")]
		[SerializeField] private Transform _objRaccoon;

		private Animator _animatorRaccoon;

		// ---------------------------------------------
		[Header("GI")]
		[SerializeField] private Transform _objGi;

		private Animator _animatorGi;

		// ---------------------------------------------
		[Header("FLOOR")]
		[SerializeField] private GameObject _objFloor;
		[SerializeField] private Transform _objStand;
		
		// ---------------------------------------------
		[Header("CAMERA")]
		[SerializeField] private Camera _cameraMain;

		// ---------------------------------------------
		[Header("TORCH")]
		[SerializeField] private Transform _objTorch;

		private Animator _animatorTorch;

		// ---------------------------------------------
		[Header("STOOLS")]
		[SerializeField] private SpriteRenderer[] _srStools;

		// ---------------------------------------------
		[Header("POINTS")]
		[SerializeField] private Transform _giEnterPos;
		[SerializeField] private Transform _standEndPos;

		// ---------------------------------------------
		// actions
		private readonly List< Func<float> > _actions = new List< Func<float> >();

		// ---------------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			InitVariables();
			InitActionsList();

			ManagerGoogle.Instance.HideSmallBanner();
		}

		protected override void InitStart()
		{
			PlayMusic();

			//
			StartActionCicle();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnDestroy()
		{
			AudioController.Release();
		}



		// INTERFACES (CLICK)
		public void ClickButtonSkip()
		{
			StopAllCoroutines();
			LevelsController.LoadIntroSecretCave();
		}
	


		// METHODS
		private void InitVariables()
		{
			_animatorRaccoon = _objRaccoon.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorRaccoon);
	#endif
			#endregion
		
			_animatorGi = _objGi.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _animatorGi);
	#endif
			#endregion

			_animatorTorch = _objTorch.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR

			Assert.AreNotEqual(null, _animatorTorch);

	#endif
			#endregion
		}

		private void InitActionsList()
		{
			// Gi go to scene
			AddAction(GiGoToScene);
			AddAction(CameraCloseUpOnGi);
			AddAction(GiIdleAfterGoToScene);
			AddAction(GiJumpShort);
			AddAction(GiIdleAfterJumpShort);

			// Raccoon speech
			AddAction(RaccoonSpeechIntro);
			AddAction(RaccoonIdle);

			// Gi ye-ye!
			AddAction(GiYeYe);
			AddAction(GiIdleAfterJumpLong);

			// change floor
			AddAction(ChangeStoolsSortingLayer);
			AddAction(ChangeFloor);
		
			// Raccoon pull torch
			AddAction(RaccoonPullTorch);
			AddAction(TorchAnimation);
			AddAction(RaccoonIdleAfterPullTorch);
			AddAction(CameraLongRangePlan);
			AddAction(StandGoDown);
			AddAction(NextLevel);
		}

		// =========================================================
		// Gi go to scene
		private float GiGoToScene()
		{
			const float timeMove = 3f;
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needWalk);
			_objGi.DOMove(_giEnterPos.position, timeMove).SetEase(Ease.Linear);

			return 0.5f;
		}

		private float CameraCloseUpOnGi()
		{
			Vector3 endPos = new Vector3(1.6f, 1.13f, -10f);
			const float endCameraSize = 3.8f;
			const float durationTween = 3f;

			// tween camera position
			_cameraMain.transform.DOMove(endPos, durationTween).SetEase(Ease.Linear);

			// tween camera size
			DOTween.To( () => _cameraMain.orthographicSize, 
				x => _cameraMain.orthographicSize = x, endCameraSize, durationTween)
				.SetEase(Ease.Linear);

			return 2.5f;
		}

		private float GiIdleAfterGoToScene()
		{
			_objGi.DOKill();

			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needIdle);

			return 0.2f;
		}

		private float GiTurnRightAfterGoToScene()
		{
			_objGi.localScale = new Vector2(-_objGi.localScale.x, _objGi.localScale.y);

			return 0.2f;
		}

		private float GiJumpShort()		
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needJumpShort);

			return 0.5f;
		}

		private float GiIdleAfterJumpShort()
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needIdle);

			return 0.8f;
		}

		// =========================================================
		// Raccoon start speech
		private float RaccoonTurnLeftAfreGiGoToScene()
		{
			_objRaccoon.localScale = new Vector2(-_objRaccoon.localScale.x, _objRaccoon.localScale.y);

			return 0f;
		}

		private float RaccoonSpeechIntro()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.introTavern);

			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Intro.introTavernRaccoon);
			_soundManager.PlaySound(SoundsSpeech.Intro.introTavernRaccoon);

			return soundLenght;
		}

		private float RaccoonIdle()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.needIdle);

			return 0f;
		}

		// =========================================================
		// Gi ye-ye!
		private float GiYeYe()
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needJumpLong);

			float soundLenght = _soundManager.GetSoundLenght(SoundsSpeech.Intro.introTavernGiYeYe);
			_soundManager.PlaySound(SoundsSpeech.Intro.introTavernGiYeYe);

			return soundLenght;
		}

		private float GiIdleAfterJumpLong()
		{
			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needIdle);

			return 0.2f;
		}

		private float GiIdleAfterGoToPlatform()
		{
			_objGi.DOKill();

			_animationManager.SetParameterTrigger(_animatorGi, ParamTriggers.GiIntro.needIdle);

			return 0f;
		}

		private float GiTurnLeftAfterGoToPlatform()
		{
			_objGi.localScale = new Vector2(-_objGi.localScale.x, _objGi.localScale.y);

			return 0f;
		}

		// =========================================================
		// change flour
		private float ChangeStoolsSortingLayer()
		{
			for (int i = 0; i < _srStools.Length; i++)
			{
				_srStools[i].sortingLayerName = "INTRO/Stool";
			}

			return 0f;
		}

		private float ChangeFloor()
		{
			_objFloor.SetActive(true);

			return 0.5f;
		}
	
		// =========================================================
		// Raccoon pull torch
		private float RaccoonPullTorch()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.needPullTorch);

			_timerManager.StartTimer(
				_soundManager.PlaySound, SoundsIntroFX.Raccoon.raccoonPullTorch, 0.2f);

			return 0.2f;
		}

		private float TorchAnimation()
		{
			_animationManager.SetParameterTrigger(_animatorTorch, ParamTriggers.RaccoonIntro.needPullTorch);

			return 0.7f;
		}

		private float RaccoonIdleAfterPullTorch()
		{
			_animationManager.SetParameterTrigger(_animatorRaccoon, ParamTriggers.RaccoonIntro.needIdle);

			return 0f;
		}

		private float CameraLongRangePlan()
		{
			Vector3 endPos = new Vector3(0, 0, -10);
			const float endCameraSize = 5f;
			const float durationTween = 3f;

			// tween camera position
			_cameraMain.transform.DOMove(endPos, durationTween).SetEase(Ease.Linear);

			// tween camera size
			DOTween.To(() => _cameraMain.orthographicSize,
				x => _cameraMain.orthographicSize = x, endCameraSize, durationTween)
				.SetEase(Ease.Linear);

			return 0f;
		}

		private float StandGoDown()
		{
			_soundManager.PlaySound(SoundsIntroFX.Misc.standMovesDown);

			const float timeMove = 6f;
			_objStand.DOMove(_standEndPos.position, timeMove).SetEase(Ease.Linear);

			return timeMove * 0.7f;
		}

		private float NextLevel()
		{
			LevelsController.LoadIntroSecretCave();

			return 0f;
		}

		// =========================================================

		#region MISC

		private void AddAction(Func<float> action)
		{
			_actions.Add(action);
		}

		private void StartActionCicle()
		{
			StartCoroutine(ActionCicle());
		}

		private IEnumerator ActionCicle()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actions.Count)
				{
					break;
				}

				delay = _actions[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		private void PlayMusic()
		{
			const string musicBG = "GameBackground";
			_soundManager.PlayMusic(musicBG);
		}



	}
}