﻿
using Pirates_3.Common;
using Pirates_3.Common.Info;
using RateMePlugin;
using Zenject;

namespace Assets.Game.Scripts._Game.Menus.RateUsMenu
{
	public class RateUsMenu : AMonoMenu
	{
		// FIELDS
		// -------------------------------
		// dependences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			_timerManager.StartTimer(ShowRateUsPanel, 2f);
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}



		// UNITY
		private void OnEnable()
		{
			RateMeModule.OnDialogueShown += OnDialogClosed;
			CloseButton.OnClose += OnClosed;
		}

		private void OnDisable()
		{
			RateMeModule.OnDialogueShown -= OnDialogClosed;
			CloseButton.OnClose -= OnClosed;
		}



		// METHODS
		private void OnDialogClosed(bool boolValue)
		{
			if (!boolValue) {
				OnClosed(); }
		}

		private void OnClosed()
		{
			//
			if (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.VERTICAL_A)
			{
				LevelsController.LoadNextLevel();
			}

			//
			else if (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.VERTICAL_B)
			{
				LevelsController.LoadMainMenu();
			}
		}

		private void ShowRateUsPanel()
		{
			RateMePlugin.RateMeModule.Instance.ShowDialogue(true);
		}

		private void HideRateUsPaneL()
		{
			RateMePlugin.RateMeModule.Instance.ShowDialogue(false);
		}



	}
}