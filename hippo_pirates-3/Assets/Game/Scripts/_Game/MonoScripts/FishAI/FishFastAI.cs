﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishFastAI : ABaseAI
	{
		// FIELDS
		private FishFastMove _goMove;

		private int[] _tempHoleSections = new int[4];
		private int _countSectHole;

	

		// A_SCRIPTS
		protected override void InitAwake()
		{
			base.InitAwake();

			_goMove = gameObject.GetComponent<FishFastMove>();
			Assert.AreNotEqual(null, _goMove);
		}

		protected override void InitStart()
		{
			InitFishDepth();
			FindTarget();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		private void InitFishDepth()
		{
			if (_goMove.Get_CurrFishDepth() == Enumerators.FishDepths.TOP)
			{
				_currFishDepth = Enumerators.FishDepths.TOP;
			}
			else if (_goMove.Get_CurrFishDepth() == Enumerators.FishDepths.CENTER)
			{
				_currFishDepth = Enumerators.FishDepths.CENTER;
			}
			else if (_goMove.Get_CurrFishDepth() == Enumerators.FishDepths.BOTTOM)
			{
				_currFishDepth = Enumerators.FishDepths.BOTTOM;
			}
		}

		private void FindTarget()
		{
			int[][] sectionsHole =
			{
				new[] { 1, 0 },
				new[] { 3, 2 },
				new[] { 5, 4 }
			};

			if (_currFishDepth == Enumerators.FishDepths.TOP)
			{
				_tempHoleSections = sectionsHole[0];
			}
			else if (_currFishDepth == Enumerators.FishDepths.CENTER)
			{
				_tempHoleSections = sectionsHole[1];
			}
			else if (_currFishDepth == Enumerators.FishDepths.BOTTOM)
			{
				_tempHoleSections = sectionsHole[2];
			}

			int num = _tempHoleSections[_countSectHole];
			Transform sectTransf = Services.Get<IDamageManager>().Get_HoleList() [num];
			_targetX = sectTransf.position.x;
		}

		protected override void CheckReachTarget()
		{
			if (_transform.position.x < _targetX)
			{
				if (_currFishDepth == Enumerators.FishDepths.TOP)
				{
					CheckScratchSection(0);
				}
				else if (_currFishDepth == Enumerators.FishDepths.CENTER)
				{
					CheckHoleSection();
				}
				else if (_currFishDepth == Enumerators.FishDepths.BOTTOM)
				{
					CheckScratchSection(1);
				}
			}
		}

		private void CheckScratchSection(int sectNum)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(sectNum >= 0);
	#endif
			#endregion

			Transform scratchTransf = Services.Get<IDamageManager>().Get_ScratchList() [sectNum];

			var scratchDT = scratchTransf.GetComponent<ABaseDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, scratchDT);
	#endif
			#endregion

			// ===========================================================
			//
			if (scratchDT.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
			{
				int rnd = Random.Range(0, 2);
				if (rnd == 0)
				{
					Services.Get<IDamageManager>().SetDirectDamage(_objCollider, Enumerators.DamageTypes.SCRATCH, sectNum);
					_isHit = true;
				}
				else if (rnd == 1)
				{
					CheckHoleSection();
				}
			}

			else
			{
				CheckHoleSection();
			}
			// ===========================================================
		}

		private void CheckHoleSection()
		{
			int num = _tempHoleSections[_countSectHole];
			Transform sectTramsf = Services.Get<IDamageManager>().Get_HoleList() [num];

			// ===========================================================
			//
			var sectDT = sectTramsf.GetComponent<ABaseDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, sectDT);
	#endif
			#endregion

			if (sectDT.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
			{
				Services.Get<IDamageManager>().SetDirectDamage(_objCollider, Enumerators.DamageTypes.HOLE, num);
				_isHit = true;
			}

			else
			{
				if (_countSectHole < 1)
				{
					_countSectHole += 1;
					FindTarget();
				}
				else
				{
					_isHit = true;
				}
			}
			// ===========================================================
		}



	}
}