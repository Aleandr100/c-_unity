﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public abstract class ABaseAI : AMonoScript
	{
		// FIELDS
		// -------------------------------------------------
		[SerializeField] protected GameObject _objCollider;

		// -------------------------------------------------
		protected Transform _transform;

		// -------------------------------------------------
		protected float _targetX;
		protected int _countSections;
		protected Enumerators.FishDepths _currFishDepth;

		// -------------------------------------------------
		protected bool _isHit;

	

		// A_SCRIPTS
		protected override void InitAwake()
		{
			_transform = transform;
		}



		// UNITY
		protected void Update()
		{
			if (!_isHit) {
				CheckReachTarget(); }
		}



		// ABSTRACT
		protected abstract void CheckReachTarget();


	
	}
}