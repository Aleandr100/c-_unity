﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{ 
	public class FishSmallAI : ABaseAI
	{
		// FIELDS
		private FishSmallMove _goMove;
	
		private int[] _tempSections = new int[4];
		private Enumerators.Directions _currDir;



		// A_SERVICES
		protected override void InitAwake()
		{
			base.InitAwake();

			_goMove = gameObject.GetComponent<FishSmallMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _goMove);
	#endif
			#endregion
		}

		protected override void InitStart()
		{
			InitDirections();
			InitFishDepth();
			CheckFish();
			FindTarget();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnDisable()
		{
			HideCollider();
		}

	
	
		// METHODS
		private void InitDirections()
		{
			if (_goMove.Get_CurrentDirection() == Enumerators.Directions.LEFT)
			{
				_currDir = Enumerators.Directions.LEFT;
			}
			else if (_goMove.Get_CurrentDirection() == Enumerators.Directions.RIGHT)
			{
				_currDir = Enumerators.Directions.RIGHT;
			}
		}

		private void InitFishDepth()
		{
			if (_goMove.Get_CurrentFishDepth() == Enumerators.FishDepths.TOP)
			{
				_currFishDepth = Enumerators.FishDepths.TOP;
			}
			else if (_goMove.Get_CurrentFishDepth() == Enumerators.FishDepths.CENTER)
			{
				_currFishDepth = Enumerators.FishDepths.CENTER;
			}
			else if (_goMove.Get_CurrentFishDepth() == Enumerators.FishDepths.BOTTOM)
			{
				_currFishDepth = Enumerators.FishDepths.BOTTOM;
			}
		}

		private void CheckFish()
		{
			// ===========================================================
			//
			if (_currDir == Enumerators.Directions.LEFT)
			{
				int[][] sectionsLeft =
				{
					new[] { 3, 2, 1, 0 },
					new[] { 7, 6, 5, 4 },
					new[] { 11, 10, 9, 8 }
				};
			
				if (_currFishDepth == Enumerators.FishDepths.TOP)
				{
					_tempSections = sectionsLeft[0];
				}
				else if (_currFishDepth == Enumerators.FishDepths.CENTER)
				{
					_tempSections = sectionsLeft[1];
				}
				else if (_currFishDepth == Enumerators.FishDepths.BOTTOM)
				{
					_tempSections = sectionsLeft[2];
				}
			}

			// ===========================================================
			//
			else if (_currDir == Enumerators.Directions.RIGHT)
			{
				int[][] sectionsRight =
				{
					new[] { 0, 1, 2, 3 },
					new[] { 4, 5, 6, 7 },
					new[] { 8, 9, 10, 11 }
				};

				if (_currFishDepth == Enumerators.FishDepths.TOP)
				{
					_tempSections = sectionsRight[0];
				}
				else if (_currFishDepth == Enumerators.FishDepths.CENTER)
				{
					_tempSections = sectionsRight[1];
				}
				else if (_currFishDepth == Enumerators.FishDepths.BOTTOM)
				{
					_tempSections = sectionsRight[2];
				}
			}
			// ===========================================================
		}

		private void FindTarget()
		{
			int num = _tempSections[_countSections];
			Transform sectTransf = Services.Get<IDamageManager>().Get_HullTornList() [num];
			_targetX = sectTransf.position.x;
		}

		private void CheckSection()
		{
			int num = _tempSections[_countSections];
			Transform sectTramsf = Services.Get<IDamageManager>().Get_HullTornList() [num];

			// ===========================================================
			//
			var sectDmgTypes = sectTramsf.GetComponent<ABaseDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, sectDmgTypes);
	#endif
			#endregion

			if (sectDmgTypes.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
			{
				Services.Get<IDamageManager>().SetDirectDamage(_objCollider, Enumerators.DamageTypes.HULL_TORN, num);
				_isHit = true;
			}

			else
			{
				if (_countSections < 3)
				{
					_countSections += 1;
					FindTarget();
				}
				else
				{
					_isHit = true;
				}	
			}
			// ===========================================================
		}
	
		protected override void CheckReachTarget()
		{
			if (_currDir == Enumerators.Directions.LEFT)
			{
				if (_transform.position.x < _targetX)
				{
					CheckSection();
				}
			}

			else if (_currDir == Enumerators.Directions.RIGHT)
			{
				if (_transform.position.x > _targetX)
				{
					CheckSection();
				}
			}
		}

		private void HideCollider()
		{
			_objCollider.SetActive(false);
		}


	}
}