﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.Tutorial;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public abstract class ABaseDamageType : AMonoScript
	{
		// FIELDS
		// -------------------------------------------------------
		protected Transform _transform;
		protected GameObject _goImage;
		protected GameObject _goCollider;
		protected ParticleSystem _goPartSys;

		// -------------------------------------------------------
		protected float _timerRepair;
		protected float _tempTimerRepair;

		protected float _normalRepair;
		protected float _fastRepair;

		// -------------------------------------------------------
		protected bool _isNeedRepair;

		// -------------------------------------------------------
		public enum DamageCond
		{
			OFF,
			ON
		}
		protected DamageCond _currDmgCond;



		// A_SCRIPT
		protected override void InitAwake()
		{
			InitVariablesProtected();
		}
	


		// UNITY
		protected void Update()
		{
			if (_isNeedRepair) {
				CheckRepair(); }
		}




		// INTERFACES
		#region GETTERS/SETTERS
		public DamageCond Get_CurrentDamageCondition()
		{
			return _currDmgCond;
		}
		#endregion



		// ABSTRACT
		protected abstract void CheckRepair();

		protected abstract void HideDamage();



		// PROTECTED
		protected void CheckRepairRate(bool isBonusRepairRate)
		{
			_timerRepair = (isBonusRepairRate) ? (_fastRepair) : (_normalRepair);
		}

		protected void StopRepair()
		{
			_isNeedRepair = false;
			_tempTimerRepair = 0;

			if ( IsTutorial() ) {
				Services.Get<IToolsManagerTutorial>().RepairCompleted(); }
			else {
				Services.Get<IToolsManager>().RepairCompleted(); }
		}

		protected void DamageRapaired(Enumerators.DamageTypes dmgType)
		{
			Services.Get<ICriticalDamageManager>().RepairCritDamage(dmgType);
			Services.Get<ISubmarineHpManager>().ReduceDmgPerSec(dmgType);
			Services.Get<IToolsButtonsVisualEffectsManager>().CheckHideButtonVisualEffects(dmgType);
		}
	
		protected void CheckTutorialProceedOnHaveDamage()
		{
			// ----------------------------------------------------------------------------
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.HAMMER_VS_DAMAGE)
			{
				Services.Get<IHammerVsDamage>().SubmarineHaveDamage(transform);
			}
			// ----------------------------------------------------------------------------
			else if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.MAGNET_VS_DAMAGE)
			{
				Services.Get<IMagnetVsDamage>().SubmarineHaveDamage(transform);
			}
			// ----------------------------------------------------------------------------
			else if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.WELDING_VS_DAMAGE)
			{
				Services.Get<IWeldingVsDamage>().SubmarineHaveDamage(transform);
			}
			// ----------------------------------------------------------------------------
			else if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.SCISSORS_VS_DAMAGE)
			{
				Services.Get<IScissorsVsDamage>().SubmarineHaveDamage(transform);
			}
			// ----------------------------------------------------------------------------
		}


	
		// PRIVATE
		private void InitVariablesProtected()
		{
			_transform = transform;
			_goImage = _transform.GetChild(0).gameObject;
			_goCollider = _transform.GetChild(1).gameObject;

			_goPartSys = _transform.GetChild(2).GetChild(0).GetComponent<ParticleSystem>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _goPartSys);
	#endif
			#endregion
		}

		private bool IsTutorial()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}



	}
}