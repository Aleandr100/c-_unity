﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class ScrewNet : ABaseDamageType, IDamageType
	{
		// FIELDS
		// -------------------------------------
		//
		[SerializeField] private SpriteRenderer _srScrew;
		[SerializeField] private Sprite _spriteScrewOriginal;
		[SerializeField] private Sprite _spriteScrewBreak;

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								ITimerManager timerManager)
		{
			_soundManager = soundManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			base.InitAwake();

			_normalRepair = 2f;						// 5f
			_fastRepair = _normalRepair / 2f;
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void ShowDamage()
		{
			if (_currDmgCond == DamageCond.OFF)
			{
				_srScrew.sprite = _spriteScrewBreak;
				_goCollider.SetActive(true);
				_goPartSys.Play();

				_currDmgCond = DamageCond.ON;

				_soundManager.PlaySound(SoundsGameplayFX.DamageTypes.screwBreak);

				// tutorial
				CheckTutorialProceedOnHaveDamage();
			}
		}

		public void ShowDamage(int num)
		{
		}

		public bool IsRightTool(Enumerators.ToolsTypes toolType)
		{
			return (toolType == Enumerators.ToolsTypes.SCISSORS);
		}

		public bool StartRepair(Enumerators.ToolsTypes toolType, bool isBonusRepairRate)
		{
			if (IsRightTool(toolType))
			{
				_isNeedRepair = true;
				_tempTimerRepair = 0;

				CheckRepairRate(isBonusRepairRate);
				Services.Get<IDiverVisualEffects>().ShowRepairBar(_timerRepair);

				_timerManager.StartTimer(
					_soundManager.PlaySoundRepeating, SoundsGameplayFX.Tools.scissors, 1f);

				return true;
			}

			return false;
		}
	


		// METHODS
		protected override void CheckRepair()
		{
			_tempTimerRepair += Time.deltaTime;
		
			if (_tempTimerRepair > _timerRepair)
			{
				HideDamage();
				StopRepair();
				DamageRapaired(Enumerators.DamageTypes.SCREW_NET);

				PostEffects();
			}
		}

		private void PostEffects()
		{
			_timerManager.StartTimer(
				_soundManager.StopSound, SoundsGameplayFX.Tools.scissors, 0.5f);
		}

		protected override void HideDamage()
		{
			_srScrew.sprite = _spriteScrewOriginal;
			_goCollider.SetActive(false);
			_goPartSys.Stop();

			Services.Get<ISubmarineVisualEffects>().StartScrewRotate();

			_currDmgCond = DamageCond.OFF;
		}

	




	}
}