﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class HullTorn : ABaseDamageType, IDamageType
	{
		// FIELDS
		// -------------------------------
		// dependences
		private ISoundManager _soundManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								ITimerManager timerManager)
		{
			_soundManager = soundManager;
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
			base.InitAwake();

			_normalRepair = 2f;                 // 2f
			_fastRepair = _normalRepair / 2f;
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACE
		public void ShowDamage()
		{
			if (_currDmgCond == DamageCond.OFF)
			{
				_goImage.SetActive(true);
				_goCollider.SetActive(true);
				_goPartSys.Play();

				_currDmgCond = DamageCond.ON;

				_soundManager.PlaySound(SoundsGameplayFX.DamageTypes.hitSmall);
			
				// tutorial
				CheckTutorialProceedOnHaveDamage();
			}
		}

		public void ShowDamage(int num)
		{
		}

		public bool IsRightTool(Enumerators.ToolsTypes toolType)
		{
			return (toolType == Enumerators.ToolsTypes.HAMMER);
		}

		public bool StartRepair(Enumerators.ToolsTypes toolType, bool isBonusRepairRate)
		{
			if ( IsRightTool(toolType) )
			{
				_isNeedRepair = true;
				_tempTimerRepair = 0;
				CheckRepairRate(isBonusRepairRate);

				Services.Get<IDiverVisualEffects>().ShowRepairBar(_timerRepair);

				_timerManager.StartTimer(
					_soundManager.PlaySoundRepeating, SoundsGameplayFX.Tools.hammer, 1f);

				return true;
			}

			return false;
		}

	

		// METHODS
		protected override void CheckRepair()
		{
			_tempTimerRepair += Time.deltaTime;

			if (_tempTimerRepair > _timerRepair)
			{
				HideDamage();
				StopRepair();
				DamageRapaired(Enumerators.DamageTypes.HULL_TORN);

				PostEffects();
			}
		}

		protected override void HideDamage()
		{
			_goImage.SetActive(false);
			_goCollider.SetActive(false);
			_goPartSys.Stop();

			_currDmgCond = DamageCond.OFF;
		}

		private void PostEffects()
		{
			_timerManager.StartTimer(
				_soundManager.StopSound, SoundsGameplayFX.Tools.hammer, 0.5f);
		}



	}
}