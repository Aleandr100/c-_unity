﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MovementVertAMono : ABaseBottomMovement
	{



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_resetPos = -6.5f;
			_width = 7.9f;
			_speed = 1f;
			_compareValue = 999999f;
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		protected override void BottomMovement()
		{
			for (int i = 0; i < _bottoms.Length; i++)
			{
				_bottoms[i].Translate(Vector2.up * Time.deltaTime * (_speed / _speedMultiplier) );

				if (_bottoms[i].localPosition.y > _resetPos)
				{
					float lowestPosY = GetExtremalPosition(_bottoms);
					SetPosition(_bottoms[i], new Vector2(0f, lowestPosY - _width));
				}
			}
		}

		protected override float GetExtremalPosition(Transform[] myArray)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(myArray.Length > 0);
	#endif
			#endregion

			//
			float tempPosY = _compareValue;

			for (int i = 0; i < myArray.Length; i++)
			{
				if (tempPosY > myArray[i].localPosition.y)
				{
					tempPosY = myArray[i].localPosition.y;
				}
			}

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(tempPosY < _compareValue);
	#endif
			#endregion

			return tempPosY;
		}
		


	}
}