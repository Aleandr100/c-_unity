﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public abstract class ABaseBottomMovement : AMonoScript
	{
		// FIELDS
		// ----------------------------------------
		[SerializeField] protected Transform[] _bottoms;

		// ----------------------------------------
		protected float _resetPos;
		protected float _width;
		protected float _speed;
		protected float _compareValue;

		// ----------------------------------------
		protected float _speedMultiplier = 1f;
		protected float _speedMultiplierNormal = 1f;
		protected float _speedMultiplierSubmarineDamaged = 2f;



		// UNITY
		protected void Update()
		{
			BottomMovement();
		}



		// INTERFACES
		public void SetSpeedNormal()
		{
			_speedMultiplier = _speedMultiplierNormal;
		}

		public void SetSpeedSlow()
		{
			_speedMultiplier = _speedMultiplierSubmarineDamaged;
		}



		// METHODS
		protected abstract void BottomMovement();

		protected abstract float GetExtremalPosition(Transform[] myArray);

		protected void SetPosition(Transform goTransf, Vector2 myPos)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
	#endif
			#endregion

			goTransf.localPosition = myPos;
		}



	}
}