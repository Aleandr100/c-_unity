﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MovementHorB : ABaseBottomMovement
	{



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_resetPos = -12f;
			_width = 14.9f;
			_speed = 1f;
			_compareValue = -999999f;
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		protected override void BottomMovement()
		{
			for (int i = 0; i < _bottoms.Length; i++)
			{
				_bottoms[i].Translate(Vector2.left * Time.deltaTime * (_speed / _speedMultiplier) );

				if (_bottoms[i].localPosition.x < _resetPos)
				{
					float biggestPosX = GetExtremalPosition(_bottoms);
					SetPosition(_bottoms[i], new Vector2(biggestPosX + _width, 0f));
				}
			}
		}

		protected override float GetExtremalPosition(Transform[] myArray)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(myArray.Length > 0);
	#endif
			#endregion

			//
			float tempPosX = _compareValue;

			for (int i = 0; i < myArray.Length; i++)
			{
				if (tempPosX < myArray[i].localPosition.x)
				{
					tempPosX = myArray[i].localPosition.x;
				}
			}

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(tempPosX > _compareValue);
	#endif
			#endregion

			return tempPosX;
		}
		


	}
}