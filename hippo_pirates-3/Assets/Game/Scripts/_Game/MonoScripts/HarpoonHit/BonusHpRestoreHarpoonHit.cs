﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class BonusHpRestoreHarpoonHit : AMonoScript, IHarpoonHit
	{
		// FIELDS
		// ---------------------------------------
		//
		[SerializeField] private GameObject _objImages;
		[SerializeField] private ParticleSystem _partSysBubbles;



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACE
		public void HitActions()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Bonuses.bonusHit);

			ActivateBubbles();
			Services.Get<ITimerManagerMono>().StartTimer(HideBonusImage, 0.1f);
			Services.Get<ITimerManagerMono>().StartTimer(Hide, 2f);

			Services.Get<ISubmarineHpManager>().RestoreHp();
		}



		// METHODS
		private void ActivateBubbles()
		{
			_partSysBubbles.Play();
		}

		private void HideBonusImage()
		{
			_objImages.SetActive(false);
		}


	
	}
}