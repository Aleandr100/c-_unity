﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MineHarpoonHit : AMonoScript, IHarpoonHit
	{
	


		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACE
		public void HitActions()
		{
			var feedback = GetComponent<IFeedback>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, feedback);
	#endif
			#endregion

			feedback.ApplyFeedback();
		}
	
	

	}
}