﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class ChainToolUse : AMonoScript, IToolUse
	{
	


		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public bool IsRightTool(Enumerators.ToolsTypes toolType)
		{
			return (toolType == Enumerators.ToolsTypes.SCISSORS);
		}

		public bool StartHandleThreat(Enumerators.ToolsTypes toolType, Transform coll)
		{
			if (IsRightTool(toolType))
			{
				ScissorsHitChain(coll);
				return true;
			}

			return false;
		}



		// METHODS
		private void ScissorsHitChain(Transform coll)
		{
			Services.Get<IChainPool>().CutChain(transform, coll);
			Hide();
		}
	


	}
}