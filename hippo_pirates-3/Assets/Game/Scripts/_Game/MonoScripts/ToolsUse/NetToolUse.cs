﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class NetToolUse : AMonoScript, IToolUse
	{


		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public bool IsRightTool(Enumerators.ToolsTypes toolType)
		{
			return (toolType == Enumerators.ToolsTypes.SCISSORS);
		}

		public bool StartHandleThreat(Enumerators.ToolsTypes toolType, Transform coll)
		{
			if (IsRightTool(toolType))
			{
				Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Tools.scissors);

				Services.Get<ITimerManagerMono>().StartTimer(
					Services.Get<ISoundManagerMono>().StopSound, SoundsGameplayFX.Tools.scissors, 0.5f);

				ScissorsHitNet();
				return true;
			}

			return false;
		}



		// METHODS
		private void ScissorsHitNet()
		{
			Hide();
		}

	

	}
}