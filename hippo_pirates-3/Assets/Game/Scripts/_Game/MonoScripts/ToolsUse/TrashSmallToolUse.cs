﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TrashSmallToolUse : AMonoScript, IToolUse
	{
		// FIELDS
		// -------------------------------------
		//
		private Rigidbody2D _rigid;



		// A_SERVICES
		protected override void InitAwake()
		{
			_rigid = GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _rigid);
	#endif
			#endregion
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public bool IsRightTool(Enumerators.ToolsTypes toolType)
		{
			return (toolType == Enumerators.ToolsTypes.HAMMER);
		}

		public bool StartHandleThreat(Enumerators.ToolsTypes toolType, Transform coll)
		{
			if (IsRightTool(toolType))
			{
				Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Tools.hammer);

				ChangeDirectionOnHit();
			
				return true;
			}

			return false;
		}




		// METHODS
		private void ChangeDirectionOnHit()
		{
			// bug fixed - multiple click on object !!!
			_rigid.velocity = Vector2.zero;

			_rigid.AddForce(new Vector2(1f, -0.15f), ForceMode2D.Impulse);     // Vector2(1f, -0.15f)
		}



	}
}