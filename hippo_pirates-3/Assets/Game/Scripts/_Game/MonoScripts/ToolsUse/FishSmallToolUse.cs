﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishSmallToolUse : AMonoScript, IToolUse
	{
		// FIELDS
		// ---------------------------------------
		//
		private Rigidbody2D _rigid;


	

		// A_SERVICES
		protected override void InitAwake()
		{
			_rigid = GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _rigid);
	#endif
			#endregion
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public bool IsRightTool(Enumerators.ToolsTypes toolType)
		{
			return (toolType == Enumerators.ToolsTypes.HAMMER);
		}

		public bool StartHandleThreat(Enumerators.ToolsTypes toolType, Transform coll)
		{
			if (IsRightTool(toolType))
			{
				Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Tools.hammer);

				HammerHitFishSmall();
			
				return true;
			}

			return false;
		}



		// METHODS
		private void HammerHitFishSmall()
		{
			var goMove = GetComponent<FishSmallMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			FishSmallGoDown();
			DisableFishSmallMove(goMove);
			DisableFishSmallAI();
			SetFishSmallFishAnimations();

			Services.Get<ITimerManagerMono>().StartTimer(Hide, 3f);
		}

		private void FishSmallGoDown()
		{
			_rigid.isKinematic = false;
			_rigid.gravityScale = 0.2f;
		}

		private void DisableFishSmallMove(FishSmallMove goMove)
		{
			goMove.enabled = false;
		}

		private void DisableFishSmallAI()
		{
			var goAI = GetComponent<FishSmallAI>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goAI);
	#endif
			#endregion

			goAI.enabled = false;
		}

		private void SetFishSmallFishAnimations()
		{
			Animator goAnimator = transform.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goAnimator);
	#endif
			#endregion

			Services.Get<IAnimationManagerMono>().SetParameterBool(goAnimator, ParamBools.HarpoonableObject.isHit, true);

			// show stars
			var stars = GetComponent<FishSmallStars>();
			stars.ShowStars();
		}



	}
}