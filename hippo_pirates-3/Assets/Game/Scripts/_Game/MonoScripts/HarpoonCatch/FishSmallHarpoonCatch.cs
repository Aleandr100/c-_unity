﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishSmallHarpoonCatch : AMonoScript, IHarpoonCatch
	{
		// FIELDS
		// ----------------------------------------
		//
		[Header("SPRITES")]
		[SerializeField] private SpriteRenderer[] _spritesRenderers;
		[SerializeField] private Sprite[] _spritesTraped;

		// ----------------------------------------
		//
		[Header("HARPOON PLACE")]
		[SerializeField] private Transform _harpoonBindPlace;

		// ----------------------------------------
		//
		private float _delayMoveUp = 0.5f;




		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void CatchActions()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Shooting.itemsInNet);

			ActivateHideCheck();
			ShowTraped();
			ActivateTrapedAnimation();
			MoveUp();
			DeactivateMove();
			DeactivateAI();
		}

		public Transform Get_HarpoonBindPlace()
		{
			return _harpoonBindPlace;
		}



		// METHODS
		private void ActivateHideCheck()
		{
			var hideCheck = GetComponent<ITrapedSupport>();
			hideCheck.FinalTrapedActions();
		}

		private void ShowTraped()
		{
			for (int i = 0; i < _spritesRenderers.Length; i++)
			{
				_spritesRenderers[i].sprite = _spritesTraped[i];
			}
		}

		private void ActivateTrapedAnimation()
		{
			var goAnimator = transform.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goAnimator);
	#endif
			#endregion

			Services.Get<IAnimationManagerMono>().SetParameterBool(goAnimator, ParamBools.HarpoonableObject.isTraped, true);
		}

		private void MoveUp()
		{
			var collMove = GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, collMove);
	#endif
			#endregion

			Services.Get<ITimerManagerMono>().StartTimer(collMove.MoveUp, _delayMoveUp);
		}

		private void DeactivateMove()
		{
			var goMove = GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			Services.Get<ITimerManagerMono>().StartTimer(goMove.StopMovement, _delayMoveUp);
		}

		private void DeactivateAI()
		{
			var goAI = GetComponent<FishSmallAI>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goAI);
	#endif
			#endregion

			goAI.enabled = false;
		}



	}
}