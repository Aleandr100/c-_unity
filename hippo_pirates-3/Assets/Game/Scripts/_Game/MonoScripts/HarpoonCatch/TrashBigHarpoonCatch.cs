﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TrashBigHarpoonCatch : AMonoScript, IHarpoonCatch
	{
		// FIELDS
		// -----------------------------------------------
		//
		[Header("SPRITES")]
		[SerializeField] private SpriteRenderer _spriteRenderer;
		[SerializeField] private Sprite _spriteTraped;

		// -----------------------------------------------
		//
		[Header("HARPOON PLACE")]
		[SerializeField] private Transform _harpoonBindPlace;

		// -----------------------------------------------
		//
		private float _delayMoveUp = 0.5f;

		// -----------------------------------------------
		//
		#region INNER CLASS: HITS COUNT

		private class Hits
		{
			private int _count;
			private readonly int _maxCount = 3;
			private bool _isMax;

			//
			public int Get_Count()
			{
				return _count;
			}
		
			public void IncreaseCount()
			{
				_count ++;

				if (_count > _maxCount)
				{
					_count = _maxCount;
					_isMax = true;
				}
			}
		
			public bool IsMax()
			{
				return (_count >= _maxCount);
			}
		}

		private Hits _hits = new Hits();

		#endregion



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void CatchActions()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Shooting.itemsInNet);

			_hits.IncreaseCount();

			if ( !_hits.IsMax() )
			{
				ShowTraped();
				ShowHpOnHit();
			}
			else
			{
				ActivateHideCheck();
				HideAvatar();
				MoveUp();
				DeactivateMove();
			}
		}

		public Transform Get_HarpoonBindPlace()
		{
			return _harpoonBindPlace;
		}



		// METHODS
		private void ActivateHideCheck()
		{
			var hideCheck = GetComponent<ITrapedSupport>();
			hideCheck.FinalTrapedActions();
		}

		private void ShowTraped()
		{
			if (_hits.Get_Count() == 1)
			{
				_spriteRenderer.sprite = _spriteTraped;
			}
		}

		private void ShowHpOnHit()
		{
			var avatar = GetComponent<IAvatarSupport>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, avatar);
	#endif
			#endregion

			Services.Get<IAvatarsManager>().ShowHpOnHit( avatar.Get_AvatarLink(), _hits.Get_Count() );
		}

		private void MoveUp()
		{
			var collMove = GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, collMove);
	#endif
			#endregion

			Services.Get<ITimerManagerMono>().StartTimer(collMove.MoveUp, _delayMoveUp);
		}

		private void DeactivateMove()
		{
			var goMove = GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			Services.Get<ITimerManagerMono>().StartTimer(goMove.StopMovement, _delayMoveUp);
		}

		private void HideAvatar()
		{
			var avatar = GetComponent<IAvatarSupport>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, avatar);
	#endif
			#endregion

			avatar.HideAvatar();
		}



	}
}