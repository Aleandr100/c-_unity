﻿using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TreasureHarpoonCatch : AMonoScript, IHarpoonCatch
	{
		// FIELDS
		// -----------------------------------------------
		[Header("SPRITES")]
		[SerializeField] private SpriteRenderer _spriteRenderer;
		[SerializeField] private Sprite _spriteTraped;

		// -----------------------------------------------
		[Header("HARPOON PLACE")]
		[SerializeField] private Transform _harpoonBindPlace;

		// -----------------------------------------------
		private float _delayMoveUp =  0.5f;



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}




		// INTERFACES
		public void CatchActions()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Misc.hitTreasure);

			ActivateHideCheck();
			ShowTraped();
			MoveUp();
			DeactivateMove();
		}

		public Transform Get_HarpoonBindPlace()
		{
			return _harpoonBindPlace;
		}



		// METHODS
		private void ActivateHideCheck()
		{
			var hideCheck = GetComponent<ITrapedSupport>();
			hideCheck.FinalTrapedActions();
		}

		private void ShowTraped()
		{
			_spriteRenderer.sprite = _spriteTraped;
		}

		private void MoveUp()
		{
			var collMove = GetComponent<ABaseMove>();
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, collMove);
#endif
			#endregion

			Services.Get<ITimerManagerMono>().StartTimer(collMove.MoveUp, _delayMoveUp);
		}

		private void DeactivateMove()
		{
			var goMove = GetComponent<ABaseMove>();
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
#endif
			#endregion

			Services.Get<ITimerManagerMono>().StartTimer(goMove.StopMovement, _delayMoveUp);
		}



	}
}