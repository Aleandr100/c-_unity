﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class HarpoonCollider : AMonoScript
	{
		// FIELDS
		// ---------------------------------------
		//
		[SerializeField] private Transform _parent;
		[SerializeField] private GameObject _harpImage;
		[SerializeField] private GameObject[] _objBalloons;

		// ---------------------------------------
		//
		private Harpoon _harpoonScript;



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_harpoonScript = _parent.GetComponent<Harpoon>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _harpoonScript);
	#endif
			#endregion
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnTriggerEnter2D(Collider2D coll)
		{
			GameObject goColl = coll.gameObject;

			TryHitObject(goColl);
			TryCatchObject(goColl);
		}



		// METHODS

		#region ACTIONS

		private void TryHitObject(GameObject coll)
		{
			Transform collParent = GetCollidedObjectParent(coll);
			var harpoonHit = collParent.GetComponent<IHarpoonHit>();

			if (harpoonHit != null)
			{
				harpoonHit.HitActions();
				DeactivateHarpoonColl();
				Hide();
				HideHarpoon();
			}
		}

		private void TryCatchObject(GameObject coll)
		{
			Transform collParent = GetCollidedObjectParent(coll);
			var harpoonCatch = collParent.GetComponent<IHarpoonCatch>();

			if (harpoonCatch != null)
			{
				harpoonCatch.CatchActions();
				ShowBalloon();
				BindToCollidedObject(coll);
				DeactivateHarpoonColl();
				Hide();
			}
		}

		#endregion
	
		#region SHOW_BALLOONS

		private void ShowBalloon()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Shooting.balloonBlowing);


			_parent.localRotation = Quaternion.Euler(0, 0, 0);

			int rnd = GetRandomBalloonColor();
			_objBalloons[rnd].SetActive(true);

			_harpImage.SetActive(false);
		}

		private int GetRandomBalloonColor()
		{
			const int variants = 4;
			return Random.Range(0, variants);
		}

		#endregion

		#region BIND_TO_OBJECT

		private void BindToCollidedObject(GameObject coll)
		{
			var bindPosition = GetBindPosition(coll);
			_parent.SetParent(bindPosition, true);

			SetPosition(coll);
			SetScale();
		}

		private Transform GetBindPosition(GameObject coll)
		{
			Transform collParent = GetCollidedObjectParent(coll);

			var harpoonCatch = collParent.GetComponent<IHarpoonCatch>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, harpoonCatch);
	#endif
			#endregion

			return harpoonCatch.Get_HarpoonBindPlace();
		}

		private void SetPosition(GameObject coll)
		{
			if (coll.CompareTag(Tags.FishBig) ||
				coll.CompareTag(Tags.TrashBig)) 
			{
				_parent.localPosition = GetRandomPos();
			}
			else
			{
				_parent.localPosition = Vector2.zero;
			}
		}

		private void SetScale()
		{
			_parent.localScale = Vector2.one;
		}
	
		private Vector2 GetRandomPos()
		{
			return new Vector2( GetX(), GetY() );
		}

		private float GetX()
		{
			const float deltaMinX = -0.5f;
			const float deltaMaxX = 0.5f;

			return Random.Range(deltaMinX, deltaMaxX);
		}

		private float GetY()
		{
			const float deltaMinY = -0.5f;
			const float deltaMaxY = 0.5f;

			return Random.Range(deltaMinY, deltaMaxY);
		}

		#endregion

		#region MISC

		private Transform GetCollidedObjectParent(GameObject coll)
		{
			var colliderScript = coll.GetComponent<ABaseCollider>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, colliderScript);
	#endif
			#endregion

			return colliderScript.Get_Parent();
		}

		private void DeactivateHarpoonColl()
		{
			var rigid = _parent.GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rigid);
	#endif
			#endregion

			rigid.isKinematic = true;
		}

		private void HideHarpoon()
		{
			_parent.gameObject.SetActive(false);
		}

		#endregion
	


	}
}