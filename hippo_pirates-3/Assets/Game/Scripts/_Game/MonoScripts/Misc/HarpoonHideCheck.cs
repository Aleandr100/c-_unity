﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class HarpoonHideCheck : AMonoScript
	{
		// FIELDS
		[SerializeField] private GameObject _parent;

		private Harpoon _harpoonScript;



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_harpoonScript = _parent.GetComponent<Harpoon>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _harpoonScript);
	#endif
			#endregion
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// UNITY
		private void OnBecameInvisible()
		{
			// bug fixed - with twice recursively SetActive(false) parent object !!!
			if (gameObject.activeSelf)
			{
				_harpoonScript.Hide();
			}
		}

	

	}
}