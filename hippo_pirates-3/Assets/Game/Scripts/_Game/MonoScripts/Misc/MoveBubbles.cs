﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MoveBubbles : AMonoScript
	{
		// FIELDS
		// -------------------------------------------------
		private Transform _transform;

		private readonly SpriteRenderer[] _sr = new SpriteRenderer[3];

		// -------------------------------------------------
		private float _tempColorAlpha;
		private readonly float _maxColorAlpha = 1f;         // max alpha is 1 !!!

		// -------------------------------------------------
		private readonly float _bottomPosY = -15f;
		private readonly float _fadePosY = 1f;

		private readonly float _speed = 0.7f;
		private readonly float _fadeSpeed = 0.1f;			// 0.01f - for FixedUpdate / 0.1f - for SlowUpdate



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_transform = transform;
		}

		protected override void InitStart()
		{
			InitSpriteRenderers();

			//
			StartSlowUpdate();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void SlowUpdate()
		{
			CheckBuubleFade();
		}



		// UNITY
		private void Update()
		{
			Movement();
		}

	
	
		// METHODS
		private void InitSpriteRenderers()
		{
			_sr[0] = _transform.GetChild(0).GetComponent<SpriteRenderer>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _sr[0]);
	#endif
			#endregion

			_sr[1] = _transform.GetChild(1).GetComponent<SpriteRenderer>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _sr[1]);
	#endif
			#endregion

			_sr[2] = _transform.GetChild(2).GetComponent<SpriteRenderer>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _sr[2]);
	#endif
			#endregion
		}

		private void Movement()
		{
			_transform.Translate(Vector2.up * Time.deltaTime * _speed);
		}

		private void CheckBuubleFade()
		{
			if (_transform.localPosition.y > _fadePosY)
			{
				// -----------------------------------------------------------
				for (int i = 0; i < _transform.childCount; i++)
				{
					if (_sr[i].color.a > 0)
					{
						_tempColorAlpha = _sr[i].color.a - _fadeSpeed;
						ChangeBubbleAlpha(_sr[i], _tempColorAlpha);
					}
					else
					{
						ResetBubble();
					}
				}
				// -----------------------------------------------------------
			}
		}

		private void ResetBubble()
		{
			_transform.localPosition = new Vector2(_transform.localPosition.x, _bottomPosY);
		
			for (int i = 0; i < _transform.childCount; i++)
			{
				ChangeBubbleAlpha(_sr[i], _maxColorAlpha);		
			}
		}

		private void ChangeBubbleAlpha(SpriteRenderer sr, float myValue)
		{
			Color myColor = sr.color;
			myColor.a = myValue;
			sr.color = myColor;
		}



	}
}