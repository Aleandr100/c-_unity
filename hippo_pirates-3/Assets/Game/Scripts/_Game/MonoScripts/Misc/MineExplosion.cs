﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MineExplosion : AMonoScript
	{
		// FIELDS
		// -----------------------------------------------------
		//
		[Header("ANIMATIONS")]
		[SerializeField] private Animator _animatorMsineExplosion;
	
		// -----------------------------------------------------
		//
		[Header("PARTICLES")]
		[SerializeField] private ParticleSystem _psBlow;
		[SerializeField] private ParticleSystem _psBubblesBigSlow;
		[SerializeField] private ParticleSystem _psBubblesFast;

		// -----------------------------------------------------
		//
		private readonly ParticleSystem[] _particleSystems = new ParticleSystem[3];




		// A_SCRIPTS
		protected override void InitAwake()
		{
			InitParticleSystems();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES 
		public void ShowExplosion()
		{
			ShowAnimation();
			ShowParticles();
		}



		// METHODS
		private void InitParticleSystems()
		{
			_particleSystems[0] = _psBlow;
			_particleSystems[1] = _psBubblesBigSlow;
			_particleSystems[2] = _psBubblesFast;
		}

		private void ShowAnimation()
		{
			Services.Get<IAnimationManagerMono>().SetParameterTrigger(_animatorMsineExplosion, ParamTriggers.Mine.needExplosion);
		}

		private void ShowParticles()
		{
			for (int i = 0; i < _particleSystems.Length; i++)
			{
				_particleSystems[i].Play();
			}
		}



	}
}