﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class Avatar : AMonoScript, IAvatarSupport
	{
		// FIELDS
		protected GameObject _avatarLink;

	

		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			// no need to do auto-assert here !
		}

	

		// INTERFACES
		public GameObject Get_AvatarLink()
		{
			return _avatarLink;
		}

		public void Set_AvatarLink(GameObject go)
		{
			_avatarLink = go;
		}

		public void HideAvatar()
		{
			HideAvatarActions();
		}



		// PROTECTED
		protected void HideAvatarActions()
		{
			// pre - condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, Get_AvatarLink());
	#endif
			#endregion

			Services.Get<IAvatarsManager>().DeactivateThreat( Get_AvatarLink() );
		}




	}
}