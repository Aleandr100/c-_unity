﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TrashBigFeedback : AMonoScript, IFeedback
	{


		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void ApplyFeedback()
		{
			ChangeMoveVector();
			HideAvatar();
		}



		// METHODS
		private void ChangeMoveVector()
		{
			var goMove = GetComponent<TrashBigMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion
			goMove.RecalculateMove( new Vector2(-0.5f, -1f), 1f);
		}

		private void HideAvatar()
		{
			var goAvatar = GetComponent<IAvatarSupport>();
			Assert.AreNotEqual(null, goAvatar);

			goAvatar.HideAvatar();
		}



	}
}