﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class RockFeedback : AMonoScript, IFeedback
	{
		// FIELDS
		// -----------------------------------------------------
		[Header("PARTICLES")]
		[SerializeField] private ParticleSystem _partSysRockDust;
		[SerializeField] private ParticleSystem _partSysRockDebris;

		// -----------------------------------------------------
		[Header("MISC")]
		[SerializeField] private GameObject _objImageRock;



		// A_SCRIPT
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void ApplyFeedback()
		{
			HideRockImage();
			PlayParticleSystemRockDust();
			PlayParticleSystemRockDebris();
			Services.Get<ITimerManagerMono>().StartTimer(Hide, 1.5f);
		}
	


		// METHODS
		private void PlayParticleSystemRockDust()
		{
			_partSysRockDust.Play();
		}

		private void PlayParticleSystemRockDebris()
		{
			_partSysRockDebris.Play();
		}

		private void HideRockImage()
		{
			_objImageRock.SetActive(false);
		}



	}
}