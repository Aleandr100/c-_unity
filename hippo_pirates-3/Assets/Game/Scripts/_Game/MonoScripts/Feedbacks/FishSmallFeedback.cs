﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishSmallFeedback : AMonoScript, IFeedback
	{





		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void ApplyFeedback()
		{
			var goMove = GetComponent<FishSmallMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.RecalculateMove(new Vector2(-0.5f, -1f), 2f);
			ActivateFishSmallWasHitAnimation();
		}



		// METHODS
		private void ActivateFishSmallWasHitAnimation()
		{
			// fish small hitted
			Animator goAnimator = transform.GetChild(0).GetComponent<Animator>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goAnimator);
	#endif
			#endregion

			Services.Get<IAnimationManagerMono>().SetParameterBool(goAnimator, ParamBools.HarpoonableObject.isHit, true);

			// fish small stars
			var stars = GetComponent<FishSmallStars>();
			stars.ShowStars();
		}



	}
}