﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MineFeedback : AMonoScript, IFeedback
	{
		// FIELDS
		// -------------------------------------
		//
		[SerializeField] private GameObject _mineCollider;



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void ApplyFeedback()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Misc.mineExplosion);

			DeactivateCollider();
			ActivateExplosionVisualEffect();
			WipeDestroyableObjectsFromScreen();
			HideMine();
			MoveCameraToDeafaultPosition();
		}



		// METHODS
		private void DeactivateCollider()
		{
			_mineCollider.SetActive(false);
		}

		private void ActivateExplosionVisualEffect()
		{
			var mineExplosion = GetComponent<MineExplosion>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, mineExplosion);
	#endif
			#endregion

			mineExplosion.ShowExplosion();
		}

		private void WipeDestroyableObjectsFromScreen()
		{
			Services.Get<ITimerManagerMono>().StartTimer(
				Services.Get<IMineExplosionManager>().AplyExplosion, 0.5f);
		}

		private void HideMine()
		{
			Services.Get<ITimerManagerMono>().StartTimer(Hide, 1.5f);
		}

		private void MoveCameraToDeafaultPosition()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				Services.Get<ICameraManager>().MoveCameraCenter(1f);
			}
		}



	}
}