﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishBigFeedback : AMonoScript, IFeedback
	{


		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}


		// INTERFACES
		public void ApplyFeedback()
		{
			var goMove = GetComponent<FishBigMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			SetFishBigReverseDirection(goMove);
			SetFishBigPhysics(goMove);
			Services.Get<ICameraManager>().MoveCameraCenter(2f);

			// avatar
			var goAvatar = GetComponent<IAvatarSupport>();
			goAvatar.HideAvatar();
		}


		// METHODS
		private void SetFishBigReverseDirection(FishBigMove goMove)
		{
			if (goMove.Get_CurrentDirection() == Enumerators.Directions.LEFT)
			{
				goMove.Set_CurrentDirection(Enumerators.Directions.RIGHT);
			}
			else if (goMove.Get_CurrentDirection() == Enumerators.Directions.RIGHT)
			{
				goMove.Set_CurrentDirection(Enumerators.Directions.LEFT);
			}
		}

		private void SetFishBigPhysics(FishBigMove goMove)
		{
			var rigid = goMove.gameObject.GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rigid);
	#endif
			#endregion

			rigid.velocity *= -1;       // revert FishBig move direction
			rigid.velocity *= 2f;       // x2 speed
		}



	}
}