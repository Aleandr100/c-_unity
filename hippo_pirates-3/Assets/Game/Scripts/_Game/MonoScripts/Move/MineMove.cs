﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class MineMove : ABaseMove
	{
		// FIELDS
		private float _speedNormal = 1f;
		private float _speedSlow = 0.5f;
	


		// A_SCRIPTS
		protected override void InitStart()
		{
			SetInitSpeedCheck();
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}


		// INTERFACES
		public void SetSpeedNormal()
		{
			_speed = _speedNormal;
			Move();
		}

		public void SetSpeedSlow()
		{
			_speed = _speedSlow;
			Move();
		}



		// METHODS
		private void Move()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				_rigid.velocity = Vector2.left * _speed;
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				_rigid.velocity = Vector2.up * _speed;
			}
		}

		private void SetInitSpeedCheck()
		{
			if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.NORMAL)
			{
				_speed = _speedNormal;
			}
			else if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.SLOW)
			{
				_speed = _speedSlow;
			}
		}

	

	}
}
