﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class BonusMove : ABaseMove
	{
		// FIELDS
		private Vector2 _vectMoveHorizontal = new Vector2(-1, 0);
		private Vector2 _vectMoveVertical = new Vector2(-1, -1);

		private float _speedNormal = 1f;
		private float _speedSlow = 0.5f;



		// A_SCRIPTS
		protected override void InitStart()
		{
			CheckInitSpeed();
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void SetVectorMoveHorizontal()
		{
			_vectMove = _vectMoveHorizontal;
			Move();
		}

		public void SetVectorMoveVertical()
		{
			_vectMove = _vectMoveVertical;
			Move();
		}

		public void SetSpeedNormal()
		{
			if ( IsNeedChangeSpeed() )
			{
				_speed = _speedNormal;
				Move();
			}
		}

		public void SetSpeedSlow()
		{
			if ( IsNeedChangeSpeed() )
			{
				_speed = _speedSlow;
				Move();
			}
		}



		// METHODS
		private void Move()
		{
			_rigid.velocity = _vectMove * _speed;
		}

		private bool IsNeedChangeSpeed()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				// if bonus spwns on bottom
				if (_vectMove == _vectMoveHorizontal)
				{
					return true;
				}
			}

			return false;
		}

		private void CheckInitSpeed()
		{
			if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.NORMAL)
			{
				_speed = _speedNormal;
			}
			else if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.SLOW)
			{
				_speed = _speedSlow;
			}
		}



	}
}