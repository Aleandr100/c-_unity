﻿using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TreasureMove : ABaseMove
	{
		// FIELDS
		private float _speedNormal = 1f;
		private float _speedSlow = 0.5f;



		// A_SCRIPTS
		protected override void InitStart()
		{
			CheckInitVectorMove();
			CheckInitSpeed();
			CheckInitposition();
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}



		// INTERFACE
		public void SetSpeedNormal()
		{
			_speed = _speedNormal;
			Move();
		}

		public void SetSpeedSlow()
		{
			_speed = _speedSlow;
			Move();
		}



		// METHODS
		private void Move()
		{
			_rigid.velocity = _vectMove * _speed;
		}

		private void CheckInitVectorMove()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				_vectMove = new Vector2(-1, 0);
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				_vectMove = new Vector2(0, 1);
			}
		}

		private void CheckInitSpeed()
		{
			if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.NORMAL)
			{
				_speed = _speedNormal;
			}
			else if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.SLOW)
			{
				_speed = _speedSlow;
			}
		}

		private void CheckInitposition()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				SetPositionHorizontal();
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				SetPositionVertical();
			}
		}

		private void SetPositionHorizontal()
		{
			_transform.position = new Vector2(15f, -3.5f);
		}

		private void SetPositionVertical()
		{
			int rndVariant = Random.Range(0, 2);		// we have two varialnts - left and right "wall"

			if (rndVariant == 0)
			{
				_transform.position = new Vector2(-7f, -7f);
			}
			else if (rndVariant == 1)
			{
				_transform.position = new Vector2(7f, -7f);
			}
		}



	}
}