﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishBigMove : ABaseMove
	{
		// FIELDS
		private Enumerators.Directions _currDir;			
		private Enumerators.FishDepths _currFishDepth;	

	
	
		// A_SCRIPTS
		protected override void InitStart()
		{
			CheckNeedSetDefaultSpeed();
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		#region GETTERS/SETTERS
		//
		public Vector2 Get_VectorMove()
		{
			return _vectMove;
		}

		public void Set_VectorMove(Vector2 pos)
		{
			_vectMove = pos;
		}

		//
		public float Get_Speed()
		{
			return _speed;
		}

		public void Set_Speed(float speed)
		{
			_speed = speed;
		}

		//
		public Enumerators.Directions Get_CurrentDirection()
		{
			return _currDir;
		}

		public void Set_CurrentDirection(Enumerators.Directions direction)
		{
			_currDir = direction;

			SetHideCheckParameters();
			SetCameraEnterParameters();
		}

		#endregion
	


		// METHODS
		private void CheckNeedSetDefaultSpeed()
		{
			if (_speed <= 0) {
				_speed = 1f; }
		}

		private void Move()
		{
			_rigid.velocity = _vectMove * _speed;
		}

		private void SetHideCheckParameters()
		{
			var hideCheck = GetComponent<FishBigHideCheck>();
			Assert.AreNotEqual(null, hideCheck);

			hideCheck.Set_CurrDir(_currDir);
		}

		private void SetCameraEnterParameters()
		{
			var cameraEnter = GetComponent<FishBigCameraEnter>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, cameraEnter);
	#endif
			#endregion

			cameraEnter.Set_CurrDir(_currDir);
		}

	

	}
}