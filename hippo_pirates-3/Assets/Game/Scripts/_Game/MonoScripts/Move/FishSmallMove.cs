﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishSmallMove : ABaseMove
	{
		// FIELDS
		private Enumerators.Directions _currDir;
		private Enumerators.FishDepths _currFishDepth;

	

		// A_SCRIPT
		protected override void InitStart()
		{
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		#region GETTERS/SETTERS

		//

		public void Set_VectorMove(Vector2 pos)
		{
			_vectMove = pos;
		}

		public void Set_Speed(float speed)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(speed > 0);
	#endif
			#endregion

			_speed = speed;
		}

		//
		public void RecalculateMove(Vector2 pos, float speed)
		{
			_vectMove = pos;
			_speed = speed;
			Move();
		}

		//
		public Enumerators.Directions Get_CurrentDirection()
		{
			return _currDir;
		}

		public void Set_CurrentDirection(Enumerators.Directions direction)
		{
			_currDir = direction;
		}

		// 
		public Enumerators.FishDepths Get_CurrentFishDepth()
		{
			return _currFishDepth;
		}

		public void Set_CurrentFishDepth(Enumerators.FishDepths fishDepth)
		{
			_currFishDepth = fishDepth;
		}

		#endregion
	
	

		// METHODS
		private void Move()
		{
			_rigid.velocity = _vectMove * _speed;
		}

	

	}
}