﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class RopeCuteMove : ABaseMove
	{
	


		// A_SCRIPTS
		protected override void InitStart()
		{
			_speed = 1f;        // 1f
			SetVector();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		private void SetVector()
		{
			_rigid.velocity = Vector2.left * _speed;
		}

	

	}
}