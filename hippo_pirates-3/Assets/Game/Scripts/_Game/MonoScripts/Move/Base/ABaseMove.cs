﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public abstract class ABaseMove : AMonoScript 
	{
		// FIELDS
		// ---------------------------------------
		//
		protected Transform _transform;
		protected Rigidbody2D _rigid;

		protected float _speed;
		protected Vector2 _vectMove;



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_transform = transform;

			_rigid = gameObject.GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _rigid);
	#endif
			#endregion
		}

	

		// INTERFACES
		public void StopMovement()
		{
			_rigid.velocity = Vector2.zero;
		}

		public void MoveUp()
		{
			Services.Get<ISoundManagerMono>().PlaySound(SoundsGameplayFX.Shooting.itemsStartFly);

			_rigid.isKinematic = false;
			_rigid.gravityScale = -1;
		}
	}
}