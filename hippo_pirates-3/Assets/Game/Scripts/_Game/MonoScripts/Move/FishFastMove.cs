﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishFastMove : ABaseMove
	{
		// FIELDS
		private float _tempStartY;

		private Enumerators.FishDepths _currFishDepth;




		// A_SCRIPTS
		protected override void InitAwake()
		{
			base.InitAwake();

			SetRandomSpeed();
			SetRandomDepthY();
			SetDirection();
		}

		protected override void InitStart()
		{
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES (getters/setters)
		public Enumerators.FishDepths Get_CurrFishDepth()
		{
			return _currFishDepth;
		}
	


		// METHODS
		private void SetRandomSpeed()
		{
			const float minSpeed = 8f;
			const float maxSpeed = 10f;
			float rndSpeed = Random.Range(minSpeed, maxSpeed);

			_speed = rndSpeed;
		}

		private void SetDirection()
		{
			_vectMove = Vector2.left;

			const float startRightX = 20f;
			transform.position = new Vector2(startRightX, _tempStartY);
		}

		private void SetRandomDepthY()
		{
			// hull sections => top / center / bottom
			const int variants = 3;		
			int rnd = Random.Range(0, variants);

			if (rnd == 0)
			{
				_currFishDepth = Enumerators.FishDepths.TOP;
			}
			else if (rnd == 1)
			{
				_currFishDepth = Enumerators.FishDepths.CENTER;
			}
			else if (rnd == 2)
			{
				_currFishDepth = Enumerators.FishDepths.BOTTOM;
			}

			//
			float[] startsY = { 1, -0.2f, -1.3f };        // 0, -1.2f, -2.3f
			_tempStartY = startsY[rnd];
		}

		private void Move()
		{
			_rigid.velocity = _vectMove * _speed;
		}

	

	}
}