﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TrashBigMove : ABaseMove
	{
	
	
	
		// A_SCRIPTS
		protected override void InitStart()
		{
			SetGravityScale();
			SetVector();

			CheckNeedSetDefaultSpeed();
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void RecalculateMove(Vector2 pos, float speed)
		{
			_vectMove = pos;
			_speed = speed;
			Move();
		}

		public void Set_Speed(float speed)
		{
			_speed = speed;
		}



		// METHODS
		private void SetGravityScale()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				_rigid.gravityScale = 0;
			}

			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				const float minGravScale = 0.005f;
				const float maxGravScale = 0.007f;

				float rnd = Random.Range(minGravScale, maxGravScale);
				_rigid.gravityScale = rnd;
			}
		}

		private void SetVector()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				_vectMove = new Vector2(-1f, -0.3f);            // new Vector2(-1.2f, -0.5f);
			}

			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				// --- none ---
			}
		}

		private void CheckNeedSetDefaultSpeed()
		{
			if (_speed <= 0) {
				_speed = 1f; }
		}

		private void Move()
		{
			_rigid.velocity = _vectMove * _speed;
		}



	}
}