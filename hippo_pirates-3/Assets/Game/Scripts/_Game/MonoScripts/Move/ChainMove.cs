﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class ChainMove : ABaseMove
	{
		// FIELDS
		[SerializeField] private Transform _pointUp;
		[SerializeField] private Transform _pointDown;

		private float _speedNormal = 1f;
		private float _speedSlow = 0.5f;



		// A_SCRIPTS
		protected override void InitStart()
		{
			SetInitSpeedCheck();
			Move();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		#region GETTERS/

		public Transform Get_PointUp()
		{
			return _pointUp;
		}

		public Transform Get_PointDown()
		{
			return _pointDown;
		}

		//
		public void SetSpeedNormal()
		{
			_speed = _speedNormal;
			Move();
		}

		public void SetSpeedSlow()
		{
			_speed = _speedSlow;
			Move();
		}

		#endregion

	

		// METHODS
		private void Move()
		{
			_rigid.velocity = Vector2.left * _speed;
		}

		private void SetInitSpeedCheck()
		{
			if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.NORMAL)
			{
				_speed = _speedNormal;
			}
			else if (Services.Get<IBottomSpeedManager>().Get_Speedtype() == BottomSpeedManager.SpeedType.SLOW)
			{
				_speed = _speedSlow;
			}
		}
	

	}
}