﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class RopeHideCheck : ABaseHideCheck
	{
		// FIELDS
		private readonly float _hideX_left = -12f;



		// A_SCRIPTS
		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		protected override void CheckHide()
		{
			if (_transform.position.x < _hideX_left)
			{
				Hide();
			}
		}



	}
}