﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class BonusHideCheck : ABaseHideCheck
	{
		// FIELDS
		private readonly float _hideX_left = -10f;
		private readonly float _hideY_down = -10f;


		// A_SCRIPTS
		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}


		// METHODS
		protected override void CheckHide()
		{
			//
			if (transform.position.x < _hideX_left)
			{
				Hide();
			}

			//
			if (transform.position.y < _hideY_down)
			{
				Hide();
			}
		}
	


	}
}