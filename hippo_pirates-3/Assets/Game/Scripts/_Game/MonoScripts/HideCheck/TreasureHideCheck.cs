﻿using Pirates_3.Common;
using Pirates_3.Common.Info;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TreasureHideCheck : ABaseHideCheck, ITrapedSupport
	{
		// FIELDS
		private readonly float _hideX_left = -10f;
		private readonly float _hideY_up = 8f;

		private bool _isTrapedFinal;


		// A_SCRIPTS
		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}



		// INTERFACES
		public void FinalTrapedActions()
		{
			_isTrapedFinal = true;
		}



		// METHODS
		protected override void CheckHide()
		{
			//
			if (transform.position.x < _hideX_left)
			{
				Hide();
			}

			//
			if (transform.position.y > _hideY_up)
			{
				if (_isTrapedFinal ||
				    LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
				{
					Hide();
				}
			}
		}



	}
}