﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class TrashBigHideCheck : ABaseHideCheck, ITrapedSupport
	{
		// FIELDS
		private readonly float _hideY_up = 8f;
		private readonly float _hideY_down = -10f;

		private bool _isTrapedFinal;



		// A_SCRIPTS
		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES 
		public void FinalTrapedActions()
		{
			_isTrapedFinal = true;
		}
	


		// METHODS
		protected override void CheckHide()
		{
			if (_transform.position.y > _hideY_up)    
			{
				if (_isTrapedFinal)
				{
					Hide();
				}
			}

			else if (_transform.position.y < _hideY_down)
			{
				Hide();
			}
		}



	}
}