﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishBigHideCheck : ABaseHideCheck, ITrapedSupport
	{
		// FIELDS
		private readonly float _hideX_left = -14f;
		private readonly float _hideX_right = 25f;
		private readonly float _hideY_up = 10f;

		private bool _isTrapedFinal;

		private Enumerators.Directions _currDir;
	


		// A_SCRIPTS
		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// INTERFACES
		public void FinalTrapedActions()
		{
			_isTrapedFinal = true;
		}

		public void Set_CurrDir(Enumerators.Directions dir)
		{
			_currDir = dir;
		}
	


		// METHODS
		protected override void CheckHide()
		{

			if (_currDir == Enumerators.Directions.LEFT)       
			{
				if (_transform.position.x < _hideX_left)
				{
					Hide();
				}
			}
			else if (_currDir == Enumerators.Directions.RIGHT)
			{
				if (_transform.position.x > _hideX_right)
				{
					Hide();
				}
			}

			//
			if (_transform.position.y > _hideY_up)				
			{
				if (_isTrapedFinal)
				{
					Hide();
				}
			}
		}
	


	}
}