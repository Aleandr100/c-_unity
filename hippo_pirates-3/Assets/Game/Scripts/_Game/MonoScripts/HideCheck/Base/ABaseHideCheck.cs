﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public abstract class ABaseHideCheck : AMonoScript
	{
		// FIELDS
		protected Transform _transform;
	
	

		// A_SCRIPTS
		protected override void InitAwake()
		{
			_transform = transform;
		}

		protected override void InitStart()
		{
			StartSlowUpdate();
		}

		protected override void SlowUpdate()
		{
			CheckHide();
		}



		// ABSTRACT
		protected abstract void CheckHide();



	}
}