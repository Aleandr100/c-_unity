﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public abstract class ABaseCollider : AMonoScript
	{
		// FIELDS
		[SerializeField] protected Transform _parent;

	

		// UNITY
		protected void OnTriggerEnter2D(Collider2D coll)
		{
			CheckCollType(coll.gameObject);
		}



		// INTERFACES (getters/setters)
		public Transform Get_Parent()
		{
			return _parent;
		}



		// ABSTRACT
		protected abstract void CheckCollType(GameObject coll);

    
	
	}
}