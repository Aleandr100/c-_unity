﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoScripts
{
	public class FishBigCameraEnter : AMonoScript
	{
		// FIELDS
		// ----------------------------------------
		private Transform _transform;

		// ----------------------------------------
		private readonly string _strOctopus = "Octopus";
		private readonly float _posEnterCameraX_SquidBig = 18.7f;

		private readonly string _strSquidBig = "Squid_Big";
		private readonly float _posEnterCameraX_Whale = 18.7f;

		private readonly string _strWhale = "Whale";
		private readonly float _posEnterCameraX_Octopus = 15.8f;

		private bool _isEnterCamera;

		// ----------------------------------------
		private Enumerators.Directions _currDir;		



		// A_SCRIPTS
		protected override void InitAwake()
		{
			_transform = transform;
		}

		protected override void InitStart()
		{
			StartSlowUpdate();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void SlowUpdate()
		{
			CheckEnterCamera();
		}



		// INTERFACES
		public void Set_CurrDir(Enumerators.Directions direction)
		{
			_currDir = direction;
		}
	


		// METHODS
		private void CheckEnterCamera()
		{
			if (_currDir == Enumerators.Directions.LEFT)
			{
				if (!_isEnterCamera)
				{
					CheckOctopus();
					CheckSquidBig();
					CheckWhale();
				}
			}
		}

		private void CheckOctopus()
		{
			if (name == _strOctopus)
			{
				if (_transform.position.x < _posEnterCameraX_Octopus)
				{
					EnterCameraActions();
				}
			}
		}

		private void CheckSquidBig()
		{
			if (name == _strSquidBig)
			{
				if (_transform.position.x < _posEnterCameraX_SquidBig)
				{
					EnterCameraActions();
				}
			}
		}

		private void CheckWhale()
		{
			if (name == _strWhale)
			{
				if (_transform.position.x < _posEnterCameraX_Whale)
				{
					EnterCameraActions();
				}
			}
		}

		private void EnterCameraActions()
		{
			_isEnterCamera = true;
			Services.Get<ICameraManager>().MoveCameraRight();
		}

	

	}
}