﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class SubmarineVisualEffects : AMonoService, ISubmarineVisualEffects
	{
		// FIELDS
		// --------------------------------------------
		[SerializeField] private Animator _animatorSubmarine;
		[SerializeField] private Animator _animatorHatch;
		[SerializeField] private Animator _animatorBalloons;
		[SerializeField] private Animator _animatorProjector;
		[SerializeField] private Animator _animatorScrew;
		[SerializeField] private Animator _animatorScrewWaves;
		[SerializeField] private Animator _animatorForceField;

		// --------------------------------------------
		// dependences
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(IAnimationManager animationManager,
								ITimerManager timerManager)
		{
			_animationManager = animationManager;
			_timerManager = timerManager;
		}


	
		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			if ( CheckIsDarkLevel() ) {
				LightProjectors(true); }
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ISubmarineVisualEffects>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ISubmarineVisualEffects>();
		}



		// INTERFACES
		public void HitSubmarine()
		{
			_animationManager.SetParameterTrigger(_animatorSubmarine, ParamTriggers.Submarine.wasHit);

			if ( CheckIsDarkLevel() )
			{
				TemporaryProjectorsBreak();
			}
		}

		//
		public void OpenHatch()
		{
			_animationManager.SetParameterTrigger(_animatorHatch, ParamTriggers.Submarine.needOpenhatch);
		}

		public void BlowBalloons()
		{
			_animationManager.SetParameterTrigger(_animatorBalloons, ParamTriggers.Submarine.needBalloons);
		}

		//
		public void LightProjectors(bool boolValue)
		{
			_animationManager.SetParameterBool(_animatorProjector, ParamBools.Submarine.isLightUp, boolValue);
		}

		//
		public void StartScrewRotate()
		{
			_animationManager.SetParameterBool(_animatorScrew, ParamBools.Submarine.isRotating, true);
		}

		public void StopScrewRotate()
		{
			_animationManager.SetParameterBool(_animatorScrew, ParamBools.Submarine.isRotating, false);
		}

		//
		public void StartScrewWavesHorizontal()
		{
			_animationManager.SetParameterInt(_animatorScrewWaves, ParamInts.ScrewWaves.direction, (int)ParamInts.ScrewWaves.Directions.LEFT);
		}

		public void StartScrewWavesVertical()
		{
			_animationManager.SetParameterInt(_animatorScrewWaves, ParamInts.ScrewWaves.direction, (int)ParamInts.ScrewWaves.Directions.UP);
		}

		//
		public void ShowForceField(bool boolValue)
		{
			_animationManager.SetParameterBool(_animatorForceField, ParamBools.Submarine.isShowForceField, boolValue);
		}
	


		// METHODS
		private bool CheckIsDarkLevel()
		{
			return 
			(
				LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.HORIZONTAL_B ||
				LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.VERTICAL_B
			);
		}

		private void TemporaryProjectorsBreak()
		{
			LightProjectors(false);

			_timerManager.StartTimer(LightProjectors, true, 1f);
		}



	}
}