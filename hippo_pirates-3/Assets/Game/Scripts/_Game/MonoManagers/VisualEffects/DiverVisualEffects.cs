﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class DiverVisualEffects : AMonoService, IDiverVisualEffects
	{
		// FIELDS
		// -------------------------------------------------------
		[Header("ANIMATORS")]
		[SerializeField] private Animator _animatorDiverTools;
		[SerializeField] private Animator _animatorDiverStars;

		// -------------------------------------------------------
		[Header("REPAIR BAR")]
		[SerializeField] private GameObject _objRepairBar;
		[SerializeField] private Image _imageRepairBar;

		// -------------------------------------------------------
		[Header("RELOAD BAR")]
		[SerializeField] private GameObject _objReloadBar;
		[SerializeField] private Image _imageReload;
	
		// -------------------------------------------------------
		[Header("HARPOON-GUN INDICATOR")]
		[SerializeField] private GameObject _objHarpoonLabel;

		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IDiverVisualEffects>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IDiverVisualEffects>();
		}


	
		// INTERFACES
		public void ShowToolInHands(Enumerators.ToolsTypes toolType)
		{
			_soundManager.PlaySound(SoundsGameplayFX.Misc.toolSwap);
			_animationManager.SetParameterInt(_animatorDiverTools, ParamInts.Tools.selectedTool, (int)toolType);
		}

		public void AnimateToolInHands(bool boolValue)
		{
			_animationManager.SetParameterBool(_animatorDiverTools, ParamBools.Diver.isUsingTool, boolValue);
		}

		public void ShowShortToolAnimation()
		{
			_animationManager.SetParameterBool(_animatorDiverTools, ParamBools.Diver.isUsingTool, true);

			_timerManager.StartTimer<object>(
				_animationManager.SetParameterBool,
				(new object[] { _animatorDiverTools, ParamBools.Diver.isUsingTool, false }), 0.5f);

			_timerManager.StartTimer(
				Services.Get<IDiverVisualEffects>().ShowToolInHands, Enumerators.ToolsTypes.NONE, 1f);
		}

		//
		public void ShowDiverStars(float starsTime)
		{
			_animationManager.SetParameterBool(_animatorDiverStars, ParamBools.Diver.isShowStars, true);
			_timerManager.StartTimer(HideDiverStars, starsTime);
		}

		public void ShowRepairBar(float repairTime)
		{
			_objRepairBar.SetActive(true);
			StartCoroutine( FillingRepairBar(repairTime) );
		}

		public void ShowReloadBar(float reloadTime)
		{
			_objReloadBar.SetActive(true);
			StartCoroutine( FillingReloadBar(reloadTime) );
		}

		public void ShowHarpoonLabel(bool boolValue)
		{
			_objHarpoonLabel.SetActive(boolValue);
		}



		// METHODS

		#region DIVER_STARS

		private void HideDiverStars()
		{
			_animationManager.SetParameterBool(_animatorDiverStars, ParamBools.Diver.isShowStars, false);
		}

		#endregion

		#region REPAIR_BAR

		private IEnumerator FillingRepairBar(float repairTime)
		{
			const float delay = 0.05f;
			float tempTimer = 0f;
		
			while (true)
			{
				tempTimer += delay;
				_imageRepairBar.fillAmount = tempTimer / repairTime;

				if (tempTimer > repairTime)
				{
					HideRepairBar();
					break;
				}

				yield return new WaitForSeconds(delay);
			}
		}

		private void HideRepairBar()
		{
			_objRepairBar.SetActive(false);
		}

		#endregion

		#region RELOAD_BAR

		private IEnumerator FillingReloadBar(float reloadTime)
		{
			const float delay = 0.05f;
			float tempTimer = 0f;

			while (true)
			{
				tempTimer += delay;
				_imageReload.fillAmount = tempTimer / reloadTime;
			
				if (tempTimer > reloadTime)
				{
					HideReloadBar();
					break;
				}

				yield return new WaitForSeconds(delay);
			}
		}

		private void HideReloadBar()
		{
			_objReloadBar.SetActive(false);
		}

		#endregion
	


	}
}