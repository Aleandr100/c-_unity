﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using Pirates_3.Common.Info;
using Pirates_3.Common;
using DG.Tweening;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class ToolsButtonsVisualEffectsManager : AMonoService, IToolsButtonsVisualEffectsManager
	{
		// FIELDS
		// -----------------------------------------------
		[Header("BUTTONS")]
		[SerializeField] private Transform _transfButtonScissors;
		[SerializeField] private Transform _transfButtonMagnet;
		[SerializeField] private Transform _transfButtonHammer;
		[SerializeField] private Transform _transfButtonWelding;

		// -----------------------------------------------
		[Header("HALO")]
		[SerializeField] private GameObject _objScissorsHalo;
		[SerializeField] private GameObject _objMagnetHalo;
		[SerializeField] private GameObject _objHammerHalo;
		[SerializeField] private GameObject _objWeldingHalo;

		// -----------------------------------------------
		private int _countScissors = 0;
		private int _countMagnet = 0;
		private int _countHammer = 0;
		private int _countWelding = 0;

		// -----------------------------------------------
		private readonly Dictionary<Enumerators.DamageTypes, Transform> _dictButtons =
			new Dictionary<Enumerators.DamageTypes, Transform>();

		private readonly Dictionary<Enumerators.DamageTypes, GameObject> _dictButtonsHalos =
			new Dictionary<Enumerators.DamageTypes, GameObject>();

		private readonly Dictionary<Enumerators.DamageTypes, int> _dictDamageTypeCounts =
			new Dictionary<Enumerators.DamageTypes, int>();



		// A_SERVICES
		protected override void InitAwake()
		{
			InitDictButtons();
			InitDictButtonsHalos();
			InitDictDamageCounts();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IToolsButtonsVisualEffectsManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IToolsButtonsVisualEffectsManager>();
		}



		// INTERFACES
		public void CheckShowButtonVisualEffects(Enumerators.DamageTypes dmgType)
		{
			IncreaseCount(dmgType);

			if (_dictDamageTypeCounts[dmgType] > 0 && _dictDamageTypeCounts[dmgType] <= 2)
			{
				ShowHalo(dmgType, true);
				StartButtonPulsing(dmgType);
			}
		}

		public void CheckHideButtonVisualEffects(Enumerators.DamageTypes dmgType)
		{
			ReduceCount(dmgType);

			if (_dictDamageTypeCounts[dmgType] <= 0)
			{
				ShowHalo(dmgType, false);
				StopButtonPulsing(dmgType);
			}
		}



		// METHODS 

		#region INITS

		private void InitDictButtons()
		{
			_dictButtons.Add(Enumerators.DamageTypes.HULL_TORN, _transfButtonHammer);

			_dictButtons.Add(Enumerators.DamageTypes.DENT_SMALL, _transfButtonMagnet);
			_dictButtons.Add(Enumerators.DamageTypes.DENT_BIG, _transfButtonMagnet);

			_dictButtons.Add(Enumerators.DamageTypes.SCRATCH, _transfButtonWelding);
			_dictButtons.Add(Enumerators.DamageTypes.HOLE, _transfButtonWelding);
			_dictButtons.Add(Enumerators.DamageTypes.BOTTOM_BREAK, _transfButtonWelding);

			_dictButtons.Add(Enumerators.DamageTypes.SCREW_NET, _transfButtonScissors);
			_dictButtons.Add(Enumerators.DamageTypes.SCREW_ROPE, _transfButtonScissors);
			_dictButtons.Add(Enumerators.DamageTypes.SCREW_SEAWEED, _transfButtonScissors);
		}

		private void InitDictButtonsHalos()
		{
			_dictButtonsHalos.Add(Enumerators.DamageTypes.HULL_TORN, _objHammerHalo);

			_dictButtonsHalos.Add(Enumerators.DamageTypes.DENT_SMALL, _objMagnetHalo);
			_dictButtonsHalos.Add(Enumerators.DamageTypes.DENT_BIG, _objMagnetHalo);

			_dictButtonsHalos.Add(Enumerators.DamageTypes.SCRATCH, _objWeldingHalo);
			_dictButtonsHalos.Add(Enumerators.DamageTypes.HOLE, _objWeldingHalo);
			_dictButtonsHalos.Add(Enumerators.DamageTypes.BOTTOM_BREAK, _objWeldingHalo);

			_dictButtonsHalos.Add(Enumerators.DamageTypes.SCREW_NET, _objScissorsHalo);
			_dictButtonsHalos.Add(Enumerators.DamageTypes.SCREW_ROPE, _objScissorsHalo);
			_dictButtonsHalos.Add(Enumerators.DamageTypes.SCREW_SEAWEED, _objScissorsHalo);
		}

		private void InitDictDamageCounts()
		{
			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.HULL_TORN, _countHammer);

			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.DENT_SMALL, _countMagnet);
			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.DENT_BIG, _countMagnet);

			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.SCRATCH, _countWelding);
			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.HOLE, _countWelding);
			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.BOTTOM_BREAK, _countWelding);

			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.SCREW_NET, _countScissors);
			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.SCREW_ROPE, _countScissors);
			_dictDamageTypeCounts.Add(Enumerators.DamageTypes.SCREW_SEAWEED, _countScissors);
		}

		#endregion

		private void ShowHalo(Enumerators.DamageTypes dmgType, bool boolValue)
		{
			_dictButtonsHalos[dmgType].SetActive(boolValue);
		}

		private void StartButtonPulsing(Enumerators.DamageTypes dmgType)
		{
			Vector2 endScale = new Vector2(1.1f, 1.1f);
			const float scaleDuration = 0.5f;
			const int numberOfLoops = 999;

			_dictButtons[dmgType].DOScale(endScale, scaleDuration)
				.SetLoops(numberOfLoops, LoopType.Yoyo);
		}

		private void StopButtonPulsing(Enumerators.DamageTypes dmgType)
		{
			_dictButtons[dmgType].DOKill();
			_dictButtons[dmgType].localScale = new Vector2(1, 1);
		}

		private void IncreaseCount(Enumerators.DamageTypes dmgType)
		{
			_dictDamageTypeCounts[dmgType]++;
		}

		private void ReduceCount(Enumerators.DamageTypes dmgType)
		{
			_dictDamageTypeCounts[dmgType]--;
		}
	


	}
}