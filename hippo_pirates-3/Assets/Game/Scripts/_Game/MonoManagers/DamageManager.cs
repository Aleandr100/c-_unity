﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Pirates_3.Common.Info;
using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Menu.Gameplay.MonoScripts;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class DamageManager : AMonoService, IDamageManager
	{
		// FIELDS
		// --------------------------------------
		//
		[Header("[DAMAGE TYPES]")]
		[SerializeField] private Transform _hullTornObject;
		[SerializeField] private Transform _dentSmallObject;
		[SerializeField] private Transform _dentBigObject;
		[SerializeField] private Transform _scrathObject;
		[SerializeField] private Transform _holeObject;
		[SerializeField] private Transform _bottomBreakOnject;

		// --------------------------------------
		//
		[Header("[SCREW BREAKS]")]
		[SerializeField] private Transform _screwNetObject;
		[SerializeField] private Transform _screwRopeObject;
		[SerializeField] private Transform _screwSeaweedObject;

		// --------------------------------------
		//
		private readonly IDamageType[] _arrScrewSeaweedsDT = new IDamageType[3];

		private List<Transform> _hullTornList = new List<Transform>();
		private List<Transform> _dentSmallList = new List<Transform>();
		private List<Transform> _dentBigList = new List<Transform>();
		private List<Transform> _scratchList = new List<Transform>();
		private List<Transform> _holeList = new List<Transform>();
		private List<Transform> _bottomBreakList = new List<Transform>();
		private List<Transform> _screwSeaweedList = new List<Transform>();

		// --------------------------------------
		//
		private readonly List<int> _tempIdSecList = new List<int>();

		private readonly Dictionary<Enumerators.DamageTypes, List<Transform>> _dictDmgTypesLists =
			new Dictionary<Enumerators.DamageTypes, List<Transform>>();

		// --------------------------------------
		// bonus Armor
		private bool _isBonusArmor;



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IDamageManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IDamageManager>();
		}


		// UNITY
		private void OnEnable()
		{
			#region INIT_LISTS

			InitHullTornList();
			InitDentSmallList();
			InitDentBigList();
			InitScratchList();
			InitHoleSmallList();
			InitBottomBreakList();
			InitScrewSeaweedList();

			#endregion

			InitDictDmgTypesLists();
			InitArrayScrewSeaweedsDT();
		}



		// INTERFACES
		#region GETTERS/SETTERS
		public List<Transform> Get_HullTornList()
		{
			return _hullTornList;
		}

		public List<Transform> Get_DentSmallList()
		{
			return _dentSmallList;
		}

		public List<Transform> Get_DentBigList()
		{
			return _dentBigList;
		}

		public List<Transform> Get_ScratchList()
		{
			return _scratchList;
		}

		public List<Transform> Get_HoleList()
		{
			return _holeList;
		}

		public List<Transform> Get_BottomBreakList()
		{
			return _bottomBreakList;
		}

		public List<Transform> Get_ScrewSeaweedList()
		{
			return _screwSeaweedList;
		}
		#endregion
	
		public void SetDirectDamage(GameObject coll, Enumerators.DamageTypes dmgType, int secNum)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, coll);
			Assert.IsTrue(secNum >= 0);
	#endif
			#endregion

			List<Transform> tempDmgList = _dictDmgTypesLists[dmgType];

			var dmgTypeScript = tempDmgList[secNum].GetComponent<ABaseDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, dmgTypeScript);
	#endif
			#endregion

			if (dmgTypeScript.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
			{
				var dmgTypeInterface = tempDmgList[secNum].GetComponent<IDamageType>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, dmgTypeScript);
	#endif
				#endregion

				ChekBonusArmorOnDirectDamage(dmgTypeInterface, dmgType);

				//
				var baseClollider = coll.GetComponent<ABaseCollider>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, baseClollider);
	#endif
				#endregion

				Transform collParent = baseClollider.Get_Parent();
				var collFeedback = collParent.GetComponent<IFeedback>();

				if (collFeedback != null)
				{
					collFeedback.ApplyFeedback();
				}
			}
		}

		public void SetRandomDamage(GameObject coll, Enumerators.DamageTypes dmgType, int num)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, coll);
			Assert.IsTrue(num >= 0 && num < 12);        // 12 - number of Submarine sections
	#endif
			#endregion

			List<Transform> tempDmgList = _dictDmgTypesLists[dmgType];
			FillTempDamageList(tempDmgList);
		
			if (_tempIdSecList.Count > 0)
			{
				CheckBonusArmorOnRandomDamage_PartOne(dmgType);

				//
				var baseClollider = coll.GetComponent<ABaseCollider>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, baseClollider);
	#endif
				#endregion

				Transform collParent = baseClollider.Get_Parent();
				var collFeedback = collParent.GetComponent<IFeedback>();

				if (collFeedback != null)
				{
					collFeedback.ApplyFeedback();
				}

				//
				ShuffleList(_tempIdSecList);

				int finalNum = GetFinalNumber(num);
				AddRandomDamage(tempDmgList, dmgType, finalNum);
			
				ClearList(_tempIdSecList);
			}
		}

		public void SetScrewDamage(GameObject coll, Enumerators.DamageTypes dmgType)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, coll);
	#endif
			#endregion

			int totalDmgCount = GetTotalScrewDamageCount();

			if (totalDmgCount == 0)
			{
				if (!_isBonusArmor)
				{
					ShowScrewDamage(coll);
					ApplyEffectsOnScrewDamage(coll, dmgType);

					//
					Services.Get<IToolsButtonsVisualEffectsManager>().CheckShowButtonVisualEffects(dmgType);
				}
				else
				{
					Services.Get<IBonusArmorManager>().Deactivate();
				}
			}
		}

		// bonus
		public void ActivateBonusArmor()
		{
			_isBonusArmor = true;
		}

		public void DeactivateBonusArmor()
		{
			_isBonusArmor = false;
		}
	


		// METHODS

		#region INIT_LISTS

		private void InitHullTornList()
		{
			for (int i = 0; i < 12; i++)	// hull torn = 12
			{
				_hullTornList.Add(_hullTornObject.GetChild(i));
			}
		}

		private void InitDentSmallList()
		{
			for (int i = 0; i < 8; i++)		// dent small = 8
			{
				_dentSmallList.Add(_dentSmallObject.GetChild(i));
			}
		}

		private void InitDentBigList()
		{
			for (int i = 0; i < 2; i++)		// dent big = 2
			{
				_dentBigList.Add(_dentBigObject.GetChild(i));
			}
		}

		private void InitScratchList()
		{
			for (int i = 0; i < 2; i++)		// scratch = 2
			{
				_scratchList.Add(_scrathObject.GetChild(i));
			}
		}

		private void InitHoleSmallList()
		{
			for (int i = 0; i < 6; i++)		// hole small = 6
			{
				_holeList.Add(_holeObject.GetChild(i));
			}
		}

		private void InitBottomBreakList()
		{
			for (int i = 0; i < 1; i++)		// bottom break = 1
			{
				_bottomBreakList.Add(_bottomBreakOnject.GetChild(i));
			}	
		}

		private void InitScrewSeaweedList()
		{
			for (int i = 0; i < 3; i++)		// screw seaweed = 3
			{
				_screwSeaweedList.Add(_screwSeaweedObject.GetChild(i));
			}
		}

		#endregion

		// direct damage
		private void ChekBonusArmorOnDirectDamage(IDamageType dmgTypeScript, Enumerators.DamageTypes dmgType)
		{
			if (!_isBonusArmor)
			{
				dmgTypeScript.ShowDamage();

				Services.Get<ISubmarineVisualEffects>().HitSubmarine();

				ActionsOnHaveDamage();
				Services.Get<ICriticalDamageManager>().CheckDamageType(dmgType);
				Services.Get<ISubmarineHpManager>().AddDmgPerSec(dmgType);

				//
				Services.Get<IToolsButtonsVisualEffectsManager>().CheckShowButtonVisualEffects(dmgType);
			}
			else
			{
				Services.Get<IBonusArmorManager>().Deactivate();
			}
		}

		#region RANDOM_DAMAGE

		private void FillTempDamageList(List<Transform> someList)
		{
			for (int i = 0; i < someList.Count; i++)
			{
				var dmgTypeScript = someList[i].GetComponent<ABaseDamageType>();

				if (dmgTypeScript.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
				{
					_tempIdSecList.Add(i);
				}
			}
		}

		private int GetFinalNumber(int num)
		{
			if (_tempIdSecList.Count >= num)
			{
				return num;
			}

			return _tempIdSecList.Count;
		}

		private void AddRandomDamage(List<Transform> someList, Enumerators.DamageTypes dmgType, int num)
		{
			for (int i = 0; i < num; i++)
			{
				var dmgTypeScript = someList[_tempIdSecList[i]].GetComponent<IDamageType>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, dmgTypeScript);
	#endif
				#endregion

				if (dmgTypeScript.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
				{
					CheckBonusArmorOnRandomDamage_PartTwo(dmgTypeScript, dmgType);
				}
			}
		}

		private void CheckBonusArmorOnRandomDamage_PartOne(Enumerators.DamageTypes dmgType)
		{
			if (!_isBonusArmor)
			{
				Services.Get<ISubmarineVisualEffects>().HitSubmarine();
				Services.Get<IToolsButtonsVisualEffectsManager>().CheckShowButtonVisualEffects(dmgType);

				ActionsOnHaveDamage();
			}
		}

		private void CheckBonusArmorOnRandomDamage_PartTwo(IDamageType dmgTypeScript, Enumerators.DamageTypes dmgType)
		{
			if (!_isBonusArmor)
			{
				dmgTypeScript.ShowDamage();

				Services.Get<ICriticalDamageManager>().CheckDamageType(dmgType);
				Services.Get<ISubmarineHpManager>().AddDmgPerSec(dmgType);
			}
			else
			{
				Services.Get<IBonusArmorManager>().Deactivate();
			}
		}

		#endregion

		#region SCREW_DAMAGE

		private int GetTotalScrewDamageCount()
		{
			int totalDmgCount = 0;

			totalDmgCount += GetScrewNetDamageCount();
			totalDmgCount += GetScrewRopeDamageCount();
			totalDmgCount += GetScrewSeaweedDamageCount();
		
			return totalDmgCount;
		}

		private int GetScrewNetDamageCount()
		{
			int dmgCount = 0;

			var screwNetDT = _screwNetObject.GetChild(0).GetComponent<ABaseDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, screwNetDT);
	#endif
			#endregion

			if (screwNetDT.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.ON)
			{
				dmgCount++;
			}

			return dmgCount;
		}

		private int GetScrewRopeDamageCount()
		{
			int dmgCount = 0;

			var screwRopeDT = _screwRopeObject.GetChild(0).GetComponent<ABaseDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, screwRopeDT);
	#endif
			#endregion

			if (screwRopeDT.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.ON)
			{
				dmgCount++;
			}

			return dmgCount;
		}

		private int GetScrewSeaweedDamageCount()
		{
			int dmgCount = 0;

			for (int i = 0; i < _arrScrewSeaweedsDT.Length; i++)
			{
				if (_arrScrewSeaweedsDT[i].Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.ON)
				{
					dmgCount++;
				}
			}

			return dmgCount;
		}

		private void ShowScrewDamage(GameObject coll)
		{
			// -----------------------------------------------
			//
			if (coll.CompareTag(Tags.Net))
			{
				ShowScrewNetDamage();
			}
			// -----------------------------------------------
			//
			else if (coll.CompareTag(Tags.Rope) ||
					coll.CompareTag(Tags.CutRope))
			{
				ShowScrewRopeDamage();
			}
			// -----------------------------------------------
			//
			else if (coll.CompareTag(Tags.Seaweed))
			{
				ShowScrewSeaweedDamage(coll);
			}
			// -----------------------------------------------
		}

		private void ShowScrewNetDamage()
		{
			var screwNetDT = _screwNetObject.GetChild(0).GetComponent<IDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, screwNetDT);
	#endif
			#endregion

			screwNetDT.ShowDamage();
		}

		private void ShowScrewRopeDamage()
		{
			var screwRopeDT = _screwRopeObject.GetChild(0).GetComponent<IDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, screwRopeDT);
	#endif
			#endregion

			screwRopeDT.ShowDamage();
		}

		private void ShowScrewSeaweedDamage(GameObject coll)
		{
			if (coll.name == "0")
			{
				_arrScrewSeaweedsDT[0].ShowDamage(0);
			}
			else if (coll.name == "1")
			{
				_arrScrewSeaweedsDT[1].ShowDamage(1);
			}
			else if (coll.name == "2")
			{
				_arrScrewSeaweedsDT[2].ShowDamage(2);
			}
			else
			{
				#region DEBUG
	#if UNITY_EDITOR
				Debug.Log(string.Format("coll.name < 0 / coll.name > 2 => {0}", coll.name));
	#endif
				#endregion
			}
		}

		private void ApplyEffectsOnScrewDamage(GameObject coll, Enumerators.DamageTypes dmgType)
		{
			Services.Get<ISubmarineVisualEffects>().HitSubmarine();

			Services.Get<ISubmarineVisualEffects>().StopScrewRotate();
			ActionsOnHaveDamage();
			Services.Get<ICriticalDamageManager>().CheckDamageType(dmgType);
			Services.Get<ISubmarineHpManager>().AddDmgPerSec(dmgType);

			//
			var baseClollider = coll.GetComponent<ABaseCollider>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, baseClollider);
	#endif
			#endregion

			Transform collParent = baseClollider.Get_Parent();
			var collFeedback = collParent.GetComponent<IFeedback>();

			if (collFeedback != null)
			{
				collFeedback.ApplyFeedback();
			}
		}

		#endregion

		// common scripts
		private void InitDictDmgTypesLists()
		{
			_dictDmgTypesLists.Add(Enumerators.DamageTypes.HULL_TORN, _hullTornList);
			_dictDmgTypesLists.Add(Enumerators.DamageTypes.DENT_SMALL, _dentSmallList);
			_dictDmgTypesLists.Add(Enumerators.DamageTypes.DENT_BIG, _dentBigList);
			_dictDmgTypesLists.Add(Enumerators.DamageTypes.SCRATCH, _scratchList);
			_dictDmgTypesLists.Add(Enumerators.DamageTypes.HOLE, _holeList);
			_dictDmgTypesLists.Add(Enumerators.DamageTypes.BOTTOM_BREAK, _bottomBreakList);

			_dictDmgTypesLists.Add(Enumerators.DamageTypes.SCREW_SEAWEED, _screwSeaweedList);
		}

		private void InitArrayScrewSeaweedsDT()
		{
			for (int i = 0; i < _screwSeaweedList.Count; i++)
			{
				_arrScrewSeaweedsDT[i] = _screwSeaweedList[i].GetComponent<IDamageType>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, _arrScrewSeaweedsDT);
	#endif
				#endregion
			}
		}

		private void ShuffleList(List<int> someList)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(someList.Count > 0);
	#endif
			#endregion

			for (int i = 0; i < someList.Count; i++)
			{
				int tmp = someList[i];
				int r = Random.Range(i, someList.Count);
				someList[i] = someList[r];
				someList[r] = tmp;
			}
		}

		private void ClearList(List<int> someList)
		{
			someList.Clear();

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.IsTrue(someList.Count == 0);
	#endif
			#endregion
		}

		private void ActionsOnHaveDamage()
		{
			Services.Get<IRaccoonGameplayController>().ActionsHaveDamage();

			Services.Get<IDamageBubbles>().IncreaseDamageCount();
		}



	}
}