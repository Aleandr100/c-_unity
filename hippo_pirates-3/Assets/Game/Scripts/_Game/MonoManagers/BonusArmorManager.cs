﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class BonusArmorManager : AMonoService, IBonusArmorManager
	{
		// FIELDS
		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IBonusArmorManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IBonusArmorManager>();
		}



		// INTERFACES
		public void Activate()
		{
			Services.Get<IDamageManager>().ActivateBonusArmor();
			Services.Get<IAvatarsManager>().ActivateBonusArmor();
			Services.Get<ISubmarineVisualEffects>().ShowForceField(true);

			_soundManager.PlaySound(SoundsGameplayFX.Bonuses.forceField);
		}

		public void Deactivate()
		{
			Services.Get<IDamageManager>().DeactivateBonusArmor();
			Services.Get<IAvatarsManager>().DeactivateBonusArmor();
			Services.Get<ISubmarineVisualEffects>().ShowForceField(false);

			_soundManager.StopSound(SoundsGameplayFX.Bonuses.forceField);
		}



	}
}