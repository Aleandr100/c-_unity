﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using DG.Tweening;
using Pirates_3.Common;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class CameraManager : AMonoService, ICameraManager
	{
		// FIELDS
		// ---------------------------------------
		//
		[SerializeField] private Transform _cameraGUI;
		[SerializeField] private Transform _cameraMain;

		// ---------------------------------------
		//
		private readonly float _moveSpeed = 2f;
		private readonly Vector3 _posHorCenterX = new Vector3(3.5f, 0, -10);
		private readonly Vector3 _posHorLeftX = new Vector3(-2.95f, 0, -10);
		private readonly Vector3 _posHorRightX = new Vector3(7.05f, 0, -10);
		private readonly Vector3 _posVert = new Vector3(0, 0, -10);

		// ---------------------------------------
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ICameraManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ICameraManager>();
		}

	

	
		// INTEFACES
		public void MoveCameraCenter()
		{
			MoveCamera(_posHorCenterX);
		}

		public void MoveCameraCenter(float myDelay)
		{
			_timerManager.StartTimer( MoveCamera, _posHorCenterX, myDelay );
		}

		//
		public void MoveCameraLeft()
		{
			MoveCamera(_posHorLeftX);
		}

		public void MoveCameraLeft(float myDelay)
		{
			_timerManager.StartTimer( MoveCamera, _posHorLeftX, myDelay );
		}

		//
		public void MoveCameraRight()
		{
			MoveCamera(_posHorRightX);
		}

		public void MoveCameraRight(float myDelay)
		{
			_timerManager.StartTimer( MoveCamera, _posHorRightX, myDelay);
		}

		//
		public void SetCameraHorizontalLevel()
		{
			SetCameraPosition(_posHorCenterX);
		}

		public void SetCameraVerticalLevel()
		{
			SetCameraPosition(_posVert);
		}



		// METHODS
		private void SetCameraPosition(Vector3 myPos)
		{
			_cameraMain.transform.position = myPos;
			_cameraGUI.transform.position = myPos;
		}

		private void MoveCamera(Vector3 myPos)
		{
			_cameraGUI.DOMove(myPos, _moveSpeed);
			_cameraMain.DOMove(myPos, _moveSpeed);
		}
	


	}
}