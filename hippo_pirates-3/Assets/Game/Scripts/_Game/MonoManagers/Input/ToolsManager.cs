﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class ToolsManager : BaseInputManager, IToolsManager
	{
		// FIELDS
		// ------------------------------
		//
		private bool _isBonusRepairRate;

		// ------------------------------
		// dependences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IToolsManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IToolsManager>();
		}



		// INTERFACES
		public void ActionsMouseClickToolButtons(Enumerators.ToolsTypes toolType)
		{
			if (IsUnlocked())
			{
				SetTool(toolType);
				Services.Get<IFakeToolButtonManager>().Activate(toolType);
			}
		}

		public void HandleFakeToolButtonMouseRelease(Vector2 mousePos)
		{
			Collider2D[] arrColls = GetClickedColliders(mousePos);

			if (arrColls != null)
			{
				if (arrColls.Length > 0)
				{
					Transform coll = GetFirstClickedCollider(arrColls);
					CheckColiderType(coll);
				}
			}
		}

		public virtual void RepairCompleted()
		{
			Services.Get<IRaccoonGameplayController>().ActionsRepairDamage();
			_timerManager.StartTimer(RepairCompleteActions, 1f);
		}

		public void TryHandleThreat(Transform coll)
		{
			if (coll != null)
			{
				var collToolUse = GetThreatToolUseScript(coll);
				// -------------------------------------------------------
				if (collToolUse != null)
				{
					Services.Get<IDiverVisualEffects>().ShowShortToolAnimation();

					_timerManager.StartTimer<object>(
						HandleThreat, (new object[] {coll, collToolUse}), 0.5f);
				}
				// -------------------------------------------------------
			}
		}

		//
		public void ActivateBonusRepairRate()
		{
			_isBonusRepairRate = true;
		}

		public void DeactivateBonusRepairRate()
		{
			_isBonusRepairRate = false;
		}

	
	
		// METHDOS

		#region MOUSE_CLICK

		// condition - can we click buttons?
		protected bool IsUnlocked()
		{
			return (_currDiverState == Enumerators.DiverStates.HARPOON_GUN);
		}

		#endregion

		#region FAKE_TOOL_BUTTON_RELEASE

		protected void CheckColiderType(Transform coll)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, coll);
	#endif
			#endregion

			if (coll.gameObject.layer == (int)Enumerators.Layers.DAMAGE_TYPE)
			{
				ApplyToolOnDmgSection(coll);
			}
			else if (coll.gameObject.layer == (int)Enumerators.Layers.THREAT)
			{
				ApplyToolOnThreat(coll);
			}
		}

		#endregion

		#region TOOL_VS_DAMAGE_SECTION

		private void ApplyToolOnDmgSection(Transform coll)
		{
			var dmgType = coll.parent.GetComponent<IDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, dmgType);
	#endif
			#endregion

			bool isRightTool = dmgType.IsRightTool(_currTool);

			if (isRightTool)
			{
				float moveTime = Services.Get<IDiverMovementManager>().MoveDiver(coll.position);
				_timerManager.StartTimer(TryStartRepair, coll, moveTime);
			}
		}

		private void TryStartRepair(Transform coll)
		{
			var dmgType = coll.parent.GetComponent<IDamageType>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, dmgType);
	#endif
			#endregion

			bool isRepairStarted = dmgType.StartRepair(_currTool, _isBonusRepairRate);

			if (isRepairStarted)
			{
				// diver state!
				SetDiverState(Enumerators.DiverStates.REPARING);

				// show tool
				Services.Get<IDiverVisualEffects>().ShowToolInHands(_currTool);
				Services.Get<IDiverVisualEffects>().AnimateToolInHands(true);
			}
		}

		private void RepairCompleteActions()
		{
			Services.Get<IDiverMovementManager>().MoveDiverToNearestHarpoon();

			// hide tool
			Services.Get<IDiverVisualEffects>().ShowToolInHands(Enumerators.ToolsTypes.NONE);

			Services.Get<IDamageBubbles>().ReduceDamageCount();
		}

		#endregion
	
		#region TOOL_VS_THREAT

		private void ApplyToolOnThreat(Transform coll)
		{
			var collToolUse = GetThreatToolUseScript(coll);
			// -------------------------------------------------------
			if (collToolUse != null)
			{
				bool isUsingRightTool = collToolUse.IsRightTool(_currTool);
				if (isUsingRightTool)
				{
					Services.Get<IDiverVisualEffects>().ShowToolInHands(_currTool);

					Services.Get<IDiverMovementManager>().StartDiverCorrectionCourse(coll);
				}
			}
			// -------------------------------------------------------
		}

		private IToolUse GetThreatToolUseScript(Transform coll)
		{
			var baseColl = coll.GetComponent<ABaseCollider>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, baseColl);
	#endif
			#endregion

			Transform collParent = baseColl.Get_Parent();

			var collToolUse = collParent.GetComponent<IToolUse>();
		
			return collToolUse;
		}

		private void HandleThreat(object[] param)
		{
			// paran[0] - Transform coll
			// param[1] - AMonoBaseToolUse toolUse

			Transform coll = (Transform) param[0];
			var toolUse = (IToolUse) param[1];
		
			bool isHandleThreatStarted = toolUse.StartHandleThreat(_currTool, coll);
			if (isHandleThreatStarted)
			{
				// diver state!
				SetDiverState(Enumerators.DiverStates.HANDLING_THREAT);

				HandleThreatCompleted(coll);
			}
		}

		private void HandleThreatCompleted(Transform coll)
		{
			_timerManager.StartTimer(HandleThreatCompletedActions, coll,  1f);
		}

		protected virtual void HandleThreatCompletedActions(Transform coll)
		{
			if (!coll.CompareTag(Tags.Mine))
			{
				Services.Get<IDiverMovementManager>().MoveDiverToNearestHarpoon();
			}
		}

		#endregion

		#region FOR_CheckToolRelease

		protected Collider2D[] GetClickedColliders(Vector2 mousePos)
		{
			var layerMask = 1 << (int)Enumerators.Layers.DAMAGE_TYPE |
							1 << (int)Enumerators.Layers.THREAT;

			const float radius = 0.7f;

			Collider2D[] arrColls = Physics2D.OverlapCircleAll(mousePos, radius, layerMask);

			return arrColls;
		}

		protected Transform GetFirstClickedCollider(Collider2D[] arrColls)
		{
			return arrColls[0].transform;
		}

		#endregion
	


	}
}