﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Pirates_3.Common.Info;
using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public abstract class BaseInputManager : AMonoService
	{
		// FIELDS
		// -----------------------------------------------------------------------
		protected Transform _objDiver;
		protected readonly float _diverShiftX = 1f;
		protected static Enumerators.DiverStates _currDiverState = Enumerators.DiverStates.NONE;

		// -----------------------------------------------------------------------
		protected Transform _objHarpoonGunLeft;
		protected Transform _objForHarpoonLeft;

		protected Transform _objHarpoonGunRight;
		protected Transform _objForHarpoonRight;

		// -----------------------------------------------------------------------
		protected enum HarpponTypes
		{
			NONE,
			HARPOON_LEFT,
			HARPOON_RIGHT
		}
		protected static HarpponTypes _currHarpoonType = HarpponTypes.NONE;

		protected readonly Dictionary<HarpponTypes, Transform> _dictHarpoons =
			new Dictionary<HarpponTypes, Transform>();

		// -----------------------------------------------------------------------
		protected enum HarpoonStates
		{
			NONE,
			ROTATING,
			RELOADING
		}
		protected static HarpoonStates _currHarpoonState = HarpoonStates.NONE;

		// -----------------------------------------------------------------------
		protected static Enumerators.ToolsTypes _currTool = Enumerators.ToolsTypes.NONE;



		// UNITY
		private void OnDestroy()
		{
			// this things need to bee static to share same values over the scripts !!!
			_currDiverState = Enumerators.DiverStates.NONE;
			_currHarpoonType = HarpponTypes.NONE;
			_currHarpoonState = HarpoonStates.NONE;
			_currTool = Enumerators.ToolsTypes.NONE;
		}

	

		// A_SERVICES
		protected override void InitAwake()
		{
			InitBaseVariables();
			InitDictHarpoons();
		}



		// METHODS

		#region INIT

		private void InitBaseVariables()
		{
			_objDiver = transform.Find("/GameMenu/Diver");

			_objHarpoonGunLeft = transform.Find("/GameMenu/Submarine/Object/Harpoon_Guns/Harpoon_Gun_left");
			_objForHarpoonLeft = transform.Find("/GameMenu/Submarine/Object/Harpoon_Guns/Harpoon_Gun_left/Object/For_Harpoon");

			_objHarpoonGunRight = transform.Find("/GameMenu/Submarine/Object/Harpoon_Guns/Harpoon_Gun_right");
			_objForHarpoonRight = transform.Find("/GameMenu/Submarine/Object/Harpoon_Guns/Harpoon_Gun_right/Object/For_Harpoon");
		}

		private void InitDictHarpoons()
		{
			_dictHarpoons.Add(HarpponTypes.HARPOON_LEFT, _objHarpoonGunLeft);
			_dictHarpoons.Add(HarpponTypes.HARPOON_RIGHT, _objHarpoonGunRight);
		}

		protected bool IsPointerOverUIObject()
		{
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
			{
				position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
			};

			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

			return results.Count > 0;
		}

		#endregion

		#region DIVER_STATE

		protected void SetDiverState(Enumerators.DiverStates myState)
		{
			_currDiverState = myState;

			CheckDiverState();
		}

		private void CheckDiverState()
		{
			if (_currDiverState == Enumerators.DiverStates.HARPOON_GUN)
			{
				Services.Get<IDiverVisualEffects>().ShowHarpoonLabel(true);
			}
			else
			{
				Services.Get<IDiverVisualEffects>().ShowHarpoonLabel(false);
			}
		}

		#endregion

		protected void SetTool(Enumerators.ToolsTypes toolType)
		{
			_currTool = toolType;
		}

		protected void SetHarpoonGunState(HarpoonStates harpoonState)
		{
			_currHarpoonState = harpoonState;
		}

		protected void SetHarpoonGunType(HarpponTypes harpoontype)
		{
			_currHarpoonType = harpoontype;
		}



	}
}