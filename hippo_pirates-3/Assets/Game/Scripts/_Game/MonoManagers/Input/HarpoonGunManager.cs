﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using DG.Tweening;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class HarpoonGunManager : BaseInputManager, IHarpoonGunManager
	{
		// FIELDS
		// -------------------------------------------------------
		protected readonly float _rotateSpeed = 0.2f;
		protected readonly float _diverReactionSpeed = 0.1f;

		// -------------------------------------------------------
		protected float _delayRelaoad;
		protected readonly float _normalReload = 0.5f;
		protected float _fastReload;

		// -------------------------------------------------------
		protected bool _isBonusReload;

		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitStart()
		{
			_delayRelaoad = _normalReload;
			_fastReload = _normalReload / 2f;
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IHarpoonGunManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IHarpoonGunManager>();
		}



		// UNITY
		private void Update()
		{
			CheckMouseClick();
		}



		// INTERFACES
		public void ResetHarpoonGunsRotation()
		{
			_objHarpoonGunLeft.DOLocalRotate(Vector3.zero, _rotateSpeed);
			_objHarpoonGunRight.DOLocalRotate(Vector3.zero, _rotateSpeed);
		}

		//
		public void ActivateBonusRepairRate()
		{
			_delayRelaoad = _fastReload;
		}

		public void DeactivateBonusRepairRate()
		{
			_delayRelaoad = _normalReload;
		}



		// METHODS
		#region MOUSE

		private void CheckMouseClick()
		{
			if (Input.GetMouseButtonDown(0))
			{
				ActionMouseClick();
			}
		}

		private void ActionMouseClick()
		{
			if ( IsCanShoot() && 
				!IsBlocked() ) 
			{
				Vector2 mousePos = GetMousePosition();
				Transform harpoon = _dictHarpoons[_currHarpoonType];

				SetRotation(harpoon, mousePos);
				_timerManager.StartTimer(CorrectDiverPosition, _diverReactionSpeed);
				_timerManager.StartTimer(Shoot, harpoon, _rotateSpeed);
			}
		}

		private bool IsCanShoot()
		{
			return (_currDiverState == Enumerators.DiverStates.HARPOON_GUN &&
					!IsPointerOverUIObject() &&
					_currHarpoonState == HarpoonStates.NONE);
		}

		private bool IsBlocked()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}

		#endregion

		#region ROTATION

		protected void SetRotation(Transform go, Vector2 myPos)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			SetHarpoonGunState(HarpoonStates.ROTATING);

			Vector2 direction = GetNormalizedDirection(go, myPos);
			Vector3 finalRotation = GetFinalRotation(direction);

			go.DOLocalRotate(finalRotation, _rotateSpeed);
		}

		private Vector2 GetNormalizedDirection(Transform go, Vector2 myPos)
		{
			Vector2 direction = Vector2.zero;

			if (_currHarpoonType ==  HarpponTypes.HARPOON_LEFT)
			{
				direction = (Vector2)go.position - myPos;
			}
			else if (_currHarpoonType == HarpponTypes.HARPOON_RIGHT)
			{
				direction = myPos - (Vector2)go.position;
			}

			direction.Normalize();

			return direction;
		}

		private Vector3 GetFinalRotation(Vector2 direction)
		{
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			return new Vector3(0, 0, angle);
		}

		#endregion

		#region CORRECT_DIVER_POSITION

		protected void CorrectDiverPosition()
		{
			Vector2 tempPos = Vector2.zero;

			if (_currHarpoonType == HarpponTypes.HARPOON_LEFT)
			{
				tempPos = CorrectDiverPosHarpoonGunLeft();
			}
			else if (_currHarpoonType == HarpponTypes.HARPOON_RIGHT)
			{
				tempPos = CorrectDiverPosHarpoonGunRight();
			}

			_objDiver.DOMove(tempPos, _rotateSpeed);
		}

		private Vector2 CorrectDiverPosHarpoonGunLeft()
		{
			Vector2 tempPos = Vector2.zero;

			if (_objForHarpoonLeft.position.x <= _objHarpoonGunLeft.position.x)
			{
				tempPos = new Vector2(_objHarpoonGunLeft.position.x + _diverShiftX, _objHarpoonGunLeft.position.y);
				Services.Get<IDiverMovementManager>().FlipDiverLeft();
			}
			else if (_objForHarpoonLeft.position.x > _objHarpoonGunLeft.position.x)
			{
				tempPos = new Vector2(_objHarpoonGunLeft.position.x - _diverShiftX, _objHarpoonGunLeft.position.y);
				Services.Get<IDiverMovementManager>().FlipDiverRight();
			}

			return tempPos;
		}

		private Vector2 CorrectDiverPosHarpoonGunRight()
		{
			Vector2 tempPos = Vector2.zero;

			if (_objForHarpoonRight.position.x <= _objHarpoonGunRight.position.x)
			{
				tempPos = new Vector2(_objHarpoonGunRight.position.x + _diverShiftX, _objHarpoonGunRight.position.y);
				Services.Get<IDiverMovementManager>().FlipDiverLeft();
			}
			else if (_objForHarpoonRight.position.x > _objHarpoonGunRight.position.x)
			{
				tempPos = new Vector2(_objHarpoonGunRight.position.x - _diverShiftX, _objHarpoonGunRight.position.y);
				Services.Get<IDiverMovementManager>().FlipDiverRight();
			}

			return tempPos;
		}

		#endregion

		#region SHOOT 

		protected void Shoot(Transform myHarpoon)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myHarpoon);
	#endif
			#endregion

			StartReload();

			_soundManager.PlaySound(SoundsGameplayFX.Shooting.harpoonGunShot);
		
			Services.Get<IHarpoonPool>().ShowObject(myHarpoon, GetHarpoonType() );
		}

		protected void StartReload()
		{
			SetHarpoonGunState(HarpoonStates.RELOADING);

			Services.Get<IDiverVisualEffects>().ShowReloadBar(_delayRelaoad);
			_timerManager.StartTimer(StopReload, _delayRelaoad);
		}

		protected void StopReload()       // need for Invoke()
		{
			SetHarpoonGunState(HarpoonStates.NONE);
		}

		private int GetHarpoonType()
		{
			int tempHarpType = 0;

			switch (_currHarpoonType)
			{
				case HarpponTypes.HARPOON_LEFT:
					tempHarpType = 0;
					break;

				case HarpponTypes.HARPOON_RIGHT:
					tempHarpType = 1;
					break;

				default:
					#region DEBUG
	#if UNITY_EDITOR
					Debug.Log("Unknown TYPE !");
	#endif
					#endregion
					break;
			}

			return tempHarpType;
		}

		#endregion

		#region MISC

		private Vector2 GetMousePosition()
		{
			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			return mousePos;
		}
	
		#endregion
	

	}
}