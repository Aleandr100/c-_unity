﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using System.Collections;
using UnityEngine;
using DG.Tweening;
using Pirates_3.Common.Info;
using Pirates_3.Common;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class DiverMovementMamager : BaseInputManager, IDiverMovementManager
	{
		// FIELDS
		// ----------------------------------------
		//
		[SerializeField] private Transform _submarine;

		// ----------------------------------------
		//
		private const float _speed = 5f;

		// ----------------------------------------
		//
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitStart()
		{
			// start point !!!
			_timerManager.StartTimer(MoveDiverToNearestHarpoon, 1f);
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IDiverMovementManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IDiverMovementManager>();
		}



		// INTERFACES
		public float MoveDiver(Vector2 pos)
		{
			StopDoMove();
			CheckDiverRotationAngle(pos);
			CheckDiverDirection(pos);
			Move(pos);

			float moveTime = GetMoveTime(pos);

			// diver state!
			SetDiverState(Enumerators.DiverStates.MOVE);

			Services.Get<IHarpoonGunManager>().ResetHarpoonGunsRotation();

			return moveTime;
		}

		public void MoveDiverToNearestHarpoon()
		{
			HarpponTypes nearestHarpoon = GetNearestHarpoon();
			Vector2 pos = GetDiverOnHarpoonGunPosition(nearestHarpoon);

			StopDoMove();
			CheckDiverRotationAngle(pos);
			CheckDiverDirection(pos);
			Move(pos);
		
			// kostil - to flip Diver on harpoon, because he often turn not in right side !!!
			FlipDiverOnEndHarpoonGunPos(nearestHarpoon);

			float moveTime = GetMoveTime(pos);

			_timerManager.StartTimer(SetHarpoonGunType, nearestHarpoon, moveTime);

			// diver state!
			_timerManager.StartTimer(SetDiverState, Enumerators.DiverStates.HARPOON_GUN, moveTime);
		}

		public void ActivateDiverStunEffect()
		{
			SetDiverState(Enumerators.DiverStates.STUNNED);

			MoveDiver(_submarine.position);

			float stunTime = 5f;
			Services.Get<IDiverVisualEffects>().ShowDiverStars(stunTime);
			_timerManager.StartTimer(DeactivateDiverStunEffect, stunTime + 1f);
		}

		public void FlipDiverLeft()
		{
			FlipLeft();
		}

		public void FlipDiverRight()
		{
			FlipRight();
		}

		public void StartDiverCorrectionCourse(Transform coll)
		{
			StartCoroutine( CorrectionCourseCircle(coll) );
		}



		// METHODS
	
		#region MOVEMENT

		private void StopDoMove()
		{
			// bug fixed - Diver teleportation !!!
			DOTween.Kill(_objDiver);
		}

		private void Move(Vector2 mousePos)
		{
			float moveTime = GetMoveTime(mousePos);
			_objDiver.DOMove(mousePos, moveTime).OnComplete(ResetDiverRotation);
		}

		private float GetMoveTime(Vector2 pos)
		{
			float moveTime = Vector2.Distance(_objDiver.position, pos) / _speed;

			return moveTime;
		}

		#endregion

		#region ROTATE

		private void CheckDiverRotationAngle(Vector2 mousePos)
		{
			const float angleZLeft = 35f;
			const float angleZRight = 325f;

			if (mousePos.x < _objDiver.position.x)
			{
				Rotate(angleZLeft);
			}
			else if (mousePos.x >= _objDiver.position.x)
			{
				Rotate(angleZRight);
			}
		}

		private void ResetDiverRotation()
		{
			const float angleZ = 0f;
			Rotate(angleZ);
		}

		private void Rotate(float angle)
		{
			const float rotateSpeed = 0.3f;

			Vector3 rotateAngle = new Vector3(_objDiver.localRotation.x, _objDiver.localRotation.y, angle);
			_objDiver.DORotate(rotateAngle, rotateSpeed);
		}

		#endregion

		#region FLIP

		private void CheckDiverDirection(Vector2 mousePos)
		{
			if (mousePos.x < _objDiver.position.x)
			{
				FlipLeft();
			}
			else if (mousePos.x >= _objDiver.position.x)
			{
				FlipRight();
			}
		}

		private void FlipLeft()
		{
			if (_objDiver.localScale.x > 0)
			{
				Flip();
			}
		}

		private void FlipRight()
		{
			if (_objDiver.localScale.x < 0)
			{
				Flip();
			}
		}

		private void Flip()
		{
			_objDiver.transform.localScale = new Vector2(-1 * _objDiver.transform.localScale.x, _objDiver.transform.localScale.y);
		}

		private void FlipDiverOnEndHarpoonGunPos(HarpponTypes harpponType)
		{
			float delay = GetMoveTime(_dictHarpoons[harpponType].position);
			float correctedDelay = delay * 1.2f;

			if (harpponType == HarpponTypes.HARPOON_LEFT)
			{
				_timerManager.StartTimer(FlipLeft, correctedDelay);
			}
			else if (harpponType == HarpponTypes.HARPOON_RIGHT)
			{
				_timerManager.StartTimer(FlipRight, correctedDelay);
			}
		}

		#endregion

		#region HARPOONS

		private HarpponTypes GetNearestHarpoon()
		{
			float distLeft = Vector2.Distance(_objDiver.position, _objHarpoonGunLeft.position);
			float distRight = Vector2.Distance(_objDiver.position, _objHarpoonGunRight.position);

			return (distLeft < distRight) ? (HarpponTypes.HARPOON_LEFT) : (HarpponTypes.HARPOON_RIGHT);
		}

		#endregion

		#region DIVER_ON_HARPPON_GUN_POS

		private Vector2 GetDiverOnHarpoonGunPosition(HarpponTypes harpoonType)
		{
			Vector2 finalPos = Vector2.zero;

			if (harpoonType == HarpponTypes.HARPOON_LEFT)
			{
				finalPos = GetDiverOnHarpoonGunLeftPos();
			}
			else if (harpoonType == HarpponTypes.HARPOON_RIGHT)
			{
				finalPos = GetDiverOnHarpoonRightGunPos();
			}
			else
			{
				#region DEBUG
	#if UNITY_EDITOR
			Debug.Log("Unknown Harpoon Type !");
	#endif
				#endregion
			}

			return finalPos;
		}

		private Vector2 GetDiverOnHarpoonGunLeftPos()
		{
			Vector2 tempPos = Vector2.zero;

			if (_objForHarpoonLeft.position.x <= _objHarpoonGunLeft.position.x)
			{
				tempPos = new Vector2(
					_objHarpoonGunLeft.position.x + _diverShiftX,
					_objHarpoonGunLeft.position.y);
			}
			else if (_objForHarpoonLeft.position.x > _objHarpoonGunLeft.position.x)
			{
				tempPos = new Vector2(
					_objHarpoonGunLeft.position.x - _diverShiftX,
					_objHarpoonGunLeft.position.y);
			}

			return tempPos;
		}

		private Vector2 GetDiverOnHarpoonRightGunPos()
		{
			Vector2 tempPos = Vector2.zero;

			if (_objForHarpoonRight.position.x <= _objHarpoonGunRight.position.x)
			{
				tempPos = new Vector2(
					_objHarpoonGunRight.position.x + _diverShiftX,
					_objHarpoonGunRight.position.y);
			}
			else if (_objForHarpoonRight.position.x > _objHarpoonGunRight.position.x)
			{
				tempPos = new Vector2(
					_objHarpoonGunRight.position.x - _diverShiftX,
					_objHarpoonGunRight.position.y);
			}

			return tempPos;
		}

		#endregion

		#region STUN_EFFECT

		private void DeactivateDiverStunEffect()
		{
			MoveDiverToNearestHarpoon();
		}

		#endregion

		#region CORRECTION_COURSE

		private IEnumerator CorrectionCourseCircle(Transform coll)
		{
			const float delay = 0.2f;
		
			while (true)
			{
				if (coll != null)
				{
					bool isNeedExit = CheckColl(coll);

					if (isNeedExit) {
						break; }
				}
				else
				{
					MoveDiverToNearestHarpoon();
				}
			
				yield return new WaitForSeconds(delay);
			}
		}

		private bool CheckColl(Transform coll)
		{
			if ( IsDiverCloseEnoughToThreat(coll) )
			{
				if ( IsTutorial() ) {
					Services.Get<IToolsManagerTutorial>().TryHandleThreat(coll); }
				else {
					Services.Get<IToolsManager>().TryHandleThreat(coll); }

				return true;
			}
		
			MoveDiver(coll.position);
			return false;
		}

		private bool IsDiverCloseEnoughToThreat(Transform coll)
		{
			const float deltaDistance = 2f;
			float distance = Vector2.Distance(_objDiver.position, coll.position);

			return (distance <= deltaDistance);
		}

		#endregion

		private bool IsTutorial()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}


	}
}