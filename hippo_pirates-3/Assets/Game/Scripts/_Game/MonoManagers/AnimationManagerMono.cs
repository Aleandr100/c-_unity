﻿
using Pirates_3.Common;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class AnimationManagerMono : AMonoService, IAnimationManagerMono
	{
		// FIELDS
		// ------------------------------------
		// dependences
		private IAnimationManager _animationManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(IAnimationManager animationManager)
		{
			_animationManager = animationManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IAnimationManagerMono>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IAnimationManagerMono>();
		}



		// INTERFACES
		//
		public void SetParameterBool(Animator myAnimator, string myParam, bool myBool)
		{
			_animationManager.SetParameterBool(myAnimator, myParam, myBool);
		}

		public void SetParameterBool(object[] param)
		{
			_animationManager.SetParameterBool(param);
		}

		//
		public void SetParameterInt(Animator myAnimator, string myParam, int myInt)
		{
			_animationManager.SetParameterInt(myAnimator, myParam, myInt);
		}

		public void SetParameterInt(object[] param)
		{
			_animationManager.SetParameterInt(param);
		}

		//
		public void SetParameterTrigger(Animator myAnimator, string myTrig)
		{
			_animationManager.SetParameterTrigger(myAnimator, myTrig);
		}

		public void SetParameterTrigger(object[] param)
		{
			_animationManager.SetParameterTrigger(param);
		}



	}
}