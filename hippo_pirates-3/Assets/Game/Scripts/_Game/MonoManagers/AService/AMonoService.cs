﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections;

namespace Pirates_3.Common
{
	public abstract class AMonoService : MonoBehaviour, IMonoService
	{
    


		// UNITY
		protected void Awake()
		{
			RegisterService();
			InitAwake();
		}

		protected void Start()
		{
			AssertVariables();
			InitStart();
		}

		protected void OnDisable()
		{
			DeregisterService();
		}



		// ABSTRACT
		protected abstract void InitAwake();

		protected abstract void InitStart();

		protected abstract void AssertVariables();

		protected abstract void RegisterService();

		protected abstract void DeregisterService();



		// PROTECTED
		#region SLOW_UPDATE

		protected IEnumerator SlowUpdateCoroutine()
		{
			const float timer = 0.333f;

			while (true)
			{
				yield return new WaitForSeconds(timer);

				SlowUpdate();
			}
		}

		protected virtual void SlowUpdate()
		{
		}

		protected void StartSlowUpdate()
		{
			StartCoroutine(SlowUpdateCoroutine());
		}

		#endregion

		#region INVOKE

		protected void SafeStartInvoke(string methodName, float myDelay)
		{
			if (!IsInvoking(methodName))
			{
				Invoke(methodName, myDelay);
			}
		}

		protected void SafeStartInvokeRepeating(string methodName, float myDelay, float timeRepeat)
		{
			if (!IsInvoking(methodName))
			{
				InvokeRepeating(methodName, myDelay, timeRepeat);
			}
		}

		protected void SafeCancelInvoke(string methodName)
		{
			if (IsInvoking(methodName))
			{
				CancelInvoke(methodName);
			}
		}

		#endregion



	}
}