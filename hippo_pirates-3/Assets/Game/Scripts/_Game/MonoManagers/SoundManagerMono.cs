﻿using Pirates_3.Common;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class SoundManagerMono : AMonoService, ISoundManagerMono
	{
		// FIELDS
		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(this.GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ISoundManagerMono>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ISoundManagerMono>();
		}



		// INTERFACES
		public void PlaySound(string strSound)
		{
			_soundManager.PlaySound(strSound);
		}

		public void PlaySoundRepeating(string strSound)
		{
			_soundManager.PlaySoundRepeating(strSound);
		}

		public void StopSound(string strSound)
		{
			_soundManager.StopSound(strSound);
		}

		public void PlayMusic(string strMusic)
		{
			_soundManager.PlayMusic(strMusic);
		}

		public float GetSoundLenght(string strSound)
		{
			return _soundManager.GetSoundLenght(strSound);
		}

		public string GetRandomSound(string[] someArray)
		{
			return _soundManager.GetRandomSound(someArray);
		}

		public void StopAllRaccoonSpeeches()
		{
			_soundManager.StopAllRaccoonSpeeches();
		}

		public void SetMisicVolume()
		{
			_soundManager.SetMisicVolume();
		}



	}
}