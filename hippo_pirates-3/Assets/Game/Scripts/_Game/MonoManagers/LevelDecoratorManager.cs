﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class LevelDecoratorManager : AMonoService, ILevelDecoratorManager
	{
		// FIELDS
		// -------------------------------------
		//
		[Header("[OBJECTS]")]
		[SerializeField] private Transform _objDiver;
		[SerializeField] private Transform _objSubmarine;
		[SerializeField] private Transform _objForDiver;

		// -------------------------------------
		//
		[Header("[ANIMATORS]")]
		[SerializeField] private Animator _animatorSubmarine;
		[SerializeField] private Animator _animatorBalloons;
		[SerializeField] private Animator _animatorScrew;
		[SerializeField] private Animator _animatorScrewWaves;

		// -------------------------------------
		//
		[Header("[BACKGROUNDS]")]
		[SerializeField] private GameObject[] _backgrounds;

		// -------------------------------------
		//
		[Header("[RIBKI]")]
		[SerializeField] private GameObject _objRibkiAll;

		// -------------------------------------
		//
		private bool _isLoosing;

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}

	

		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			CheckLevel();
			Services.Get<ISubmarineVisualEffects>().StartScrewRotate();

			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgScrewRotate);
			_soundManager.PlaySoundRepeating(SoundsGameplayFX.Backgrounds.bgUnderwater);

			CheckLevelForScrewWaves();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ILevelDecoratorManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ILevelDecoratorManager>();
		}


	
		// INTERFACES
		public void StartLooseSequence()
		{
			if (!_isLoosing)
			{
				_isLoosing = true;

				Services.Get<ISubmarineVisualEffects>().StopScrewRotate();
				Services.Get<ISubmarineVisualEffects>().OpenHatch();

				//
				_soundManager.StopSound(SoundsGameplayFX.Misc.alarmCriticalDamage);

				PlayLooseBallonsSounds();

				//
				_timerManager.StartTimer(
					Services.Get<ISubmarineVisualEffects>().BlowBalloons, 1f);

				SetDiverPosition();

				Rigidbody2D subRigid = _objSubmarine.GetComponent<Rigidbody2D>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, subRigid);
	#endif
				#endregion

				_timerManager.StartTimer(SubmarineMoveUp, subRigid, 2f);
				_timerManager.StartTimer(LoadLooseScene, 6f);
			}
		}



		// METHODS
		private void CheckLevel()
		{
			// ===========================================================
			// 
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				Services.Get<ICameraManager>().SetCameraHorizontalLevel();

				SetSubmarinePos( new Vector2(1.3f, 1f) );
				SetDiverPosition( new Vector2(2.65f, 1.2f) );
				SetBackground();
				ShowRibkiAllAnimations(true);
			}

			// ===========================================================
			// 
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				Services.Get<ICameraManager>().SetCameraVerticalLevel();

				SetSubmarinePos( new Vector2(0f, -1.55f) );
				SetDiverPosition( new Vector2(1.35f, -1.35f) );
				SetBackground();
				ShowRibkiAllAnimations(false);
			}
			// ===========================================================
		}

		private void SetSubmarinePos(Vector2 pos)
		{
			_objSubmarine.transform.localPosition = pos;
		}

		private void SetDiverPosition(Vector2 pos)
		{
			_objDiver.transform.localPosition = pos;
		}

		private void SetBackground()
		{
			int num = (int)LevelsController.Get_CurrentLevelSubType();
			GameObject bg = _backgrounds[num];

			for (int i = 0; i < _backgrounds.Length; i++)
			{
				if (_backgrounds[i] == bg)
				{
					_backgrounds[i].SetActive(true);
				}
				else
				{
					_backgrounds[i].SetActive(false);
				}
			}
		}

		private void CheckLevelForScrewWaves()
		{
			if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.HORIZONTAL)
			{
				SetScrewWavesAnimations( (int)ParamInts.ScrewWaves.Directions.LEFT );
			}
			else if (LevelsController.Get_CurrentLevelType() == Enumerators.LevelsTypes.VERTICAL)
			{
				SetScrewWavesAnimations( (int)ParamInts.ScrewWaves.Directions.UP );
			}
		}

		private void SetScrewWavesAnimations(int myValue)
		{
			_animationManager.SetParameterInt(_animatorScrewWaves, ParamInts.ScrewWaves.direction, myValue);
		}

		private void SetDiverPosition()
		{
			_objDiver.SetParent(_objForDiver);
		}

		private void LoadLooseScene()
		{
			Services.Get<IGameplayNextLevelController>().SetWeAreLoose();
			Services.Get<IGameplayNextLevelController>().ShowPanel();
		}

		private void SubmarineMoveUp(Rigidbody2D myRigid)
		{
			// pre-conditrions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myRigid);
	#endif
			#endregion

			myRigid.isKinematic = false;
			myRigid.gravityScale = -1;
		}

		private void ShowRibkiAllAnimations(bool myValue)
		{
			_objRibkiAll.SetActive(myValue);
		}

		private void PlayLooseBallonsSounds()
		{
			_timerManager.StartTimer(
				_soundManager.PlaySound, SoundsGameplayFX.Misc.looseBalloons, 1f);

			_timerManager.StartTimer(
				_soundManager.PlaySound, SoundsGameplayFX.Misc.looseBalloons, 1.1f);

			_timerManager.StartTimer(
				_soundManager.PlaySound, SoundsGameplayFX.Misc.looseBalloons, 1.3f);

			_timerManager.StartTimer(
				_soundManager.PlaySound,SoundsGameplayFX.Misc.looseGoesUp, 1.5f);
		}




	}
}