﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class FakeToolButtonManager : AMonoService, IFakeToolButtonManager
	{
		// FIELDS
		// -------------------------------------------------------
		[Header("FAKE_BUTTON")]
		[SerializeField] private GameObject _objFake;

		private Transform _transformFake;
		private SpriteRenderer _srFake;

		// -------------------------------------------------------
		[Header("TOOLS_BUTTONS")]
		[SerializeField] private Transform _buttonScissors;
		[SerializeField] private Transform _buttonMagnet;
		[SerializeField] private Transform _ButtonHammer;
		[SerializeField] private Transform _buttonWelding;
	
		private readonly Dictionary<Enumerators.ToolsTypes, Transform> _dictButtons = 
			new Dictionary<Enumerators.ToolsTypes, Transform>();

		// -------------------------------------------------------
		[Header("SPRITES")]
		[SerializeField] private Sprite _spriteScissors;
		[SerializeField] private Sprite _spriteMagnet;
		[SerializeField] private Sprite _spriteHammer;
		[SerializeField] private Sprite _spriteWelding;
	
		private readonly Dictionary<Enumerators.ToolsTypes, Sprite> _dictToolsSprites =
			new Dictionary<Enumerators.ToolsTypes, Sprite>();

	

		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			InitVariables();
			InitDictButtons();
			InitDictToolsSprites();
		}

		protected override void AssertVariables()
		{
			// can't apply auto-assert here!
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _objFake);

			Assert.AreNotEqual(null, _buttonScissors);
			Assert.AreNotEqual(null, _buttonMagnet);
			Assert.AreNotEqual(null, _ButtonHammer);
			Assert.AreNotEqual(null, _buttonWelding);

			Assert.AreNotEqual(null, _spriteScissors);
			Assert.AreNotEqual(null, _spriteMagnet);
			Assert.AreNotEqual(null, _spriteHammer);
			Assert.AreNotEqual(null, _spriteWelding);
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IFakeToolButtonManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IFakeToolButtonManager>();
		}



		// UNITY
		protected void Update()
		{
			CheckMouseHold();
			CheckMouseRelease();
		}
	


		// INTERFACES
		public void Activate(Enumerators.ToolsTypes toolType)
		{
			SetPosition(toolType);
			Show(true);
			ChangeSprite(toolType);
		}



		// METHODS
		#region INIT

		private void InitVariables()
		{
			_transformFake = _objFake.transform;

			_srFake = _objFake.GetComponent<SpriteRenderer>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _srFake);
	#endif
			#endregion
		}

		private void InitDictButtons()
		{
			_dictButtons.Add(Enumerators.ToolsTypes.SCISSORS, _buttonScissors);
			_dictButtons.Add(Enumerators.ToolsTypes.MAGNET, _buttonMagnet);
			_dictButtons.Add(Enumerators.ToolsTypes.HAMMER, _ButtonHammer);
			_dictButtons.Add(Enumerators.ToolsTypes.WELDING, _buttonWelding);
		}

		private void InitDictToolsSprites()
		{
			_dictToolsSprites.Add(Enumerators.ToolsTypes.SCISSORS, _spriteScissors);
			_dictToolsSprites.Add(Enumerators.ToolsTypes.MAGNET, _spriteMagnet);
			_dictToolsSprites.Add(Enumerators.ToolsTypes.HAMMER, _spriteHammer);
			_dictToolsSprites.Add(Enumerators.ToolsTypes.WELDING, _spriteWelding);
		}

		#endregion
	
		#region MOUSE_HOLD

		private void CheckMouseHold()
		{
			if (Input.GetMouseButton(0))
			{
				if (_objFake.activeInHierarchy)
				{
					ActionMouseHold();
				}
			}
		}

		private void ActionMouseHold()
		{
			MoveToCucsor();
		}

		private void MoveToCucsor()
		{
			_transformFake.position = GetMousePosition();
		}

		#endregion

		#region MOUSE_RELEASE

		private void CheckMouseRelease()
		{
			if (Input.GetMouseButtonUp(0))
			{
				if (_objFake.activeInHierarchy)
				{
					ActionMouseRelease();
				}
			}
		}

		protected virtual void ActionMouseRelease()
		{
			Show(false);

			Vector2 mousePos = GetMousePosition();
			Services.Get<IToolsManager>().HandleFakeToolButtonMouseRelease(mousePos);
		}

		#endregion

		#region MISC

		protected Vector2 GetMousePosition()
		{
			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			return mousePos;
		}

		protected void Show(bool boolValue)
		{
			_objFake.SetActive(boolValue);
		}

		private void ChangeSprite(Enumerators.ToolsTypes toolType)
		{
			_srFake.sprite = _dictToolsSprites[toolType];
		}

		private void SetPosition(Enumerators.ToolsTypes toolType)
		{
			_transformFake.position = _dictButtons[toolType].position;
		}
	
		#endregion
	


	}
}