﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class BonusRepairRateManager : AMonoService, IBonusRepairRateManager
	{
		// FIELDS
		// -----------------------------------
		// preferences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}


	
		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IBonusRepairRateManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IBonusRepairRateManager>();
		}



		// INTERFACES
		public void Activate()
		{
			Services.Get<IToolsManager>().ActivateBonusRepairRate();
			Services.Get<IHarpoonGunManager>().ActivateBonusRepairRate();
			Services.Get<IAvatarsManager>().ActivateBonusRepairRate();

			const float timerDeactivate = 15f;
			_timerManager.StartTimer(Deactivate, timerDeactivate);
		}

	
	
		// METHODS
		private void Deactivate()
		{
			Services.Get<IToolsManager>().DeactivateBonusRepairRate();
			Services.Get<IHarpoonGunManager>().DeactivateBonusRepairRate();
		}
	


	}
}