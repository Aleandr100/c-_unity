﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Menu.Gameplay.MonoScripts;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class ColliderManager : AMonoService, IColliderManager
	{
	
	

		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IColliderManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IColliderManager>();
		}
	


		// INTERFACES
		public void CheckThreat(GameObject coll)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, coll);
	#endif
			#endregion

			//
			if (coll.layer == (int)Enumerators.Layers.THREAT)
			{
				// ===========================================================
				// TRASH
				if (coll.CompareTag(Tags.TrashSmall))
				{
					Services.Get<IDamageManager>().SetRandomDamage(coll, Enumerators.DamageTypes.HULL_TORN, 1);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.TrashBig))
				{
					Services.Get<IDamageManager>().SetRandomDamage(coll, Enumerators.DamageTypes.DENT_BIG, 1);
				}
				// ===========================================================
				// FISH
				else if (coll.CompareTag(Tags.FishSmall))
				{
					//  --- none ---
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.FishBig))
				{
					SubmarineHitFishBig(coll);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.FishFast))
				{
					// --- none ---
				}
				// ===========================================================
				// SCREW BREAK
				else if (coll.CompareTag(Tags.Net))
				{
					Services.Get<IDamageManager>().SetScrewDamage(coll, Enumerators.DamageTypes.SCREW_NET);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.Rope) ||
					coll.CompareTag(Tags.CutRope))
				{
					Services.Get<IDamageManager>().SetScrewDamage(coll, Enumerators.DamageTypes.SCREW_ROPE);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.Seaweed))
				{
					Services.Get<IDamageManager>().SetScrewDamage(coll, Enumerators.DamageTypes.SCREW_SEAWEED);
				}
				// ===========================================================
				// OTHERS
				else if (coll.CompareTag(Tags.Rock))
				{
					SubmarineHitRock(coll);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.Mine))
				{
					SubmarineHitMine(coll);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.Chain))
				{
					Services.Get<IDamageManager>().SetDirectDamage(coll, Enumerators.DamageTypes.SCRATCH, 0);
				}
				// ===========================================================
				//
				else if (coll.CompareTag(Tags.CutChain))
				{
					int secNum = 4;
					for (int i = 0; i < secNum; i++)
					{
						Services.Get<IDamageManager>().SetDirectDamage(coll, Enumerators.DamageTypes.HULL_TORN, i);
					}
				}
				// ===========================================================
			}
		}
	


		// METHODS
		private void SubmarineHitFishBig(GameObject go)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			// ===========================================================
			// 
			int rnd = Random.Range(0, 3);
			if (rnd == 0)
			{
				Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.DENT_SMALL, 3);
			}
			else if (rnd == 1)
			{
				Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.DENT_BIG, 1);
			}
			else if (rnd == 2)
			{
				Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.BOTTOM_BREAK, 1);
			}
			// ===========================================================
		}

		private void SubmarineHitRock(GameObject go)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			// Assert.AreNotEqual(null, go);
	#endif
			#endregion

			// ===========================================================
			// 
			int rnd = Random.Range(0, 2);
			if (rnd == 0)
			{
				var bottomBreak = Services.Get<IDamageManager>().Get_BottomBreakList() [0];

				var dt = bottomBreak.GetComponent<ABaseDamageType>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, dt);
	#endif
				#endregion

				if (dt.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
				{
					Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.BOTTOM_BREAK, 1);
				}
				else if (dt.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.ON)
				{
					Services.Get<IDamageManager>().SetDirectDamage(go, Enumerators.DamageTypes.SCRATCH, 1);
				}
			}
			// ===========================================================
			// 
			else if (rnd == 1)
			{
				var scratch = Services.Get<IDamageManager>().Get_ScratchList() [1];

				var dt = scratch.GetComponent<ABaseDamageType>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, dt);
	#endif
				#endregion

				if (dt.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
				{
					Services.Get<IDamageManager>().SetDirectDamage(go, Enumerators.DamageTypes.SCRATCH, 1);
				}
				else if (dt.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.ON)
				{
					Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.BOTTOM_BREAK, 1);
				}
			}
			// ===========================================================
		}

		private void SubmarineHitMine(GameObject go)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			// ===========================================================
			// 
			int rnd = Random.Range(0, 2);

			if (rnd == 0)
			{
				var bottomBreak = Services.Get<IDamageManager>().Get_BottomBreakList() [0];

				var dt = bottomBreak.GetComponent<ABaseDamageType>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, dt);
	#endif
				#endregion

				if (dt.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.OFF)
				{
					Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.BOTTOM_BREAK, 1);
				}
				else if (dt.Get_CurrentDamageCondition() == ABaseDamageType.DamageCond.ON)
				{
					Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.HOLE, 1);
				}
			}
			// ===========================================================
			// 
			else if (rnd == 1)
			{
				Services.Get<IDamageManager>().SetRandomDamage(go, Enumerators.DamageTypes.HOLE, 1);
			}
			// ===========================================================
		}

	
	
	}
}