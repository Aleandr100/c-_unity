﻿using System.Collections;
using Pirates_3.Common;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class TimerManagerMono : AMonoService, ITimerManagerMono
	{


		// FIELDS
		public delegate void OnTimerVoid();
		public delegate void OnTimer<in T>(T param);
		public delegate void OnTimerObjects<in T>(T[] param);



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ITimerManagerMono>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ITimerManagerMono>();
		}



		// INTERFACE
		public void StartTimer(OnTimerVoid handler, float delay)
		{
			StartCoroutine(DelayedMethodVoid(handler, delay));
		}

		public void StartTimer<T>(OnTimer<T> handler, T param, float delay)
		{
			StartCoroutine(DelayedMethodT(handler, param, delay));
		}

		public void StartTimer<T>(OnTimerObjects<T> handler, T[] param, float delay)
		{
			StartCoroutine( DelayedMethodObjects(handler, param, delay) );
		}



		// METHODS
		private IEnumerator DelayedMethodVoid(OnTimerVoid handler, float delay)
		{
			yield return new WaitForSeconds(delay);

			handler();
		}

		private IEnumerator DelayedMethodT<T>(OnTimer<T> handler, T param, float delay)
		{
			yield return new WaitForSeconds(delay);

			handler(param);
		}

		private IEnumerator DelayedMethodObjects<T>(OnTimerObjects<T> handler, T[] param, float delay)
		{
			yield return new WaitForSeconds(delay);

			handler(param);
		}




	}
}