﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using UnityEngine.UI;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class SubmarineHpManager : AMonoService, ISubmarineHpManager
	{
		// FIELDS
		// -------------------------------------
		//
		[SerializeField] private Image _progressBar;

		// -------------------------------------
		//
		private readonly int _startHp = 2000;		// 2000

		private int _currHp;
		private int _dmgPerSec;

		// -------------------------------------
		//
		private readonly Dictionary<Enumerators.DamageTypes, int> _dicDamage = 
			new Dictionary<Enumerators.DamageTypes, int>
		{
			{ Enumerators.DamageTypes.HULL_TORN,	 10	},
			{ Enumerators.DamageTypes.DENT_SMALL,	 10	},
			{ Enumerators.DamageTypes.DENT_BIG,		 15	},
			{ Enumerators.DamageTypes.SCRATCH,		 20	},
			{ Enumerators.DamageTypes.HOLE,			 25	},
			{ Enumerators.DamageTypes.BOTTOM_BREAK,  25 },

			{ Enumerators.DamageTypes.SCREW_NET,	 10	},
			{ Enumerators.DamageTypes.SCREW_ROPE,	 10	},
			{ Enumerators.DamageTypes.SCREW_SEAWEED, 10 }
		};

		// -------------------------------------
		// dependences
		private ISoundManager _soundManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager)
		{
		    _soundManager = soundManager;
		}



		// A_SERVICE
		protected override void InitAwake()
		{
			_currHp = _startHp;
		}

		protected override void InitStart()
		{
			InvokeRepeating("CheckDmgPerSec", 0f, 1f);
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ISubmarineHpManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ISubmarineHpManager>();
		}


	
		// INTERFACES
		public void AddDmgPerSec(Enumerators.DamageTypes dmgType)
		{
			_dmgPerSec += _dicDamage[dmgType];

			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
                ShowProgressBar();
			}

			// slow bottom spped
			if (_dmgPerSec > 0)
			{
				Services.Get<IBottomSpeedManager>().SetSpeedSlow();
			}
		}

		public void ReduceDmgPerSec(Enumerators.DamageTypes dmgType)
		{
			_dmgPerSec -= _dicDamage[dmgType];

			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
				ShowProgressBar();
			}

			// normal bottom speed
			if (_dmgPerSec <= 0)
			{
				Services.Get<IBottomSpeedManager>().SetSpeedNormal();
			}
		}

		public void RestoreHp()
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
				int restoredHp = 400;
				_currHp += restoredHp;

				if (_currHp >= _startHp) {
					_currHp = _startHp; }

				_soundManager.PlaySound(SoundsGameplayFX.Bonuses.submarineHp);
				ShowProgressBar();
			}
		}

		public void SetHpEmpty()
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
				_currHp = 0;
			}
		}



		// METHODS
		private void CheckDmgPerSec()       // used in Invoke()
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
				_currHp -= _dmgPerSec;
				ShowProgressBar();

				if (_currHp <= 0)
				{
					LoseGameHp();
					CancelInvoke("CheckDmgPerSec");
				}
			}
		}

		private void ShowProgressBar()
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
				_progressBar.fillAmount = (float)_currHp / _startHp;
			}
		}

		private void LoseGameHp()
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS &&
			    !Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
			{
				Services.Get<ILevelDecoratorManager>().StartLooseSequence();
			}
		}



	}
}