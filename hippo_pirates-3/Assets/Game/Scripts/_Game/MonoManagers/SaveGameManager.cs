﻿
using UnityEngine;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Common.Info;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class SaveGameManager : AMonoService, ISaveGameManager
	{
		// FIELDS
		// -------------------------------------
		private readonly string _nameCountStarsVeryEasy = "countStarsVeryEasy";
		private readonly string _nameCountStarsEasy = "countStarsEasy";
		private readonly string _nameCountStarsNormal = "countStarsNormal";

		// check - if we complete Tutorial
		private readonly string _nameIsTutorialCompleted = "isTutorialCompleted";

		// -------------------------------------
		// 0 - completed 1_lvl (_)
		// 1 - completed 2_lvl (*)
		// 2 - completed 3_lvl (**)
		// 3 - completed 4_lvl (***)
		private const int _maxCountStars = 3;



		// A_SERVICE
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			InitPlayerPrefs();

			// delete saves (remove in release) !!!
			//ClearAllData();
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
			#if UNITY_EDITOR
			    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			    autoAssertsHandler.AssertAllFieldsInExternalClass();
			#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ISaveGameManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ISaveGameManager>();
		}



		// INTERFACES

		#region GETTERS/SETTERS: ints

		//
		public int Get_CountStarsVeryEasy()
		{
			return GetData(_nameCountStarsVeryEasy);
		}

		public void Set_CountStarsVeryEasy(int number)
		{
			// kostil
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS)
			{
				CheckSetCountStarsVeryEasy(number);
				SaveData();
			}
		}

		//
		public int Get_CountStarsEasy()
		{
			return GetData(_nameCountStarsEasy);
		}

		public void Set_CountStarsEasy(int number)
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS)
			{
				CheckSetCountStarsEasy(number);
                SaveData();
			}
		}

		//
		public int Get_CountStarsNormal()
		{
			return GetData(_nameCountStarsNormal);
		}

		public void Set_CountStarsNormal(int number)
		{
			if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS)
			{
				CheckSetCountStarsNormal(number);
				SaveData();
			}
		}

		//
		public int Get_MaxCountStars()
		{
			return _maxCountStars;
		}

		//
		public bool Get_IsTutorialCompleted()
		{
			// 0 is false
			// 1 is true

			int flagTutorialCompleted = GetData(_nameIsTutorialCompleted);

			if (flagTutorialCompleted == 0) {
				return false; }
			if (flagTutorialCompleted == 1) {
				return true; }

			return false;
		}

		public void Set_IsTutorialCompleted()
		{
			// 0 is false
			// 1 is true

			SetData(_nameIsTutorialCompleted, 1);
			SaveData();
		}

		#endregion



		// METHODS
		private void CheckSetCountStarsVeryEasy(int number)
		{
			// pre-conditions
			#region ASSERTS
			    #if UNITY_EDITOR
			    Assert.IsTrue(number <= _maxCountStars);
			    #endif
			#endregion

			if (GetData(_nameCountStarsVeryEasy) < number) {
				SetData(_nameCountStarsVeryEasy, number); }
		}

		private void CheckSetCountStarsEasy(int number)
		{
			// pre-conditions
			#region ASSERTS
#if UNITY_EDITOR
			Assert.IsTrue(number <= _maxCountStars);
#endif
			#endregion

			if (GetData(_nameCountStarsEasy) < number) {
				SetData(_nameCountStarsEasy, number); }
		}

		private void CheckSetCountStarsNormal(int number)
		{
			// pre-conditions
			#region ASSERTS
#if UNITY_EDITOR
			Assert.IsTrue(number <= _maxCountStars);
#endif
			#endregion

			if (GetData(_nameCountStarsNormal) < number) {
				SetData(_nameCountStarsNormal, number); }
		}

		#region PLAYER_PREFS

		private void InitPlayerPrefs()
		{
			InitData(_nameCountStarsVeryEasy, 	0);
			InitData(_nameCountStarsEasy, 		0);
			InitData(_nameCountStarsNormal, 	0);

			InitData(_nameIsTutorialCompleted,	0);
		}

		private void InitData(string someName, int someValue)
		{
			if ( !PlayerPrefs.HasKey(someName) ) {
				PlayerPrefs.SetInt(someName, someValue); }
		}

		private void SetData(string someName, int someValue)
		{
			if ( PlayerPrefs.HasKey(someName) )
			{
				PlayerPrefs.SetInt(someName, someValue);
			}
			else
			{
				#region DEBUG
#if UNITY_EDITOR
				Debug.Log("[ERROR] there is no such key = " + someName);
#endif
				#endregion
			}
		}

		private void SaveData()
		{
			PlayerPrefs.Save();
		}

		private int GetData(string someName)
		{
			if ( PlayerPrefs.HasKey(someName) ) {
				return PlayerPrefs.GetInt(someName); }

			#region DEBUG
#if UNITY_EDITOR
			Debug.Log("[ERROR] cannot get data from: " + someName);
#endif
			#endregion
			return -999;
		}

		private void ClearAllData()
		{
			PlayerPrefs.DeleteAll();
			Debug.Log("Clear all data !");
		}

		#endregion



	}
}