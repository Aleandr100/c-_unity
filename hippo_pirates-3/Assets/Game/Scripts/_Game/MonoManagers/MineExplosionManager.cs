﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Common.Info.AnimationParameters;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class MineExplosionManager : AMonoService, IMineExplosionManager
	{
		// FIELDS
		private readonly List<GameObject> _destroyableObjects = new List<GameObject>();



		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IMineExplosionManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IMineExplosionManager>();
		}


		// UNITY
		private void OnDestroy()
		{
			ClearList();
		}

	

		// INTERFACES

		#region GETTERS/SETTERS

		public List<GameObject> Get_DestroyableObjects()
		{
			return _destroyableObjects;
		}

		#endregion

		public void AddToList(GameObject go)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			_destroyableObjects.Add(go);
		}

		public void AplyExplosion()
		{
			Services.Get<IAvatarsManager>().DeactivateAllThreats();
			WipeDestroyableObjects();
		}

	

		// METHODS
		private void WipeDestroyableObjects()
		{
			foreach (var item in _destroyableObjects)
			{
				if (item.activeSelf)
				{
					// kostil
					if (!item.CompareTag(Tags.Mine)) {
						item.SetActive(false); }
				}
			}
		}

		private void ClearList()
		{
			foreach (var item in _destroyableObjects)
			{
				Destroy(item);
			}

			_destroyableObjects.Clear();
		}

	

	}
}