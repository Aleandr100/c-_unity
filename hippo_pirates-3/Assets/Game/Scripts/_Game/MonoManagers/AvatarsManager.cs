﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Pirates_3.Common;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class AvatarsManager : AMonoService, IAvatarsManager
	{
		// FIELDS
		[Header("[AVATARS]")]
		[SerializeField] private Sprite _sprHeartRed;
		[SerializeField] private Sprite _sprHeartBlue;

		[Header("[THREATS]")]
		[SerializeField] private GameObject[] _threats;

		private int _countThreats;

		private readonly Vector2[] _avatarsPositions =
		{
			new Vector2(-400, 170),		// default y = 190
			new Vector2(-300, 170),
			new Vector2(-200, 170),
			new Vector2(-100, 170),
			new Vector2(0   , 170)
		};

		[Header("[BONUS - ARMOR]")]
		[SerializeField] private GameObject _objBonusArmor;
		[SerializeField] private Image _progBarBonusArmor;

		private const int _maxTimeBonusArmor = 15;
		private int _timerBonusArmor;

		[Header("[BONUS - REPAIR RATE]")]
		[SerializeField] private GameObject _objBonusRepairRate;
		[SerializeField] private Image _progBarBonusRepairRate;

		private const int _maxTimeBonusRepairRate = 20;
		private int _timerRepairRate;

		// others
		private readonly Dictionary<string, int> _dictThreats = new Dictionary<string, int>
		{
			{ "Whale",		0 },
			{ "Octopus",	1 },
			{ "Squid_Big",	2 },
			{ "Container",	3 },
			{ "Car_Big",	4 },
			{ "Anchor",		5 }
		};


    
		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IAvatarsManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IAvatarsManager>();
		}

	

		// INTERFACES
		public void ActivateThreat(GameObject go)
		{
			ResetAllHeartsRed();
			ShowThreat(go);
			SortDynamicListOfAvatars();
		}

		public void DeactivateThreat(GameObject go)
		{
			HideThreat(go);
			SortDynamicListOfAvatars();
		}

		public void DeactivateAllThreats()
		{
			for (int i = 0; i < _threats.Length; i++)
			{
				if (_threats[i].activeSelf)
				{
					DeactivateThreat( _threats[i] );
				}
			}
		}

		public void ActivateBonusArmor()
		{
			ShowBonusArmor();
			SortDynamicListOfAvatars();
		}

		public void DeactivateBonusArmor()
		{
			HideBonusArmor();
		}

		public void ActivateBonusRepairRate()
		{
			ShowBonusRepairRate();
			SortDynamicListOfAvatars();
		
			SafeCancelInvoke("CheckBonusRepairRateTimer");
			_timerRepairRate = _maxTimeBonusRepairRate;
			InvokeRepeating("CheckBonusRepairRateTimer", 0, 1f);
		}

		public void ShowHpOnHit(GameObject go, int hits)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			Transform goHearts = go.transform.GetChild(2);
		
			for (int i = 0; i < hits; i++)
			{
				if (hits < goHearts.childCount)
				{
					Image tempImage = goHearts.GetChild(i).GetComponent<Image>();
					Assert.AreNotEqual(null, tempImage);

					tempImage.sprite = _sprHeartBlue;
				}
			}

			if (hits >= goHearts.childCount)
			{
				ResetAvatarThreat(go);
			}
		}

	
	
		// METHODS
		private void SortDynamicListOfAvatars()
		{
			CalcActiveAvatars();
			SetThreatsPositions();
			SetBonusArmorPosition();
			SetBonusRepairRatePosition();
		}

		#region THREATS

		private void ShowThreat(GameObject go)
		{
			for (int i = 0; i < _threats.Length; i++)
			{
				if (!_threats[i].activeSelf)
				{
					_threats[i].SetActive(true);
					LinkAvatarToThreatBig(go, _threats[i]);
					ShowThreatImage(go, i);
					break;
				}
			}
		}

		private void ShowThreatImage(GameObject go, int threatNum)
		{
			int childNum = _dictThreats[go.name];
			Transform avatarImages = _threats[threatNum].transform.GetChild(1);

			for (int i = 0; i < avatarImages.childCount; i++)
			{
				if (i != childNum)
				{
					avatarImages.GetChild(i).gameObject.SetActive(false);
				}
				else
				{
					avatarImages.GetChild(i).gameObject.SetActive(true);
				}
			}
		}

		private void HideThreat(GameObject go)
		{
			go.SetActive(false);
		}

		private void CalcActiveAvatars()
		{
			_countThreats = 0;

			for (int i = 0; i < _threats.Length; i++)
			{
				if (_threats[i].activeSelf)
				{
					_countThreats++;
				}
			}
		}

		private void LinkAvatarToThreatBig(GameObject goThreatBig, GameObject goAvatar)
		{
			var goMove = goThreatBig.GetComponent<IAvatarSupport>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_AvatarLink(goAvatar);
		}

		private void SetThreatsPositions()
		{
			for (int i = 0; i < _threats.Length; i++)
			{
				if (_threats[i].activeSelf)
				{
					var rt = _threats[i].GetComponent<RectTransform>();
					#region ASSERTS
	#if UNITY_EDITOR
					Assert.AreNotEqual(null, rt);
	#endif
					#endregion

					if (i <= _countThreats - 1)
					{
						rt.anchoredPosition = _avatarsPositions[i];
					}
				}
			}
		}

		private void ResetAvatarThreat(GameObject go)
		{
			// hide all avatars
			Transform goAvatars = go.transform.GetChild(1);
			for (int i = 0; i < goAvatars.childCount; i++)
			{
				goAvatars.GetChild(i).gameObject.SetActive(false);
			}

			// show all hearts as red
			Transform goHearts = go.transform.GetChild(2);
			for (int i = 0; i < goHearts.childCount; i++)
			{
				goHearts.GetChild(i).GetComponent<Image>().sprite = _sprHeartRed;
			}
		}

		private void ResetAllHeartsRed()
		{
			for (int u = 0; u < _threats.Length; u++)
			{
				// show all hearts as red
				Transform goHearts = _threats[u].transform.GetChild(2);
				for (int i = 0; i < goHearts.childCount; i++)
				{
					goHearts.GetChild(i).GetComponent<Image>().sprite = _sprHeartRed;
				}
			}
		}

		#endregion

		#region BONUS_ARMOR

		private void ShowBonusArmor()
		{
			_objBonusArmor.SetActive(true);
		}

		private void HideBonusArmor()
		{
			_objBonusArmor.SetActive(false);
		}

		#endregion

		#region BONUS_REPAIR_RATE

		private void CheckBonusRepairRateTimer()    // used in Invoke()
		{
			_timerRepairRate--;

			if (_timerRepairRate <= 0)
			{
				_timerRepairRate = 0;

				HideBonusRepairRate();
				SortDynamicListOfAvatars();
				CancelInvoke("CheckBonusRepairRateTimer");
			}

			ShowProgressBar();
		}

		private void ShowProgressBar()
		{
			_progBarBonusRepairRate.fillAmount = (float)_timerRepairRate / _maxTimeBonusRepairRate;
		}

		private void ShowBonusRepairRate()
		{
			_objBonusRepairRate.SetActive(true);
		}

		private void HideBonusRepairRate()
		{
			_objBonusRepairRate.SetActive(false);
		}

		#endregion

		// bonuses
		private void SetBonusArmorPosition()
		{
			if (_objBonusArmor.activeSelf)
			{
				var rt = _objBonusArmor.GetComponent<RectTransform>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, rt);
	#endif
				#endregion

				rt.anchoredPosition = _avatarsPositions[_countThreats];
			}
		}

		private void SetBonusRepairRatePosition()
		{
			if (_objBonusRepairRate.activeSelf)
			{
				var rt = _objBonusRepairRate.GetComponent<RectTransform>();
				#region ASSERTS
	#if UNITY_EDITOR
				Assert.AreNotEqual(null, rt);
	#endif
				#endregion

				if (!_objBonusArmor.activeSelf)
				{
					rt.anchoredPosition = _avatarsPositions[_countThreats];
				}
				else
				{
					rt.anchoredPosition = _avatarsPositions[_countThreats + 1];
				}
			}
		}



	}
}