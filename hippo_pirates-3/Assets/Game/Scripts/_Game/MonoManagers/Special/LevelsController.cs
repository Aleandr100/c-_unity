﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common.Info;
using PSV_Prototype;
using RateMePlugin;
using UnityEngine;

namespace Pirates_3.Common
{
	public class LevelsController
	{
		// FIELDS
		private static Enumerators.LevelsTypes _currLevelType =
			Enumerators.LevelsTypes.HORIZONTAL;         // HORIZONTAL

		private static Enumerators.LevelsSubtypes _currLevelSubtype =
			Enumerators.LevelsSubtypes.TUTORIAL;        // TUTORIAL

		private static int _countLevels;

		// level difficulty
		private static Enumerators.LevelDifficulty _currLevelDifficulty =
			Enumerators.LevelDifficulty.VERY_EASY;      // VERY_EASY

		// age (2+ / 5+)
		private static Enumerators.AgeType _currAgeType = Enumerators.AgeType.TWO_PLUS;



		// INTERFACES
		#region GETTERS/SETTERS

		public static Enumerators.LevelsTypes Get_CurrentLevelType()
		{
			return _currLevelType;
		}

		public static Enumerators.LevelsSubtypes Get_CurrentLevelSubType()
		{
			return _currLevelSubtype;
		}

		//
		public static Enumerators.LevelDifficulty Get_CurrentLevelDifficulty()
		{
			return _currLevelDifficulty;
		}

		public static void Set_CurrentDifficultyLevel(Enumerators.LevelDifficulty levelDifficulty)
		{
			_currLevelDifficulty = levelDifficulty;
		}

		//
		public static Enumerators.AgeType Get_AgeType()
		{
			return _currAgeType;
		}

		public static void Set_AgeType(Enumerators.AgeType ageType)
		{
			_currAgeType = ageType;
		}

		#endregion

		public static void LoadMainMenu()
		{
			_countLevels = 0;
			_currLevelType = Enumerators.LevelsTypes.HORIZONTAL;
			_currLevelSubtype = Enumerators.LevelsSubtypes.TUTORIAL;

			SceneLoader.Instance.SwitchToScene(Scenes.MainMenu);
		}

		public static void LoadChooseDifficulty()
		{
			SceneLoader.Instance.SwitchToScene(Scenes.ChooseDifficulty);
		}

		public static void LoadIntroTavern()
		{
			SceneLoader.Instance.SwitchToScene(Scenes.Intro_Tavern);
		}

		public static void LoadIntroSecretCave()
		{
			SceneLoader.Instance.SwitchToScene(Scenes.Intro_SecretCave);
		}

		public static void LoadGameplayTutorial()
		{
			_countLevels = 0;
			_currLevelType = Enumerators.LevelsTypes.HORIZONTAL;
			_currLevelSubtype = Enumerators.LevelsSubtypes.TUTORIAL;

			SceneLoader.Instance.SwitchToScene(Scenes.Gameplay);
		}

		public static void LoadGameplayLevelOne()
		{
			_countLevels = 1;
			_currLevelType = Enumerators.LevelsTypes.HORIZONTAL;
			_currLevelSubtype = Enumerators.LevelsSubtypes.HORIZONTAL_A;

			SceneLoader.Instance.SwitchToScene(Scenes.Gameplay);
		}

		public static void LoadLooseScene()
		{
			SceneLoader.Instance.SwitchToScene(Scenes.LooseMenu);
		}

		public static void LoadNextLevel()
		{
			_countLevels++;

			// ---------------------------------------------------------------
			if (_countLevels == 1)
			{
				_currLevelType = Enumerators.LevelsTypes.HORIZONTAL;
				_currLevelSubtype = Enumerators.LevelsSubtypes.HORIZONTAL_A;
				SceneLoader.Instance.SwitchToScene(Scenes.Gameplay);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 2)
			{
				// HOR_A -> VERT_A
				SceneLoader.Instance.SwitchToScene(Scenes.Interlevel_HorA_VertA);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 3)
			{
				_currLevelType = Enumerators.LevelsTypes.VERTICAL;
				_currLevelSubtype = Enumerators.LevelsSubtypes.VERTICAL_A;
				SceneLoader.Instance.SwitchToScene(Scenes.Gameplay);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 4)
			{
				// show RateUs
				if (RateMeModule.Instance.CanShow(Time.time))
				{
					SceneLoader.Instance.SwitchToScene(Scenes.RateUs);
				}
				else
				{
					LoadNextLevel();
				}
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 5)
			{
				// VERT_A -> HOR_B
				SceneLoader.Instance.SwitchToScene(Scenes.Interlevel_VertA_HorB);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 6)
			{
				_currLevelType = Enumerators.LevelsTypes.HORIZONTAL;
				_currLevelSubtype = Enumerators.LevelsSubtypes.HORIZONTAL_B;
				SceneLoader.Instance.SwitchToScene(Scenes.Gameplay);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 7)
			{
				// HOR_B -> VERT_B
				SceneLoader.Instance.SwitchToScene(Scenes.Interlevel_HorB_VertB);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 8)
			{
				_currLevelType = Enumerators.LevelsTypes.VERTICAL;
				_currLevelSubtype = Enumerators.LevelsSubtypes.VERTICAL_B;
				SceneLoader.Instance.SwitchToScene(Scenes.Gameplay);
			}
			// ---------------------------------------------------------------
			else if (_countLevels == 9)
			{
				SceneLoader.Instance.SwitchToScene(Scenes.WinMenu_SunkShip);
			}
			// ---------------------------------------------------------------
		}



	}
}