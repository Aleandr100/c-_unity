﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using System.Collections.Generic;

namespace Pirates_3.Common
{
	public class Services 
	{
		// FIELDS
		public static Dictionary<Type, IMonoService> _dictServices =
			new Dictionary<Type, IMonoService>();   
	


		// INTERFACES
		public static void Add<T>(IMonoService service)
		{
			if (service is T)
			{
				_dictServices.Add(typeof(T), service);
			}
			else
			{
				throw new Exception( string.Format("Service <{0}> have not implemented interface: {1} !", 
					service, typeof(T) ));
			}
		}

		public static void Remove<T>()
		{
			_dictServices.Remove(typeof(T));
		}

		public static T Get<T>()
		{
			try
			{
				return (T)_dictServices[typeof(T)];
			}
			catch (KeyNotFoundException)
			{
				throw new Exception( string.Format("Service <{0}> is not registered !", 
					typeof(T) ));
			}
		}

		public static void Clear()
		{
			_dictServices.Clear();
		}



	}
}