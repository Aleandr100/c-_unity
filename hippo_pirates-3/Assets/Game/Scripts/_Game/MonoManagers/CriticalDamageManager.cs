﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Assets.Game.Scripts._Game.Interfaces.IMonoManagers;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using Pirates_3.Common.Info.Sounds;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Pirates_3.Menu.Gameplay.MonoManagers
{
	public class CriticalDamageManager : AMonoService, ICriticalDamageManager
	{
		// FIELDS
		// ---------------------------------------
		//
		[SerializeField] private GameObject _critDamage;
		[SerializeField] private Animator _animatorAlarmLamp;
		[SerializeField] private Image _progressBar;

		// ---------------------------------------
		//
		private readonly int _baseTime = 30;
		private readonly int _dopTime = 5;
		private int _timer;
		private int _countCritDmg;

		// ---------------------------------------
		// dependences
		private ISoundManager _soundManager;
		private IAnimationManager _animationManager;
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
		    _soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}


	
		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<ICriticalDamageManager>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ICriticalDamageManager>();
		}


	

		// INTERFACES
		public void CheckDamageType(Enumerators.DamageTypes myDmgType)
		{
			if ( !IsTutorial() )
			{
				if ( IsCriticalDamageType(myDmgType) )
				{
					ApplyCritDamage();
				}
			}
		}

		public void RepairCritDamage(Enumerators.DamageTypes myDmgType)
		{
			_countCritDmg--;

			// ===========================================================
			// 
			if (_countCritDmg >= 1) {
				_timer += _dopTime; }

			// ===========================================================
			// 
			else
			{
				_countCritDmg = 0;
				ShowAlarm(false);

				_soundManager.StopSound(SoundsGameplayFX.Misc.alarmCriticalDamage);
				SafeCancelInvoke("StartTimer");
			}
			// ===========================================================
		}



		// METHODS
		private void StartTimer()       // used in Invoke()
		{
			_timer -= 1;

			CheckTimer();
			ShowProgressBar();
		}

		private void CheckTimer()
		{
			if (_timer <= 0)
			{
				SafeCancelInvoke("StartTimer");
				Services.Get<ISubmarineHpManager>().SetHpEmpty();
			}
		}

		private void ShowProgressBar()
		{
			_progressBar.fillAmount = (float) _timer / _baseTime;
		}

		private void ApplyCritDamage()
		{
			// ===========================================================
			// 
			if (_countCritDmg <= 0)
			{
				_timerManager.StartTimer(
					_soundManager.PlaySoundRepeating, SoundsGameplayFX.Misc.alarmCriticalDamage, 1f);

				_timerManager.StartTimer( ShowAlarm, true, 1f );

				if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS ||
					Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
				{
					_timer = 0;
					const int baseTime = 30;
					_timer += baseTime;

					SafeStartInvokeRepeating("StartTimer", 0f, 1f);
				}
			}
			else
			{
				if (LevelsController.Get_AgeType() == Enumerators.AgeType.FIVE_PLUS ||
				    Services.Get<IGameplaySpawnController>().Get_IsEndLevelTimer() )
				{
					_timer -= _dopTime;
					CheckTimer();
				}
			}
			// ===========================================================

			_countCritDmg++;
		}

		private void ShowAlarm(bool myValue)
		{
			_critDamage.SetActive(myValue);
			_animationManager.SetParameterBool(_animatorAlarmLamp, ParamBools.Submarine.isAlarm, myValue);
		}

		private bool IsTutorial()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}

		private bool IsCriticalDamageType(Enumerators.DamageTypes myDmgType)
		{
			return (myDmgType == Enumerators.DamageTypes.SCRATCH ||
					myDmgType == Enumerators.DamageTypes.HOLE ||
					myDmgType == Enumerators.DamageTypes.BOTTOM_BREAK ||
					myDmgType == Enumerators.DamageTypes.SCREW_NET ||
					myDmgType == Enumerators.DamageTypes.SCREW_ROPE ||
					myDmgType == Enumerators.DamageTypes.SCREW_SEAWEED);
		}



	}
}