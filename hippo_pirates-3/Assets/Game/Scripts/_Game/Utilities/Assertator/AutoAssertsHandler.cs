﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System.Reflection;
using System;

namespace Pirates_3.Common
{
	// класс, ответсвенный за автопроверку полей. Служит для уменьшения кода и облегчения автопроверки

	public class AutoAssertsHandler
	{
		#region Variables
		private BindingFlags bindingFlags;
		private Type typeOfExternalClass;
		private object externalClass;
		#endregion



		// INTERFACES
		public AutoAssertsHandler(Type typeOfExternalClass, object externalClass)
		{
			Assertator.AssertClassNotNull(typeOfExternalClass, "typeOfExternalClass", this.GetType());
			Assertator.AssertClassNotNull(externalClass, "externalClass", this.GetType());
			bindingFlags = BindingFlags.Public |
							BindingFlags.NonPublic |
							BindingFlags.Instance |
							BindingFlags.Static;
			this.typeOfExternalClass = typeOfExternalClass;
			this.externalClass = externalClass;
		}

		public void AssertAllFieldsInExternalClass()
		{

			foreach (FieldInfo field in typeOfExternalClass.GetFields(bindingFlags))
			{
				Assertator.AssertClassNotNull(field.GetValue(externalClass), field.Name, typeOfExternalClass);
			}
		}



	}
}

