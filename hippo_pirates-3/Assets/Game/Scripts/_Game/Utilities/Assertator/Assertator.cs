﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using System.Diagnostics;
using UnityEngine.Assertions;

// Класс, который является оберткой над утверждениями.
// Цель - собрать проверки в одном место

namespace Pirates_3.Common
{
	public static class Assertator
	{
		// FIELDS
		private static string _strError = "[ERROR]";



		// INTERFACES
		public static void AssertClassNotNull(object obj)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

			Assert.AreNotEqual(null, obj, _strError + "WAS CALLED IN METHOD "
					+  stackFrames[stackFrames.Length - 1].GetMethod().Name + "()"
					+ " FROM CLASS "+ stackFrames[stackFrames.Length - 1].GetMethod().DeclaringType);
		}

		public static void AssertClassNotNull(object obj, string name)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

			Assert.AreNotEqual(null, obj, _strError + "OBJECT " + name
				+ " IS NULL. IT WAS CALLED IN METHOD "
				+ stackFrames[stackFrames.Length - 1].GetMethod().Name + "()"
				+ " FROM CLASS " + stackFrames[stackFrames.Length - 1].GetMethod().DeclaringType);
		}

		public static void AssertClassNotNull(object obj, string name, Type clasS)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

			Assert.AreNotEqual(null, obj, _strError + "OBJECT " + name
				+ " IS NULL. IT WAS CALLED IN METHOD "
				+ stackFrames[stackFrames.Length - 1].GetMethod().Name + "()"
				+ " FROM CLASS " + clasS.Name);
		}

		public static void AssertIsTrue(bool condition, string message)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();

			Assert.IsTrue(condition, _strError + message + 
				" WAS CALLED IN METHOD "
					+ stackFrames[stackFrames.Length - 1].GetMethod().Name);
		}

		public static void AssertIsTrue(bool condition, string message, Type clasS)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();

			Assert.IsTrue(condition, _strError + message +
				" WAS CALLED IN METHOD "
					+ stackFrames[stackFrames.Length - 1].GetMethod().Name + "()"
					+ " FROM CLASS " + clasS.Name);
		}

		public static void AssertIsTrue(bool condition, string message,  Type clasS, int deptOfReflection = 1)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();

			Assert.IsTrue(condition, _strError + message +
				" WAS CALLED IN METHOD "
					+ stackFrames[stackFrames.Length - deptOfReflection].GetMethod().Name + "()"
					+ " FROM CLASS " + clasS.Name);
		}

		public static void AssertIsFalse(bool condition, string message)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();

			Assert.IsFalse(condition, _strError + message +
				" WAS CALLED IN METHOD "
					+ stackFrames[stackFrames.Length - 1].GetMethod().Name + "()"
					);
		}

		public static void AssertIsFalse(bool condition, string message, Type clasS)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();

			Assert.IsFalse(condition, _strError + message +
				" WAS CALLED IN METHOD "
					+ stackFrames[stackFrames.Length - 1].GetMethod().Name + "()"
					+ " FROM CLASS " + clasS.Name);
		}

		public static void AssertIsFalse(bool condition, string message, Type clasS, int deptOfReflection = 1)
		{
			StackTrace stackTrace = new StackTrace();           // get call stack
			StackFrame[] stackFrames = stackTrace.GetFrames();

			Assert.IsFalse(condition, _strError + message +
				" WAS CALLED IN METHOD "
					+ stackFrames[stackFrames.Length - deptOfReflection].GetMethod().Name + "()"
					+ " FROM CLASS " + clasS.Name);
		}



	}
}