﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using DG.Tweening;
using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public abstract class AMonoTutorial : AMonoService
	{
		// FIELDS
		// --------------------------------------------------------
		//
		protected Camera _cameraUI;

		protected Transform _helpHand;
		protected Animator _animatorHelpHand;

		protected RectTransform _helpHandUI;
		protected Animator _animatorHelpHandUI;

		protected Button _buttonScissors;
		protected Button _buttonMagnet;
		protected Button _buttonHammer;
		protected Button _buttonWelding;

		protected Animator _animatorRaccoon;

		// --------------------------------------------------------
		//
		public enum Tutorials
		{
			NONE, 

			HARPOON_VS_FISH_SMALL,
			HARPOON_VS_TRASH_SMALL,
			SCISSORS_VS_ROPE_MOVE,
			HARPOON_VS_FISH_BIG,
			HAMMER_VS_DAMAGE,
			MAGNET_VS_DAMAGE,
			WELDING_VS_DAMAGE,
			SCISSORS_VS_DAMAGE
		}

		protected static Tutorials _currentTutorial;

		// ---------------------------------------
		// dependences
		protected ITutorialSceneData _tutorialSceneData;
		protected ISoundManager _soundManager;
		protected IAnimationManager _animationManager;
		protected ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITutorialSceneData tutorialSceneData,
								ISoundManager soundManager,
								IAnimationManager animationManager,
								ITimerManager timerManager)
		{
			_tutorialSceneData = tutorialSceneData;
			_soundManager = soundManager;
			_animationManager = animationManager;
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
			_cameraUI = _tutorialSceneData.Get_CameraUI();

			_helpHand = _tutorialSceneData.Get_HelpHand();
			_animatorHelpHand = _tutorialSceneData.Get_AnimatorHelpHand();

			_helpHandUI = _tutorialSceneData.Get_HelpHandUI();
			_animatorHelpHandUI = _tutorialSceneData.Get_AnimatorHelpHandUI();

			_buttonScissors = _tutorialSceneData.Get_ButtonScissors();
			_buttonMagnet = _tutorialSceneData.Get_ButonMagnet();
			_buttonHammer = _tutorialSceneData.Get_ButtonHammer();
			_buttonWelding = _tutorialSceneData.Get_ButtonWelding();

			_animatorRaccoon = _tutorialSceneData.Get_AnimatorRaccoon();
		}



		// INTERFACES
		public static Tutorials Get_CurrentTutorial()
		{
			return _currentTutorial;
		}



		// METHODS

		#region MOUSE_CLICK

		protected Vector2 GetMousePosition()
		{
			Vector2 mousePos = _cameraUI.ScreenToWorldPoint(Input.mousePosition);

			return mousePos;
		}

		protected virtual void ActionMouseClick(Vector2 mousePos)
		{
			// in some Tutorial parts click is not nessessary (so - this method is virtual, NOT abstract!)
		}

		protected bool IsClickHelpHand(Vector2 mousePos)
		{
			var layerMask = 1 << (int)Enumerators.Layers.HELP_HAND;
			Collider2D[] arrColls = Physics2D.OverlapPointAll(mousePos, layerMask);

			return (arrColls.Length > 0);
		}

		#endregion

		#region HALP_HAND

		protected void ShowCursor(bool myValue)
		{
			_helpHand.gameObject.SetActive(myValue);
		}

		protected void MoveCursor(Vector2 myPos)
		{
			_helpHand.position = myPos;
		}

		protected void AnimateCursor_Hold()
		{
			_animationManager.SetParameterInt(_animatorHelpHand, ParamInts.HelpHand.type, (int)ParamInts.HelpHand.Types.HELPHAND_HOLD);
		}

		#endregion

		#region HELP_HAND_UI

		protected void ShowCursorUI(bool myValue)
		{
			_helpHandUI.gameObject.SetActive(myValue);
		}

		protected void MoveCursorUI(RectTransform myRT)
		{
			// pre-condition
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, myRT);
	#endif
			#endregion

			_helpHandUI.anchoredPosition = myRT.anchoredPosition;
			_helpHandUI.position = myRT.position;
		}

		protected void AnimateCursorUI_Hold()
		{
			_animationManager.SetParameterInt(_animatorHelpHandUI, ParamInts.HelpHand.type, (int)ParamInts.HelpHand.Types.HELPHAND_HOLD);
		}

		protected void ResetCursorUI()
		{
			_helpHandUI.DOKill();
		}

		#endregion

		protected void PlaySoundTutorial(string strSound)
		{
			float soundLenght = _soundManager.GetSoundLenght(strSound);
			_soundManager.PlaySound(strSound);

			_animationManager.SetParameterBool(_animatorRaccoon, ParamBools.Raccoon.isSpeechOnRepair, true);

			_timerManager.StartTimer<object>( _animationManager.SetParameterBool,
				(new object[] { _animatorRaccoon, ParamBools.Raccoon.isSpeechOnRepair, false }), soundLenght);
		}



	}
}