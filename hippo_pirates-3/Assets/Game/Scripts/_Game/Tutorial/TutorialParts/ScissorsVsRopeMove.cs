﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Common.Info.Sounds;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class ScissorsVsRopeMove : AMonoTutorial, IScissorsVsRopeMove
	{
		// FIELDS
		// --------------------------------------------------------
		//
		private GameObject _rope;

		// --------------------------------------------------------
		// actions
		private readonly List< Func<float> > _actionsOne = new List< Func<float> >();



		// A_SERVICES
		protected override void InitAwake()
		{
			base.InitAwake();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			// can't apply auto-assert here!
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _cameraUI);

			Assert.AreNotEqual(null, _helpHand);
			Assert.AreNotEqual(null, _animatorHelpHand);
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IScissorsVsRopeMove>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IScissorsVsRopeMove>();
		}



		// INTERFACES
		public void StartTutorial()
		{
			_currentTutorial =  Tutorials.SCISSORS_VS_ROPE_MOVE;       

			InitActionsOne();
			StartCoroutine( SequencesOne() );
		}

		public void HandleRopeComplete()
		{
			ShowCursorUI(false);
			Services.Get<IToolsButtonsListenersTutorial>().ActivateBlocking();
			NextTutorial();
		}



		// METHODS
		// ==========================================================================

		protected override void ActionMouseClick(Vector2 mousePos)
		{
		}

		#region INIT_ACTIONS_ONE

		private void InitActionsOne()
		{
			AddActionOne(SpawnRope);
			AddActionOne(StopRope);
			AddActionOne(ShowHelpHandOnScissorsToolButton);
			AddActionOne(DeactivateFakeToolBlock);
			AddActionOne(SoundTutorial);
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_ONE

		private float SpawnRope()
		{
			const float speed = 3f;
			_rope = Services.Get<IRopePoolTutorial>().ShowObject(speed);

			return (9f / speed);
		}

		private float StopRope()
		{
			// stop 
			var goMove = _rope.GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion
			goMove.StopMovement();

			return 1f;
		}

		private float ShowHelpHandOnScissorsToolButton()
		{
			var rtScissors = _buttonScissors.GetComponent<RectTransform>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rtScissors);
	#endif
			#endregion

			MoveCursorUI(rtScissors);
			ShowCursorUI(true);
			AnimateCursorUI_Hold();

			MoveCursorUIFromButtonScissorsToRope();

			return 0f;
		}

		private float DeactivateFakeToolBlock()
		{
			Services.Get<IToolsButtonsListenersTutorial>().DeactivateBlocking();

			return 0;
		}

		private float SoundTutorial()
		{
			PlaySoundTutorial(SoundsSpeech.Tutorial.scissorsVsropeMove);

			return 0f;
		}
	
		#endregion

		private void MoveCursorUIFromButtonScissorsToRope()
		{
			Vector2 correctedPos = new Vector2(_rope.transform.position.x, _rope.transform.position.y - 5f);
		
			_helpHandUI.DOMove(correctedPos, 2f)
				.SetLoops(999, loopType:LoopType.Yoyo)
				.SetEase(Ease.InCubic); 
		}

		// ==========================================================================

		#region MISC_ONE

		private void AddActionOne(Func<float> action)
		{
			_actionsOne.Add(action);
		}

		private IEnumerator SequencesOne()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsOne.Count)
				{
					break;
				}

				delay = _actionsOne[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		// ==========================================================================

		private void NextTutorial()
		{
			Services.Get<IHarpoonVsFishBig>().StartTutorial();
		}



	}
}