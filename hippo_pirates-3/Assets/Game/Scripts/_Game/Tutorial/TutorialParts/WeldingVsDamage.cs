﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using Pirates_3.Common;
using Pirates_3.Common.Info.Sounds;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class WeldingVsDamage : AMonoTutorial, IWeldingVsDamage
	{
		// FIELDS
		// --------------------------------------------------------
		//
		private Transform _damage;

		// --------------------------------------------------------
		// actions
		private readonly List< Func<float> > _actionsOne = new List< Func<float> >();
		private readonly List< Func<float> > _actionsTwo = new List< Func<float> >();
		private readonly List< Func<float> > _actionsThree = new List< Func<float> >();

		// --------------------------------------------------------
		// dependences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
			base.InitAwake();

			InitActionsOne();
			InitActionsTwo();
			InitActionsThree();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			// can't apply auto-assert here!
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _cameraUI);

			Assert.AreNotEqual(null, _helpHand);
			Assert.AreNotEqual(null, _animatorHelpHand);
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IWeldingVsDamage>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IWeldingVsDamage>();
		}



		// INTERFACES
		public void StartTutorial()
		{
			_currentTutorial = Tutorials.WELDING_VS_DAMAGE;
			Services.Get<IToolsButtonsListenersTutorial>().ActivateBlocking();

			StartCoroutine( Sequence(_actionsOne) );
		}

		public void SubmarineHaveDamage(Transform damage)
		{
			_damage = damage;

			StartCoroutine( Sequence(_actionsTwo) );
		}

		public void RepairCompleted()
		{
			StartCoroutine( Sequence(_actionsThree) );
		}



		// METHODS
		// ==========================================================================

		#region INIT_ACTIONS

		private void InitActionsOne()
		{
			AddActionToList(_actionsOne, SpawnFishFast);
		}

		private void InitActionsTwo()
		{
			AddActionToList(_actionsTwo, ShowCursorOnDamage);
			AddActionToList(_actionsTwo, SoundTutorial);
			AddActionToList(_actionsTwo, DeactivateFakeToolBlock);
		}

		private void InitActionsThree()
		{
			AddActionToList(_actionsThree, HideCursorOnDmamage);
			AddActionToList(_actionsThree, NextTutorial);
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_ONE

		private float SpawnFishFast()
		{
			Services.Get<IFishFastPool>().ShowObject();

			return 0f;
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_TWO

		private float ShowCursorOnDamage()
		{
			var rtWelding = _buttonWelding.GetComponent<RectTransform>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rtWelding);
	#endif
			#endregion

			MoveCursorUI(rtWelding);
			ResetCursorUI();
			ShowCursorUI(true);
			AnimateCursorUI_Hold();

			_helpHandUI.DOMove(_damage.position, 2f)
				.SetLoops(999, loopType: LoopType.Yoyo)
				.SetEase(Ease.InCubic);

			return 3f;
		}

		private float SoundTutorial()
		{
			PlaySoundTutorial(SoundsSpeech.Tutorial.weldingVsDamage);
		
			return 0.5f;
		}

		private float DeactivateFakeToolBlock()
		{
			Services.Get<IToolsButtonsListenersTutorial>().DeactivateBlocking();

			return 0;
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_THREE

		private float HideCursorOnDmamage()
		{
			_timerManager.StartTimer(ShowCursorUI, false, 1f);

			return 1.5f;
		}

		private float NextTutorial()
		{
			ResetCursorUI();

			Services.Get<IScissorsVsDamage>().StartTutorial();

			return 0f;
		}

		#endregion

		// ==========================================================================

		#region MISC_ALL

		private void AddActionToList(List<Func<float>> actionList, Func<float> action)
		{
			actionList.Add(action);
		}

		private IEnumerator Sequence(List<Func<float>> actionList)
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == actionList.Count)
				{
					break;
				}

				delay = actionList[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion



	}
}