﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using Pirates_3.Menu.Gameplay.MonoScripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class HarpoonVsFishBig : AMonoTutorial, IHarpoonVsFishBig
	{
		// FIELDS
		// --------------------------------------------------------
		//
		private GameObject _fishBig;
	
		// --------------------------------------------------------
		// actions
		private readonly List< Func<float> > _actionsOne = new List< Func<float> >();
		private readonly List< Func<float> > _actionsTwo = new List< Func<float> >();
		private readonly List< Func<float> > _actionsThree = new List< Func<float> >();
		private readonly List< Func<float> > _actionsFour = new List< Func<float> >();

		// --------------------------------------------------------
		//
		#region INNER CLASS: MOUSE_CLICKS

		private class MouseClicks
		{
			private int _count;
			private readonly int _maxCount = 3;

			public int Get_Count()
			{
				return _count;
			}

			public void IncreaseCount()
			{
				_count ++;

				if (_count > _maxCount) {
					_count = _maxCount; }
			}
		}

		private MouseClicks _mouseClicks = new MouseClicks();

		#endregion

		// --------------------------------------------------------
		// dependences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void InitAwake()
		{
			base.InitAwake();

			InitActionsOne();
			InitActionsTwo();
			InitActionsThree();
			InitActionsFour();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			// can't apply auto-assert here!
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _cameraUI);

			Assert.AreNotEqual(null, _helpHand);
			Assert.AreNotEqual(null, _animatorHelpHand);
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IHarpoonVsFishBig>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IHarpoonVsFishBig>();
		}



		// UNITY
		protected void Update()
		{
			CheckMouseClick();
		}



		// INTERFACES
		public void StartTutorial()
		{
			_currentTutorial = Tutorials.HARPOON_VS_FISH_BIG;        
			StartCoroutine( Sequence(_actionsOne) );
		}



		// METHODS
		// ==========================================================================

		#region MOUSE

		private void CheckMouseClick()
		{
			if (Input.GetMouseButtonDown(0) &&
				_currentTutorial == Tutorials.HARPOON_VS_FISH_BIG )
			{
				Vector2 mousePos = GetMousePosition();
				if ( IsClickHelpHand(mousePos) )
				{
					ActionMouseClick(mousePos);
				}
			}
		}

		protected override void ActionMouseClick(Vector2 mousePos)
		{
			_mouseClicks.IncreaseCount();
			CheckClickCount();
		}

		private void CheckClickCount()
		{
			if (_mouseClicks.Get_Count() == 1)
			{
				StartCoroutine( Sequence(_actionsTwo) );
			}
			else if (_mouseClicks.Get_Count() == 2)
			{
				StartCoroutine( Sequence(_actionsThree) );
			}
			else if (_mouseClicks.Get_Count() == 3)
			{
				StartCoroutine( Sequence(_actionsFour) );
			}
		}

		#endregion

		// ==========================================================================

		#region INIT_ACTIONS

		private void InitActionsOne()
		{
			AddActionToList(_actionsOne, SpawnFishBig);
			AddActionToList(_actionsOne, StopFishBig);
			AddActionToList(_actionsOne, ShowHelpHandOnFishBig);
			AddActionToList(_actionsOne, SoundTutorial);
		}

		private void InitActionsTwo()
		{
			AddActionToList(_actionsTwo, ShootFishBig);
			AddActionToList(_actionsTwo, HideHalphand);
		}

		private void InitActionsThree()
		{
			AddActionToList(_actionsThree, ShootFishBig);
			AddActionToList(_actionsThree, HideHalphand);
		}

		private void InitActionsFour()
		{
			AddActionToList(_actionsFour, ShootFishBig);
			AddActionToList(_actionsFour, HideHalpHandFinal);
			AddActionToList(_actionsFour, NextTutorial);
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_ONE

		private float SpawnFishBig()
		{
			const float speed = 3f;
			_fishBig = Services.Get<IFishBigPoolTutorial>().ShowObject(Enumerators.Directions.LEFT, speed);
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _fishBig);
	#endif
			#endregion

			return (9f / speed );
		}

		private float StopFishBig()
		{
			var goMove = _fishBig.GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.StopMovement();

			return 0.5f;
		}

		private float ShowHelpHandOnFishBig()
		{
			Vector2 fishBigFinalPos = new Vector2(_fishBig.transform.position.x - 2f, _fishBig.transform.position.y);

			MoveCursor(fishBigFinalPos);
			ShowCursor(true);

			return 0.5f;
		}

		private float SoundTutorial()
		{
			PlaySoundTutorial(SoundsSpeech.Tutorial.harpoonVsFishBig);

			return 0.5f;
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_TWO

		private float ShootFishBig()
		{
			Services.Get<IHarpoonGunManagerTutorial>().Shoot(_fishBig.transform.position);

			return 0;
		}

		private float HideHalphand()
		{
			ShowCursor(false);
			_timerManager.StartTimer(ShowCursor, true, 1f);

			return 1f;
		}
	
		#endregion

		// ==========================================================================

		#region ACTIONS_THREE

		// same as ACTIONS_FOUR

		#endregion

		// ==========================================================================

		#region ACTIONS_FOUR

		private float HideHalpHandFinal()
		{
			ShowCursor(false);

			return 5f;
		}

		private float NextTutorial()
		{
			Services.Get<IHammerVsDamage>().StartTutorial();

			return 0f;
		}

		#endregion

		// ==========================================================================

		#region MISC_ALL

		private void AddActionToList(List< Func<float> > actionList,  Func<float> action)
		{
			actionList.Add(action);
		}

		private IEnumerator Sequence(List< Func<float> > actionList)
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == actionList.Count)
				{
					break; 
				}

				delay = actionList[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion



	}
}