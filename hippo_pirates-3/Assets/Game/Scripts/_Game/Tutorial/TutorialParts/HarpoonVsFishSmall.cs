﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.Sounds;
using Pirates_3.Menu.Gameplay.MonoScripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class HarpoonVsFishSmall : AMonoTutorial, IHarpoonVsFishSmall
	{
		// FIELDS
		// --------------------------------------------------------
		//
		private GameObject _fishSmall;

		// --------------------------------------------------------
		// actions
		private readonly List< Func<float> > _actionsOne = new List< Func<float> >();
		private readonly List< Func<float> > _actionsTwo = new List< Func<float> >();
	


		// A_SERVICES
		protected override void InitAwake()
		{
			base.InitAwake();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			 // can't apply auto-assert here!
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _cameraUI);

			Assert.AreNotEqual(null, _helpHand);
			Assert.AreNotEqual(null, _animatorHelpHand);
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IHarpoonVsFishSmall>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IHarpoonVsFishSmall>();
		}



		// UNITY
		protected void Update()
		{
			CheckMouseClick();
		}



		// INTERFACES
		public void StartTutorial()
		{
			_currentTutorial = Tutorials.HARPOON_VS_FISH_SMALL;

			InitActionsOne();
			StartCoroutine( SequencesOne() );
		}



		// METHODS
		// ==========================================================================

		#region MOUSE

		private void CheckMouseClick()
		{
			if ( Input.GetMouseButtonDown(0) &&
				_currentTutorial == Tutorials.HARPOON_VS_FISH_SMALL )
			{
				Vector2 mousePos = GetMousePosition();
				if (IsClickHelpHand(mousePos))
				{
					ActionMouseClick(mousePos);
				}
			}
		}

		protected override void ActionMouseClick(Vector2 mousePos)
		{
			InitActionsTwo();
			StartCoroutine(SequencesTwo());
		}

		#endregion

		// ==========================================================================
	
		#region INIT_ACTIONS_ONE

		private void InitActionsOne()
		{
			AddActionOne(SpawnFishSmall);
			AddActionOne(StopFish);
			AddActionOne(ShowHelphandOnFishSmall);
			AddActionOne(SoundTutorial);
		}

		#endregion

		// ==========================================================================

		#region INIT_ACTIONS_TWO

		private void InitActionsTwo()
		{
			AddActionTwo(ShootFishSmall);
			AddActionTwo(HideHelpHandOnFishSmall);
			AddActionTwo(NextTutorial);
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_ONE

		private float SpawnFishSmall()
		{
			const float speed = 3f;
			_fishSmall = Services.Get<IFishSmallPoolTutorial>().ShowObject(Enumerators.Directions.LEFT, speed);

			return (7f / speed);
		}

		private float StopFish()
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _fishSmall);
	#endif
			#endregion

			var move = _fishSmall.GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, move);
	#endif
			#endregion
			move.StopMovement();

			return 1f;
		}

		private float ShowHelphandOnFishSmall()
		{
			Vector2 correctedPos = new Vector2(_fishSmall.transform.position.x + 1f, _fishSmall.transform.position.y);
			MoveCursor(correctedPos);
			ShowCursor(true);

			return 0.5f;
		}

		private float SoundTutorial()
		{
			PlaySoundTutorial(SoundsSpeech.Tutorial.harpoonVsFishSmall);

			return 0f;
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_TWO

		private float ShootFishSmall()
		{
			Services.Get<IHarpoonGunManagerTutorial>().Shoot(_helpHand.position);

			return 0f;
		}

		private float HideHelpHandOnFishSmall()
		{
			ShowCursor(false);

			return 0.5f;
		}

		private float NextTutorial()
		{
			Services.Get<IHarpoonVsTrashSmall>().StartTutorial();

			return 0.5f;
		}
	
		#endregion

		// ==========================================================================
	
		#region MISC_ONE

		private void AddActionOne(Func<float> action)
		{
			_actionsOne.Add(action);
		}

		private IEnumerator SequencesOne()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsOne.Count)
				{
					break;
				}

				delay = _actionsOne[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		// ==========================================================================

		#region MISC_TWO

		private void AddActionTwo(Func<float> action)
		{
			_actionsTwo.Add(action);
		}

		private IEnumerator SequencesTwo()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsTwo.Count)
				{
					break;
				}

				delay = _actionsTwo[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		// ==========================================================================

	

	}
}