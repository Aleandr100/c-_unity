﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class HarpoonVsTrashSmall : AMonoTutorial, IHarpoonVsTrashSmall
	{
		// FIELDS
		// --------------------------------------------------------
		//
		private GameObject _trashSmall;
		// --------------------------------------------------------
		// actions
		private readonly List< Func<float> > _actionsOne = new List< Func<float> >();
		private readonly List< Func<float> > _actionsTwo = new List< Func<float> >();
		// --------------------------------------------------------
	


		// A_SERVICES
		protected override void InitAwake()
		{
			base.InitAwake();
		}

		protected override void InitStart()
		{
		}

		protected override void AssertVariables()
		{
			// can't apply auto-assert here!
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _cameraUI);

			Assert.AreNotEqual(null, _helpHand);
			Assert.AreNotEqual(null, _animatorHelpHand);
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IHarpoonVsTrashSmall>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IHarpoonVsTrashSmall>();
		}



		// UNITY
		protected void Update()
		{
			CheckMouseClick();
		}



		// INTERFACES
		public void StartTutorial()
		{
			_currentTutorial = Tutorials.HARPOON_VS_TRASH_SMALL;

			InitActionsOne();
			StartCoroutine( SequencesOne() );
		}

	

		// METHODS
		// ==========================================================================

		#region MOUSE

		private void CheckMouseClick()
		{
			if ( Input.GetMouseButtonDown(0) &&
				_currentTutorial == Tutorials.HARPOON_VS_TRASH_SMALL )
			{
				Vector2 mousePos = GetMousePosition();
				if ( IsClickHelpHand(mousePos) )
				{
					ActionMouseClick(mousePos);
				}
			}
		}

		protected override void ActionMouseClick(Vector2 mousePos)
		{
			InitActionsTwo();
			StartCoroutine( SequencesTwo() );
		}

		#endregion

		// ==========================================================================

		#region INIT_ACTIONS_ONE

		private void InitActionsOne()
		{
			AddActionOne(SpawnTrashSmall);
			AddActionOne(StopTrashSmall);
			AddActionOne(HelpHandOnTrashSmall);
		}

		#endregion

		// ==========================================================================

		#region INIT_ACTIONS_TWO

		private void InitActionsTwo()
		{
			AddActionTwo(ShootTrashSmall);
			AddActionTwo(HideHelpHandOnTrashSmall);
			AddActionTwo(NextTutorial);
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_ONE

		private float SpawnTrashSmall()
		{
			const float speed = 3f;
			_trashSmall = Services.Get<ITrashSmallPoolTutorial>().ShowObject(speed);

			return (5.5f / speed);
		}

		private float StopTrashSmall()
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, _trashSmall);
	#endif
			#endregion

			// stop movement
			var move = _trashSmall.GetComponent<ABaseMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, move);
	#endif
			#endregion

			move.StopMovement();

			// set gravity off
			var rigid = _trashSmall.GetComponent<Rigidbody2D>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, rigid);
	#endif
			#endregion

			rigid.isKinematic = true;

			return 0.5f;
		}

		private float HelpHandOnTrashSmall()
		{
			MoveCursor(_trashSmall.transform.position);
			ShowCursor(true);

			return 0.5f;
		}

		#endregion

		// ==========================================================================

		#region ACTIONS_TWO

		private float ShootTrashSmall()
		{
			Services.Get<IHarpoonGunManagerTutorial>().Shoot(_helpHand.position);

			return 0f;
		}

		private float HideHelpHandOnTrashSmall()
		{
			ShowCursor(false);

			return 0.5f;
		}

		private float NextTutorial()
		{
			Services.Get<IScissorsVsRopeMove>().StartTutorial();

			return 0.5f;
		}
	
		#endregion

		// ==========================================================================

		#region MISC_ONE

		private void AddActionOne(Func<float> action)
		{
			_actionsOne.Add(action);
		}

		private IEnumerator SequencesOne()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsOne.Count)
				{
					break;
				}

				delay = _actionsOne[countActions].Invoke();
				countActions++;
			
				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		// ==========================================================================

		#region MISC_TWO

		private void AddActionTwo(Func<float> action)
		{
			_actionsTwo.Add(action);
		}

		private IEnumerator SequencesTwo()
		{
			int countActions = 0;
			float delay = 0f;

			while (true)
			{
				if (countActions == _actionsTwo.Count)
				{
					break;
				}

				delay = _actionsTwo[countActions].Invoke();
				countActions++;

				yield return new WaitForSeconds(delay);
			}
		}

		#endregion

		// ==========================================================================
	

	}
}