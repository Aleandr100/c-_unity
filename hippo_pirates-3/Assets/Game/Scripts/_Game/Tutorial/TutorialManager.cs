﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class TutorialManager : AMonoScript
	{


		// A_SERVICES
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			if ( IsTutorial() )
			{
				Services.Get<IHarpoonVsFishSmall>().StartTutorial();
				//Services.Get<IHarpoonVsTrashSmall>().StartTutorial();
				//Services.Get<IScissorsVsRopeMove>().StartTutorial();
				//Services.Get<IHarpoonVsFishBig>().StartTutorial();
				//Services.Get<IHammerVsDamage>().StartTutorial();
				//Services.Get<IMagnetVsDamage>().StartTutorial();
				//Services.Get<IWeldingVsDamage>().StartTutorial();
				//Services.Get<IScissorsVsDamage>().StartTutorial();
			}
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}



		// METHODS
		private bool IsTutorial()
		{
			return (LevelsController.Get_CurrentLevelSubType() == Enumerators.LevelsSubtypes.TUTORIAL);
		}



	}
}