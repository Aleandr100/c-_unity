﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Menu.Gameplay.Pools;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class FishSmallPoolTutorial : FishSmallPool, IFishSmallPoolTutorial
	{

	
		// A_SERVICES
		protected override void RegisterService()
		{
			Services.Add<IFishSmallPoolTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IFishSmallPoolTutorial>();
		}



		// INTERFACES
		public GameObject ShowObject(Enumerators.Directions direction, float speed)
		{
			GameObject go = ShowObject();

			FishSmallMove goMove = go.GetComponent<FishSmallMove>();
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			SetDirection(goMove, direction);
			SetSpeed(goMove, speed);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}



		// METHODS

		#region SPEED

		private void SetSpeed(FishSmallMove goMove, float mySpeed)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
			Assert.IsTrue(mySpeed > 0);
	#endif
			#endregion

			goMove.Set_Speed(mySpeed);
		}

		#endregion

		#region DIRECTIONS

		private void SetDirection(FishSmallMove goMove, Enumerators.Directions myDirect)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			if (myDirect == Enumerators.Directions.LEFT)
			{
				MoveLeft(goMove);
			}
			else if (myDirect == Enumerators.Directions.RIGHT)
			{
				MoveRight(goMove);
			}
		}

		private void MoveLeft(FishSmallMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.LEFT);
			goMove.Set_VectorMove(Vector2.left);
			goMove.transform.position = new Vector2(_startRightX, _tempStartY);
			CheckNeedFlip(goMove.transform, Enumerators.Directions.LEFT);
		}

		private void MoveRight(FishSmallMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.RIGHT);
			goMove.Set_VectorMove(Vector2.right);
			goMove.transform.position = new Vector2(_startLeftX, _tempStartY);
			CheckNeedFlip(goMove.transform, Enumerators.Directions.RIGHT);
		}

		#endregion

		#region FLIP

		private void CheckNeedFlip(Transform goTransf, Enumerators.Directions direction)
		{
			if (direction == Enumerators.Directions.LEFT)
			{
				if (goTransf.localScale.x < 0)
				{
					Flip(goTransf);
				}
			}
			else if (direction == Enumerators.Directions.RIGHT)
			{
				if (goTransf.localScale.x > 0)
				{
					Flip(goTransf);
				}
			}
		}

		private void Flip(Transform goTransf)
		{
			// pre-conditions
			#region ASSERTS
#if UNITY_EDITOR
			Assert.AreNotEqual(null, goTransf);
#endif
			#endregion

			Vector2 tempScale = goTransf.localScale;

			goTransf.localScale = new Vector2(-1 * tempScale.x, tempScale.y);
		}

		#endregion


	
	}
}