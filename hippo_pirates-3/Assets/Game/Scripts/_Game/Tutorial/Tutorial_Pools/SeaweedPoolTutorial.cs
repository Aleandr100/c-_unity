﻿
using Assets.Game.Scripts._Game.Interfaces.ITutorial.ITutorialPools;
using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Menu.Gameplay.Pools;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class SeaweedPoolTutorial : SeaweedPool, ISeaweedPoolTutorial
	{




		// A_SERVICES
		protected override void RegisterService()
		{
			Services.Add<ISeaweedPoolTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ISeaweedPoolTutorial>();
		}



		// INTERFACES
		public GameObject ShowObject(float speed)
		{
			// pre-conditions
			#region ASSERTS
			    #if UNITY_EDITOR
			    Assert.IsTrue(speed > 0);
			    #endif
			#endregion

			GameObject go = ShowObject();

			var goMove = go.GetComponent<SeaweedMove>();
			#region ASSERTS
			    #if UNITY_EDITOR
			    Assert.AreNotEqual(null, goMove);
			    #endif
			#endregion
			goMove.Set_Speed(speed);

			return go;
		}



	}
}