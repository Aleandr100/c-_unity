﻿

using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Menu.Gameplay.Pools;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class TrashBigPoolTutorial : TrashBigPool, ITrashBigPoolTutorial
	{



		// A_SERVICE
		protected override void RegisterService()
		{
			Services.Add<ITrashBigPoolTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<ITrashBigPoolTutorial>();
		}



		// INTERFACES
		public GameObject ShowObject(float speed)
		{
			GameObject go = ShowObject();

			var goMove = go.GetComponent<TrashBigMove>();
			#region ASSERTS
			    #if UNITY_EDITOR
			    Assert.AreNotEqual(null, goMove);
			    #endif
			#endregion
			goMove.Set_Speed(speed);

			return go;
		}



	}
}