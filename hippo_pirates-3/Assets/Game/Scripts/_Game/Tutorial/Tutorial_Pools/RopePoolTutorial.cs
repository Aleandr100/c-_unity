﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Menu.Gameplay.Pools;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class RopePoolTutorial : RopePool, IRopePoolTutorial
	{


		// A_SERVICES
		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IRopePoolTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IRopePoolTutorial>();
		}



		// INTERFACES
		public GameObject ShowObject(float speed)
		{
			// check - spawn Rope_Tytorial prefab
			GameObject go = GetBackupPrefab();

			var goMove = go.GetComponent<RopeMove>();
			#region ASSERTS
			    #if UNITY_EDITOR
			    Assert.AreNotEqual(null, goMove);
			    #endif
			#endregion

			Transform goTransf = go.transform;

			SetPosition(goTransf);
			SetParentToObject(goTransf);
			goMove.RecalculateSpeed(speed);

			return go;
		}



	}
}