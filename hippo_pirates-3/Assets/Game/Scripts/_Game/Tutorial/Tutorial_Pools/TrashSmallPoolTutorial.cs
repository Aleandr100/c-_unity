﻿
using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Menu.Gameplay.Pools;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class TrashSmallPoolTutorial : TrashSmallPool, ITrashSmallPoolTutorial
    {



    	// A_SERVICES
    	protected override void AssertVariables()
    	{
    		#region Auto Asserts
    		#if UNITY_EDITOR
    		    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
    		    autoAssertsHandler.AssertAllFieldsInExternalClass();
    		#endif
    		#endregion
    	}

    	protected override void RegisterService()
    	{
			Services.Add<ITrashSmallPoolTutorial>(this);
    	}

    	protected override void DeregisterService()
    	{
			Services.Remove<ITrashSmallPoolTutorial>();
    	}



    	// INTERFACES
    	public GameObject ShowObject(float speed)
    	{
    		// pre-conditions
    		#region ASSERTS
    		    #if UNITY_EDITOR
    		    Assert.IsTrue(speed > 0);
    		    #endif
    		#endregion

    		GameObject go = ShowObject();

		    var goMove = go.GetComponent<TrashSmallMove>();
		    #region ASSERTS
		        #if UNITY_EDITOR
		        Assert.AreNotEqual(null, goMove);
		        #endif
		    #endregion
			goMove.Set_Speed(speed);

    		// post-conditions
    		#region ASSERTS
    		    #if UNITY_EDITOR
    		    Assert.AreNotEqual(null, go);
    		    #endif
    		#endregion

    		return go;
    	}



    }
}