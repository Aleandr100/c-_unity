﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoScripts;
using Pirates_3.Menu.Gameplay.Pools;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class FishBigPoolTutorial : FishBigPool, IFishBigPoolTutorial
	{
		
	

		// A_SERVICES
		protected override void RegisterService()
		{
			Services.Add<IFishBigPoolTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IFishBigPoolTutorial>();
		}



		// INTERFACES
		public GameObject ShowObject(Enumerators.Directions direction, float speed)
		{
			GameObject go = ShowObject();

			var goMove = go.GetComponent<FishBigMove>();
			#region ASSERTS
			    #if UNITY_EDITOR
			    Assert.AreNotEqual(null, goMove);
			    #endif
			#endregion

			SetSpeed(goMove, speed);
			SetDirection(goMove, Enumerators.Directions.LEFT);

			// post-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, go);
	#endif
			#endregion

			return go;
		}



		// METHODS
		#region SPEED

		private void SetSpeed(FishBigMove goMove, float mySpeed)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
			Assert.IsTrue(mySpeed > 0);
	#endif
			#endregion

			goMove.Set_Speed(mySpeed);
		}

		#endregion

		#region DIRECTION

		private void SetDirection(FishBigMove goMove, Enumerators.Directions myDirect)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			if (myDirect == Enumerators.Directions.LEFT)
			{
				MoveLeft(goMove);
			}
			else if (myDirect == Enumerators.Directions.RIGHT)
			{
				MoveRight(goMove);
				Services.Get<ICameraManager>().MoveCameraLeft();
			}
		}

		private void MoveLeft(FishBigMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.LEFT);
			goMove.Set_VectorMove(Vector2.left);
			goMove.transform.position = new Vector2(_startRightX, _startY);
		}

		private void MoveRight(FishBigMove goMove)
		{
			// pre-conditions
			#region ASSERTS
	#if UNITY_EDITOR
			Assert.AreNotEqual(null, goMove);
	#endif
			#endregion

			goMove.Set_CurrentDirection(Enumerators.Directions.RIGHT);
			goMove.Set_VectorMove(Vector2.right);
			goMove.transform.position = new Vector2(_startLeftX, _startY);
		}

		#endregion



	}
}