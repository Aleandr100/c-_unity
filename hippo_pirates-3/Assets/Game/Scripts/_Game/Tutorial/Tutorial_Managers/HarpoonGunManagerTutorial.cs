﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoManagers;
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class HarpoonGunManagerTutorial : HarpoonGunManager, IHarpoonGunManagerTutorial
	{
		// PRIVATE
		// -----------------------------------
		// dependences
		private ITimerManager _timerManager;



		// CONSTRUCTOR
		[Inject]
		public void Constructor(ITimerManager timerManager)
		{
			_timerManager = timerManager;
		}



		// A_SERVICES
		protected override void RegisterService()
		{
			Services.Add<IHarpoonGunManagerTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IHarpoonGunManagerTutorial>();
		}

	
	
		// INTERFACES
		public void Shoot(Vector2 mousePos)
		{
			SetRotation(_objHarpoonGunRight, mousePos);
			_timerManager.StartTimer(CorrectDiverPosition, _diverReactionSpeed);
			_timerManager.StartTimer(Shoot, _objHarpoonGunRight, _rotateSpeed);
		}

	

	}
}