﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Common.Info.AnimationParameters;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class ToolsButtonsListenersTutorial : AMonoService, IToolsButtonsListenersTutorial
	{
		// FIELDS
		// --------------------------------------------------
		[Header("BUTTONS")]
		[SerializeField] private Button _buttonScissors;
		[SerializeField] private Button _buttonMagnet;
		[SerializeField] private Button _buttonHammer;
		[SerializeField] private Button _buttonWelding;
	
		// --------------------------------------------------
		private bool _isBlocked;



		// A_SCRIPTS
		protected override void InitAwake()
		{
		}

		protected override void InitStart()
		{
			_isBlocked = true;
		}

		protected override void AssertVariables()
		{
			#region Auto Asserts
	#if UNITY_EDITOR
			AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
			autoAssertsHandler.AssertAllFieldsInExternalClass();
	#endif
			#endregion
		}

		protected override void RegisterService()
		{
			Services.Add<IToolsButtonsListenersTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IToolsButtonsListenersTutorial>();
		}



		// UNITY
		private void Update()
		{
			CheckMouseClick();
		}


	
		// INTERFACES
		public void ActivateBlocking()
		{
			_isBlocked = true;
		}

		public void DeactivateBlocking()
		{
			_isBlocked = false;
		}

	

		// METHODS

		#region MOUSE

		private void CheckMouseClick()
		{
			if (Input.GetMouseButtonDown(0) &&
				IsPointerOverUIObject())
			{
				ActionsMouseClick();
			}
		}

		private void ActionsMouseClick()
		{
			if ( !_isBlocked &&
				IsClickToolButton() )
			{
				string buttonTag = GetToolButtonTag();

				if (buttonTag == Tags.ToolButtonScissors)
				{
					OnClickButtonScissors();
				}
				else if (buttonTag == Tags.ToolButtonMagnet)
				{
					OnClickButtonMagnet();
				}
				else if (buttonTag == Tags.ToolButtonHammer)
				{
					OnClickButtonHammer();
				}
				else if (buttonTag == Tags.ToolButtonWelding)
				{
					OnClickButtonWelding();
				}
			}
		}

		protected bool IsPointerOverUIObject()
		{
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
			{
				position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
			};

			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

			return results.Count > 0;
		}

		private bool IsClickToolButton()
		{
			return (EventSystem.current.currentSelectedGameObject != null);
		}

		private string GetToolButtonTag()
		{
			return (EventSystem.current.currentSelectedGameObject.tag);
		}

		#endregion

		#region ON_CLICK

		private void OnClickButtonScissors()
		{
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.SCISSORS_VS_ROPE_MOVE || 
				AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.SCISSORS_VS_DAMAGE)
			{
				Services.Get<IToolsManagerTutorial>().ActionsMouseClickToolButtonsTutorial(Enumerators.ToolsTypes.SCISSORS);
			}
		}

		private void OnClickButtonMagnet()
		{
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.MAGNET_VS_DAMAGE)
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.MAGNET);
			}
		}

		private void OnClickButtonHammer()
		{
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.HAMMER_VS_DAMAGE)
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.HAMMER);
			}
		}

		private void OnClickButtonWelding()
		{
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.WELDING_VS_DAMAGE)
			{
				Services.Get<IToolsManager>().ActionsMouseClickToolButtons(Enumerators.ToolsTypes.WELDING);
			}
		}

		#endregion

	

	}
}