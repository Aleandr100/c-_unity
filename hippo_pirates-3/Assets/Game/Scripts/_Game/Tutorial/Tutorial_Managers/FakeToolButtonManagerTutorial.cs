﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Menu.Gameplay.MonoManagers;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class FakeToolButtonManagerTutorial : FakeToolButtonManager, IFakeToolButtonManagerTutorial
	{
	


		// A_SERVICES
		protected override void RegisterService()
		{
			Services.Add<IFakeToolButtonManagerTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IFakeToolButtonManagerTutorial>();
		}


	
		// METHODS
		#region MOUSE_RELEASE

		protected override void ActionMouseRelease()
		{
			Show(false);

			Vector2 mousePos = GetMousePosition();
			Services.Get<IToolsManagerTutorial>().HandleFakeToolButtonMouseReleaseTutorial(mousePos);
		}

		#endregion

	

	}
}