﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

using Pirates_3.Common;
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoManagers;
using UnityEngine;

namespace Pirates_3.Menu.Gameplay.Tutorial
{
	public class ToolsManagerTutorial : ToolsManager, IToolsManagerTutorial
	{
	


		// A_SERVICES
		protected override void RegisterService()
		{
			Services.Add<IToolsManagerTutorial>(this);
		}

		protected override void DeregisterService()
		{
			Services.Remove<IToolsManagerTutorial>();
		}



		// INTERFACES
		public void ActionsMouseClickToolButtonsTutorial(Enumerators.ToolsTypes toolType)
		{
			if (IsUnlocked())
			{
				SetTool(toolType);
				Services.Get<IFakeToolButtonManagerTutorial>().Activate(toolType);
			}
		}

		public void HandleFakeToolButtonMouseReleaseTutorial(Vector2 mousePos)
		{
			Collider2D[] arrColls = GetClickedColliders(mousePos);

			if (arrColls != null)
			{
				if (arrColls.Length > 0)
				{
					Transform coll = GetFirstClickedCollider(arrColls);
					CheckColiderType(coll);
				}
			}
		}

		public override void RepairCompleted()
		{
			CheckTutorialProceedOnCompleateRepair();

			base.RepairCompleted();
		}



		// METHODS

		#region FOR_TUTORIAL

		private void CheckTutorialProceedOnCompleateRepair()
		{
			// ----------------------------------------------------------------------------
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.HAMMER_VS_DAMAGE)
			{
				Services.Get<IHammerVsDamage>().RepairCompleted();
			}
			// ----------------------------------------------------------------------------
			else if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.MAGNET_VS_DAMAGE)
			{
				Services.Get<IMagnetVsDamage>().RepairCompleted();
			}
			// ----------------------------------------------------------------------------
			else if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.WELDING_VS_DAMAGE)
			{
				Services.Get<IWeldingVsDamage>().RepairCompleted();
			}
			// ----------------------------------------------------------------------------
			else if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.SCISSORS_VS_DAMAGE)
			{
				Services.Get<IScissorsVsDamage>().RepairCompleted();
			}
			// ----------------------------------------------------------------------------
		}

		private void CheckTutorialProceedOnCompleteHandleThreat()
		{
			if (AMonoTutorial.Get_CurrentTutorial() == AMonoTutorial.Tutorials.SCISSORS_VS_ROPE_MOVE)
			{
				Services.Get<IScissorsVsRopeMove>().HandleRopeComplete();
			}
		}

		#endregion

		protected override void HandleThreatCompletedActions(Transform coll)
		{
			CheckTutorialProceedOnCompleteHandleThreat();
		
			base.HandleThreatCompletedActions(coll);
		}



	}
}