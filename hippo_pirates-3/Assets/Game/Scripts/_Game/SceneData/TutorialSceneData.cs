﻿
using Pirates_3.Common;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSceneData : AMonoService, ITutorialSceneData
{
	// FIELDS
	// --------------------------------------------------------
	[Header("CAMERA")]
	[SerializeField] private Camera _cameraUI;

	// --------------------------------------------------------
	[Header("HELP HAND")]
	[SerializeField] private Transform _helpHand;
	[SerializeField] private Animator _animatorHelpHand;

	// --------------------------------------------------------
	[Header("HELP HAND UI")]
	[SerializeField] private RectTransform _helpHandUI;
	[SerializeField] private Animator _animatorHelpHandUI;

	// --------------------------------------------------------
	[Header("TOOLS BUTTONS")]
	[SerializeField] private Button _buttonScissors;
	[SerializeField] private Button _buttonMagnet;
	[SerializeField] private Button _buttonHammer;
	[SerializeField] private Button _buttonWelding;

	// --------------------------------------------------------
	[Header("RACCOON")]
	[SerializeField] private Animator _animatorRaccoon;



	// A_SERVICES
	protected override void InitAwake()
	{
	}

	protected override void InitStart()
	{
	}

	protected override void AssertVariables()
	{
		#region Auto Asserts
		#if UNITY_EDITOR
		    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(this.GetType(), this);
		    autoAssertsHandler.AssertAllFieldsInExternalClass();
		#endif
		#endregion
	}

	protected override void RegisterService()
	{
		Services.Add<ITutorialSceneData>(this);
	}

	protected override void DeregisterService()
	{
		Services.Remove<ITutorialSceneData>();
	}



	// INTERFACE
	// ===================================================
	//
	public Camera Get_CameraUI()
	{
		return _cameraUI;
	}

	// ===================================================
	//
	public Transform Get_HelpHand()
	{
		return _helpHand;
	}

	public Animator Get_AnimatorHelpHand()
	{
		return _animatorHelpHand;
	}

	// ===================================================
	//
	public RectTransform Get_HelpHandUI()
	{
		return _helpHandUI;
	}

	public Animator Get_AnimatorHelpHandUI()
	{
		return _animatorHelpHandUI;
	}

	// ===================================================
	//
	public Button Get_ButtonScissors()
	{
		return _buttonScissors;
	}

	public Button Get_ButonMagnet()
	{
		return _buttonMagnet;
	}

	public Button Get_ButtonHammer()
	{
		return _buttonHammer;
	}

	public Button Get_ButtonWelding()
	{
		return _buttonWelding;
	}

	// ===================================================
	//
	public Animator Get_AnimatorRaccoon()
	{
		return _animatorRaccoon;
	}


}