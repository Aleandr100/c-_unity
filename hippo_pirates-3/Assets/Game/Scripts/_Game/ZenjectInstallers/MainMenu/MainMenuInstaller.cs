﻿
using UnityEngine;
using Zenject;

namespace Pirates_3.Menu.Main
{
	public class MainMenuInstaller : MonoInstaller<MainMenuInstaller>
	{
		// FIEDLS
		// ---------------------------------
		//
		[SerializeField] private MainMenuSceneData _mainMenuSceneData;



		// BINDINGS
		public override void InstallBindings()
		{
			Container.Bind<IMainMenuSceneData>().To<MainMenuSceneData>().FromInstance(_mainMenuSceneData).AsSingle().NonLazy();

			Container.BindAllInterfacesAndSelf<MainMenu>().To<MainMenu>().AsSingle().NonLazy();
			Container.BindAllInterfacesAndSelf<MainMenuButtonsListeners>().To<MainMenuButtonsListeners>().AsSingle().NonLazy();
			Container.BindAllInterfacesAndSelf<MainMenuButtonsClickHandler>().To<MainMenuButtonsClickHandler>().AsSingle().NonLazy();
		}



	}
}

