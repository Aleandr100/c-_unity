
using Zenject;

namespace Pirates_3.Common
{
	public class ProjectInstaller : MonoInstaller<ProjectInstaller>
	{



		// BINDINGS
		public override void InstallBindings()
		{
			Container.Bind<ITimerManager>().To<TimerManager>().AsSingle().NonLazy();
			Container.Bind<IAnimationManager>().To<AnimationManager>().AsSingle().NonLazy();
			Container.Bind<ISoundManager>().To<SoundManager>().AsSingle().NonLazy();
		}



	}
}

