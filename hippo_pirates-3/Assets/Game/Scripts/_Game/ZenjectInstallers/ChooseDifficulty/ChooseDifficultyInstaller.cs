
using Pirates_3.Common;
using Pirates_3.Menu.ChooseDifficulty;
using UnityEngine;
using Zenject;

public class ChooseDifficultyInstaller : MonoInstaller<ChooseDifficultyInstaller>
{
	// FIELDS
	// -----------------------------------------
	//
	[SerializeField] private ChooseDifficultySceneData _chooseDifficultySceneData;



	// BINDINGS
    public override void InstallBindings()
    {
		AssertVariables();

	    // scene data
		Container.Bind<IChooseDifficultySceneData>().To<ChooseDifficultySceneData>().FromInstance(_chooseDifficultySceneData).AsSingle().NonLazy();

	    //
	    Container.BindAllInterfacesAndSelf<ChooseDifficultyMenu>().To<ChooseDifficultyMenu>().AsSingle().NonLazy();
	    Container.BindAllInterfacesAndSelf<ChooseDifficultyButtonsListeners>().To<ChooseDifficultyButtonsListeners>().AsSingle().NonLazy();
	    Container.Bind<IChooseDifficultyButtonsClickHandler>().To<ChooseDifficultyButtonsClickHandler>().AsSingle().NonLazy();
	    Container.BindAllInterfacesAndSelf<ChooseDifficultyView>().To<ChooseDifficultyView>().AsSingle().NonLazy();
    }



	// METHODS
	private void AssertVariables()
	{
		#region Auto Asserts
		#if UNITY_EDITOR
		    AutoAssertsHandler autoAssertsHandler = new AutoAssertsHandler(GetType(), this);
		    autoAssertsHandler.AssertAllFieldsInExternalClass();
		#endif
		#endregion
	}



}