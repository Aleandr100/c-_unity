
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

public class GameplayInstaller : MonoInstaller<GameplayInstaller>
{
	// FIEDLS
	[SerializeField] private GameObject _objTutorialSceneData;

	private TutorialSceneData _tutorialSceneData;



	// BINDINGS
    public override void InstallBindings()
    {
	    InitSerializedFields();

	    Container.Bind<ITutorialSceneData>().To<TutorialSceneData>().FromInstance(_tutorialSceneData).AsSingle().NonLazy();
    }



	// METHODS
	private void InitSerializedFields()
	{
		_tutorialSceneData = _objTutorialSceneData.GetComponent<TutorialSceneData>();
		#region ASSERTS
		#if UNITY_EDITOR
			Assert.AreNotEqual(null, _tutorialSceneData);
		#endif
		#endregion
	}



}