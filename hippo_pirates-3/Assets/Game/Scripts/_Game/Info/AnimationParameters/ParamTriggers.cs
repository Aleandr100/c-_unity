﻿
namespace Pirates_3.Common.Info.AnimationParameters
{
	public class ParamTriggers
	{
		// ========================================================
		//
		public static class GiIntro
		{
			public static readonly string needIdle = "needIdle";
			public static readonly string needWalk = "needWalk";
			public static readonly string needJumpShort = "needJumpShort";
			public static readonly string needJumpLong = "needJumpLong";
			public static readonly string needThrowGold = "needThrowGold";
		}

		// ========================================================
		//
		public static class RaccoonIntro
		{
			public static readonly string needIdle = "needIdle";
			public static readonly string needWalk = "needWalk";
			public static readonly string introTavern = "introTavern";
			public static readonly string introCave = "introCave";
			public static readonly string needPullTorch = "needPullTorch";
			public static readonly string needThrowGold = "needThrowGold";
		}

		// ========================================================
		//
		public static class RaccoonLoose
		{
			public static readonly string needThrowKostil = "needThrowKostil";
			public static readonly string needLooseSpeech = "needLooseSpeech";
			public static readonly string needIdleLoose = "needIdleLoose";
		}

		// ========================================================
		//
		public static class SubmarineIntro
		{
			public static readonly string needUpDown = "needUpDown";
			public static readonly string needCloseHatch = "needCloseHatch";
			public static readonly string needRotate = "needRotate";
		}

		// ========================================================
		//
		public static class Submarine
		{
			public static readonly string wasHit = "wasHit";
			public static readonly string needOpenhatch = "needOpenHatch";
			public static readonly string needBalloons = "needBalloons";
		}

		// ========================================================
		//
		public static class Mine
		{
			public static readonly string needExplosion = "needExplosion";
		}

		// ========================================================
		//
		public static class GoldCoins
		{
			public static readonly string needCoins = "needCoins";
		}
		// ========================================================
	


	}
}