﻿
namespace Pirates_3.Common.Info.AnimationParameters
{
	public class ParamInts
	{
		// =========================================================
		//
		public static class HelpHand
		{
			public static readonly string type = "helphandType";
		
			public enum Types
			{
				HELPHAND_CLICK,
				HELPHAND_HOLD
			}
		}

		// =========================================================
		//
		public static class Balloon
		{
			public static readonly string color = "balloonColor";

			public enum Colors
			{
				BLUE,
				GREEN,
				RED,
				YELLOW
			}
		}

		// =========================================================
		//
		public static class Manipulator
		{
			public static readonly string direction = "manipulatorDir";

			public enum Directions
			{
				NONE = -999,
				LEFT = 0,
				RIGHT= 1
			}
		}

		// =========================================================
		//
		public static class ScrewWaves
		{
			public static readonly string direction = "screwWavesDir";

			public enum Directions
			{
				LEFT,
				UP
			}
		}

		// =========================================================
		//
		public static class Tools
		{
			public static readonly string selectedTool = "selectedTool";

			public enum SelectedTools
			{
				NONE = -999
			}
		}
		// =========================================================



	}
}