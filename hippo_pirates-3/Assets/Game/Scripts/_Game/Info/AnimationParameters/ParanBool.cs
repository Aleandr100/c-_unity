﻿
namespace Pirates_3.Common.Info.AnimationParameters
{
	public class ParamBools
	{
		// ====================================================================
		//
		public static class Raccoon
		{
			public static readonly string isSpeechOnDamage = "isSpeechOnDamage";
			public static readonly string isSpeechOnRepair = "isSpeechOnRepair";

			public static readonly string isSpeechWinTavern = "isSpeechWinTavern";
		}

		// ====================================================================
		//
		public static class Diver
		{
			public static readonly string isUsingTool = "isUsingTool";
			public static readonly string isShowStars = "isShowStars";

			public static readonly string isSpeechWinTavern = "isSpeechWinTavern";
		}

		// ====================================================================
		//
		public static class HarpoonableObject
		{
			public static readonly string isHit = "isHit";
			public static readonly string isTraped = "isTraped";
		}

		// ====================================================================
		//
		public static class Submarine
		{
			public static readonly string isRotating = "isRotating";
			public static readonly string isAlarm = "isAlarm";
			public static readonly string isLightUp = "isLightUp";
			public static readonly string isShowForceField = "isShowForceField";
		}

		// ====================================================================



	}
}