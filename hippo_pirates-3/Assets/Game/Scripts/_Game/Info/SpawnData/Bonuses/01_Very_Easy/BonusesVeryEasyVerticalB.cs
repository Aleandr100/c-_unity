﻿
namespace Pirates_3.Common.SpawnData
{
	public class BonusesVeryEasyVerticalB : ABonuses
	{


	
		// CONSTRUCTOR
		public BonusesVeryEasyVerticalB()
		{
			repairRate = 2;
			hpRestore = 2;
			armor = 2;

			//
			delaySpawn = 15f;
			timerSpawn = 17f;
		}
	


	}
}