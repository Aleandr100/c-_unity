﻿
namespace Pirates_3.Common.SpawnData
{
	public class BonusesEasyVerticalA : ABonuses
	{



		// CONSTRUCTOR
		public BonusesEasyVerticalA()
		{
			repairRate = 2;
			hpRestore = 1;
			armor = 1;

			//
			delaySpawn = 15f;
			timerSpawn = 25f;
		}


	
	}
}
