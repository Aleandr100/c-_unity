﻿
namespace Pirates_3.Common.SpawnData
{
	public class BonusesNormalHorizontalB : ABonuses
	{



		// CONSTRUCTORS
		public BonusesNormalHorizontalB()
		{
			repairRate = 2;
			hpRestore = 2;
			armor = 2;

			//
			delaySpawn = 15f;
			timerSpawn = 17f;
		}
	


	}
}