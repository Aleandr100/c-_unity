﻿
namespace Pirates_3.Common.SpawnData
{
	public class BonusesVeryEasyVerticalA : ABonuses
	{

	

		// CONSTRUCTOR
		public BonusesVeryEasyVerticalA()
		{
			repairRate = 2;
			hpRestore = 1;
			armor = 1;

			//
			delaySpawn = 15f;
			timerSpawn = 25f;
		}
	

	
	}
}