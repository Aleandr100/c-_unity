﻿
namespace Pirates_3.Common.SpawnData
{
	public class BonusesVeryEasyHorizontalA : ABonuses
	{


	
		// CONSTRUCTOR
		public BonusesVeryEasyHorizontalA()
		{
			repairRate = 1;
			hpRestore = 1;
			armor = 1;

			//
			delaySpawn = 25f;
			timerSpawn = 25f;
		}



	}
}