﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsNormalHorizontalB : AThreats
	{

	

		// CONSTRUCTOR
		public ThreatsNormalHorizontalB()
		{
			trashSmall = 5;
			trashBig = 2;

			fishSmall = 5;
			fishBig = 1;
			fishFast = 3;

			net = 0;
			rope = 2;
			seaweed = 3;

			chain = 1;
			mine = 1;
			rock = 1;

			//
			delaySpawn = 5f;
			timerSpawn = 5f;
		}
	


	}
}