﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsVeryEasyVerticalA : AThreats
	{
	


		// CONSTRUCTOR 
		public ThreatsVeryEasyVerticalA()
		{
			trashSmall = 8;
			trashBig = 1;

			fishSmall = 0;
			fishBig = 0;
			fishFast = 0;

			net = 0;
			rope = 0;
			seaweed = 0;

			chain = 0;
			mine = 2;
			rock = 2;

			//
			delaySpawn = 5f;
			timerSpawn = 9f;
		}



	}
}