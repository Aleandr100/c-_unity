﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsEasyVerticalB : AThreats
	{


	
		// CONSTRUCTOR
		public ThreatsEasyVerticalB()
		{
			trashSmall = 10;
			trashBig = 2;

			fishSmall = 0;
			fishBig = 0;
			fishFast = 0;

			net = 0;
			rope = 0;
			seaweed = 0;

			chain = 0;
			mine = 3;
			rock = 3;

			//
			delaySpawn = 5f;
			timerSpawn = 6.5f;
		}
	


	}
}