﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsEasyHorizontalA : AThreats
	{

	
	
		// CONSTRUCTOR
		public ThreatsEasyHorizontalA()
		{
			trashSmall = 4;
			trashBig = 2;

			fishSmall = 4;
			fishBig = 1;
			fishFast = 2;

			net = 0;
			rope = 0;
			seaweed = 2;

			chain = 0;
			mine = 1;
			rock = 1;

			//
			delaySpawn = 5f;
			timerSpawn = 7f;
		}
	


	}
}