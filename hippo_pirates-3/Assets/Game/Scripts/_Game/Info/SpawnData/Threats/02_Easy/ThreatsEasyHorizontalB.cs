﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsEasyHorizontalB : AThreats
	{



		// CONSTRUCTOR
		public ThreatsEasyHorizontalB()
		{
			trashSmall = 4;
			trashBig = 1;

			fishSmall = 4;
			fishBig = 1;
			fishFast = 2;

			net = 0;
			rope = 2;
			seaweed = 2;

			chain = 1;
			mine = 1;
			rock = 1;

			//
			delaySpawn = 5f;
			timerSpawn = 6.5f;
		}
	

	
	}
}