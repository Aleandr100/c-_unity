﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsVeryEasyHorizontalA : AThreats
	{
	

	
		// CONSTRUCTOR 
		public ThreatsVeryEasyHorizontalA()
		{
			trashSmall = 3;
			trashBig = 2;

			fishSmall = 3;
			fishBig = 1;
			fishFast = 1;

			net = 0;
			rope = 0;
			seaweed = 1;

			chain = 0;
			mine = 1;
			rock = 1;

			//
			delaySpawn = 5f;
			timerSpawn = 9f;
		}

	

	}
}