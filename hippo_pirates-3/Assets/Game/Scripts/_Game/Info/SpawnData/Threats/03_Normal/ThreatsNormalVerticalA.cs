﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsNormalVerticalA : AThreats
	{



		// CONSTRUCTOR
		public ThreatsNormalVerticalA()
		{
			trashSmall = 11;
			trashBig = 3;

			fishSmall = 0;
			fishBig = 0;
			fishFast = 0;

			net = 0;
			rope = 0;
			seaweed = 0;

			chain = 0;
			mine = 3;
			rock = 4;

			//
			delaySpawn = 5f;
			timerSpawn = 5.5f;
		}
	


	}
}