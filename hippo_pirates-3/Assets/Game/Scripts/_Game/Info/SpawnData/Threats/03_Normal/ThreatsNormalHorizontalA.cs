﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsNormalHorizontalA : AThreats
	{

	

		// CONSTRUCTOR
		public ThreatsNormalHorizontalA()
		{
			trashSmall = 5;
			trashBig = 2;

			fishSmall = 5;
			fishBig = 1;
			fishFast = 3;

			net = 0;
			rope = 0;
			seaweed = 3;

			chain = 0;
			mine = 1;
			rock = 1;

			//
			delaySpawn = 5f;
			timerSpawn = 5.5f;
		}
	

	
	}
}