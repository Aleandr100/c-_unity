﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsNormalVerticalB : AThreats
	{



		// CONSTRUCTOR
		public ThreatsNormalVerticalB()
		{
			trashSmall = 12;
			trashBig = 3;

			fishSmall = 0;
			fishBig = 0;
			fishFast = 0;

			net = 0;
			rope = 0;
			seaweed = 0;

			chain = 0;
			mine = 4;
			rock = 5;

			//
			delaySpawn = 5f;
			timerSpawn = 5f;
		}

	
	
	}
}