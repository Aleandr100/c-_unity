﻿
namespace Pirates_3.Common.SpawnData
{
	public class ThreatsVeryEasyHorizontalB : AThreats
	{
	

	
		// CONSTRUCTOR 
		public ThreatsVeryEasyHorizontalB()
		{
			trashSmall = 3;
			trashBig = 1;

			fishSmall = 3;
			fishBig = 1;
			fishFast = 1;

			net = 0;
			rope = 2;
			seaweed = 2;

			chain = 1;
			mine = 1;
			rock = 0;

			//
			delaySpawn = 5f;
			timerSpawn = 8.5f;
		}



	}
}