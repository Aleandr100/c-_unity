﻿
namespace Pirates_3.Common.SpawnData
{
	public abstract class AThreats
	{
		// FIELDS
		public int trashSmall;
		public int trashBig;

		public int fishSmall;
		public int fishBig;
		public int fishFast;

		public int net;
		public int rope;
		public int seaweed;

		public int chain;
		public int mine;
		public int rock;

		//
		public float delaySpawn;
		public float timerSpawn;


	}
}