﻿
namespace Pirates_3.Common.Info.AnimationParameters
{
	public static class Tags
	{
		// =============================================
		// STANDART
		public static readonly string GuiCamera = "GUICamera";

		// =============================================
		// TRASH
		public static readonly string TrashSmall = "Trash_Small";
		public static readonly string TrashBig = "Trash_Big";

		// =============================================
		// FISH
		public static readonly string FishSmall = "Fish_Small";
		public static readonly string FishBig = "Fish_Big";
		public static readonly string FishFast = "Fish_Fast";

		// =============================================
		// SCREW BREAK
		public static readonly string Net = "Screw_Net";
		public static readonly string Seaweed = "Screw_Seaweed";
		public static readonly string Rope = "Screw_Rope";
		public static readonly string CutRope = "Cut_Rope";

		// =============================================
		// SPECIAL
		public static readonly string Rock = "Rock";
		public static readonly string Mine = "Mine";
		public static readonly string Chain = "Chain";
		public static readonly string CutChain = "Cut_Chain";

		// =============================================
		// BONUSES
		public static readonly string BonusRepairRate = "Bonus_RepairRate";
		public static readonly string BonusHpRestore = "Bonus_HpRestore";
		public static readonly string BonusArmor = "Bonus_Armor";

		// =============================================
		// HULL
		public static readonly string HullOuter = "Hull_Outer";
		public static readonly string HullInner = "Hull_Inner";

		// =============================================
		// DAMEGE TYPES
		public static readonly string HullTorn = "_Hull_Torn";
		public static readonly string DentSmall = "_Dent_Small";
		public static readonly string DentBig = "_Dent_Big";
		public static readonly string Scratch = "_Scratch";
		public static readonly string Hole = "_Hole";
		public static readonly string BottomBreak = "_Bottom_Break";
		public static readonly string ScrewBreak = "_Screw_Break";

		// =============================================
		// MISC
		public static readonly string HarpoonGunLeft = "HarpoonStand_0";
		public static readonly string HarpoonGunRight = "HarpoonStand_1";
		public static readonly string Harpoon = "Harpoon";
		public static readonly string Treasure = "Treasure";

		// =============================================
		// TOOLS BUTTONS
		public static readonly string ToolButtonScissors = "ToolButton/Scissors";
		public static readonly string ToolButtonMagnet = "ToolButton/Magnet";
		public static readonly string ToolButtonHammer = "ToolButton/Hammer";
		public static readonly string ToolButtonWelding = "ToolButton/Welding";
		
		// =============================================



	}
}