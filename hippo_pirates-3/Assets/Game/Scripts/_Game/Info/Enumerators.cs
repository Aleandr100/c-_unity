﻿
namespace Pirates_3.Common.Info
{
	public class Enumerators
	{
		// ===========================================================
		//
		public enum Layers
		{
			HULL = 8,
			SCREW = 9,
			THREAT = 10,
			BONUS = 11,
			HARPOON = 12,

			HARPOON_GUN = 13,
			DAMAGE_TYPE = 14,
			HELP_HAND = 15
		}

		// ===========================================================
		//
		public enum LevelsTypes
		{
			HORIZONTAL, 
			VERTICAL
		}

		// ===========================================================
		//
		public enum LevelsSubtypes
		{
			TUTORIAL,

			HORIZONTAL_A,
			VERTICAL_A,
			HORIZONTAL_B,
			VERTICAL_B
		}

		// ===========================================================
		//
		public enum LevelDifficulty
		{
			VERY_EASY,
			EASY,
			NORMAL
		}

		// ===========================================================
		//
		public enum AgeType
		{
			TWO_PLUS,
			FIVE_PLUS
		}

		// ===========================================================
		//
		public enum Directions
		{
			LEFT,
			RIGHT
		}

		// ===========================================================
		//
		public enum FishDepths
		{
			TOP,
			CENTER,
			BOTTOM
		}

		// ===========================================================
		//
		public enum ThreatTypes
		{
			NONE,

			TRASH_SMALL,
			TRASH_BIG,

			FISH_SMALL,
			FISH_BIG,
			FISH_FAST,

			NET,
			ROPE,
			SEAWEED,

			CHAIN,
			MINE,
			ROCK
		}

		// ===========================================================
		//
		public enum DamageTypes
		{
			NONE,
			HULL_TORN,			
			DENT_SMALL,			
			DENT_BIG,           

			SCRATCH,			
			HOLE,				
			BOTTOM_BREAK,		

			SCREW_NET,			
			SCREW_ROPE,
			SCREW_SEAWEED		
		}

		// ===========================================================
		//
		public enum ToolsTypes
		{
			NONE = -999,
			SCISSORS = 0,		
			MAGNET = 1,			
			HAMMER = 2,			
			WELDING = 3			
		}

		// ===========================================================
		//
		public enum DiverStates
		{
			NONE,
			HARPOON_GUN,
			MOVE,
			REPARING,
			HANDLING_THREAT,
			STUNNED
		}

		// ===========================================================
		//
		public enum BonusTypes
		{
			BONUS_REPAIR_RATE,
			BONUS_HP_RESTORE,
			BONUS_ARMOR
		}
		// ===========================================================



	}
}