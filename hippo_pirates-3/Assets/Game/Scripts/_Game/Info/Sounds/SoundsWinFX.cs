﻿
namespace Pirates_3.Common.Info.Sounds
{
	public class SoundsWinFX
	{

		public static readonly string manipulator = "manipulator";

		public static readonly string coinsOne = "coins-1";
		public static readonly string coinsTwo = "coins-2";
		public static readonly string coinsThree = "coins-3";

	}
}