﻿
namespace Pirates_3.Common.Info.Sounds
{
	public class SoundsSpeech
	{
		// ========================================================
		//
		public class Tutorial
		{
			public static readonly string harpoonVsFishSmall = "dk-5";
			public static readonly string scissorsVsropeMove = "dk-6";
			public static readonly string harpoonVsFishBig = "dk-7";
			public static readonly string hammerVsDamage = "dk-8";
			public static readonly string magnetVsDamage = "dk-9";
			public static readonly string weldingVsDamage = "dk-10";
			public static readonly string scissorsVsDamage = "dk-11";

			public static readonly string endTutorialSpeech = "dk-12";
		}

		// ========================================================
		// 
		public class MainMenu
		{
			public static readonly string mainMenuRaccoon = "dk-1";
		}

		// ========================================================
		// 
		public class Intro
		{
			public static readonly string introTavernGiYeYe = "ji-1";
			public static readonly string introTavernRaccoon = "dk-2";
			public static readonly string introCaveRaccoon = "dk-3";
		}

		// ========================================================
		// 
		public class Gameplay
		{
			public static readonly string gameplayRaccoonStart = "dk-4";

			public static readonly string[] haveDmgSpeeches =
			{
				"dk-13",
				"dk-14",
				"dk-15",
				"dk-16",
				"dk-17",
				"dk-18"
			};

			public static readonly string[] repairedSpeeches =
			{
				"dk-19",
				"dk-20",
				"dk-21"
			};
		}

		// ========================================================
		// 
		public class Interlevels
		{
			public static readonly string endLevelOne = "dk-22";
		}

		// ========================================================
		// 
		public class Loose
		{
			public static readonly string loose = "dk-23";
		}

		// ========================================================
		//
		public class Win
		{
			public static readonly string winSunkShip = "dk-24";
			public static readonly string winTavern = "dk-25";
		}

		// ========================================================



	}
}