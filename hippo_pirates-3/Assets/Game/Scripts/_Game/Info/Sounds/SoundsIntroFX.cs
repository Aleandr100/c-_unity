﻿
namespace Pirates_3.Common.Info.Sounds
{
	public class SoundsIntroFX
	{
		// ========================================================
		//
		public class Gi
		{
			public static readonly string hippoRunSingle = "Н_2_short";
			public static readonly string hippoJump = "Н_3";
		}

		// ========================================================
		//
		public class Raccoon
		{
			public static readonly string raccoonKostilHit = "kostilHit";
			public static readonly string raccoonPullTorch = "torchPull";
			public static readonly string raccoonWalk = "Н_7";
		}

		// ========================================================
		// 
		public class Submarine
		{
			public static readonly string submarineCloseHatch = "Н_8";
			public static readonly string submarineLightsUpProjectors = "Н_9";
			public static readonly string submarineScrewRotate = "Н_10";
		}

		// ========================================================
		//
		public class Misc
		{
			public static readonly string standMovesDown = "Н_6";
		}

		// ========================================================



	}
}