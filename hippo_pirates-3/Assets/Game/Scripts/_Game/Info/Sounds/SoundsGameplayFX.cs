﻿
namespace Pirates_3.Common.Info.Sounds
{
	public class SoundsGameplayFX
	{
		// ========================================================
		//
		public class Tools
		{
			public static readonly string scissors = "scissors";
			public static readonly string magnet = "magnet";
			public static readonly string hammer = "hammer";
			public static readonly string welding = "welding";
		}

		// ========================================================
		//
		public class Shooting
		{
			public static readonly string harpoonGunShot = "harpoonGunShot";
			public static readonly string itemsInNet = "itemsInNet";
			public static readonly string balloonBlowing = "balloonBlowing";
			public static readonly string itemsStartFly = "itemsStartFly";
		}

		// ========================================================
		//
		public class DamageTypes
		{
			public static readonly string scratch = "scratch";
			public static readonly string hitSmall = "hitSmall";
			public static readonly string hitBig = "hitBig";
			public static readonly string bottomBreak = "bottomBreak";                  
			public static readonly string hole = "hole";                                       
			public static readonly string screwBreak = "screwBreak";
		}

		// ========================================================
		//
		public class Bonuses
		{
			public static readonly string bonusHit = "bonusHit";
			public static readonly string submarineHp = "submarineHp";
			public static readonly string forceField = "forceField";
		}
	
		// ========================================================
		//
		public class Misc
		{
			public static readonly string looseBalloons = "looseBalloons";
			public static readonly string looseGoesUp = "looseGoesUp";

			public static readonly string alarmCriticalDamage = "alarmCriticalDamage";
			public static readonly string mineExplosion = "mineExplosion";
			public static readonly string bubblesOnHit = "bubblesOnHit";
			public static readonly string toolSwap = "toolSwap";

			public static readonly string submarineCloseHatch = "submarineCloseHatch";
			public static readonly string submarineLightsUpProjectors = "submarineLightsUpProjectors";

			public static readonly string hitTreasure = "hitTreasure";
		}

		// ========================================================
		//
		public class Backgrounds
		{
			public static readonly string bgScrewRotate = "bgScrewRotate";
			public static readonly string bgUnderwater = "bgUnderwater";
		}

		// ========================================================



	}
}