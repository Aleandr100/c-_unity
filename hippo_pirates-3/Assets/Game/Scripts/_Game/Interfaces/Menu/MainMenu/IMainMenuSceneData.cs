﻿using UnityEngine;
using UnityEngine.UI;

namespace Pirates_3.Menu.Main
{
	public interface IMainMenuSceneData
	{

		//
		Animator Get_AnimatorRaccoon();
		Animator Get_AnimatorGi();

		//
		Button Get_ButtonSettings();
		Button Get_ButtonPlayTwoPlus();
		Button Get_ButtonPlayFivePlus();

		//
		GameObject Get_PromoModule();

	}
}