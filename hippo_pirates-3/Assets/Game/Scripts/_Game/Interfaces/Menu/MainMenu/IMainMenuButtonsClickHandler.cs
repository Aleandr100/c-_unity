﻿namespace Pirates_3.Menu.Main
{
	public interface IMainMenuButtonsClickHandler
	{

		void ShowOrHidePromoModule();
		void HandleClickButtonTwoPlus();
		void HandleClickButtonFivePlus();

	}
}