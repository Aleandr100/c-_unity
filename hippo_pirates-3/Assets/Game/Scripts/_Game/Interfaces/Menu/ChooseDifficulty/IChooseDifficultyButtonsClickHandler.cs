﻿namespace Pirates_3.Menu.ChooseDifficulty
{
	public interface IChooseDifficultyButtonsClickHandler
	{

		void HandleClickButtonVeryEasy();
		void HandleClickButtonEasy();
		void HandleClickButtonNormal();

		void HandleClickButtonMainMenu();

	}
}