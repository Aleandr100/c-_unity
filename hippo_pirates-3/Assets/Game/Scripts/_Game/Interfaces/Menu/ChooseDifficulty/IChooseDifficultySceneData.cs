﻿using UnityEngine;
using UnityEngine.UI;

namespace Pirates_3.Menu.ChooseDifficulty
{
	public interface IChooseDifficultySceneData
	{

		// buttons
		Button Get_ButtonVeryEasy();
		Button Get_ButtonEasy();
		Button Get_ButtonNormal();
		Button Get_ButtonMainMenu();

		// stars
		Sprite Get_SpriteStarEmpty();
		Sprite Get_SpriteStarGold();

		// very easy
		Image[] Get_ImagesStarsVeryEasy();

		// easy
		GameObject Get_ObjectEasy();
		Image[] Get_ImagesStarsEasy();
		Image Get_ImageEasy();

		// normal
		GameObject Get_ObjectNormal();
		Image[] Get_ImagesStarsNormal();
		Image Get_ImageNormal();

	}
}