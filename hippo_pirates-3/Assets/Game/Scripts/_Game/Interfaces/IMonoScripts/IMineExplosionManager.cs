﻿using UnityEngine;
using System.Collections.Generic;

public interface IMineExplosionManager
{

	//
	List<GameObject> Get_DestroyableObjects();

	//
	void AddToList(GameObject go);
	void AplyExplosion();

}
