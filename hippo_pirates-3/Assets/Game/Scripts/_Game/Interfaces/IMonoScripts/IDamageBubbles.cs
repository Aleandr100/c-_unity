﻿using UnityEngine;

public interface IDamageBubbles
{

	void IncreaseDamageCount();
	void ReduceDamageCount();
	
}
