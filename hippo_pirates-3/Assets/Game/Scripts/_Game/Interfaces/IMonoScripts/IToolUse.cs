﻿
using Pirates_3.Common.Info;
using UnityEngine;

public interface IToolUse
{

	bool IsRightTool(Enumerators.ToolsTypes toolType);

	bool StartHandleThreat(Enumerators.ToolsTypes toolType, Transform coll);

}
