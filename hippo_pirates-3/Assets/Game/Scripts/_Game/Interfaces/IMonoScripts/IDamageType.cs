﻿
using Pirates_3.Common.Info;
using Pirates_3.Menu.Gameplay.MonoScripts;

public interface IDamageType
{

	// getters/setters
	ABaseDamageType.DamageCond Get_CurrentDamageCondition(); 

	//
	bool IsRightTool(Enumerators.ToolsTypes toolType);
	bool StartRepair(Enumerators.ToolsTypes toolType, bool isBonusRepairRate);
	
	void ShowDamage();
	void ShowDamage(int num);
	
}
