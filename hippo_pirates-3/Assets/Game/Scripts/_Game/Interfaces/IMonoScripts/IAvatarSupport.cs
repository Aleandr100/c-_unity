﻿using UnityEngine;

public interface IAvatarSupport
{

    GameObject Get_AvatarLink();
    void Set_AvatarLink(GameObject go);
    
	void HideAvatar();

}
