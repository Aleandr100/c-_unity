﻿using UnityEngine;

public interface IHarpoonCatch
{

	void CatchActions();
	Transform Get_HarpoonBindPlace();

}
