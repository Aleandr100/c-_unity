﻿using UnityEngine;

public interface IHarpoonPool 
{

	GameObject ShowObject(Transform myHarpoon, int harpType);
	
}
