﻿using UnityEngine;

public interface IRopePool
{

	GameObject ShowObject();
	void CutRope(Transform go, Transform coll);

}
