﻿using UnityEngine;

namespace Assets.Game.Scripts._Game.Interfaces.IPools
{
	public interface ITreasurePool
	{

		GameObject ShowObject();

	}
}