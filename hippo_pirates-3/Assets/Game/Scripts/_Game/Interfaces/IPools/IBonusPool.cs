﻿using Pirates_3.Common.Info;
using UnityEngine;

public interface IBonusPool
{

	GameObject ShowObject(Enumerators.BonusTypes bonusType);

}
