﻿
using UnityEngine;

public interface ISeaweedPool
{

	GameObject ShowObject();
	void CutSeaweed(Transform go);

}
