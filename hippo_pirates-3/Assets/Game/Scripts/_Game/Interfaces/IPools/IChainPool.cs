﻿using UnityEngine;

public interface IChainPool
{

	GameObject ShowObject();
	void CutChain(Transform go, Transform coll);

}
