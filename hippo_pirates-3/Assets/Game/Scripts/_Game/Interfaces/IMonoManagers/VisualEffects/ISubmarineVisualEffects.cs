﻿
public interface ISubmarineVisualEffects
{

	void HitSubmarine();

	void OpenHatch();
	void BlowBalloons();

	void LightProjectors(bool boolValue);

	void StartScrewRotate();
	void StopScrewRotate();

	void StartScrewWavesHorizontal();
	void StartScrewWavesVertical();

	void ShowForceField(bool boolValue);

}
