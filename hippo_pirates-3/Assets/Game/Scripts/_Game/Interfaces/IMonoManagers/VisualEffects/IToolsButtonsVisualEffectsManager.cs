﻿
using Pirates_3.Common.Info;

public interface IToolsButtonsVisualEffectsManager
{

	void CheckShowButtonVisualEffects(Enumerators.DamageTypes dmgType);
	void CheckHideButtonVisualEffects(Enumerators.DamageTypes dmgType);

}
