﻿
using Pirates_3.Common.Info;

public interface IDiverVisualEffects
{

	void ShowToolInHands(Enumerators.ToolsTypes toolType);
	void AnimateToolInHands(bool boolValue);
	void ShowShortToolAnimation();

	void ShowDiverStars(float starsTime);
	void ShowRepairBar(float repairTime);
	void ShowReloadBar(float reloadTime);
	void ShowHarpoonLabel(bool boolValue);

}
