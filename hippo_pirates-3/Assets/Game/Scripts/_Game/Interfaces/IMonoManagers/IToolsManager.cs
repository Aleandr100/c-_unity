﻿
using Pirates_3.Common.Info;
using UnityEngine;

public interface IToolsManager
{

	void ActionsMouseClickToolButtons(Enumerators.ToolsTypes toolType);
	void HandleFakeToolButtonMouseRelease(Vector2 mousePos);
	void RepairCompleted();
	void TryHandleThreat(Transform coll);

	void ActivateBonusRepairRate();
	void DeactivateBonusRepairRate();

	
}
