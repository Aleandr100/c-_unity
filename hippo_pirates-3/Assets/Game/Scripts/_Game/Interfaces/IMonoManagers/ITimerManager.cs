﻿using Pirates_3.Common;

public interface ITimerManager
{
	void StartTimer(TimerManager.OnTimerVoid handler, float delay);
	void StartTimer<T>(TimerManager.OnTimer<T> handler, T param, float delay);
	void StartTimer<T>(TimerManager.OnTimerObjects<T> handler, T[] param, float delay);
}
