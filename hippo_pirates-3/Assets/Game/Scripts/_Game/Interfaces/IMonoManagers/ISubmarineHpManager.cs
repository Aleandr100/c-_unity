﻿
using Pirates_3.Common.Info;

public interface ISubmarineHpManager
{

	void AddDmgPerSec(Enumerators.DamageTypes dmgType);
	void ReduceDmgPerSec(Enumerators.DamageTypes dmgType);
	void RestoreHp();
	void SetHpEmpty();

}
