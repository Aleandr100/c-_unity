﻿using UnityEngine;

public interface IAvatarsManager
{

	void ActivateThreat(GameObject go);
	void DeactivateThreat(GameObject go);
	void DeactivateAllThreats();
	
	void ActivateBonusArmor();
	void DeactivateBonusArmor();

	void ActivateBonusRepairRate();
	
	void ShowHpOnHit(GameObject go, int hits);

}
