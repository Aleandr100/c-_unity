﻿
using Pirates_3.Common.Info;

public interface IChooseDifficulty
{

	void HandleClickDifficultyButton(Enumerators.LevelDifficulty levelDifficulty);

}
