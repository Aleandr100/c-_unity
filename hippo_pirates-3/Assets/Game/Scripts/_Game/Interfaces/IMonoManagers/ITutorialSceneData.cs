﻿
using UnityEngine;
using UnityEngine.UI;

public interface ITutorialSceneData
{

	//
	Camera Get_CameraUI();

	//
	Transform Get_HelpHand();
	Animator Get_AnimatorHelpHand();

	//
	RectTransform Get_HelpHandUI();
	Animator Get_AnimatorHelpHandUI();

	//
	Button Get_ButtonScissors();
	Button Get_ButonMagnet();
	Button Get_ButtonHammer();
	Button Get_ButtonWelding();

	//
	Animator Get_AnimatorRaccoon();

}
