﻿
using Pirates_3.Common.Info;

public interface ICriticalDamageManager
{

	void CheckDamageType(Enumerators.DamageTypes myDmgType);
	void RepairCritDamage(Enumerators.DamageTypes myDmgType);

}
