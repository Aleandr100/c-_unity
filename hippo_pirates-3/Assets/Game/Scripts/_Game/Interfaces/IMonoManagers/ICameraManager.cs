﻿
public interface ICameraManager
{

	void MoveCameraCenter();
	void MoveCameraCenter(float myDelay);

	void MoveCameraLeft();
	void MoveCameraLeft(float myDelay);

	void MoveCameraRight();
	void MoveCameraRight(float myDelay);

	void SetCameraHorizontalLevel();
	void SetCameraVerticalLevel();

}
