﻿using UnityEngine;

public interface IDiverMovementManager
{

	float MoveDiver(Vector2 pos);
	void MoveDiverToNearestHarpoon();

	void ActivateDiverStunEffect();

	void FlipDiverLeft();
	void FlipDiverRight();

	void StartDiverCorrectionCourse(Transform coll);

}
