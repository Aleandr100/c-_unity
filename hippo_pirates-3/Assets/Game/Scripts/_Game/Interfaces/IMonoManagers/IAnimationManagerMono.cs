﻿using UnityEngine;

public interface IAnimationManagerMono
{

	void SetParameterBool(Animator myAnimator, string myParam, bool myBool);
	void SetParameterBool(object[] param);

	void SetParameterInt(Animator myAnimator, string myParam, int myInt);
	void SetParameterInt(object[] param);

	void SetParameterTrigger(Animator myAnimator, string myTrig);
	void SetParameterTrigger(object[] param);

}