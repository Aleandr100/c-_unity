﻿using UnityEngine;
using System.Collections.Generic;
using Pirates_3.Common.Info;

public interface IDamageManager
{

	// getters/setters
	List<Transform> Get_HullTornList();
	List<Transform> Get_DentSmallList();
	List<Transform> Get_DentBigList();
	List<Transform> Get_ScratchList();
	List<Transform> Get_HoleList();
	List<Transform> Get_BottomBreakList();
	List<Transform> Get_ScrewSeaweedList();
	
	//
	void SetDirectDamage(GameObject coll, Enumerators.DamageTypes dmgType, int secNum);
	void SetRandomDamage(GameObject coll, Enumerators.DamageTypes dmgType, int num);
	void SetScrewDamage(GameObject coll, Enumerators.DamageTypes dmgType);

	void ActivateBonusArmor();
	void DeactivateBonusArmor();

}
