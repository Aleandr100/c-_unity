﻿
public interface ISoundManager
{

	void PlaySound(string strSound);
	void PlaySoundRepeating(string strSound);
	void StopSound(string strSound);

	void PlayMusic(string strMusic);

	float GetSoundLenght(string strSound);
	string GetRandomSound(string[] someArray);
	void StopAllRaccoonSpeeches();

	void SetMisicVolume();

}
