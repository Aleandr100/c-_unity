﻿
public interface ISaveGameManager
{

	//
	int Get_CountStarsVeryEasy();
	void Set_CountStarsVeryEasy(int number);

	int Get_CountStarsEasy();
	void Set_CountStarsEasy(int number);

	int Get_CountStarsNormal();
	void Set_CountStarsNormal(int number);

	//
	int Get_MaxCountStars();

	//
	bool Get_IsTutorialCompleted();
	void Set_IsTutorialCompleted();


}
