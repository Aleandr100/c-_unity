﻿using Pirates_3.Menu.Gameplay;
using UnityEngine;

namespace Assets.Game.Scripts._Game.Interfaces.IMonoManagers
{
	public interface IBottomSpeedManager
	{

		//
		BottomSpeedManager.SpeedType Get_Speedtype();

		//
		void SetSpeedNormal();
		void SetSpeedSlow();

		//
		void AddToStaticBottomObjectsList(GameObject go);

	}
}