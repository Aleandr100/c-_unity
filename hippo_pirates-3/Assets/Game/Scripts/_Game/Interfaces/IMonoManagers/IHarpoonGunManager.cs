﻿
public interface IHarpoonGunManager
{

	void ResetHarpoonGunsRotation();

	void ActivateBonusRepairRate();
	void DeactivateBonusRepairRate();

}
