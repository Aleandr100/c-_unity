﻿using Pirates_3.Menu.Gameplay.MonoManagers;

public interface ITimerManagerMono
{

	void StartTimer(TimerManagerMono.OnTimerVoid handler, float delay);
	void StartTimer<T>(TimerManagerMono.OnTimer<T> handler, T param, float delay);
	void StartTimer<T>(TimerManagerMono.OnTimerObjects<T> handler, T[] param, float delay);


}
