﻿namespace Assets.Game.Scripts._Game.Interfaces.IMonoManagers
{
	public interface IGameplaySpawnController
	{

		bool Get_IsEndLevelTimer();

	}
}