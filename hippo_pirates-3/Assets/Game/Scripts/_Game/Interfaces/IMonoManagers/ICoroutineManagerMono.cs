﻿using Pirates_3.Common;
using UnityEngine;

namespace Assets.Game.Scripts._Game.Interfaces.IMonoManagers
{
	public interface ICoroutineManagerMono
	{

		CoroutineManagerMono Get_Instance();

	}
}