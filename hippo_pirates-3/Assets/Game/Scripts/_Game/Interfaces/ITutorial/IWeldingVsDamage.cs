﻿using UnityEngine;

public interface IWeldingVsDamage 
{

	void StartTutorial();
	void SubmarineHaveDamage(Transform damge);
	void RepairCompleted();

}
