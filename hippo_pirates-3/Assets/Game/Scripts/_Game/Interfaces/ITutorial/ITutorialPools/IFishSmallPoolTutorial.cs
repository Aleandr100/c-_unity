﻿using Pirates_3.Common.Info;
using UnityEngine;

public interface IFishSmallPoolTutorial
{

	GameObject ShowObject(Enumerators.Directions direction, float speed);

}
