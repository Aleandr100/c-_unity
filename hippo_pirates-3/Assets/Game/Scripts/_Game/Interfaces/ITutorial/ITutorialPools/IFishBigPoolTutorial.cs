﻿using Pirates_3.Common.Info;
using UnityEngine;

public interface IFishBigPoolTutorial
{

	GameObject ShowObject(Enumerators.Directions direction, float speed);

}
