﻿
using UnityEngine;

namespace Assets.Game.Scripts._Game.Interfaces.ITutorial.ITutorialPools
{
	public interface ISeaweedPoolTutorial
	{

		GameObject ShowObject(float speed);

	}
}