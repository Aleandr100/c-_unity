﻿
using UnityEngine;

public interface IMagnetVsDamage 
{

	void StartTutorial();
	void SubmarineHaveDamage(Transform damage);
	void RepairCompleted();

}
