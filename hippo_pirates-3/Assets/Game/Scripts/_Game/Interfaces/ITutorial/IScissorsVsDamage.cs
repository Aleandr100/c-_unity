﻿using UnityEngine;

public interface IScissorsVsDamage
{

	void StartTutorial();
	void SubmarineHaveDamage(Transform damage);
	void RepairCompleted();

}
