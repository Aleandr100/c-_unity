﻿
using Pirates_3.Common.Info;
using UnityEngine;

public interface IToolsManagerTutorial
{

	void ActionsMouseClickToolButtonsTutorial(Enumerators.ToolsTypes toolType);
	void HandleFakeToolButtonMouseReleaseTutorial(Vector2 mousePos);
	void RepairCompleted();
	void TryHandleThreat(Transform coll);

}
