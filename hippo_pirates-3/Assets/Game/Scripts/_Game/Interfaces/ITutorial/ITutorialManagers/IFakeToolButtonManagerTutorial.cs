﻿
using Pirates_3.Common.Info;

public interface IFakeToolButtonManagerTutorial
{

	void Activate(Enumerators.ToolsTypes toolType);

}
