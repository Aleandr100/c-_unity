﻿
using UnityEngine;

public interface IHammerVsDamage 
{

	void StartTutorial();
	void SubmarineHaveDamage(Transform damage);
	void RepairCompleted();

}
