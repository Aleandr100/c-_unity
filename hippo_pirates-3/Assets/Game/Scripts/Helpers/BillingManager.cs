﻿using UnityEngine;
using OnePF;
using System.Collections;
using System.Collections.Generic;

public class BillingManager : MonoBehaviour
{
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
    static public BillingManager Instance;

	public const string SKU_ADMOB = "admob";
	//	public const string SKU_COINSLITE = "coinslite";
	//	public const string SKU_COINSLARGE = "coinslarge";
	//	public const string SKU_COINSEXTRALARGE = "coinsxl";

	//const string PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiRQ81aWh2Wo6D/xR5k3BSnktb4D7D2RUqKv1M6D8a5XZwLZbULWQd5nnmvu61YAZqZORiiiJ8vkYD3lYk1Oh7XTKVe3CMAP8ZXj990a8JZ5g/s3eAsv9NIS4Kfr/GUQZrgomS6shJCjN5Y/Xk7CDfmEQlsommMd3rzlpBXG9wUFw8s+XYGNyuvuowRyZ/+RfH8ZVxULFpYLYBCwLDwbcQTUZuKaHWgO9RmLvuh51Mh2U4Ec1AdhsCR5Xngk85lHBLfSm1F3ujfI+65l7xdut6BWgaIE+CcPGYlg7WtzensUg1S/TgpLtJ1/eCFBOAijZilDZhvnsxKW86SAyqRBtHwIDAQAB";

	private const string PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjmXOfbI0ixfcCBuQQ5/E13MfgBzGTX0IHlmklSCyKU1hSwTX2KzTzIUZVnN4Umdles9SPnsOU4f40RT0U81E8QRBCWwp+KXkG/AWzJr96N13KA3VGHsMrLdgZHhiXh6oZxgHlMJaSEKEQoEMt3Zv+ncVFznKHlEuDvInAuxPBa6k8mlraBzlKjVUg6HWD7fwCbm7cz+VizPw4Vui6GvkhAL2oam4BfG0TRrQsn16lXPQ3f/NNjCqnNPs/ZERDtylW80TiNf7l1fzJDtB/IZ01xoITE5jNmk1LlDNL0ZJKYc/lt8xtH3SfJwA+++oppy7DHZiTmig6iIKCNzPBXurwIDAQAB";

	bool _isInitialized = false;
	Inventory _inventory = null;

	public bool IsInitialized {
		get {
			return _isInitialized;
		}
	}

	void Awake()
	{
		if (!Instance)
			{
				Instance = this;
			}
	}

	private void OnEnable()
	{
		// Listen to all events for illustration purposes
		OpenIABEventManager.billingSupportedEvent += billingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent += purchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
	}

	private void OnDisable()
	{
		// Remove all event handlers
		OpenIABEventManager.billingSupportedEvent -= billingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
	}

	public void MapSKU()
	{
		OpenIAB.mapSku(SKU_ADMOB, OpenIAB_Android.STORE_GOOGLE, "admob");
//		OpenIAB.mapSku (SKU_COINSLITE, OpenIAB_Android.STORE_GOOGLE, "coinslite");
//		OpenIAB.mapSku (SKU_COINSLARGE, OpenIAB_Android.STORE_GOOGLE, "coinslarge");
//		OpenIAB.mapSku (SKU_COINSEXTRALARGE, OpenIAB_Android.STORE_GOOGLE, "coinsxl");

		OpenIAB.mapSku(SKU_ADMOB, OpenIAB_iOS.STORE, "admob");
//		OpenIAB.mapSku (SKU_COINSLITE, OpenIAB_iOS.STORE, "coinslite");
//		OpenIAB.mapSku (SKU_COINSLARGE, OpenIAB_iOS.STORE, "coinslarge");
//		OpenIAB.mapSku (SKU_COINSEXTRALARGE, OpenIAB_iOS.STORE, "coinsxl");

		OpenIAB.mapSku(SKU_ADMOB, OpenIAB_WP8.STORE, "admob");
//		OpenIAB.mapSku (SKU_COINSLITE, OpenIAB_WP8.STORE, "coinslite");
//		OpenIAB.mapSku (SKU_COINSLARGE, OpenIAB_WP8.STORE, "coinslarge");
//		OpenIAB.mapSku (SKU_COINSEXTRALARGE, OpenIAB_WP8.STORE, "coinsxl");
	}

	public void Init()
	{
		var options = new Options();
		options.checkInventoryTimeoutMs = Options.INVENTORY_CHECK_TIMEOUT_MS * 2;
		options.discoveryTimeoutMs = Options.DISCOVER_TIMEOUT_MS * 2;
		options.checkInventory = false;
		options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;
		options.storeKeys = new Dictionary<string, string> { { OpenIAB_Android.STORE_GOOGLE, PUBLIC_KEY } };
		options.storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT;
		
		OpenIAB.init(options);
	}

	public void Purchase(string SKU)
	{
		if (_isInitialized)
			{
				OpenIAB.purchaseProduct(SKU);
			} else
			{
				Init();
			}
	}

	public void PurchaseSubscription(string SKU)
	{
		if (_isInitialized)
			{
				OpenIAB.purchaseSubscription(SKU);
			} else
			{
				Init();
			}
	}

	private void billingSupportedEvent()
	{
		_isInitialized = true;
		//Debug.Log("billingSupportedEvent");
	}

	private void billingNotSupportedEvent(string error)
	{
		//Debug.Log("billingNotSupportedEvent: " + error);
	}

	private void queryInventorySucceededEvent(Inventory inventory)
	{
		//Debug.Log("queryInventorySucceededEvent: " + inventory);
		if (inventory != null)
			{
				_inventory = inventory;
			}
	}

	private void queryInventoryFailedEvent(string error)
	{
		//Debug.Log("queryInventoryFailedEvent: " + error);
	}

	private void purchaseSucceededEvent(Purchase purchase)
	{
		//Debug.Log("purchaseSucceededEvent: " + purchase);
		switch (purchase.Sku)
			{
			case SKU_ADMOB:
				{
					ManagerGoogle.Instance.DisableAdmob();
					break;
				}
				;
//			}case SKU_COINSLITE: {
//	//			ShopController.Instance.CoinsPurchaseLite();
//				break;
//			}case SKU_COINSLARGE: {
//	//			ShopController.Instance.CoinsPurchaseLarge();
//				break;
//			}case SKU_COINSEXTRALARGE: {
//	//			ShopController.Instance.CoinsPurchaseXL();
//			
//				break;
//			};
			}
	}

	private void purchaseFailedEvent(int errorCode, string errorMessage)
	{
		//Debug.Log("purchaseFailedEvent: " + errorMessage);
	}

	private void consumePurchaseSucceededEvent(Purchase purchase)
	{
		Debug.Log("consumePurchaseSucceededEvent: " + purchase);
	}

	private void consumePurchaseFailedEvent(string error)
	{
		//Debug.Log("consumePurchaseFailedEvent: " + error);
	}


#endif
}
