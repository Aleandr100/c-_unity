﻿#define NEAT_PLUG
//#define GOOGLE_ADS

using UnityEngine;
using System.Collections.Generic;
#if GOOGLE_ADS
using GoogleMobileAds.Api;
#endif
namespace PSV_Prototype
{

    public enum Scenes  //List here all scene names included in the build   (permanent scenes are InitScene, PSV_Splash (or other), Push)
    {
	    InitScene,
	    PSV_Splash,
	    Push,

	    // -----------------------------
	    MainMenu,
	    ChooseDifficulty,

	    // -----------------------------
	    Intro_Tavern,
	    Intro_SecretCave,

	    // -----------------------------
	    Gameplay,

	    // -----------------------------
	    LooseMenu,

	    WinMenu_SunkShip,
	    WinMenu_Tavern,

	    // -----------------------------
	    Interlevel_HorA_VertA,
	    Interlevel_VertA_HorB,
	    Interlevel_HorB_VertB,

	    // -----------------------------
	    RateUs,
    }

    public static class SceneLoaderSettings
    {

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1

#if GOOGLE_ADS
    private AdPosition
        small_banner_default_pos = AdPosition.Bottom;        //overrides AdmobAdPosition



    private Dictionary<Scenes, AdPosition>
        small_banner_override = new Dictionary<Scenes, AdPosition> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
        {
            //{ Scenes.MainMenu, AdPosition.BottomRight },
        };
#endif
#if NEAT_PLUG
        public static AdmobAd.AdLayout
            small_banner_default_pos = AdmobAd.AdLayout.Bottom_Left;        //overrides AdmobAdPosition

        public static Dictionary<Scenes, AdmobAd.AdLayout>
            small_banner_override = new Dictionary<Scenes, AdmobAd.AdLayout> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
            {
                //{ Scenes.MainMenu, AdmobAd.AdLayout.BottomRight },
            };
#endif

        public static List<Scenes>
            not_allowed_small_banner = new List<Scenes> ( ) //list here scenes that shouldn't show ad 
            {
	            Scenes.InitScene,
	            Scenes.PSV_Splash,
	            Scenes.Push,
	            Scenes.RateUs,
	            Scenes.MainMenu,
	            Scenes.ChooseDifficulty,
	            Scenes.Intro_Tavern,
	            Scenes.Intro_SecretCave,
	            Scenes.Gameplay,
	            Scenes.LooseMenu,
	            Scenes.WinMenu_SunkShip,
	            Scenes.WinMenu_Tavern,
	            Scenes.Interlevel_HorA_VertA,
	            Scenes.Interlevel_VertA_HorB,
	            Scenes.Interlevel_HorB_VertB
            };

        public static List<Scenes>
            not_allowed_interstitial = new List<Scenes> ( ) //list here scenes which wouldn't show big banner if we will leave them
            {
                Scenes.InitScene,
                Scenes.PSV_Splash,
                Scenes.Push,
	            Scenes.MainMenu,
	            Scenes.ChooseDifficulty,
	            Scenes.Intro_Tavern,
	            Scenes.WinMenu_Tavern,
            };

#endif

        public static List<Scenes>
            rate_me_after_end = new List<Scenes> ( ) //list here scenes which will call rate_me dialogue on end
            {
                //Scenes.Gameplay,
            };


    }
}
