﻿using Firebase.Analytics;


namespace PSV_Prototype
{
    public enum AnalyticsEvents    //used to log events from one place
    {
        StartApplication,
        CloseApplication,
        LogScreen,
        BannerClicked,
        InterstitialClicked,
        OpenPub,
        OpenPromo,
        Custom,
        NeatPlugError,
        HomeAdsError,
    }


    public static class AnalyticsManager
    {




        public static void LogEvent (AnalyticsEvents _event, string _message = "")
        {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
            LogGoogleAnalyticsEvent ( _event, _message );
            LogFirebaseEvent ( _event, _message );
#endif
        }

        private static void LogGoogleAnalyticsEvent (AnalyticsEvents _event, string _message)
        {
            if (GoogleAnalytics.Instance != null)
            {
                switch (_event)
                {
                    case AnalyticsEvents.Custom:
                        GoogleAnalytics.Instance.LogEvent ( _event.ToString ( ), _message );
                        break;
                    case AnalyticsEvents.CloseApplication:
                    case AnalyticsEvents.StartApplication:
                        GoogleAnalytics.Instance.LogEvent ( _event.ToString ( ), _event.ToString ( ) );
                        break;
                    case AnalyticsEvents.LogScreen:
                        GoogleAnalytics.Instance.LogScreen ( _message );
                        break;
                    case AnalyticsEvents.BannerClicked:
                        GoogleAnalytics.Instance.LogEvent ( "AdMob Clicks", "Banner clicked" );
                        break;
                    case AnalyticsEvents.InterstitialClicked:
                        GoogleAnalytics.Instance.LogEvent ( "AdMob Clicks", "Interstitial clicked" );
                        break;
                    case AnalyticsEvents.OpenPub:
                        GoogleAnalytics.Instance.LogEvent ( "OpenURL " + PromoPlugin.ServiceUtils.GetPlatformName ( ) + ": MoreGames", "Promo" );
                        break;
                    case AnalyticsEvents.OpenPromo:
                        GoogleAnalytics.Instance.LogEvent ( "OpenURL " + PromoPlugin.ServiceUtils.GetPlatformName ( ) + ": " + _message, "Promo" );
                        break;
                    case AnalyticsEvents.NeatPlugError:
                        GoogleAnalytics.Instance.LogEvent ( "NeatPlugError: " + _message, "NeatPlug" );
                        break;
                    case AnalyticsEvents.HomeAdsError:
                        GoogleAnalytics.Instance.LogEvent ( "HomeAdsError: " + _message, "HomeAds" );
                        break;
                }
            }
        }

        private static void LogFirebaseEvent (AnalyticsEvents _event, string _message)
        {
//            switch (_event)
//            {
//                case AnalyticsEvents.CloseApplication:
//                case AnalyticsEvents.StartApplication:
//                case AnalyticsEvents.BannerClicked:
//                case AnalyticsEvents.InterstitialClicked:
//                    FirebaseAnalytics.LogEvent ( _event.ToString ( ) );
//                    break;
//                case AnalyticsEvents.OpenPub:
//                case AnalyticsEvents.OpenPromo:
//                case AnalyticsEvents.NeatPlugError:
//                case AnalyticsEvents.HomeAdsError:
//                case AnalyticsEvents.Custom:
//                    FirebaseAnalytics.LogEvent ( _event + "_" + _message );
//                    break;
//                case AnalyticsEvents.LogScreen:
//                    FirebaseAnalytics.LogEvent ( FirebaseAnalytics.EventSelectContent, new Parameter [] {
//                        new Parameter ( FirebaseAnalytics.ParameterContentType, _message + GetGameMode()),
//                        new Parameter ( FirebaseAnalytics.ParameterItemCategory, "GameMode" )
//                    } );
//                    break;
//            }
        }



        public static string GetGameMode ()
        {
            string res = "";
            //put here processing of current selected game_mode for scenes that support it
            int current_game_mode = 0; //{2+, 5+, 0}

            if (current_game_mode > 0)
            {
                res = "_" + current_game_mode;
            }
            return res;
        }



    }
}
