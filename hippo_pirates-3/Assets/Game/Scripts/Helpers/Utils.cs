using UnityEngine;
using System.Collections.Generic;


static public class Utils
{

    public static Languages.Language GetSystemLanguage ()
    {
        Languages.Language res = Languages.Language.English;
#if UNITY_ANDROID
        res = (Languages.Language) System.Enum.Parse ( typeof ( Languages.Language ), GetAndroidDisplayLanguage ( ), true );
#else
        res = (Languages.Language) System.Enum.Parse ( typeof ( Languages.Language ), Application.systemLanguage.ToString ( ), true );
#endif
        return res;

    }




    private static List<string>
        muslim_locales = new List<string> ( )
        {
            "Arabic",
            "Albanian",
            "Bengali",
            "Indonesian",
            "Kyrgyz",
            "Kazakh",
            "Sinhala",
            "Turkish",
        };



    public static bool IsMuslim ()
    {
        return muslim_locales.IndexOf ( GetAndroidDisplayLanguage ( ) ) >= 0;
    }





    public static string GetAndroidDisplayLanguage ()
    {
		Languages.Language res = Languages.Language.English;
		// PASTER
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass localeClass = new AndroidJavaClass ( "java/util/Locale" );
        AndroidJavaObject defaultLocale = localeClass.CallStatic<AndroidJavaObject> ( "getDefault" );
        AndroidJavaObject usLocale = localeClass.GetStatic<AndroidJavaObject> ( "US" );
        string systemLanguage = defaultLocale.Call<string> ( "getDisplayLanguage", usLocale );
        Debug.Log ( "Android language is " + systemLanguage + " detected as " + systemLanguage );
        return systemLanguage;
#else
         res = (Languages.Language)Application.systemLanguage;
#endif
		return res.ToString();
	}










    public static Vector2 ConvertPoint (Vector2 poin, Camera Cam)
    {
        return Cam.ScreenToWorldPoint ( Camera.main.WorldToScreenPoint ( poin ) );
    }


    public static T ToEnum<T> (this string value)
    {
        return (T) System.Enum.Parse ( typeof ( T ), value, true );
    }


    static public T GetRandomEnum<T> ()
    {
        System.Array A = System.Enum.GetValues ( typeof ( T ) );
        T V = (T) A.GetValue ( UnityEngine.Random.Range ( 0, A.Length ) );
        return V;
    }

    static public bool RandomBool ()
    {
        return Random.value > 0.5f;
    }

    static public List<T> Shuffle<T> (this List<T> list)
    {
        int n = list.Count;
        List<T> new_list = list.CopyList ( );
        while (n > 0)
        {
            int k = Random.Range ( 0, n );
            T item = new_list [n - 1];
            new_list [n - 1] = new_list [k];
            new_list [k] = item;
            n--;
        }
        return new_list;
    }

    static public T [] Shuffle<T> (this T [] array)
    {
        int n = array.Length;
        T [] new_array = array.CopyArray ( );
        while (n > 0)
        {
            int k = Random.Range ( 0, n );
            T item = new_array [n - 1];
            new_array [n - 1] = new_array [k];
            new_array [k] = item;
            n--;
        }
        return new_array;
    }

    static public List<T> CopyList<T> (this List<T> list)
    {
        int n = list.Count;
        List<T> new_list = new List<T> ( );
        ;
        for (int i = 0; i < n; i++)
        {
            new_list.Add ( list [i] );
        }
        return new_list;
    }

    static public T [] CopyArray<T> (this T [] array)
    {
        int n = array.Length;
        T [] new_array = new T [n];
        for (int i = 0; i < n; i++)
        {
            new_array [i] = array [i];
        }
        return new_array;
    }

    static public T GetRandomElement<T> (this List<T> list)
    {
        return list [Random.Range ( 0, list.Count )];
    }


    static public T GetRandomElement<T> (this T [] array)
    {
        return array [Random.Range ( 0, array.Length )];
    }

    static public string GetUID ()
    {
        string UID = System.Guid.NewGuid ( ).ToString ( );
        if (PlayerPrefs.HasKey ( "UID" ))
        {
            UID = PlayerPrefs.GetString ( "UID" );
        }
        else
        {
            PlayerPrefs.SetString ( "UID", UID );
            PlayerPrefs.Save ( );
        }
        return UID;
    }


	public static class WeightedRandom
	{
		public static float[] CalcLookups(float[] weights)
		{
			float total_weight = 0;
			for (int i = 0; i < weights.Length; i++)
			{
				total_weight += weights[i];
			}
			float[] lookups = new float[weights.Length];
			for (int i = 0; i < weights.Length; i++)
			{
				lookups[i] = (weights[i] / total_weight) + (i == 0 ? 0 : lookups[i - 1]);
			}
			return lookups;
		}

		private static int binary_search(float needle, float[] lookups)
		{
			int high = lookups.Length - 1;
			int low = 0;
			int probe = 0;
			if (lookups.Length < 2)
			{
				return 0;
			}
			while (low < high)
			{
				probe = (int)((high + low) / 2);

				if (lookups[probe] < needle)
				{
					low = probe + 1;
				}
				else if (lookups[probe] > needle)
				{
					high = probe - 1;
				}
				else
				{
					return probe;
				}
			}

			if (low != high)
			{
				return probe;
			}
			else
			{
				return (lookups[low] >= needle) ? low : low + 1;
			}
		}


		public static int RandomW(float[] weights)
		{
			weights = CalcLookups(weights);

			if (weights.Length > 0)
				return binary_search(Random.value, weights);
			else
				return -1;
		}


		public static int PrecalculatedRandomW(float[] lookups) //used for getting random on the same set to optimise time by excludinc CalcLookups from each cycle of getting random
		{
			if (lookups.Length > 0)
				return binary_search(Random.value, lookups);
			else
				return -1;
		}

	}
}