﻿#define NEAT_PLUG
//#define GOOGLE_ADS

using System;
using UnityEngine;
#if GOOGLE_ADS
using GoogleMobileAds.Api;
#endif

using PromoPlugin;

public class ManagerGoogle :MonoBehaviour
{
    public delegate void Callback ();

    public static event Callback OnAdsDisabled;
    public static event Callback OnInterstitialShown;
    public static event Callback OnInterstitialClosed;

    static public ManagerGoogle Instance;

    public GameObject purchase_btn;

    private float last_time = 0; //0
    private float bannertime = 30;  //30


#if UNITY_EDITOR

    private float refferenced_dpi = 480;

    private Vector2
        refferenced_res = new Vector2 ( 1920, 1080 ),
        refferenced_size = new Vector2 ( 960, 150 );

    private bool
        initialized = false,
        banner_visible = false,
        inter_visible = false;



    Vector2 GetBannerSize ()
    {
        float coef = Mathf.Max ( Screen.width, Screen.height ) / Mathf.Max ( refferenced_res.x, refferenced_res.y);
        return refferenced_size * coef;
    }


    public void OnGUI ()
    {
        if (!initialized)
        {
            if (AdmobManager.Instance != null)
            {
                //banner_size = AdmobAd.Instance ( ).GetAdSizeInPixels ( AdmobManager.Instance.BannerAdType );
                float dpi_coef = Screen.dpi / 160f;
                //banner_size = new Vector2 ( 320f * dpi_coef, 50f * dpi_coef );

                //Debug.Log ( "ManagerGoogle: banner size in pixels = " + banner_size );
                initialized = true;
            }
        }
        else
        {

            Vector2 banner_size = GetBannerSize ( );

            if (banner_visible)
            {
                GUI.color = Color.white;
                Vector2 pos = GetRectPos ( banner_size );
                GUI.Button ( new Rect ( pos.x, pos.y, banner_size.x, banner_size.y ), "Dummy ad 320x50" );
            }

            if (inter_visible)
            {
                if (GUI.Button ( new Rect ( 0, 0, Screen.width, Screen.height ), "Dummy\n\ninterstitial" ))
                {
                    inter_visible = false;
                    OnFullscreenbannerClosed ( );
                }
            }
        }
    }


    Vector2 GetRectPos (Vector2 banner_size)
    {
        Vector2 res = Vector2.zero;
        AdmobAd.AdLayout ad_pos = AdmobManager.Instance.BannerAdPosition;
        switch (ad_pos)
        {
            case AdmobAd.AdLayout.Bottom_Centered:
                res.y = Screen.height - banner_size.y;
                res.x = Screen.width * 0.5f - banner_size.x * 0.5f;
                break;
            case AdmobAd.AdLayout.Top_Centered:
                res.y = 0;
                res.x = Screen.width * 0.5f - banner_size.x * 0.5f;
                break;
            case AdmobAd.AdLayout.Bottom_Left:
                res.y = Screen.height - banner_size.y;
                res.x = 0;
                break;
            case AdmobAd.AdLayout.Bottom_Right:
                res.y = Screen.height - banner_size.y;
                res.x = Screen.width - banner_size.x;
                break;
            case AdmobAd.AdLayout.Top_Left:
                res.y = 0;
                res.x = 0;
                break;
            case AdmobAd.AdLayout.Top_Right:
                res.y = 0;
                res.x = Screen.width - banner_size.x;
                break;
            case AdmobAd.AdLayout.Middle_Centered:
                res.y = Screen.height * 0.5f - banner_size.y * 0.5f;
                res.x = Screen.width * 0.5f - banner_size.x * 0.5f;
                break;
            case AdmobAd.AdLayout.Middle_Left:
                res.y = Screen.height * 0.5f - banner_size.y * 0.5f;
                res.x = 0;
                break;
            case AdmobAd.AdLayout.Middle_Right:
                res.y = Screen.height * 0.5f - banner_size.y * 0.5f;
                res.x = Screen.width - banner_size.x;
                break;
        }



        return res;
    }
#endif

    void Awake ()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad ( this );
        }
    }



    public void OnEnable ()
    {
        PromoModule.OnBannerTimeUpdate += UpdateBannerTime;
        AdmobAdAgent.OnDismissScreenInterstitial += OnFullscreenbannerClosed;
    }

    public void OnDisable ()
    {
        PromoModule.OnBannerTimeUpdate -= UpdateBannerTime;
        AdmobAdAgent.OnDismissScreenInterstitial += OnFullscreenbannerClosed;
    }

    void UpdateBannerTime (float val)
    {
        bannertime = val;
    }

    public void ResetLastTime ()
    {
        last_time = Time.time;
    }

    void Start ()
    {
        Init ( );
        UpdateBannerTime ( PromoModule.Instance.GetBannerTime ( ) );
    }




    public void Init ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        BillingManager.Instance.MapSKU ( );
        BillingManager.Instance.Init ( );

        AdmobManager.Instance.Init ( false, false );
#endif

        if (purchase_btn != null && AdmobManager.Instance.isAdvertsDisabled)
        {
            purchase_btn.SetActive ( false );
        }

        //		if (!FB.IsInitialized) {
        //			InitFacebook ();
        //		}
    }

    public void InitFacebook ()
    {
        //		FB.Init (delegate{});
    }

    //	void LoginCallback(FBResult result)
    //	{
    //		if (result.Error != null)
    //			Debug.Log ("Error Response:\n" + result.Error);
    //		else if (!FB.IsLoggedIn) {
    //			Debug.Log ("Login cancelled by Player");
    //		}
    //		else {
    //			Debug.Log ("Login was successful!");
    //			FacebookPost ();
    //		}
    //	}

    private void FacebookPost ()
    {
        //		FB.Feed (
        //			link: "https://play.google.com/store/apps/details?id=com.PSVStudio.kidsacademy",
        //			linkName: "I am playing Kid's Academy!",
        //			linkDescription: " ",
        //			picture: "http://psvgamestudio.com/data/PICS/kidsacademy.png"
        //		);
    }

    public void FacebookButton ()
    {
        //		if (!FB.IsInitialized) {
        //			InitFacebook ();
        //		} else if (FB.IsLoggedIn) {
        //			FacebookPost ();
        //		} else {
        //			InitFacebook ();
        //			FB.Login("email,publish_actions", LoginCallback);
        //		}
    }

    //////////////////////////////////////
    /////////////-AdMob Ads-//////////////
    //////////////////////////////////////

    public void OnFullscreenbannerClosed ()
    {
        if (OnInterstitialClosed != null)
        {
            OnInterstitialClosed ( );
        }
    }


#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1

    public void LoadInterstitialAd ()
    {
        AdmobManager.Instance.LoadBanner ( );
    }

    public bool ShowFullscreenBanner ()
    {
        if (!IsAdmobDisabled ( ))
        {
            float time = Time.time;
            if (time - last_time >= bannertime)
            {
#if UNITY_EDITOR
                Debug.Log ( "Show interstitial" );
                inter_visible = true;
                last_time = time;
                HideSmallBanner ( ); //for Debug, it actually hides in AdmobManager
                return true;
#endif
                if (AdmobManager.Instance.ShowInterstitial ( ))
                {
                    last_time = time;
                    if (OnInterstitialShown != null)
                    {
                        OnInterstitialShown ( );
                    }
                    return true;
                }
            }
        }
        return false;
    }
    

    public void HideSmallBanner ()
    {
#if UNITY_EDITOR
        banner_visible = false;
#endif
        AdmobManager.Instance.HideBanner ( );
    }

    public void ShowSmallBanner ()
    {
        if (!IsAdmobDisabled ( ))
        {
#if UNITY_EDITOR
            banner_visible = true;
#endif
            AdmobManager.Instance.ShowBanner ( );

        }
    }

#if GOOGLE_ADS
    public void RepositionSmallBanner (AdPosition layout)
    {
        //check if banner in needed position to avoid its unnecessary reload
        if (!IsAdmobDisabled ( ) && AdmobManager.Instance.BannerPosition != layout)
        {
            AdmobManager.Instance.RepositionBanner ( layout );
        }
    }
#endif
#if NEAT_PLUG
    public void RepositionSmallBanner (AdmobAd.AdLayout layout)
    {
        //check if banner in needed position to avoid its unnecessary reload
        if (!IsAdmobDisabled ( ) && AdmobManager.Instance.BannerAdPosition != layout)
        {
            AdmobManager.Instance.RepositionBanner ( layout );
        }
    }
#endif

    public void DisableAdmob ()
    {
        AdmobManager.Instance.DisableAllAds ( );
        if (OnAdsDisabled != null)
        {
            OnAdsDisabled ( );
        }
    }

    public bool IsAdmobDisabled ()
    {
        return AdmobManager.Instance.isAdvertsDisabled;
    }

#endif

}
