﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PNNewsScript : MonoBehaviour {

	public static PNNewsScript Instance;

	public GameObject[] border;
	public GameObject[] newsbtn;

	public GameObject 
		Self,
		PNTitleBorder,
		PNDesc,
		PNDescText,
		PNTitle,
		PNTitleText,
		PNImage;

	private float brwidth = 0.2f;

	private float camsizedef;

	private Vector2 screenWH;

	private Texture2D tex;
	private string 
		pstitle,
		psimgurl,
		psdescr;

	void Awake () {
		Instance = this;
	}

	IEnumerator PNLoadImg(string psimgurl) {
		WWW wwwimg = new WWW(psimgurl);
		yield return wwwimg;
		tex = wwwimg.texture;
		Rect rec = new Rect(0, 0, tex.width, tex.height);
		Vector2 pivot = new Vector2(0.5f, 0.5f);
		Sprite newPlanet = Sprite.Create(tex,rec, pivot);
		PNImage.GetComponent<SpriteRenderer>().sprite = newPlanet;
		PNImage.transform.localScale = new Vector3 (1f, 1f, PNImage.transform.localScale.z);
		Vector3 currposimg = PNImage.transform.position;
		Vector2 currimgscale = new Vector2(1f, 1f);
		Vector2 center = new Vector2 ((PNGetScreen.left + PNGetScreen.right)/2, (PNGetScreen.top + PNGetScreen.bottom)/2);
		if ( (Screen.orientation == ScreenOrientation.Landscape)||(Screen.orientation == ScreenOrientation.LandscapeLeft)||(Screen.orientation == ScreenOrientation.LandscapeRight) ) {
			currposimg.x = ( PNGetScreen.left + brwidth + center.x )/2;
			currposimg.y = center.y + (border [5].GetComponent<SpriteRenderer> ().bounds.size.y - PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y)/2;
			currimgscale.y = (PNGetScreen.height - border [5].GetComponent<SpriteRenderer> ().bounds.size.y - PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y) / PNImage.GetComponent<SpriteRenderer> ().bounds.size.y;
			currimgscale.x = (PNGetScreen.right - brwidth - center.x) / PNImage.GetComponent<SpriteRenderer> ().bounds.size.x;
		} else {
			currposimg.x = center.x;
			currposimg.y = ( PNGetScreen.top - PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y + center.y )/2;
			currimgscale.y = ( PNGetScreen.top - PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y - center.y ) / PNImage.GetComponent<SpriteRenderer> ().bounds.size.y;
			currimgscale.x = (PNGetScreen.width - (border [1].GetComponent<SpriteRenderer> ().bounds.size.x * 2)) / PNImage.GetComponent<SpriteRenderer> ().bounds.size.x;
		}
		PNImage.transform.position = currposimg;
		if (currimgscale.x < currimgscale.y){
			PNImage.transform.localScale = new Vector3 (currimgscale.x, currimgscale.x, PNImage.transform.localScale.z);
		} else {
			PNImage.transform.localScale = new Vector3 (currimgscale.y, currimgscale.y, PNImage.transform.localScale.z);
		}
	}

	private void PNSetTitleDescr (string pstitle, string psdescr){
		PNDescText.GetComponent<Text> ().text = psdescr;
		PNTitleText.GetComponent<Text> ().text = pstitle;
	}

	private void PNSetBackground(){
		border [0].transform.localScale = new Vector3 (1f, 1f, border [0].transform.localScale.z);
		border [1].transform.localScale = new Vector3 (1f, 1f, border [1].transform.localScale.z);
		border [2].transform.localScale = new Vector3 (1f, 1f, border [2].transform.localScale.z);
		border [3].transform.localScale = new Vector3 (1f, 1f, border [3].transform.localScale.z);
		border [6].transform.localScale = new Vector3 (1f, 1f, border [3].transform.localScale.z);
		Vector2 currscale;
		currscale.x = (PNGetScreen.width - border [4].GetComponent<SpriteRenderer> ().bounds.size.x * 2) / border [0].GetComponent<SpriteRenderer> ().bounds.size.x;
		currscale.y = (PNGetScreen.height - border [4].GetComponent<SpriteRenderer> ().bounds.size.y - border [5].GetComponent<SpriteRenderer> ().bounds.size.y) / border [1].GetComponent<SpriteRenderer> ().bounds.size.y;
		border [0].transform.localScale = new Vector3 (currscale.x + brwidth/10, border [0].transform.localScale.y, border [0].transform.localScale.z);
		border [1].transform.localScale = new Vector3 (border [1].transform.localScale.y, currscale.y + brwidth/10 + 0.2f, border [1].transform.localScale.z);
		border [2].transform.localScale = new Vector3 (border [2].transform.localScale.y, currscale.y + brwidth/10 + 0.2f, border [2].transform.localScale.z);
		border [3].transform.localScale = new Vector3 (currscale.x + brwidth/10, border [3].transform.localScale.y, border [3].transform.localScale.z);
		border [6].transform.localScale = new Vector3 (currscale.x + brwidth*2, currscale.y*2 + brwidth + 0.2f, border [3].transform.localScale.z);
		border [9].GetComponent<BoxCollider2D> ().size = new Vector2 (PNGetScreen.width, (PNGetScreen.height - border [5].GetComponent<SpriteRenderer> ().bounds.size.y));
		border [9].GetComponent<BoxCollider2D> ().offset = new Vector2 ((PNGetScreen.left + PNGetScreen.right)/2, (PNGetScreen.top + PNGetScreen.bottom)/2 + border [5].GetComponent<SpriteRenderer> ().bounds.size.y/2);
	}

	private void PNSetTitleBorder(){
		PNTitle.transform.position = new Vector3 ((PNGetScreen.left + PNGetScreen.right)/2, PNGetScreen.top - camsizedef*brwidth/2, PNDesc.transform.position.z);
		if (PNGetScreen.width < PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.x) {
			PNTitle.GetComponent<RectTransform>().sizeDelta = new Vector2( ((PNGetScreen.right - PNGetScreen.left-camsizedef*brwidth*2))/camsizedef, (PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y - camsizedef*brwidth)/camsizedef);
		} else {
			PNTitle.GetComponent<RectTransform>().sizeDelta = new Vector2((PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.x - camsizedef*brwidth*2)/camsizedef, (PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y - camsizedef*brwidth)/camsizedef);
		}
		PNTitleText.GetComponent<RectTransform>().sizeDelta = new Vector2(PNTitle.GetComponent<RectTransform>().sizeDelta.x/PNTitleText.GetComponent<RectTransform>().localScale.x,
		                                                                  PNTitle.GetComponent<RectTransform>().sizeDelta.y/PNTitleText.GetComponent<RectTransform>().localScale.y);
		PNTitleText.GetComponent<Text> ().fontSize = (int) (PNTitle.GetComponent<RectTransform> ().sizeDelta.x * 18.5f);
	}

	private void PNSetDescriptionBorder(){
		if ( (Screen.orientation == ScreenOrientation.Landscape)||(Screen.orientation == ScreenOrientation.LandscapeLeft)||(Screen.orientation == ScreenOrientation.LandscapeRight) ) {
			PNDesc.transform.position = new Vector3 ((PNGetScreen.right + PNGetScreen.left)/2 + camsizedef*brwidth, PNGetScreen.top - PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y - camsizedef*brwidth, PNDesc.transform.position.z);
			PNDesc.GetComponent<RectTransform> ().sizeDelta = new Vector2 ( ((PNGetScreen.right - PNGetScreen.left)/2 - camsizedef*brwidth*2)/camsizedef, (PNGetScreen.top - PNGetScreen.bottom - camsizedef*brwidth*2 - PNTitleBorder.GetComponent<SpriteRenderer> ().bounds.size.y)/camsizedef);
		} else {
			PNDesc.transform.position = new Vector3 (PNGetScreen.left + camsizedef*brwidth, (PNGetScreen.top + PNGetScreen.bottom)/2-camsizedef*brwidth, PNDesc.transform.position.z);
			PNDesc.GetComponent<RectTransform>().sizeDelta = new Vector2((PNGetScreen.right - PNGetScreen.left - camsizedef*brwidth*2)/camsizedef, ((PNGetScreen.top - PNGetScreen.bottom)/2 - border[5].GetComponent<SpriteRenderer> ().bounds.size.y - camsizedef*brwidth)/camsizedef);
		}
		PNDescText.GetComponent<RectTransform> ().sizeDelta = new Vector2 (PNDesc.GetComponent<RectTransform> ().sizeDelta.x / PNDescText.GetComponent<RectTransform> ().localScale.x,
		                                                                   PNDesc.GetComponent<RectTransform> ().sizeDelta.y / PNDescText.GetComponent<RectTransform> ().localScale.y);
		PNDescText.GetComponent<Text> ().fontSize = (int) (PNDesc.GetComponent<RectTransform> ().sizeDelta.x * 16);
	}

	private void PNLoadUrlData (){
		pstitle = PsvPushService.Instance.pstitle;
		psimgurl = PsvPushService.Instance.psimgurl;
		psdescr = PsvPushService.Instance.psdescr;
	}

	// Use this for initialization
	public void Start () {
		camsizedef = PsvPushService.Instance.PushNewsScale;
		screenWH = new Vector2(Screen.width, Screen.height);
		print ("Start width=" + Screen.width + " x height=" + Screen.height);
		PNSetBackground ();
		PNSetTitleBorder ();
		PNSetDescriptionBorder ();
		PNLoadUrlData ();
		PNSetTitleDescr (pstitle, psdescr);
		if (psimgurl.Length > 1){
			try{
				StartCoroutine( PNLoadImg (psimgurl));
			} catch (UnityException err){
				print ("UnityException:" + err);
			}
		}
	}

	void PNReSetAlign (GameObject obj){
		PNAlignment.Instance.SetAlignment (obj, obj.GetComponent<PNAlignment>().vAlign, obj.GetComponent<PNAlignment>().hAlign, obj.GetComponent<PNAlignment>().xOffset, obj.GetComponent<PNAlignment>().yOffset);
	}

	void PNReStart () {
		camsizedef = PsvPushService.Instance.PushNewsScale;
		for (int i=0; i<9; i++) {
			PNReSetAlign (border [i]);
		}
		PNReSetAlign (PNTitleBorder);
		for (int i=0; i<3; i++) {
			PNReSetAlign (newsbtn [i]);
		}
		PNSetBackground ();
		PNSetTitleBorder ();
		PNSetDescriptionBorder ();
		if (psimgurl.Length > 1){
			try{
				StartCoroutine( PNLoadImg (psimgurl));
			} catch (UnityException err){
				print ("UnityException:" + err);
			}
		}
	}

	// Update is called once per frame
	public void Update(){
		if ( !((screenWH.x == Screen.width)&&(screenWH.y == Screen.height)) ) {
			screenWH = new Vector2(Screen.width, Screen.height);
			print ("Change width=" + Screen.width + " x height=" + Screen.height);
			PNReStart ();
		}
//		if (Input.GetKeyDown (KeyCode.Escape)) {
//			PsvPushService.Instance.PSShowNews (false);
//		}
	}

}
