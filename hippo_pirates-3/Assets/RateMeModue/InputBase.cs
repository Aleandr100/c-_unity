﻿using UnityEngine;
using System.Collections;

public class InputBase : MonoBehaviour {
	protected bool focus = false;

	public bool IsFocused () {
		return focus;
	}

	virtual public bool OnTouchDown (Vector2 point) {
		return false;
	}
	
	virtual public bool OnTouchUp (Vector2 point) {
		return false;
	}

	virtual public bool OnTouchMove (Vector2 point) {
		return false;
	}
	
	virtual public bool OnTouchExit (Vector2 point) {
		return false;
	}
}
