﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PromoRotateIconUI :MonoBehaviour
{

    //params
    [Header ( "Buttons settings" )]
    public int
        max_buttons_to_show = 10;

    [Header ( "Scroll settings" )]
    public float
        start_rotation_speed = 1,
        scroll_inertia = 0.3f;
    public bool
        invert_button_rotation = false;
    public float
        swipe_threshold = 0.1f,
        max_swipe_forse = 10,
        scroll_acceleration = 15f,
        rotation_speed_scale = 0.5f,
        scroll_speed = 50f;    

    //variables
    private int
        current_item;
    private Vector2
        touch_pos = Vector2.zero,
        scroll_direction = Vector2.down;
    private bool
        is_initiated = false,
        focus = false;
    private float
        //last_direction = 1,
        my_time_scale = 0;
    List<PackageData>
        items = new List<PackageData> ( );


    //objects
    [Header ( "Objects" )]
    public RectTransform
        scroller;
    public PromoButton
        btn;
    public RectTransform []
        rotate_obj;
    private RectTransform
        btn_r;

    void Awake ()
    {
        btn_r = btn.GetComponent<RectTransform> ( );
    }

    void OnEnable ()
    {
        is_initiated = false;
        btn.gameObject.SetActive ( false );
        //if (ManagerGoogle.Instance != null)
        //{
        //    ManagerGoogle.Instance.HideSmallBanner ( );
        //}
        //else
        //{
        //    Debug.Log ( "ManagerGoogle not initialised" );
        //}
        StartCoroutine ( InitButtons ( ) );
        //Init ( );
        PromoModule.OnPackageReceived += ProcessPackage;
    }


    IEnumerator InitButtons ()
    {
        yield return new WaitForEndOfFrame ( );
        Init ( );
    }

    void OnDisable ()
    {
        PromoModule.OnPackageReceived -= ProcessPackage;

        //if (PrimarySettings.show_banner_on_disable && ManagerGoogle.Instance != null)
        //{
        //    ManagerGoogle.Instance.ShowSmallBanner ( );
        //}
        //else if (PrimarySettings.show_banner_on_disable)
        //{
        //    Debug.Log ( "ManagerGoogle not initialised" );
        //}

    }

    void Update ()
    {
        if (!focus && my_time_scale != 1)
        {
            my_time_scale = Mathf.Lerp ( my_time_scale, Mathf.Sign ( my_time_scale ) * start_rotation_speed, Time.deltaTime * scroll_inertia );
            if (Mathf.Abs ( my_time_scale ) < 1.01f)
                my_time_scale = Mathf.Sign ( my_time_scale ) * start_rotation_speed;
        }
        Animate ( );
    }

    public void OnPointerDown (BaseEventData e)
    {
        focus = true;
        PointerEventData pointerData = e as PointerEventData;
        Vector2 point;
        RectTransformUtility.ScreenPointToLocalPointInRectangle ( scroller, pointerData.position, Camera.main, out point );
        //for touch controll
        //my_time_scale = -Mathf.Clamp ( point.y, scroller.rect.yMin, scroller.rect.yMax ) / scroller.rect.yMax * scroll_acceleration;

        //for swipe
        touch_pos = point;
    }


    public void OnDrag (BaseEventData e)
    {
        if (focus)
        {
            PointerEventData pointerData = e as PointerEventData;
            Vector2 point;
            RectTransformUtility.ScreenPointToLocalPointInRectangle ( scroller, pointerData.position, Camera.main, out point );
            //for touch controll
            //my_time_scale = -Mathf.Clamp ( point.y, scroller.rect.yMin, scroller.rect.yMax ) / scroller.rect.yMax * scroll_acceleration;

            //for swipe
            ProcessSwipe ( point );
        }
    }


    void ProcessSwipe (Vector2 point)
    {
        float swipe_dist = Vector2.Distance ( point, touch_pos );
        if (swipe_dist > swipe_threshold)
        {
            focus = false;
            my_time_scale = Mathf.Clamp ( -Mathf.Clamp ( (scroll_acceleration * 0.1f * (swipe_dist / swipe_threshold)), 0, scroll_acceleration ) * Mathf.Sign ( point.y - touch_pos.y ) + my_time_scale, -max_swipe_forse, max_swipe_forse );
        }
    }


    public void OnPointerUp (BaseEventData e)
    {
        if (focus)
        {
            PointerEventData pointerData = e as PointerEventData;
            Vector2 point;
            RectTransformUtility.ScreenPointToLocalPointInRectangle ( scroller, pointerData.position, Camera.main, out point );
            ProcessSwipe ( point );
            focus = false;
        }
    }


    void Init ()
    {
        //Debug.Log ( "Init" );
        items.Clear ( );
        if (PromoModule.Instance)
        {
            items.AddRange ( PromoModule.Instance.GetPromoItems ( max_buttons_to_show ) );
        }
        else
        {
            Debug.LogError ( "PromoModule is not present on the scene" );
        }
        if (items.Count > 0)
        {
            float [] items_weight = new float [items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                items_weight [i] = PromoModule.Instance.GetAppWeight ( items [i].app_bundle );
            }
            btn.gameObject.SetActive ( true );
            btn.Init ( items [WeightedRandom.RandomW ( items_weight )] );
            my_time_scale = start_rotation_speed;
            is_initiated = true;
        }

        if (PromoModule.debug)
        {
            PrintPromoSet ( items );
        }

    }


    void PrintPromoSet (List<PackageData> set)
    {
        string res = "PromoUI GetItems(), received items " + items.Count + "\n";
        for (int i = 0; i < set.Count; i++)
        {
            res += "| " + set [i].app_bundle + " | " + set [i].app_alias + " | " + set [i].app_name + "|\n";
        }
        Debug.Log ( res );
    }


    void Animate ()
    {
        Vector2 offset = scroll_direction * scroll_speed * my_time_scale * Time.deltaTime;

        for (int i = 0; i < rotate_obj.Length; i++)
        {
            ApplyRotation ( rotate_obj [i], offset );
        }
        ApplyRotation ( btn_r, offset * (invert_button_rotation ? -1 : 1) );
    }


    void ApplyRotation (RectTransform obj, Vector2 offset)
    {
        obj.Rotate ( 0, 0, offset.y * rotation_speed_scale );
    }



    void ProcessPackage (PackageData data)
    {
        if (!is_initiated && !items.Contains ( data ))
        {
            items.Add ( data );

            Init ( );
        }
    }

}
