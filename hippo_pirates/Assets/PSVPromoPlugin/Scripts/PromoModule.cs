﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;


/*
version 3 
    - implemented better plafotrm override for testing capabilities 
    - implemented market page licale override according to language presets
    - implemented settings for disabling app name search
    - implemented version tracking
version 4
    - implemented announce for local data (needed for games with menu_scene(with ui_promo) going first (without splash screen))
    - added option to enable local announce
version 5
    - moved some settings to external class for better usability
    - added animated moreGames button for hippo games
    - due to need to scale buttons prefabs were et to bigger size
version 6
    - added standalone UI module with stationary set of buttons that can be placed anywhere
version 7
    - added promo box prefab with scrolling promo buttons
*/

public class PromoModule :MonoBehaviour
{
    public static PromoModule Instance;

    public delegate void Action (PackageData data);
    public static event Action OnPackageReceived;

    public delegate void Callback (string acc_alias);
    public static event Callback OnAccAliasReceived;


    public const int promo_version = 8;

    //params
    public static bool
        debug = true;
    private bool
        use_new_primary_set_logic = true;   //actual from animated UI (according to new rules anly one icon will be returned if no data received). If you want to get mroe than one icon set this to false

    const string
        promo_set_file = "promo_set.json",
        dat_file = "data_set.json";

    //variables
    private bool
        blocking_access = false;

    private float
        acc_alias_update_interval = 5,
        promo_set_update_interval = 1,
        retry_delay = 60f,
        min_weight = 0.01f;



    private int
        max_retry_efforts = 5,
        retry_efforts = 0,
        faulty_downloads = 0,
        downloads_in_progress = 0;


    private string
        publisher = "";

    //variables
    private List<PromoItem>
        promo_set = new List<PromoItem> ( );


    List<PackageData>
        available_set;

    Coroutine
        delayed_download = null;

    //methods

    void OnEnable ()
    {
        MarketBridge.OnDataReceived += ProcessData;
    }


    void OnDisable ()
    {
        MarketBridge.OnDataReceived -= ProcessData;
    }


    void Awake ()
    {

        if (!Instance)
        {
            //#if UNITY_EDITOR
            //            PlayerPrefs.DeleteAll ( );
            //#else
            //            debug = false;
            //#endif

            Instance = this;
            DontDestroyOnLoad ( this );
            CheckVersion ( );
        }
    }


    public float GetAppWeight (string app_bundle)
    {
        float res = min_weight;
        int index = promo_set.FindIndex ( X => X.app_bundle.Equals ( app_bundle ) );
        if (index >= 0)
        {
            res = (float) promo_set [index].app_weight;
        }
        return res;
    }

    void CheckVersion ()
    {
        int curent_version = PlayerPrefs.GetInt ( "PromoVersion", -1 ); //due to all previos versions didn't save this value we use -1 by default for all versions bellow 3
        //fixes local data for versions below 3
        if (curent_version < promo_version)
        {
            //make some stuff to fix differences in saved data between versions


            //to use new implementation of market locale override we have to remove all saved app aliases
            if (curent_version < 3 && PrimarySettings.receive_app_name && PrimarySettings.use_locale_override)
            {
                for (int i = 0; i < PrimarySettings.primary_set.Count; i++)
                {
                    PlayerPrefs.DeleteKey ( PrimarySettings.primary_set [i].app_bundle );
                }
                //removing old app names received in wrong locale
                ClearLocalData ( );
            }

            //save version of saved data
            PlayerPrefs.SetInt ( "PromoVersion", promo_version );
            PlayerPrefs.Save ( );
        }

    }


    void ClearLocalData ()
    {
        string dat_path = GetExternalPath ( ) + dat_file;
        if (File.Exists ( dat_path ))
        {
            File.Delete ( dat_path );
        }
    }


    void Start ()
    {
        UpdatePrimarySetNames ( );
        publisher = PlayerPrefs.GetString ( "Publisher", PrimarySettings.default_acc );


        if (!Directory.Exists ( GetExternalPath ( ) ))
        {
            Directory.CreateDirectory ( GetExternalPath ( ) );
        }


        //performing init before there will be requests to Instance
        LoadLocalResources ( );




        //#if !UNITY_EDITOR
        System.DateTime next_promo_update;
        System.DateTime.TryParse ( PlayerPrefs.GetString ( "LastPromoUpd", System.DateTime.Now.ToString ( ) ), out next_promo_update );

        if (next_promo_update <= System.DateTime.Now)
        {
            LoadPromoSet ( );
        }




        System.DateTime next_acc_alias_update;
        System.DateTime.TryParse ( PlayerPrefs.GetString ( "LastAccAliasUpd", System.DateTime.Now.ToString ( ) ), out next_acc_alias_update );

        if (next_acc_alias_update <= System.DateTime.Now)
        {
            LoadAccAlias ( );
        }
        //#endif
    }


    void UpdatePrimarySetNames ()
    {
        if (PrimarySettings.use_locale_override)
        {
            //List<PackageInfo> queue = new List<PackageInfo> ( );
            for (int i = 0; i < PrimarySettings.primary_set.Count; i++)
            {
                string app_name = PlayerPrefs.GetString ( PrimarySettings.primary_set [i].app_bundle, "" );
                if (app_name != "")
                {
                    PrimarySettings.primary_set [i] = new PackageInfo ( PrimarySettings.primary_set [i].app_bundle, PrimarySettings.primary_set [i].app_alias, app_name );
                }
                else
                {
                    //queue.Add ( PrimarySettings.primary_set [i] );
                    StartCoroutine ( MarketBridge.Instance.GetPackageDataOneShot ( PrimarySettings.primary_set [i], ProcessPrimarySetData, false, PrimarySettings.use_locale_override ) );
                }
            }
        }
    }


    void ProcessPrimarySetData (PackageData data)
    {
        if (!string.IsNullOrEmpty ( data.app_name ))
        {
            PlayerPrefs.SetString ( data.app_bundle, data.app_name );
            PlayerPrefs.Save ( );
            int index = PrimarySettings.primary_set.FindIndex ( X => X.app_bundle.Equals ( data.app_bundle ) );
            if (index >= 0)
            {
                if (debug)
                {
                    Debug.Log ( "Appname has changed from " + PrimarySettings.primary_set [index].app_name + " to " + data.app_name );
                }
                PrimarySettings.primary_set [index] = new PackageInfo ( data.app_bundle, data.app_alias, data.app_name );
            }

            index = available_set.FindIndex ( X => X.app_bundle.Equals ( data.app_bundle ) );
            if (index >= 0)
            {
                if (debug)
                {
                    Debug.Log ( "Available set app name has changed from " + available_set [index].app_name + " to " + data.app_name );
                }
                available_set [index].app_name = data.app_name;
            }
        }
    }


    public void LoadPrimarySetIcons ()
    {
        for (int i = 0; i < PrimarySettings.primary_set.Count; i++)
        {
            StartCoroutine ( MarketBridge.Instance.GetPackageDataOneShot ( PrimarySettings.primary_set [i], ProcessEditorData, false, false ) );
        }
    }


    void ProcessEditorData (PackageData data)
    {
        string path = Application.dataPath + "/Resources/PSVPromo/";
        if (!Directory.Exists ( path ))
        {
            Directory.CreateDirectory ( path );
        }
        if (data != null && !string.IsNullOrEmpty ( data.app_bundle ) && data.app_icon != null)
        {
            File.WriteAllBytes ( path + data.app_bundle + ".png", data.app_icon.EncodeToPNG ( ) );
        }
    }

    public string GetPublisher ()
    {
        return publisher;
    }


    void SetPublisher (string val)
    {
        publisher = val;
        if (debug)
            Debug.Log ( "Publisher = " + val );
        PlayerPrefs.SetString ( "Publisher", val );
        PlayerPrefs.Save ( );
        if (OnAccAliasReceived != null)
        {
            OnAccAliasReceived ( val );
        }
    }

    public void LoadPromoSet (string bundle = "", int platform = -1)
    {
        StartCoroutine ( ReceivePromoSet ( bundle, platform ) );
    }

    public void LoadAccAlias (string bundle = "", string platform = "")
    {
        StartCoroutine ( ReceiveAccounntAlias ( bundle, platform ) );
    }

    void SetNextAccAliasUpdate ()
    {
        PlayerPrefs.SetString ( "LastAccAliasUpd", System.DateTime.Now.AddDays ( acc_alias_update_interval ).AddHours ( Random.Range ( 0, 23 ) ).ToString ( ) );
        PlayerPrefs.Save ( );
    }

    void SetNextPromoUpdate ()
    {
        PlayerPrefs.SetString ( "LastPromoUpd", System.DateTime.Now.AddDays ( promo_set_update_interval ).AddHours ( Random.Range ( 0, 23 ) ).ToString ( ) );
        PlayerPrefs.Save ( );
    }


    void BlockAccess (bool param)
    {
        blocking_access = param;
    }







    void LoadLocalResources ()
    {
        available_set = new List<PackageData> ( );

        //load data from cache (app_names)
        string dat_path = GetExternalPath ( ) + dat_file;
        if (File.Exists ( dat_path ))
        {
            List<PackageInfo> buffer = LitJson.JsonMapper.ToObject<List<PackageInfo>> ( File.ReadAllText ( dat_path ) );
            for (int i = 0; i < buffer.Count; i++)
            {
                string icon_path = GetExternalPath ( ) + buffer [i].app_bundle + ".png";
                if (File.Exists ( icon_path ))
                {
                    Texture2D icon = new Texture2D ( 300, 300 );
                    icon.LoadImage ( File.ReadAllBytes ( icon_path ) );
                    available_set.Add ( new PackageData ( buffer [i], icon ) );
                }
                else
                {
                    if (debug)
                        Debug.Log ( "File " + icon_path + " does not exist" );
                }
            }
        }

        //load primary data from Assets/Resources/PSVPromo/
        Texture2D [] res = Resources.LoadAll<Texture2D> ( "PSVPromo/" );
        for (int i = 0; i < res.Length; i++)
        {
            int index = PrimarySettings.primary_set.FindIndex ( X => X.app_bundle == res [i].name );
            if (index >= 0)
            {
                available_set.Add ( new PackageData ( PrimarySettings.primary_set [index], res [i] ) );
            }
        }

        //load promo_set from local file
        List<PromoItem> set = new List<PromoItem> ( );
        dat_path = GetExternalPath ( ) + promo_set_file;
        if (File.Exists ( dat_path ))
        {
            string _dat = File.ReadAllText ( dat_path );

            set = ServiceUtils.GetPlatform ( ) == 2 ?
                //WP has different comma seperator - using own parse method
                WP_JSON_Parse ( _dat ) :
                //if not WP using generic method
                LitJson.JsonMapper.ToObject<List<PromoItem>> ( _dat );
        }

        InitPromoSet ( set, PrimarySettings.announce_local );
    }



    List<PromoItem> WP_JSON_Parse (string src)
    {
        List<PromoItem> res = new List<PromoItem> ( );
        SimpleJSON.JSONArray data = SimpleJSON.JSONNode.Parse ( src ).AsArray;

        for (int i = 0; i < data.Count; i++)
        {
            string app_bundle = data [i] ["app_bundle"];
            string app_alias = data [i] ["app_alias"];
            string app_name = data [i] ["app_name"];
            double app_weight = data [i] ["app_weight"].AsDouble;
            //for WP debug
            //Debug.Log ( app_weight );

            bool is_local = data [i] ["is_local"].AsBool;
            res.Add ( new PromoItem ( app_bundle, app_alias, (float) app_weight, app_name, is_local ) );
        }
        return res;
    }


    void PrintAvailableSet ()
    {
        string res = "";
        for (int i = 0; i < available_set.Count; i++)
        {
            res += available_set [i].app_bundle + " " + available_set [i].app_name + "\n";
        }
        Debug.Log ( res );
    }

    void InitPromoSet (List<PromoItem> set, bool local = false)
    {
        //this commented due to no need of adding items from primary_set that are not in promo_set

        //build set of unused local promo items
        List<int> local_set = new List<int> ( );
        for (int j = 0; j < PrimarySettings.primary_set.Count; j++)
        {
            local_set.Add ( j );
        }


        int i = 0;
        while (i < set.Count)
        {
            //check item weight and if items package is installed on device and not same as this app
            if (set [i].GetPromoWeight ( ) > 0 && ServiceUtils.IsAppAllowed ( set [i].app_bundle ))
            {
                //find local items
                int index = local_set.FindIndex ( X => PrimarySettings.primary_set [X].app_bundle.Equals ( set [i].app_bundle ) );

                bool is_local = index >= 0;

                if (!use_new_primary_set_logic && is_local)
                    local_set.RemoveAt ( index );


                //such complicated data asignment is due to LitJSON convertor does not handle classes
                set [i] = new PromoItem ( set [i].app_bundle, set [i].app_alias, set [i].GetPromoWeight ( ), set [i].app_name, is_local );
                i++;
            }
            else
            {
                //remove installed or zero weighted items
                set.RemoveAt ( i );
            }
        }



        //add to promo set items left unused inside local_set
        //it will let us show at least something if loading will fail or non zero weighted apps would be unavailable

        if (!use_new_primary_set_logic)
        {
            for (int j = 0; j < local_set.Count; j++) //commented to exclude items not in primary set
            {
                set.Add ( new PromoItem ( PrimarySettings.primary_set [local_set [j]], min_weight, true ) );
            }
        }


        StartCoroutine ( SelectPromoItems ( set, local ) );
    }


    IEnumerator ReceiveAccounntAlias (string b, string p)
    {

        string bundle_id = b == "" ? ServiceUtils.GetAppBundle ( ) : b;
        string platform = p == "" ? ServiceUtils.GetPlatform ( ).ToString ( ) : p;
        int type = 1;
        string key = "PrOmOhAsH";

        string hash = Crypto.Md5Sum ( type.ToString ( ) + bundle_id + platform + key );

        string url = "http://psvpromo.psvgamestudio.com/Scr/getpromoset.php?type=" + type + "&bundle=" + WWW.EscapeURL ( bundle_id ) + "&platform=" + WWW.EscapeURL ( platform ) + "&hash=" + hash;
        if (debug)
           Message.Show( "url " + url );
        int attempts = 0;
        WWW www = new WWW ( url );
        do
        {
            attempts++;
            yield return www;
        } while (!string.IsNullOrEmpty ( www.error ) && attempts < max_retry_efforts);
        if (!string.IsNullOrEmpty ( www.error ))
        {
            if (debug)
                Debug.LogWarning ( "Receive Acc Id from PSV server failed" );
        }
        else
        {
            int i = 0;
            int j = www.text.IndexOf ( ';', i );
            if (j > i)
            {
                SetPublisher ( www.text.Substring ( i, j - i ) );
                SetNextAccAliasUpdate ( );
            }
        }
    }


    IEnumerator ReceivePromoSet (string b, int p)
    {

        string bundle_id = b == "" ? ServiceUtils.GetAppBundle ( ) : b;
        string platform = p < 0 ? ServiceUtils.GetPlatform ( ).ToString ( ) : p.ToString ( );
        int type = 0;
        string key = "PrOmOhAsH";

        string hash = Crypto.Md5Sum ( type.ToString ( ) + bundle_id + platform + key );

        string url = "http://psvpromo.psvgamestudio.com/Scr/getpromoset.php?type=" + type + "&bundle=" + WWW.EscapeURL ( bundle_id ) + "&platform=" + WWW.EscapeURL ( platform ) + "&hash=" + hash;

        if (debug)
            Message.Show( "ReceivePromoSet " + url );

        int attempts = 0;
        WWW www = new WWW ( url );
        do
        {
            attempts++;
            yield return www;

        } while (!string.IsNullOrEmpty ( www.error ) && attempts < max_retry_efforts);
        if (!string.IsNullOrEmpty ( www.error ))
        {
            if (debug)
                Debug.LogWarning ( "Receive PromoSet from PSV server failed" );
        }
        else
        {
            List<PromoItem> set = new List<PromoItem> ( );

            int i = 0;
            while (i < www.text.Length)
            {
                string bundle = "";
                string alias = "";
                string weight = "";


                int j = www.text.IndexOf ( ';', i );
                if (j > i)
                {
                    bundle = www.text.Substring ( i, j - i );
                    i = j + 1;
                    j = www.text.IndexOf ( ';', i );
                    if (j > i)
                    {
                        alias = www.text.Substring ( i, j - i );
                        i = j + 1;
                        j = www.text.IndexOf ( '\n', i );
                        if (j > i)
                        {
                            weight = www.text.Substring ( i, j - i );
                            i = j + 1;
                        }
                    }
                }

                if (!string.IsNullOrEmpty ( bundle ) && !string.IsNullOrEmpty ( alias ) && !string.IsNullOrEmpty ( weight ))
                {
                    float w = 0;
                    float.TryParse ( weight, out w );
                    set.Add ( new PromoItem ( bundle, alias, w ) );
                    if (debug)
                        Debug.Log ( "bundle = " + bundle + "\nalias = " + alias + "\nweight = " + w );
                }
                else
                {
                    break;
                }
            }


            //save promo_set
            if (set.Count > 0)
            {
                if (!Directory.Exists ( GetExternalPath ( ) ))
                {
                    Directory.CreateDirectory ( GetExternalPath ( ) );
                }
                File.WriteAllText ( GetExternalPath ( ) + promo_set_file, LitJson.JsonMapper.ToJson ( set ) );
                InitPromoSet ( set );
                SetNextPromoUpdate ( );
            }

        }
    }


    IEnumerator SelectPromoItems (List<PromoItem> set, bool local)
    {
        List<PromoItem> p_selection = new List<PromoItem> ( );
        List<float> weights = new List<float> ( );
        List<int> w_ind = new List<int> ( );
        for (int i = 0; i < set.Count; i++)
        {
            weights.Add ( set [i].GetPromoWeight ( ) );
            w_ind.Add ( i );
        }
        for (int i = 0; i < set.Count; i++)
        {
            if (weights.Count > 0)
            {
                int selection = WeightedRandom.RandomW ( weights.ToArray ( ) );
                p_selection.Add ( set [w_ind [selection]] );
                weights.RemoveAt ( selection );
                w_ind.RemoveAt ( selection );
            }
        }
        while (blocking_access)
        {
            yield return null;
        }
        //        yield return new WaitUntil ( () => blocking_access == false )
        BlockAccess ( true );
        promo_set = p_selection;
        if (local)
            AnnounceAvailableItems ( );
        EnqueDownloads ( true );
        BlockAccess ( false );
    }


    void AnnounceAvailableItems ()
    {
        for (int i = 0; i < promo_set.Count; i++)
        {
            int index = available_set.FindIndex ( X => X.app_bundle.Equals ( promo_set [i].app_bundle ) );
            if (index >= 0)
            {
                if (OnPackageReceived != null)
                {
                    OnPackageReceived ( available_set [index] );
                }
            }
        }
    }



    void PrintPromoSet ()
    {
        string res = "";
        for (int i = 0; i < promo_set.Count; i++)
        {
            res += "| " + promo_set [i].app_bundle + " | " + promo_set [i].app_alias + " | " + promo_set [i].app_name + " | " + promo_set [i].app_weight + "|\n";
        }
        Debug.Log ( res );
    }


    void EnqueDownloads (bool new_data = false)
    {
        List<PackageInfo> queue = new List<PackageInfo> ( );
        for (int i = 0; i < promo_set.Count; i++)
        {
            if (!promo_set [i].IsLocal ( ))
            {
                if (available_set.FindIndex ( X => X.app_bundle.Equals ( promo_set [i].app_bundle ) ) < 0)
                {
                    queue.Add ( new PackageInfo ( promo_set [i].app_bundle, promo_set [i].app_alias ) );
                }
            }
        }
        MarketBridge.Instance.EnqueItems ( queue.ToArray ( ), ovverride_locale: PrimarySettings.use_locale_override );
        downloads_in_progress += queue.Count;
        //we reset efforts with every external call
        if (new_data && delayed_download != null)
        {
            StopCoroutine ( delayed_download );
        }

        retry_efforts = !new_data ? retry_efforts + 1 : 0;

        if (downloads_in_progress > 0 && retry_efforts < max_retry_efforts)
        {
            delayed_download = StartCoroutine ( DelayedRetry ( ) );
        }
    }


    IEnumerator DelayedRetry ()
    {
        yield return new WaitForSeconds ( retry_delay );
        while (downloads_in_progress > 0)
        {
            yield return new WaitForSeconds ( retry_delay );
        }
        if (faulty_downloads > 0)
            EnqueDownloads ( );
    }


    public PackageData [] GetPromoItems (int required_items)
    {
        List<PackageData> res = new List<PackageData> ( );
        if (!blocking_access)
        {
            BlockAccess ( true );
            for (int i = 0; i < promo_set.Count; i++)
            {
                PackageData item = GetPromoItem ( promo_set [i].app_bundle );
                if (item != null && item.app_icon != null)
                {
                    res.Add ( item );
                }
                //limit items count to required
                if (res.Count == required_items)
                    break;
            }
            BlockAccess ( false );
        }

        if (res.Count == 0)
        {
            if (PromoModule.debug)
                Debug.LogWarning ( "There are no available items from promo_set" );
            if (use_new_primary_set_logic)
            {
                PackageData item = GetRandomPrimarySetItem ( );
                if (item != null)
                {
                    res.Add ( item );
                }
                else
                {
                    Debug.LogError ( "Can't get random primary set item" );
                }
            }
            else
            {
                //old style behaviour for sliding UI
                //this code give us first available items
                for (int i = 0; i < available_set.Count; i++)
                {
                    if (available_set [i].app_icon != null)
                    {
                        res.Add ( available_set [i] );
                    }
                    //limit items count to required
                    if (res.Count == required_items)
                        break;
                }
            }
        }

        return res.ToArray ( );
    }


    PackageData GetRandomPrimarySetItem ()
    {
        PackageData res = null;
        if (PrimarySettings.primary_set.Count > 0)
        {
            res = GetPromoItem ( PrimarySettings.primary_set [Random.Range ( 0, PrimarySettings.primary_set.Count )].app_bundle );
        }
        return res;
    }


    PackageData GetPromoItem (string app_bundle)
    {
        int index = available_set.FindIndex ( X => X.app_bundle.Equals ( app_bundle ) );
        if (index >= 0)
            return available_set [index];
        else
            return null;
    }



    void SaveData ()
    {
        List<PackageInfo> buffer = new List<PackageInfo> ( );
        for (int i = 0; i < available_set.Count; i++)
        {
            if (PrimarySettings.primary_set.FindIndex ( X => X.app_bundle.Equals ( available_set [i].app_bundle ) ) < 0)
            {
                buffer.Add ( new PackageInfo ( available_set [i].app_bundle, available_set [i].app_alias, available_set [i].app_name ) );
            }
        }
        File.WriteAllText ( GetExternalPath ( ) + dat_file, LitJson.JsonMapper.ToJson ( buffer ) );
    }


    void SaveIcon (Texture2D icon, string app_bundle)
    {
        File.WriteAllBytes ( GetExternalPath ( ) + app_bundle + ".png", icon.EncodeToPNG ( ) );
    }

    void ProcessData (string app_bundle, PackageData data)
    {
        if (data != null && data.app_icon != null)
        {
            //if (ServiceUtils.IsAppAllowed ( data.app_bundle ))
            //{
            available_set.Add ( data );
            if (OnPackageReceived != null)
            {
                OnPackageReceived ( data );
            }
            //}
            SaveIcon ( data.app_icon, app_bundle );
            SaveData ( );
        }
        else
        {
            faulty_downloads++;
        }

        downloads_in_progress--;

        if (debug)
            Debug.Log ( app_bundle + " request ended" + (data == null ? "with fail" : "") );
    }



    private string GetExternalPath ()
    {
        return Application.persistentDataPath + "/PSVPromo/";
    }

}
