﻿using UnityEngine;
using System.Collections;


public struct PromoItem
{
    public string
        app_bundle;

    public string
        app_alias;

    public string
        app_name;

    public double
        app_weight;

    public bool
        is_local;


    public bool IsLocal ()
    {
        return is_local;
    }

    public float GetPromoWeight ()
    {
        return (float) app_weight;
    }

    public PromoItem (string bundle = "", string alias = "", float weight = 0, string name = "", bool local = false)
    {
        app_alias = alias;
        app_bundle = bundle;
        app_name = name;
        app_weight = weight;
        is_local = local;
    }

    public PromoItem (PackageInfo info, float weight = 0,  bool local = false)
    {
        app_alias = info.app_alias;
        app_bundle = info.app_bundle;
        app_name = info.app_name;
        app_weight = weight;
        is_local = local;
    }
}