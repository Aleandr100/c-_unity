﻿using UnityEngine;
using System.Collections;

public struct PackageInfo {

    public string
        app_bundle;

    public string
        app_alias;

    //Using device lang
    public string
        app_name;

    public PackageInfo (string bundle = "",  string alias = "", string _name = ""  )
    {
        app_bundle = bundle;
        app_name = _name;
        app_alias = alias;
    }
}


public class PackageData
{
    public string
        app_bundle,
        app_alias;


    [Tooltip ( "Use english by default" )]
    public string
        app_name;
    public Texture2D
        app_icon;


    public PackageData (string bundle = "", string alias = "", string _name = "", Texture2D icon = null)
    {
        app_name = _name;
        app_alias = alias;
        app_bundle = bundle;
        app_icon = icon;
    }

    public PackageData (PackageInfo info, Texture2D icon = null)
    {
        app_bundle = info.app_bundle;
        app_alias = info.app_alias;
        app_name = info.app_name;
        app_icon = icon;
    }
}