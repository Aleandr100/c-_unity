﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PromoModule))]
public class PromoModuleEditor : Editor {


    PromoModule example
    {
        get
        {
            return target as PromoModule;
        }
    }


    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ( );
        if (GUILayout.Button ( "LoadPrimarySetIcons" ))
        {
            example.LoadPrimarySetIcons ( );
        }
    }


}
