﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PannelResizer))]
public class PannelResizerEditor : Editor {

	private PannelResizer example{
		get{
			return target as PannelResizer;
		}
	}


	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		if (GUILayout.Button("Resize")){
			example.ResizePannel();
		}
	}
}
