﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class PrimarySettings
{
    public static bool
        show_banner_on_disable = false, //determines if small banner will be shown after bromo scene will be hidden (by default small banner should be hidden on promo scene)
        announce_local = true, //Loading resources will take some time and if promo scene will go first promo set won't be ready. This option will fix that
        receive_app_name = false, //determines whether script will search for app_name on application's page from market (app_name is optional variable)
        use_locale_override = false; //determines wether to use locale that is detected by market (usually by device location) or ovverride it in accordence to device language and supported_languages

    //this string is ussed to open publishers account by pressing more games button
    public static string
#if UNITY_ANDROID
        default_acc = "Лунтик+Moonzy+Барбоскины";
#elif UNITY_IPHONE
        default_acc = "id1024436268";
#elif UNITY_WP8 || UNITY_WP8_1
        default_acc = "PsV%20studio";
#endif


    //this list is used to display set of applications in promo by default (when no connection was performed yet)
    public static List<PackageInfo>
        primary_set = new List<PackageInfo> ( )
        {
            // bundle id                                               link for market                              app name (optional)

#if UNITY_ANDROID //dublicate android bundle in two columns, last column used for name of the application
            { new PackageInfo("com.psvn.KidsBicycle",               "com.psvn.KidsBicycle",                     "Kids Bicycle") },
            { new PackageInfo("com.PSVStudio.HippoSkate",           "com.PSVStudio.HippoSkate",                 "Kids Skateboard") },
            { new PackageInfo("com.PSVStudio.SnowGame",             "com.PSVStudio.SnowGame",                   "Kids Winter Games") },
#elif UNITY_IPHONE
            { new PackageInfo("com.Kidnimals.Circus",               "id1040513567",                             "Kidnimals Circus") },
            { new PackageInfo("com.cibgHippo.Babyshop",             "id1035322849",                             "Hippo Pepa: Baby Shop") },
            { new PackageInfo("com.cibgHippo.BabyTalkingPhone",     "id1036844001",                             "Hippo Pepa: Talking Phone") },

#elif UNITY_WP8 || UNITY_WP8_1
            { new PackageInfo("com.psvn.babyshopingfree",           "bdc151fe-9948-4493-a4c7-9a950a33ae74",     "Kids Shop") },
            { new PackageInfo("com.psvn.peppaTydingupfree",         "0aff95cb-40e2-4655-9199-e342217c1fa6",     "House Cleaning") },
            { new PackageInfo("com.PSVStudio.Airport",              "71871338-2a43-4603-9529-006ff13566dd",     "Peppa Airport Adventures") },
#endif
        };

    //this list determines what device languages will be processed by promo tho load add_name in this locale (other languages will be recognised as English)
    public static List<SystemLanguage>
       supported_languages = new List<SystemLanguage> ( )
       {          
                SystemLanguage.English, //always leave this (used by default)
                SystemLanguage.Russian,
                SystemLanguage.Ukrainian,
       };

}
