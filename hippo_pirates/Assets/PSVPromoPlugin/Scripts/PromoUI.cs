﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PromoUI :MonoBehaviour
{
    public PannelResizer
        resizer;

    public RectTransform
        mask;


    private List<PromoButton>
        buttons;

    public bool
        fixed_buttons_set = false;

    private bool
        announce = true;


    public GameObject
        button_prefab;

    public int
        max_items = 10;

    public ScrollRect
        scroller;

    public Vector2
        default_scroll_pos = new Vector2 ( 0, 1 );

    public float
        visible_items = 4.5f,
        items_size = 60;

    void Awake ()
    {
        buttons = new List<PromoButton> ( );
        if (fixed_buttons_set)
        {
            //buttons.AddRange ( GetComponentsInChildren<PromoButton> (true ) );
            GetComponentsInChildren<PromoButton> ( true, buttons );
        }
        else
        if (scroller != null)
        {
            for (int i = 0; i < max_items; i++)
            {
                buttons.Add ( CreateButton ( ) );
            }
        }
    }





    void OnEnable ()
    {
        //if (ManagerGoogle.Instance != null)
        //{
        //    ManagerGoogle.Instance.HideSmallBanner ( );
        //}
        //else
        //{
        //    Debug.Log ( "ManagerGoogle not initialised" );
        //}

        StartCoroutine( InitButtons ( ));
        PromoModule.OnPackageReceived += GenerateButton;
    }


    IEnumerator InitButtons ()
    {
        yield return new WaitForEndOfFrame ( );
        Init ( );
    }

    void OnDisable ()
    {
        PromoModule.OnPackageReceived -= GenerateButton;

        //if (PrimarySettings.show_banner_on_disable && ManagerGoogle.Instance != null)
        //{
        //    ManagerGoogle.Instance.ShowSmallBanner ( );
        //}
        //else if (PrimarySettings.show_banner_on_disable)
        //{
        //    Debug.Log ( "ManagerGoogle not initialised" );
        //}

    }

    void PrintPromoSet (PackageData [] set)
    {
        string res = "";
        for (int i = 0; i < set.Length; i++)
        {
            res += "| " + set [i].app_bundle + " | " + set [i].app_alias + " | " + set [i].app_name + "|\n";
        }
        Debug.Log ( res );
    }

    private bool IsVertical ()
    {
        bool res = false;
        if (resizer != null)
        {
            res = resizer.max_size.x < resizer.max_size.y;
        }
        return res;
    }


    public void InitSettings ()
    {
        if (mask && scroller && resizer)
        {
            RectTransform scroll_t = scroller.GetComponent<RectTransform> ( );
            VerticalLayoutGroup vert_layout = GetComponentInChildren<VerticalLayoutGroup> ( );
            HorizontalLayoutGroup hor_layout = GetComponentInChildren<HorizontalLayoutGroup> ( );
            if (scroll_t && vert_layout || hor_layout)
            {
                Vector2 borders = new Vector2 ( Mathf.Abs ( scroll_t.offsetMax.x ) + Mathf.Abs ( scroll_t.offsetMin.x ),
                                                Mathf.Abs ( scroll_t.offsetMax.y ) + Mathf.Abs ( scroll_t.offsetMin.y ) );

                Vector2 paddings = new Vector2 ( (hor_layout != null ? hor_layout.padding.left + hor_layout.padding.right : 0),
                                                    (vert_layout != null ? vert_layout.padding.top + vert_layout.padding.bottom : 0) );

                float spacing = vert_layout != null ? vert_layout.spacing : hor_layout.spacing;

                float visible_size = (visible_items * items_size) + (spacing * visible_items);
                float content_width = visible_size + borders.x + paddings.x;
                float content_height = visible_size + borders.y + paddings.y;

                if (IsVertical ( ))
                {
                    mask.sizeDelta = new Vector2 ( items_size + borders.x, content_height );
                    resizer.max_size = new Vector2 ( 0, content_height );
                }
                else
                {
                    mask.sizeDelta = new Vector2 ( content_width, items_size + borders.y );
                    resizer.max_size = new Vector2 ( content_width, 0 );
                }
#if UNITY_EDITOR
                Debug.Log ( "New size is set" );
#endif
            }
        }
    }


    void Init ()
    {
        //if (resizer == null)
        //{
        //    Debug.LogWarning ( "No resizer attached" );
        //}
        announce = false;
        ClearButtons ( );
        PackageData [] items = PromoModule.Instance.GetPromoItems ( buttons.Count );
        if (PromoModule.debug)
        {
            Debug.Log ( "PromoUI GetItems(), received items " + items.Length );
            PrintPromoSet ( items );
        }
        for (int i = 0; i < items.Length; i++)
        {
            GenerateButton ( items [i] );
        }
        AnnounseChanges ( );
        announce = true;
    }

    IEnumerator SetScrollPos ()
    {
        yield return new WaitForEndOfFrame ( );
        scroller.normalizedPosition = default_scroll_pos;
    }


    void ClearButtons ()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            if (buttons [i].gameObject.activeSelf)
                buttons [i].gameObject.SetActive ( false );
        }
    }

    void GenerateButton (PackageData data)
    {
        PromoButton free_button = GetFreeButton ( data.app_bundle );
        if (free_button != null)
        {
            free_button.gameObject.SetActive ( true );
            free_button.Init ( data );
            if (announce)
                AnnounseChanges ( );
        }
    }


    PromoButton GetFreeButton (string app_bundle)
    {
        int res = buttons.FindIndex ( X => X.GetAppBundle ( ).Equals ( app_bundle ) );
        if (res < 0)
        {
            res = buttons.FindIndex ( X => X.gameObject.activeSelf == false );
            if (res < 0)
            {
                if (PromoModule.debug)
                    Debug.Log ( "No free button for " + app_bundle );
            }
        }
        else
        {
            if (PromoModule.debug)
                Debug.Log ( "Button exists " + app_bundle );
        }


        //int res = -1;
        //for (int i = buttons.Count - 1; i >= 0; i--)
        //{

        //    if (!buttons [i].gameObject.activeSelf)
        //    {
        //        res = i;
        //    }
        //    else
        //    if (buttons [i].GetAppBundle ( ).Equals ( app_bundle ))
        //    {

        //        res = -1;
        //        break;
        //    }
        //}

        return (res >= 0) ? buttons [res] : null;
    }


    void AnnounseChanges ()
    {
        if (resizer != null)
        {
            resizer.ResizePannel ( );
        }
        if (scroller != null)
            StartCoroutine ( SetScrollPos ( ) );
    }



    private PromoButton CreateButton ()
    {
        GameObject res = Instantiate ( button_prefab ) as GameObject;
        res.SetActive ( false );
        res.transform.SetParent ( scroller.content.transform );
        res.transform.localScale = Vector3.one;
        LayoutElement le = res.GetComponent<LayoutElement> ( );
        if (le)
        {
            le.preferredHeight = le.preferredWidth = items_size;
        }
        else
        {
            Debug.Log ( "LayoutElement on button_prefab not found" );
        }
        return res.GetComponent<PromoButton> ( );
    }


}