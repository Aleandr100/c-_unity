﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CanvasBlocker : MonoBehaviour {

    public delegate void Callback (bool locked);
    public delegate void ActionCallback ();

    public static event Callback OnCanvasLocked;
    public static event ActionCallback OnClick;

    
    void OnEnable ()
    {
        if (OnCanvasLocked != null)
        {
            OnCanvasLocked ( true );
        }
    }
    
    void OnDisable ()
    {
        if (OnCanvasLocked != null)
        {
            OnCanvasLocked ( true );
        }
    }

    public void Action (BaseEventData e)
    {
        if (OnClick != null)
        {
            OnClick ( );
        }
    }


}
