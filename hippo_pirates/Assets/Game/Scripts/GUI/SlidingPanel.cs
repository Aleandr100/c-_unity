﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;


public class SlidingPanel : MonoBehaviour {

    protected bool
            sliderShown = false,
            isMoving = false;

    protected RectTransform
        Slider;
    public RectTransform
        Rotator;

    public ScrollRect
       scroller;

    //public bool
    //    slide_up = true;

    [Tooltip("Set y = 1 to slide up or y = -1 to slide down")]
    public Vector2
        slide_direction;

    public float
        sliderDeuration = 0.5f;

    private Sequence
        seq = null;

    void Awake ()
    {
        Slider = GetComponent<RectTransform> ( );
    }


    virtual public void Init ()
    {
        isMoving = sliderShown = false;
        if (Rotator != null)
        {
            Rotator.localRotation = Quaternion.identity;
        }
        if (Slider != null)
        {
            Slider.localPosition = Vector3.zero;
        }
    }


    void Start ()
    {
        Init ( );
    }


    public void ShowMenu ()
    {
        if (seq != null && isMoving)
        {
            seq.Kill ( );
        }
        sliderShown = !sliderShown;
        if (sliderShown)
        {
            //if (scroller)
            //{
            //    scroller.verticalNormalizedPosition = (Languages.GetLanguageOrder ( ) + 1) / (float) Languages.languages.Count;
            //}
        }
        isMoving = true;
        seq = DOTween.Sequence ( );
        if (Rotator != null)
        {
            seq.Join ( Rotator.DOLocalRotate ( sliderShown ? new Vector3 ( 0, 0, 180f ) : Vector3.zero, sliderDeuration ) );
        }
        if (Slider != null)
        {
            //				Vector2 sliderPos = sliderShown ? Vector2.zero : sliderOffset;
            Slider.DOLocalMove ( new Vector2 ( sliderShown ? Slider.rect.width * slide_direction .x : 0, sliderShown ? Slider.rect.height * slide_direction.y : 0), sliderDeuration, true ).SetEase ( Ease.OutSine );
        }
        seq.AppendCallback ( () =>
        {
            isMoving = false;
        } );
    }


    //public bool OnEsc ()
    //{
    //    if (sliderShown)
    //    {
    //        ShowMenu ( );
    //        return true;
    //    }

    //    return false;
    //}
}
