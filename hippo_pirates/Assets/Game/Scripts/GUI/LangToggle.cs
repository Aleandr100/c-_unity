﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LangToggle : MonoBehaviour {

    public Languages.Language
        lang;


    public void SetLang ()
    {
        Languages.SetLanguage ( lang );
    }


    void Start ()
    {
        Toggle toggle = GetComponent<Toggle> ( );
        if (toggle)
        {
            if (Languages.GetLanguage ( ) == lang)
            {
                toggle.isOn = true;
            }
            //toggle.isOn = Languages.GetLanguage ( ) == lang;
        }
        else
        {
            Debug.LogError ( "WARNING: Toggle not attached to " + name );
        }
    }
    
}
