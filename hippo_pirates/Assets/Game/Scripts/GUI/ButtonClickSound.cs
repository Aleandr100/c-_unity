﻿using UnityEngine;
using System.Collections;

public class ButtonClickSound : MonoBehaviour
{
	//	public enum ClickSounds
	//	{
	//		ButtonCLick1,
	//		ButtonCLick2,
	//	}

	//	public ClickSounds CLickSound = ClickSounds.ButtonCLick1;

	private string ClickSound = "Button - Click";
	private string soundStart = "start";


	public void PlaySound()
	{
		AudioController.PlaySound(ClickSound);
		Vibration.Vibrate(10);
//		AudioController.PlaySound(CLickSound.ToString());
	}

	public void PlaySoundStart()
	{
		AudioController.PlaySound(soundStart);
		Vibration.Vibrate(10);
	}


}
