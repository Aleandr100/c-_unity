﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SettingsPanel :MonoBehaviour
{
    private bool
        pannel_shown = false;
    public float
        anim_duration = 0.2f;
    public Ease
        easing = Ease.InCubic;


    public RectTransform
        pannel;

    public GameObject
        blocker;





    public void OnEnable ()
    {
        ShowPannel ( false, false );
        CanvasBlocker.OnClick += OnBlockerClick;
    }

    public void OnDisable ()
    {
        CanvasBlocker.OnClick -= OnBlockerClick;
    }


    void OnBlockerClick ()
    {
        //hide pannel
        ShowPannel ( false );
    }


    void ShowPannel (bool param, bool use_tween = true)
    {
        pannel_shown = param;
        if (use_tween)
            pannel.DOScale ( pannel_shown ? 1 : 0, anim_duration ).SetEase( easing );
        else
            pannel.localScale = Vector3.zero;
        blocker.SetActive ( pannel_shown );
    }



    public void OpenSettingsPannel ()
    {

        //Debug.Log ( "activate blocker and scale pannel" );
        ShowPannel ( !pannel_shown );
    }

}
