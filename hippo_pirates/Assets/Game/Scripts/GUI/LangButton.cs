﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LangButton :MonoBehaviour
{
    [Header ("en fr ge pt ru sp")]
    public Sprite [] 
        lang_array;    //set here lang sprites in strong order of their index in lang_order


    private Dictionary<Languages.Language, int> 
        lang_order = new Dictionary<Languages.Language, int> //defines accordance of language to lang_array items by their index in array
    {
        { Languages.Language.English, 0 },
		{ Languages.Language.Italian, 1 },
		{ Languages.Language.Portuguese, 2 },
		{ Languages.Language.Spanish, 3 },
		{ Languages.Language.Russian, 4 }
    };

    private Image
        img;

    public int GetLanguageOrder (Languages.Language current_lang)
    {
        if (lang_order.ContainsKey ( current_lang ))
            return lang_order [current_lang];
        else
            return -1;
    }



    void Awake ()
    {
        img = GetComponent<Image> ( );
    }

    void OnEnable ()
    {
        Languages.OnLangChange += SetSprite;
    }


    void OnDisable ()
    {
        Languages.OnLangChange -= SetSprite;
    }



    void SetSprite (Languages.Language current_lang)
    {
        img.sprite = lang_array[GetLanguageOrder(current_lang)];
    }

    void Start ()
    {
        Languages.Language current_lang = Languages.GetLanguage ( );
        SetSprite ( current_lang);        
    }

}
