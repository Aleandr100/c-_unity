﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioToggle : MonoBehaviour
{
	public enum Toggles
	{
		Music,
		Sounds,
		Vibro,
	}


	public Toggles ToggleType;

	private Toggle toggle_cmp;

	void Start()
	{
		toggle_cmp = GetComponent<Toggle>();
		bool param = false;
		switch (ToggleType)
			{
			case Toggles.Music:
				{
					param =	!GameSettings.IsMusicEnabled();
					break;
				}
			case Toggles.Sounds:
				{
					param =	!GameSettings.IsSoundsEnabled();
					break;
				}
			case Toggles.Vibro:
				{
					param =	!GameSettings.IsVibroEnabled();
					break;
				}
			}

        //this stuff is done to avoid listeners from catching init changes events
        Toggle.ToggleEvent evnt = toggle_cmp.onValueChanged;
        toggle_cmp.onValueChanged = new Toggle.ToggleEvent();

        toggle_cmp.isOn = param;

        toggle_cmp.onValueChanged = evnt;
    }


	public void Action(bool param)
	{
		//Debug.Log("Toggle action = " + ToggleType.ToString());
		switch (ToggleType)
			{
			case Toggles.Music:
				{
					GameSettings.EnableMusic(!param);
					break;
				}
			case Toggles.Sounds:
				{
					GameSettings.EnableSounds(!param);
					break;
				}
			case Toggles.Vibro:
				{
					GameSettings.EnableVibro(!param);
					break;
				}
			}
	}

}
