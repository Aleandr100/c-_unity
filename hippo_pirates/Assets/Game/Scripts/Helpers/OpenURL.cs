﻿using UnityEngine;
using System.Collections;

public class OpenURL : MonoBehaviour {

    public string
        URL = "http://www.psvgamestudio.com/";


    public void Action ()
    {

#if UNITY_WEBGL
        Application.ExternalEval ( "window.open(\"" + URL + "\")" );
#else
        Application.OpenURL ( URL );
#endif
    }


}
