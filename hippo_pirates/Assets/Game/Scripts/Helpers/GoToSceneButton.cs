﻿using UnityEngine;
using System.Collections;

public class GoToSceneButton : MonoBehaviour
{

	public SceneLoader.Scenes target;
    

	public void GoToScene()
	{
		SceneLoader.Instance.SwitchToScene(target);
	}


}
