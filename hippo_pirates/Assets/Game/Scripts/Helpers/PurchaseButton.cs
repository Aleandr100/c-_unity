﻿using UnityEngine;
using System.Collections;

public class PurchaseButton :MonoBehaviour
{

    // Use this for initialization
    void Start ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        this.gameObject.SetActive ( !AdmobManager.Instance.isAdvertsDisabled );
#endif
    }

    void OnEnable ()
    {
        ManagerGoogle.OnAdsDisabled += DisableButton;
    }

    void OnDisable ()
    {
        ManagerGoogle.OnAdsDisabled -= DisableButton;
    }


    public void Action ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        BillingManager.Instance.Purchase ( BillingManager.SKU_ADMOB );
#endif
    }


    private void DisableButton ()
    {
        this.gameObject.SetActive ( false );
    }

}
