﻿using UnityEngine;
using System.Collections;

public class Initialiser : MonoBehaviour
{

	void Awake()
	{
#if UNITY_EDITOR
        //PlayerPrefs.DeleteAll ( );
#endif
        GameSettings.UpdateSettings();
		AudioController.InitStreams(gameObject);
		Languages.Init();
        //LocalizationManager.Init ( ); //uncomment if need localisastion (unnecessary resources load will slow down process)
		DG.Tweening.DOTween.Init(true, true, DG.Tweening.LogBehaviour.Verbose).SetCapacity(200, 10);
	}


}
