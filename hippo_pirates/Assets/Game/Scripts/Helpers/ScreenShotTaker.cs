﻿using UnityEngine;
using System.Collections;


public class ScreenShotTaker : MonoBehaviour
{
	public const KeyCode CaptureButton = KeyCode.F12;

	[Header("Use F12 to save screenshot in Assets folder")]
	public int ScreenshotSize = 2;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	// Use this for initialization
	void Start()
	{
			
	}

	void OnEnable()
	{
		KeyListener.OnKeyPressed += TakeScreenShot;
	}


	void OnDisable()
	{
		KeyListener.OnKeyPressed -= TakeScreenShot;
	}

	public void TakeScreenShot(KeyCode key)
	{
		if (key == CaptureButton)
			{
				print("Screenshot taken");
				Application.CaptureScreenshot("Sccreenshot_" +
				System.DateTime.Now.Hour.ToString() +
				"-" +
				System.DateTime.Now.Minute.ToString() +
				"-" +
				System.DateTime.Now.Second.ToString() + ".png", 
					ScreenshotSize);
			}
	}


	// Update is called once per frame
	//	void Update()
	//	{
	//		if (Input.GetKeyDown(CaptureButton))
	//			{
	//					TakeScreenShot();
	//			}
	//	}
}
