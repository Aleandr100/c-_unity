﻿using UnityEngine;
using System.Collections;

public class KeyListener : MonoBehaviour
{

	public delegate void Callback(KeyCode key);

	public static event Callback OnKeyPressed;

	public KeyCode[] keys_to_check = new KeyCode[] {
		KeyCode.Escape,
		ScreenShotTaker.CaptureButton,	//		KeyCode.F12,
	};


	void Awake()
	{
		DontDestroyOnLoad(this);
	}


	
	// Update is called once per frame
	void Update()
	{
		if (OnKeyPressed != null)
			{
				for (int i = 0; i < keys_to_check.Length; i++)
					{
						if (Input.GetKeyDown(keys_to_check [i]))
							{
								OnKeyPressed(keys_to_check [i]);
//								Debug.Log("Key pressed : " + keys_to_check [i].ToString());
							}
					}
			}

	}
}
