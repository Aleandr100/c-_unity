﻿using System;
using UnityEngine;

public class ManagerGoogle : MonoBehaviour
{
    public delegate void Callback ();

    public static event Callback OnAdsDisabled;
    public static event Callback OnInterstitialShown;
    public static event Callback OnInterstitialClosed;

    static public ManagerGoogle Instance;

    private float last_time = 0f;		// 0
    private float bannertime = 30f;		// 20

    void Awake ()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad ( this );
        }
    }

    void Start ()
    {
        Init ( );
    }

    public void Init ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        BillingManager.Instance.MapSKU ( );
        BillingManager.Instance.Init ( );

        AdmobManager.Instance.Init ( false, false );
#endif

        //		if (AdmobManager.Instance.isAdvertsDisabled)
        //			{
        //				purchase_btn.SetActive(false);
        //			}

        //		if (!FB.IsInitialized) {
        //			InitFacebook ();
        //		}
    }

    public void InitFacebook ()
    {
        //		FB.Init (delegate{});
    }

    //	void LoginCallback(FBResult result)
    //	{
    //		if (result.Error != null)
    //			Debug.Log ("Error Response:\n" + result.Error);
    //		else if (!FB.IsLoggedIn) {
    //			Debug.Log ("Login cancelled by Player");
    //		}
    //		else {
    //			Debug.Log ("Login was successful!");
    //			FacebookPost ();
    //		}
    //	}

    private void FacebookPost ()
    {
        //		FB.Feed (
        //			link: "https://play.google.com/store/apps/details?id=com.PSVStudio.kidsacademy",
        //			linkName: "I am playing Kid's Academy!",
        //			linkDescription: " ",
        //			picture: "http://psvgamestudio.com/data/PICS/kidsacademy.png"
        //		);
    }

    public void FacebookButton ()
    {
        //		if (!FB.IsInitialized) {
        //			InitFacebook ();
        //		} else if (FB.IsLoggedIn) {
        //			FacebookPost ();
        //		} else {
        //			InitFacebook ();
        //			FB.Login("email,publish_actions", LoginCallback);
        //		}
    }

    //////////////////////////////////////
    /////////////-AdMob Ads-//////////////
    //////////////////////////////////////

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1

    public void LoadInterstitialAd ()
    {
        AdmobManager.Instance.LoadBanner ( );
    }

    public bool ShowFullscreenBanner ()
    {
        if (!IsAdmobDisabled ( ))
        {
            float time = Time.time;
            if (time - last_time >= bannertime)
            {
                if (AdmobManager.Instance.ShowInterstitial ( ))
                {
                    last_time = time;
                    if (OnInterstitialShown != null)
                    {
                        OnInterstitialShown ( );
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void OnFullscreenbannerClosed ()
    {
        if (OnInterstitialClosed != null)
        {
            OnInterstitialClosed ( );
        }
    }


    public void HideSmallBanner ()
    {
        AdmobManager.Instance.HideBanner ( );
    }

    public void ShowSmallBanner ()
    {
        if (!IsAdmobDisabled ( ))
        {
            AdmobManager.Instance.ShowBanner ( );
        }
    }

    public void RepositionSmallBanner (AdmobAd.AdLayout layout)
    {
        //check if banner in needed position to avoid its unnecessary reload
        if (!IsAdmobDisabled ( ) && AdmobManager.Instance.BannerAdPosition != layout)
        {
            AdmobAd.Instance().RepositionBannerAd ( layout );
        }
    }

    public void DisableAdmob ()
    {
        AdmobManager.Instance.DisableAllAds ( );
        if (OnAdsDisabled != null)
        {
            OnAdsDisabled ( );
        }
    }

    public bool IsAdmobDisabled ()
    {
        return AdmobManager.Instance.isAdvertsDisabled;
    }

#endif

}
