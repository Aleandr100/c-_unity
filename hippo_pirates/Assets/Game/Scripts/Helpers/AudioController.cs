﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class AudioController : MonoBehaviour
{
	// PROPERTIES
	public static AudioController Instance { get; private set; }

	// 
	static private List<AudioSource> streams = new List<AudioSource>();
	static private List<AudioSource> musics = new List<AudioSource>();

	static private bool leave_music = false;

	//for playing Non/Language audio by name
	private const string PAUSE = "||";
	private static AudioClip clip;

	private static List<AudioSource>
		Src = new List<AudioSource>();

	private static AudioSource MusicSrc;

	// PASTER
	private static AudioSource StreamSrc;

	private static int
		streams_count = 5;

	private static GameObject
		SrcParrent;

	private static List<Sequence>
		_tweens = new List<Sequence>();



	private void Awake()
	{
		Instance = this;
	}


	#region Methods to play sound as string (clip name)



	public static void PlaySound(string[] snd_name, float interval = 0) // "||2.2" gives extra pause between phrases
	{
		AudioClip[] sounds = new AudioClip[snd_name.Length];
		Sequence stack = DOTween.Sequence();
		_tweens.Add(stack);
		float t = 0;
		stack.OnComplete(() => _tweens.Remove(stack));
		for (int i = 0; i < snd_name.Length; i++)
		{
			if (snd_name[i].Substring(0, 2).Equals(PAUSE))
			{
				float delay = 0;
				float.TryParse(snd_name[i].Substring(2), out delay);
				t += delay;
			}
			else
			{
				sounds[i] = LanguageAudio.GetSoundByName(snd_name[i]);
				AudioClip clip = sounds[i];
				stack.InsertCallback(t, () =>
				{
					PlayStream(GetSrc(), clip);
				});
				if (sounds[i] == null)
				{
					Debug.Log("Sound " + snd_name[i] + " is null");
				}
				else
				{
					t += sounds[i].length + interval;
				}
			}
		}
	}

	// PASTER
	public void PlaySound(string myString, float myDelay)
	{
		StartCoroutine(DelayPlaySound(myString, myDelay));
	}
	
	// PASTER
	private IEnumerator DelayPlaySound(string myString, float myDelay)
	{
		yield return new WaitForSeconds(myDelay);

		PlaySound(myString);
	}

	// PASTER
	public static void PlayStreamCicle(string stream, AudioSource Src = null)
	{
		AudioSource current_src = Src == null ? StreamSrc : Src;
		if (current_src == null)
		{
			current_src =
				StreamSrc = CreateSrc();
		}
		AudioClip clip = LanguageAudio.GetSoundByName(stream);
		AudioController.PlayStream(current_src, clip, true);
	}

	// PASTER
	public static void StopSound(string myClip)
	{
		AudioClip a_clip = LanguageAudio.GetSoundByName(myClip);

		if (a_clip != null)
		{
			for (int i = 0; i < streams.Count; i++)
			{
				if (streams[i].clip == a_clip)
				{
					if (streams[i].isPlaying)
						streams[i].Stop();
				}
			}
		}
	}

	// PASTER
	public static float GetClipLenght(string snd_name)
	{
		if (snd_name != "")
		{
			AudioClip clip = LanguageAudio.GetSoundByName(snd_name);
			if (clip != null)
			{
				return clip.length;
			}
		}

		return 0;
	}

	// PASTER
	public void StopAll()
	{
		StopAllCoroutines();
	}

	public static void PlayMusic (string music, AudioSource Src = null)
    {
        AudioSource current_src = Src == null ? MusicSrc : Src;
        if (current_src == null)
        {
            current_src =
                MusicSrc = CreateSrc ( );
        }
        AudioClip clip = LanguageAudio.GetSoundByName ( music );
        AudioController.PlayMusic ( current_src, clip );
    }

    public static void PlaySound (string snd_name)
    {
        if (snd_name != "")
        {
            clip = LanguageAudio.GetSoundByName ( snd_name );
	        if (clip != null)
	        {
				PlayStream(GetSrc(), clip);
			}
	        
            else
            {
                Debug.LogWarning ( "AudioClip null " + snd_name );
            }
        }
        else
        {
            Debug.Log ( "Utils.Play sound called with null argument" );
        }
    }

	#endregion

	#region Own AudioSources management

	public static void InitStreams (GameObject src_parrent)
    {
        SrcParrent = src_parrent;
        if (SrcParrent == null)
        {
            Debug.Log ( "AudioSourcePool not found" );
            return;
        }
        streams_count = PlayerPrefs.GetInt ( "StreamsCount", streams_count );
        for (int i = 0; i < streams_count; i++)
        {
            Src.Add ( CreateSrc ( ) );
        }
    }


    private static AudioSource GetSrc ()
    {
        AudioSource res = null;
        for (int i = 0; i < Src.Count; i++)
        {
            if (!Src [i].isPlaying)
            {
                res = Src [i];
                break;
            }
        }
        if (res == null)
        {
            res = CreateSrc ( );
            Src.Add ( res );
        }
        return res;
    }


    private static AudioSource CreateSrc ()
    {
        return SrcParrent.AddComponent<AudioSource> ( );
    }

	// PASTER
    private static void StopStreams ()
    {
        for (int i = 0; i < Src.Count; i++)
        {
			if (Src[i] != null)			// check >>> != null
				Src [i].Stop ( );
        }
    }


    public static void ReleaseStreams ()
    {
        StopStreams ( );
        for (int i = 0; i < _tweens.Count; i++)
        {
            _tweens [i].Kill ( );
        }
        _tweens.Clear ( );
    }

    public static void CrossFadeMusic (string music, float fade = 0.5f, AudioSource Src = null)
    {
        AudioSource fade_src = Src == null ? MusicSrc : Src;

        AudioClip clip = LanguageAudio.GetSoundByName ( music );
        Sequence fader = DOTween.Sequence ( );
        _tweens.Add ( fader );
        //		fader.OnComplete (() => _tweens.Remove (fader));
        if (fade_src == null)
        {
            fade_src =
            MusicSrc = CreateSrc ( );
        }
        float vol = fade_src.volume;
        fader.Append ( fade_src.DOFade ( 0, fade ) );
        fader.AppendCallback ( () =>
        {
            fade_src.clip = clip;
            fade_src.Play ( );
        } );
        fader.Append ( fade_src.DOFade ( vol, fade ) );
        fader.AppendCallback ( () => _tweens.Remove ( fader ) );
    }

    static public void DoNotReleaseMusic ()
    {
        leave_music = true;
    }


    #endregion

    #region AudioSource management

    static public void Release ()
    {
        ReleaseStreams ( );
        ReleaseSources ( streams );
        if (!leave_music)
            ReleaseSources ( musics );
        leave_music = false;
    }


    static private void ReleaseSources (List<AudioSource> sources)
    {
        foreach (AudioSource source in sources)
        {
            if (source != null)
            {
                source.Stop ( );
            }
        }
        sources.Clear ( );
    }


    static public void EnableSounds (bool enable)
    {
        //Debug.Log("enable sounds");
        EnableSources ( streams, enable );
    }

    static public void EnableMusic (bool enable)
    {
        //Debug.Log("enable music");
        EnableSources ( musics, enable );
    }

    static public void SetSoundsVolume (float volume)
    {
        SetVolume ( streams, volume );
    }

    static public void SetMusicVolume (float volume)
    {
        SetVolume ( musics, volume );
    }


    static private void PauseSources (List<AudioSource> sources, bool pause, bool enabled)
    {
        foreach (AudioSource source in sources)
        {
            if (pause)
            {
                source.Pause ( );
            }
            else
            {
                if (enabled)
                {
                    source.Play ( );
                }
            }
        }
    }

    static public void PauseStreams (bool pause)
    {
        PauseSources ( streams, pause, GameSettings.IsSoundsEnabled ( ) );
    }

    static public void PauseMusics (bool pause)
    {
        PauseSources ( musics, pause, GameSettings.IsMusicEnabled ( ) );
    }

    static public void Pause (bool pause)
    {
        PauseStreams ( pause );
        PauseMusics ( pause );
    }

    static private void SetVolume (List<AudioSource> sources, float volume)
    {
        foreach (AudioSource source in sources)
        {
            source.volume = volume;
        }
    }


    static private void EnableSources (List<AudioSource> sources, bool param)
    {
        foreach (AudioSource source in sources)
        {
            source.mute = !param;
        }
    }

    static public void AddStream (AudioSource stream)
    {
        if (!ExistsStream ( stream ))
        {
            streams.Add ( stream );
        }
    }

    static private bool ExistsStream (AudioSource stream)
    {
        return streams.Contains ( stream );
    }

    static private void RemoveStream (AudioSource stream)
    {
        if (ExistsStream ( stream ))
        {
            streams.Remove ( stream );
        }
    }



    static public void PauseStream (AudioSource stream)
    {
        stream.Pause ( );
    }

    static public void StopStream (AudioSource stream)
    {
        stream.Stop ( );
        RemoveStream ( stream );
    }

    static private void AddMusic (AudioSource music)
    {
        if (!ExistsMusic ( music ))
        {
            musics.Add ( music );
        }
    }

    static private bool ExistsMusic (AudioSource music)
    {
        return musics.Contains ( music );
    }

    static private void RemoveMusic (AudioSource music)
    {
        if (ExistsMusic ( music ))
        {
            musics.Remove ( music );
        }
    }



    static public void PauseMusic (AudioSource music)
    {
        //		Debug.Log ("PauseMusic");
        music.Pause ( );
    }

    static public void StopMusic (AudioSource music)
    {
        //		Debug.Log ("StopMusic");
        music.Stop ( );
        RemoveMusic ( music );
    }

    #endregion

    #region Methods to play sound as AudioClip

    static public void PlayStream (AudioSource stream, AudioClip clip = null, bool loop = false)
    {
        if (clip != null)
        {
            stream.clip = clip;
        }
        stream.loop = loop;
        stream.mute = !GameSettings.IsSoundsEnabled ( );
        stream.volume = GameSettings.GetSoundsVol ( );
        stream.Play ( );
        AddStream ( stream );
    }


    static public void PlayMusic (AudioSource music, AudioClip clip = null, bool loop = true)
    {
        if (clip != null)
        {
            music.clip = clip;
        }
        music.mute = !GameSettings.IsMusicEnabled ( );
        music.volume = GameSettings.GetMusicVol ( );
        music.loop = loop;
        music.Play ( );
        AddMusic ( music );
    }


    static public void PlaySound (AudioClip clip, Vector3 point = default ( Vector3 ))
    {
        if (GameSettings.IsSoundsEnabled ( ))
        {
            AudioSource.PlayClipAtPoint ( clip, point, GameSettings.GetSoundsVol ( ) );
        }
    }

    #endregion

}
