﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Languages : MonoBehaviour
{
	private static int locale_index;
	private static string locale_name;
	private static Language locale_lang;

	public delegate void Callback(Language lang);
	public static event Callback OnLangChange;

	public enum Language
	{
		Arabic = SystemLanguage.Arabic,
		Chinese = SystemLanguage.Chinese,
		Czech = SystemLanguage.Czech,
		Danish = SystemLanguage.Danish,
		Dutch = SystemLanguage.Dutch,
		English = SystemLanguage.English,
		Finnish = SystemLanguage.Finnish,
		French = SystemLanguage.French,
		German = SystemLanguage.German,
		Greek = SystemLanguage.Greek,
		Italian = SystemLanguage.Italian,
		Japanese = SystemLanguage.Japanese,
		Norwegian = SystemLanguage.Norwegian,
		Polish = SystemLanguage.Polish,
		Portuguese = SystemLanguage.Portuguese,
		Romanian = SystemLanguage.Romanian,
		Russian = SystemLanguage.Russian,
		Spanish = SystemLanguage.Spanish,
		Swedish = SystemLanguage.Swedish,
		Turkish = SystemLanguage.Turkish,
		Hindi = 50,
		Hebrew = SystemLanguage.Hebrew,
		Indonesian = SystemLanguage.Indonesian,
		Korean = SystemLanguage.Korean,
		Thai = SystemLanguage.Thai,
		Ukrainian = SystemLanguage.Ukrainian,
	};

	//                                 ключ / значение
	public static readonly Dictionary<Language, string> languages = new Dictionary<Language, string>() {
		//{ Language.Arabic, "Arabic" },
		//{ Language.Chinese, "Chinese" },
		//{ Language.Czech, "Czech" },
		//{ Language.Danish, "Danish" },
		//{ Language.Dutch, "Dutch" },
		{ Language.English, "English" },
		//{ Language.Finnish, "Finnish" },
		//{ Language.French, "French" },
		//{ Language.German, "German" },
		//{ Language.Greek, "Greek" },
		//{ Language.Hebrew, "Hebrew" },
		//{ Language.Hindi, "Hindi" },
		//{ Language.Indonesian, "Indonesian" },
		{ Language.Italian, "Italian" },
		//{ Language.Japanese, "Japanese" },
		//{ Language.Korean, "Korean" },
		//{ Language.Norwegian, "Norwegian" },
		//{ Language.Polish, "Polish" },
		{ Language.Portuguese, "Portuguese" },
		//{ Language.Romanian, "Romanian" },
		{ Language.Russian, "Russian" },
		{ Language.Spanish, "Spanish" },
		//{ Language.Swedish, "Swedish" },
		//{ Language.Thai, "Thai" },
		//{ Language.Turkish, "Turkish" },
		//{ Language.Ukrainian, "Ukrainian" },
	};

	public static readonly string[] Localization = 
	{
		"Arabic",
		"Chinese",
		"Czech",
		"Danish",
		"Dutch",
		"English",
		"Finnish",
		"French",
		"German",
		"Greek",
		"Hebrew",
		"Hindi",
		"Indonesian",
		"Italian",
		"Japanese",
		"Korean",
		"Norwegian",
		"Polish",
		"Portuguese",
		"Romanian",
		"Russian",
		"Spanish",
		"Swedish",
		"Thai",
		"Turkish",
		"Ukrainian"
	};



	//----------------------------------------------------------------- Init 
	public static void Init()
	{
		LanguageAudio.Init();
		Language lang = Language.English;
		// если в плеер префере записано значение больше или равно "0" то  
		if (GameSettings.GetCurrentLang() >= 0)
		{
			lang = (Language)GameSettings.GetCurrentLang();  // выбераем записаное значение в плеер префере 
		}
		else
		{
			lang = DetectLanguage();  // если в плеер префере Значение -1 то проверяем в системе на системный язык 
		}
		SetLanguage(lang);
	}

	//----------------------------------------------------------------- DetectLanguage
	private static Language DetectLanguage()
	{
		Language lang = (Language)Application.systemLanguage;  // получаем системный язык 

		if (languages.ContainsKey(lang))   /// проверяем если он в библиотеке 
		{
			return lang;   // ели есть то возвращаем язык  без изменений 
		}
		else
		{
			// default   // если язык неизвестный возвращаем Английский 
			//return Language.English;
			return other_Lang();
		}
	}

	//---------------------------------------------------------------- Добавление Языка Имени 
	public static void SetLanguageByName(string lname)
	{
		foreach (KeyValuePair<Language, string> pair in languages)
		{
			if (pair.Value == lname)
			{
				SetLanguage(pair.Key);
				break;
			}
		}
	}

	//---------------------------------------------------------------- SetLanguage  
	public static void SetLanguage(Language lang)
	{
		string name = languages[lang];
		for (int i = 0; i < Localization.Length; i++)
		{
			if (Localization[i] == name)
			{
				locale_index = i;
				locale_name = Localization[i];
				locale_lang = lang;
				break;
			}
		}
		GameSettings.SetCurrentLang((int)locale_lang);
		LanguageAudio.LoadSounds(true, false);
		if (OnLangChange != null)
		{
			OnLangChange(locale_lang);
		}
	}

	//--------------------------------------------------------------- NextLanguage
	public static void NextLanguage()
	{
		locale_index++;
		if (locale_index > Localization.Length - 1)
		{
			locale_index = 0;
		}
		locale_name = Localization[locale_index];
	}

	//--------------------------------------------------------------- GetLanguageName
	public static string GetLanguageName()
	{
		return locale_name;
	}

	//--------------------------------------------------------------- GetLanguage
	public static Language GetLanguage()
	{
		return locale_lang;
	}

	//--------------------------------------------------------------- Насторойки перевода 
	private static Language other_Lang()
	{
		Language lang_local1 = Language.English;

		if (Application.systemLanguage == SystemLanguage.Belarusian ||
			Application.systemLanguage == SystemLanguage.Ukrainian)
		{
			lang_local1 = Language.Russian;
		}

		if (languages.ContainsKey(Language.Spanish))
		{
			if (Application.systemLanguage == SystemLanguage.Catalan)
			{
				lang_local1 = Language.Spanish;
			}
		}

#if UNITY_ANDROID  // извлекаем языки которых нету в Application.systemLanguage
		using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
		{
			string loc_lang = null;

			using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
			{
				loc_lang = locale.Call<string>("getDisplayLanguage");
				Debug.Log("current lang = " + locale.Call<string>("getDisplayLanguage"));

				if (loc_lang == "Kazakh" || 
					loc_lang == "қазақ тілі" || 
					loc_lang == "Uzbek")
				{
					lang_local1 = Language.Russian;
				}
			}
		}
#endif
		return lang_local1;
	}



}
