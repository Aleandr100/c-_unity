﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using DG.Tweening;


/* ChangeLog:
 * Updated Spine package (http://ru.esotericsoftware.com/forum/Noteworthy-Spine-Unity-Topics-5924) (use Spine.Unity and Spine.Unity.Modules  (http://ru.esotericsoftware.com/forum/Spine-Unity-namespaces-6025))
 * New GoogleAdsSDK (fixed bugs)
 * Scenes are loaded by string. Its not necessary now to keep same order of scenes in enum
 * Banners wont be repositioned untill splashes will end
 * Added extra check before banner reposition to avoid its reload call (ManagerGoogle)
 * Added push scene to no-ads list
 * Added platform directives to support different platforms
 * Removed Transition kit with shaders
 * Pannel -> Panel
 */

public class SceneLoader :MonoBehaviour
{
    public static SceneLoader Instance;

    public enum Scenes  //List here all scene names included in the build   (permanent scenes are InitScene, PSV_Splash (or other), Push)
    {
        InitScene,
        PSV_Splash,
        //Melnitsa_Splash,
        Push,
        MainMenu,
		Intro,
        Game
    }

    public enum TransitionMethod
    {
        Default,    //do not select this (service item)
        Tween,      //using animathion of Canvas Image
        None,       //using siple SceneManager.LoadScene()
    }

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1

    private AdmobAd.AdLayout
        small_banner_default_pos = AdmobAd.AdLayout.Bottom_Centered;        //overrides AdmobAdPosition

    private Dictionary<Scenes, AdmobAd.AdLayout>
        small_banner_override = new Dictionary<Scenes, AdmobAd.AdLayout> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
        {
            //{ Scenes.MainMenu, AdPosition.BottomRight },
        };

    private List<Scenes>
        not_allowed_small_banner = new List<Scenes> ( ) //list here scenes that shouldn't show ad 
        {
            Scenes.Push,
            Scenes.MainMenu,
        };

    private List<Scenes>
        not_allowed_interstitial = new List<Scenes> ( ) //list here scenes which wouldn't show big banner if we will leave them
        {
            Scenes.InitScene,
            Scenes.PSV_Splash,
            Scenes.Push,
        };

#endif

    public bool
        splash_transition = true;   //uncheck this to leave all scenes in Hierarchy and keep all necessary Instances created without switching to splashes

    //private float
    //    inter_called_time,      //service used for monitoring InterClosed event
    //    wait_for_inter_closed_time = 10f;   //threshold for while loop to avoid bugs (loop will wait till event comes or this time have passed)

    private bool
        //inter_shown = false,        //determines if we have to wait for interClose event
        in_transition = false;      //determines if we can start another transitrion (switching between few scenes in a row not allowed)

    public List<Scenes>
        splash_scenes = new List<Scenes> ( );   //list here all scenes that should be shown at the start of the game with some short lifetime

    public Scenes
        first_scene = Scenes.MainMenu,       //scene to load after splashes (usually MainMenu)
        push_scene = Scenes.Push;        //scene where push service is set

    public float
        splash_duration = 2f,       //splash screen lifetime
        transition_duration = 1f;   //transition animation duration

    public TransitionMethod
        transition_method;          //how to switch between scenes

    public GameObject
        loading_screen,             //prefab of Canvas and text item with "Loading" label which will appare befor Interstitial will be shown
        fadeScreen;                 //link to object with Image with black texture which will fade over screen

    private UnityEngine.UI.Image
        fade_image;                 //Image component of fade_screen


    private enum AnalyticsEvents    //used to log events from one place
    {
        StartApplication,
        CloseApplication,
        LogScreen,
    }


    void Awake ()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad ( gameObject );
            fade_image = fadeScreen.GetComponent<UnityEngine.UI.Image> ( );

            if (transition_method == TransitionMethod.Default)  //fixing if method haven't been selected properly
            {
                transition_method = TransitionMethod.Tween;
            }
        }
    }

    void OnEnable ()
    {
        ManagerGoogle.OnInterstitialShown += ShowLoadingScreen;
        ManagerGoogle.OnInterstitialClosed += OnInterClosed;
    }


    void OnDisable ()
    {
        ManagerGoogle.OnInterstitialShown -= ShowLoadingScreen;
        ManagerGoogle.OnInterstitialClosed -= OnInterClosed;
    }


    void LogEvent (AnalyticsEvents _event, string _message = "")
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        switch (_event)
        {
            case AnalyticsEvents.CloseApplication:
            case AnalyticsEvents.StartApplication:
                GoogleAnalytics.Instance.LogEvent ( _event.ToString ( ), _event.ToString ( ) );
                break;
            case AnalyticsEvents.LogScreen:
                GoogleAnalytics.Instance.LogScreen ( _message );
                break;
        }
#endif
    }




    void Start ()
    {
#if !UNITY_EDITOR
        //if you forget to put this trigger true before build
        splash_transition = true;
#endif
        if (splash_transition)
        {
            StartCoroutine ( SplashSequence ( ) );
        }

        LogEvent ( AnalyticsEvents.StartApplication );
        //GoogleAnalytics.Instance.LogEvent ( "StartApplication", "StartApplication" );
    }


    #region ADS
    private void ShowBigBanner ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        if (!not_allowed_interstitial.Contains ( SceneManager.GetActiveScene ( ).name.ToEnum<Scenes> ( ) ))
        {
            //inter_called_time = Time.time + wait_for_inter_closed_time;
            //inter_shown =
                ManagerGoogle.Instance.ShowFullscreenBanner ( );
        }
#endif
    }

    private void RepositionBanner ()
    {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        Scenes scene = SceneManager.GetActiveScene ( ).name.ToEnum<Scenes> ( );
        if (!splash_scenes.Contains ( scene ))
        {
            bool show_banner = !not_allowed_small_banner.Contains ( scene );

            if (show_banner)
            {
                AdmobAd.AdLayout target_pos = small_banner_default_pos;
                if (small_banner_override.ContainsKey ( scene ))
                {
                    target_pos = small_banner_override [scene];
                }
                ManagerGoogle.Instance.RepositionSmallBanner ( target_pos );
                ManagerGoogle.Instance.ShowSmallBanner ( );
            }
            else
            {
                ManagerGoogle.Instance.HideSmallBanner ( );
            }
        }
#endif
    }

    #endregion

    void CompleteTransition ()
    {
        //Debug.Log ( "CompleteTransition" );
        in_transition = false;
        RepositionBanner ( );
    }


    public void ShowLoadingScreen ()
    {
        Instantiate ( loading_screen );
    }


    private void onScreenObscured ()    //occurs when screen is frosen (hidden)
    {
        //Debug.Log ( "OnScreenObscured" );
        AudioController.ReleaseStreams ( );
        ShowBigBanner ( );
    }

    void OnInterClosed ()               //occurs when user closes interstitial
    {
        //inter_shown = false;
    }

    private void LoadScene (string scene_to_load)   //generic way of loading scene without animation
    {
        onScreenObscured ( );
        SceneManager.LoadScene ( scene_to_load );
        CompleteTransition ( );
    }

    public void OnApplicationQuit ()
    {
        PlayerPrefs.Save ( );
        LogEvent ( AnalyticsEvents.CloseApplication );
        //GoogleAnalytics.Instance.LogEvent ( "CloseApplication", "CloseApplication" );
    }


    IEnumerator SplashSequence ()   //coroutine for showing splash scenes with certain delay
    {
        for (int i = 0; i < splash_scenes.Count; i++)
        {
            SwitchToScene ( splash_scenes [i] );
            yield return new WaitForSeconds ( splash_duration );
        }
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
        SwitchToScene ( PsvPushService.Instance.AreNewsAvailable ( ) ? push_scene : first_scene );
#else
        SwitchToScene ( first_scene );
#endif
    }



    public bool IsLoadingLevel ()
    {
        return in_transition;
    }


    public void SwitchToScene (Scenes target_scene, TransitionMethod method = TransitionMethod.Default, float override_duration = -1)
    {
        if (!IsLoadingLevel ( ))
        {
            in_transition = true;

            if (method == TransitionMethod.Default)
                method = transition_method;


            float duration = override_duration < 0 ? transition_duration : override_duration;

            LogEvent ( AnalyticsEvents.LogScreen, target_scene.ToString ( ) );
            //GoogleAnalytics.Instance.LogScreen ( target_scene.ToString ( ) );


            string target = target_scene.ToString ( );
            switch (method)
            {
                case TransitionMethod.None:
                    {
                        LoadScene ( target );
                        break;
                    }
                case TransitionMethod.Tween:
                    {
                        StartTransition ( target, fade_image, duration );
                        break;
                    }
            }
        }
    }


    #region TweenTransition
    public static void SceneTransition (bool open, TweenCallback callback, UnityEngine.UI.Image fade_image, float fadeTime)
    {
        if (open)
        {
            TweenCallback fadeCallback = delegate
            {
                fade_image.gameObject.SetActive ( false );
                if (callback != null)
                {
                    callback ( );
                }
            };
            fade_image.DOFade ( 0, fadeTime ).OnComplete ( fadeCallback );
        }
        else
        {
            fade_image.gameObject.SetActive ( true );
            fade_image.DOFade ( 1, fadeTime ).OnComplete ( callback ).SetUpdate ( true );
        }
    }


    public void StartTransition (string scene_to_load, UnityEngine.UI.Image fade_image, float transition_duration)
    {
        if (fade_image != null)
        {
            TweenCallback StartLayout = delegate
            {
                SceneManager.LoadScene ( scene_to_load );
                StartCoroutine ( WaitForTransitionComplete ( scene_to_load, transition_duration, fade_image ) );
            };

            SceneTransition ( false, delegate
            {
                onScreenObscured ( );
                StartLayout ( );
            }, fade_image, transition_duration );
        }
        else
        {
            Debug.Log ( "Fade image null" );
        }
    }


    IEnumerator WaitForTransitionComplete (string level, float transition_duration, UnityEngine.UI.Image fade_image)
    {

        while (SceneManager.GetActiveScene ( ).name != level)
            yield return null;
        SceneTransition ( true, delegate
        {
            CompleteTransition ( );
        }, fade_image, transition_duration );
    }

    #endregion
}
