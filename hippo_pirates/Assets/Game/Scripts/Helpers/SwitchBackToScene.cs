﻿using UnityEngine;
using System.Collections;

public class SwitchBackToScene : MonoBehaviour
{


	public SceneLoader.Scenes 
		target_scene;



	void OnEnable()
	{
		KeyListener.OnKeyPressed += ProcessKey;
	}

	void OnDisable()
	{
		KeyListener.OnKeyPressed -= ProcessKey;
	}


	public void ProcessKey(KeyCode key)
	{
		if (key == KeyCode.Escape)
			{
				Action();
			}
	}

	public void Action()
	{
		SceneLoader.Instance.SwitchToScene(target_scene);
	}
}
