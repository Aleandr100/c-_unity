﻿using UnityEngine;
using System.Collections;

public class PNButton : MonoBehaviour {
	//protected bool focus = false;

	//public bool IsFocused () {
	//	return focus;
	//}

	//public enum Button
	//{
	//	News,
	//	Action,
	//	Back,
	//	Forward,
	//	Exit,
	//}

	//public Button type;
	//public Sprite[] sprites;
	//public bool radio = false;
	//public bool is_enabled = true;

	//private SpriteRenderer button_renderer;
	//private Collider2D button_collider;
	
	//void Awake ()
	//{
	//	button_renderer = gameObject.GetComponent<SpriteRenderer> ();
	//	button_collider = gameObject.GetComponent<Collider2D> ();
	//}
	
	//void Start ()
	//{
	//	Init ();
	//}

	//public void Init ()
	//{
	//	ButtonUp ();
	//}

	//public void NewsBtn () {
	//	PsvPushService.Instance.PSShowNews (true);
	//}

    public void NewsBtn ()
    {
        if (PsvPushService.Instance.PSShowNews ( true ))
        {
            SceneLoader.Instance.SwitchToScene ( SceneLoader.Scenes.Push );
        }
    }


    public void ActionBtn () {
        Debug.Log ( "ActionBtn" );
		PsvPushService.Instance.PSRunAction ();
	}

	//public void BackBtn () {
	//}

	//public void ForwardBtn () {
	//}

	//public void ExitBtn () {
	//	PsvPushService.Instance.PSShowNews (false);
	//}

	//public void OnButtonPress ()
	//{
	//	switch (type) {
	//	case Button.News:
	//	{
	//		NewsBtn ();
	//		break;
	//	}
	//	case Button.Action:
	//	{
	//		ActionBtn ();
	//		break;
	//	}
	//	case Button.Back:
	//	{
	//		BackBtn ();
	//		break;
	//	}
	//	case Button.Forward:
	//	{
	//		ForwardBtn ();
	//		break;
	//	}
	//	case Button.Exit:
	//	{
	//		ExitBtn ();
	//		break;
	//	}
	//	}
	//}
	
	//public void ButtonDown ()
	//{
	//	if (radio) {
	//		button_renderer.sprite = is_enabled ? sprites [1] : sprites [3];
	//	} else {
	//		button_renderer.sprite = sprites [1];
	//	}
	//}
	
	//public void ButtonUp ()
	//{
	//	if (radio) {
	//		button_renderer.sprite = is_enabled ? sprites [0] : sprites [2];
	//	} else {
	//		button_renderer.sprite = sprites [0];
	//	}
	//}
	
	//virtual public bool OnTouchDown (Vector2 point)
	//{
	//	if (button_collider.OverlapPoint (point)) {
	//		focus = true;
	//		ButtonDown ();
	//		return true;
	//	} else {
	//		return false;
	//	}
	//}
	
	//virtual public bool OnTouchUp (Vector2 point)
	//{
	//	if (focus) {
	//		focus = false;
	//		ButtonUp ();
	//		if (button_collider.OverlapPoint (point)) {
	//			OnButtonPress ();
	//		}
	//	}
	//	return true;
	//}
	
	//virtual public bool OnTouchMove (Vector2 point)
	//{
	//	if (focus) {
	//		if (button_collider.OverlapPoint (point)) {
	//			ButtonDown ();
	//		} else {
	//			ButtonUp ();
	//		}
	//	}
	//	return true;
	//}
	
	//virtual public bool OnTouchExit (Vector2 point)
	//{
	//	focus = false;
	//	ButtonUp ();
	//	return true;
	//}

	//private bool CheckPos(Vector3 pos){
	//	bool focused = false;
	//	if ( (pos.x > (gameObject.transform.position.x-(gameObject.GetComponent<Collider2D>().bounds.size.x/2))) && (pos.x < (gameObject.transform.position.x+(gameObject.GetComponent<Collider2D>().bounds.size.x/2))) ){
	//		if ( (pos.x > (gameObject.transform.position.x-(gameObject.GetComponent<Collider2D>().bounds.size.x/2))) && (pos.x < (gameObject.transform.position.x+(gameObject.GetComponent<Collider2D>().bounds.size.x/2))) ){
	//			focused = true;
	//		}
	//	}
	//	return focused;
	//}

	//public void Update(){
	//	Vector3 currpos = PNGetScreen.ScreenToWorldPoint(Input.mousePosition);
	//	if (Input.GetMouseButtonDown (0)) {
	//		if (CheckPos(currpos)){
	//			OnTouchDown (new Vector2(currpos.x, currpos.y));
	//		} else {
	//			OnTouchExit (new Vector2(currpos.x, currpos.y));
	//		}
	//	} else if (Input.GetMouseButtonUp (0)) {
	//		if (CheckPos(currpos)){
	//			OnTouchUp (new Vector2(currpos.x, currpos.y));
	//		} else {
	//			OnTouchExit (new Vector2(currpos.x, currpos.y));
	//		}
	//	} else if (Input.GetMouseButton (0)) {
	//		if (CheckPos(currpos)){
	//			OnTouchMove (new Vector2(currpos.x, currpos.y));
	//		} else {
	//			OnTouchExit (new Vector2(currpos.x, currpos.y));
	//		}
	//	}
	//}

}
