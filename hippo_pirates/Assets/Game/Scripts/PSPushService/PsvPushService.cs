using UnityEngine;
using System.Collections;

public class PsvPushService :MonoBehaviour
{
    public static PsvPushService Instance;

    public GameObject
        PNMainScene,
        PNNewsScene,
        PNNewsBtn;

    public string
        psisnews = "",
        pstitle = "",
        psimgurl = "",
        psdescr = "",
        psflag = "",
        pstargeturl = "";

    private bool NewsOpened = false;


    public float PushNewsScale = 1f;


    /***modified***/

    private bool show_news = false;


    public bool AreNewsAvailable ()
    {
        return show_news;
    }

    bool SceneSystem ()
    {
        return PNMainScene == null && PNNewsScene == null;
    }    

    void Awake ()
    {
        Instance = this;
        if (SceneSystem ( ))
        {
            PSInit ( "icon" );
        }
    }

    public bool PSShowNews (bool setscene)
    {
        if (!(psisnews.Equals ( "-1" )))
        {
            if (SceneSystem() && setscene)
            {
                show_news = true;
            }
            else
            {
                PNMainScene.SetActive ( !setscene );
                PNNewsBtn.SetActive ( !setscene );
                if (setscene)
                {
                    PNNewsScript.Instantiate ( PNNewsScene );
                    NewsOpen ( setscene );
                }
                else
                {
                    NewsOpen ( setscene );
                    PNNewsScript.Destroy ( PNNewsScript.Instance.Self );
                }
            }
        }
        return show_news;
    }
    /****modified****/




    public bool isNewsOpened ()
    {
        return NewsOpened;
    }

    public void NewsOpen (bool isopen)
    {
        NewsOpened = isopen;
    }



    public void PSStart ()
    {
        string [] psstr = PSGetNews ( );
        psisnews = psstr [0];
        pstitle = psstr [1];
        psimgurl = psstr [2];
        psdescr = psstr [3];
        psflag = psstr [4];
        pstargeturl = "market://details?id=" + psstr [5];
        if (psimgurl.Length > 0)
        {
            try
            {
                if (!(psimgurl.Substring ( 0, 7 ).Equals ( "http://" ) || psimgurl.Substring ( 0, 8 ).Equals ( "https://" )))
                {
                    psimgurl = "http://" + psimgurl;
                }
            }
            catch (UnityException error)
            {
                print ( "UnityException:" + error );
            }
        }
        if (psisnews.Equals ( "1" ))
        {
            PSShowNews ( true );
            PSSetIsNews ( );
        }
    }

    public string PSInit (string s_icon, string blank_title = "New Message!", string uid = "")
    {
        //s_icon - ����� ������ ��� ���������, blank_title - ������� ��� ������������ ��� �������� ������� uid �� ������� ���� ����� 30 �������
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
		string myuid = toast.CallStatic<string>("PSStart", app, s_icon, blank_title, uid);
		PSStart ();
		return myuid;
#endif
        PSStart ( );
        return "";
    }

    public string [] PSGetNews ()
    {
        //s_icon - ����� ������ ��� ���������, blank_title - ������� ��� ������������ ��� �������� ������� uid �� ������� ���� ����� 30 �������
        string [] str = new string [6] { "", "", "", "", "", "" };
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
		str = toast.CallStatic<string[]>("GetPSNews", app);
		return str;
#endif
        return str;
    }

    public int PSTimeZone ()
    {   //+���������� ��������� �������� � �����(� ��� +3, � �.�.)
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
		int timeoffset = toast.CallStatic<int>("gcmtimezone", app);
		return timeoffset;
#endif
        return 0;
    }

    public string PSCurrentDateTime ()
    {   //+���������� ������� ����� � ������� 2014-10-19 15:20:48
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
		string currentdate = toast.CallStatic<string>("currentdatetime", app);
		return currentdate;
#endif
        return "";
    }

    public string PSRandomString (int UIDlength, bool useSpecial, bool useUpper)
    {   //���������� ��������� ������ � ��������� ����������� ��������
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
		string randomstr = toast.CallStatic<string>("RandomString", UIDlength, useSpecial, useUpper);
		return randomstr;
#endif
        return "";
    }

    public void PSShowNotif (long delay, string title, string text, bool sound = false, bool vibrate = true, string small_icon_name = "", string big_icon_name = "")
    {
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSReceiver");
		toast.CallStatic("SetNotification", app, delay * 1000L, title, text, "", sound ? 1 : 0, vibrate ? 1 : 0, 0, small_icon_name, big_icon_name, 11167436);
#endif
    }

    public void PSNotifRepeating (long delay, long timeout, string title, string text, bool sound = false, bool vibrate = true, string small_icon_name = "", string big_icon_name = "")
    {
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSReceiver");
		toast.CallStatic("SetRepeatingNotification", app, delay * 1000L, title, text, "", timeout * 1000L, sound ? 1 : 0, vibrate ? 1 : 0, 0, small_icon_name, big_icon_name, 11167436);
#endif
    }

    public void PSCancelNotif ()
    {
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSReceiver");
		toast.CallStatic("CancelNotification", app);
#endif
    }

    public void PSRunAction ()
    {
        if (psflag.Equals ( "1" ))
        {
            Application.OpenURL ( pstargeturl );
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			toast.CallStatic("PSSendClick", app, pstargeturl);
#endif
        }
    }

    public void PSSetIsNews ()
    {
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
		AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
		toast.CallStatic("PSSetIsNews", app);
#endif
    }

}