﻿using UnityEngine;
using System.Collections;

public class PN_CanvasCompatibility :MonoBehaviour
{


    void OnEnable ()
    {
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WP8 && !UNITY_WP8_1
        this.gameObject.SetActive ( false );
#endif
    }


    public void NewsBtn ()
    {
        if (PsvPushService.Instance.PSShowNews ( true ))
        {
            SceneLoader.Instance.SwitchToScene ( SceneLoader.Scenes.Push );
        }
    }


    public void ActionBtn ()
    {
        Debug.Log ( "ActionBtn" );
        PsvPushService.Instance.PSRunAction ( );
    }
}
