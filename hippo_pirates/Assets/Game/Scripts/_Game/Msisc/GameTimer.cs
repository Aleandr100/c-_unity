﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent();
	public static event MyEvent OnTimerIsRunOut;

	// FIELDS
	[SerializeField] private GameObject _timerObj;
	[SerializeField] private Image _timerDialGreen;
	[SerializeField] private AudioSource _audioSrcTicTac;

	private readonly float _timerStart = 120f;		// 120 s  
	private static float _timer;
	private readonly float _alarmClockSec = 10f;
	private readonly float _warningTicTac = 20f;

	private bool _isTicTac;
	private bool _isTimerRunOut;



	// UNIYU
	private void OnEnable()
	{
		ScoreManager.OnAddSecToTimer += AddSecondsToTimer;
	}

	private void OnDisable()
	{
		ScoreManager.OnAddSecToTimer -= AddSecondsToTimer;
	}

	private void Awake()
	{
		AudioController.AddStream(_audioSrcTicTac);
	}

	private void Start()
	{
		_timer = _timerStart;
	}

	private void Update()
	{
		TimerDecrease();
	}



	// METHODS
	private void TimerDecrease()
	{
		_timer -= Time.deltaTime;
		_timerDialGreen.fillAmount = _timer/_timerStart;

		if (_timer > _warningTicTac)
		{
			if (_isTicTac)
				StopTicTac();
		}

		else if (_timer < _warningTicTac && _timer > 0)
		{
			if (!_isTicTac)
				StartTicTac();
		}

		else if (_timer < 0)
		{
			if (!_isTimerRunOut)
			{
				StopTicTac();

				// event!
				SendEvent_TimerIsRunOut();

				_isTimerRunOut = true;
				enabled = false;
			}
		}
	}

	private void RescaleTimerImage()
	{
		_timerObj.transform.DOScale(new Vector2(0.4f, 0.4f), 0.5f);
	}

	private void StartTicTac()
	{
		_isTicTac = true;

		_audioSrcTicTac.Play();
	}

	private void StopTicTac()
	{
		_isTicTac = false;
		_audioSrcTicTac.Stop();
	}



	// INTERFACES
	public void AddSecondsToTimer()
	{
		_timerObj.transform.DOScale(new Vector2(0.6f, 0.6f), 0.5f).OnComplete(RescaleTimerImage);
		_timer += _alarmClockSec;
	}
	


	// SENDS EVENTS
	private void SendEvent_TimerIsRunOut()
	{
		if (OnTimerIsRunOut != null)
			OnTimerIsRunOut();
	}



}
