﻿using UnityEngine;
using System.Collections;

public class Hook : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent(GameObject coll);			
	public static event MyEvent OnCatchItem;

	// PROPERTIES
	public bool IsCatchItem
	{
		get { return _isCatchItem; }
		set { _isCatchItem = value; }
	}

	public GameObject CatchedItem
	{
		get { return _catchedItem; }
		set { _catchedItem = value; }
	}

	// FIELDS
	[SerializeField] private Transform _forItems;

	private GameObject _catchedItem;
	private bool _isCatchItem;
	private readonly string _srCatchedItem = "CatchedItem";
	


	// UNITY
	private void Start ()
	{
	
	}

	private void OnTriggerEnter2D(Collider2D coll)
	{
		if (!IsCatchItem)
		{
			CatchItem(coll.gameObject);
		}
	}



	// METHODS
	private void CatchItem(GameObject coll)
	{
		IsCatchItem = true;
		CheckItemType(coll);
		
		// event!
		SendEvent_CatchItem(_catchedItem);
	}

	private void CheckItemType(GameObject coll)
	{
		if (coll.CompareTag(Tags.Treasure))
		{
			_catchedItem = coll.transform.parent.parent.gameObject;
			coll.transform.parent.parent.SetParent(_forItems);
		}
		else if (coll.CompareTag(Tags.Trash))
		{
			_catchedItem = coll.transform.parent.parent.parent.gameObject;
			coll.transform.parent.parent.parent.SetParent(_forItems);
		}
		else if (coll.CompareTag(Tags.AlarmClock))
		{
			_catchedItem = coll.transform.parent.gameObject;
			coll.transform.parent.SetParent(_forItems);
		}
		else if (coll.CompareTag(Tags.Fish))
		{
			_catchedItem = coll.transform.parent.gameObject;
			coll.transform.parent.SetParent(_forItems);
		}

		ChangeSpriteTag(_catchedItem);
		StopFishMove(_catchedItem);
		DisableObjectShadow(_catchedItem);
	}

	private void ChangeSpriteTag(GameObject coll)
	{
		// for all NOT fish items 
		if (!coll.CompareTag(Tags.Fish))
		{
			var collSR = coll.GetComponentInChildren<SpriteRenderer>();
			if (collSR != null)
				collSR.sortingLayerName = _srCatchedItem;
			else
				Error.Show("there is no SpriteRenderer = " + coll.name);
		}
	}

	private void StopFishMove(GameObject coll)
	{
		if (coll.CompareTag(Tags.Fish))
		{
			var collFishMove = coll.GetComponent<FishMove>();
			if (collFishMove != null && collFishMove.isActiveAndEnabled)
				collFishMove._currState = FishMove.States.STOP;
		}
	}

	private void DisableObjectShadow(GameObject coll)
	{
		Transform collShadow = coll.transform.Find("Shadow");
		if (collShadow != null)
			collShadow.gameObject.SetActive(false);
		else
			Error.Show("there is no Shadow sub-object");
	}



	// EVENTS
	private void SendEvent_CatchItem(GameObject go)
	{
		if (OnCatchItem != null)
			OnCatchItem(go); 
	}



}
