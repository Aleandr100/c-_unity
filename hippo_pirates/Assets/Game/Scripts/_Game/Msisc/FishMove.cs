﻿using UnityEngine;
using System.Collections;

public class FishMove : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent(GameObject go);
	public static event MyEvent OnNeedToHideFish;

	// PROPERTIES
	public Vector2 EndPos
	{
		get { return _endPos; }
		set { _endPos = value; }
	}

	// FIELDS
	[SerializeField] private float _minPosY;			// 0f gameplay
	[SerializeField] private float _maxPosY;			// 0.5f gameplay

	private Transform _transf;
	private Vector2 _startPos;
	private Vector2 _endPos;
	
	private float _speed;
	private readonly float _speedMin = 1.2f;
	private readonly float _speedMax = 1.4f;

	private Transform _shadow;
	private readonly float _minShadowPosY = -0.2f;		 // -0.2f gameplay

	private bool _isCastShadow;

	public enum States
	{
		NONE, 
		START_POS,
		STOP
	}
	public States _currState = States.NONE;



	// UNITY
	private void Start ()
	{
		_transf = transform;
		_startPos = _transf.position;

		_shadow = transform.Find("Shadow");
		if (_shadow == null)
			Error.Show("there is no Shadow sub-object");

		//
		ChangeSpeed();
	}

	private void Update ()
	{
		Movement();
		CheckPosForShadow();
	}



	// METHODS
	private void Movement()
	{
		if (_currState != States.STOP)
			_transf.localPosition = Vector2.MoveTowards(_transf.localPosition, EndPos, Time.deltaTime * _speed);

		if ((Vector2) _transf.localPosition == EndPos)
		{
			if (_currState == States.NONE)
			{
				_currState = States.START_POS;
				EndPos = _startPos;
				ResetFishParameters();
			}

			else if (_currState == States.START_POS)
			{
				_currState = States.NONE;
				
				// event!
				SendEvent_NeedToHideFish(gameObject);
			}
		}
	}

	private void CheckPosForShadow()
	{
		if (_transf.position.y > _minShadowPosY)
		{
			if (_isCastShadow)
			{
				_isCastShadow = false;
				_shadow.gameObject.SetActive(false);
			}	
		}

		else
		{
			if (!_isCastShadow)
			{
				_isCastShadow = true;
				_shadow.gameObject.SetActive(true);
			}
		}
	}

	private void ResetFishParameters()
	{
		ChangeSpeed();
		ChangeHeight();
		ChangeDirection();
	}

	private void ChangeSpeed()
	{
		_speed = Random.Range(_speedMin, _speedMax);
	}

	private void ChangeHeight()
	{
		float myPosY = Random.Range(_minPosY, _maxPosY);
		_transf.localPosition = new Vector2(_transf.localPosition.x, myPosY);
	}

	private void ChangeDirection()
	{
		float myScaleX = _transf.localScale.x;
		_transf.localScale = new Vector2(-myScaleX, _transf.localScale.y);

		// check fish shadow pos
		if (_transf.localScale.x < 0)
			_shadow.localPosition = new Vector2(-_shadow.localPosition.x, _shadow.localPosition.y);
	}



	// SEND EVENTS
	private void SendEvent_NeedToHideFish(GameObject go)
	{
		if (OnNeedToHideFish != null)
			OnNeedToHideFish(go);
	}



}
