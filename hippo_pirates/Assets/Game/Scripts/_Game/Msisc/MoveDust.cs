﻿using UnityEngine;
using System.Collections;

public class MoveDust : MonoBehaviour
{
	// FIELDS
	private Transform _transform;
	
	private readonly float _leftPosX = -5f;
	private readonly float _rightPosX = 10f;
	private readonly float _deltaShift = 0.1f;

	private readonly float _speed = 0.4f;



	// UNITY
	private void Awake()
	{
		_transform = transform;
	}

	private void Start()
	{
	
	}
	
	private void Update()
	{
		Movement();
	}



	// METHODS
	private void Movement()
	{
		if (_transform.localScale.z > 0)
		{
			_transform.Translate(Vector2.right * Time.deltaTime * _speed);

			if (_transform.localPosition.x > _rightPosX)
			{
				_transform.localPosition = new Vector2(_transform.localPosition.x - _deltaShift, _transform.localPosition.y);
				FlipObjectZ(_transform);
			}
		}
			
		else
		{
			_transform.Translate(Vector2.left * Time.deltaTime * _speed);

			if (_transform.localPosition.x < _leftPosX)
			{
				_transform.localPosition = new Vector2(_transform.localPosition.x + _deltaShift, _transform.localPosition.y);
				FlipObjectZ(_transform);
			}
		}
	}

	private void FlipObjectZ(Transform myTransf)
	{
		// coordinate Z stores direction (1 = to right; -1 = to left)
		float myScaleZ = myTransf.localScale.z;
		myScaleZ *= -1f;

		myTransf.localScale = new Vector3(myTransf.localScale.x, myTransf.localScale.y, myScaleZ);
	}



}
