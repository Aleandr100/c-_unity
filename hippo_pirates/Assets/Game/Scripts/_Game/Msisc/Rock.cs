﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour
{
	// FIELDS
	[SerializeField] private GameObject _parentBottom;

	private Transform _parentBottomTransf;
	private readonly float _leftPosX = -8f;

	private bool _isHide;
	

	
	// UNITY
	void Start ()
	{
		_parentBottomTransf = _parentBottom.transform;
	}

	void Update()
	{
		if (!_isHide)
		{
			if (_parentBottomTransf.localPosition.x < _leftPosX)
			{
				_isHide = true;
				gameObject.SetActive(false);
			}
		}
	}



}