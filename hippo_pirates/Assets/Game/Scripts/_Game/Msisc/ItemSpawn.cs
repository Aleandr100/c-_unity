﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemSpawn : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent(Transform parentPoint, Vector2 pos);
	public static event MyEvent OnNeedToShowTreasure;
	public static event MyEvent OnNeedToShowTrash;
	public static event MyEvent OnNeedToShowAlarmClock;

	// FIELDS
	[SerializeField] private Transform _forItems;
	[SerializeField] private Transform _trashPoolObjects;
	[SerializeField] private Transform[] _points;

	private readonly List<int> _tempCountPoints = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	private List<int> _countPoints = new List<int>();

	

	// UNITY
	private void Start()
	{
		ResetItems();
	}

	

	// METHODS
	public void ResetItems()
	{
		int itemsCount = _forItems.childCount;
		for (int i = 0; i < itemsCount; i++)
		{
			_forItems.GetChild(i).gameObject.SetActive(false);
		}

		//
		if (_countPoints.Count > 0)
			DestroyList(_countPoints);

		
		ShuffleArray(_tempCountPoints);
		FillListFromList(_countPoints, _tempCountPoints);
		FillPoints(_countPoints);
	}

	private void ShuffleArray(List<int> someList)
    {
        for (int i = 0; i < someList.Count; i++)
        {
            int tmp = someList[i];
			int r = Random.Range(i, someList.Count);
            someList[i] = someList[r];
            someList[r] = tmp;
        }
	}

	private void FillListFromList(List<int> toList, List<int> fromList)
	{
		for (int i = 0; i < fromList.Count; i++)
		{
			toList.Add(fromList[i]);
		}
	}

	private void FillPoints(List<int> someList)
	{
		int[] tempVariants = RandomManager.Instance.GetVariants();
		
		for (int i = 0; i < someList.Count; i++)
		{
			Vector2 pos = _points[someList[i]].localPosition;
			ShowItem(tempVariants[i], pos);
		}
	}

	private void ShowItem(int myType, Vector2 pos)
	{
		switch (myType)
		{
			case 5:
				// event!
				SendEvent_NeedToShowTreasure(_forItems, pos);
				break;
		
			case 1:
				// event!
				SendEvent_NeedToShowTrash(_forItems, pos); 
				break;
			
			case 9:
				// event!
				SendEvent_NeedToShowAlarmClock(_forItems, pos); 
				break;

			case 0:
				// --- none ---
				break;

			default:
				Error.Show("ItemType is incorrect! = " + myType);
				break;
		}
	}

	private void DestroyList(List<int> someList)
	{
		someList.Clear();
	}



	// SENDS EVENTS
	private void SendEvent_NeedToShowTreasure(Transform myTransf, Vector2 myPos)
	{
		if (OnNeedToShowTreasure != null)
			OnNeedToShowTreasure(myTransf, myPos);
	}

	private void SendEvent_NeedToShowTrash(Transform myTransf, Vector2 myPos)
	{
		if (OnNeedToShowTrash != null)
			OnNeedToShowTrash(myTransf, myPos);
	}

	private void SendEvent_NeedToShowAlarmClock(Transform myTransf, Vector2 myPos)
	{
		if (OnNeedToShowAlarmClock != null)
			OnNeedToShowAlarmClock(myTransf, myPos);
	}
	


}
