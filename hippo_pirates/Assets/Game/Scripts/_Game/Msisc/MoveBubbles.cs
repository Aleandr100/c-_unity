﻿using UnityEngine;
using System.Collections;

public class MoveBubbles : MonoBehaviour
{
	// FIELDS
	private Transform _transform;
	private SpriteRenderer[] _sr = new SpriteRenderer[3];
	private float _tempColorAlpha;
	private readonly float _maxColorAlpha = 1f;			// max alpha is 1 !!!

	private readonly float _bottomPosY = -15f;
	private readonly float _fadePosY = 1f;

	private readonly float _speed = 0.7f;
	private readonly float _fadeSpeed = 0.01f;



	// UNITY
	private void Awake()
	{
		_transform = transform;
	}

	private void Start()
	{
		_sr[0] = _transform.GetChild(0).GetComponent<SpriteRenderer>();
		_sr[1] = _transform.GetChild(1).GetComponent<SpriteRenderer>();
		_sr[2] = _transform.GetChild(2).GetComponent<SpriteRenderer>();
	}
	
	private void Update()
	{
		Movement();
	}

	private void FixedUpdate()
	{
		CheckBuubleFade();
	}



	// METHODS
	private void Movement()
	{
		_transform.Translate(Vector2.up * Time.deltaTime * _speed);
	}

	private void CheckBuubleFade()
	{
		if (_transform.localPosition.y > _fadePosY)
		{
			// -----------------------------------------------------------
			for (int i = 0; i < _transform.childCount; i++)
			{
				if (_sr[i].color.a > 0)
				{
					_tempColorAlpha = _sr[i].color.a - _fadeSpeed;
					ChangeBubbleAlpha(_sr[i], _tempColorAlpha);
				}

				else
				{
					ResetBubble();
				}
			}
			// -----------------------------------------------------------
		}
	}

	private void ResetBubble()
	{
		_transform.localPosition = new Vector2(_transform.localPosition.x, _bottomPosY);
		
		for (int i = 0; i < _transform.childCount; i++)
		{
			ChangeBubbleAlpha(_sr[i], _maxColorAlpha);		
		}
	}

	private void ChangeBubbleAlpha(SpriteRenderer sr, float myValue)
	{
		Color myColor = sr.color;
		myColor.a = myValue;
		sr.color = myColor;
	}



}
