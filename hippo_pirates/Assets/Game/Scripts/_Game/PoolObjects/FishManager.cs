﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FishManager : MonoBehaviour
{
	// FIELDS
    [SerializeField] private Transform _forFish;
	[SerializeField] private Transform _fishPoolObjects;
    [SerializeField] private List<GameObject> _fishes;

	[SerializeField] private float _minPosY;		// -1f gameplay
	[SerializeField] private float _maxPosY;		// -2.25f gameplay

	private List<int> _idFishes = new List<int>();
	private int _tempIdFish;

    private float _delayCheck = 10f;				// 10 s
	private readonly int _minDelay = 3;				// 3 s
	private readonly int _maxDelay = 12;			// 12 s

	private readonly float _minPosX = -10f;
    private readonly float _maxPosX = 10f;



	// UNITY
	private void OnEnable()
	{
		FishMove.OnNeedToHideFish += HideFish;
	}

	private void OnDisable()
	{
		FishMove.OnNeedToHideFish -= HideFish;
	}

	private void Start()
    {
		InitIdFishList();
		ResetDelay();
		InvokeRepeating("ShowFish", 3f, _delayCheck);
	}



	// METHODS
	private void InitIdFishList()
	{
		for (int i = 0; i < _fishes.Count; i++)
		{
			_idFishes.Add(i);
		}

		ShuffleList(_idFishes);
	}

	private void ResetDelay()
	{
		int rndDelay = Random.Range(_minDelay, _maxDelay);
		_delayCheck += rndDelay;
	}

	private void ShowFish()			// used in Invoke
    {
		int rndFishId = ChooseFish();
		int rndFishDir = Random.Range(0, 2);

		Transform shadow = _fishes[rndFishId].transform.Find("Shadow");
		if (shadow == null)
			Error.Show("there is no Shadow sub-object");
		
		SetFishPosition(rndFishId, rndFishDir);
		SetFishDirection(rndFishId, rndFishDir);
		SetFishShadow(rndFishDir, shadow);

		_fishes[rndFishId].name = "Fish_" + rndFishId;

		_fishes[rndFishId].SetActive(true);
	}

	private int ChooseFish()
	{
		// start from previous
		for (int i = _tempIdFish; i < _idFishes.Count; i++)
		{
			int num = _idFishes[i];

			if (!_fishes[num].activeInHierarchy)
			{
				_tempIdFish = i + 1;
				//Debug.Log("find - start from prev");
				return num;
			}
		}

		// start from beginning
		for (int i = 0; i < _tempIdFish; i++)
		{
			int num = _idFishes[i];

			if (!_fishes[num].activeInHierarchy)
			{
				_tempIdFish = i + 1;
				//Debug.Log("find - start from begin");
				return num;
			}
		}

		Error.Show("there is no active fish in _fishes List");
		return 999;
	}

	private void SetFishPosition(int id, int dir)
	{
		GameObject go = _fishes[id];
		Transform goTransf = go.transform;

        goTransf.SetParent(_forFish);
        go.name = _fishes[id].name;

	    // coord X
        float posX = 0f;

        if (dir == 0)
            posX = _minPosX;
        else if (dir == 1)
            posX = _maxPosX;

		// coord Y
		float posY = Random.Range(_minPosY, _maxPosY);

		goTransf.localPosition = new Vector2(posX, posY);
	}

	private void SetFishDirection(int id, int dir)
	{
		GameObject go = _fishes[id];
		Transform goTransf = go.transform;

		if (dir == 0)		
        {
			var goFishMove = go.GetComponent<FishMove>();
			if (goFishMove != null)
				goFishMove.EndPos = new Vector2(_maxPosX, goTransf.position.y);
			else
				Error.Show("there is no FishMove script = " + go.name);
		}

        else if (dir == 1)
        {
			float goScaleX = goTransf.localScale.x;
			if (goScaleX > 0)
				goTransf.localScale = new Vector2(-goScaleX, goTransf.localScale.y);

			var goFishMove = go.GetComponent<FishMove>();
			if (goFishMove != null)
				goFishMove.EndPos = new Vector2(_minPosX, goTransf.position.y);
			else
				Error.Show("there is no FishMove script = " + go.name);
		}
	}

	private void SetFishShadow(int dir, Transform shadow)
	{
		if (dir == 1)
		{
			if (shadow.localPosition.x > 0)
				shadow.localPosition = new Vector2(-shadow.localPosition.x, shadow.localPosition.y);
		}
	}

	private void ShuffleList(List<int> someList)
    {
        for (int i = 0; i < someList.Count; i++)
        {
            int tmp = someList[i];
            int r = Random.Range(i, someList.Count);
            someList[i] = someList[r];
            someList[r] = tmp;
        }
    }



	// INTERFACE
	public void HideFish(GameObject go)
	{
		go.SetActive(false);
	}



}
