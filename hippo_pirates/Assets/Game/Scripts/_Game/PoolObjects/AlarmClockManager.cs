﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlarmClockManager : MonoBehaviour
{
	// FIELDS
	[SerializeField] private Transform _alarmClockPoolObject;
	[SerializeField] private List<GameObject> _alarmClockPrefabs;



	// UNITY
	private void OnEnable()
	{
		ItemSpawn.OnNeedToShowAlarmClock += ShowAlarmClock;
	}

	private void OnDisable()
	{
		ItemSpawn.OnNeedToShowAlarmClock -= ShowAlarmClock;
	}

	private void Start()
	{
		
	}



	// METHODS
	private void SetParameters(GameObject go, Transform parentPoint, Vector2 myPos)
	{
		Transform goTransf = go.transform;

		goTransf.SetParent(parentPoint);
		goTransf.localPosition = myPos;
		go.SetActive(true);
	}



	// INTERFACES
	public void ShowAlarmClock(Transform parentPoint, Vector2 pos)
	{
		for (int i = 0; i < _alarmClockPrefabs.Count; i++)
		{
			if (!_alarmClockPrefabs[i].activeInHierarchy)
			{
				SetParameters(_alarmClockPrefabs[i], parentPoint, pos);
				break;
			}
		}
	}

	

}
