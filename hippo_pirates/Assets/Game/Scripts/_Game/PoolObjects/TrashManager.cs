﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrashManager : MonoBehaviour
{
	// FIELDS
	[SerializeField] private Transform _trashPoolObject;
	[SerializeField] private List<GameObject> _trashPrefabs;
	[SerializeField] private Sprite[] _trashSprites;

	private readonly int[] _idTrash = { 0, 1, 2, 3, 4, 5, 6 };



	// UNITY
	private void OnEnable()
	{
		ItemSpawn.OnNeedToShowTrash += ShowTrash;
	}

	private void OnDisable()
	{
		ItemSpawn.OnNeedToShowTrash -= ShowTrash;
	}

	private void Start()
	{
		
	}



	// METHODS
	private void SetRandomSprite(GameObject go, int id)
	{
		SpriteRenderer goSR = go.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();

		if (goSR != null)
			goSR.sprite = _trashSprites[id];
		else
			Error.Show("there is no SpriteRenderer = " + go.name);
	}

	private void SetCollider(GameObject go, int id)
	{
		Transform colls = go.transform.GetChild(0).Find("Colliders_");
		if (colls == null)
			Error.Show("there is no /Colliders_/ child object = " + go.name);

		int collsCount = colls.childCount;
		for (int i = 0; i < collsCount; i++)
		{
			if (i == id)
				colls.GetChild(i).gameObject.SetActive(true);
			else
				colls.GetChild(i).gameObject.SetActive(false);
		}
	}

	private void SetParameters(GameObject go, Transform parentPoint, Vector2 myPos)
	{
		Transform goTransf = go.transform;

		RandomRotate(go);

		goTransf.SetParent(parentPoint);
		goTransf.localPosition = myPos;

		go.SetActive(true);
	}

	private void RandomRotate(GameObject go)
	{
		Transform goTrash = go.transform.GetChild(0);
		
		int rndAngle = Random.Range(-360, 360);
		goTrash.localRotation = Quaternion.Euler(goTrash.localRotation.x, goTrash.localRotation.y, rndAngle);
	}



	// INTERFACES
	public void ShowTrash(Transform parentPoint, Vector2 pos)
	{
		int rndId = Random.Range(0, _idTrash.Length);

		for (int i = 0; i < _trashPrefabs.Count; i++)
		{
			if (!_trashPrefabs[i].activeInHierarchy)
			{
				SetRandomSprite(_trashPrefabs[i], rndId);
				SetCollider(_trashPrefabs[i], rndId);
				SetParameters(_trashPrefabs[i], parentPoint, pos);
				break;
			}
		}
	}
	


}
