﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreasuresManager : MonoBehaviour
{
	// FIELDS
	[SerializeField] private Transform _treasurePoolObject;
	[SerializeField] private List<GameObject> _treasuresPrefabs;
	[SerializeField] private Sprite[] _treasuresSprites;

	private readonly int[] _idTreasures = { 0, 1, 2, 3, 4, 5 };
	


	// UNITY
	private void OnEnable()
	{
		ItemSpawn.OnNeedToShowTreasure += ShowTreasure;
	}

	private void OnDisable()
	{
		ItemSpawn.OnNeedToShowTreasure -= ShowTreasure;
	}

	private void Start()
	{

	}

	

	// METHODS
	private void SetRandomSprite(GameObject go, int id)
	{
		SpriteRenderer goSR = go.transform.GetChild(0).GetComponent<SpriteRenderer>();

		if (goSR != null)
			goSR.sprite = _treasuresSprites[id];
		else
			Error.Show("there is no SpriteRenderer = " + go.name);
	}

	private void SetCollider(GameObject go, int id)
	{
		Transform colls = go.transform.Find("Colliders_");
		if (colls == null)
			Error.Show("there is no /Colliders_/ child object = " + go.name);

		int collsCount = colls.childCount;
		for (int i = 0; i < collsCount; i++)
		{
			if (i == id)
				colls.GetChild(i).gameObject.SetActive(true);
			else
				colls.GetChild(i).gameObject.SetActive(false);
		}
	}

	private void SetParameters(GameObject go, int id, Transform parentPoint, Vector2 myPos)
	{
		go.name = "Treasure_" + id;
		Transform goTransf = go.transform;

		goTransf.SetParent(parentPoint);
		goTransf.localPosition = myPos;
		go.SetActive(true);
	}



	// INTERFACES
	public void ShowTreasure(Transform parentPoint, Vector2 pos)
	{
		int rndId = Random.Range(0, _idTreasures.Length);

		for (int i = 0; i < _treasuresPrefabs.Count; i++)
		{
			if (!_treasuresPrefabs[i].activeInHierarchy)
			{
				SetRandomSprite(_treasuresPrefabs[i], rndId);
				SetCollider(_treasuresPrefabs[i], rndId);
				SetParameters(_treasuresPrefabs[i], rndId, parentPoint, pos);
				break;
			}
		}
	}



}
