﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IntroMenu : MonoBehaviour
{
	// FIELDS
	[SerializeField] private GameObject _diver;
	[SerializeField] private GameObject _peppa;
	[SerializeField] private GameObject _splash;
	
	private Animator _animatorDiver;
	private Animator _animatorPeppa;
	private Animator _animatorSplash;
	
	private readonly float _lenDop = 0.5f;

	// sounds
	private readonly string _soundPeppaOne = "hp-2";
	private readonly string _soundPeppaTwo = "hp-3";

	private readonly string _soundPeppaLaugh = "hp-7";
	private readonly string _soundSealoop = "sealoop";
	private readonly string _soundSplash = "WaterSplash";

	// animations
	private readonly string _animPeppaIdleTalk = "idle_talk";
	private readonly string _animPeppaIdle = "idle";
	private readonly string _animPeppaJoyJump = "joy_2_foot";

	private readonly string _animDiverIntroHand = "DiverIntro_hand";
	private readonly string _animDiverIntroIdle = "DiverIntro_idle";
	private readonly string _animDiverIntroJump = "DiverIntro_jump";

	private readonly string _animSplash = "SplashIntro_play";
	


	// UNITY
	private void OnDisable()
	{
		AudioController.Instance.StopAll();
		AnimationManager.Instance.StopAll();
		AudioController.Release();
		StopAll();
	}

	private void Awake()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		//
		_animatorDiver = _diver.transform.GetChild(0).GetComponent<Animator>();
		if (_animatorDiver == null)
			Error.Show("there is no Animator = " + _diver);

		_animatorPeppa = _peppa.GetComponent<Animator>();
		if (_animatorPeppa == null)
			Error.Show("there is no Animator = " + _peppa);

		_animatorSplash = _splash.transform.GetChild(0).GetComponent<Animator>();
		if (_animatorSplash == null)
			Error.Show("there is no Animator = " + _splash);
	}

	private void Start()
	{
		PlaySequence();
		PlayStreams();
	}



	// UNITY
	private void PlaySequence()
	{
		// ----------------------------------------------------------------------------------------------------
		// 1-st Peppa's speech
		float delayOne = 1.5f;

		float tempPeppaOne = AudioController.GetClipLenght(_soundPeppaOne);

		AudioController.Instance.PlaySound(_soundPeppaOne, delayOne);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdleTalk, delayOne);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdle, delayOne + tempPeppaOne);

		//
		float delayOneDop = delayOne + tempPeppaOne + _lenDop + 0.5f;

		AudioController.Instance.PlaySound(_soundPeppaLaugh, delayOneDop);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaJoyJump, delayOneDop);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdle, delayOneDop + 1f);

		// ----------------------------------------------------------------------------------------------------
		// 2-st Peppa's speech
		float delayTwo = delayOneDop + 1.5f;

		float tempPeppaTwo = AudioController.GetClipLenght(_soundPeppaTwo);

		AudioController.Instance.PlaySound(_soundPeppaTwo, delayTwo);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdleTalk, delayTwo);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdle, delayTwo + tempPeppaTwo + _lenDop);

		//
		float delayTwoDopA = delayTwo + 1f;

		AnimationManager.Instance.PlayAnimation(_animatorDiver, _animDiverIntroHand, delayTwoDopA);
		AnimationManager.Instance.PlayAnimation(_animatorDiver, _animDiverIntroIdle, delayTwoDopA + 2f);

		AnimationManager.Instance.PlayAnimation(_animatorDiver, _animDiverIntroJump, delayTwo + tempPeppaTwo + _lenDop + 0.5f);

		//
		float delayTwoDopB = delayTwo + tempPeppaTwo + _lenDop + 0.5f;

		AudioController.Instance.PlaySound(_soundSplash, delayTwoDopB + 0.6f);
		AnimationManager.Instance.PlayAnimation(_animatorSplash, _animSplash, delayTwoDopB + 0.6f);

		// ----------------------------------------------------------------------------------------------------

		StartCoroutine(LoadScene(delayTwoDopB + 1.5f));
	}

	private void LoadScene()
	{
		SceneLoader.Instance.SwitchToScene(SceneLoader.Scenes.Game);
	}

	private IEnumerator LoadScene(float delay)
	{
		yield return new WaitForSeconds(delay);

		LoadScene();
	}

	private void PlayStreams()
	{
		AudioController.PlayStreamCicle(_soundSealoop);
	}

	private void StopAll()
	{
		StopAllCoroutines();
	}



}
