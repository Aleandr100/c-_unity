﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	// FIELDS
	private readonly string _soundMusicMenu = "menu";
	private readonly string _soundPeppa = "hp-1";



	// UNITY
	private void OnDisable()
	{
		AudioController.Release();
		AudioController.Instance.StopAll();
	}

	private void Awake()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		ManagerGoogle.Instance.HideSmallBanner();
	}

	private void Start()
	{
		PlayMusics();
		AudioController.Instance.PlaySound(_soundPeppa, 1f);
	}

	private void Update()
	{
		ManagerGoogle.Instance.HideSmallBanner();
	}



	// METHODS
	private void PlayMusics()
	{
		AudioController.PlayMusic(_soundMusicMenu);
	}
	


}
