﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class PopupWin : MonoBehaviour
{
	// FIELDS
	[SerializeField] private GameObject _scriptsOnScene;
	[SerializeField] private GameObject _timerClock;

	[SerializeField] private Button _buttonRestart;
	[SerializeField] private Button _buttonMainMenu;
	[SerializeField] private Text[] _textsScores;
	[SerializeField] private Image[] _stars;

	private readonly string[] _labelsScores =
	{
		"№ 1 . . . ",
		"№ 2 . . . ",
		"№ 3 . . . ",
		"№ 4 . . . ",
		"№ 5 . . . ",
	};
	private Vector2[] _starsPos = new Vector2[5];

	private readonly Vector2 _startPopupScale = new Vector2(0, 0);
	private readonly Vector2 _startStarsScale = new Vector2(5, 5);

	private readonly Color _winTextColor = new Color(0.918f, 0.435f, 0.063f, 1.0f);
	private readonly Color _winOutlineColor = new Color(0.957f, 0.555f, 0.555f, 0.5f); 

	// sounds
	private readonly string _soundButtonClick = "Button - Click";
	private readonly string _soundStar = "star";



	// UNITY
	private void Awake()
	{
		SetStartParameters();
	}

	private void OnEnable()
	{
		SaveDataManager.OnSavedCurrScore += CheckScore;
		SaveDataManager.OnSavedScoresTable += ShowScoresTable;
	}

	private void OnDisable()
	{
		SaveDataManager.OnSavedCurrScore -= CheckScore;
		SaveDataManager.OnSavedScoresTable -= ShowScoresTable;
	}

	private void Start ()
	{
		_buttonRestart.onClick.AddListener( () => OnClickButtonRestart() );
		_buttonMainMenu.onClick.AddListener( () => OnClickButtonMainMenu() );
	}



	// METHODS
	private void SetStartParameters()
	{
		transform.localScale = _startPopupScale;

		for (int i = 0; i < _stars.Length; i++)
		{
			_stars[i].transform.localScale = _startStarsScale;
			_starsPos[i] = _stars[i].transform.localPosition;
		}
	}

	private void ShowPopup()
	{
		gameObject.SetActive(true);
		transform.DOScale(new Vector3(1, 1), 1f).SetUpdate(true);
	}

	private void StartFakePause()
	{
		var inpMan = _scriptsOnScene.GetComponent<InputManager>();
		if (inpMan != null && inpMan.isActiveAndEnabled)
			inpMan.enabled = false;
		else
			Error.Show("there is no InputManager");

		_timerClock.SetActive(false);
	}

	private void ShowStars(int myNum, float myDelay)
	{
		StartCoroutine(DelayedShowStar(myNum, myDelay));
	}

	private IEnumerator DelayedShowStar(int myNum, float myDelay)
	{
		yield return new WaitForSeconds(myDelay);

		AudioController.PlaySound(_soundStar);

		_stars[myNum].gameObject.SetActive(true);
		_stars[myNum].transform.DOScale(new Vector3(1, 1), 0.5f).SetUpdate(true);
		_stars[myNum].transform.DOLocalMove(_starsPos[myNum], 0.5f).SetUpdate(true);
	}



	// INTERFACES
	private void CheckScore(int myScore)
	{
		// ----------------------------------------------------------
		//
		if (myScore <= 0)
		{
			// none
		}
		// ----------------------------------------------------------
		//
		if (myScore > 1 && myScore < 1000)
		{
			ShowStars(0, 0.5f);
		}
		// ----------------------------------------------------------
		//
		else if (myScore >= 1000 && myScore < 2000)
		{
			ShowStars(0, 0.5f);
			ShowStars(1, 1f);
		}
		// ----------------------------------------------------------
		//
		else if (myScore >= 2000 && myScore < 3000)
		{
			ShowStars(0, 0.5f);
			ShowStars(1, 1f);
			ShowStars(2, 1.5f);
		}
		// ----------------------------------------------------------
		//
		else if (myScore >= 3000 && myScore < 4000)
		{
			ShowStars(0, 0.5f);
			ShowStars(1, 1f);
			ShowStars(2, 1.5f);
			ShowStars(3, 2f);
		}
		// ----------------------------------------------------------
		//
		else if (myScore > 4000)
		{
			ShowStars(0, 0.5f);
			ShowStars(1, 1f);
			ShowStars(2, 1.5f);
			ShowStars(3, 2f);
			ShowStars(4, 2.5f);
		}
		// ----------------------------------------------------------
	}

	private void ShowScoresTable(int[] myScores, int myScorePos)
	{
		ShowPopup();
		StartFakePause();

		for (int i = 0; i < _textsScores.Length; i++)
		{
			_textsScores[i].text = _labelsScores[i] + myScores[i];
			if (i == myScorePos)
			{
				_textsScores[i].color = _winTextColor;
				_textsScores[i].GetComponent<Outline>().effectColor = _winOutlineColor;
			}

		}
	}



	// BUTTON CLICK
	public void OnClickButtonMainMenu()
	{
		AudioController.PlaySound(_soundButtonClick);
		SceneLoader.Instance.SwitchToScene(SceneLoader.Scenes.MainMenu);
	}

	public void OnClickButtonRestart()
	{
		AudioController.PlaySound(_soundButtonClick);
		SceneLoader.Instance.SwitchToScene(SceneLoader.Scenes.Game);
	}



}
