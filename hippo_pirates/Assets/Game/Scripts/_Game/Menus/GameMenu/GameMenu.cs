﻿using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour
{
	// FIELDS
	[SerializeField] private GameObject _peppa;

	private Animator _animatorPeppa;

	// animations
	private readonly string _animPeppaIdleTalk = "idle_talk";
	private readonly string _animPeppaIdle = "idle";

	// sounds
	private readonly string _soundSealoop = "sealoop";
	private readonly string _soundShiploop = "shiploop";
	private readonly string _soundMusicGame = "game";

	private readonly string _soundPeppaHowManyTreasures = "hp-4";



	// UNITY
	private void Awake()
	{
		Application.targetFrameRate = 60;				// FPS - frame per second
		ManagerGoogle.Instance.ShowSmallBanner();
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		//
		_animatorPeppa = _peppa.GetComponent<Animator>();
		if (_animatorPeppa == null)
			Error.Show("there is no Animator = " + _peppa);
	}

	private void OnDisable()
	{
		ManagerGoogle.Instance.HideSmallBanner();
		AudioController.Instance.StopAll();
		AnimationManager.Instance.StopAll();
		AudioController.Release();
	}

	private void Start()
	{
		PlayMusics();
		PlayStreams();
		StartSequence();
	}



	// METHODS
	private void StartSequence()
	{
		float tempClipLenght = AudioController.GetClipLenght(_soundPeppaHowManyTreasures);

		AudioController.Instance.PlaySound(_soundPeppaHowManyTreasures, 1f);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdleTalk, 1f);
		AnimationManager.Instance.PlayAnimation(_animatorPeppa, _animPeppaIdle, tempClipLenght + 0.5f);

		StartCoroutine(StopAll(tempClipLenght + 2f));
	}

	private IEnumerator StopAll(float delay)
	{
		yield return new WaitForSeconds(delay);

		StopAllCoroutines();
	}

	private void PlayMusics()
	{
		AudioController.PlayMusic(_soundMusicGame);
	}

	private void PlayStreams()
	{
		AudioController.PlayStreamCicle(_soundSealoop);
		AudioController.PlayStreamCicle(_soundShiploop);
	}



}
