﻿//#define USE_LOGS

using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Text;

using Debug = UnityEngine.Debug;

public class Message : MonoBehaviour
{

	[Conditional("USE_LOGS")]
	public static void Show(string msg)
	{
		Debug.Log(msg);
	}

}
