﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SaveDataManager : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent(int[] myScores, int myCurrScorePos);
	public static event MyEvent OnSavedScoresTable;

    public delegate void MyEvent2(int myScore);
    public static event MyEvent2 OnSavedCurrScore;

    // FIELDS
    private readonly string[] _names =
	{
		"Score_0",
		"Score_1",
		"Score_2",
		"Score_3",
		"Score_4"
	};

	private readonly int[] _values = { 0, 0, 0, 0, 0 };
	private int[] _scoresOld = new int[5];
	private int[] _scoresNew = new int[5];

	private int _currScorePos;
	private readonly int _wrongScorePos = -999;



	// UNITY
	private void OnEnable()
	{
		ScoreManager.OnSaveScore += SetAndSaveScore;
	}

	private void OnDisable()
	{
		ScoreManager.OnSaveScore -= SetAndSaveScore;
	}

	private void Start ()
	{
		
	}



	// METHODS
	private void FirstTimeInitPlayerPrefs()
	{
		for (int i = 0; i < _names.Length; i++)
		{
			if (!PlayerPrefs.HasKey(_names[i]))
				PlayerPrefs.SetInt(_names[i], _values[i]);
		}
	}

	private void GetScoreFromPlayerPrefs()
	{
		for (int i = 0; i < _names.Length; i++)
		{
			if (PlayerPrefs.HasKey(_names[i]))
				_scoresOld[i] = PlayerPrefs.GetInt(_names[i]);
			else
				Error.Show("SaveDataManger problem");
		}
	}

	private void SortScores()
	{
		Array.Sort(_scoresOld);
		Array.Reverse(_scoresOld);
	}

	private void GetCurrScorePosition(int myScore)
	{
		_currScorePos = _wrongScorePos;

		for (int i = 0; i < _names.Length; i++)
		{
			if (myScore >= _scoresOld[i])
			{
				_currScorePos = i;
				break;
			}
		}
	}

	private void SetScoreTableNew(int myScore)
	{
		if (_currScorePos == _wrongScorePos)
		{
			for (int i = 0; i < _names.Length; i++)
			{
				_scoresNew[i] = _scoresOld[i];
			}
		}

		else
		{
			for (int i = 0; i < _names.Length; i++)
			{
				if (i < _currScorePos)
					_scoresNew[i] = _scoresOld[i];
				else if (i == _currScorePos)
					_scoresNew[i] = myScore;
				else if (i > _currScorePos)
					_scoresNew[i] = _scoresOld[i - 1];
			}
		}
	}

	private void SetPlayerPrefs()
	{
		for (int i = 0; i < _names.Length; i++)
		{
			if (PlayerPrefs.HasKey(_names[i]))
				PlayerPrefs.SetInt(_names[i], _scoresNew[i]);
			else
				Error.Show("there is no such key = " + _names[i]);
		}
	}

	private void SavePlayerPrefs()
	{
		PlayerPrefs.Save();
	}



	// INTERFACES
	public void SetAndSaveScore(int myScore)
	{
		FirstTimeInitPlayerPrefs();

		GetScoreFromPlayerPrefs();
		SortScores();
		GetCurrScorePosition(myScore);
		SetScoreTableNew(myScore);
		SetPlayerPrefs();
		SavePlayerPrefs();

		// event!
		SendEvent_SavedCurrScore(myScore);
		// event!
		SendEvent_SavedScoresTable(_scoresNew, _currScorePos);
	}



	// SEND EVENTS
	private void SendEvent_SavedCurrScore(int myScore)
	{
		if (OnSavedCurrScore != null)
			OnSavedCurrScore(myScore);        
	}

	private void SendEvent_SavedScoresTable(int[] arrScores, int myScorePos)
	{
		if (OnSavedScoresTable != null)
			OnSavedScoresTable(arrScores, myScorePos); 
	}
	
	

}
