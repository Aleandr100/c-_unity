﻿using UnityEngine;
using System.Collections;

public class RandomManager : MonoBehaviour
{
	// PROPERTIES
	public static RandomManager Instance { get; private set; }

	// FIELDS
	private int[][] _variants = new int[20][];
	private int[] _idVariants = new int[20];
	private int[] _tempVariants = new int[10];

	private int _spawnCount;
	private int _spawnCountAlarn;

	private int _compareCountAlarm;



	// UNITY
	private void Awake()
	{
		Instance = this;

		FillVariants();
		FillIdVariants();
		ShuffleArray(_idVariants);
		ResetCompareAlarm();
	}

	private void Start()
	{
		
	}



	// METHODS
	private void FillVariants()
	{
		// 0 - emty
		// 1 - trash
		// 5 - treasure
		// 9 - alarm-clock

		// ----------------------------------------------------
		// fill-6
		_variants[0] = new int[]  { 0, 1, 5, 0, 1, 5, 0, 1, 1, 0 };
		_variants[1] = new int[]  { 0, 0, 1, 1, 5, 1, 0, 5, 0, 1 };
		_variants[2] = new int[]  { 5, 1, 0, 1, 0, 5, 1, 0, 5, 0 };
		_variants[3] = new int[]  { 1, 1, 0, 5, 0, 1, 0, 1, 0, 5 };

		// ----------------------------------------------------
		// fill-7
		_variants[4] = new int[]  { 5, 1, 0, 1, 5, 0, 1, 0, 5, 1 };
		_variants[5] = new int[]  { 0, 5, 5, 1, 0, 1, 5, 1, 0, 1 };
		_variants[6] = new int[]  { 1, 1, 0, 5, 1, 0, 1, 5, 0, 5 };
		_variants[7] = new int[]  { 0, 5, 1, 1, 0, 5, 1, 1, 5, 0 };

		// ----------------------------------------------------
		// fill-8
		_variants[8] = new int[]  { 1, 5, 0, 5, 1, 1, 0, 1, 1, 5 };
		_variants[9] = new int[]  { 5, 0, 1, 1, 5, 1, 5, 0, 1, 1 };
		_variants[10] = new int[] { 0, 1, 5, 1, 0, 5, 1, 1, 5, 1 };
		_variants[11] = new int[] { 5, 5, 1, 1, 0, 1, 5, 1, 0, 1 };


		// ----------------------------------------------------
		// fill-9
		_variants[12] = new int[] { 1, 1, 1, 5, 1, 0, 1, 5, 5, 1 };
		_variants[13] = new int[] { 1, 5, 1, 1, 1, 5, 1, 1, 0, 5 };
		_variants[14] = new int[] { 5, 1, 0, 1, 5, 1, 1, 1, 5, 1 };
		_variants[15] = new int[] { 1, 1, 5, 1, 1, 5, 5, 1, 1, 0 };

		// ----------------------------------------------------
		// fill-10
		_variants[16] = new int[] { 1, 5, 5, 1, 1, 5, 1, 1, 5, 1 };
		_variants[17] = new int[] { 5, 1, 1, 5, 1, 1, 5, 5, 1, 1 };
		_variants[18] = new int[] { 1, 5, 1, 5, 1, 5, 1, 1, 1, 5 };
		_variants[19] = new int[] { 1, 1, 5, 1, 5, 1, 5, 1, 5, 1 };
	}

	private void FillIdVariants()
	{
		for (int i = 0; i < 20; i++)
		{
			_idVariants[i] = i;
		}
	}

	private void ResetCompareAlarm()
	{
		int rnd = Random.Range(2, 4);		// every 2 or 3 bottom item spawn
		_compareCountAlarm = rnd;
	}

	public int[] GetVariants()
	{
		if (_spawnCount >= 20)
		{
			_spawnCount = 0;
			ShuffleArray(_idVariants);
		}
		
		_tempVariants = _variants[ _idVariants[_spawnCount] ];
		_spawnCount++;
		_spawnCountAlarn++;

		CheckForAlarmSpawn(_tempVariants);

		return _tempVariants;
	}

	private void CheckForAlarmSpawn(int[] myArray)
	{
		if (_spawnCountAlarn >= _compareCountAlarm)
		{
			if (_spawnCount >= 4)
			{
				int rnd = Random.Range(0, 10);
				for (int i = 0; i < 10; i++)
				{
					if (i == rnd)
						myArray[rnd] = 9;		// 9 - alarm-clock
				}

				_spawnCountAlarn = 0;
				ResetCompareAlarm();
			}
		}
	}

	private void ShuffleArray(int[] someArray)
    {
        for (int i = 0; i < someArray.Length; i++)
        {
            int tmp = someArray[i];
            int r = Random.Range(i, someArray.Length);
            someArray[i] = someArray[r];
            someArray[r] = tmp;
        }
    }



}
