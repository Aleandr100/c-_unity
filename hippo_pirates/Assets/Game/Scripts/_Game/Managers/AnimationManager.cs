﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour
{
	// PROPERTIES
	public static AnimationManager Instance { get; private set; }



	// UNITY
	private void Awake()
	{
		Instance = this;
	}
	
	private void Start()
	{
	
	}



	// METHODS
	public static void PlayAnimation(Animator anim, string myClip)
	{
		if (myClip != null)
		{
			anim.Play(myClip);
		}
		else
		{
			Error.Show("clip for Play in Animator is null");
		}
	}

	public void PlayAnimation(Animator anim, string myClip, float myDelay)
	{
		StartCoroutine(DelayPlayAnimation(anim, myClip, myDelay));
	}

	public static IEnumerator DelayPlayAnimation(Animator anim, string myClip, float myDelay)
	{
		yield return new WaitForSeconds(myDelay);

		PlayAnimation(anim, myClip);
	}

	public void StopAll()
	{
		StopAllCoroutines();
	}



}
