﻿using System;
using UnityEngine;
using System.Collections;
using DG.Tweening;
using Random = UnityEngine.Random;

public class InputManager : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent(GameObject go);			
	public static event MyEvent OnPulledItemOnBoat;

	// FIELDS
	[SerializeField] private GameObject _diver;
	[SerializeField] private GameObject _peppa;
	[SerializeField] private GameObject _hook;
	[SerializeField] private GameObject _hookForAnim;
	[SerializeField] private Transform _dummyHook;
	[SerializeField] private GameObject _rope;
	[SerializeField] private Transform _forCatchedItems;

	public enum HookStates
	{
		NONE,
		MOVE_TARGET,
		MOVE_START,
		MOVE_HALFWAY,
		CATCH_ITEM
	}

	[HideInInspector] public HookStates _currHookState = HookStates.NONE;

	private enum ClickStates
	{
		NONE,
		CLICKED,
		THROW_HOOK
	}

	private ClickStates _currClickState = ClickStates.NONE;

	private Transform _hookTransf;
	private LineRenderer _ropeLR;

	private Vector2 _hookStartPos;
	private Vector2 _endPos;
	private readonly float _speed = 4f;
	private readonly float _minInputY = 0.6f;

	private Animator _diverAnimator;
	private Animator _peppaAnimator;

	private GameObject _catchedItem;
	private readonly float _delayThrowHook = 0.5f;	
	private readonly Vector2 _vectThrowOnBoat = new Vector2(-0.5f, 4.9f);
	private readonly float _delayThrowOnBoat = 0.7f;

	private readonly string _ropeSL = "Rope";
	private readonly string _objStrShadow = "Shadow";

	// animations
	private readonly string _animDiverThrowHook = "HippoDiver_throw_hook";
	private readonly string _animDiverCatchOnBoat = "HippoDiver_catch_on_boat";
	private readonly string _animDiverIdle = "Idle";

	private readonly string _animPeppaJoy = "joy 0";
	private readonly string _animPeppaSad = "sad";

	// sounds
	private readonly string _soundClock = "clock";
	private readonly string _soundWwrong = "wrong";
	private readonly string _soundShoot = "shoot";
	private readonly string _soundSpinner = "spinner";
	private readonly string _soundBoat = "boat";
	private readonly string _soundGold = "gold";
	private readonly string _soundLock = "lock";
	private readonly string _soundPunch = "Punch";

	private readonly string[] _trSounds =
	{
		"tr_1",
		"tr_2",
		"tr_3"
	};

	private readonly string[] _goodCatchSounds =
	{
		"hp-5",
		"hp-6",
		"hp-7",
		"hp-8",
		"hp-9",
		"hp-10",
		"hp-11"
	};

	private readonly string[] _badCatchSounds =
	{
		"hp-12",
		"hp-13",
		"hp-14",
		"hp-15"
	};



	// UNITY
	private void OnEnable()
	{
		Hook.OnCatchItem += WhenCatchItem;
	}

	private void OnDisable()
	{
		Hook.OnCatchItem -= WhenCatchItem;
	}

	private void Start()
	{
		_hookTransf = _hook.transform;
		_hookStartPos = _hook.transform.position;

		_diverAnimator = _diver.GetComponent<Animator>();
		if (_diverAnimator == null)
			Error.Show("there is no Diver Animator");

		_peppaAnimator = _peppa.GetComponent<Animator>();
		if (_peppaAnimator == null)
			Error.Show("there is no Peppa Animator");

		_ropeLR = _rope.GetComponent<LineRenderer>();
		_ropeLR.sortingLayerName = _ropeSL;
	}

	private void Update()
	{
		CheckInput();
		
		if (_currHookState != HookStates.NONE)
		{
			MoveHook();
			MoveRope();
		}
	}



	// METHODS
	private void CheckInput()
	{
		if (_currHookState == HookStates.NONE)
		{
			// ----------------------------------------------------------
			//
			if (_currClickState == ClickStates.NONE)
			{
				if (Input.GetMouseButtonDown(0))
				{
					_endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

					if (_endPos.y < _minInputY)
					{
						_currClickState = ClickStates.CLICKED;

						AnimationManager.PlayAnimation(_diverAnimator, _animDiverThrowHook);
						AudioController.PlaySound(_soundShoot);

						Invoke("HideHookForAnim", _delayThrowHook);
						Invoke("ThrowHook", _delayThrowHook);

						AudioController.Instance.PlaySound(_soundSpinner, 0.3f);
					}
				}
			}
			// ----------------------------------------------------------
			//
			else if (_currClickState == ClickStates.THROW_HOOK)
			{
				if (_endPos.y < _minInputY)
				{
					ChangeHookRotation();
					_currHookState = HookStates.MOVE_TARGET;
				}
			}
			// ----------------------------------------------------------
		}
	}

	private void MoveHook()
	{
		_hookTransf.position = Vector2.MoveTowards(_hookTransf.position, _endPos, Time.deltaTime * _speed);

		// ----------------------------------------------------------
		//
		if (_currHookState == HookStates.CATCH_ITEM)
		{
			_endPos = GetBeetweenPosition();
			_currHookState = HookStates.MOVE_HALFWAY;
		}
		// ----------------------------------------------------------
		//
		if ((Vector2)_hookTransf.position == _endPos)
		{
			if (_currHookState == HookStates.MOVE_TARGET)
			{
				_endPos = GetBeetweenPosition();
				_currHookState = HookStates.MOVE_HALFWAY;
			}

			else if (_currHookState == HookStates.MOVE_HALFWAY)
			{
				_endPos = _hookStartPos;
				_currHookState = HookStates.MOVE_START;

				ResetHookRotation();
			}

			else if (_currHookState == HookStates.MOVE_START)
			{
				_currHookState = HookStates.NONE;
				_currClickState = ClickStates.NONE;		// now we can throw hook again!

				AudioController.StopSound(_soundSpinner);

				ResetHookRotation();
				ReactivateHookTrigger();
				ThrowCatchedItemOnBoat();

				_rope.SetActive(false);	
				_hookForAnim.SetActive(true);
			}
		}
		// ----------------------------------------------------------
	}

	private void MoveRope()
	{
		_ropeLR.SetPosition(1, _hookTransf.localPosition);
	}

	private void HideHookForAnim()		// use in Invoke()
	{
		_hookForAnim.SetActive(false);
	}

	private void ThrowHook()			// use in Invoke()
	{
		_currClickState = ClickStates.THROW_HOOK;
		_rope.SetActive(true);
	}

	private Vector2 GetBeetweenPosition()
	{
		Vector2 firstPos = _hookStartPos;
		Vector2 secondPos = new Vector2(_hookStartPos.x, _hook.transform.position.y);
		Vector2 betweenPos = (firstPos + secondPos) * 0.5f;

		return betweenPos;
	}

	private void ChangeHookRotation()
	{
		_dummyHook.LookAt(_endPos);
		float angle = 90f - _dummyHook.eulerAngles.x;

		if (_endPos.x < _hookTransf.position.x)
			angle *= -1f; 
		
		_hook.transform.localRotation = Quaternion.Euler(0, 0, angle);
	}

	private void ResetHookRotation()
	{
		_hookTransf.localRotation = Quaternion.Euler(0, 0, 0);
	}

	private void ReactivateHookTrigger()
	{
		var hookTrig = _hook.GetComponent<Hook>();
		if (hookTrig != null && hookTrig.isActiveAndEnabled)
			hookTrig.IsCatchItem = false;
		else
			Error.Show("there is no Hook script");
	}

	private void ThrowCatchedItemOnBoat()
	{
		if (_catchedItem != null)
		{
			AudioController.PlaySound(_soundBoat);
			AnimationManager.PlayAnimation(_diverAnimator, _animDiverCatchOnBoat);

			Transform _catchedTransf = _catchedItem.transform;

			_catchedTransf.SetParent(_forCatchedItems);
			_catchedTransf.DOMove(_vectThrowOnBoat, _delayThrowOnBoat);
			_catchedTransf.DOScale(new Vector2(_catchedTransf.localScale.x / 2f, _catchedTransf.localScale.y / 2f), _delayThrowOnBoat);

			StartCoroutine(HideCatchedItem(_delayThrowOnBoat));
		}

		else
		{
			AnimationManager.PlayAnimation(_diverAnimator, _animDiverIdle);
		}
	}

	private IEnumerator HideCatchedItem(float myDelay)
	{
		yield return new WaitForSeconds(myDelay);

		// event!
		SendEvent_PulledItemOnBoat();

		CheckCatchPeppa();
		_catchedItem.SetActive(false);
		ResetObjectParamsAfterHide(_catchedItem);
		_catchedItem = null;
	}

	private void WhenCatchItem(GameObject coll)
	{
		_catchedItem = coll;
		_currHookState = HookStates.CATCH_ITEM;

		if (coll.CompareTag(Tags.Treasure))
		{
			AudioController.PlaySound(_soundGold);
		}
		else
		{
			AudioController.PlaySound(_soundLock);
		}
	}

	private void CheckCatchPeppa()
	{
		if (_catchedItem != null)
		{
			// ----------------------------------------------------------
			//
			if (_catchedItem.CompareTag(Tags.Treasure))
			{
				AudioController.ReleaseStreams();
				string rndSound = GetRandomSound(_trSounds);
				float tempClipLenght = GetClipLenght(rndSound);
				AudioController.PlaySound(rndSound);

				

				AnimationManager.Instance.PlayAnimation(_peppaAnimator, _animPeppaJoy, tempClipLenght);
				AudioController.Instance.PlaySound(GetRandomSound(_goodCatchSounds), tempClipLenght);
			}
			// ----------------------------------------------------------
			//
			else if (_catchedItem.CompareTag(Tags.Fish))
			{
				AudioController.ReleaseStreams();
				float tempClipLenght = GetClipLenght(_soundPunch);
				AudioController.PlaySound(_soundPunch);

				AnimationManager.Instance.PlayAnimation(_peppaAnimator, _animPeppaJoy, tempClipLenght);
				AudioController.Instance.PlaySound(_goodCatchSounds[0], tempClipLenght);
			}
			// ----------------------------------------------------------
			//
			else if (_catchedItem.CompareTag(Tags.AlarmClock))
			{
				AnimationManager.PlayAnimation(_peppaAnimator, _animPeppaJoy);
				AudioController.ReleaseStreams();
				AudioController.PlaySound(_soundClock);
			}
			// ----------------------------------------------------------
			//
			else if (_catchedItem.CompareTag(Tags.Trash))
			{
				AudioController.ReleaseStreams();
				float tempClipLenght = GetClipLenght(_soundWwrong);
				AudioController.PlaySound(_soundWwrong);

				AnimationManager.Instance.PlayAnimation(_peppaAnimator, _animPeppaSad, tempClipLenght);
				AudioController.Instance.PlaySound(GetRandomSound(_badCatchSounds), tempClipLenght);
			}
			// ----------------------------------------------------------
		}
	}

	private void ResetObjectParamsAfterHide(GameObject go)
	{
		// reset scale 
		Transform goTransf = go.transform;
		goTransf.localScale = new Vector2(goTransf.localScale.x * 2f, goTransf.localScale.y * 2f);

		// reset rotation
		goTransf.localRotation = Quaternion.Euler(0, 0, 0);

		// reset shadow
		Transform goShadow = go.transform.Find(_objStrShadow);
		if (goShadow != null)
			goShadow.gameObject.SetActive(true);
		else
			Error.Show("there is no Shadow sub-object");
	}

	private string GetRandomSound(string[] mySounds)
	{
		int rnd = Random.Range(0, mySounds.Length);

		return mySounds[rnd];
	}

	private float GetClipLenght(string snd_name)
	{
		if (snd_name != "")
		{
			AudioClip clip = LanguageAudio.GetSoundByName(snd_name);
			if (clip != null)
			{
				return clip.length;
			}
		}

		return 0;
	}



	// SEND EVENTS
	private void SendEvent_PulledItemOnBoat()
	{
		if (OnPulledItemOnBoat != null)
			OnPulledItemOnBoat(_catchedItem); 
	}



}
