﻿using UnityEngine;
using System.Collections;

public class MovementManager : MonoBehaviour
{
	// FIELDS
	[SerializeField] private Transform[] _bottoms;

	private readonly float _bottomResetPosX = -20f;
	private readonly float _bottomWidht = 19.1f;
	private readonly float _bottomSpeed = 1f;

	private readonly float _minCompareX = -999999f;
	
	

	// UNITY
	private void Start ()
	{

	}

	private void Update()
	{
		BottomMovement();
	}



	// METHODS
	private void BottomMovement()
	{
		foreach (var item in _bottoms)
		{
			item.Translate(Vector2.left * Time.deltaTime * _bottomSpeed);
		
			if (item.localPosition.x < _bottomResetPosX)
			{
				float biggestPosX = GetBiggestPosition(_bottoms);
				SetPosition(item, new Vector2(biggestPosX + _bottomWidht, 0f));
				ResetItems(item);
			}
		}
	}

	private void ResetItems(Transform goTransf)
	{
		var goItemSpawn = goTransf.GetComponent<ItemSpawn>();
		if (goItemSpawn != null)
		{
			goItemSpawn.ResetItems();
		}
		else
		{
			Error.Show("there is no ItemSpawn script = " + goTransf.name);
		}
	}

	private float GetBiggestPosition(Transform[] myArray)
	{
		float tempPosX = _minCompareX;

		for (int i = 0; i < myArray.Length; i++)
		{
			if (tempPosX < myArray[i].localPosition.x)
				tempPosX = myArray[i].localPosition.x;
		}

		return tempPosX;
	}

	private void SetPosition(Transform goTransf, Vector2 myPos)
	{
		goTransf.localPosition = myPos;
	}



}
