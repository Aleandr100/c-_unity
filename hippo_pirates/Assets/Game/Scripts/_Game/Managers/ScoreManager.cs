﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
	// EVENTS
	public delegate void MyEvent(int myScore);
	public static event MyEvent OnSaveScore;

	public delegate void MyEvent2();
	public static event MyEvent2 OnAddSecToTimer;

	// FIELDS
	[SerializeField] private Text _textScore;

	private readonly int _scoreStart = 0;
	private int _score;

	private readonly Dictionary<string, int> _treasures = new Dictionary<string, int>()
	{
		{ "Treasure_0", 150 },
		{ "Treasure_1", 200 },
		{ "Treasure_2", 250 },
		{ "Treasure_3", 300 },
		{ "Treasure_4", 350 },
		{ "Treasure_5", 400 }
	};

	private readonly Dictionary<string, int> _fishes = new Dictionary<string, int>()
	{
		{ "Fish_0", 70 },
		{ "Fish_1", 75 },
		{ "Fish_2", 80 },
		{ "Fish_3", 85 },
		{ "Fish_4", 90 },
		{ "Fish_5", 95 },
		{ "Fish_6", 100 },
		{ "Fish_7", 105 },
		{ "Fish_8", 110 },
		{ "Fish_9", 115 },
		{ "Fish_10", 120 },
		{ "Fish_11", 125 },
	};


	// UNITY
	private void OnEnable()
	{
		InputManager.OnPulledItemOnBoat += CheckItemType;
		GameTimer.OnTimerIsRunOut += SaveScore;
	}

	private void OnDisable()
	{
		InputManager.OnPulledItemOnBoat -= CheckItemType;
		GameTimer.OnTimerIsRunOut -= SaveScore;
	}

	private void Start ()
	{
		
	}



	// METHODS
	private void ResetScore()
	{
		_score = _scoreStart;
		_textScore.text = _score.ToString();
	}

	private void ChangeScore(int myValue)
	{
		_textScore.transform.DOScale(new Vector2(1.5f, 1.5f), 0.5f).OnComplete(RescaleTextScore);

		_score += myValue;
		_textScore.text = _score.ToString();
	}

	private void RescaleTextScore()
	{
		_textScore.transform.DOScale(Vector2.one, 0.5f);
	}



	// INTERFACES
	public void CheckItemType(GameObject go)
	{
		if (go != null)
		{
			// ----------------------------------------------------------
			//
			if (go.CompareTag(Tags.Treasure))
			{
				int myScore = _treasures[go.name];
				ChangeScore(myScore);
			}
			// ----------------------------------------------------------
			//
			else if (go.CompareTag(Tags.Fish))
			{
				int myScore = _fishes[go.name];
				ChangeScore(myScore);
			}
			// ----------------------------------------------------------
			//
			else if (go.CompareTag(Tags.AlarmClock))
			{
				// event!
				SendEvent_AddSecToGameTimer();
			}
			// ----------------------------------------------------------
			//
			else if (go.CompareTag(Tags.Trash))
			{
				// none
			}
			// ----------------------------------------------------------
		}
	}

	public void SaveScore()
	{
		// event!
		SendEvent_SaveScore(_score);
	}
	


	// SEND EVENTS
	private void SendEvent_AddSecToGameTimer()
	{
		if (OnAddSecToTimer != null)
			{ OnAddSecToTimer(); }
	}

	private void SendEvent_SaveScore(int myScore)
	{
		if (OnSaveScore != null)
			{ OnSaveScore(myScore); }
	}



}
