﻿using UnityEngine;
using UnityEditor;

namespace PromoPlugin
{

    [CanEditMultipleObjects]
    [CustomEditor ( typeof ( PromoUI ) )]
    public class PromoUIEditor :Editor
    {

        PromoUI example
        {
            get
            {
                return target as PromoUI;
            }
        }

        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI ( );
            if (GUILayout.Button ( "Set new sizes" ))
            {
                example.InitSettings ( );
            }

        }

    }
}