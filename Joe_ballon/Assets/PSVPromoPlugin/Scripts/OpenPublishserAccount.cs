﻿using UnityEngine;
using System.Collections;

namespace PromoPlugin
{
    public class OpenPublishserAccount :MonoBehaviour
    {

        const string
            android_market_url = "market://search?q=pub:",
            ios_market_url = "itms-apps://itunes.apple.com/developer/",
            wp_market_url = "ms-windows-store:search?publisher=";



        public void Action ()
        {
            Application.OpenURL ( GetURL ( PromoModule.Instance.GetPublisher ( ) ) );
            PSV_Prototype.AnalyticsManager.LogEvent ( PSV_Prototype.AnalyticsEvents.OpenPub );
        }


        string GetURL (string acc_alias)
        {
#if UNITY_ANDROID
            return android_market_url + acc_alias;
#elif UNITY_IPHONE
        return ios_market_url  + acc_alias;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
        return wp_market_url  + acc_alias;
#endif
        }

    }
}