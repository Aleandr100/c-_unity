﻿using UnityEngine;
using System.Collections;

public class InputBase : MonoBehaviour {
	protected bool focus = false;

    protected int TouchId;

    public bool IsFocused () {
		return focus;
	}

    public void SetTouchId (int id) {
		TouchId = id;
	}

    public int GetTouchId () {
		return TouchId;
	}

    virtual public bool OnTouchDown (Vector2 point) {
		return false;
	}
	
	virtual public bool OnTouchUp (Vector2 point) {
		return false;
	}

	virtual public bool OnTouchMove (Vector2 point) {
		return false;
	}
	
	virtual public bool OnTouchExit (Vector2 point) {
		return false;
	}
}
