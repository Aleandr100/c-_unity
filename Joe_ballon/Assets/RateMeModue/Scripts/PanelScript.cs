﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace RateMePlugin
{
    [RequireComponent ( typeof ( Image ) )]
    public class PanelScript :MonoBehaviour
    {
        private bool
            //dialogue_shown = false,
            animate = false;

        private Image
            img;

        private GameObject
            child;

        public float
            anim_speed = 0.05f;

        private float
            target_alpha = 100 / 255f;

        public void OnEnable ()
        {
            ShowBackground ( );
        }

        public void Awake ()
        {
            img = GetComponent<Image> ( );
            Transform t = transform.GetChild ( 0 );
            if (t != null)
            {
                child = t.gameObject;
            }
            else
            {
                Debug.LogError ( "RateMe dialogue: You are missing Dialogue window in Panel" );
            }
        }

        void ShowBackground ()
        {
            SetAlpha ( -img.color.a );
            animate = true;
        }



        // Update is called once per frame
        void Update ()
        {
            if (animate)
            {
                float step = /*(dialogue_shown ? 1: -1) * */ Time.unscaledDeltaTime;
                float res = SetAlpha ( step );
                if (res == target_alpha /*|| res == 0*/)
                {
                    animate = false;
                    //if (!dialogue_shown)
                    //{
                    //    this.gameObject.SetActive ( false );
                    //}
                }
            }
        }

        float SetAlpha (float val)
        {
            Color col = img.color;
            col.a += val;
            if (col.a >= target_alpha)
                col.a = target_alpha;
            //if (col.a < 0.05f)
            //    col.a = 0;
            img.color = col;
            return col.a;
        }

    }
}
