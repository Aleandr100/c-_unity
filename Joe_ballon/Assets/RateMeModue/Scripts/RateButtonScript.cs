﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using PromoPlugin;


namespace RateMePlugin
{
    [RequireComponent ( typeof ( Button ) )]
    public class RateButtonScript :MonoBehaviour, IPointerClickHandler
    {
        public delegate void Callback (bool positive);
        public static event Callback OnRateClicked;

        private bool
            positive = false;

        private Button
            btn;

        private string
            android_bundle_id = "",
            ios_bundle_id = "",
            wp_bundle_id = "",
            android_makret_app_url = "market://details?id=",
            ios_market_app_url = "itms-apps://itunes.apple.com/app/",
            wp_market_app_url = "ms-windows-store:navigate?appid=";


        public void Awake ()
        {
            btn = GetComponent<Button> ( );
#if UNITY_ANDROID
            android_bundle_id = Application.bundleIdentifier;
#endif
        }

        public void OnEnable ()
        {
            btn.interactable = false;
            StarScript.OnStarClicked += OnStarClicked;
        }

        public void OnDisable ()
        {
            StarScript.OnStarClicked -= OnStarClicked;
        }

        void OnStarClicked (bool _positive, float x)
        {
            btn.interactable = true;
            this.positive = _positive;
        }




        public void OnPointerClick (PointerEventData eventData)
        {
            if (btn.interactable)
            {
                if (positive)
                {
                    RateApplication ( );
                }
                else
                {
                    RateMeModule.Instance.ShowDialogue ( false );
                }
                if (OnRateClicked != null)
                {
                    OnRateClicked ( positive );
                }
            }
        }


        void RateApplication ()
        {
            string app_alias = GetAppAlias ( );
            if (!string.IsNullOrEmpty ( app_alias ))
            {

                string url = GetAppMarketURL ( app_alias );
                Debug.Log ( "RateMeDialogue: open URL=" + url );
                Application.OpenURL ( url );
                if (GoogleAnalytics.Instance != null)
                {
                    GoogleAnalytics.Instance.LogEvent ( "OpenURL " + url, "RateMeDialogue" );
                }
                else
                {
                    Debug.Log ( "RateMeDialogue: GoogleAnalytics not initialised" );
                }
            }
            else
            {
                Debug.LogError ( "RateMeDialogue: package_name is empty" );
            }
        }



        string GetAppAlias ()
        {
            string res = "";
#if UNITY_ANDROID
            res = android_bundle_id;
#elif UNITY_IPHONE
            res = ios_bundle_id;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            res = wp_bundle_id;
#endif
            return res;
        }

        public string GetAppMarketURL (string app_alias)
        {
            string res = "";
#if UNITY_ANDROID
            res = android_makret_app_url + app_alias;
#elif UNITY_IPHONE
            res = ios_market_app_url + app_alias;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            res = wp_market_app_url + app_alias;
#endif
            return res;
        }
    }
}
