﻿#define JAR_RESOLVER_V_1_2

#if JAR_RESOLVER_V_1_2

using System;
using System.Collections.Generic;
using UnityEditor;


[InitializeOnLoad]
public class SampleDependencies :AssetPostprocessor
{
#if UNITY_ANDROID
    /// <summary>Instance of the PlayServicesSupport resolver</summary>
    public static object svcSupport;
#endif  // UNITY_ANDROID

    /// Initializes static members of the class.
    static SampleDependencies ()
    {

        //
        //
        // NOTE:
        //
        //       UNCOMMENT THIS CALL TO MAKE THE DEPENDENCIES BE REGISTERED.
        //   THIS FILE IS ONLY A SAMPLE!!
        //
        RegisterDependencies ( );
        //
    }



    public static void RegisterDependencies ()
    {
#if UNITY_ANDROID
        RegisterAndroidDependencies ( );
#elif UNITY_IOS
        RegisterIOSDependencies();
#endif
    }


    public static void RegisterAndroidDependencies ()
    {

        Type playServicesSupport = Google.VersionHandler.FindClass (
          "Google.JarResolver", "Google.JarResolver.PlayServicesSupport" );
        if (playServicesSupport == null)
        {
            return;
        }
        svcSupport = svcSupport ?? Google.VersionHandler.InvokeStaticMethod (
          playServicesSupport, "CreateInstance",
          new object [] {
          "GooglePlayGames",
          EditorPrefs.GetString("AndroidSdkRoot"),
          "ProjectSettings"
          } );

        Google.VersionHandler.InvokeInstanceMethod (
          svcSupport, "DependOn",
          new object [] {
              "com.google.android.gms",
              "play-services-basement",
              "LATEST"
          },
          namedArgs: new Dictionary<string, object> ( ) {
              {
                  "packageIds",
                  new string[] { "extra-google-m2repository" }
              }
        } );


        Google.VersionHandler.InvokeInstanceMethod (
          svcSupport, "DependOn",
          new object [] {
              "com.google.android.gms",
              "play-services-ads",
              "LATEST"
          },
          namedArgs: new Dictionary<string, object> ( ) {
          {"packageIds", new string[] { "extra-google-m2repository" } }
        } );

        Google.VersionHandler.InvokeInstanceMethod (
            svcSupport, "DependOn",
            new object [] {
                "com.android.support",
                "appcompat-v7",
                //"support-v4",
                "LATEST"/*"23.1+"*/
            },
            namedArgs: new Dictionary<string, object> ( ) {
                    {
                        "packageIds",
                        new string[] { "extra-android-m2repository" }
                    }
                }
            );
    }

    public static void RegisterIOSDependencies ()
    {

        Type iosResolver = Google.VersionHandler.FindClass (
            "Google.IOSResolver", "Google.IOSResolver" );
        if (iosResolver == null)
        {
            return;
        }


        Google.VersionHandler.InvokeStaticMethod (
          iosResolver, "AddPod",
          new object [] { "GooglePlayGames" },
          namedArgs: new Dictionary<string, object> ( ) {
      { "version", "5.0+" },
      { "bitcodeEnabled", false },
          } );
    }

    private static void OnPostprocessAllAssets (
        string [] importedAssets, string [] deletedAssets,
        string [] movedAssets, string [] movedFromPath)
    {
        foreach (string asset in importedAssets)
        {
            if (asset.Contains ( "IOSResolver" ) ||
              asset.Contains ( "JarResolver" ))
            {
                RegisterDependencies ( );
                break;
            }
        }
    }
}

#else
using Google.JarResolver;
using UnityEditor;


[InitializeOnLoad]
public static class NeatPlugDependencies
{

    private static readonly string PluginName = "NeatPlug";
    public static PlayServicesSupport svcSupport;

    static NeatPlugDependencies()
    {

        svcSupport = PlayServicesSupport.CreateInstance(
                                             PluginName,
                                             EditorPrefs.GetString("AndroidSdkRoot"),
                                             "ProjectSettings");
        RegisterDependencies();
    }

    public static void RegisterDependencies()
    {
        svcSupport.DependOn ( "com.google.android.gms", "play-services-basement", "LATEST" );
        svcSupport.DependOn ( "com.google.android.gms", "play-services-ads", "LATEST" );
        svcSupport.DependOn ( "com.android.support", "appcompat-v7", "23.1.0+" );
    }

}
#endif