﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour {

    public Vector3 cameraOrigin;
    public Vector3 strength;
    
    public float decay = 0.8f;
    public float shakeTime = 1;

    private float shakeTimeDefault = 1;
    private Vector3 strengthDefault;

    private bool _isShaking = false;
    public bool isShaking { get { return _isShaking; } set { _isShaking = value; } }

    void Start() {
        strengthDefault = strength;
        shakeTimeDefault = shakeTime;
    }

    public void ShakingStart() {
        isShaking = true;
    }

    void Update () {
        if (isShaking == true) {
            if (shakeTime > 0) {
                shakeTime -= Time.deltaTime;
                float xPos, yPos, zPos;

                xPos = cameraOrigin.x + Random.Range(-strength.x, strength.x);
                yPos = cameraOrigin.y + Random.Range(-strength.y, strength.y);
                zPos = cameraOrigin.z + Random.Range(-strength.z, strength.z);

                Camera.main.transform.position = new Vector3(xPos, yPos, zPos);

                strength *= decay;
            } else {
                shakeTime = shakeTimeDefault;
                strength = strengthDefault;

                Camera.main.transform.position = cameraOrigin;
                isShaking = false;
            }
        }
    }
}
