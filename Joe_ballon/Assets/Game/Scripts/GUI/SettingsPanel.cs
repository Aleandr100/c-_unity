﻿using UnityEngine;
using DG.Tweening;

public class SettingsPanel :MonoBehaviour
{

    public delegate void Callback (bool is_shown);
    public static event Callback OnSettingsShown;

    private bool
        pannel_shown = false;
    public float
        anim_duration = 0.2f;
    public Ease
        easing = Ease.InCubic;


    public RectTransform
        pannel;

    public GameObject
        blocker;

    public GameObject
        _promoUI;


    public void OnEnable ()
    {
        ShowPannel ( false, false );
        CanvasBlocker.OnClick += OnBlockerClick;
    }

    public void OnDisable ()
    {
        CanvasBlocker.OnClick -= OnBlockerClick;
    }


    void OnBlockerClick ()
    {
        //hide pannel
        _promoUI.SetActive(true);
        ShowPannel ( false );
    }


    void ShowPannel (bool param, bool use_tween = true)
    {
        pannel_shown = param;
        if (use_tween)
        {
            if (pannel_shown)
            {
                ActivatePanelObjects ( pannel_shown );
            }
            pannel.DOScale ( pannel_shown ? 1 : 0, anim_duration ).SetEase ( easing ).OnComplete ( () =>
               {
                   if (!pannel_shown)
                   {
                       ActivatePanelObjects ( pannel_shown );
                   }
               } );
        }
        else
        {
            pannel.localScale = pannel_shown ? Vector3.one : Vector3.zero;
            ActivatePanelObjects ( pannel_shown );
        }

        if (OnSettingsShown != null)
        {
            OnSettingsShown ( param );
        }
    }


    void ActivatePanelObjects (bool param)
    {
        pannel.gameObject.SetActive ( param );
        blocker.SetActive ( param );
    }


    public void OpenSettingsPannel ()
    {
        _promoUI.SetActive(false);
        //Debug.Log ( "activate blocker and scale pannel" );
        ShowPannel ( !pannel_shown );
    }

}
