﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MultiButtonController : InputBase
{
	public enum Button {
		MoveUp,
		Attack,
	}

	public Button type;
	public Sprite[] sprites;

    public BoxCollider2D collider { get; private set; }

    public bool is_enabled = true;

    private bool exit = false;

	private SpriteRenderer button_renderer;

    void Awake() {
        collider = GetComponent<BoxCollider2D>();
        button_renderer = gameObject.GetComponent<SpriteRenderer>();
	}

	public void OnButtonExit() {
		OnButtonUp();
	}

	public void OnButtonPress() {
        /**/
    }

    public void ButtonDown() {
        if (sprites.Length > 0) {
            button_renderer.sprite = sprites [1];
	    } else {
            print("WARNING : Button " + name + " is missing sprites");
		}
	}

	public void ButtonUp() {
        if (sprites.Length > 0) {
            button_renderer.sprite = sprites[0];
        } else {
            print("WARNING : Button " + name + " is missing sprites");
        }
    }

    public void OnButtonUp() {
        if (type == Button.MoveUp) {
            if (!PlaygroundController.Instance.player.isInPortal && PlaygroundController.Instance.player.isMovingUp) {
                PlaygroundController.Instance.player.isBig = false;
            }
            PlaygroundController.Instance.player.isForcingUp = false;
            PlaygroundController.Instance.player.isMovingUp = false;
            PlaygroundController.Instance.player.isFlameActive = false;
        }
    }

    public void MoveDownButon() {
        PlaygroundController.Instance.player.isForcingUp = true;
        PlaygroundController.Instance.player.Inflation = true;
        PlaygroundController.Instance.player.flame.SetActive(true);
        PlaygroundController.Instance.isForcingUp = true;
    }

	public void OnButtonDown() {
        switch (type) {
            case Button.MoveUp:
                MoveDownButon();
                break;  
            case Button.Attack:
                PlaygroundController.Instance.player.Attack();
                break;
        }
    }

	override public bool OnTouchDown(Vector2 point) {        
        focus = true;
		exit = false;
		ButtonDown();
		OnButtonDown();

		return true;
	}
	
	override public bool OnTouchUp(Vector2 point) {

        if (type == Button.MoveUp) {
            PlaygroundController.Instance.player.Inflation = false;
            PlaygroundController.Instance.player.flame.SetActive(false);
            PlaygroundController.Instance.isForcingUp = false;
        }
        
        if (focus) {
			focus = false;
			exit = false;
			ButtonUp();
		}

		OnButtonUp();
		return true;
	}
	
	override public bool OnTouchMove(Vector2 point) {
        if (focus) {
			if (gameObject.GetComponent<BoxCollider2D>().OverlapPoint (point)) {
				ButtonDown ();
			} else {
				ButtonUp ();
			}
		}
		if (exit) {
			OnTouchDown (point);
		}
		OnButtonDown();
		return true;
	}
	
	override public bool OnTouchExit(Vector2 point) {
		if (focus) {
			exit = true;
		}
		ButtonUp ();
		OnButtonExit ();
		return true;
	}
}
