﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ScoreController : MonoBehaviour {
    public static ScoreController Instance;

    private const float MIN_GAS_TO_SPAWN = 0.4f;
    private const float MAX_GAS = 1;
	private const float BONUS_GAS = 0.45f;
    private const float FIRE_GAS_COST = 0.00026f; //0.0011f;
    private const float TIME_MULTYPLYER = 50;
    public const float BALLON_PROGRESS = 5.82f;

    private Vector2 addScoreScale = new Vector2(1.4f,1.4f);
    private float stopBallonPosX = 2.75f;

    private int _availableGold;     
    private float ballonPosX;
    private float _distance;

    private bool _isPulsation;
    private bool isPunchScale;
    private bool _fuelPunchScale;
    private bool _enabledSmoke;
    private bool isScoreShowed;
    private string level;

    public Transform fuelProgressBar;
    public GameObject buyGasBtn;

    public Transform fuelProgress;
    public Transform pathProgress;
    public Transform ballonMark;

    public SpriteRenderer fadeScreen;

	public TextMesh diamondsText;
    public TextMesh bigDiamondsText;
    public TextMesh diamondsTextBG;

	public TextMesh scoreText;

    public float gasAmount{get;set;} 

	public bool isGasNeeded{get;set;}
    public bool isGasGenerated{ get;set;}

    public float score { get; set; }
    public int diamonds {get;set;}
    public int bigDiamonds { get; set; }

    public int goldLoose { get; set; }    
    public int availableGold { get { return _availableGold; } set { _availableGold = value; } }
    public bool fuelPunchScale { get { return _fuelPunchScale; } set { _fuelPunchScale = value; } }
    public float distance { get { return _distance; } set { _distance = value; } }
    public bool isPulsation { get { return _isPulsation; } set { _isPulsation = value; } }
    public bool enabledSmoke { get { return _enabledSmoke; } set { _enabledSmoke = value; } }

    void Awake() {
        Instance = this;
    }    

    void Update() {
        if (isPulsation) {
            if (isPunchScale) {
                isPunchScale = false;
                buyGasBtn.transform.DOPunchScale(new Vector3(-0.3f, -0.3f, -0.3f), 0.7f).OnComplete(delegate {
                    DOVirtual.DelayedCall(0.4f, delegate { isPunchScale = true; });
                });
            }
        }
    }

    public void GetDamage(float amount) {
        int dm_old = diamonds;
        diamonds = Mathf.Clamp(diamonds-30,0,diamonds);
        goldLoose += Mathf.Abs(dm_old-diamonds);
    }

    public void AddScore(int amount) {
		diamonds+=amount;
		diamondsText.transform.DOScale(addScoreScale,0.1f).SetEase(Ease.Linear).OnComplete(delegate {
			diamondsText.transform.DOScale(Vector2.one, 0.1f);
		});
		diamondsTextBG.transform.DOScale(addScoreScale,0.1f).SetEase(Ease.Linear).OnComplete(delegate {
			diamondsTextBG.transform.DOScale(Vector2.one, 0.1f);
		});
	}

    public void AddDiamonds(int amount) {
        bigDiamonds += amount;
        bigDiamondsText.transform.DOScale(addScoreScale, 0.1f).SetEase(Ease.Linear).OnComplete(delegate {
            bigDiamondsText.transform.DOScale(Vector2.one, 0.1f);
        });
    }

    public void AddGas() {
        gasAmount = Mathf.Clamp(gasAmount+BONUS_GAS,0,MAX_GAS);
        isGasGenerated = false;
    }

    private void DecreeseGasProp() {
		gasAmount = Mathf.Clamp (gasAmount - FIRE_GAS_COST,0,MAX_GAS);
    }

    private void DecreeseGasFire() {
		gasAmount = Mathf.Clamp (gasAmount - (FIRE_GAS_COST*Time.deltaTime*TIME_MULTYPLYER),0,MAX_GAS);
    }

    public void UpdateScores() {   
		diamondsText.text = diamonds.ToString();
        bigDiamondsText.text = bigDiamonds.ToString();
        diamondsTextBG.text = diamonds.ToString();
        distance = score * 0.1f;
        //*****************************Debug***************************************
        //scoreText.text = ((int)distance).ToString();
        //*****************************Debug***************************************
        pathProgress.GetComponent<Image>().fillAmount = distance / (PlaygroundController.Instance.fullDistance);
        ballonPosX = pathProgress.GetComponent<Image>().fillAmount * BALLON_PROGRESS;
        if (ballonMark.position.x < stopBallonPosX) {
            ballonMark.position = new Vector3(ballonPosX - 2.77f, ballonMark.position.y, 0f);
        } else {
            ballonMark.position = new Vector2(stopBallonPosX, ballonMark.position.y);
        }       
    }

    private void UpdateGas() {
		if(PlayerController.Instance.isInPortal) {
			return;
		}
		if (gasAmount > 0) {
		    if(!PlayerController.Instance.isHasTouchedGround) {
                DecreeseGasProp();
		    }
		    if(PlayerController.Instance.isMovingUp) {
               DecreeseGasFire();
		    }
            fuelProgress.GetComponent<Image>().fillAmount = gasAmount / MAX_GAS;
            if (fuelProgress.GetComponent<Image>().fillAmount < 0.1f) {
                PlayerController.Instance.isSmokeActive = true;
                if (enabledSmoke) {
                    PlaygroundController.Instance.smoke.gameObject.SetActive(true);
                    PlaygroundController.Instance.player.isFlameActive = false;
                }                
            } else {
                PlayerController.Instance.isSmokeActive = false;
                PlaygroundController.Instance.smoke.gameObject.SetActive(false);
            }
        } else {            
            if (isEndinglevel() || isGasCollected()) {
                return;
            }
            if (gasAmount <= 0 && (diamonds >= PlaygroundController.Instance.gasPrise)) {
                if (fuelPunchScale) {
                    PlaygroundController.Instance.smoke.gameObject.SetActive(false);
                    PlaygroundController.Instance.flame.gameObject.SetActive(false);
                    fuelProgressBar.transform.DOPunchScale(new Vector3(-0.3f, -0.3f, -0.3f), 0.7f).OnComplete(delegate {
                        buyGasBtn.SetActive(true);                        
                        PlaygroundController.Instance.gameState = PlaygroundController.GameState.Paused;
                        PlaygroundController.Instance.PauseTween();
                        PlayerController.Instance.DrawRopes();
                        Time.timeScale = 0;
                        PlaygroundController.Instance.player.isBig = false;
                        PlayerController.Instance.isMovingUp = false;
                        PlaygroundController.Instance.speed = 0;
                        PlaygroundController.Instance.smoke.gameObject.SetActive(false);
                        DOTween.defaultTimeScaleIndependent = true;
                        PlaygroundController.Instance.SoundPause();
                        fadeScreen.gameObject.SetActive(true);

                        if (SaveManager.GetTipsStatus()) {
                            if (!TutorialController.Instance.isGasTutorialShowed && level.Equals("Greece_level")) {
                                if (SaveManager.GetGasTutorial()) {
                                    buyGasBtn.GetComponent<Collider2D>().enabled = false;
                                    TutorialController.Instance.EnabledGasTutorial();
                                }
                            }
                        } else {
                            ScoreController.Instance.isPulsation = true;
                        }
                        if (!SaveManager.GetGasTutorial()) {
                            ScoreController.Instance.isPulsation = true;
                        }
                        PlaygroundController.Instance.openPausePannel_btn.gameObject.SetActive(false);
                        PlaygroundController.Instance.forceUp_btn.gameObject.SetActive(false);
                        PlaygroundController.Instance.klaxon_btn.gameObject.SetActive(false);
                    });
                    fuelPunchScale = false;
                }
            } else {
                if (!isScoreShowed) {                    
                    Time.timeScale = 0;
                    DOTween.defaultTimeScaleIndependent = true;
                    PlaygroundController.Instance.StopAllMobsSounds();

                    PlaygroundController.Instance.forceUp_btn.gameObject.GetComponent<Collider2D>().enabled = false;
                    PlaygroundController.Instance.klaxon_btn.gameObject.GetComponent<Collider2D>().enabled = false;

                    PlaygroundController.Instance.InitScore();
                    isScoreShowed = true;
                }                
            }
        }
        if (SaveData.currentLevelName != "Greece_level") {
            if ((gasAmount < MIN_GAS_TO_SPAWN) && (diamonds < PlaygroundController.Instance.gasPrise) && !isGasGenerated) {
                isGasNeeded = true; isGasGenerated = true;
            }
        }        
    }

    private bool isGasCollected() {
        return PlaygroundController.Instance.activeCells[0].isGasCollected;
    }

    private bool isEndinglevel() {
        return PlaygroundController.Instance.isEndinglevel;
    }

    public void Init() {
        level = SaveData.currentLevelName;
        distance = 0;
        isPunchScale = true;
        fuelPunchScale = true;
        isPulsation = false;
        enabledSmoke = true;
        isScoreShowed = false;
        ballonPosX = ballonMark.position.x;
        diamonds = 0;
        bigDiamonds = 0;
        goldLoose = 0;
        availableGold = 0;
        score = 0;
        gasAmount = MAX_GAS;
        isGasNeeded = false;
        isGasGenerated = false;
        diamondsText.text = diamonds.ToString();
        bigDiamondsText.text = bigDiamonds.ToString();
        UpdateScores();
        buyGasBtn.SetActive(false);
    }

	public void Process() {
		UpdateGas();
		UpdateScores();
	}

    void OnDestroy() {

        switch (level) {
            case "Greece_level":
                SaveData.collectedGreeceEmeralds = bigDiamonds;
                break;
            case "Egypt_level":
                SaveData.collectedEgyptEmeralds = bigDiamonds;
                break;
            case "Latin_level":
                SaveData.collectedLatinEmeralds = bigDiamonds;
                break;
        }

        SaveData.availableGold = availableGold;
        SaveData.collectedGold = diamonds;
        SaveData.score = score;
        SaveData.goldLoose = goldLoose;
    }
}
