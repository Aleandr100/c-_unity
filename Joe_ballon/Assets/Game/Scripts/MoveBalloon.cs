﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class MoveBalloon : MonoBehaviour {

    private float timeToAnimStart = 12;

    public Animator animator;

    void Update() {
        if (timeToAnimStart > 0) {
            timeToAnimStart -= Time.deltaTime;
        } else {
            animator.SetTrigger("Move");
            timeToAnimStart = 12;
        }
    }

    public void MoveUp() {
        gameObject.transform.DOMoveY(0.49f, 1.8f).SetEase(Ease.Linear);
    }

    public void MoveDown() {
        gameObject.transform.DOMoveY(-0.49f, 1.8f).SetEase(Ease.Linear);
    }
}
