﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using System.Linq;
using System;
using DG.Tweening;

public class TutorialController : MonoBehaviour {
    public static TutorialController Instance;

    public const float TUTORIAL_TIME = 4f;
    public const float TUTORIAL_OFFSET = 6.5f;

    public ButtonController openPausePannel_btn;

    public Collider2D buyGas_btn;

    public MultiButtonController klaxon_btn;
    public MultiButtonController forceUp_btn;

    public GameObject tutorialObject;
    public SpriteRenderer fadeScreen;

    public Canvas[] canvas;
    public Transform hand;

    public GameObject pointerPrefab;

    public Transform[] handsPos;

    public List<Tutorial> tutorials;

    private GameObject pointer;

    private MeshRenderer meshRender;

    private SpriteRenderer magnet;

    private bool[] IsShowTPGsMG;

    private bool _isGasTutorialShowed;
    private bool _isTpTutorialShowed;
    private bool _isMgTutorialShowed;
    private bool _isBallonMoveTutorial;

    private float delay = 0.0f;
    private float lastTutorialTime;

    private int lastTutorial = 0;
    private int nextTutorial = 0;

    private List<CellController> activeCells;

    public bool isGasTutorialShowed { get { return _isGasTutorialShowed; } set { _isGasTutorialShowed = value; } }
    public bool isTpTutorialShowed  { get { return _isTpTutorialShowed;  } set { _isTpTutorialShowed = value;  } }
    public bool isMgTutorialShowed  { get { return _isMgTutorialShowed;  } set { _isMgTutorialShowed = value;  } }
    public bool isBallonMoveTutorial { get { return _isBallonMoveTutorial; } set { _isBallonMoveTutorial = value; } }

    public enum Tutorial
    {
        ButtonUp,
        ButtonAttack,
        Teleport,
        Magnet,
        Gas,
        Fuel,

        None,
    }

    private void Awake() {
		Instance = this;        
    }

    public void Init() {

        isGasTutorialShowed = false;
        isTpTutorialShowed = false;
        isMgTutorialShowed = false;

        isBallonMoveTutorial = false;     

        IsShowTPGsMG = new bool[3];
        for (int i = 0; i < IsShowTPGsMG.Length; i++) {
            IsShowTPGsMG[i] = false;
        }

        lastTutorialTime = Time.time;

        if (!SaveManager.GetControllTutorial()) {
            lastTutorial = 2;
        }

        tutorials = new List<Tutorial>();
        tutorials = Tutorial.GetValues(typeof(Tutorial)).Cast<Tutorial>().ToList();

        activeCells = PlaygroundController.Instance.activeCells;
    }

    private IEnumerator WaitForAudioStop(AudioSource audioSource, int index, bool isUseHand, Tutorial tutorial, Transform ptr = null) {

        if (isUseHand) {
            hand.transform.parent = handsPos[index];
            hand.transform.localPosition = Vector3.zero;
            hand.gameObject.SetActive(true);            
        }
        else {

            print(tutorial.ToString());

            pointer = Instantiate(pointerPrefab, ptr.transform.position, Quaternion.Euler(Vector3.forward * 90)) as GameObject;
            pointer.transform.parent = ptr;
            pointer.transform.localPosition = Vector3.zero;

            if (tutorial.Equals(Tutorial.Teleport)) {

                meshRender.sortingLayerName = "Interface";
                meshRender.sortingOrder = 4;
                
            } else if (tutorial.Equals(Tutorial.Magnet)) {
                magnet.sortingLayerName = "Interface";
                magnet.sortingOrder = 4;
            }
        }

        while (audioSource.isPlaying) {
            yield return null;
        }

        if (isUseHand) {
            hand.transform.parent = tutorialObject.transform;
            hand.transform.localPosition = Vector3.zero;
            hand.gameObject.SetActive(false);

            if (tutorial != Tutorial.Gas) {
                nextTutorial++;
                if (nextTutorial.Equals(2)) {
                    SaveManager.DisableControllTutorial(1);
                }
            }
        }
        else {
            Destroy(pointer);

            if (tutorial.Equals(Tutorial.Teleport)) {

                meshRender.sortingLayerName = "Default";
                meshRender.sortingOrder = 2;
                
            } else if (tutorial.Equals(Tutorial.Magnet)) {
                magnet.sortingLayerName = "Default";
                magnet.sortingOrder = 3;
            }
        }

        if (!tutorial.Equals(Tutorial.Gas)) {
            HideTutorial();
        }
        else {
            ScoreController.Instance.isPulsation = true;
            buyGas_btn.enabled = true;

            forceUp_btn.GetComponent<Collider2D>().enabled = true;
            klaxon_btn.GetComponent<Collider2D>().enabled = true;
        }
    }

    private IEnumerator DelayedCall(float delay, Action callAction) {
        while (delay > 0) {
            delay -= Time.deltaTime;
            yield return null;
        }
        callAction();        
    }

    private void EnabledMgTutorial(int cell, int index) {
        magnet = activeCells[cell].magnetPlaceholder.GetChild(0).GetComponent<SpriteRenderer>();
        Transform pointer = activeCells[cell].tutorials[index];
        ShowTutorial(Tutorial.Magnet, ref delay, pointer);
        SaveManager.DisableMgTutorial(1);
        IsShowTPGsMG[1] = true;
        isMgTutorialShowed = true;
        lastTutorial++;
    }

    private void EnabledTpTutorial(int cell)
    {
        meshRender = activeCells[cell].portalsInPlaceholders[0].GetChild(0).GetComponent<MeshRenderer>();
        Transform pointer = activeCells[cell].tutorials[0];

        ShowTutorial(Tutorial.Teleport, ref delay, pointer);
        SaveManager.DisableTpTutorial(1);
        IsShowTPGsMG[0] = true;
        isTpTutorialShowed = true;
        lastTutorial++;
    }

    public void EnabledGasTutorial() {
        if (!isGasTutorialShowed) {
            ShowTutorial(Tutorial.Gas, ref delay, handsPos[2]);
            isGasTutorialShowed = true;
            lastTutorial++;
        }        
    }

    //***********************************last_changes***********************************
    public void UpdateFuelTutorial()
    {
        if (SaveManager.CheckFuelTutorial())
        {
            for (int index = 0; index < activeCells.Count; index++)
            {
                print("HEEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRREEEEEEEEEEEEEEEEEEE");

                if (activeCells[index].isHasFuelTutorial && activeCells[index].gasTransform.GetChild(0).gameObject.activeSelf)
                {
                    if (activeCells[index].gasTransform.position.x < GetScreen.left + TUTORIAL_OFFSET)
                    {
                        EnabledFuelTutorial(index);
                    }
                }
            }
        }
    }

    private void EnabledFuelTutorial(int index)
    {
        Transform pointer = activeCells[index].gasTransform;
        ShowTutorial(Tutorial.Fuel, ref delay, pointer);
        SaveManager.FuelTutorialOff(1);

    }
    //***********************************last_changes***********************************

    public void UpdateTutorial()
    {
        if (SaveManager.GetTipsStatus())
        {
            

            if (Time.time - lastTutorialTime >= TUTORIAL_TIME)
            {
                

                if (SaveManager.GetControllTutorial())
                {
                    print("XDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
                    if (lastTutorial < tutorials.Count - 5)   //was 4
                    {
                        print("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");

                        Tutorial tutorial = tutorials.ElementAt(lastTutorial);
                        StartCoroutine(DelayedCall(delay, () => 
                        {
                            ShowTutorial(tutorial, ref delay);
                        }));
                        lastTutorial++;
                    }
                }               
            }

            if (!SaveManager.GetControllTutorial()) {
                nextTutorial = 2;
            }

            if ((!isTpTutorialShowed || !isMgTutorialShowed) && nextTutorial.Equals(2)) {

                for (int index = 0; index < activeCells.Count; index++) {
                    if (activeCells[index].tag != "StartCell" && activeCells[index].isHasTutorial) {
                        
                        if (activeCells[index].tutorials.Length.Equals(2)) {
                            if (!IsShowTPGsMG[0].Equals(true)) {                                
                                if (activeCells[index].tutorials[0].position.x < GetScreen.left + TUTORIAL_OFFSET) {
                                    if (SaveManager.GetTpTutorial()) {
                                        EnabledTpTutorial(index);
                                    }                                 
                                }
                            }
                            if (!IsShowTPGsMG[1].Equals(true)) {
                                if (activeCells[index].tutorials[1].position.x < GetScreen.left + TUTORIAL_OFFSET) {
                                    if (SaveManager.GetMgTutorial()) {
                                        EnabledMgTutorial(index, 1);
                                    }                                  
                                }
                            }
                        } else {
                            if (activeCells[index].tutorials[0].position.x < GetScreen.left + TUTORIAL_OFFSET) {
                                if (!activeCells[index].magnetPlaceholder) {
                                    if (!IsShowTPGsMG[0].Equals(true)) {
                                        if (SaveManager.GetTpTutorial()) {
                                            EnabledTpTutorial(index);
                                        }                                    
                                    }
                                } else {
                                    if (!IsShowTPGsMG[1].Equals(true)) {
                                        if (SaveManager.GetMgTutorial()) {
                                            EnabledMgTutorial(index, 0);
                                        }                                      
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if (lastTutorial.Equals(6)) {
                SaveManager.SetTipsStatus("Disable");
                //***************************debug*********************************
                //print("###Disable_tutorial###");
                //***************************debug*********************************
            }
        }
    }

    public void ShowTutorial(Tutorial tutorial, ref float delay, Transform ptr = null)
    {
        print(tutorial.ToString());

        print("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");

        fadeScreen.gameObject.SetActive(true);

        if (!tutorial.Equals(Tutorial.Gas))
        {
            foreach (Canvas item in canvas)
            {
                item.sortingOrder = 2;
            }
        }
       
        PlaygroundController.Instance.SoundPause();
        PlaygroundController.Instance.PauseTween();
        PlaygroundController.Instance.isPaused = true;
        PlayerController.Instance.DrawRopes();
        
        if (AudioSources.Instance.fireAudioSource.isPlaying)
        {
            PlaygroundController.Instance.isForcingUp = false;
            AudioController.StopStream(AudioSources.Instance.fireAudioSource);
        }

        Time.timeScale = 0;
        
        PlaygroundController.Instance.gameState = PlaygroundController.GameState.Paused;

        openPausePannel_btn.gameObject.SetActive(false);

        forceUp_btn.GetComponent<Collider2D>().enabled = false;
        klaxon_btn.GetComponent<Collider2D>().enabled = false;      

        switch (tutorial) {
            case Tutorial.ButtonUp:
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-10", true));
                forceUp_btn.GetComponent<SpriteRenderer>().sortingOrder = 4;
                StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 0, true, Tutorial.None));
                delay += 2.5f;
                break;
            case Tutorial.ButtonAttack:
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-11", true));
                klaxon_btn.GetComponent<SpriteRenderer>().sortingOrder = 4;
                StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 1, true, Tutorial.None));                
                break;
            case Tutorial.Teleport:
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-13", true));
                StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 0, false, Tutorial.Teleport, ptr));
                delay = 0;
                break;
            case Tutorial.Magnet:
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-12", true));
                StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 0, false, Tutorial.Magnet, ptr));
                delay = 0;
                break;
            case Tutorial.Gas:
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-15", true));
                StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 2, true, Tutorial.Gas, ptr));
                break;
            //***********************************last_changes***********************************
            case Tutorial.Fuel:
                /**/
                StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 2, true, Tutorial.Fuel, ptr));
                break;
                //***********************************last_changes***********************************
        }
    }

    public void HideTutorial() {
        fadeScreen.gameObject.SetActive(false);
       
        int layerOffset = 2;
        foreach (Canvas item in canvas) {
            item.sortingOrder = 2 + layerOffset;
            layerOffset += 2;
        }

        Time.timeScale = 1;
        PlaygroundController.Instance.PlayTween();
        PlaygroundController.Instance.isPaused = false;
        PlaygroundController.Instance.SoundPlay();

        forceUp_btn.ButtonUp();
        klaxon_btn.ButtonUp();

        forceUp_btn.GetComponent<SpriteRenderer>().sortingOrder = 0;
        klaxon_btn.GetComponent<SpriteRenderer>().sortingOrder  = 0;

        forceUp_btn.GetComponent<Collider2D>().enabled = true;
        klaxon_btn.GetComponent<Collider2D>().enabled = true;

        PlaygroundController.Instance.gameState = PlaygroundController.GameState.Playing;
		openPausePannel_btn.gameObject.SetActive(true);
    }

    //public void DisableTutorialSound(string name) {
    //    AudioController.StopStream(AudioSources.Instance.mobsSound[name]);
    //}

    public void IncTutorials() {
        lastTutorial++;
        SaveManager.DisableGasTutorial(1);
    }
}
