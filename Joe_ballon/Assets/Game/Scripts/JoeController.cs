﻿using UnityEngine;
using System.Collections;

public class JoeController : MonoBehaviour {
    public Animator animator;

    public void UseKlaxon() {
        animator.SetTrigger("Klaxon");
    }
}
