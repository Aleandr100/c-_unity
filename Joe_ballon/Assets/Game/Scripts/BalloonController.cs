﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BalloonController : MonoBehaviour {

    public Animator thisAnimator;
    public Animator charAnimator;
    public Animator flagAnimator;

    public Transform emeraldPos;

    public List<GameObject> flags;
    public List<LineRenderer> ropes;
    public List<Transform> ropesEdge;
    
    private bool _isUpdateRopes;

    public bool isUpdateRopes {
        get {
            return (_isUpdateRopes ? _isUpdateRopes : false);
        }
        set {
            _isUpdateRopes = value;
        }
    }

    public void MoveUp() {
        this.transform.DOMoveY(0.49f, 1.8f).SetEase(Ease.Linear);
    }

    public void MoveDown() {
        this.transform.DOMoveY(-0.49f, 1.8f).SetEase(Ease.Linear);
    }

    public void MoveForward() {
        flagAnimator.enabled = true;
        gameObject.transform.DOMoveX(-4.22f, 2.8f).SetEase(Ease.Linear).OnComplete(delegate {
            thisAnimator.enabled = true;            
            FinishController.Instance.MoveEmeraldsToTample();
        });
    }

    public void StopAnimator() {
        thisAnimator.enabled = false;
        charAnimator.enabled = false;
        flagAnimator.enabled = false;
    }

    public void drawRopes() {
        for (int i = 0; i < ropes.Count; i++) {
            ropes[i].SetPosition(0, ropesEdge[i].position);
            ropes[i].SetPosition(1, ropesEdge[ropesEdge.Count / 2 + i].position);
        }
           
        int r_index;  float xPos, yPos, lambda; 
        for (int f_index=0; f_index < flags.Count; ++f_index) {
            r_index = (f_index > 1) ? 3 : 0;
            lambda = ((f_index % 2) != 0) ? 0.4f : 1;
            xPos = (ropesEdge[r_index].position.x + (ropesEdge[ropesEdge.Count / 2 + r_index].position.x * lambda)) / (1 + lambda);
            yPos = (ropesEdge[r_index].position.y + (ropesEdge[ropesEdge.Count / 2 + r_index].position.y * lambda)) / (1 + lambda);

            flags[f_index].transform.position = new Vector3(xPos, yPos);
            flags[f_index].SetActive(true);
        }
    }

    public void Init() {
        drawRopes();
        MoveForward();
    } 
}
