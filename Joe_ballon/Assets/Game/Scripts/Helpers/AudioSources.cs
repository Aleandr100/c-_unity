﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioSources : MonoBehaviour {
    public static AudioSources Instance;

    public List<AudioSource> pointAudioSource;

    public Dictionary<string, AudioSource> sounds;
    public Dictionary<string, AudioSource> backSound;
    public Dictionary<string, AudioSource> mobsSound;
    public Dictionary<string, AudioSource> tutorialSound;

    public AudioSource displayAudioSource;
    public AudioSource gasCollectSource;
    public AudioSource punchAudioSource;
    public AudioSource klaxonAudioSource;
    public AudioSource tochGroundAudioSource;
    public AudioSource fireAudioSource;
    public AudioSource fightAudioSource;
    public AudioSource seaAudioSource;
    public AudioSource gullAudioSource;
    public AudioSource desertAudioSource;
    public AudioSource foresAudioSource;
    public AudioSource portal;

    public AudioSource anubisAttackAudioSource;
    public AudioSource anubisDawnAudioSource;
    public AudioSource anubisWalkAudioSource;

    public AudioSource succubusAttackAudioSource;
    public AudioSource succubusDawnAudioSource;
    public AudioSource succubusRunAudioSource;

    public AudioSource centaurAttackAudioSource;
    public AudioSource centaurDawnAudioSource;
    public AudioSource centaurWalkAudioSource;

    public AudioSource mummyAttackAudioSource;
    public AudioSource mummyDawnAudioSource;
    public AudioSource mummyWalkAudioSource;

    public AudioSource mankeyAttackAudioSource;
    public AudioSource mankeyDawnAudioSource;
    public AudioSource mankeyWalkAudioSource;

    public AudioSource golemAttackAudioSource;
    public AudioSource golemDawnAudioSource;
    public AudioSource golemWalkAudioSource;

    public AudioSource scarabAttackAudioSource;
    public AudioSource scarabDawnAudioSource;
    public AudioSource scarabFlyAudioSource;

    public AudioSource waspAttackAudioSource;
    public AudioSource waspDawnAudioSource;
    public AudioSource waspFlyAudioSource;

    public AudioSource harpyAttackAudioSource;
    public AudioSource harpyDawnAudioSource;
    public AudioSource harpyFlyAudioSource;

    public AudioSource fishAttackAudioSource;
    public AudioSource fishDawnAudioSource;
    public AudioSource fishFlyAudioSource;

    public AudioSource dragonAttackAudioSource;
    public AudioSource dragonDawnAudioSource;
    public AudioSource dragonFlyAudioSource;

    public AudioSource batAttackAudioSource;
    public AudioSource batDawnAudioSource;
    public AudioSource batFlyAudioSource;

    public AudioSource greeceTransform;
    public AudioSource pyramidTransform;
    public AudioSource latinTransform;

    public AudioSource explosion;
    public AudioSource bigLock;

    public AudioSource hp_10;
    public AudioSource hp_11;
    public AudioSource hp_12;
    public AudioSource hp_13;
    public AudioSource hp_15;
    public AudioSource hp_16;
    public AudioSource hp_17;
    public AudioSource hp_18;

    public AudioSource hp_01;
    public AudioSource hp_03;
    public AudioSource hp_04;
    public AudioSource hp_07;
    public AudioSource hp_08;
    public AudioSource hp_09;
    public AudioSource hp_14;

    //**************last_changes**************
    public AudioSource sound;
    //**************last_changes**************

    void Awake() {
        Instance = this;
        DontDestroyOnLoad(gameObject);
       
        sounds    = new Dictionary<string, AudioSource>();
        backSound = new Dictionary<string, AudioSource>();
        mobsSound = new Dictionary<string, AudioSource>();

        tutorialSound = new Dictionary<string, AudioSource>();        

        backSound.Add("Sea",    seaAudioSource   );
        backSound.Add("Gulls",  gullAudioSource  );
        backSound.Add("Desert", desertAudioSource);
        backSound.Add("Forest", foresAudioSource );

        backSound.Add("Lock",              foresAudioSource);
        backSound.Add("Explosion",         foresAudioSource);        
        backSound.Add("Greece_transform",  foresAudioSource);
        backSound.Add("Pyramid_transform", foresAudioSource);
        backSound.Add("Latin_transform",   foresAudioSource);

        mobsSound.Add("AnubisAttack", anubisAttackAudioSource);
        mobsSound.Add("AnubisDawn",   anubisDawnAudioSource  );
        mobsSound.Add("AnubisWalk",   anubisWalkAudioSource  );

        mobsSound.Add("SuccubusAttack", succubusAttackAudioSource);
        mobsSound.Add("SuccubusDawn",   succubusDawnAudioSource  );
        mobsSound.Add("SuccubusRun",    succubusRunAudioSource  );

        mobsSound.Add("CentaurAttack", centaurAttackAudioSource);
        mobsSound.Add("CentaurDawn",   centaurDawnAudioSource  );
        mobsSound.Add("CentaurWalk",   centaurWalkAudioSource   );

        mobsSound.Add("MummyAttack", mummyAttackAudioSource);
        mobsSound.Add("MummyDawn",   mummyDawnAudioSource  );
        mobsSound.Add("MummyWalk",   mummyWalkAudioSource  );

        mobsSound.Add("MankeyAttack", mankeyAttackAudioSource);
        mobsSound.Add("MankeyDawn",   mankeyDawnAudioSource  );
        mobsSound.Add("MankeyWalk",   mankeyWalkAudioSource  );

        mobsSound.Add("GolemAttack", golemAttackAudioSource);
        mobsSound.Add("GolemDawn",   golemDawnAudioSource  );
        mobsSound.Add("GolemWalk",   golemWalkAudioSource  );

        mobsSound.Add("ScarabAttack", scarabAttackAudioSource);
        mobsSound.Add("ScarabDawn",   scarabDawnAudioSource  );
        mobsSound.Add("ScarabFly",    scarabFlyAudioSource   );

        mobsSound.Add("WaspAttack", waspAttackAudioSource);
        mobsSound.Add("WaspDawn",   waspDawnAudioSource);
        mobsSound.Add("WaspFly",    waspFlyAudioSource);

        mobsSound.Add("HarpyAttack", harpyAttackAudioSource);
        mobsSound.Add("HarpyDawn",   harpyDawnAudioSource  );
        mobsSound.Add("HarpyFly",    harpyFlyAudioSource   );

        mobsSound.Add("FishAttack", fishAttackAudioSource);
        mobsSound.Add("FishDawn",   fishDawnAudioSource  );
        mobsSound.Add("FishFly",    fishFlyAudioSource   );

        mobsSound.Add("DragonAttack", dragonAttackAudioSource);
        mobsSound.Add("DragonDawn",   dragonDawnAudioSource  );
        mobsSound.Add("DragonFly",    dragonFlyAudioSource   );

        mobsSound.Add("BatAttack", batAttackAudioSource);
        mobsSound.Add("BatDawn",   batDawnAudioSource  );
        mobsSound.Add("BatFly",    batFlyAudioSource   );

        tutorialSound.Add("hp_10", hp_10);
        tutorialSound.Add("hp_11", hp_11);
        tutorialSound.Add("hp_12", hp_12);
        tutorialSound.Add("hp_13", hp_13);
        tutorialSound.Add("hp_15", hp_15);
        tutorialSound.Add("hp_16", hp_16);
        tutorialSound.Add("hp_17", hp_17);
        tutorialSound.Add("hp_18", hp_18);     

        sounds.Add("hp_01", hp_01);
        sounds.Add("hp_03", hp_03);
        sounds.Add("hp_04", hp_04);
        sounds.Add("hp_07", hp_07);
        sounds.Add("hp_08", hp_08);
        sounds.Add("hp_09", hp_09);
        sounds.Add("hp_14", hp_14);        
    }

    public AudioSource pointAudoiSource {
        get {
            foreach (AudioSource source in pointAudioSource) {
                if (!source.isPlaying) {
                    return source;
                }
            }
            return pointAudioSource[0];
        }
    }
}
