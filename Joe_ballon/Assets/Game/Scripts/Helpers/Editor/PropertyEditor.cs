﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CellController))]
public class PropertyEditor : Editor {

    CellController m_Instance;
    PropertyField[] m_fields;

    public void OnEnable() {
        m_Instance = target as CellController;
        m_fields = ExposeProperties.GetProperties(m_Instance);
    }

    public override void OnInspectorGUI() {
        if (m_Instance == null)
            return;
        this.DrawDefaultInspector();
        ExposeProperties.Expose(m_fields);
    }
}