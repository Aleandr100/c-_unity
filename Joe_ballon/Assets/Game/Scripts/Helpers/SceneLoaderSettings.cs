﻿#define NEAT_PLUG
//#define GOOGLE_ADS

using UnityEngine;
using System.Collections.Generic;
#if GOOGLE_ADS
using GoogleMobileAds.Api;
#endif

namespace PSV_Prototype {

    public enum Scenes {  //List here all scene names included in the build   (permanent scenes are InitScene, PSV_Splash (or other), Push)
        InitScene,
        PSV_Splash,
        //Melnitsa_Splash,
        Push,
        StartMenu,
        LevelMenu,
        GreeceLevel,
        EgyptLevel,        
        AmericaLevel,
        Finish,
        Progress,
        RateUs,
        Sample,
        None
    }

    public static class SceneLoaderSettings {

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1

#if GOOGLE_ADS
    private AdPosition
        small_banner_default_pos = AdPosition.Bottom;        //overrides AdmobAdPosition

    private Dictionary<Scenes, AdPosition>
        small_banner_override = new Dictionary<Scenes, AdPosition> ( ) { //change here ad position for certain scenes, scenes not listed here will use default_position
            //{ Scenes.MainMenu, AdPosition.BottomRight },
        };
#endif
#if NEAT_PLUG
        public static AdmobAd.AdLayout
            small_banner_default_pos = AdmobAd.AdLayout.Bottom_Centered;        //overrides AdmobAdPosition

        public static Dictionary<Scenes, AdmobAd.AdLayout>
            small_banner_override = new Dictionary<Scenes, AdmobAd.AdLayout> ( ) { //change here ad position for certain scenes, scenes not listed here will use default_position
                //{ Scenes.MainMenu, AdmobAd.AdLayout.BottomRight },
            };
#endif

        public static List<Scenes>
            not_allowed_small_banner = new List<Scenes> ( ) {   //list here scenes that shouldn't show ad 
                Scenes.PSV_Splash,
                Scenes.Push,
                Scenes.StartMenu,
            };

        public static List<Scenes>
            not_allowed_interstitial = new List<Scenes> ( ) {   //list here scenes which wouldn't show big banner if we will leave them
                Scenes.InitScene,
                Scenes.PSV_Splash,
                Scenes.Push,
            };

#endif

        public static List<Scenes>
            rate_me_after_end = new List<Scenes> ( ) {   //list here scenes which will call rate_me dialogue on end
                Scenes.Finish,
            };
    }
}
