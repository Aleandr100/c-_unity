﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV_Prototype;

public static class SaveData : object {
    static int _collectedGreeceEmeralds = 0;
    static int _collectedEgyptEmeralds = 0;
    static int _collectedLatinEmeralds = 0;

    static bool _isActiveExpEgypt = false;
    static bool _isActiveExpLatin = false;

    static int _collectedGold     = 0;
    static int _availableGold     = 0;
    static int _goldLoose         = 0;
    static float _score           = 0;

    static bool _isSoundEnabled = true;

    static Scenes _saveScene = Scenes.None;

    static string _currentLevelName = "None";

    public static Scenes saveScene {
        get {
            return _saveScene;
        }
        set {
            _saveScene = value;
        }
    }

    public static bool isActiveExpEgypt {
        get {
            return _isActiveExpEgypt;
        }
        set {
            _isActiveExpEgypt = value;
        }
    }

    public static bool isActiveExpLatin {
        get {
            return _isActiveExpLatin;
        }
        set {
            _isActiveExpLatin = value;
        }
    }

    public static int collectedGreeceEmeralds {
        get {
            return _collectedGreeceEmeralds;
        }
        set {
            _collectedGreeceEmeralds = value;
        }
    }

    public static int collectedEgyptEmeralds {
        get {
            return _collectedEgyptEmeralds;
        }
        set {
            _collectedEgyptEmeralds = value;
        }
    }

    public static int collectedLatinEmeralds {
        get {
            return _collectedLatinEmeralds;
        }
        set {
            _collectedLatinEmeralds = value;
        }
    }

    public static int availableGold {
        get {
            return _availableGold;
        }
        set {
            _availableGold = value;
        }
    }

    public static int collectedGold {
        get {
            return _collectedGold;
        }
        set {
            _collectedGold = value;
        }
    }

    public static string currentLevelName {
        get {
            return _currentLevelName;
        }
        set {
            _currentLevelName = value;
        }
    }

    public static int goldLoose {
        get {
            return _goldLoose;
        }
        set {
            _goldLoose = value;
        }
    }

    public static float score {
        get {
            return _score;
        }
        set {
            _score = value;
        }
    }

    public static bool isSoundEnabled {
        get {
            return _isSoundEnabled;
        }
        set {
            _isSoundEnabled = value;
        }
    }
}
