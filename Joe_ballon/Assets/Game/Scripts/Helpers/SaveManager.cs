﻿using UnityEngine;
using System.Collections;

public static class SaveManager : object {
    static int countOfGreeceEmeralds  = 0;
    static int countOfEgyptEmeralds   = 0;
    static int countOfAmericaEmeralds = 0;

    static string isGreeceLevel = "Opened";
    static string isEgyptLevel  = "Closed";
    static string isLatinLevel  = "Closed";

    static string currentLevelName = "None";

    static string isGreeceComplete = "Incomplete";
    static string isEgyptComplete  = "Incomplete";
    static string isLatinComplete  = "Incomplete";

    static int isShowedLevelTutorial = 0;
    static int isRestartedTutorial   = 0;

    static int isShowedControllTutorial = 0;

    static int isShowTpTutorial = 0;
    static int isShowMgTutorial = 0;

    static int isMovedBallonTutorial = 0;

    static int isShowGasTutorial = 0;

    //*****************last_changes*****************
    static int isShowFuelTutorial = 0;
    //*****************last_changes*****************

    static string isShowTips = "Enabled";

    public static void CreateAndLoadData() {
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.HasKey("Greece level"))
        {
            LoadData();
        }
        else
        {
            CreateDate();
        }
    }

    public static void CreateDate()
    {
        Debug.Log("Create");
             
        PlayerPrefs.SetInt("Count of greece emeralds",  0);
        PlayerPrefs.SetInt("Count of egypt emeralds",   0);
        PlayerPrefs.SetInt("Count of america emeralds", 0);

        PlayerPrefs.SetString("Greece level", "Opened");
        PlayerPrefs.SetString("Egypt level",  "Closed");
        PlayerPrefs.SetString("Latin level",  "Closed");

        PlayerPrefs.SetString("Current level name", "Greece_level");

        PlayerPrefs.SetString("Status of greece", "Incomplete");
        PlayerPrefs.SetString("Status of egypt",  "Incomplete");
        PlayerPrefs.SetString("Status of latin",  "Incomplete");

        PlayerPrefs.SetInt("Enable show level tutorial",   0);
        PlayerPrefs.SetInt("Enable show restart tutorial", 0);

        PlayerPrefs.SetInt("Show controll tutorial", 0);

        PlayerPrefs.SetInt("Show tp tutorial", 0);
        PlayerPrefs.SetInt("Show mg tutorial", 0);
        PlayerPrefs.SetInt("Show ballon tutorial", 0);

        //*****************last_changes*****************
        PlayerPrefs.SetInt("Show fuel tutorial", 0);
        //*****************last_changes*****************

        PlayerPrefs.SetInt("Show gas tutorial", 0);

        PlayerPrefs.SetString("Show tips", "Enabled");
    }

    static void LoadData() {
        countOfGreeceEmeralds  = PlayerPrefs.GetInt("Count of greece emeralds");
        countOfEgyptEmeralds   = PlayerPrefs.GetInt("Count of egypt emeralds");
        countOfAmericaEmeralds = PlayerPrefs.GetInt("Count of america emeralds");

        isGreeceLevel = PlayerPrefs.GetString("Greece level");
        isEgyptLevel  = PlayerPrefs.GetString("Egypt level");
        isLatinLevel  = PlayerPrefs.GetString("Latin level");

        currentLevelName = PlayerPrefs.GetString("Current level name");

        isGreeceComplete = PlayerPrefs.GetString("Status of greece");
        isEgyptComplete  = PlayerPrefs.GetString("Status of egypt");
        isLatinComplete  = PlayerPrefs.GetString("Status of latin");

        isShowedLevelTutorial = PlayerPrefs.GetInt("Enable show level tutorial");
        isRestartedTutorial   = PlayerPrefs.GetInt("Enable restart tutorial");

        isShowedControllTutorial = PlayerPrefs.GetInt("Show controll tutorial");

        isShowTpTutorial = PlayerPrefs.GetInt("Show tp tutorial");
        isShowMgTutorial = PlayerPrefs.GetInt("Show mg tutorial");

        isMovedBallonTutorial = PlayerPrefs.GetInt("Show ballon tutorial");

        //**************************last_changes**************************
        isShowFuelTutorial = PlayerPrefs.GetInt("Show fuel tutorial");
        //**************************last_changes**************************

        isShowGasTutorial = PlayerPrefs.GetInt("Show gas tutorial");

        isShowTips = PlayerPrefs.GetString("Show tips");
    }

    public static void SetTipsStatus(string value) {
        isShowTips = value;
        PlayerPrefs.SetString("Show tips", isShowTips);
        PlayerPrefs.Save();
    }

    public static bool GetTipsStatus() {
        return (isShowTips == "Enabled");
    }
    
    public static int GetCountEgyptEmeralds() {
        return countOfEgyptEmeralds;
    }

    public static int GetCountAmericaEmeralds() {
        return countOfAmericaEmeralds;
    }

    //************************************change_now************************************
    public static void AddGreeceEmeralds(int emerald) {
        countOfGreeceEmeralds += emerald;
        PlayerPrefs.SetInt("Count of greece emeralds", countOfGreeceEmeralds);
        PlayerPrefs.Save();
    }

    public static void SetCountGreeceEmeralds(int ammount) {
        countOfGreeceEmeralds = ammount;
        PlayerPrefs.SetInt("Count of greece emeralds", countOfGreeceEmeralds);  
        PlayerPrefs.Save();
    }

    public static int GetCountGreeceEmeralds() {
        return countOfGreeceEmeralds;
    }
    //************************************change_now************************************

    public static void SetCountEgyptEmeralds(int ammount) {
        countOfEgyptEmeralds = ammount;
        PlayerPrefs.SetInt("Count of egypt emeralds", countOfEgyptEmeralds);
        PlayerPrefs.Save();
    }

    public static void SetCountAmericaEmeralds(int ammount) {
        countOfAmericaEmeralds = ammount;
        PlayerPrefs.SetInt("Count of america emeralds", countOfAmericaEmeralds);
        PlayerPrefs.Save();
    }    

    public static bool IsEgyptOpened() {
        return (isEgyptLevel == "Opened");
    }

    public static bool IsLatinOpened() {
        return (isLatinLevel == "Opened");
    }

    public static void EgyptOpen(string value) {
        isEgyptLevel = value;
        PlayerPrefs.SetString("Egypt level", isEgyptLevel);
        PlayerPrefs.Save();
    }

    public static void LatinOpen(string value) {
        isLatinLevel = value;
        PlayerPrefs.SetString("Latin level", isLatinLevel);
        PlayerPrefs.Save();
    }

    public static string GetCurrentLevelname() {
        if (currentLevelName.Equals("None")) {
            return "Greece_level";
        }
        return currentLevelName;
    }

    public static void SetCurrentLevelname(string value) {
        currentLevelName = value;
        PlayerPrefs.SetString("Current level name", currentLevelName);
        PlayerPrefs.Save();
    }

    public static bool IsGreeceComplete() {
        return (isGreeceComplete == "Complete");
    }

    public static bool IsEgyptComplete() {
        return (isEgyptComplete == "Complete");
    }

    public static bool IsLatinComplete() {
        return (isLatinComplete == "Complete");
    }

    public static void SetGreeceStatus(string status) {
        isGreeceComplete = status;
        PlayerPrefs.SetString("Status of greece", isGreeceComplete);
        PlayerPrefs.Save();
    }

    public static void SetEgyptStatus(string status) {
        isEgyptComplete = status;
        PlayerPrefs.SetString("Status of egypt", isEgyptComplete);
        PlayerPrefs.Save();
    }

    public static void SetLatinStatus(string status) {
        isLatinComplete = status;
        PlayerPrefs.SetString("Status of latin", isLatinComplete);
        PlayerPrefs.Save();
    }

    public static void DisableShowLevelTutorial(int value) {
        isShowedLevelTutorial = value;
        PlayerPrefs.SetInt("Enable show level tutorial", isShowedLevelTutorial);  
        PlayerPrefs.Save();
    }

    public static void DisableRestartTutorial(int value) {
        isRestartedTutorial = value;
        PlayerPrefs.SetInt("Enable show restart tutorial", isRestartedTutorial);  
        PlayerPrefs.Save();
    }

    public static bool GetLevelTutorial() {
        return (isShowedLevelTutorial.Equals(0));
    }

    public static bool GetRestartTutorial() {
        return (isRestartedTutorial.Equals(0));
    }
    
    public static void DisableControllTutorial(int value) {
        isShowedControllTutorial = value;
        PlayerPrefs.SetInt("Show controll tutorial", isShowedControllTutorial);
        PlayerPrefs.Save();
    }

    public static void DisableTpTutorial(int value) {
        isShowTpTutorial = value;
        PlayerPrefs.SetInt("Show tp tutorial", isShowTpTutorial);
        PlayerPrefs.Save();
    }

    public static void DisableMgTutorial(int value) {
        isShowMgTutorial = value;
        PlayerPrefs.SetInt("Show mg tutorial", isShowMgTutorial);
        PlayerPrefs.Save();
    }

    public static void DisableBallonTutorial(int value) {
        isMovedBallonTutorial = value;
        PlayerPrefs.SetInt("Show ballon tutorial", isMovedBallonTutorial);
        PlayerPrefs.Save();
    }

    public static void DisableGasTutorial(int value) {
        isShowGasTutorial = value;
        PlayerPrefs.SetInt("Show gas tutorial", isShowGasTutorial);
        PlayerPrefs.Save();
    }

    public static bool GetGasTutorial() {
        return (isShowGasTutorial.Equals(0));
    }

    public static bool GetBallonTutorial() {
        return (isMovedBallonTutorial.Equals(0));
    }

    public static bool GetTpTutorial() {
        return (isShowTpTutorial.Equals(0));
    }

    public static bool GetMgTutorial() {
        return (isShowMgTutorial.Equals(0));
    }

    public static bool GetControllTutorial() {
        return (isShowedControllTutorial.Equals(0));
    }

    //**************************last_changes**************************
    public static bool CheckFuelTutorial()
    {
        return (isShowFuelTutorial.Equals(0));
    }

    public static void FuelTutorialOff(int value)
    {
        isShowFuelTutorial = value;
        PlayerPrefs.SetInt("Show fuel tutorial", isShowFuelTutorial);
        PlayerPrefs.Save();
    }
    //**************************last_changes**************************
}
