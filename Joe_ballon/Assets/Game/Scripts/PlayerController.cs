using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class PlayerController : MonoBehaviour {
    public static PlayerController Instance;

    private const float minAngleRotSpeed = 20;

    private float constPosXPlayer;
    private float cellLength;
    private float rotationSpeed = 1f;

    public float distanceToAnObctical { get; set; }
    public float maxDistanceToAnObctical { get; set; }

    private static float MAX_BOOST_ROTATION_SPEED = 4;
    private static float NORMAL_ROTATION_SPEED = 1;
    private static float FORCE_UP_MULTIPLYER = 6;

    private float xScale;

    private bool _isFlameActive;
    private bool _isSmokeActive;

    public BoxCollider2D klaxonCollider;
    
    public PolygonCollider2D thisCollider;

    public Animator charAnimator;
    public Animator klaxonAnimator;

    public GameObject flame;
    public GameObject wave;
    public GameObject sound;

    public Transform lineCastObstical1Start;
    public Transform lineCastObstical3Start;
    public Transform lineCastObstical1End;
    public Transform lineCastObstical3End;
    public Transform lineCastGroundStart;
    public Transform lineCastGroundEnd;
    public Transform LineCastingObject;
    public Transform balloonTransform;
    public Transform collector;
    public Transform magnetTransform;

    public Rigidbody2D thisRigidBody { get; set; }

    public RaycastHit2D ObsticalHit;

    public GameObject magnet;
    public GameObject[] damageEffects;

    public bool isOverRotated { get; set; }
    public bool isStabilised { get; private set; }
    public bool isCollided { get; set; }
    public bool isAnObsticalOnTheRight { get; set; }
    public bool isGrounded { get; set; }
    public bool isForcing { get; set; }
    public bool isMovingUp { get; set; }
    public bool isBig { get; set; }
    public bool isInPortal { get; set; }
    public bool isForceButtonPressed { get; set; }
    public bool isHasTouchedGround { get; set; }
    public bool isEngineHasChoked { get; set; }
    public bool isForcingUp { get; set; }
    public bool isAttackable { get; set; }
    public bool isFlameActive { get { return _isFlameActive; } set { _isFlameActive = value; } }
    public bool isSmokeActive { get { return _isSmokeActive; } set { _isSmokeActive = value; } }

    private bool isUpdateRotation;

    public ParticleSystem fight;
    public ParticleSystem looseDiamants;

    public List<LineRenderer> ropes;
    public List<Transform> ropesEdge;

    public List<GameObject> flags;

    public Transform balloon;
    public Transform basket;

    public Transform goldIn;
    public Transform goldOut;

    private List<CellController> activeCells;

    private bool isScaleBallon;

    private string level;

    private float _playerPosX;
    public float playerPosX { get { return _playerPosX; } set { _playerPosX = value; } }

    private bool _inflation;
    public bool Inflation { get { return _inflation; } set { _inflation = value; } }

    void Awake() {
		Instance = this;
	}

    void Update() {     
        if (Inflation) {
            ShrikeBallon();
        }
        if (isFlameActive) {
            PlaygroundController.Instance.flame.gameObject.SetActive(true);
        } else {
            PlaygroundController.Instance.flame.gameObject.SetActive(false);
        }
        foreach (GameObject effect in damageEffects) {
            effect.transform.eulerAngles = Vector3.zero;
        }
        playerPosX = gameObject.transform.position.x;
    }    

    private void ShrikeBallon() {
        if (isScaleBallon) {
            isScaleBallon = !isScaleBallon;
            float airAmountMax = 0f, airAmountMin = 0f;

            switch (level) {
                case "Greece_level":
                    airAmountMax = 0.55f; airAmountMin = 0.47f;
                    break;
                case "Egypt_level":
                    airAmountMax = 1.2f; airAmountMin = 1.05f;
                    break;
                case "Latin_level":
                    airAmountMax = 1.15f; airAmountMin = 1.05f;
                    break;
            }

            balloon.transform.DOScaleX(airAmountMax, 0.35f).OnComplete(delegate {
                balloon.transform.DOScaleX(airAmountMin, 0.35f).OnComplete(delegate {
                    isScaleBallon = !isScaleBallon;
                });                    
            });
        }        
    }

    private bool lineCast(Transform start, Transform end, bool countDist = false) {
        Debug.DrawLine(start.position, end.position, Color.red);
        ObsticalHit = Physics2D.Linecast(start.position, end.position, 1 << LayerMask.NameToLayer("ground"));

        if (countDist && ObsticalHit.collider != null) {
            if (maxDistanceToAnObctical == 0) {
                maxDistanceToAnObctical = Mathf.Abs(ObsticalHit.point.x - lineCastObstical1Start.position.x);
                distanceToAnObctical = maxDistanceToAnObctical;
            }

            float curDist = Mathf.Abs(ObsticalHit.point.x - lineCastObstical1Start.position.x);
            distanceToAnObctical = curDist < distanceToAnObctical ? curDist : distanceToAnObctical;
            PlaygroundController.Instance.deltaAcceleration = distanceToAnObctical / maxDistanceToAnObctical;
            return isAnObsticalOnTheRight ? true : ObsticalHit.collider != null;
        }
        return ObsticalHit.collider != null;
    }

    private void releaseObstical() {
        PlaygroundController.Instance.deltaAcceleration = 1;
        maxDistanceToAnObctical = 0;
        distanceToAnObctical = 0;
    }

    public void Raycasting() {
        if (isCollided) {
            releaseObstical();
        }
        isAnObsticalOnTheRight = false;
        isAnObsticalOnTheRight = lineCast(lineCastObstical1Start, lineCastObstical1End, true) ? true : false;
        isAnObsticalOnTheRight = lineCast(lineCastObstical3Start, lineCastObstical3End, true) ? true : false;
        isGrounded = lineCast(lineCastGroundStart, lineCastGroundEnd) ? true : false;
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll != null) {
            if (!isCollided) {
                //********************************************change_later*******************************************************
                //AudioController.PlayStream(AudioSources.Instance.punchAudioSource, LanguageAudio.GetSoundByName("Punch"));
                //********************************************change_later*******************************************************
            }
            isCollided = true;
            isStabilised = false;
        }
    }

    public void OnCollisionExit2D(Collision2D coll) {
        isCollided = false;
    }

    private bool isAllowedToFrceUp() {
        if ((transform.eulerAngles.z < 45 && transform.eulerAngles.z >= 0) || (transform.eulerAngles.z > 300 && transform.eulerAngles.z < 361)) {
            if (ScoreController.Instance.gasAmount > 0 && !isInPortal) {
                return true;
            }
        }
        return false;
    }

    private void FroceUp() {        
        if (isAllowedToFrceUp()) {
            isMovingUp = true;
            if (!isSmokeActive) {
                isFlameActive = true;
            }            
            thisRigidBody.AddForce(Vector2.up * FORCE_UP_MULTIPLYER);
            if (!isBig) {
                isBig = true;
                isGrounded = false;
            }
        }
    }

    private void stabiliseRotation() {
        if (!isCollided && ((isForcing && thisRigidBody.rotation >= 20) || (!isForcing || isHasTouchedGround))) {
            if (Mathf.Abs(thisRigidBody.rotation) > 1) {
                thisRigidBody.rotation -= Time.deltaTime * rotationSpeed * Mathf.Clamp(Mathf.Abs(thisRigidBody.rotation), minAngleRotSpeed, 1000) * Mathf.Sign(thisRigidBody.rotation);
            } else {
                thisRigidBody.rotation = 0;
            }
        }
        LineCastingObject.eulerAngles = Vector3.zero;
        thisRigidBody.angularVelocity = 0;
        imitateRotation();
        isUpdateRotation = true;
        DrawRopes();
    }

    public void Attack() {
        if (isAttackable) {
            isAttackable = false;
            klaxonCollider.enabled = true;
            if (isInPortal) {
                return;
            }            
            charAnimator.SetTrigger("Klaxon");
            DOVirtual.DelayedCall(0.2f, delegate {
                sound.SetActive(true); wave.SetActive(true);
                AudioController.PlayStream(AudioSources.Instance.klaxonAudioSource, LanguageAudio.GetSoundByName("Klaxon"));
            });
        }
    }

    private void stabilisePosition() {
        transform.position = new Vector2(constPosXPlayer, transform.position.y);
        if (transform.position.y > GetScreen.height - 5.3f) {
            transform.position = new Vector3(transform.position.x, GetScreen.height - 5.3f);
            thisRigidBody.velocity = Vector3.zero;
        } else if (transform.position.y < GetScreen.bottom + 1.4f) {   //0.98
            transform.position = new Vector3(transform.position.x, GetScreen.bottom + 1.4f);   //0.98
            thisRigidBody.velocity = Vector3.zero;
        }
        if (transform.position.y < GetScreen.bottom + 1.5f) {
            if (!isHasTouchedGround) {
                PlaygroundController.Instance.NormalizeSpeed();
            }
            if (!AudioSources.Instance.tochGroundAudioSource.isPlaying && !isHasTouchedGround) {
                //********************************************change_later*******************************************************************
                //AudioController.PlayStream(AudioSources.Instance.tochGroundAudioSource, LanguageAudio.GetSoundByName("TochGround", false));
                //********************************************change_later*******************************************************************
            }
            isHasTouchedGround = true;
        } else {
            isHasTouchedGround = false;
        }
        thisRigidBody.gravityScale = isGrounded ? 0 : PlaygroundController.NORMAL_GRAVITY_SCALE;
    }

    private void imitateRotation() {
        basket.transform.eulerAngles = Vector3.forward * thisRigidBody.rotation * 0.3f;
        if (thisRigidBody.rotation < 0) {
            balloonTransform.eulerAngles = Vector3.zero;
        }
    }

    public void DrawRopes() {
        for (int i = 0; i < ropes.Count; i++) {
            ropes[i].SetPosition(0, ropesEdge[i].position);
            ropes[i].SetPosition(1, ropesEdge[ropesEdge.Count / 2 + i].position);
        }
           
        int r_index;  float xPos, yPos, lambda; 
        for (int f_index=0; f_index < flags.Count; ++f_index) {
            r_index = (f_index > 1) ? 3 : 0;
            lambda = ((f_index % 2) != 0) ? 0.4f : 1;
            xPos = (ropesEdge[r_index].position.x + (ropesEdge[ropesEdge.Count / 2 + r_index].position.x * lambda)) / (1 + lambda);
            yPos = (ropesEdge[r_index].position.y + (ropesEdge[ropesEdge.Count / 2 + r_index].position.y * lambda)) / (1 + lambda);

            flags[f_index].transform.position = new Vector3(xPos, yPos);
            flags[f_index].SetActive(true);
        }
    }

    public IEnumerator WaitUdateRotation() {
        while (!isUpdateRotation) {
            DrawRopes();
            yield return null;
        }
    }

    public IEnumerator WaitForDamageEnd(Animator animator, GameObject damage) {
        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f) {
            yield return null;
        }
        damage.SetActive(false);
    }

    public void DamageEffect(int index) {
        if (PlaygroundController.Instance.gameState == PlaygroundController.GameState.Finish) {
            return;
        }            
        Animator animator = damageEffects[index].GetComponent<Animator>();
        DOVirtual.DelayedCall(0.3f, delegate { damageEffects[index].SetActive(true); }).OnComplete(delegate {
            StartCoroutine(WaitForDamageEnd(animator, damageEffects[index]));
        });
    }

    private void ReleaseEffects() {
        foreach (GameObject effect in damageEffects) {
            effect.SetActive(false);
        }
    }

    private void MoveBallonToTpTutorial(int index)
    {
        thisRigidBody.isKinematic = true;
        Vector2 tempScale = transform.localScale;

        TutorialController.Instance.isBallonMoveTutorial = true;

        PlaygroundController.Instance.forceUp_btn.GetComponent<Collider2D>().enabled = false;
        PlaygroundController.Instance.klaxon_btn.GetComponent<Collider2D>().enabled  = false;

        transform.DOScale(Vector2.one * 0.1f, 0.3f).OnComplete(delegate 
        {
            transform.position = activeCells[index].ballonPlacceHolder.position;
            transform.DOScale(tempScale, 0.3f);
            SaveManager.DisableBallonTutorial(1);
        });
    }

    private void StartTutorial()
    {
        if (!TutorialController.Instance.isTpTutorialShowed)
        {
            for (int index = 0; index < activeCells.Count; index++)
            {
                if (activeCells[index].tag != "StartCell" && activeCells[index].isHasTutorial)
                {
                    if (activeCells[index].isHasBallonTutorial)
                    {
                        int currIndexCellTutorial = index;
                        float ballonposX = activeCells[currIndexCellTutorial].ballonPlacceHolder.position.x;

                        if (transform.position.x > ballonposX - 2f)
                        {
                            if (!TutorialController.Instance.isBallonMoveTutorial)
                            {
                                MoveBallonToTpTutorial(currIndexCellTutorial);
                            }
                        }
                    }
                }
            }
        }       
    }

    public void Init()
    {
        level = SaveData.currentLevelName;
        isScaleBallon = true;
        thisCollider.enabled = true;
        transform.localScale = PlaygroundController.PlayerSourceScale;
        isForcing = false;
        isForcingUp = false;
        isAttackable = true;
        klaxonCollider.enabled = false;
        Inflation = false;
        playerPosX = gameObject.transform.position.x;
        isHasTouchedGround = false;
        isCollided = false;
        isStabilised = true;
        isBig = false;
        isInPortal = false;

        activeCells = PlaygroundController.Instance.activeCells;

        isForceButtonPressed = false;
        isAnObsticalOnTheRight = false;
        isUpdateRotation = false;
        isFlameActive = false;
        isSmokeActive = false;
        magnet.SetActive(false);
        distanceToAnObctical = 0;
        transform.localPosition = new Vector2(-2.7f, 0.5f);

        maxDistanceToAnObctical = 0;
        constPosXPlayer = transform.position.x;
        thisRigidBody = GetComponent<Rigidbody2D>();

        thisRigidBody.rotation = 0;
        StartCoroutine(WaitUdateRotation());
    }

    public void Process()
    {
        if (!isInPortal)
        {          
            Raycasting();
            stabilisePosition();
            stabiliseRotation();          
        }

        if (SaveManager.GetBallonTutorial() && level == "Greece_level")
        {
            StartTutorial();
        }        
    }

    void FixedUpdate()
    {        
        if (isForcingUp)
        {
            FroceUp();
        }        
    }
}
