﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using PSV_Prototype;
using RateMePlugin;
using System;
using DG.Tweening;

public class ButtonController : InputBase {

	public enum Button {
		Play,
        SelectionLevel,
        StartMenu,
        ShowIntro,
        ShowLevelMenu,
        Restart,
        ShowScore,
        AddGas,
        Buy,
        OpenPausePannel,
        ClosePausePannel,
        Sounds,
        Music,
        PlayAgain,

        None,
    }
    
    private List<Scenes>
            not_allowed_rate_me_after_end = new List<Scenes>() {
                Scenes.GreeceLevel,
                Scenes.EgyptLevel,
                Scenes.AmericaLevel,
                Scenes.Progress,
                Scenes.LevelMenu,
            };

    public Button type;
    public Sprite[] sprites;

    public bool is_locked;
    public bool radio = false;
    public bool is_enabled = true;    

    public BoxCollider2D collider { get; private set; }

    public SpriteRenderer button_renderer{get; private set;}

    private string ClickSound = "Button - Click";

    void Awake() {
        collider = GetComponent<BoxCollider2D>();
        button_renderer = gameObject.GetComponent<SpriteRenderer>();        
    }

    public void Init() {                
        if (radio) {
			if (type == Button.Music) {
                SwitchRadio(GameSettings.IsMusicEnabled());
			} else if (type == Button.Sounds) {
                SwitchRadio(GameSettings.IsSoundsEnabled());
			}
            ButtonUp();
        }        
    }

    public void SetButtonState(bool enable) {
        is_enabled = enable;
        ButtonUp();
    }

    private void SwitchRadio (bool enable) {
		is_enabled = enable;
	}

    public void PlayButton() {
        SceneLoader.Instance.SwitchToScene(Scenes.LevelMenu);

        //SceneLoader.Instance.SwitchToScene(Scenes.Progress);
    }

    public bool ShowRateUs() {
        foreach (Scenes scene in not_allowed_rate_me_after_end) {
            if (scene.ToString() == SceneManager.GetActiveScene().name) {
                return false;
            }
        }
        return true;
    }
    
    public void StartMenu() {
        if (ShowRateUs()) {            
            if (RateMeModule.Instance.ShowDialogue(true)) {
                SaveData.saveScene = Scenes.StartMenu;
                SceneLoader.Instance.SwitchToScene(Scenes.RateUs);
            } else {
                SceneLoader.Instance.SwitchToScene(Scenes.StartMenu);
            }
        } else {
            SceneLoader.Instance.SwitchToScene(Scenes.StartMenu);
        }              
    }

    public void SelectionLevel() {
        LevelController.Instance.SelectionLevel(LevelController.Instance.GetIndexLevel(tag));
    }

    public void PlayLevelAgain() {
        ProgressController.Instance.SelectionLevel(ProgressController.Instance.GetIndexLevel(tag));
    }

    public void ShowIntro() {
        ProgressController.Instance.SelectionIntro();
    }

    public void ShowLevelMenu() {
        
        if (RateMeModule.Instance.ShowDialogue(true)) {
            SaveData.saveScene = Scenes.LevelMenu;
            SceneLoader.Instance.SwitchToScene(Scenes.RateUs);
        } else {
            SceneLoader.Instance.SwitchToScene(Scenes.LevelMenu);
        }              
    }  

    public void RestartButton() {
        string level = SaveData.currentLevelName;
        switch (level) {
            case "Greece_level":
                SceneLoader.Instance.SwitchToScene(Scenes.GreeceLevel);                
                break;
            case "Egypt_level":
                SceneLoader.Instance.SwitchToScene(Scenes.EgyptLevel);
                break;
            case "Latin_level":
                SceneLoader.Instance.SwitchToScene(Scenes.AmericaLevel);
                break;
        }        
    }

    public void ShowScore() {
        PlaygroundController.Instance.buyingMenu.transform.localScale = Vector2.zero;
        PlaygroundController.Instance.InitScore();
    }

    public void SoundsButton() {
        GameSettings.EnableSounds(!GameSettings.IsSoundsEnabled());
        SetButtonState(GameSettings.IsSoundsEnabled());
    }

    public void MusicButton() {
        GameSettings.EnableMusic(!GameSettings.IsMusicEnabled());
        SetButtonState (GameSettings.IsMusicEnabled());
	}

    public void OpenPausePannel () {
        PlaygroundController.Instance.Pause();
    }

    public void ClosePausePannel() {
        PlaygroundController.Instance.Pause();
    }

    public void OnButtonPress () {
        switch (type) {
            case Button.Play:
				PlayButton ();
                break;
            case Button.StartMenu:
                StartMenu();
                break;
            case Button.SelectionLevel:
                SelectionLevel();
                break; 
            case Button.ShowIntro:
                ShowIntro();
                break;
            case Button.ShowLevelMenu:
                ShowLevelMenu();
                break;
            case Button.Restart:
                RestartButton();
                break;
            case Button.ShowScore:
                ShowScore();
                break;
            case Button.Buy:
                BuyButton();
                break;
            case Button.AddGas:
                AddButtonGas();
                break;
            case Button.OpenPausePannel:
                OpenPausePannel();
                break;
            case Button.Sounds:
                SoundsButton();
                break;
            case Button.Music:
                MusicButton();
                break;
            case Button.ClosePausePannel:
                ClosePausePannel();
                break;
            case Button.PlayAgain:
                PlayLevelAgain();
                break;
        }
	}

    public void BuyButton() {
        Time.timeScale = 1;
        DOTween.defaultTimeScaleIndependent = false;

        PlaygroundController.Instance.SoundPlay();

        PlaygroundController.Instance.buyingMenu.transform.localScale = Vector2.zero;
        PlaygroundController.Instance.forceUp_btn.gameObject.SetActive(true);
        PlaygroundController.Instance.openPausePannel_btn.gameObject.SetActive(true);

        PlaygroundController.Instance.klaxon_btn.gameObject.SetActive(true);

        PlaygroundController.Instance.forceUp_btn.ButtonUp();
        PlaygroundController.Instance.klaxon_btn.ButtonUp();

        PlaygroundController.Instance.PlayTween();
        ScoreController.Instance.diamonds -= PlaygroundController.Instance.gasPrise;
        ScoreController.Instance.UpdateScores();
        ScoreController.Instance.AddGas();
        PlaygroundController.Instance.gasPriseText.text = PlaygroundController.Instance.gasPrise.ToString();
        //**************************************************************************************
        //PlaygroundController.Instance.gasPrise += PlaygroundController.MIN_GAS_COST;
        //**************************************************************************************
        PlaygroundController.Instance.speed = 2;
        PlaygroundController.Instance.gameState = PlaygroundController.GameState.Playing;
        PlaygroundController.Instance.pausePannel.SetAlignment();
        ScoreController.Instance.fuelPunchScale = true;

    }

    public void AddButtonGas() {
        ScoreController.Instance.isPulsation = false;
        ScoreController.Instance.fadeScreen.DOFade(0f, 0.5f).OnComplete(delegate {
            ScoreController.Instance.buyGasBtn.SetActive(false);
            ScoreController.Instance.fadeScreen.gameObject.SetActive(false);
            ScoreController.Instance.fadeScreen.DOFade(0.5f, 0.1f);
            PlaygroundController.Instance.InitBuyingMenu();
        });
    }

    public void OnButtonDown() {
        /**/
    }

    public void OnButtonUp() {
        /**/
    }

    public void MoveUpButon() {
        PlaygroundController.Instance.player.isForcingUp = true;
    }

    public void OnButtonDownProcess() {
        OnButtonDown();
    }

    public void ButtonDown() {
        if (radio) {
            button_renderer.sprite = is_enabled ? sprites[1] : sprites[3];
        } else {
            if (sprites.Length > 0) {
                button_renderer.sprite = sprites[1];
            }
        }
    }
	
	public void ButtonUp() {
        if (radio) {
            button_renderer.sprite = is_enabled ? sprites[0] : sprites[2];
		} else {
            if (sprites.Length > 0) {
                button_renderer.sprite = sprites[0];
            }                
        }
    }

	override public bool OnTouchDown(Vector2 point) {
        focus = true;
        AudioController.PlaySound(ClickSound);
        InputManager.Instance.SetFocus(GetComponent<InputBase>());
		ButtonDown();
	    OnButtonDown();

        return true;
	}

	override public bool OnTouchUp(Vector2 point) {

        if (focus) {         
            focus = false;            
            InputManager.Instance.RemoveFocus(GetComponent<InputBase> ());
			if (gameObject.GetComponent<Collider2D>().OverlapPoint(point)) {
                OnButtonPress();   
            }

            ButtonUp();            
		}

        OnButtonUp();
        return true;
	}

	override public bool OnTouchMove(Vector2 point) {
		if (focus) {
			if (gameObject.GetComponent<Collider2D>().OverlapPoint (point)) {
				ButtonDown ();
			} else {
                ButtonUp();
			}
			OnButtonDownProcess();
		}
		return true;
	}

	override public bool OnTouchExit(Vector2 point) {
		focus = false;
        ButtonUp();
		return true;
	}
}
