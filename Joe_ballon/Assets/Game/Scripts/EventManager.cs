﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EventManager : MonoBehaviour {
    public static EventManager Instance;

    public event Action activeHarpyEvent;
    public event Action activeFishEvent;

    public event Action activeAnubisEvent;
    public event Action activeMummyEvent;
    public event Action activeScarabEvent;
    public event Action activeWaspEvent;

    public event Action activeMankeyEvent;
    public event Action activeGolemEvent;
    public event Action activeDragonEvent;
    public event Action activeBatEvent;

    private string level;

    public BeastController[] animals;

    private Dictionary<string, BeastController> beasts;

    public Dictionary<string, Action> activeNextMob;

    void Awake() {
        Instance = this;
    }

    public void Init() {
        level = SaveData.currentLevelName;

        beasts = new Dictionary<string, BeastController>();

        if (level == "Greece_level") {
            beasts.Add("Harpy", animals[0]);
            beasts.Add("Fish",  animals[1]);

            activeHarpyEvent += () => {
                DisableSound("FishFly");
                EnabledSound("HarpyFly", false, true);
                beasts["Fish"].gameObject.SetActive(false);
                beasts["Harpy"].isActive = true;
                beasts["Harpy"].gameObject.SetActive(true);
            };

            activeFishEvent += () => {
                DisableSound("HarpyFly");
                EnabledSound("FishFly", false, true);
                beasts["Harpy"].gameObject.SetActive(false);
                beasts["Fish"].isActive = true;
                beasts["Fish"].gameObject.SetActive(true);
            };
        } else if (level == "Egypt_level") {

            beasts.Add("Mummy",  animals[0]);
            beasts.Add("Anubis", animals[1]);
            beasts.Add("Scarab", animals[2]);
            beasts.Add("Wasp",   animals[3]);

            activeMummyEvent += () => {
                DisableSound("AnubisWalk");
                EnabledSound("MummyWalk", false, true);
                beasts["Anubis"].gameObject.SetActive(false);
                beasts["Mummy"].isActive = true;
                beasts["Mummy"].gameObject.SetActive(true);
            };

            activeAnubisEvent += () => {
                DisableSound("MummyWalk");
                EnabledSound("AnubisWalk", false, true);
                beasts["Mummy"].gameObject.SetActive(false);
                beasts["Anubis"].isActive = true;
                beasts["Anubis"].gameObject.SetActive(true);
            };

            activeScarabEvent += () => {
                DisableSound("WaspFly");
                EnabledSound("ScarabFly", false, true);
                beasts["Wasp"].gameObject.SetActive(false);
                beasts["Scarab"].isActive = true;
                beasts["Scarab"].gameObject.SetActive(true);
            };

            activeWaspEvent += () => {
                DisableSound("ScarabFly");
                EnabledSound("WaspFly", false, true);
                beasts["Scarab"].gameObject.SetActive(false);
                beasts["Wasp"].isActive = true;
                beasts["Wasp"].gameObject.SetActive(true);
            };

        } else if (level == "Latin_level") {
            beasts.Add("Golem",  animals[0]);
            beasts.Add("Mankey", animals[1]);
            beasts.Add("Dragon", animals[2]);
            beasts.Add("Bat",    animals[3]);

            activeGolemEvent += () => {
                DisableSound("MankeyWalk");
                EnabledSound("GolemWalk", false, true);
                beasts["Mankey"].gameObject.SetActive(false);
                beasts["Golem"].isActive = true;
                beasts["Mankey"].isActive = false;
                beasts["Golem"].gameObject.SetActive(true);
            };

            activeMankeyEvent += () => {
                DisableSound("GolemWalk");
                EnabledSound("MankeyWalk", false, true);
                beasts["Golem"].gameObject.SetActive(false);
                beasts["Mankey"].isActive = true;
                beasts["Golem"].isActive = false;
                beasts["Mankey"].gameObject.SetActive(true);
            };

            activeDragonEvent += () => {
                DisableSound("BatFly");
                EnabledSound("DragonFly", false, true);
                beasts["Bat"].gameObject.SetActive(false);
                beasts["Dragon"].isActive = true;
                beasts["Dragon"].gameObject.SetActive(true);
            };

            activeBatEvent += () => {
                DisableSound("DragonFly");
                EnabledSound("BatFly", false, true);
                beasts["Dragon"].gameObject.SetActive(false);
                beasts["Bat"].isActive = true;
                beasts["Bat"].gameObject.SetActive(true);
            };
        }
        InitCallBacks();
    }

    private void InitCallBacks() {
        if (level.Equals("Greece_level")) {
            activeNextMob = new Dictionary<string, Action> {
                {"Harpy", ActiveFish }, {"Fish", ActiveHarpy}
            };
        } else if (level.Equals("Egypt_level")) {
            activeNextMob = new Dictionary<string, Action> {
                { "Anubis", ActiveMummy }, {"Mummy", ActiveAnubis }, {"Scarab", ActiveWasp }, {"Wasp", ActiveScarab }
            };
        } else if (level.Equals("Latin_level")) {
            activeNextMob = new Dictionary<string, Action> {
                { "Mankey", ActiveGolem }, {"Golem", ActiveMankey }, {"Bat", ActiveDragon }, {"Dragon", ActiveBat }
            };
        }        
    }

    public void DisableSound(string name) {
        AudioController.StopStream(AudioSources.Instance.mobsSound[name]);
    }

    public void EnabledSound(string name, bool multi = true, bool loop = false) {
        AudioController.PlayStream(AudioSources.Instance.mobsSound[name], LanguageAudio.GetSoundByName(name, multi), loop);
    }

    public void ActiveHarpy() {
        if (activeHarpyEvent != null) {
            activeHarpyEvent();
        }
    }

    public void ActiveFish() {
        if (activeFishEvent != null) {
            activeFishEvent();
        }
    }

    public void ActiveMummy() {
        if (activeMummyEvent != null) {            
            activeMummyEvent();
        }
    }

    public void ActiveAnubis() {
        if (activeAnubisEvent != null) {
            activeAnubisEvent();
        }
    }

    public void ActiveScarab() {
        if (activeScarabEvent != null) {
            activeScarabEvent();
        }
    }

    public void ActiveWasp() {
        if (activeWaspEvent != null) {
            activeWaspEvent();
        }
    }

    public void ActiveGolem() {
        if (activeGolemEvent != null) {
            activeGolemEvent();
        }
    }

    public void ActiveMankey() {
        if (activeMankeyEvent != null) {
            activeMankeyEvent();
        }
    }

    public void ActiveDragon() {
        if (activeDragonEvent != null) {
            activeDragonEvent();
        }
    }

    public void ActiveBat() {
        if (activeBatEvent != null) {
            activeBatEvent();
        }
    }
}
