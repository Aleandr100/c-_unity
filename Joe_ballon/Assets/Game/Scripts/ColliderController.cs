﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ColliderController : MonoBehaviour {

	public enum Type { Dog, Klaxon };

	public Type type;
    public PlayerController player;

    void OnTriggerEnter2D(Collider2D col) {           
        if (col.tag == "Centaur") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Centaur"].isHasBitten = true;
                ScoreController.Instance.GetDamage(50f);
                player.fight.Play();
                PlaygroundController.Instance.beasts["Centaur"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(2);
            } else {
                PlaygroundController.Instance.beasts["Centaur"].Die();
            }
        } else if (col.tag == "Succubus") {
            if (type == Type.Dog) {
                col.transform.parent.gameObject.GetComponent<BeastController>().isHasBitten = true;
                ScoreController.Instance.GetDamage(40f);
                PlayerController.Instance.fight.Play();                
                col.transform.parent.gameObject.GetComponent<BeastController>().collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(1);
            } else {                
                col.transform.parent.gameObject.GetComponent<BeastController>().Die();
            }
        }  else if (col.tag == "Harpy") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Harpy"].isHasBitten = true;
                ScoreController.Instance.GetDamage(30f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Harpy"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(0);
            } else {
                PlaygroundController.Instance.beasts["Harpy"].Die();
            }
        } else if (col.tag == "Fish") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Fish"].isHasBitten = true;
                ScoreController.Instance.GetDamage(30f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Fish"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(0);
            } else {
                PlaygroundController.Instance.beasts["Fish"].Die();
            }
        } else if (col.tag == "Anubis") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Anubis"].isHasBitten = true;
                ScoreController.Instance.GetDamage(50f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Anubis"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(2);
            } else {
                PlaygroundController.Instance.beasts["Anubis"].Die();
            }
        } else if (col.tag == "Mummy") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Mummy"].isHasBitten = true;
                ScoreController.Instance.GetDamage(40f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Mummy"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(1);
            }
            else {
                PlaygroundController.Instance.beasts["Mummy"].Die();
            }
        } else if (col.tag == "Scarab") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Scarab"].isHasBitten = true;
                ScoreController.Instance.GetDamage(30f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Scarab"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(0);
            } else {
                PlaygroundController.Instance.beasts["Scarab"].Die();
            }
        } else if (col.tag == "Wasp") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Wasp"].isHasBitten = true;
                ScoreController.Instance.GetDamage(30f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Wasp"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(0);
            } else {
                PlaygroundController.Instance.beasts["Wasp"].Die();
            }
        } else if (col.tag == "Mankey") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Mankey"].isHasBitten = true;
                ScoreController.Instance.GetDamage(40f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Mankey"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(1);
            } else {
                PlaygroundController.Instance.beasts["Mankey"].Die();
            }
        } else if (col.tag == "Golem") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Golem"].isHasBitten = true;
                ScoreController.Instance.GetDamage(50f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Golem"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(2);
            } else {
                PlaygroundController.Instance.beasts["Golem"].Die();
            }
        } else if (col.tag == "Bat") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Bat"].isHasBitten = true;
                ScoreController.Instance.GetDamage(30f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Bat"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(0);               
            } else {
                PlaygroundController.Instance.beasts["Bat"].Die();
            }
        } else if (col.tag == "Dragon") {
            if (type == Type.Dog) {
                PlaygroundController.Instance.beasts["Dragon"].isHasBitten = true;
                ScoreController.Instance.GetDamage(40f);
                PlayerController.Instance.fight.Play();
                PlaygroundController.Instance.beasts["Dragon"].collider2D.enabled = false;
                AudioController.PlayStream(AudioSources.Instance.fightAudioSource, LanguageAudio.GetSoundByName("Fight"));
                player.looseDiamants.Play();
                player.DamageEffect(1);
                
            } else {
                PlaygroundController.Instance.beasts["Dragon"].Die();
            }
        }        
    }

    public void Refresh() {
        player.klaxonCollider.enabled = false;
        player.isAttackable = true;
        player.wave.SetActive(false);
        player.sound.SetActive(false);
    }
}
