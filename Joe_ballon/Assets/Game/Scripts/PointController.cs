﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class PointController : MonoBehaviour {

    public enum Ratio { THIRTY_PERCENT, SIXTY_PERCENT, NINETY_PERCENT }

    public GameObject[] points;
    private Dictionary<Ratio, float> ratios;

    public Animator[] trees;

    private string level;

    void Start() {
        level = SaveManager.GetCurrentLevelname();

        ratios = new Dictionary<Ratio, float>();
        ratios.Add(Ratio.THIRTY_PERCENT, 0.0f);
        ratios.Add(Ratio.SIXTY_PERCENT, 0.0f);
        ratios.Add(Ratio.NINETY_PERCENT, 0.0f);
    }

    public void SetRatio() {
        int rate = 30;
        List<Ratio> keys = new List<Ratio>(ratios.Keys);
        foreach (Ratio key in keys) {
            ratios[key] = SaveData.availableGold * rate / 100;
            rate += 30;
        }
    }

    public float GetRatio() {
        return (SaveData.collectedGold > 0 ? (SaveData.availableGold * 100) / SaveData.collectedGold : 0);
    }

    public void ScaleEfects(GameObject go, float scale, float delay = 0) {
        go.transform.localScale = Vector2.zero;
        go.transform.DOScale(Vector2.one * scale, 0.5f).SetEase(Ease.OutBounce).SetDelay(delay).OnStart(delegate {
            AudioController.PlayStream(AudioSources.Instance.displayAudioSource, LanguageAudio.GetSoundByName("Display"));
        });
    }

    public void PointsFinish(float ratio, float scale) {
        if (ratio >= ratios[Ratio.NINETY_PERCENT]) {
            points[0].SetActive(true);
            ScaleEfects(points[0], scale);

            points[1].SetActive(true);
            ScaleEfects(points[1], scale, .5f);

            points[2].SetActive(true);
            ScaleEfects(points[2], scale, 1f);
        } else if (ratio >= ratios[Ratio.SIXTY_PERCENT]) {
            points[0].SetActive(true);
            ScaleEfects(points[0], scale);

            points[1].SetActive(true);
            ScaleEfects(points[1], scale, .5f);
        } else if (ratio >= ratios[Ratio.THIRTY_PERCENT]) {
            points[0].SetActive(true);
            ScaleEfects(points[0], scale);
        }
    }
}
