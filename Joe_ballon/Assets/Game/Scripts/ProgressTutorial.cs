﻿using UnityEngine;
using System.Collections;

namespace Tutorials {

    public enum Tutorial { OpenLevel, Restartlevel, None }

    public class ProgressTutorial : MonoBehaviour {

        public static ProgressTutorial Instance;

        public Transform hand;
        public Transform[] handsPos;

        public GameObject tutorialObject;

        private AudioSource audioSource;

        private bool _isLevelTutorialShowed;
        private bool _isRestartTutorialShowed;

        //PROPERTY
        public bool isLevelTutorialShowed {
            get {
                return _isLevelTutorialShowed;
            } set {
                _isLevelTutorialShowed = value;
            }
        }
    
        public bool isRestartTutorialShowed {
            get {
                return _isRestartTutorialShowed;
            } set {
                _isRestartTutorialShowed = value;
            }
        }        

        private void Awake() {
		    Instance = this;
        }

        void Start () {
            isLevelTutorialShowed   = false;
            isRestartTutorialShowed = false;
        }

        private IEnumerator WaitForAudioStop(AudioSource audioSource, int index) {

            hand.transform.parent = handsPos[index];
            hand.transform.localPosition = Vector3.zero;
            hand.gameObject.SetActive(true);

            while (audioSource.isPlaying) {
                yield return null;
            }

            hand.transform.parent = tutorialObject.transform;
            hand.transform.localPosition = Vector3.zero;
            hand.gameObject.SetActive(false);
        }

        public void ShowLevelTutorial() {
            AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-17", true));
            StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 1));
        }

        public void ShowRestartTutorial() {
            AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-18", true));
            StartCoroutine(WaitForAudioStop(AudioSources.Instance.sound, 0));
        }
    }
}
