﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

using DG.Tweening;
using PSV_Prototype;

public class LevelController : MonoBehaviour {
    public static LevelController Instance;

    public GameObject[] lights;
    public GameObject[] icons;
    public GameObject[] lock_static;

    public GameObject[] surf_dynamic;
    public GameObject[] lock_dynamic;

    public Transform[] emeraldsPlaceholdersGroup;

    public ButtonController[] level_btn;

    public ParticleSystem[] explosion;

    private Dictionary <string, int> levels;

    private string level;

    private bool _activeExpEgypt = false;
    private bool _activeExpLatin = false;

    public bool activeExpEgypt { get { return _activeExpEgypt; } set { _activeExpEgypt = value; } }
    public bool activeExpLatin { get { return _activeExpLatin; } set { _activeExpLatin = value; } }

    void Awake()
    {
        Instance = this;
        levels = new Dictionary<string, int>();

        levels.Add("Greece_level", 0);
        levels.Add("Egypt_level",  1);
        levels.Add("Latin_level",  2);
    }

    void Start()
    {        
        StopStreams();
        if (SaveData.isSoundEnabled)
        {

            if (AudioSources.Instance.sounds != null)
            {
                print("Not_null");
                print(AudioSources.Instance.sounds.Count);
            }

            AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-1", true));

            SaveData.isSoundEnabled = false;
        }            
        ShowEmeralds();
        if (SaveManager.IsEgyptOpened()) {            
            if (SaveData.isActiveExpEgypt) {
                ShowBreakLock(0);
                SaveData.isActiveExpEgypt = false;
            } else {
                lock_static[0].SetActive(false);
                icons[0].SetActive(true);
            }
            level_btn[1].is_locked = false;
        }
        if (SaveManager.IsLatinOpened()) {            
            if (SaveData.isActiveExpLatin) {
                ShowBreakLock(1);
                SaveData.isActiveExpLatin = false;
            } else {                
                lock_static[1].SetActive(false);
                icons[1].SetActive(true);
            }
            level_btn[2].is_locked = false;
        }
    }

    void Update() {
        InputManager.Instance.Process();
    }

    public void SelectionLevel(int index)
    {
        if (!level_btn[index].is_locked)
        {
            level_btn[index].transform.DOPunchScale(new Vector3(-0.2f, -0.2f, -0.2f), 0.3f).OnStart(delegate 
            {
                level_btn[index].collider.enabled = false;
                lights[index].SetActive(true);
                SaveManager.SetCurrentLevelname(level);
                SaveData.currentLevelName = level;
                LoadLevel();
            });
        }              
   }

   public int GetIndexLevel(string level)
    {
        this.level = level;
        List<string> keys = new List<string>(levels.Keys);
        foreach (string key in keys)
        {
            if (key.Equals(level))
            {
                return levels[key];
            }
        }
        return 0;
    }

   private void LoadLevel()
    {
        switch (level)
        {
            case "Greece_level":
                //**********************************debug*************************************
                SceneLoader.Instance.SwitchToScene(Scenes.GreeceLevel);
                //SceneLoader.Instance.SwitchToScene(Scenes.Progress);
                //SceneLoader.Instance.SwitchToScene(Scenes.Finish);
                //**********************************debug*************************************
                break;
            case "Egypt_level":
                SceneLoader.Instance.SwitchToScene(Scenes.EgyptLevel);
                break;
            case "Latin_level":
                SceneLoader.Instance.SwitchToScene(Scenes.AmericaLevel);
                break;
        }
   }

   private void ShowEmeralds()
    {
        int index = 0;

        int gCountOfEmeralds = SaveManager.GetCountGreeceEmeralds();
        //**********************************debug*************************************
        //print("###gCountOfEmeralds: " + gCountOfEmeralds + " ###");
        //**********************************debug*************************************
        foreach (Transform pl in emeraldsPlaceholdersGroup[0].transform) {
            if (index < gCountOfEmeralds) {
                pl.GetChild(0).gameObject.SetActive(true);
                ++index;
            }  else break;
        }
        index = 0;

        int eCountOfEmeralds = SaveManager.GetCountEgyptEmeralds();
        //**********************************debug*************************************
        //print("###eCountOfEmeralds: " + eCountOfEmeralds + " ###");
        //**********************************debug*************************************
        foreach (Transform pl in emeraldsPlaceholdersGroup[1].transform)
        {
            if (index < eCountOfEmeralds) {
                pl.GetChild(0).gameObject.SetActive(true);
                ++index;
            } else break;
        }
        index = 0;

        int aCountOfEmeralds = SaveManager.GetCountAmericaEmeralds();
        foreach (Transform pl in emeraldsPlaceholdersGroup[2].transform)
        {
            if (index < aCountOfEmeralds) {
                pl.GetChild(0).gameObject.SetActive(true);
                ++index;
            } else break;
        }
    }

    private void ShowBreakLock(int index)
    {
        lock_static[index].SetActive(false);

        surf_dynamic[index].SetActive(true);
        lock_dynamic[index].SetActive(true);

        explosion[index].Play();
        AudioController.PlayStream(AudioSources.Instance.backSound["Explosion"], LanguageAudio.GetSoundByName("Explosion"));

        DOVirtual.DelayedCall(0.5f, delegate 
        {
            icons[index].SetActive(true);
            surf_dynamic[index].GetComponent<Animator>().enabled = true;
            lock_dynamic[index].GetComponent<Animator>().enabled = true;
            AudioController.PlayStream(AudioSources.Instance.backSound["Lock"], LanguageAudio.GetSoundByName("Lock"));
            DOVirtual.DelayedCall(0.5f, delegate 
            {
                lock_dynamic[index].transform.DOMoveY(-4.5f, 0.7f).OnComplete(delegate 
                {
                    lock_dynamic[index].SetActive(false);
                    surf_dynamic[index].SetActive(false);
                });
            });
        });
    }

    private void StopStreams() {
        AudioController.StopMusic(AudioController.GetMusicSrc());

        if (AudioSources.Instance.backSound["Sea"].isPlaying)
        {
            AudioController.StopStream(AudioSources.Instance.backSound["Sea"]);
        }
        if (AudioSources.Instance.backSound["Gulls"].isPlaying)
        {
            AudioController.StopStream(AudioSources.Instance.backSound["Gulls"]);
        }
        if (AudioSources.Instance.backSound["Desert"].isPlaying)
        {
            AudioController.StopStream(AudioSources.Instance.backSound["Desert"]);
        }
        if (AudioSources.Instance.backSound["Forest"].isPlaying)
        {
            AudioController.StopStream(AudioSources.Instance.backSound["Forest"]);
        }
    }
}
