﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultitouchInput : InputBase{
	public static MultitouchInput Instance;

	public LayerMask touchInputMask;
	private List<InputBase> touchList;
	private List<InputBase> focused;

	private InputBase[] touchesOld;
	
	private RaycastHit hit;
	
	void Awake() {
		Instance = this;
	}
	void Start() {
		focused = new List<InputBase> ();
		touchList = new List<InputBase>();
	}

	public void SetFocus(InputBase _focus) {
		focused.Add (_focus);
	}
	
	public void RemoveFocus (InputBase _focus) {
		focused.Remove (_focus);
	}
	
	public void ClearFocus () {
		focused.Clear ();
	}

	private void FocusedItems () {
		TouchPhase phase = TouchPhase.Began;
		Vector3 position = new Vector3();
#if UNITY_EDITOR
		if (Input.GetMouseButtonUp(0) || Input.GetMouseButton(0)) {
			if (Input.GetMouseButtonUp(0)) {
				phase = TouchPhase.Ended;
			} else if (Input.GetMouseButton(0)) {
				phase = TouchPhase.Moved;
			}
			position = Input.mousePosition;
		}
		position = Camera.main.ScreenToWorldPoint (position);
		if (phase != TouchPhase.Began) {
			for (int i = focused.Count - 1; i >= 0; i--) {
				InputBase input = focused[0];
				Touch (input, phase, position, true);
			}
		}
#else
		foreach (InputBase focus in focused) {
			foreach (Touch touch in Input.touches) {
				if (focus.GetTouchId() == touch.fingerId) {
					phase = touch.phase;
					position = Camera.main.ScreenToWorldPoint (touch.position);
					break;
				}
			}
			if (phase != TouchPhase.Began) {
				Touch (focus, phase, position, true);
			}
		}
#endif
	}
	
	private bool Touch (InputBase input, TouchPhase phase, Vector2 point, bool focus = false) {
		bool breaking = false;
		
		if (input != null && input.IsFocused() == focus) {
			if (phase == TouchPhase.Began) {
				breaking = input.OnTouchDown(point);
			}
			if (phase == TouchPhase.Ended) {
				breaking = input.OnTouchUp(point);
			}
			if (phase == TouchPhase.Stationary || phase == TouchPhase.Moved) {
				breaking = input.OnTouchMove(point);
			}
			if (phase == TouchPhase.Canceled) {
				breaking = input.OnTouchExit(point);
			}
		}
		return breaking;
	}

	void Update() {
		touchesOld = null;
#if UNITY_EDITOR

		if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0)) {
			touchesOld = new InputBase[touchList.Count];
			touchList.CopyTo (touchesOld);
			touchList.Clear();
			
			Vector3 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray, Vector2.zero,20, touchInputMask);

			if (hit.transform!=null) {
				InputBase recipient = hit.transform.gameObject.GetComponent<InputBase>();
                touchList.Add (recipient);

			    if (Input.GetMouseButtonDown(0)) {
				    recipient.OnTouchDown(hit.point);
				}
				if (Input.GetMouseButtonUp(0)) {
					recipient.OnTouchUp(hit.point);
				}
				if (Input.GetMouseButton(0)) {
					recipient.OnTouchMove(hit.point);
				}
			}
			
			foreach (InputBase g in touchesOld) {
				if (!touchList.Contains(g)) {
					g.OnTouchExit(hit.point);
				}
			}
		}
#endif
		if (Input.touchCount > 0) {
			touchesOld = new InputBase[touchList.Count];
			touchList.CopyTo (touchesOld);
			touchList.Clear();
			
			foreach (Touch touch in Input.touches) {
				Vector3 ray = Camera.main.ScreenToWorldPoint(touch.position);
				RaycastHit2D hit = Physics2D.Raycast (ray, Vector2.zero,20, touchInputMask);

				if (hit.transform!=null){
					InputBase recipient = hit.transform.gameObject.GetComponent<InputBase>();
					touchList.Add (recipient);

					recipient.SetTouchId(touch.fingerId);
                    if (touch.phase == TouchPhase.Began) {
						recipient.OnTouchDown(hit.point);
					}
                    if (touch.phase == TouchPhase.Ended) {
						recipient.OnTouchUp(hit.point);
					}
                    if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
						recipient.OnTouchMove(hit.point);
					}
                    if (touch.phase == TouchPhase.Canceled) {
						recipient.OnTouchExit(hit.point);
					}
				}
			}

			foreach (InputBase g in touchesOld) {
				if (!touchList.Contains (g)) {
					g.OnTouchExit(hit.point);
				}
			}
		} 
	}
}

