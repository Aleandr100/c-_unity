﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV_Prototype;
using RateMePlugin;
using DG.Tweening;

public class FinishController : MonoBehaviour {
    public static FinishController Instance;

    private const int EMERALDS_COUNT = 7;

    public Transform[] emeraldsPlaceholdersGroup;

    public Animator[] templesAnimator;
    
    public TextMesh[] collectScore;
    public TextMesh[] highScore;
    public TextMesh[] looseScore;

    public GameObject greeceLevelEnd;
    public GameObject egyptLevelEnd;
    public GameObject americaLevelEnd;

    public GameObject[] scoreTables;
    public GameObject[] redLight;
    
    public PointController[] points;

    public SpriteRenderer[] pentagrams;
    public BalloonController[] players;

    public GameObject[] emeraldPrefabs;

    private float ratio;
    private int iLevel;

    private string level;
    private bool isStopAnimator = false;

    private List<GameObject> emeralds = new List<GameObject>();
    private List<Transform> emeraldsPlaceholders = new List<Transform>();

    void Awake() {
        Instance = this;
    }

    void Start() {
        level = SaveData.currentLevelName;

        AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-20", true));

        switch (level) {
            case "Greece_level":
                greeceLevelEnd.SetActive(true);
                iLevel = 0; 
                break;
            case "Egypt_level":
                egyptLevelEnd.SetActive(true);
                iLevel = 1;
                break;
            case "Latin_level":
                americaLevelEnd.SetActive(true);
                iLevel = 2;
                break;
        }

        players[iLevel].Init();

        scoreTables[iLevel].transform.localScale = Vector2.zero;
        InitPlaceHolders();     
    }

    private void FadePentagram(int count) {
        foreach (GameObject emerald in emeralds) {
            emerald.transform.DOScale(0.0f, 0.5f).SetEase(Ease.OutBounce).OnComplete(delegate {
                if (emerald != null) {
                    Destroy(emerald);
                }
                count--;
                if (count.Equals(0)) {
                    emeralds.Clear(); emeraldsPlaceholders.Clear();

                    pentagrams[iLevel].DOFade(0.0f, 0.5f).SetEase(Ease.Linear).OnComplete(delegate {
                        PlayTransformSounds();
                        templesAnimator[iLevel].Play("transform");
                        StartCoroutine(WaitTransformTemple());
                    });                   
                }
            });
        }
    }

    private void ShowBacklight(int count) {
        SpriteRenderer _renderer = redLight[iLevel].GetComponent<SpriteRenderer>();
        Color newColor = new Color(_renderer.material.color.r, _renderer.material.color.g, _renderer.material.color.b, 0.0f);
        _renderer.color = newColor; redLight[iLevel].SetActive(true);

        redLight[iLevel].GetComponent<SpriteRenderer>().DOFade(1.0f, 0.8f).OnComplete(delegate {
            redLight[iLevel].GetComponent<SpriteRenderer>().DOFade(0.0f, 0.8f).OnComplete(delegate {
                FadePentagram(count);             
            });
        });
    }

    public void MoveEmeraldsToTample() {
        float delay = 0.0f; int count = 0;
        OpenNextLevel();
        for (int index = 0; index < emeraldsPlaceholders.Count; index++) {
            emeralds.Add(Instantiate(emeraldPrefabs[iLevel], players[iLevel].emeraldPos.position, players[iLevel].emeraldPos.rotation) as GameObject);
            emeralds[emeralds.Count-1].transform.localScale = Vector2.one * 0.25f;
            emeralds[index].SetActive(false);
            emeralds[index].GetComponent<Animator>().enabled = false;
            emeralds[index].transform.GetChild(1).gameObject.SetActive(false);

            int pIndex = index;
            DOVirtual.DelayedCall(delay, delegate {
                emeralds[pIndex].transform.DOMove(emeraldsPlaceholders[pIndex].transform.position, 0.5f).OnStart(delegate { emeralds[pIndex].SetActive(true); }).OnComplete(delegate {
                    emeralds[pIndex].transform.parent = emeraldsPlaceholders[pIndex].transform;
                    ++count;
                    if (count.Equals(EMERALDS_COUNT)) {
                        ShowBacklight(count);       
                    }
                });
            });  
            delay += 0.3f;
        }
    }

    private IEnumerator WaitTransformTemple() {
        yield return new WaitForSeconds(0.05f);
        while (templesAnimator[iLevel].GetCurrentAnimatorStateInfo(0).IsName("transform") && (templesAnimator[iLevel].GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)) {
            yield return null;
        }
        PlaySounds();
        DOVirtual.DelayedCall(3.5f, delegate {
            InitScoreTable();
        });
    }

    private void InitScoreTable() {
        stopAnimations();
        players[iLevel].StopAnimator();
     
        scoreTables[iLevel].transform.DOScale(1, 0.3f).SetEase(Ease.OutBack);

        DOVirtual.DelayedCall(2.5f, delegate { isStopAnimator = true; });

        collectScore[iLevel].text = SaveData.collectedGold.ToString();
        highScore[iLevel].text = ((int)(SaveData.score * 0.1f)).ToString();
        looseScore[iLevel].text = SaveData.goldLoose.ToString();

        points[iLevel].SetRatio();

        switch (level) {
            case "Greece_level":
                PointsFinish(points[iLevel].GetRatio(), .5f);
                break;
            case "Egypt_level":
                egyptLevelEnd.SetActive(true);
                PointsFinish(points[iLevel].GetRatio(), 1f);
                break;
            case "Latin_level":
                PointsFinish(points[iLevel].GetRatio(), 1.05f);
                iLevel = 2;
                break;
        }
    }    

    private void OpenNextLevel() {
        switch (level) {
            case "Greece_level":
                if (SaveManager.IsEgyptOpened()) return;
                SaveManager.EgyptOpen("Opened");
                SaveData.isActiveExpEgypt = true;
                break;
            case "Egypt_level":
                if (SaveManager.IsLatinOpened()) return;
                SaveManager.LatinOpen("Opened");
                SaveData.isActiveExpLatin = true;
                break;
        }
    }

    private void PlayTransformSounds() {
        switch (level) {
            case "Greece_level":
                AudioController.PlayStream(AudioSources.Instance.backSound["Greece_transform"],  LanguageAudio.GetSoundByName("Greece_transform"));
                break;
            case "Egypt_level":
                AudioController.PlayStream(AudioSources.Instance.backSound["Pyramid_transform"], LanguageAudio.GetSoundByName("Pyramid_transform"));
                break;
            case "Latin_level":
                AudioController.PlayStream(AudioSources.Instance.backSound["Latin_transform"],   LanguageAudio.GetSoundByName("Latin_transform"));
                break;
        }
    }

    private void PlaySounds() {       
        AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-4", true));
        DOVirtual.DelayedCall(4.5f, delegate {
            AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-7", true));
        });
        
    }
    private void PointsFinish(float ratio, float scale) {
        points[iLevel].PointsFinish(ratio, scale);
    }

    public void stopAnimations() {
        foreach (Animator tree in points[iLevel].trees) {
            tree.enabled = false;
        }
    }

    private void InitPlaceHolders() {
        foreach (Transform pl in emeraldsPlaceholdersGroup[iLevel].transform) {
            emeraldsPlaceholders.Add(pl);
        }
    }

    private void Update() {
        if (!isStopAnimator) {
            players[iLevel].drawRopes();
        }        
        InputManager.Instance.Process();
    }
}
