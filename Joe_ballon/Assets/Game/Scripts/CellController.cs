﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class CellController : MonoBehaviour {

    public List<GameObject> ObstaclePrefabs;
    public List<GameObject> golds;
    public List<GameObject> emeralds;

    public List<GameObject> portalsIn;
    public List<GameObject> portalsOut;    
    public Animator magnetAnimator;

    public bool[] starActivity;
    public bool[] emeraldActivity;
    public bool[] IsPortalInActive;
    public bool[] IsPortalOutActive;    

    public Transform edge;
    public Transform succubusPlaceholder;
  
    public GameObject magnet;
    private BeastController beast;

    public Transform[] tutorials;
    public Transform placeholdersGroup;
    public Transform starsPlaceholdersGroup;
    public Transform emeraldsPlaceholdersGroup;

    public Transform elementsGroup;
    public Transform portalsInGroup;
    public Transform portalsOutGroup;
    public Transform gasTransform;
    public Transform magnetPlaceholder;
    public Transform goldPos;
    public Transform emeraldPos;
    public Transform ballonPlacceHolder;

    public bool isGasActive { get; set; }
    public bool isMagnetActive { get; set; }

    public List<GameObject> elements = new List<GameObject>();
    public List<Transform> placeholders = new List<Transform>();
    public List<Transform> starsPlaceholders = new List<Transform>();
    public List<Transform> emeraldsPlaceholders = new List<Transform>();

    public List<Transform> portalsInPlaceholders = new List<Transform>();
    public List<Transform> portalsOutPlaceholders = new List<Transform>(); 

    [HideInInspector, SerializeField]
    private bool _isHasSuccubus;
    private bool isJewelScale;

    //**************last_changes**************
    public bool isHasFuelTutorial;
    //**************last_changes**************

    private bool isDiamondScale;

    private GameObject gas;
    private GameObject succubus;

    [ExposeProperty]
    public  bool isHasSuccubus {
        get {
            return (_isHasSuccubus ? _isHasSuccubus : false);
        }
        set {
            _isHasSuccubus = value;
        }
    }

    private void Awake()
    {
        //ballonPlacceHolder = transform.Find("Cell_6/BallonPlaceHolder");
    }

    public bool isHasTutorial;    
    public bool isHasBallonTutorial;

    public bool isHasGas;

    private bool _isGasCollected;
    public bool isGasCollected { get { return _isGasCollected; } set { _isGasCollected = value; } }

    private void InitStars() {
        foreach (Transform pl in starsPlaceholdersGroup.transform) {
            starsPlaceholders.Add(pl);
        }

        foreach (Transform pl in emeraldsPlaceholdersGroup.transform) {
            emeraldsPlaceholders.Add(pl);
        }

        foreach (Transform pl in emeraldsPlaceholders) {
            emeralds.Add(Instantiate(PlaygroundController.Instance.bigEmeraldPrefab, pl.transform.position, pl.transform.rotation) as GameObject);
            emeralds[emeralds.Count - 1].transform.parent = pl;

            if (SaveData.currentLevelName != "Greece_level") {
                emeralds[emeralds.Count - 1].SetActive(false);
            }
        }

        emeraldActivity = new bool[emeralds.Count];
        for (int i = 0; i < emeraldActivity.Length; i++) {
            if (SaveData.currentLevelName != "Greece_level") {
                emeraldActivity[i] = false;
            } else {
                emeraldActivity[i] = true;
            }         
        }

        foreach (Transform pl in starsPlaceholders) {
            golds.Add(Instantiate(PlaygroundController.Instance.goldPrefab, pl.transform.position, pl.transform.rotation) as GameObject);
            golds[golds.Count - 1].transform.parent = pl;
        }
        starActivity = new bool[golds.Count];
        for (int i = 0; i < starActivity.Length; i++) {
            starActivity[i] = true;
        }
    }

    //************************************not_fixed************************************
    private void InitSuccubus() {
        if (isHasSuccubus) {
            succubus = Instantiate(PlaygroundController.Instance.succubusPrefab, succubusPlaceholder.position, succubusPlaceholder.rotation) as GameObject;
            succubus.transform.parent = succubusPlaceholder;
            beast = succubus.GetComponent<BeastController>();
            beast.isActive = true;
        }        
    }
    //************************************not_fixed************************************

    private void InitPortals() {
        foreach (Transform pl in portalsInGroup) {
			portalsInPlaceholders.Add (pl);
		}

		foreach(Transform pl in portalsOutGroup) {
			portalsOutPlaceholders.Add (pl);
        }

        IsPortalInActive = new bool[portalsInPlaceholders.Count];
        IsPortalOutActive = new bool[portalsOutPlaceholders.Count]; 
         
		for(int i = 0; i<IsPortalOutActive.Length;i++) {
			IsPortalOutActive[i] = true;
		}

		for(int i = 0; i<IsPortalInActive.Length;i++) {
			IsPortalInActive[i] = true;
		}

		foreach(Transform inP in portalsInPlaceholders) {
            portalsIn.Add(Instantiate(PlaygroundController.Instance.PortalInPrefab,inP.transform.position,inP.transform.rotation)as GameObject);
            portalsIn[portalsIn.Count-1].transform.parent = portalsInPlaceholders[portalsIn.Count-1].transform;
			portalsIn[portalsIn.Count-1].transform.localPosition = Vector2.zero;
		}

		foreach(Transform outP in portalsInPlaceholders) {
            portalsOut.Add(Instantiate(PlaygroundController.Instance.PortalOutPrefab,outP.transform.position,outP.transform.rotation)as GameObject);
            portalsOut[portalsOut.Count-1].transform.parent = portalsOutPlaceholders[portalsOut.Count-1].transform;
			portalsOut[portalsOut.Count-1].transform.localPosition = Vector2.zero;
		}
    }

    private void InitObstacles() {
		foreach(Transform pl in placeholdersGroup.transform) {
			placeholders.Add(pl);
		}

		for(int i = 0;i<ObstaclePrefabs.Count;i++) {
			elements.Add(Instantiate(ObstaclePrefabs[i],placeholders[i].transform.position,placeholders[i].transform.rotation) as GameObject);
			elements[i].transform.parent = elementsGroup;
        }
	}
    //*************************************************************change_now*************************************************************
    private void InitGas() {

        print(gameObject.name);

        gas = Instantiate(PlaygroundController.Instance.gasPrefab, gasTransform.position, gasTransform.rotation) as GameObject;
        gas.transform.parent = gasTransform;

        if (SaveData.currentLevelName == "Greece_level") {
            isGasActive = true;
            gas.SetActive(true);
        } else {
            isGasActive = false;
            gas.SetActive(false);
        }        
    }
    //*************************************************************change_now*************************************************************

    private void UpdateStars() {
        if (PlayerController.Instance.isInPortal) return;
        for (int i = 0; i < golds.Count; i++) {            
            if (starActivity[i]) {
                if (Vector2.Distance(golds[i].transform.position, PlayerController.Instance.collector.transform.position) < PlaygroundController.Instance.minFeatDist) {
                    int index = i;
                    AudioController.PlayStream(AudioSources.Instance.pointAudoiSource, LanguageAudio.GetSoundByName("AddGold", false));
                    Vector3 tempScale = golds[index].transform.localScale;
                    golds[index].transform.DOScale(Vector3.zero, 0.65f).OnComplete(delegate {
                        golds[index].transform.localScale = tempScale;
                        golds[index].gameObject.SetActive(false);
                    });
                    golds[index].transform.DOMove(PlayerController.Instance.goldIn.position, 0.2f).OnComplete(delegate {                        
                        GameObject goldBig = Instantiate(PlaygroundController.Instance.goldBigPrefab, PlayerController.Instance.goldOut.position, Quaternion.identity) as GameObject;
                        goldBig.transform.localScale = Vector3.one * 0.11f;
                        goldBig.transform.DOScale(0.17f, 0.35f);
                        goldBig.transform.DOMove(goldPos.position, .3f).SetEase(Ease.OutBounce).OnComplete(delegate {
                            Destroy(goldBig);
                            if (!isJewelScale) {
                                isJewelScale = !isJewelScale;                            
                                Transform gold = PlaygroundController.Instance.gold;
                                gold.DOPunchScale(Vector2.one * 0.18f, 0.9f).SetEase(Ease.OutBack).OnComplete(delegate {
                                    isJewelScale = !isJewelScale;
                                });
                            }                            
                        });
                        ScoreController.Instance.AddScore(1);                                                
                    });
                    starActivity[i] = false;
                }
            }
        }
        UpdateEmerald();
    }

    private void UpdateEmerald() {
        if (PlayerController.Instance.isInPortal) return;
        for (int i = 0; i < emeralds.Count; i++) {
            if (emeraldActivity[i]) {                
                if (Vector2.Distance(emeralds[i].transform.position, PlayerController.Instance.collector.transform.position) < PlaygroundController.Instance.minFeatDist) {
                    int index = i;
                    AudioController.PlayStream(AudioSources.Instance.pointAudoiSource, LanguageAudio.GetSoundByName("AddGold"));
                    DOVirtual.DelayedCall(0.1f, delegate {
                        AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-14", true));
                    });
                    Vector3 tempScale = emeralds[index].transform.localScale;
                    emeralds[index].transform.DOScale(Vector3.zero, 0.65f).OnComplete(delegate {
                        emeralds[index].transform.localScale = tempScale;
                    });
                    emeralds[index].transform.DOMove(PlayerController.Instance.transform.position, 0.2f).OnComplete(delegate {

                        emeralds[index].GetComponent<Animator>().enabled = false;
                        emeralds[index].transform.GetChild(1).gameObject.SetActive(false);
                        emeralds[index].transform.localScale = Vector3.zero;
                        emeralds[index].transform.DOScale(tempScale, 0.55f);

                        emeralds[index].transform.DOMove(emeraldPos.position, .3f).SetEase(Ease.OutBounce).OnComplete(delegate {
                            if (!isDiamondScale) {
                                isDiamondScale = !isDiamondScale;

                                Transform emerald = PlaygroundController.Instance.emerald;
                                emerald.DOPunchScale(Vector2.one * 0.2f, 0.9f).SetEase(Ease.OutBack).OnComplete(delegate {
                                    isDiamondScale = !isDiamondScale;
                                });
                            }                            
                            emeralds[index].transform.localScale = tempScale;
                            emeralds[index].gameObject.SetActive(false);
                        });

                        ScoreController.Instance.AddDiamonds(1);
                    });
                    emeraldActivity[index] = false;
                }
            }
        }      
    }

    //*************************************************************change_now*************************************************************
    private void CollectGas() {
        isGasCollected = true;
        AudioController.PlayStream(AudioSources.Instance.gasCollectSource, LanguageAudio.GetSoundByName("GasCollect"));
        gas.GetComponent<Animator>().enabled = false;
        gas.transform.DOScale(Vector2.zero, 0.2f).SetEase(Ease.Linear);        
		gas.transform.DOMove(PlayerController.Instance.transform.position, 0.2f).OnComplete(delegate{
			gas.SetActive(false);
            ScoreController.Instance.AddGas();
            isGasActive = false;
        });
	}
    //*************************************************************change_now*************************************************************

    private bool IsGasInReach() {
		return Vector2.Distance(gasTransform.position, PlayerController.Instance.collector.transform.position) < PlaygroundController.Instance.minFeatDist;
	}

    private void UpdateGas() {
        if (PlayerController.Instance.isInPortal) {
            return;
        } else if (isGasActive) {
            if (IsGasInReach() && !isGasCollected) {
                CollectGas();
            }
        }
    }

    private void GetOutOfThePortal() {
		for(int i = 0; i < IsPortalOutActive.Length; i++) {
			IsPortalOutActive[i] = false;
		}
        if (TutorialController.Instance.isBallonMoveTutorial) {
            float localPosPlayer = PlayerController.Instance.transform.localPosition.y;

            PlayerController.Instance.thisRigidBody.isKinematic = false;
            PlayerController.Instance.transform.localPosition = new Vector2(-2.7f, localPosPlayer);
            TutorialController.Instance.isBallonMoveTutorial = false;
        }
        PlayerController.Instance.isInPortal = false;
        PlaygroundController.Instance.ChangeAnimatorSpeed();
        PlayerController.Instance.transform.DOScale(PlaygroundController.PlayerSourceScale, 0.2f).OnUpdate(delegate {
            PlayerController.Instance.DrawRopes();
        });
        PlayerController.Instance.thisRigidBody.gravityScale = 0;
        PlayerController.Instance.isAttackable = true;

        AudioController.PlayStream(AudioSources.Instance.portal, LanguageAudio.GetSoundByName("Out", false));

        PlayerController.Instance.balloon.transform.Rotate(Vector3.zero);
        PlayerController.Instance.thisRigidBody.rotation = 0;

        PlayerController.Instance.thisCollider.enabled = true;

        PlayerController.Instance.charAnimator.enabled = true;
        PlayerController.Instance.klaxonAnimator.enabled = true;
    }

    private void GetInPortal() {

        PlayerController.Instance.charAnimator.enabled = false;
        PlayerController.Instance.klaxonAnimator.enabled = false;

        PlayerController.Instance.thisCollider.enabled = false;

        for (int i = 0; i < IsPortalInActive.Length; i++) {
            IsPortalInActive[i] = false;
        }

        AudioController.PlayStream(AudioSources.Instance.portal, LanguageAudio.GetSoundByName("In", false));

        PlayerController.Instance.transform.DOMoveY(portalsIn[0].transform.position.y, 0.2f).OnComplete(delegate {
            PlayerController.Instance.transform.DOMoveY(portalsOut[0].transform.position.y, 0.5f);
        });
        PlayerController.Instance.thisRigidBody.gravityScale = 0;
        PlayerController.Instance.thisRigidBody.velocity = Vector2.zero;

        PlayerController.Instance.isInPortal = true;
        PlayerController.Instance.transform.DOScale(Vector2.zero, 0.2f).OnUpdate(delegate {
            PlayerController.Instance.DrawRopes();
        });
    }

    private bool IsPortalInReach(GameObject portal, float dist, bool isIn) {
		if (isIn) {
			return Vector2.Distance( portal.transform.position, PlayerController.Instance.collector.transform.position) < dist;
		}
		return portal.transform.position.x <= PlayerController.Instance.collector.transform.position.x;
	}

    private void InitMagnets() {
        if (magnetPlaceholder != null) {
        	magnet = Instantiate(PlaygroundController.Instance.magnetPrefab, magnetPlaceholder.transform.position, magnetPlaceholder.transform.rotation) as GameObject;
        	magnetAnimator = magnet.GetComponent<Animator>();
        	magnetAnimator.enabled = true;
        	isMagnetActive = true;
        	magnet.transform.parent = magnetPlaceholder.transform;
        	magnet.transform.localPosition = Vector2.zero;
        }
    }

    private void UpdateMagnets() {
		if (magnet != null && !PlayerController.Instance.isInPortal && isMagnetActive)  {
			if (Vector2.Distance(magnet.transform.position, PlayerController.Instance.collector.transform.position) < PlaygroundController.Instance.minFeatDist) {
				isMagnetActive = false;
				magnet.transform.DOMove (PlayerController.Instance.magnetTransform.position, 0.2f).OnComplete(delegate{
					isMagnetActive = false;
				});
				PlaygroundController.Instance.minFeatDist = PlaygroundController.MAGNET_FEAT_DIST;
                DOVirtual.DelayedCall(15f, delegate {
                    PlaygroundController.Instance.minFeatDist = PlaygroundController.MIN_FEAT_DIST;
                    magnet.gameObject.SetActive(false);
                    PlayerController.Instance.magnet.SetActive(false);
                    PlaygroundController.Instance.EnableMagnets();                     
                });
                magnetAnimator.SetTrigger("stop");
                magnet.transform.parent = PlayerController.Instance.magnetTransform;
                PlaygroundController.Instance.DisableMagnets();
                PlayerController.Instance.magnet.SetActive(true);
			}
		}
	}

    private void UpdatePortals() {
		if(portalsIn.Count > 0) {
			if (!PlayerController.Instance.isInPortal) {
				for (int i = 0; i<portalsIn.Count; i++) { 
					if (IsPortalInActive[i]) {
						if (IsPortalInReach(portalsIn[i], PlaygroundController.Instance.minPortalInDist, true)) {
                            GetInPortal();
						}
					}
				}
			} else {
				for (int i = 0; i<portalsOut.Count; i++) { 
					if (IsPortalOutActive[i]) {
						if (IsPortalInReach(portalsOut[i], PlaygroundController.Instance.minPortalOutDist,false)) {
							GetOutOfThePortal();
                        }
					}
				}
			}
		}
	}

    public void RefreshEmeralds() {
        foreach (GameObject emerald in emeralds) {
            emerald.GetComponent<Animator>().enabled = true;
            emerald.transform.GetChild(1).gameObject.SetActive(true);
            emerald.transform.localPosition = Vector2.zero;
            emerald.gameObject.SetActive(false);
        }

        for (int i = 0; i < emeraldActivity.Length; i++) {
            emeraldActivity[i] = false;
        }
    }
    //************************************not_fixed************************************
    public void RefreshSuccubus() {
        if ((tag != "StartCell") && isHasSuccubus) {
            beast.isActive = false;
            Destroy(succubus);
            succubus = null;
            InitSuccubus();
        }
    }
    //************************************not_fixed************************************

    public void RefreshStars() {
        foreach (GameObject gold in golds) {            
            gold.transform.localPosition = Vector2.zero;
            gold.gameObject.SetActive(true);
		}

        for (int i = 0; i < starActivity.Length; i++) {
			starActivity[i] = true;
		}
    }

    public void RefreshGas() {
        gas.GetComponent<Animator>().enabled = true;
        gas.transform.localScale = Vector2.one * 0.35f;
        gas.transform.localPosition = Vector2.zero;
        isGasCollected = false;

        if (SaveData.currentLevelName == "Greece_level") {
            if (!isGasActive) {                
                isGasActive = true;
                gas.SetActive(true);
            }
        } else {
            isGasActive = false;
            gas.SetActive(false);
        }        
    }

    public void RefreshPortals() {
		for(int i = 0; i<IsPortalInActive.Length; i++) {
			IsPortalInActive[i] = true;
		}

		for(int i = 0; i<IsPortalOutActive.Length; i++) {
			IsPortalOutActive[i] = true;
		}
	}

    public void Init() {
        InitObstacles();
        InitStars();
        InitSuccubus();
        InitPortals();

        if (isHasGas) {
            InitGas();
        }
        
        InitMagnets();
        isDiamondScale = false;
        isJewelScale = false;
        isGasCollected = false;

        transform.parent = PlaygroundController.Instance.cellsGroup.transform;
        transform.position = Vector3.right * transform.position.x;
        gameObject.SetActive(false);
    }

    public void Process() {
        UpdateStars();
        //****************************not_fixed****************************
        if (isHasSuccubus && beast.isActive && (tag != "StartCell")) {
            beast.Process();
        }
        //****************************not_fixed****************************
        UpdatePortals();
		UpdateGas();
		UpdateMagnets();
    }	
}
