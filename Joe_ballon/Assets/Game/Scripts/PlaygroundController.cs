using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV_Prototype;
using DG.Tweening;

public class PlaygroundController : MonoBehaviour
{
	public static PlaygroundController Instance;

    private const float ACTIVE_CELLS_COUNT = 4;

    public const float NORMAL_SPEED = 2;
	public const float NORMAL_ACCELERATION = 1;
	public const float FORCED_SPEED = 4;
    public const float MIN_FEAT_DIST = 1.8f;
    public const float MAGNET_FEAT_DIST = 5f;
	public const float NORMAL_GRAVITY_SCALE = 0.2F;
	public const float MIN_BIRD_HIT_DIST = 2.2f;

    public const int THIRD_CAN_PRISE = 50;
	public const int HALF_CAN_PRISE = 100;
	public const int FULL_CAN_PRISE = 150;
	public const int MIN_GAS_COST = 50;
	
	public const float FULL_CAN_AMOUNT = 1f;
	public const float HALF_CAN_AMOUNT = 0.5f;
	public const float THIRD_CAN_AMOUNT = 0.3f;
    public const float EMERALDS_COUNT = 7;

    public static Vector3 PlayerSourceScale = new Vector3(0.65f,0.65f,1);

	public List<CellController> cellsPrefabs;
	public List<CellController> cells;
	public List<CellController> activeCells;
	public CellController startCell;

    public List<Transform> stars;

	public PlayerController player;
    public Transform playerTransform;

    public Transform cellsGroup;
    public Transform activeCellsGroup;
    public Transform buildingMiddleEdge;
    public Transform buildingMiddle;
    public Transform frontRoadEdge;
    public Transform frontRoad;
    public Transform animals;
    public Transform goldPos;
    public Transform gold;
    public Transform emerald;

    public Transform collectGold;
    public Transform pathBar;
    public Transform gasBar;
    public Transform ballon;

    public ButtonController musicButton;
	public ButtonController soundButton;
	public ButtonController buyButon;

    public GameObject goldPrefab;
    public GameObject bigEmeraldPrefab;
    
    public GameObject gasPrefab;
	public GameObject magnetPrefab; 
	public GameObject PortalInPrefab;
	public GameObject PortalOutPrefab;
	public GameObject Gui;

    public ParticleSystem flame;
    public ParticleSystem smoke;

    public GameState gameState;

    public GameObject stopGame;    
    public GameObject buyingMenu;
    public GameObject scoreTable;

    public GameObject goldBigPrefab;
    
    public GameObject succubusPrefab;

    public ButtonController openPausePannel_btn;
	public ButtonController closePausePannel_btn;

    public MultiButtonController klaxon_btn;
    public MultiButtonController forceUp_btn;

    public Alignment pausePannel;
    
    public int gasPrise    { get; set; }
	public float gasBought { get; set; }

    private GameObject _stop;

    private Dictionary<string, Vector3> scaler_bars;

    public Dictionary<string, BeastController> beasts;
    public Dictionary<string, float> nimStartDists;

    private float _fullDistance;
    private Dictionary<string, Vector2> spritesPos;

    private float warpSpeed = 0f;
	private float _minStarDist = 3.5f;
	private float _minPortalInDist = 1.5f;
	private float _minPortalOutDist = 0.3f;
    private float _minFeatDist = MIN_FEAT_DIST;

    private float _speed = NORMAL_SPEED;
	private float _acceleration = NORMAL_ACCELERATION;
    private float _oldAcceleration = NORMAL_ACCELERATION;
    private float _deltaAcceleration;

    private float cellOffset = 2f;
    public bool isPaused{ get; set; }

	public TextMesh BuyingMenuScores;
	public TextMesh gasPriseText;
	public TextMesh highScore;
	public TextMesh bestText;

    private float spawnPosBuilding;
    private float spawnPosRoad;

    private float buildingMiddlePos;
    private float frontRoadPos;

    private bool _isFlyingBallon;
    private bool _isForcingUp;
    private bool _isEndinglevel;

    private float length;
    private float baseDistance;

    private float playerPosX;
    private float emeraldPosX;
    private bool activeInFirst;
    private string level;

    public enum GameState {
		Playing,
		Buying,
		FinalState,
		StartState,
		Paused,
		Tutorial,
        Finish,
        ExitState,
	}

    private void Awake() {
		Instance = this;
    }

    private void Start() {
        Init();
    }

    private void Update() {
        if (isForcingUp) {
            if (!AudioSources.Instance.fireAudioSource.isPlaying) {
                AudioController.PlayStream(AudioSources.Instance.fireAudioSource, LanguageAudio.GetSoundByName("FireBoost"));
            }
        }
        InputManager.Instance.Process();
        Process();
    }

    private void OnDestroy() {
        StopAllMobsSounds();
    }

	public float speed             {get{return _speed;}set{_speed = value;} }
    public float acceleration      {get{return _acceleration;}set{ _acceleration = value;}}
    public float oldAcceleration   {get { return _oldAcceleration; } set { _oldAcceleration = value; } }
    public float deltaAcceleration {get{return _deltaAcceleration;}set{_deltaAcceleration = value;}}
    public float minStarDist       {get{return _minStarDist;}set{_minStarDist = value;}}
	public float minPortalInDist   {get{return _minPortalInDist;}}
	public float minPortalOutDist  {get{return _minPortalOutDist;}}
    public float minFeatDist   { get { return _minFeatDist; } set { _minFeatDist = value; } }
    public bool isFlyingBallon { get { return _isFlyingBallon; } set { _isFlyingBallon = value; } }
    public bool isForcingUp    { get { return _isForcingUp; } set { _isForcingUp = value; } }
    public bool isEndinglevel  { get { return _isEndinglevel; } set { _isEndinglevel = value; } }
    public float fullDistance  { get { return _fullDistance; } set { _fullDistance = value; } }

    public void NormalizeSpeed()
    {
		speed = NORMAL_SPEED;
	}

    private bool MovePlayground()
    {
        if (_stop)
        {
            if ((_stop.transform.position.x - playerTransform.position.x) < 1.5f)
            {
                isEndinglevel = true; DisableMobs();
                forceUp_btn.gameObject.SetActive(false);
                klaxon_btn.gameObject.SetActive(false);
                player.thisRigidBody.isKinematic = true;

                Transform stop_point = _stop.transform.GetChild(0);
                player.thisRigidBody.transform.DOMove(stop_point.position, 8.8f).OnUpdate(delegate 
                {
                    player.DrawRopes();
                });

                gameState = GameState.Finish;                              
                return false;
            }
        }

        if (player.isInPortal)
        {
            acceleration = 5;
        }
        else
        {
            if (player.isAnObsticalOnTheRight)
            {
                acceleration = acceleration > 0.07f ? deltaAcceleration : 0;
            }
            else
            {
                List<string> keys = new List<string>(beasts.Keys);
                foreach (string key in keys)
                {
                   if (beasts[key].isFrightActive)
                    {
                        acceleration = 0;
                    } else
                    {
                        oldAcceleration = acceleration;
                        acceleration = acceleration < 0.9f ? acceleration + Time.deltaTime * 0.5f : 1;
                    }
                }
            }
        }

        activeCellsGroup.transform.position += Vector3.left * Time.deltaTime * speed * acceleration * 0.95f;

        buildingMiddle.position = new Vector3(buildingMiddle.position.x, player.transform.localPosition.y * 0.05f - 2.025f); 

        buildingMiddle.position += Vector3.left * acceleration * Time.deltaTime * speed * .4f * 0.5f;

        frontRoad.position += Vector3.left * acceleration * Time.deltaTime * speed * 0.95f;

        if (buildingMiddleEdge.position.x < spawnPosBuilding)
        {
            buildingMiddle.position = new Vector3(buildingMiddlePos, buildingMiddle.position.y);
        }

        if (frontRoadEdge.position.x < spawnPosRoad)
        {
            frontRoad.position = new Vector3(frontRoadPos, frontRoad.position.y);
        }  

        ScoreController.Instance.score += acceleration;

        return true;
    }

    public void InitScore()
    {
        AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-19", true));
        BuyingMenuScores.text = ScoreController.Instance.diamonds.ToString();
        scoreTable.transform.DOScale(1.05f,0.3f).SetEase(Ease.OutBack);
		highScore.text = ((int)(ScoreController.Instance.score*0.1f)).ToString();       

		if((int)(ScoreController.Instance.score*0.1f) > PlayerPrefs.GetInt("Best"))
        {
			PlayerPrefs.SetInt ("Best",(int)(ScoreController.Instance.score*0.1f));
			bestText.text = ((int)(ScoreController.Instance.score*0.1f)).ToString();
		}
        else
        {
			bestText.text =  PlayerPrefs.GetInt("Best").ToString();
		}
    }

    public void InitBuyingMenu()
    {
        ScoreController.Instance.UpdateScores();
        if (SaveManager.GetTipsStatus())
        {
            if (SaveManager.GetGasTutorial())
            {
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-16", true));
                TutorialController.Instance.IncTutorials();
            }            
        }
        buyingMenu.transform.DOScale(1.3f,0.3f).SetEase(Ease.OutBack);
		gasPriseText.text = gasPrise.ToString();

        if (gasPrise > ScoreController.Instance.diamonds)
        {
            gasPriseText.color = Color.red;
        }
        else
        {
            gasPriseText.color = new Color(0.082f, 0.62f, 0.086f,1f);
        }

        if (ScoreController.Instance.diamonds < gasPrise)
        {
			buyButon.collider.enabled = false;
            buyButon.button_renderer.sprite = buyButon.sprites[1];
        }
        else
        {
			buyButon.collider.enabled = true;
            buyButon.button_renderer.sprite = buyButon.sprites[0];
        }
    }

    private void SetScaleDefault()
    {
        switch (level) {
            case "Greece_level":
                collectGold.DOScale(scaler_bars["Gold_bar_greece"], 0.5f);
                break;
            case "Egypt_level":
                collectGold.DOScale(scaler_bars["Gold_bar_egypt"],  0.5f);
                break;
            case "Latin_level":
                collectGold.DOScale(scaler_bars["Gold_bar_latin"],  0.5f);
                break;
        }
        pathBar.DOScale(new Vector3(4.6f, 0.36f, 1f), 0.5f);
        gasBar.DOScale( new Vector3(0.9f, 0.9f,  1f), 0.5f);
        ballon.gameObject.SetActive(true);
    }

    public void PauseTween()
    {
        foreach (Transform animal in animals)
        {
            if (animal.GetComponent<BeastController>().isActive)
            {
                animal.GetChild(0).DOPause();
            }
        }
        player.balloon.DOPause();
    }

    public void PlayTween()
    {
        foreach (Transform animal in animals)
        {
            if (animal.GetComponent<BeastController>().isActive)
            {
                animal.GetChild(0).DOPlay();
            }
        }
        player.balloon.DOPlay();
    }

    public void SoundPause()
    {
        List<string> categories = new List<string>()
        {
            "Attack", "Dawn", "Walk", "Fly", "Run"
        };

        foreach (Transform beast in animals)
        {
            foreach (string category in categories)
            {
                string key = beast.tag + category;
                if (AudioSources.Instance.mobsSound.ContainsKey(key))
                {
                    if (AudioSources.Instance.mobsSound[key].isPlaying)
                    {
                        AudioSources.Instance.mobsSound[key].Pause();
                    }
                }
            }                
        }

        List<string> keys = new List<string>(AudioSources.Instance.sounds.Keys);
        foreach (string key in keys)
        {
            if (AudioSources.Instance.sounds[key].isPlaying)
            {
                AudioSources.Instance.sounds[key].Pause();
            }
        }
    }

    public void StopAllMobsSounds()
    {
        List<string> keys = new List<string>(AudioSources.Instance.mobsSound.Keys);
        foreach (string key in keys)
        {
            if (AudioSources.Instance.mobsSound[key].isPlaying)
            {
                AudioController.StopStream(AudioSources.Instance.mobsSound[key]);
            }
        }
    }

    public void SoundPlay() {
        List<string> keys = new List<string>(AudioSources.Instance.mobsSound.Keys);
        foreach (string key in keys)
        {
            AudioSources.Instance.mobsSound[key].UnPause();
        }

        keys = new List<string>(AudioSources.Instance.sounds.Keys);
        foreach (string key in keys)
        {
            AudioSources.Instance.sounds[key].UnPause();
        }
    }

    public void Pause() {

        if (!isPaused)
        {            
            SoundPause();            
            ballon.gameObject.SetActive(false);
            collectGold.DOScale(0f, 0.5f); pathBar.DOScale(0f, 0.5f);

            if (player.isSmokeActive)
            {
                ScoreController.Instance.enabledSmoke = false;
                smoke.gameObject.SetActive(false);
            }
            PauseTween();
            gasBar.DOScale(0f, 0.5f).OnComplete(delegate 
            {
                Time.timeScale = 0;
                forceUp_btn.gameObject.SetActive(false);
                klaxon_btn.gameObject.SetActive(false);
                isPaused = true;
                DOTween.defaultTimeScaleIndependent = true;
                openPausePannel_btn.collider.enabled = false;
                openPausePannel_btn.transform.localScale = Vector2.zero;
                closePausePannel_btn.gameObject.SetActive(true);
                closePausePannel_btn.transform.DOScale(Vector2.one * 0.6f, 0.2f).SetEase(Ease.OutBack);
                pausePannel.transform.DOScale(Vector2.one * 0.65f, 0.2f).SetEase(Ease.OutBack);                
                gameState = GameState.Paused;
            });                 
		}
        else
        {
			Time.timeScale = 1;
            
            SoundPlay();
            ScoreController.Instance.enabledSmoke = true;
            PlayTween();
            SetScaleDefault();		
            forceUp_btn.gameObject.SetActive(true);
            klaxon_btn.gameObject.SetActive(true);

            forceUp_btn.ButtonUp();
            klaxon_btn.ButtonUp();
            isPaused = false;
            closePausePannel_btn.transform.DOScale(Vector2.zero, 0.1f).OnComplete(delegate 
            {
                closePausePannel_btn.gameObject.SetActive(false);
                openPausePannel_btn.transform.DOScale(Vector2.one * 0.45f, 0.1f).OnComplete(delegate 
                {
                    openPausePannel_btn.collider.enabled = true;
                });
            });
            pausePannel.transform.DOScale(Vector2.zero, 0.2f).SetEase(Ease.Linear);
            gameState = GameState.Playing;
		}
	}

    private void InitCells()
    {
        CreateCellsFromPrefabs();
        initActiveCells();
    }

    private void CreateCellsFromPrefabs()
    {
        foreach (CellController cell in cellsPrefabs)
        {
            GameObject temp = Instantiate(cell.gameObject) as GameObject;
            temp.GetComponent<CellController>().goldPos = gold;
            temp.GetComponent<CellController>().emeraldPos = emerald;
            cells.Add(temp.GetComponent<CellController>());            
        }
        foreach (CellController cell in cells)
        {
            cell.Init();
        }
    }

    public void DisableMagnets()
    {
		foreach (CellController cell in cells)
        {
			if(cell.magnet!=null) {
				cell.isMagnetActive = false;
				cell.magnet.SetActive(false);
			}
		}

        foreach (CellController cell in activeCells)
        {
			if(cell.magnet!=null) {
				cell.isMagnetActive = false;
				cell.magnet.SetActive(false);
			}
		}
	}

    public void EnableMagnets()
    {
		foreach (CellController cell in cells)
        {
			if(cell.magnet!=null) {
				cell.isMagnetActive = true;
				cell.magnet.SetActive(true);
				cell.magnet.transform.SetParent(cell.magnetPlaceholder);
				cell.magnet.transform.localPosition = Vector2.zero;
                cell.magnet.transform.eulerAngles = Vector2.zero;
            }
		}

		foreach (CellController cell in activeCells)
        {
			if(cell.magnet!=null) {
				cell.magnet.transform.SetParent(cell.magnetPlaceholder);
				cell.magnet.transform.localPosition = Vector2.zero;
			}
		}

		for(int i = 1; i < activeCells.Count;i++)
        {
			if(activeCells[i].magnet!=null) {
				activeCells[i].isMagnetActive = true;
				activeCells[i].magnet.SetActive(true);
			}
		}
	}

    public void ChangeAnimatorSpeed()
    {
        foreach (Transform animal in animals)
        {
            if (animal.gameObject.name != "Succubus")
            {
                if (animal.GetComponent<Animator>().enabled)
                {
                    animal.GetComponent<Animator>().speed = 1f;
                }
            }                           
        }
    }    

    private void initActiveCells()
    {
        activeCells.Add((Instantiate(startCell.gameObject) as GameObject).GetComponent<CellController>());
        activeCells[0].Init();
        activeCells[0].gameObject.transform.parent = activeCellsGroup.transform;
        activeCells[0].gameObject.transform.position = Vector2.right * GetScreen.left;
        activeCells[0].gameObject.SetActive(true);

        for (int i = 1; i < ACTIVE_CELLS_COUNT; i++)
        {
            int rand = UnityEngine.Random.Range(0, cells.Count-1);

            activeCells.Add(cells[rand]);
            cells.RemoveAt(rand);
            activeCells[i].transform.parent = activeCellsGroup.transform;
            activeCells[i].gameObject.SetActive(true);
            activeCells[i].gameObject.transform.position = activeCells[i-1].edge.transform.position;
        }       
    }

    private void UpdateCellsCount()
    {
        if (StopGame()) return;        
        if (activeCells[0].edge.transform.position.x < GetScreen.left - cellOffset)
        {
            //****************************not_fixed****************************
            activeCells[0].RefreshSuccubus();
            //****************************not_fixed****************************
            activeCells[0].gameObject.SetActive(false);
            activeCells[0].RefreshStars();
            activeCells[0].RefreshEmeralds();
            activeCells[0].RefreshPortals();

            //****************************change_now****************************
            if ((activeCells[0].tag != "StartCell") && activeCells[0].isHasGas)
            {
                activeCells[0].RefreshGas();
            }            
            //****************************change_now****************************

            if (activeCells[0].tag != "StartCell")
            {
                cells.Add(activeCells[0]);
            }

            cells[cells.Count-1].gameObject.transform.parent = cellsGroup.transform;
            cells[cells.Count-1].gameObject.transform.position = Vector3.zero;
            ScoreController.Instance.availableGold += activeCells[0].starsPlaceholders.Count;

            activeCells.RemoveAt(0);

            int rand = UnityEngine.Random.Range(0, cells.Count);
            activeCells.Add(cells[rand]);           

            cells.RemoveAt(rand);
            activeCells[activeCells.Count-1].transform.parent = activeCellsGroup.transform;
            activeCells[activeCells.Count-1].gameObject.SetActive(true);
            activeCells[activeCells.Count-1].gameObject.transform.position = activeCells[activeCells.Count-2].edge.transform.position;
        }

        if (level.Equals("Greece_level"))
        {
            /**/
        } else {
            UpdatedEmeraldActivity();
        }
    }

    private bool StopGame()
    {
        if ((ScoreController.Instance.distance >= fullDistance) || CheckEmeraldCount())
        {
            if (!_stop)
            {
                for (int index = activeCells.Count - 1; index > 0; --index)
                {
                    activeCells[index].transform.parent = cellsGroup.transform;
                    activeCells[index].gameObject.SetActive(false);
                    cells.Add(activeCells[index]);
                    activeCells.Remove(activeCells[index]);
                }

                CellController lastCell = activeCells[0];
                _stop = Instantiate(stopGame, lastCell.edge.transform.position, lastCell.edge.transform.rotation) as GameObject;
                _stop.transform.parent = activeCellsGroup.transform;
            }
            return true;
        }
        return false;
    }

    private bool CheckEmeraldCount()
    {
        switch (level) {
            case "Greece_level":
                return (SaveManager.GetCountGreeceEmeralds() == EMERALDS_COUNT) && !SaveManager.IsGreeceComplete();
            case "Egypt_level":
                return (SaveManager.GetCountEgyptEmeralds() == EMERALDS_COUNT) && !SaveManager.IsEgyptComplete();
            case "Latin_level":
                return (SaveManager.GetCountAmericaEmeralds() == EMERALDS_COUNT) && !SaveManager.IsLatinComplete();
        }
        return false;
    }

    private void UpdatedEmeraldActivity()
    {
        if (ScoreController.Instance.distance >= length)
        {
            playerPosX = player.playerPosX;           
            ActivatedEmerald(1);
            length += baseDistance;
        }
    }
    
    private void ActivatedEmerald(int index)
    {
        for (int i = 0; i < activeCells[index].emeralds.Count; i++)
        {            
            emeraldPosX = activeCells[index].emeralds[i].transform.position.x;
            if (playerPosX < emeraldPosX) {
                activeCells[index].emeralds[i].SetActive(true);
                activeCells[index].emeraldActivity[i] = true;
                break;
            }
        }
    }

    private void UpdateCells()
    {
        if (!activeCells.Count.Equals(0))
        {
            foreach (CellController cell in activeCells)
            {
                cell.Process();
            }
            UpdateCellsCount();
            if (level != "Greece_level") {
                if (activeCells.Count >= 2)
                {
                    if (ScoreController.Instance.isGasNeeded)
                    {
                        activeCells[1].gasTransform.GetChild(0).gameObject.SetActive(true);
                        activeCells[1].isGasActive = true;
                        ScoreController.Instance.isGasNeeded = false;
                    }
                }
            }            
        }
    }

    public void InitBeasts()
    {
        beasts = new Dictionary<string, BeastController>();
        foreach (Transform beast in animals)
        {
            beasts.Add(beast.tag, beast.GetComponent<BeastController>());            
        }

        List<string> keys = new List<string>(beasts.Keys);
        foreach (string key in keys)
        {
            beasts[key].Init();
        }
    }

    private void DisableMobs()
    {
        foreach (Transform animal in animals)
        {
            if (animal.gameObject.activeSelf)
            {
                animal.GetChild(0).gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }

    private void InitMinDist() {
        if (level.Equals("Greece_level")) {
            nimStartDists = new Dictionary<string, float>
            {
                { "Centaur", 1.7f}, {"Succubus", 2.7f}, {"Harpy", 2.5f}, {"Fish", 2.8f}
            };
            fullDistance = 350;
        }
        else if (level.Equals("Egypt_level")) {
            nimStartDists = new Dictionary<string, float>
            {
                { "Anubis", 1.2f}, {"Mummy", 1.5f}, {"Scarab", 3.6f}, {"Wasp", 2.6f}
            };
            fullDistance = 350;
        }
        else if (level.Equals("Latin_level"))
        {
            nimStartDists = new Dictionary<string, float> {
                { "Mankey", 1.5f}, {"Golem", 1.5f}, {"Bat", 2.8f}, {"Dragon", 2.8f}
            };
            fullDistance = 350;
        }        
    }

    private void InitPosSprite() {
        if (level.Equals("Greece_level")) {
            spritesPos = new Dictionary<string, Vector2> {
                { "Building_middle", new Vector2(-5.0f, -2.01f) }, { "Front_road", new Vector2(2.5f, frontRoad.position.y) }
            };
        } else if (level.Equals("Egypt_level") || level.Equals("Latin_level")) {
            spritesPos = new Dictionary<string, Vector2> {
                { "Building_middle", new Vector3(-5.5f, -2.27f) }, { "Front_road", new Vector3(15.5f, frontRoad.position.y) }
            };
        }
    }

    private void InitScalerBars() {
        scaler_bars = new Dictionary<string, Vector3>();

        scaler_bars.Add("Gold_bar_greece", new Vector3(0.9f,  0.9f,  1f));
        scaler_bars.Add("Gold_bar_egypt",  new Vector3(1.2f,  1.2f,  1f));
        scaler_bars.Add("Gold_bar_latin",  new Vector3(1.17f, 1.17f, 1f));
    }

    private void PlaySounds() {
        switch (level) {
            case "Greece_level":
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-3", true));
                break;
            case "Egypt_level":
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-8", true));
                break;
            case "Latin_level":
                AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-9", true));
                break;
        }
    }

    private void InitGUI() {
		Gui.SetActive (true);
        gasPrise = MIN_GAS_COST;
        soundButton.Init();
        musicButton.Init();
        pausePannel.SetAlignment();

        closePausePannel_btn.transform.localScale = Vector2.zero;

        buyingMenu.transform.localScale = Vector2.zero;
        buyingMenu.transform.localPosition = Vector2.zero;
        scoreTable.transform.localScale = Vector2.zero;

        scoreTable.transform.localPosition = Vector2.zero;
        pausePannel.transform.localScale = Vector2.zero;
    }

    private void Init() {        
        isPaused = false;
		minFeatDist = MIN_FEAT_DIST;        
        InitGUI();        
        InitScalerBars();
        AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-2", true));
        level = SaveData.currentLevelName;
        PlaySounds();
        isFlyingBallon = false;
        isForcingUp = false;
        isEndinglevel = false;
        InitMinDist();
        InitPosSprite();
        baseDistance = fullDistance / EMERALDS_COUNT;
        length = baseDistance - 15;
        activeInFirst = false;
        player.Init();
        InitBeasts();    
        ScoreController.Instance.Init();        
        InitCells();
        EventManager.Instance.Init();
        gameState = GameState.Playing;
        AudioController.PlayMusic("GameMusic");
        Time.timeScale = 1;
		acceleration = 1;
		speed = 2;
        buildingMiddle.position = spritesPos["Building_middle"];
        frontRoad.position      = spritesPos["Front_road"]; 

        spawnPosBuilding = buildingMiddle.GetChild(0).position.x;
        buildingMiddlePos = buildingMiddle.position.x;

        spawnPosRoad = frontRoad.GetChild(0).position.x;
        frontRoadPos = frontRoad.position.x;

        if (level.Equals("Greece_level")) {
            TutorialController.Instance.Init();
        }            
    }

    private void Process() {
        switch(gameState) {
			case GameState.Playing:
                if (!MovePlayground()) {
                    break;
                }

                UpdateCells();
                player.Process();                
                ScoreController.Instance.Process();
                List<string> keys = new List<string>(beasts.Keys);
                foreach (string key in keys) {
                    if (beasts[key].isActive) {
                        beasts[key].Process();
                    }
                }
                if (level.Equals("Greece_level")) {
                    TutorialController.Instance.UpdateTutorial();

                    //****************last_changes****************
                    TutorialController.Instance.UpdateFuelTutorial();
                    //****************last_changes****************
                }
                break;
            case GameState.Finish:
                SceneLoader.Instance.SwitchToScene(Scenes.Progress);
                break;

            case GameState.Tutorial:
                /**/
			    break;

            case GameState.Paused:
                return;

            case GameState.StartState:
               if (Input.touchCount > 0) {
                  gameState = GameState.Playing;
               }
#if UNITY_5_1
               if(Input.GetMouseButtonDown(0)) {
                  gameState = GameState.Playing;
               }
#endif
			   break;
		}
    }
}

