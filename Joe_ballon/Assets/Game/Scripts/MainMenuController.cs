﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    public GameObject[] menu;

    private string menuMusik = "MenuMusik";

    void Start() {
        SaveManager.CreateAndLoadData();
        string name = SaveManager.GetCurrentLevelname();
        Time.timeScale = 1f;

        switch (name) {
            case "Greece_level":
                menu[0].SetActive(true);
                AudioController.PlayStream(AudioSources.Instance.seaAudioSource, LanguageAudio.GetSoundByName("Sea", false), true);
                AudioController.PlayStream(AudioSources.Instance.gullAudioSource, LanguageAudio.GetSoundByName("Gulls", false), true);
                break;
            case "Egypt_level":
                menu[1].SetActive(true);
                AudioController.PlayStream(AudioSources.Instance.desertAudioSource, LanguageAudio.GetSoundByName("Desert", false), true);
                break;
            case "Latin_level":
                menu[2].SetActive(true);
                AudioController.PlayStream(AudioSources.Instance.foresAudioSource, LanguageAudio.GetSoundByName("Forest", false), true);
                break;
        }
        AudioController.PlayMusic(menuMusik);        
    }

    void Update() {
        InputManager.Instance.Process();
    }
}
