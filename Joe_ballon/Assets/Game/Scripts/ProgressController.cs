﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Tutorials;
using PSV_Prototype;

public class ProgressController : MonoBehaviour
{
    public static ProgressController Instance;

    public const int EMERALDS_COUNT = 7;

    private int countGreeceEmeralds;
    private int countEgyptEmeralds;
    private int countAmericaEmeralds;

    private int g_countEmeralds;
    private int e_countEmeralds;
    private int a_countEmeralds;

    private int emeraldsCollected;

    public GameObject[] last_emerald;

    public Transform[] emeraldsPlaceholdersGroup;

    public SpriteRenderer[] inactive_icons;

    public ButtonController[] intro_btn;
    public ButtonController[] level_btn;

    public BoxCollider2D[] i_colliders;
    public BoxCollider2D[] l_colliders;

    public ParticleSystem[] light_fx;

    public GameObject[] lights;

    private string level;

    private bool isOnceCall = false;

    //*************last_changes*************
    public GameObject[] locks;
    //*************last_changes*************

    private Dictionary<string, int> level_index;
    private Dictionary<string, ButtonController> levels;

    void Awake()
    {
        Instance = this;

        level_index = new Dictionary<string, int>();
        levels      = new Dictionary<string, ButtonController>();

        level_index.Add("Greece_level", 0);
        level_index.Add("Egypt_level",  1);
        level_index.Add("Latin_level",  2);

        levels.Add("Greece_level", intro_btn[0]);
        levels.Add("Egypt_level",  intro_btn[1]);
        levels.Add("Latin_level",  intro_btn[2]);
    }

    void Start()
    {
        AudioController.StopMusic(AudioController.GetMusicSrc());

        level = SaveData.currentLevelName;

        g_countEmeralds = SaveManager.GetCountGreeceEmeralds();
        countGreeceEmeralds = g_countEmeralds + SaveData.collectedGreeceEmeralds;
        SaveManager.SetCountGreeceEmeralds(countGreeceEmeralds);
        SaveData.collectedGreeceEmeralds = 0;

        e_countEmeralds = SaveManager.GetCountEgyptEmeralds();
        countEgyptEmeralds = e_countEmeralds + SaveData.collectedEgyptEmeralds;
        SaveManager.SetCountEgyptEmeralds(countEgyptEmeralds);
        SaveData.collectedEgyptEmeralds = 0;

        a_countEmeralds = SaveManager.GetCountAmericaEmeralds();
        countAmericaEmeralds = a_countEmeralds + SaveData.collectedLatinEmeralds;
        SaveManager.SetCountAmericaEmeralds(countAmericaEmeralds);
        SaveData.collectedLatinEmeralds = 0;

        //**************************last_changes**************************
        if (SaveManager.IsEgyptOpened())
        {
            locks[0].GetComponent<SpriteRenderer>().DOFade(0f, 0.5f).OnComplete(delegate
            {
                locks[0].SetActive(false);
            });
        }

        if (SaveManager.IsLatinOpened())
        {
            locks[1].GetComponent<SpriteRenderer>().DOFade(0f, 0.5f).OnComplete(delegate
            {
                locks[1].SetActive(false);
            });
        }
        //**************************last_changes**************************

        ShowAvailableEmeralds();
        ShowCollectedEmeralds();
    }

    public void SelectionLevel(int index)
    {
        level_btn[index].transform.DOPunchScale(new Vector3(-0.2f, -0.2f, -0.2f), 0.3f).OnStart(delegate 
        {
            level_btn[index].collider.enabled = false;
            lights[index].SetActive(true);
            LoadLevel();
        });              
   }

    public int GetIndexLevel(string level)
    {
        List<string> keys = new List<string>(levels.Keys);
        foreach (string key in keys)
        {
            if (key.Equals(level))
            {
                return level_index[key];
            }
        }
        return 0;
    }

    public void SelectionIntro()
    {
        List<string> keys = new List<string>(levels.Keys);
        foreach (string key in keys)
        {
            if (key.Equals(level))
            {
                levels[key].transform.DOPunchScale(new Vector3(-0.2f, -0.2f, -0.2f), 0.3f).OnStart(delegate 
                {
                    ShowIntro();
                });
            }
        }
    }

    private void LoadLevel()
    {
        switch (level)
        {
            case "Greece_level":
                SceneLoader.Instance.SwitchToScene(Scenes.GreeceLevel);
                break;
            case "Egypt_level":
                SceneLoader.Instance.SwitchToScene(Scenes.EgyptLevel);
                break;
            case "Latin_level":
                SceneLoader.Instance.SwitchToScene(Scenes.AmericaLevel);
                break;
        }
   }

    private void ShowIntro()
    {
        SceneLoader.Instance.SwitchToScene(Scenes.Finish);
    }

    private void ShowAvailableEmeralds()
    {

        if (g_countEmeralds.Equals(0)) return;
        else if (g_countEmeralds >= EMERALDS_COUNT)
        {            
            if (SaveManager.IsGreeceComplete())
            {
                inactive_icons[0].gameObject.SetActive(false);
                i_colliders[0].enabled = true;
            }
            SetActiveEmeralds(EMERALDS_COUNT, 0);
        } else SetActiveEmeralds(g_countEmeralds, 0);

        if (e_countEmeralds.Equals(0)) return;
        else if (e_countEmeralds >= EMERALDS_COUNT)
        {
            if (SaveManager.IsEgyptComplete())
            {
                inactive_icons[1].gameObject.SetActive(false);
                i_colliders[1].enabled = true;
            }
            SetActiveEmeralds(EMERALDS_COUNT, 1);
        } else SetActiveEmeralds(e_countEmeralds, 1);

        if (a_countEmeralds.Equals(0)) return;
        else if (a_countEmeralds >= EMERALDS_COUNT)
        {
            if (SaveManager.IsLatinComplete())
            {
                inactive_icons[2].gameObject.SetActive(false);
                i_colliders[2].enabled = true;
            }
            SetActiveEmeralds(EMERALDS_COUNT, 2);
        } else SetActiveEmeralds(a_countEmeralds, 2);
    }

    private void SetActiveEmeralds(int countEmeralds, int _iLevel)
    {
        int count = 0;
        foreach (Transform pl in emeraldsPlaceholdersGroup[_iLevel])
        {
            if (count.Equals(countEmeralds))
            {
                break;
            }
            pl.GetChild(0).gameObject.SetActive(true);
            ++count;
        }
    }

    private void ShowCollectedEmeralds()
    {

        switch (level)
        {
            case "Greece_level":
                if (g_countEmeralds >= EMERALDS_COUNT) return;
                else ShowCollectedEmeralds(0, g_countEmeralds, countGreeceEmeralds);
                break;
            case "Egypt_level":
                if (e_countEmeralds >= EMERALDS_COUNT) return;
                else ShowCollectedEmeralds(1, e_countEmeralds, countEgyptEmeralds);
                break;
            case "Latin_level":
                if (a_countEmeralds >= EMERALDS_COUNT) return;
                else ShowCollectedEmeralds(2, a_countEmeralds, countAmericaEmeralds);
                break;
        }
    }

    private void ShowCollectedEmeralds(int iLevel, int collectedEmeralds, int countLevelEmeralds)
    {
        float delay = 0.5f;
        Transform point;
        ParticleSystem splash;

        for (int index = collectedEmeralds; (index < countLevelEmeralds) && (index < emeraldsPlaceholdersGroup[iLevel].childCount); ++index)
        {
            point = emeraldsPlaceholdersGroup[iLevel].GetChild(index);
            splash = point.GetChild(1).gameObject.GetComponent<ParticleSystem>();
            ScaleEfects(point.GetChild(0).gameObject, splash, 0.32f, countLevelEmeralds, iLevel, index, delay);
            delay += 0.5f;
        }
    }

    private void ScaleEfects(GameObject go, ParticleSystem particle, float scale, int collectedEmeralds, int index, int numIter, float delay = 0)
    {
        go.SetActive(true);
        go.transform.localScale = Vector2.zero;
        go.transform.DOScale(Vector2.one * scale, 0.5f).SetEase(Ease.OutBounce).SetDelay(delay).OnStart(delegate 
        {
            AudioController.PlayStream(AudioSources.Instance.displayAudioSource, LanguageAudio.GetSoundByName("Display"));
            particle.Play();

        }).OnComplete(delegate {
            if (numIter.Equals(collectedEmeralds-1) || collectedEmeralds > EMERALDS_COUNT)
            {
                if (last_emerald[index].activeSelf)
                {
                    DOVirtual.DelayedCall(1.5f, delegate 
                    {
                        ActivatedButton();
                    });
                } else if (level == "Greece_level")
                {
                    if (SaveManager.GetRestartTutorial())
                    {
                        ProgressTutorial.Instance.ShowRestartTutorial();
                        StartCoroutine(DelayedCall(3.5f, Tutorial.Restartlevel, index));
                    }
                }
                else if (level == "Egypt_level" || level == "Latin_level")
                {
                    AudioController.PlayStream(AudioSources.Instance.sound, LanguageAudio.GetSoundByName("hp-18", true));
                    l_colliders[index].enabled = true;
                }
            }
        });
    }

    private void ActivatedButton()
    {
        switch (level)
        {
            case "Greece_level":
                SaveManager.SetGreeceStatus("Complete");
                light_fx[0].Play();
                inactive_icons[0].DOFade(0f, 0.7f).OnComplete(delegate 
                {
                    //if (level == "Greece_level")
                    //{
                        if (SaveManager.GetLevelTutorial())
                        {
                            if (!isOnceCall)
                            {
                                isOnceCall = true;
                                ProgressTutorial.Instance.ShowLevelTutorial();
                                StartCoroutine(DelayedCall(3.5f, Tutorial.OpenLevel));
                            }                            
                        }
                    //}   
                });
                break;
            case "Egypt_level":
                SaveManager.SetEgyptStatus("Complete");
                light_fx[1].Play();

                inactive_icons[1].DOFade(0f, 0.7f).OnComplete(delegate 
                {
                    inactive_icons[1].gameObject.SetActive(false);
                    i_colliders[1].enabled = true;
                });
                break;
            case "Latin_level":
                SaveManager.SetLatinStatus("Complete");
                light_fx[2].Play();

                inactive_icons[2].DOFade(0f, 0.7f).OnComplete(delegate 
                {
                    inactive_icons[2].gameObject.SetActive(false);
                    i_colliders[2].enabled = true;
                });                               
                break;
        }
    }

    private IEnumerator DelayedCall(float delay, Tutorial tutorial, int index = 0)
    {
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }

        if (tutorial.Equals(Tutorial.Restartlevel))
        {
            l_colliders[index].enabled = true;
            SaveManager.DisableRestartTutorial(1);

        } else if (tutorial.Equals(Tutorial.OpenLevel))
        {
            inactive_icons[0].gameObject.SetActive(false);
            i_colliders[0].enabled = true;
            SaveManager.DisableShowLevelTutorial(1);
        }
    }

    void Update()
    {
        InputManager.Instance.Process();
    }
}

