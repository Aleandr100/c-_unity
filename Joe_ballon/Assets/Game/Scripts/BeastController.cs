﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BeastController : MonoBehaviour {

    private static float MAX_SPEED_MOVE = 0.9f;
    private static float MIN_SPEED_MOVE = 0.35f;

    public const float MIN_SPAWN_DIST = -7.2f;

    public float maxScaleX;
    public float minScaleX;

    public bool isHasAttacked{get;set;}
	public bool isHasBitten{get;set;}
	public bool isHasBeenHit{get;set;}

    public GameObject Mob;
    private Animator animator;

    public BoxCollider2D collider2D;
    public Transform body;

    public bool isActive = false;
    public bool isFlying = false;

    public Transform shadow;

    private float acceleration;
    private float oldAcceleration;
    private bool _isFrightActive;

    private bool isScaredSuccubus;

    private float startSuccubusPosX;
    private float speed = .025f;

    private float startPosY;
    
    private bool isCalmness = true;    

    public bool isFrightActive { get { return _isFrightActive; } set { _isFrightActive = value; } }        

    private void ChangeAnimatorSpeed() {
        foreach (string mob in new[] { "Anubis", "Mummy", "Golem" }) {
            if (mob == tag) {
                if (mob == "Anubis" || mob == "Mummy") {
                    acceleration = PlaygroundController.Instance.acceleration;
                    oldAcceleration = PlaygroundController.Instance.oldAcceleration;
                } else {
                    acceleration = PlaygroundController.Instance.acceleration;
                    oldAcceleration = PlaygroundController.Instance.oldAcceleration;
                }
                if (acceleration >= oldAcceleration) {
                    animator.speed = Mathf.Lerp(animator.speed, MAX_SPEED_MOVE, speed);
                } else {
                    animator.speed = Mathf.Lerp(animator.speed, MIN_SPEED_MOVE, speed);
                }
                break;
            }
        }
    }    

    private bool IsAllowedToAttack() {
        return IsAllowedToApproach() && Mathf.Abs(body.position.y - PlayerController.Instance.transform.position.y) < 1.1f && !isHasBeenHit; ;        
    }

    private bool CheckPositionMob() {
        return (transform.position.x < PlayerController.Instance.transform.position.x || Mathf.Abs(transform.position.x - PlayerController.Instance.transform.position.x) < 0.3f);
    }

    private bool IsAllowedToApproach() {        
        return Mathf.Abs(transform.position.x - PlayerController.Instance.transform.position.x) < PlaygroundController.Instance.nimStartDists[tag] && !isHasAttacked && !PlayerController.Instance.isInPortal && !CheckPositionMob(); ;
    }

    private void Attack() {

        float xScale;
        isHasAttacked = true;
        if (isFlying) {
            EventManager.Instance.DisableSound(tag + "Fly");
        } else {
            if (tag != "Succubus") {
                EventManager.Instance.DisableSound(tag + "Walk");
            }            
        }
        EventManager.Instance.EnabledSound(tag + "Attack");
        if (name == "Centaur" || name == "Mankey" || name == "Anubis" || name == "Mummy" || name == "Golem") {            
            shadow.gameObject.SetActive(true);
            Mob.GetComponent<Animator>().SetTrigger("Jump");
            Mob.transform.DOMoveY(PlayerController.Instance.transform.position.y - 0.1f, 0.6f).OnUpdate(delegate {
                xScale = Mathf.Lerp(shadow.localScale.x, minScaleX, 0.03f);
                shadow.localScale = new Vector3(xScale, shadow.localScale.y);
            }).OnComplete(delegate {
                Mob.transform.DOLocalMoveY(startPosY, 0.445f).OnUpdate(delegate {
                    xScale = Mathf.Lerp(shadow.localScale.x, maxScaleX, 0.075f);
                    shadow.localScale = new Vector3(xScale, shadow.localScale.y);
                }).OnComplete( delegate {
                    if (name != "Golem") {
                        shadow.gameObject.SetActive(false);
                    }                    
                    Mob.GetComponent<Animator>().SetTrigger("Move");
                    EventManager.Instance.EnabledSound(tag + "Walk", false, true);
                });
            });
        } else if (name == "Succubus(Clone)") {
            Mob.GetComponent<Animator>().SetTrigger("Jump");
            StartCoroutine(StartSuccubusRun(() => {
                Mob.transform.DOMove(PlayerController.Instance.transform.position, 0.45f).OnUpdate(delegate {
                    if (isScaredSuccubus) {
                        Mob.transform.DOKill();
                        if (Mathf.Abs(Mob.transform.position.x - startSuccubusPosX) > 1.5f) {
                            Mob.transform.DOMoveY(-3.2f, 1.2f);
                        }                        
                    }
                }).
                //*********************************************not_fixed*********************************************
                OnComplete(delegate {
                    Mob.transform.DOMoveY(-3.2f, 0.3f).OnComplete(delegate {
                        Animator _animator = Mob.GetComponent<Animator>();

                        _animator.SetTrigger("Landing");
                        StartCoroutine(WaitForLandingEnd(_animator, () => {
                            EventManager.Instance.EnabledSound(tag + "Run", false, true);

                            Mob.transform.DOMoveX(-7.8f, 2.1f).OnComplete(delegate {
                                EventManager.Instance.DisableSound(tag + "Run");
                            });
                        }));                        
                    });
                });
                //*********************************************not_fixed*********************************************

            }
            ));
        } else if (name == "Scarab" || name == "Bat" || name == "Fish") {
            Mob.GetComponent<Animator>().SetTrigger("Attack");
            Mob.transform.DORotate(Vector3.forward * 45, 1.1f).SetEase(Ease.OutQuad);
            Mob.transform.DOMoveY(PlayerController.Instance.transform.position.y, 1.1f).OnComplete(delegate {
                EventManager.Instance.EnabledSound(tag + "Fly", false, true);
                Mob.transform.DOLocalMoveY(startPosY, 1.1f);
                Mob.transform.DORotate(Vector3.forward, 1.1f).SetEase(Ease.OutQuad);
            });
        } else if (name == "Wasp" || name == "Dragon" || name == "Harpy") {
            Mob.GetComponent<Animator>().SetTrigger("Attack");
            Mob.transform.DOMoveY(PlayerController.Instance.transform.position.y, 1.1f).OnComplete(delegate {
                EventManager.Instance.EnabledSound(tag + "Fly", false, true);
                Mob.transform.DOLocalMoveY(startPosY, 1.1f);
            });
        }
    }

    private IEnumerator MoveMobToEdge() {
        Transform parent = Mob.transform.parent;
        while (parent.position.x > GetScreen.left) {
            parent.position += Vector3.left * Time.deltaTime * PlaygroundController.Instance.speed * PlaygroundController.Instance.acceleration * 0.95f;
            yield return null;
        }

        animator.Rebind(); Refresh();
        animator.enabled = true;
    }
    //*********************************************not_fixed*********************************************
    private IEnumerator WaitForLandingEnd(Animator _animator, System.Action moveToPlayer) {
        while (_animator.GetCurrentAnimatorStateInfo(0).IsName("landing")) {
            yield return null;
        }
        moveToPlayer();
    }
    //*********************************************not_fixed*********************************************

    private IEnumerator StartSuccubusRun(System.Action moveToPlayer) {
        yield return new WaitForSeconds(0.05f);
  
        Animator _animator = Mob.GetComponent<Animator>();
        while (_animator.GetCurrentAnimatorStateInfo(0).IsName("jump") && (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.65f)) {
            yield return null;
        }
        moveToPlayer();
    }

    private IEnumerator WaitFrightAnimationEnd() {
        yield return new WaitForSeconds(0.05f);

        Animator mobAnim = Mob.GetComponent<Animator>();
        if (tag != "Dragon") {
            while (mobAnim.GetCurrentAnimatorStateInfo(0).IsName("fright") && mobAnim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.25f) {
                yield return null;
            }
            if (PlaygroundController.Instance.acceleration.Equals(0)) {
                while (mobAnim.GetCurrentAnimatorStateInfo(0).IsName("fright") && mobAnim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f) {
                    yield return null;
                }
            }
        } else {
            while (mobAnim.GetCurrentAnimatorStateInfo(0).IsName("fright") && mobAnim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f) {
                yield return null;
            }
        }
        
        isFrightActive = false;
        if (tag == "Centaur") {
            animator.enabled = true;
            animator.speed = 0.66f;
            EventManager.Instance.EnabledSound(tag + "Walk", false, true);
        } else if (tag == "Anubis" || tag == "Mummy" || tag == "Mankey" || tag == "Golem") {
            StartCoroutine(MoveMobToEdge());
        } else if (tag == "Dragon") {
            animator.enabled = true;
            EventManager.Instance.EnabledSound(tag + "Fly", false, true);
            Mob.GetComponent<Animator>().SetTrigger("Fly");
        }
    }

    public void Die() {
		isHasBeenHit = true;
		isHasAttacked = true;
		isHasBitten = true;

        collider2D.enabled = false;

        if (isFlying) {
            EventManager.Instance.DisableSound(tag + "Fly");
        } else {
            if (tag != "Succubus") {
                EventManager.Instance.DisableSound(tag + "Walk");
            }
        }
        EventManager.Instance.DisableSound(tag + "Attack");
        EventManager.Instance.EnabledSound(tag + "Dawn");
        if (tag == "Centaur" || tag == "Mankey" || tag == "Dragon") {
            Mob.transform.DOKill();
            isFrightActive = true;
            animator.enabled = false;
            if (tag != "Dragon") {
                shadow.gameObject.SetActive(false);
            }
            Mob.GetComponent<Animator>().SetTrigger("Fright");
            StartCoroutine(WaitFrightAnimationEnd());
            Mob.transform.DOLocalMoveY(startPosY, 0.7f).SetEase(Ease.InQuad);
        }
        //***********************************not_fixed***********************************
        else if (tag == "Succubus") {
            isFrightActive = true;
            isScaredSuccubus = true;
            Mob.GetComponent<Animator>().SetTrigger("Fright");
            StartCoroutine(WaitFrightAnimationEnd());
        }
        //***********************************not_fixed***********************************
        else if (tag == "Anubis" || tag ==  "Mummy" || tag == "Golem") {
            Mob.transform.DOKill();
            isFrightActive = true;
            animator.enabled = false;
            shadow.gameObject.SetActive(false);
            Mob.GetComponent<Animator>().SetTrigger("Fright");
            Mob.transform.DOLocalMoveY(startPosY, 0.5f);
            StartCoroutine(WaitFrightAnimationEnd());
        }
        else if (tag == "Scarab" || tag == "Wasp" || tag == "Bat" || tag == "Harpy" || tag == "Fish") {
            Mob.transform.DOKill();
            Mob.GetComponent<Animator>().SetTrigger("Fright");
            animator.speed = 0.6f;
            Mob.transform.DOMoveY(-4.5f, 1.6f).SetEase(Ease.InQuad).OnComplete(delegate {
                animator.speed = 1f;
                Mob.GetComponent<Animator>().enabled = false;
            });   
        }
    }

    public void Refresh() {

        if (tag == "Scarab" || tag == "Wasp" || tag == "Bat" || tag == "Fish")
        {
            Mob.transform.localPosition = Vector2.zero;
            Mob.transform.eulerAngles = Vector3.zero;
            Mob.GetComponent<Animator>().enabled = true;
            Mob.GetComponent<Animator>().SetTrigger("Fly");
        }
        else if (tag == "Harpy")
        {
            Mob.transform.localPosition = Vector2.zero;
            Mob.GetComponent<Animator>().enabled = true;
            Mob.GetComponent<Animator>().SetTrigger("Fly");
        }
        else if (tag == "Anubis" || tag == "Mummy" || tag == "Golem")
        {
            Mob.GetComponent<Animator>().SetTrigger("Move");
        }
        else if (tag == "Mankey")
        {
            Mob.GetComponent<Animator>().SetTrigger("Move");
        }
        else if (tag == "Centaur")
        {
            animator.speed = 1f;
        }

        isHasAttacked = false;
        isHasBitten = false;
        isHasBeenHit = false;

        collider2D.enabled = true;
        
        if (tag != "Centaur" && tag != "Succubus") {
            EventManager.Instance.activeNextMob[tag]();
        }        
    }

    public void Init() {
        if (tag == "Succubus") {
            isScaredSuccubus = false;
            startSuccubusPosX = Mob.transform.position.x;            
        } else {
            animator = GetComponent<Animator>();
        }
        
        if (isFlying) {
            foreach (string mob in new[] { "Scarab", "Harpy", "Bat" }) {                
                if (mob == tag) {
                    EventManager.Instance.EnabledSound(tag + "Fly", false, true);
                    break;
                }
            }
        } else {
            foreach (string mob in new[] { "Anubis", "Mankey", "Centaur" }) {
                if (mob == tag) {
                    EventManager.Instance.EnabledSound(tag + "Walk", false, true);
                    break;
                }
            }
        }        

        startPosY = Mob.transform.localPosition.y;

        isHasAttacked = false;
		isHasBeenHit = false;
		isHasBitten = false;
        isFrightActive = false;        
    }

    private void UpdateBeast() {
        if (PlaygroundController.Instance.isEndinglevel) {
            return;
        }
        if (IsAllowedToAttack()) {            
            Attack();
		}
        ChangeAnimatorSpeed();
    }

	public void Process() {
        UpdateBeast();
        if (PlayerController.Instance.isInPortal) {
            if (tag != "Succubus") {
                animator.speed = 1 + PlaygroundController.Instance.acceleration * 0.5f;
            }
        }        
    }
}
