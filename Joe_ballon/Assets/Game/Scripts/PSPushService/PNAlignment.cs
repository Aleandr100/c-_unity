﻿using UnityEngine;
using System.Collections;

public class PNAlignment : MonoBehaviour {
	public static PNAlignment Instance;

	public enum vAlignment { none, top, center, bottom }
	public enum hAlignment { none, left, center, right }

	public GameObject alObj;

	public vAlignment vAlign = vAlignment.none;
	public hAlignment hAlign = hAlignment.none;

	public float xOffset = 0f;
	public float yOffset = 0f;

	void Awake () {
		Instance = this;
	}

	public void SetAlignment (GameObject self, vAlignment vAlignm, hAlignment hAlignm, float xOffs, float yOffs) {
		float camsizedef = PsvPushService.Instance.PushNewsScale;
		Vector3 pos = self.transform.position;
		if (hAlignm == hAlignment.left) {
			pos.x = PNGetScreen.left;
		} else if (hAlignm == hAlignment.right) {
			pos.x = PNGetScreen.right;
		} else if (hAlignm == hAlignment.center) {
			pos.x = (PNGetScreen.right + PNGetScreen.left) * 0.5f;
		}

		if (vAlignm == vAlignment.top) {
			pos.y = PNGetScreen.top;
		} else if (vAlignm == vAlignment.bottom) {
			pos.y = PNGetScreen.bottom;
		} else if (vAlignm == vAlignment.center) {
			pos.y = (PNGetScreen.top + PNGetScreen.bottom) * 0.5f;
		}
		self.transform.position = pos + new Vector3(xOffs*camsizedef, yOffs*camsizedef, 0f);
	}

	void OnEnable () {
		SetAlignment (alObj, vAlign, hAlign, xOffset, yOffset);
	}
}
