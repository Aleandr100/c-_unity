﻿using UnityEngine;
//using System.Collections;
using System.Collections.Generic;



/// <summary>
/// ChangeLog
/// v2
///     - fixed bug when variables were not saved on change
///     - fixed CanShow() - replaced || operator with &&
/// v3
///     - optimised canvas settings
///     - use_time_scale set to false by default
///     - PanelScript now uses Time.unscaledDeltaTime for fade animation
/// </summary>


namespace RateMePlugin
{
    [RequireComponent ( typeof ( AudioSource ) )]
    public class RateMeModule :MonoBehaviour
    {
        public static RateMeModule Instance;

        public delegate void Callback (bool visible);
        public static event Callback OnDialogueShown;

        private string
            closed_var = "RateMeModule:closed",
            negative_var = "RateMeModule:negative",
            positive_var = "RateMeModule:positive";

        private int
            negative_limit = 2,
            closed_limit = 5,
            closed_val,
            negative_val,
            positive_val;

        private const bool
            use_time_scale = false;

        private const float
            time_interval = 60; //60
        private float
            last_time = -time_interval; //to show dialogue after first call - set to zero to avoid early activation


        private GameObject
            panel;

        private string
            txt_en = "If you like to play with me, please rate me 5 Stars!",
            txt_fr = "Si vous aimez jouer avec moi, se il vous plaît noter m'a 5 étoiles!",
            txt_pt = "Se você gosta de jogar comigo, por favor classifique-me com 5 estrelas!",
            txt_ru = "Если тебе понравилось со мной играть, пожалуйста поставь мне 5 звездочек!",
            txt_sp = "Si te gusta jugar conmigo, por favor puntúame con 5 Estrellas!";

        public AudioClip
            snd_en,
            snd_fr,
            snd_pt,
            snd_ru,
            snd_sp;

        private AudioSource src;

        public struct LocalisedData
        {
            public AudioClip audio;
            public string txt;
            public LocalisedData (AudioClip a, string t)
            {
                audio = a;
                txt = t;
            }
        }

        private Dictionary<Languages.Language, LocalisedData> localised_data;




        void InitialiseLocalisedData ()
        {
            localised_data = new Dictionary<Languages.Language, LocalisedData>
            {
                {Languages.Language.English, new LocalisedData(snd_en,txt_en) },
                {Languages.Language.French, new LocalisedData(snd_fr,txt_fr) },
                {Languages.Language.Portuguese, new LocalisedData(snd_pt,txt_pt) },
                {Languages.Language.Russian, new LocalisedData(snd_ru,txt_ru) },
                {Languages.Language.Spanish, new LocalisedData(snd_sp,txt_sp) },
            };
        }

        public void Awake ()
        {
            if (Instance == null)
            {
                Instance = this;
                panel = transform.GetChild ( 0 ).gameObject;
                src = GetComponent<AudioSource> ( );
                DontDestroyOnLoad ( this.gameObject );
                GetVariables ( );
                InitialiseLocalisedData ( );
            }
            else
            {
                Debug.LogWarning ( "RateMeModule: you have multiple instances of module in your scene" );
            }
        }


        Languages.Language GetLang ()
        {
            Languages.Language lng = Languages./*Instance.*/GetLanguage ( );
            if (!localised_data.ContainsKey ( lng ))
            {
                lng = Languages.Language.English;
            }
            return lng;
        }



        public string GetLocalisedText ()
        {
            return localised_data [GetLang ( )].txt;
        }

        public AudioClip GetLocalisedAudio ()
        {
            return localised_data [GetLang ( )].audio;
        }


        void GetVariables ()
        {
            closed_val = GetVal ( closed_var );
            negative_val = GetVal ( negative_var );
            positive_val = GetVal ( positive_var );
        }

        int GetVal (string var)
        {
            return PlayerPrefs.GetInt ( var, 0 );
        }

        void SetVal (string var, int val)
        {
            PlayerPrefs.SetInt ( var, val );
            PlayerPrefs.Save ( );
        }


        public void OnDisable ()
        {
            RateButtonScript.OnRateClicked -= OnRateClicked;
            CloseButton.OnClose -= OnClosed;
        }

        public void OnEnable ()
        {
            RateButtonScript.OnRateClicked += OnRateClicked;
            CloseButton.OnClose += OnClosed;
        }

        void OnClosed ()
        {
            closed_val++;
            SetVal ( closed_var, closed_val );
        }


        void OnRateClicked (bool positive)
        {
            if (positive)
            {
                positive_val++;
                SetVal ( positive_var, positive_val );
            }
            else
            {
                negative_val++;
                SetVal ( negative_var, negative_val );
            }
            ShowDialogue ( false );
        }



        public void ShowDialogueImmediate ()
        {
            ShowDialogue ( true, true );
        }


        public bool ShowDialogue (bool show, bool immediate = false)
        {
            bool res = false;
            float time = Time.time;
            if (!show || immediate || CanShow ( time ))
            {
                if (OnDialogueShown != null)
                {
                    OnDialogueShown ( show );
                }
                panel.SetActive ( show );
                if (use_time_scale)
                {
                    Time.timeScale = show ? 0 : 1;
                }
                if (show)
                {
                    last_time = time;
                    AudioController./*Instance.*/PlayStream ( src, GetLocalisedAudio ( ) );
                    res = true;
                }
                else
                {
                    AudioController./*Instance.*/StopStream ( src );
                }

            }
            return res;
        }

        bool CanShow (float t)
        {
            return t - last_time >= time_interval && positive_val < 1 && negative_val < negative_limit && closed_val < closed_limit;
        }

    }
}