﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteSlicer2DDemoManager : MonoBehaviour 
{
	List<SpriteSlicer2DSliceInfo> m_SlicedSpriteInfo = new List<SpriteSlicer2DSliceInfo>();
	TrailRenderer m_TrailRenderer;

	struct MousePosition
	{
		public Vector3 m_WorldPosition;
		public float m_Time;
	}

	List<MousePosition> m_MousePositions = new List<MousePosition>();
	float m_MouseRecordTimer = 0.0f;
	float m_MouseRecordInterval = 0.05f;
	int m_MaxMousePositions = 5;
	
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		m_TrailRenderer = GetComponentInChildren<TrailRenderer>();
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () 
	{
		// Left mouse button - hold and swipe to cut objects
		if(Input.GetMouseButton(0) && EateryKitchenManager.isCuttingActive)
		{
			bool mousePositionAdded = false;
			m_MouseRecordTimer -= Time.deltaTime;

			// Record the world position of the mouse every x seconds
			if(m_MouseRecordTimer <= 0.0f)
			{
				MousePosition newMousePosition = new MousePosition();
				newMousePosition.m_WorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				newMousePosition.m_Time = Time.time;

				m_MousePositions.Add(newMousePosition);
				m_MouseRecordTimer = m_MouseRecordInterval;
				mousePositionAdded = true;

				// Remove the first recorded point if we've recorded too many
				if(m_MousePositions.Count > m_MaxMousePositions)
				{
					m_MousePositions.RemoveAt(0);
				}
			}

			// Forget any positions that are too old to care about
			if(m_MousePositions.Count > 0 && (Time.time - m_MousePositions[0].m_Time) > m_MouseRecordInterval * m_MaxMousePositions)
			{
				m_MousePositions.RemoveAt(0);
			}

			// Go through all our recorded positions and slice any sprites that intersect them
			if(mousePositionAdded)
			{
				for(int loop = 0; loop < m_MousePositions.Count; loop++) //- 1; loop++)
				{
					SpriteSlicer2D.SliceAllSprites(m_MousePositions[loop].m_WorldPosition, m_MousePositions[m_MousePositions.Count - 1].m_WorldPosition, true, ref m_SlicedSpriteInfo);

					if(m_SlicedSpriteInfo.Count > 0)
					{
						// Add some force in the direction of the swipe so that stuff topples over rather than just being
						// sliced but remaining stationary
						for(int spriteIndex = 0; spriteIndex < m_SlicedSpriteInfo.Count; spriteIndex++)
                        {
                            var slicedObject = m_SlicedSpriteInfo[spriteIndex].SlicedObject;
                            //slicedObject.GetComponent<Rigidbody2D>().AddForce(sliceDirection * 500.0f);
                            if (slicedObject)
                            {
                                if (slicedObject.GetComponent<Rigidbody2D>())
                                {
                                    slicedObject.GetComponent<Rigidbody2D>().gravityScale = 1;
                                    slicedObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                                    if (slicedObject.GetComponent<PolygonCollider2D>()) slicedObject.GetComponent<PolygonCollider2D>().enabled = false;
                                }
                                if (slicedObject.GetComponent<ChangeDirectionCollider>())
                                {
                                    slicedObject.GetComponent<ChangeDirectionCollider>().enabled = false;
                                }
                                //EateryKitchenManager.Instance.ShowCuttedFood(slicedObject.name);
                                Debug.Log(slicedObject.name);
                            }

                            for (int childSprite = 0; childSprite < m_SlicedSpriteInfo[spriteIndex].ChildObjects.Count; childSprite++)
							{
								Vector2 sliceDirection = m_MousePositions[m_MousePositions.Count - 1].m_WorldPosition - m_MousePositions[loop].m_WorldPosition;
								sliceDirection.Normalize();
                                var part = m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite];
                                part.GetComponent<Rigidbody2D>().AddForce(sliceDirection * 500.0f);
                                if (part.GetComponent<Rigidbody2D>())
                                {
                                    part.GetComponent<Rigidbody2D>().gravityScale = 1;
                                    part.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                                    if (part.GetComponent<PolygonCollider2D>()) part.GetComponent<PolygonCollider2D>().enabled = false;
                                }
                                if (childSprite == 0) EateryKitchenManager.Instance.ShowCuttedFood(part.name);
                                //if (part.GetComponent<ChangeDirectionCollider>()) part.GetComponent<ChangeDirectionCollider>().enabled = false;
                                //Debug.Log(part.name);
                            }
						}
						m_MousePositions.Clear();
						break;
					}
				}
			}

			if(m_TrailRenderer)
			{
				Vector3 trailPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				trailPosition.z = -9.0f;
				m_TrailRenderer.transform.position = trailPosition;
			}
		}
		else
		{
			m_MousePositions.Clear();
		}

		// Sliced sprites sharing the same layer as standard Unity sprites could increase the draw call count as
		// the engine will have to keep swapping between rendering SlicedSprites and Unity Sprites.To avoid this, 
		// move the newly sliced sprites either forward or back along the z-axis after they are created
		for(int spriteIndex = 0; spriteIndex < m_SlicedSpriteInfo.Count; spriteIndex++)
		{
			for(int childSprite = 0; childSprite < m_SlicedSpriteInfo[spriteIndex].ChildObjects.Count; childSprite++)
			{
				Vector3 spritePosition = m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite].transform.position;
				spritePosition.z = -1.0f;
				m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite].transform.position = spritePosition;
			}
		}

		m_SlicedSpriteInfo.Clear();
	}

	///// <summary>
	///// Draws the GUI
	///// </summary>
	//void OnGUI () 
	//{
	//	if(GUI.Button(new Rect(20,20,60,20), "Reset")) 
	//	{
	//		Application.LoadLevel(Application.loadedLevel);
	//	}

	//	GUI.Label(new Rect((Screen.width/2) - 400,20,900,20), "Left Mouse Button + Drag Cursor: Slice Objects");
	//	GUI.Label(new Rect((Screen.width/2) - 400,40,900,20), "(Cuts objects intersected by the cursor movement vector)");

	//	GUI.Label(new Rect((Screen.width/2) + 0,20,900,20), "Ctrl + Click Left Mouse Button: Explode Objects");
	//	GUI.Label(new Rect((Screen.width/2) + 0,40,900,20), "(Randomly slices an objects multiple times, then applies an optional force)");
	//}
}
