﻿using UnityEngine;
using System.Collections;
using AppCore;

namespace Game
{
    public class MainEntryPoint : MonoBehaviour
    {
        bool _isAwaken = false;
        private static void Init()
        {

        }

        private void Awake()
        {
            ServiceLocator.GetService<ITimeController>().Awake();
        }
        
        private void Start()
        {
            ServiceLocator.GetService<ITimeController>().Start();
        }

        private void Update()
        {
            ServiceLocator.GetService<ITimeController>().Update();
        }
    }
}
