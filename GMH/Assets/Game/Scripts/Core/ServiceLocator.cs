﻿using UnityEngine;
using System.Collections.Generic;

namespace AppCore
{
    public static class ServiceLocator
    {
        private static Dictionary<System.Type, object> _services;

#region initialization
        static ServiceLocator()
        {
            _services = new Dictionary<System.Type, object>();
            
            AddService<ITimeController>(new TimeController());
            AddService<IAgeController>(new AgeController());
            AddService<IDebugController>(new DebugController());
            AddService<IDataController>(new DataController());
            AddService<ILevelController>(new LevelController());
        }
#endregion

#region main
        public static T GetService<T>() where T : IService
        {
                foreach (var service in _services)
                    if (service.Key == typeof(T))
                        return (T)service.Value;

                throw new System.Exception("Service not found exception.");
        }

        public static void AddService<T>(object serviceInstance) where T : IService
        {
            if (typeof(T).GetType().IsInstanceOfType(serviceInstance.GetType()))
            {
                foreach (var service in _services)
                {
                    if (service.Key == typeof(T))
                    {
                        throw new System.Exception("Service already available.");
                    }
                }
                _services.Add(typeof(T), serviceInstance);
                (serviceInstance as IService).Init();
            }
            else
            {
                throw new System.Exception("Service is not inherited from the given interface");
            }
        }

        public static void RemoveService<T>() where T : IService
        {
            foreach (var service in _services)
            {
                if (service.Key == typeof(T))
                {
                    throw new System.Exception("Service already available.");
                }
            }
            _services.Remove(typeof(T));
        }
#endregion
    }
}
