﻿using UnityEngine;
using System.Collections;

namespace AppCore
{
    public interface IDataController : IService
    {
        bool isAppPurchased { get; }
    }
}
