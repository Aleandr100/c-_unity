﻿namespace AppCore
{
    public interface ILevelController : IService
    {
        void Awake();
        void Start();
    }
}
