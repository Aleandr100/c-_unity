﻿namespace AppCore
{
    public interface IDebugController : IService
    {
        void Error(string msg);
        void Warning(string msg);
        void Log(string msg);
        void Pause();
    }
}
