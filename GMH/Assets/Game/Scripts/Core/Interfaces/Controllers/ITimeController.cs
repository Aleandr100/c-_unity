﻿namespace AppCore
{
    public interface ITimeController : IService
    {
        void Awake();
        void Start();

        void RemoveTimers();
        void RemoveTimer(TimeController.OnTimerHandler handler);
        void AddTimer(TimeController.OnTimerHandler handler, object[] args, float startTime, float endTime, float delay = 0, bool isCycled = false, bool isOnlyOnUpdate = false);
        void AddTimerSingleAction(TimeController.OnTimerHandler handler, object[] args, float startTime, float endTime);
        void AddEndlessTimer(TimeController.OnTimerHandler handler, object[] args, float startTime);

        void AddUpdate(TimeController.MonoMethodHandler handler);
        void RemoveUpdate(TimeController.MonoMethodHandler handler);
        void AddAwake(TimeController.MonoMethodHandler handler);
        void RemoveAwake(TimeController.MonoMethodHandler handler);
        void AddStart(TimeController.MonoMethodHandler handler);
        void RemoveStart(TimeController.MonoMethodHandler handler);
    }
}