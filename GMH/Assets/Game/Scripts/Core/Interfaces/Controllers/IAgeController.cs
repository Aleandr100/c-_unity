﻿namespace AppCore
{
    public interface IAgeController : IService
    {
        Enumerators.Age age { get; set; }
    }
}
