﻿namespace AppCore
{
    public interface ILevel
    {
        void Awake();
        void Start();
        void Update();

        void OnTouchDown();
        void OnTouch();
        void OnTouchUp();
    }
}
