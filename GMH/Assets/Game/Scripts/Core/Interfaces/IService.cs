﻿namespace AppCore
{
    public interface IService
    {
        void Init();
        void Update();
    }
}
