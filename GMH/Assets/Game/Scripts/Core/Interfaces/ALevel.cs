﻿namespace AppCore
{
    using UnityEngine;

    public abstract class ALevel : ILevel
    {
        protected GameObject takenObject;
        Vector3 _startPosition;

        private float _objectFlySpeed = 2.5f,
                      _objectRotationSpeed = 8;

        private bool _isDragAvailable,
                     _wasOrderUsed;

        protected int _startSortOrder,
                      _draggableSortOrder;

        protected string _draggableNameContains,
                         _releasableNameContains;

        protected void SetActionableParts(string draggable, string releasable)
        {
            _draggableNameContains = draggable;
            _releasableNameContains = releasable;
        }
        
        protected void Reset()
        {
            OnTouch();
            OnTouchUp();
            _isDragAvailable = true;
        }

        protected void TakeObject(bool withSortOrder, System.Action OnSucceededAction = null)
        {
            Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

            if (colliderTouched && _isDragAvailable)
            {
                if (colliderTouched.name.Contains(_draggableNameContains) ) {
                    _isDragAvailable = false;
                    takenObject = colliderTouched.gameObject;
                    _startPosition = takenObject.transform.position;
                    if (withSortOrder)
                    {
                        _startSortOrder = takenObject.GetComponent<SpriteRenderer>().sortingOrder;
                        takenObject.GetComponent<SpriteRenderer>().sortingOrder = _draggableSortOrder;
                        _wasOrderUsed = true;
                    }

                    if (OnSucceededAction != null) OnSucceededAction();
                }
            }
        }
        
        protected void DragObject()
        {
            if (takenObject) takenObject.transform.position += (Vector3)GetMouseDeltaWorld();
        }

        protected void ReleaseObject(System.Action OnSucceededAction = null, System.Action OnFailedAction = null)
        {
            if (takenObject)
            {
                //_isDragAvailable = true;

                bool isSucceed = false;

                if (OnFailedAction == null) OnFailedAction = ThrowDraggableObject;

                takenObject.GetComponent<Collider2D>().enabled = false; //SetActive(false);

                Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                takenObject.GetComponent<Collider2D>().enabled = true;

                if (_wasOrderUsed)
                {
                    takenObject.GetComponent<SpriteRenderer>().sortingOrder = _startSortOrder;
                    _wasOrderUsed = false;
                }
                
                if (colliderTouched)
                {
                    if (colliderTouched.name.Contains(_releasableNameContains))
                    {
                        isSucceed = true;
                    }
                }

                if (isSucceed) {
                    if (OnSucceededAction != null) OnSucceededAction();
                }
                else
                {
                    if (OnFailedAction != null) OnFailedAction();
                }
                takenObject = null;
            }
        }

        protected void ThrowDraggableObject()
        {
            Extensions.MoveObjectToPointByPosition(_startPosition, takenObject, _objectFlySpeed, ResetCanDrag);
            takenObject = null;
        }

        void ResetCanDrag()
        {
            _isDragAvailable = true;
        }

        #region Mouse_Helpers
        private Vector2 _prevMousePos,
                        _currentMousePos;

        protected Vector2 GetMouseDeltaWorld()
        {
            return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
        }

        protected Vector2 GetMouseDeltaScreen()
        {
            return _currentMousePos - _prevMousePos;
        }
        #endregion

        #region abstract
        public abstract void ChangeSceneState(int state);
        public abstract void Awake();
        public abstract void Start();
        public abstract void Update();
        public abstract void OnTouchDown();
        public abstract void OnTouch();
        public abstract void OnTouchUp();
        #endregion

        protected void UpdateMosePos()
        {
            _prevMousePos = _currentMousePos;
            _currentMousePos = Input.mousePosition;
        }
    }
}
