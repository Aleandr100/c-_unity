﻿namespace AppCore.Enumerators
{
    public enum Age
    {
        TWO_PLUS,
        FIVE_PLUS,
    }
}

namespace Game.Enumerators
{
    public enum HippoLyalyaRoomState
    {
        STATE_MOM_COMES_IN_AND_GIVES_BOTTLE,
        STATE_CHANGE_PAMPERS,
        STATE_MOM_THROWS_DIRTY_PAMPERS,
        STATE_GIVE_BOTTLE,
        STATE_GIVE_DUMMY_AND_BEANBAG,
        STATE_TURNON_RADIO,

    }

    public enum HippoGrandsSaloonMakingJuiceState
    {
        STATE_DRAG_CARROTS,
        STATE_TURNON_BLENDER,
        STATE_CLEAN_TABLE,
        STATE_FILL_CUP,
    }

    public enum HippoKitchenLyalyaState
    {
        STATE_OPEN_CUPBOARD,
        STATE_TAP_ON_CUPBOARD,
        STATE_WALK_OUT,
    }

    public enum HippoBathroomMomState
    {
        STATE_MOM_WALKS_IN,
        STATE_MOM_ASKING_SHAMPOO,
        STATE_MOM_ASKING_SPONGE,
        STATE_MOM_TAKING_SHOWER,
        STATE_MOM_ASKS_TOWEL_AND_BATHROBE,
        STATE_MOM_WALKS_OUT,
    }

    public enum HippoBathroomDadState
    {
        STATE_INTRO,
        STATE_SWIPE_OFF_BLIND,
        STATE_OPEN_WATER,
        STATE_END,
    }

    public enum HippoKitchenMomState
    {
        STATE_MOM_WALKS_IN,
        STATE_OPENING_WINDOW,
        STATE_CUP_TAKING,
        STATE_MAKING_COFFEE,
        STATE_CUP_BRINGING,
        STATE_END,
    }

    public enum KitchenFood
    {
        FOOD_CAKE,
        FOOD_BAOZI,
        FOOD_SPAGHETTI,
        FOOD_SOUP,
    }

    public enum HippoKitchenDadState
    {
        STATE_DAD_WALKS_IN,
        STATE_CUP_TAKING,
        STATE_MAKING_COFFEE,
        STATE_CUP_BRINGING,
        STATE_OPENING_OVEN,
        STATE_PIE_BRINGING,
        STATE_END,
    }

    public enum HippoGrandsDressingRoom
    {
        STATE_DRESSING_CLOTHES,
        STATE_DRESSING_BELT,
    }

    public enum HippoGrandsLighthouseRoom
    {
        STATE_GRANDDAD_WALKS_IN,
        STATE_TURN_OFF,
        STATE_GIVE_BINOCLE,
    }

    public enum HippoGrandsKitchenMomState {
        STATE_OPEN_OVEN,
        STATE_DRAG_PIE,
        STATE_OPEN_FRIDGE,
        STATE_DRAG_CARROTS,
        STATE_WALK_OUT,
    }
    
    public enum OpenState
    {
        STATE_OPEN,
        STATE_CLOSED,
    }

    public enum CharacterAnimation
    {
        walk_footOUT,
        sad,
        idle,
        turnOffSwitcher,
        idle_sit,
        drinking,
    }

    public enum Shampoo
    {
        showerGelYellow,
        showerGelGreen,
        showerGelRed,
    }

    public enum HippoBadroomDadState
    {
        STATE_DAD_SHOT_SLIPPERS,
        STATE_NEED_TO_MOVE_CLOCK,
        STATE_NEED_TOUCHED_CLOCK,
        STATE_NEED_TOUCHED_CUCKOO,
        STATE_NEED_TOUCHED_WATCH,
        STATE_NEED_TOUCH_DAD_BODY,
        STATE_DRESSING_DAD_SLIPPER,
        STATE_NEED_TOUCH_TO_CLOCK,
        NONE,
    }

    public enum HippoBadroomPeppaState
    {
        STATE_CHARS_TILT_LEFT,
        STATE_CHARS_TILT_RIGHT,
        STATE_CHARS_TILT_UP,
        STATE_CHARS_TILT_DAWN,
        STATE_CHARS_ETUDE,
        STATE_MOVE_CURTAIN,
        STATE_OPEN_WINDOW,
        STATE_JIM_SHAKE_BED,
        STATE_PEPPA_MUSIC_ENABLED,
        STATE_PEPPA_TILTS_START,
        SATATE_MOVE_OPEN_WINDOW,
        NONE,
    }

    public enum HippoBadroomJimState
    {
        SATATE_MOVE_CURTAIN,
        SATATE_MOVE_OPEN_WINDOW,
        SATATE_JUMP_JIM_ON_BED,
        NONE,
    }
}
