﻿namespace Game.Data
{
    public class Data
    {
        string location_1_quest_mom = "location_1_quest_mom",
               location_1_quest_dad = "location_1_quest_dad",
               location_1_quest_jim = "location_1_quest_jim",
               location_1_quest_hippi = "location_1_quest_hippi",
               location_1_quest_lyalya = "location_1_quest_lyalya",

               location_2_quest_grandmom = "location_2_quest_grandmom",
               location_2_quest_granddad = "location_2_quest_granddad";
    }

    public enum GameState {
        STATE_LOCATION_1_MOM,
        STATE_LOCATION_1_DAD,
        STATE_LOCATION_1_JIM_AND_HIPPI,
        STATE_LOCATION_1_LYALYA,

        STATE_LOCATION_2_GRANDMOM,
        STATE_LOCATION_2_GRANDDAD,

        STATE_LOCATION_3_MARY,
        STATE_LOCATION_3_RACCOON_DAD,
        STATE_LOCATION_3_RACCOON_LUI,

    }
}
