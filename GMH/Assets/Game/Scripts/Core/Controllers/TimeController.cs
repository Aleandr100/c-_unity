﻿using UnityEngine;
using System.Collections.Generic;

namespace AppCore
{
    public class TimeController : ITimeController
    {
        public delegate void MonoMethodHandler();

        public delegate void OnTimerHandler(object[] args);

        private class Timer
        {
            private object[] _args;

            private bool _isCycled,
                         _isIgnoringEndTime;

            private float _startTime,
                          _endTime,
                          _nextTime,
                          _delay;

            public OnTimerHandler action;

            public Timer(OnTimerHandler handler, object[] args, bool isCycled, float startTime, float delay, float endTime, bool isIgnoringEndTime)
            {
                action = handler;
                _args = args;
                _isCycled = isCycled;
                _startTime = startTime;
                _endTime = endTime;
                _isIgnoringEndTime = isIgnoringEndTime;
                if (!isCycled)
                {
                    delay = _endTime;
                    _nextTime = _endTime;
                }
                else
                {
                    _delay = delay;
                    _nextTime = _startTime;
                }
            }

            public bool UpdateTimer()
            {
                while (Time.time > _nextTime)
                {
                    if (_isIgnoringEndTime)
                    {
                        action(_args);
                        return true;
                    }
                    if (_isCycled)
                    {
                        if (_nextTime > _endTime) return false;
                        action(_args);
                        _nextTime += _delay;
                    }
                    else
                    {
                        if (Time.time > _endTime)
                        {
                            action(_args);
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        
        private List<Timer> _timers;

        private event MonoMethodHandler AwakeEvent;

        private event MonoMethodHandler StartEvent;
        
        private event MonoMethodHandler UpdateEvent;

        #region core
        public void Init()
        {
            _timers = new List<Timer>();
        }

        public void Awake()
        {
            AwakeEvent();
        }

        public void Start()
        {
            StartEvent();
        }
        
        public void Update()
        {
            UpdateTime();
        }

        private void UpdateTime()
        {
            UpdateEvent();
            for (int i = 0; i < _timers.Count; i++)
            {
                var timer = _timers[i];
                if (!timer.UpdateTimer())
                {
                    _timers.Remove(timer);
                }
            }
        }
 #endregion

        #region timer_methods
        public void AddTimer(OnTimerHandler handler,            //Method to be used as timer
                             object[] args,                     //arguments
                             float startTime,                   //starting time
                             float endTime,                     //end time
                             float delay = 0,                   // delay between iterations
                             bool isCycled = false,             //is cycled
                             bool isEndless = false)            // if true - ignoring endTime
        {
            _timers.Add(new Timer(handler, args, isCycled, startTime, delay, endTime, isEndless));
        }

        public void AddTimerSingleAction(OnTimerHandler handler,
                                        object[] args,
                                        float startTime,
                                        float endTime
                                        )
        {
            _timers.Add(new Timer(handler, args, false, startTime, 0, endTime, false));
        }

        public void AddEndlessTimer(OnTimerHandler handler,
                                        object[] args,
                                        float startTime
                                        )
        {
            _timers.Add(new Timer(handler, args, false, startTime, 0, 0, true));
        }

        public void RemoveTimer(OnTimerHandler handler)
        {
            for (int i = 0; i < _timers.Count; i++)
            {
                var timer = _timers[i];
                if (timer.action == handler)
                {
                    _timers.Remove(timer);
                    return;
                }
            }
        }

        public void RemoveTimers()
        {
            _timers.Clear();
        }
        #endregion

        #region mono_manage_methods
        public void AddUpdate(MonoMethodHandler handler)
        {
            UpdateEvent += handler;
        }

        public void RemoveUpdate(MonoMethodHandler handler)
        {
            UpdateEvent -= handler;
        }

        public void AddAwake(MonoMethodHandler handler)
        {
            AwakeEvent += handler;
        }

        public void RemoveAwake(MonoMethodHandler handler)
        {
            AwakeEvent -= handler;
        }

        public void AddStart(MonoMethodHandler handler)
        {
            StartEvent += handler;
        }

        public void RemoveStart(MonoMethodHandler handler)
        {
            StartEvent -= handler;
        }
        #endregion
    }
}