﻿using UnityEngine;
using System.Collections;
using AppCore;
using System;
using Game.Data;
using System.Collections.Generic;

namespace Game
{

    public class MainMenuSceneController : MonoBehaviour
    {
        [SerializeField]
        private GameObject _windowMomStart,
                           _windowMomFinal,
                           _windowDadStart,
                           _windowDadFinal,
                           _windowJimStart,
                           _windowJimFinal,
                           _windowHippiStart,
                           _windowHippiFinal,
                           _windowLylyaStart,
                           _windowLylyaFinal,
            
                           _windowGrandmomFinal,
                           _windowGrandDadDressingFinal,
                           _windowGrandDadUpperFinal,
                           _windowGrandDadLighthouseRoomFinal;

        private static bool isLoaded = false;

        private void Start()
        {
            if (isLoaded)
            {
                PlayerPrefs.SetInt("gameState", (int)gameState);
                Camera.main.transform.position = new Vector3(-9.86f, 0, -10);
            }
            else
            {
                Camera.main.GetComponent<Animator>().enabled = true;
                gameState = (GameState)PlayerPrefs.GetInt("gameState", 0);
                isLoaded = true;
            }
            CheckData();
        }
        public static GameState gameState;

        void CheckData()
        {
            if (gameState > GameState.STATE_LOCATION_1_MOM)
            {
                _windowMomStart.SetActive(false);
                _windowMomFinal.SetActive(true);
            }
            if (gameState > GameState.STATE_LOCATION_1_DAD)
            {
                _windowDadStart.SetActive(false);
                _windowDadFinal.SetActive(true);
            }
            if (gameState > GameState.STATE_LOCATION_1_JIM_AND_HIPPI)
            {
                _windowJimStart.SetActive(false);
                _windowJimFinal.SetActive(true);
                _windowHippiStart.SetActive(false);
                _windowHippiFinal.SetActive(true);
            }
            if (gameState > GameState.STATE_LOCATION_1_LYALYA)
            {
                _windowLylyaStart.SetActive(false);
                _windowLylyaFinal.SetActive(true);
            }


            if (gameState > GameState.STATE_LOCATION_2_GRANDMOM)
            {
                _windowGrandmomFinal.SetActive(true);
            }
            if (gameState > GameState.STATE_LOCATION_2_GRANDDAD)
            {
                _windowGrandDadDressingFinal.SetActive(true);
                _windowGrandDadUpperFinal.SetActive(true);
                _windowGrandDadLighthouseRoomFinal.SetActive(true);
            }
        }

        void TapOnLocation(string locationName)
        {
            switch (locationName)
            {
                case "hippi":
                    switch (gameState)
                    {
                        case GameState.STATE_LOCATION_1_MOM:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBadroomMom);
                            break;
                        case GameState.STATE_LOCATION_1_DAD:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBadroomDad);
                            break;
                        case GameState.STATE_LOCATION_1_JIM_AND_HIPPI:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBadroomPeppa);
                            break;
                        case GameState.STATE_LOCATION_1_LYALYA:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoKitchenLyalya);
                            break;
                    }

                    break;
                case "lighthouse":
                    switch (gameState)
                    {
                        case GameState.STATE_LOCATION_2_GRANDMOM:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsKitchenMom);
                            break;
                        case GameState.STATE_LOCATION_2_GRANDDAD:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsSaloonPie);
                            break;
                    }
                    break;
                case "raccoon":
                    switch (gameState)
                    {
                        case GameState.STATE_LOCATION_3_MARY:
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.RaccoonMarySchooner);
                            break;
                    }
                    break;
            }
        }

        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                if (colliderTouched)
                {
                    switch (colliderTouched.name)
                    {
                        case "btn":
                            Camera.main.GetComponent<Animator>().Play("play");
                            break;
                        default:
                            TapOnLocation(colliderTouched.name);
                            break;
                    }

                }
            }
        }
    }
}