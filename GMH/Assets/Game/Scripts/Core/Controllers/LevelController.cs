﻿using PSV_Prototype;

namespace AppCore
{
    public class LevelController : ILevelController
    {

        public ILevel _currentLevelService;

        public Scenes GetCurrentLevel
        {
            get { return (Scenes)System.Enum.Parse(typeof(Scenes), UnityEngine.SceneManagement.SceneManager.GetActiveScene().name); }
        }

        public void Init()
        {
            ServiceLocator.GetService<ITimeController>().AddAwake(this.Awake);
            ServiceLocator.GetService<ITimeController>().AddStart(this.Start);
            ServiceLocator.GetService<ITimeController>().AddUpdate(this.Update);
        }

        public void Awake()
        {
            ServiceLocator.GetService<ITimeController>().RemoveTimers();

            _currentLevelService = null;

            switch (GetCurrentLevel)
            {
                case Scenes.InitScene:
                    break;
                case Scenes.MainMenu:
                    //_currentLevelService = new Game.MainMenuSceneController();
                    break;
                case Scenes.PSV_Splash:
                    break;
                case Scenes.Push:
                    break;
                case Scenes.Sample:
                    break;
                case Scenes.HippoBathroomMom:
                    _currentLevelService = new Game.HippoBathroomMomController();
                    break;
                case Scenes.HippoKitchenMom:
                    //_currentLevelService = new Game.HippoKitchenMomController();
                    break;
                case Scenes.HippoBathroomDad:
                    _currentLevelService = new Game.HippoBathroomDadController();
                    break;
                case Scenes.HippoBadroomMom:
                    _currentLevelService = new GMH.Controllers.HippoBadroomMomController();
                    break;
                case Scenes.HippoBadroomDad:
                    _currentLevelService = new GMH.Controllers.HippoBadroomDadController();
                    break;
                //case Scenes.HippoBadroomJim:
                //    _currentLevelService = new GMH.Controllers.HippoBadroomJimController();
                //    break;
            }
            if (_currentLevelService != null)
            {
                _currentLevelService.Awake();
            }
        }
        
        public void Start()
        {
            if (_currentLevelService != null) _currentLevelService.Start();
        }

        public void Update()
        {
            if (_currentLevelService != null)
            {
                _currentLevelService.Update();

                if (UnityEngine.Input.GetMouseButtonDown(0))
                {
                    _currentLevelService.OnTouchDown();
                }
                else if (UnityEngine.Input.GetMouseButton(0))
                {
                    _currentLevelService.OnTouch();
                }
                else if (UnityEngine.Input.GetMouseButtonUp(0))
                {
                    _currentLevelService.OnTouchUp();
                }
            }
        }
    }
}
