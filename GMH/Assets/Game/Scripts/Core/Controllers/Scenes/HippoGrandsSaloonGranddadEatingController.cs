﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HippoGrandsSaloonGranddadEatingController : MonoBehaviour {

    [SerializeField]
    private GameObject _grandDad,
                       _telekBlinder,
                       _cupWorld,
                       _cupAnimator,
                       _cupHelper,
                       _pieWorld;
    private bool _tvOn = false;

    private Vector3 _startPosition = new Vector3(7.48f, -2.08f, 0),
                    _chairPosition = new Vector3(2.14f, -2.08f, 0),
                    _ladderPosition = new Vector3(-1.62f, -1.63f, 0);

    private void Awake()
    {
        _grandDad.transform.position = _startPosition;

        _grandDad.transform.DOMove(_chairPosition, 2.5f).SetEase(Ease.Linear).OnComplete(Sit);
    }

    void Sit()
    {
        _grandDad.GetComponent<Animator>().SetTrigger("sit");
    }

    // Use this for initialization
    void Start () {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-179"), delegate
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-180"), delegate
            {

            });
        });
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0) && !_tvOn)
        {
            Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

            if (colliderTouched)
            {
                if (colliderTouched.name == "telek")
                {
                    _tvOn = true;
                    _telekBlinder.SetActive(false);
                    _grandDad.GetComponent<Animator>().SetTrigger("eat");
                    _cupAnimator.SetActive(true);
                    _cupHelper.SetActive(true);
                    _cupWorld.SetActive(false);
                    _pieWorld.SetActive(false);

                    DOVirtual.DelayedCall(8f, OnStopedEating);
                }
            }
        }
	}

    void OnStopedEating()
    {
        _cupAnimator.SetActive(false);
        _cupAnimator.transform.SetParent(null);
        _cupHelper.transform.SetParent(null);
        _cupHelper.SetActive(false);
        _cupWorld.SetActive(true);

        DOVirtual.DelayedCall(1, WalkToStairs);
    }

    void WalkToStairs()
    {
        var _grandDadAnimator = _grandDad.GetComponent<Animator>();
        _grandDadAnimator.SetTrigger("walk");
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-182"), delegate {
            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsDressingRoom);
        });
        _grandDad.transform.DOMove(_ladderPosition, 2).SetEase(Ease.Linear).OnComplete(delegate {
            _grandDadAnimator.SetTrigger("upstairs");

        });
    }
}
