﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;

public class HippoRoomLyalyaController : MonoBehaviour {

    [SerializeField]
    private GameObject _dummy,
                       _dummyOnFace,
                       _beanbag,
                       _beanbagHand,
                       _milkBottle,
                       _milkBottleHand,
                       _cleanPampersCursor,
                       _cleanPampersLegs,
                       _pampersBox,
                       _book;

    private Vector3 _outOfScreenPos = new Vector3(-8.4f, -2.5f, 0),
                    _lyalyaTalkPos = new Vector3(-1.83f, -2.5f, 0);

    private GameObject _draggableObject;

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private bool _bookMoved;

    protected Vector2 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    private HippoLyalyaRoomState _sceneState;

    private Transform _helperMom;

    [SerializeField]
    private Animator _momAnimator,
                     _radioAnimator,
                     _bedAnimator,
                     _lyalyaAnimator;

	// Use this for initialization
	void Start () {
        _sceneState = HippoLyalyaRoomState.STATE_CHANGE_PAMPERS;
        DOVirtual.DelayedCall(2, delegate {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-140"), delegate
            {
                AudioController.PlaySound("hp-141");
            });
        });
    }
	
	// Update is called once per frame
	void Update () {
	    switch (_sceneState)
        {
            case HippoLyalyaRoomState.STATE_MOM_COMES_IN_AND_GIVES_BOTTLE:
                break;
            case HippoLyalyaRoomState.STATE_CHANGE_PAMPERS:
                if (Input.GetMouseButtonDown(0))
                {
                    _currentMousePos = Input.mousePosition;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _pampersBox.name)
                        {
                            Vector3 positionTouched = touchToWorld;
                            positionTouched.z = 0;
                            _draggableObject = _cleanPampersCursor;
                            _cleanPampersCursor.transform.position = positionTouched;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    Vector3 positionDelta = GetMouseDeltaWorld();
                    _draggableObject.transform.position += positionDelta;
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    //_draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    //_draggableObject.GetComponent<Collider2D>().enabled = true;


                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _lyalyaAnimator.name)
                        {
                            AudioController.PlaySound("hp-142");
                            _draggableObject.SetActive(false);
                            //_draggableObject.GetComponent<Collider2D>().enabled = false;
                            _sceneState = HippoLyalyaRoomState.STATE_GIVE_BOTTLE;
                            _lyalyaAnimator.SetTrigger("changePampers");
                            _lyalyaAnimator.GetComponentInChildren<ParticleSystem>().Play();
                            _draggableObject = null;
                            return;
                        }
                    }

                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);

                    _draggableObject = null;
                }
                break;
            case HippoLyalyaRoomState.STATE_MOM_THROWS_DIRTY_PAMPERS:
                break;
            case HippoLyalyaRoomState.STATE_GIVE_BOTTLE:
                if (Input.GetMouseButtonDown(0))
                {
                    _currentMousePos = Input.mousePosition;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _milkBottle.name)
                        {
                            _draggableObject = _milkBottle;
                            _helperMom = _milkBottle.transform.parent;
                            _milkBottle.transform.SetParent(null);
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    Vector3 positionDelta = GetMouseDeltaWorld();
                    _draggableObject.transform.position += positionDelta;
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;


                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _lyalyaAnimator.name)
                        {
                            DOVirtual.DelayedCall(2, delegate { AudioController.PlaySound("hp-143"); });
                            _draggableObject.transform.DOMove(_milkBottleHand.transform.position, 0.5f);
                            _draggableObject.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                            _milkBottleHand.GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
                            _draggableObject.GetComponent<Collider2D>().enabled = false;
                            _sceneState = HippoLyalyaRoomState.STATE_GIVE_DUMMY_AND_BEANBAG;
                            _lyalyaAnimator.SetTrigger("gotBottle");
                            _draggableObject = null;
                            return;
                        }
                    }

                    _milkBottle.transform.SetParent(_helperMom);
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);

                    _draggableObject = null;
                }
                break;
            case HippoLyalyaRoomState.STATE_GIVE_DUMMY_AND_BEANBAG:
                if (Input.GetMouseButtonDown(0))
                {
                    _currentMousePos = Input.mousePosition;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _book.name && !_bookMoved)
                        {
                            _book.transform.DOMove(_book.transform.position + Vector3.left * 2f, 0.5f);
                            _bookMoved = true;
                        }
                        if (colliderTouched.name == _beanbag.name)
                        {
                            _draggableObject = colliderTouched.gameObject;
                        }
                        if (colliderTouched.name == _dummy.name)
                        {
                            _draggableObject = colliderTouched.gameObject;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    Vector3 positionDelta = GetMouseDeltaWorld();
                    _draggableObject.transform.position += positionDelta;
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;


                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _lyalyaAnimator.name)
                        {
                            _draggableObject.GetComponent<Collider2D>().enabled = false;
                            
                            if (_draggableObject.name == _dummy.name)
                            {
                                _dummyOnFace.SetActive(true);
                                _dummyOnFace.GetComponent<SpriteRenderer>().enabled = true;
                                _draggableObject.transform.DORotate(_dummyOnFace.transform.rotation.eulerAngles, 1.8f);
                                _draggableObject.transform.DOScale(_dummyOnFace.transform.lossyScale, 1.7f).OnComplete(delegate { FadeAllInChildren(_dummy.transform.parent, 0, 0.7f); }); ;
                                _draggableObject.transform.DOMove(_dummyOnFace.transform.position, 2).OnComplete(delegate { _dummyOnFace.GetComponent<SpriteRenderer>().DOFade(1, 0.5f); } );
                            }
                            else if (_draggableObject.name == _beanbag.name)
                            {
                                _beanbagHand.SetActive(true);
                                _beanbagHand.GetComponent<SpriteRenderer>().enabled = true;
                                //_draggableObject.transform.DORotate(_beanbagHand.transform.rotation.eulerAngles, 1.8f);
                                //_draggableObject.transform.DOScale(_beanbagHand.transform.lossyScale, 1.7f);
                                _draggableObject.transform.DOMove(_beanbagHand.transform.position, 2).OnComplete(delegate {
                                    _beanbagHand.GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
                                    FadeAllInChildren(_beanbag.transform.parent, 0, 0.7f);
                                });
                            }

                            _draggableObject = null;

                            if (_dummyOnFace.GetComponent<SpriteRenderer>().enabled && _beanbagHand.GetComponent<SpriteRenderer>().enabled)
                            {
                                DOVirtual.DelayedCall(2, delegate { AudioController.PlaySound("hp-144"); });
                                _lyalyaAnimator.SetTrigger("gotDummyAndBeanbag");
                                _sceneState = HippoLyalyaRoomState.STATE_TURNON_RADIO;
                            }
                            return;
                        }
                    }

                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);

                    _draggableObject = null;
                }
                break;
            case HippoLyalyaRoomState.STATE_TURNON_RADIO:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _radioAnimator.name)
                        {
                            _lyalyaAnimator.SetTrigger("radioOn");
                            _radioAnimator.SetTrigger("on");

                            Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_2_GRANDMOM;

                            DOVirtual.DelayedCall(AudioController.PlaySound("hp-145"), delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu); });
                        }
                    }
                }
                break;
        }
	}

    void FadeAllInChildren(Transform transformParent, float endValue, float duration)
    {
        foreach (Transform child in transformParent)
        {
            if (child.GetComponent<SpriteRenderer>()) child.GetComponent<SpriteRenderer>().DOFade(endValue, duration);

            if (child.childCount > 0)
            {
                FadeAllInChildren(child, endValue, duration);
            }
        }
    }

    void HideAndShow(GameObject hide, GameObject show)
    {
        hide.SetActive(false);
        show.SetActive(true);
    }
}
