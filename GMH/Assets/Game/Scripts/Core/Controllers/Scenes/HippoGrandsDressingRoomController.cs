﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;
using System.Collections.Generic;

public class HippoGrandsDressingRoomController : MonoBehaviour {

    Vector3 _startPosition = new Vector3(0.84f, -4.46f, 0),
            _stairsUpPosition = new Vector3(3.16f, -4.46f, 0);

    [SerializeField]
    private GameObject _pigamaOff,
                       _grandDad,
                       _hatOnDad,
                       _rukavROnDad,
                       _rukavLOnDad,
                       _suitOnDad,
                       _pantsOnDad,
                       _beltOnDad,
                       _shtaninaROnDad,
                       _shtaninaLOnDad,
                       _bootROnDad,
                       _bootLOnDad;

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private HippoGrandsDressingRoom _sceneState;

    private GameObject _draggableObject;
    
    private Vector3 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    private void Awake()
    {
        _grandDad.transform.position = _startPosition;
    }

    void WalkAway()
    {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-191"), delegate
       {
           PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsUpperLighthouseRoom);
       });
        var animator = _grandDad.GetComponent<Animator>();
        animator.SetBool("isWalking", true);
        _grandDad.transform.DOMove(_stairsUpPosition, 1).SetEase(Ease.Linear).OnComplete( delegate { animator.SetTrigger("upstairs"); } );
    }

    // Use this for initialization
    void Start ()
    {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-183"), delegate {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-184"), delegate {
                AudioController.PlaySound("hp-186");
            });
        });
	}
    
    void MoveToAndOnOrOff(GameObject movableObject, Vector3 position, GameObject[] toOn, GameObject[] toOff)
    {
        movableObject.transform.DOMove(position, 0.3f).OnComplete(delegate {
            Turn(toOn, true);
            Turn(toOff, false);
        });
    }

    void Turn(GameObject[] turnable, bool isTurningOn)
    {
        foreach (GameObject go in turnable)
        {
            go.SetActive(isTurningOn);
        }

        if (_sceneState == HippoGrandsDressingRoom.STATE_DRESSING_CLOTHES && (_hatOnDad.activeSelf && _suitOnDad.activeSelf && _pantsOnDad.activeSelf && _bootLOnDad.activeSelf))
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-188"), delegate
            {
                AudioController.PlaySound("hp-190");
            });
            _sceneState = HippoGrandsDressingRoom.STATE_DRESSING_BELT;
        }
        else
        {
            AudioController.PlaySound("hp-187");
        }
    }
	
	// Update is called once per frame
	void Update () {
        switch (_sceneState)
        {
            case HippoGrandsDressingRoom.STATE_DRESSING_CLOTHES:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("closes_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _grandDad.name)
                        {
                            List<GameObject> turnableOn = new List<GameObject>(),
                                             turnableOff = new List<GameObject>();

                            if (_draggableObject.name.Contains("shapka"))
                            {
                                turnableOn.Add(_hatOnDad);

                                turnableOff.Add(_draggableObject);

                                MoveToAndOnOrOff(_draggableObject, _hatOnDad.transform.position, turnableOn.ToArray(), turnableOff.ToArray());
                            }
                            else if (_draggableObject.name.Contains("pingak"))
                            {
                                turnableOn.Add(_suitOnDad);
                                turnableOn.Add(_rukavLOnDad);
                                turnableOn.Add(_rukavROnDad);

                                turnableOff.Add(_draggableObject);

                                MoveToAndOnOrOff(_draggableObject, _suitOnDad.transform.position, turnableOn.ToArray(), turnableOff.ToArray());
                            }
                            else if (_draggableObject.name.Contains("shtani"))
                            {
                                turnableOn.Add(_pantsOnDad);
                                turnableOn.Add(_shtaninaLOnDad);
                                turnableOn.Add(_shtaninaROnDad);

                                turnableOff.Add(_draggableObject);
                                
                                MoveToAndOnOrOff(_draggableObject, _pantsOnDad.transform.position, turnableOn.ToArray(), turnableOff.ToArray());
                            }
                            else if (_draggableObject.name.Contains("tapok"))
                            {
                                turnableOn.Add(_bootLOnDad);
                                turnableOn.Add(_bootROnDad);

                                turnableOff.Add(_draggableObject);

                                MoveToAndOnOrOff(_draggableObject, _bootLOnDad.transform.position, turnableOn.ToArray(), turnableOff.ToArray());
                            }
                            
                            _draggableObject = null;
                            
                            return;
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;

            case HippoGrandsDressingRoom.STATE_DRESSING_BELT:
                if (Input.GetMouseButtonDown(0) && !_beltOnDad.activeSelf)
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("belt_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _grandDad.name && _draggableObject.name.ToLower().Contains("black"))
                        {
                            List<GameObject> turnableOn = new List<GameObject>(),
                                             turnableOff = new List<GameObject>();

                            turnableOn.Add(_beltOnDad);
                            turnableOff.Add(_draggableObject);

                            MoveToAndOnOrOff(_draggableObject, _beltOnDad.transform.position, turnableOn.ToArray(), turnableOff.ToArray());

                            _draggableObject = null;
                            DOVirtual.DelayedCall(2, WalkAway);
                            return;
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;
        }
    }
}
