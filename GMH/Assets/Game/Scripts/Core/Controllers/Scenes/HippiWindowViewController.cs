﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HippiWindowViewController : MonoBehaviour
{

    [SerializeField]
    private GameObject _photoContainer;
    [SerializeField]
    private List<Sprite> _photoAvailable;

    [SerializeField]
    private GameObject _toPhotoList;

    float nextTimePhotoPlayed = 0;

    // Use this for initialization
    void Start()
    {
        AudioController.PlaySound("hp-124");
    }

    void PlayPhoto(string photoName)
    {
        for (int i = 0; i < _photoAvailable.Count; i++)
        {
            var photo = _photoAvailable[i];
            if (photo.name.ToLower().Contains(photoName.ToLower()))
            {
                if (Time.time >= nextTimePhotoPlayed)
                {
                    nextTimePhotoPlayed = AudioController.PlaySound("hp-12" + Random.Range(5, 9).ToString()) + Time.time;
                }

                _photoContainer.GetComponent<Animator>().Play("photo", 0, 0);
                _photoContainer.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = photo;

                foreach (Transform child in _toPhotoList.transform)
                {
                    if (child.transform.name.ToLower().Contains(photoName.ToLower()))
                    {
                        child.gameObject.SetActive(false);
                    }
                }

                _photoAvailable.Remove(photo);

                if (_photoAvailable.Count <= 0)
                {
                    DG.Tweening.DOVirtual.DelayedCall(3, delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBadroomPeppaWalkAway); });
                }
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

            if (colliderTouched)
            {
                PlayPhoto(colliderTouched.name);
            }
        }
    }
}
