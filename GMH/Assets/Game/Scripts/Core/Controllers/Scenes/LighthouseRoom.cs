﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LighthouseRoom : MonoBehaviour {

    private delegate void OnEndFunction(object[] args);

    public List<Transform> hippoGrandDadPoints;

    public GameObject neededObject;

    public GameObject hippoGrandDad;

    private float _dadSpeed = 0.1f;
    //private int _nextStateHippoGrandDad = 1;

    public static LighthouseRoom instance;

    private void PlayCharacterAnimation(GameObject character, GameEnums.CharacterAnimations characterAnimation)
    {
       character.GetComponent<Animator>().Play(characterAnimation.ToString(), 0, 0);
    }

    public void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        hippoGrandDad.transform.position = hippoGrandDadPoints[0].position;
        StartCoroutine("hippoDadWalkToPoint", 1);
        SwitcherLighthouseRoom.instance.interactible = false;
        SwitcherLighthouseRoom.instance.isActiveOnStart = false;
        SwitcherLighthouseRoom.instance.isPlayedOnce = true;
        DraggableObject.instance.interactible = false;
    }

    public void TurnOffSwitcher()
    {
        StartCoroutine("hippoDadWalkToPoint", 2);
    }

    public IEnumerator hippoDadWalkToPoint(int index)
    {
        Transform point = hippoGrandDadPoints[index];
        PlayCharacterAnimation(hippoGrandDad, GameEnums.CharacterAnimations.walk_footOUT);

        var scale = hippoGrandDad.transform.localScale;
        hippoGrandDad.transform.localScale = new Vector3(Mathf.Abs(scale.x) * ((point.position.x > hippoGrandDad.transform.position.x) ? 1 : -1), scale.y, scale.z);


        while (hippoGrandDad.transform.position != point.position)
        {
            hippoGrandDad.transform.position = Vector3.MoveTowards(hippoGrandDad.transform.position, point.position, _dadSpeed);

            yield return new WaitForSeconds(0.03f);
        }

        PlayCharacterAnimation(hippoGrandDad, GameEnums.CharacterAnimations.idle);

        switch (index)
        {
            case 1:
                SwitcherLighthouseRoom.instance.interactible = true;
                break;

            case 2:
                StartCoroutine("AfterTurnOff");
                SwitcherLighthouseRoom.instance.TurnState();
                hippoGrandDad.transform.localScale = new Vector3(Mathf.Abs(scale.x) * 1, scale.y, scale.z);
                PlayCharacterAnimation(hippoGrandDad, GameEnums.CharacterAnimations.turnOffSwitcher);
                break;
            case 3:
                yield return new WaitForSeconds(hippoGrandDad.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
                DraggableObject.instance.interactible = true;
                break;
        }

        
        //_nextStateHippoGrandDad++;

        //if (_nextStateHippoGrandDad > hippoGrandDadPoints.Count - 1) yield break;
    }


    private IEnumerator AfterTurnOff()
    {
        yield return new WaitForSeconds(hippoGrandDad.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
        StartCoroutine("hippoDadWalkToPoint", 3);
    }
}
