﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;

public class HippoKitchenLyalyaController : MonoBehaviour {

    [SerializeField]
    private Animator _mamaAnimator;

    private Vector3 _outOfSceneMom = new Vector3(-12.25f, -3.16f, 0);

    [SerializeField]
    private GameObject _milkInHand,
                       _outTappableObjectL,
                       _outTappableObjectR;

    private int _tapCount = 0;

    private HippoKitchenLyalyaState _sceneState;

    private void Awake()
    {
        _sceneState = HippoKitchenLyalyaState.STATE_WALK_OUT;
    }

    private void SetSceneState(HippoKitchenLyalyaState state)
    {
        _sceneState = state;
    }

    // Use this for initialization
    void Start () {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-135"), delegate
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-136"), delegate
            {
                DOVirtual.DelayedCall(AudioController.PlaySound("hp-137"), delegate
                {
                    SetSceneState(HippoKitchenLyalyaState.STATE_OPEN_CUPBOARD);
                });
            });
        });
	}

    // Update is called once per frame
    void Update() {
        switch (_sceneState)
        {
            case HippoKitchenLyalyaState.STATE_OPEN_CUPBOARD:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "skaff")
                        {
                            colliderTouched.GetComponent<Collider2D>().enabled = false;
                            colliderTouched.GetComponent<Animator>().Play("open", 0, 0);
                            _sceneState = HippoKitchenLyalyaState.STATE_TAP_ON_CUPBOARD;
                        }
                    }
                }
                break;
            case HippoKitchenLyalyaState.STATE_TAP_ON_CUPBOARD:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "bottle")
                        {
                            _outTappableObjectL.transform.DOMove(_outTappableObjectL.transform.position + Vector3.left * 0.1f, 0.5f);
                            _outTappableObjectR.transform.DOMove(_outTappableObjectR.transform.position + Vector3.right * 0.1f, 0.5f);
                            _tapCount++;

                            if (_tapCount == 4)
                            {
                                _sceneState = HippoKitchenLyalyaState.STATE_WALK_OUT;
                                GameObject link = colliderTouched.gameObject;
                                colliderTouched.transform.DOMove(_milkInHand.transform.position, 1).OnComplete(delegate { DOVirtual.DelayedCall(AudioController.PlaySound("hp-138"), delegate { WalkOutTheRoom(link); }); });
                                colliderTouched.transform.DORotate(_milkInHand.transform.rotation.eulerAngles, 1);
                            }
                        }
                    }
                }
                break;
        }
    }

    void WalkOutTheRoom(GameObject turnOffObject)
    {
        turnOffObject.SetActive(false);
        _milkInHand.SetActive(true);
        _mamaAnimator.SetBool("isWalking", true);
        _mamaAnimator.transform.DOMove(_outOfSceneMom, 4).SetEase(Ease.Linear);
        DOVirtual.DelayedCall(3, delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoLylyaRoom); });
    }
}
