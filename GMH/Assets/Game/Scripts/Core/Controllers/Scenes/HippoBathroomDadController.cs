﻿using System;
using AppCore;
using UnityEngine;

namespace Game
{

    public class HippoBathroomDadController : ALevel
    {

        private float _angleToOpen = 270,
                      _rotationSpeed = 35,
                      _timeToOpen = 5,
                      _alreadyRotated,
                      _distanceForRotation;

        private bool _isOpen = false;

        private Vector3 _startPosition,
                        _startSize;
        
        private Vector2 _worldSpriteSize,
                        _rotationMousePos;

        private Quaternion _startRotation;

        bool _catchedFaucet,
             _craneOpen,
             _catchedBlind,
             _swipedOff;

        float _touchMultiPly;

        float _currentMouseDelta;

        private Transform _blindPart;

        private Enumerators.HippoBathroomDadState _sceneState;

        private GameObject _loadableObjectsPrefab,
                           _blind,
                           _waterCold,
                           _waterHot,
                           _hippoDad;

        private float _dadSpeed = 4f;

        override public void Awake()
        {
            _loadableObjectsPrefab = Resources.Load("Game/Prefabs/HippoBathroomDad/bgBathroom") as GameObject;

            var instance = GameObject.Instantiate(_loadableObjectsPrefab);

            _blind = instance.transform.Find("Blind/Image_Blind/storka").gameObject;

            var smesitel = instance.transform.Find("waterVentil");
            _waterCold = smesitel.Find("smesitelCold").gameObject;
            _waterHot = smesitel.Find("smesitelHot").gameObject;
            _hippoDad = instance.transform.Find("Hippo_papaShower").gameObject;


            Reset();

            SetActionableParts("smesitel", "");
        }

        override public void Start()
        {
            DG.Tweening.DOVirtual.DelayedCall(AudioController.PlaySound("hp-68"), delegate { AudioController.PlaySound("hp-69"); });
        }

        override public void ChangeSceneState(int state)
        {
            Enumerators.HippoBathroomDadState switchedState = (Enumerators.HippoBathroomDadState)state;

            switch (switchedState)
            {
                case Enumerators.HippoBathroomDadState.STATE_INTRO:
                    break;
                case Enumerators.HippoBathroomDadState.STATE_SWIPE_OFF_BLIND:
                    break;
                case Enumerators.HippoBathroomDadState.STATE_OPEN_WATER:
                    break;
                case Enumerators.HippoBathroomDadState.STATE_END:
                    break;
            }
            Reset();

            _sceneState = (Enumerators.HippoBathroomDadState)state;
        }

        override public void Update()
        {
            
        }

        void CheckObjectName()
        {
            if (_craneOpen) return;
            _hippoDad.GetComponent<Animator>().SetBool("papaAwaken", true);
            switch (takenObject.name)
            {
                case "smesitelHot":
                    _waterHot.transform.parent.GetComponent<Animator>().SetInteger("wrongOpen", 1);
                    break;
                case "smesitelCold":
                    _craneOpen = true;
                    _waterCold.transform.parent.GetComponent<Animator>().SetInteger("wrongOpen", 2);
                    DG.Tweening.DOVirtual.DelayedCall(7, delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoKitchenDad); });
                    break;
            }
            ReleaseObject();
        }

        #region mouseEventHandlers
        override public void OnTouchDown()
        {
            TakeObject(false, CheckObjectName);
        }


        override public void OnTouch()
        {
        }

        override public void OnTouchUp()
        {
        }
        #endregion
    }
}
