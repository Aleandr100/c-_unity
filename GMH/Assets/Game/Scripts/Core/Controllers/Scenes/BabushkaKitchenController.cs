﻿using UnityEngine;
using System.Collections;
using Game.Enumerators;
using DG.Tweening;

public class BabushkaKitchenController : MonoBehaviour {

    [SerializeField]
    private GameObject _grandmom,
                       _carrot1,
                       _carrot2,
                       _pie,
                       _draggablePie,
                       _ovenDoor,
                       _fridgeDoor;

    private bool _isOvenOpen,
                 _isFridgeOpen,
                 _isDragAvailable;

    private int _carrotsTaken = 0,
                _startSortingLayer;

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private Vector3 _startPosition;

    private GameObject _draggableObject;

    private HippoGrandsKitchenMomState _sceneState;
    
    private IEnumerable CoroutineMoveObjectToPoint(Transform transfPos) {
        do
        {
            if (UnityEngine.Vector3.SqrMagnitude(_draggableObject.transform.position - transfPos.position) < 0.01f && UnityEngine.Mathf.Abs(_draggableObject.transform.rotation.z - transfPos.rotation.z) < 1f)
            {
                _draggableObject.transform.position = transfPos.position;
                _draggableObject.transform.rotation = transfPos.rotation;
            }
            else
            {
                _draggableObject.transform.position = UnityEngine.Vector3.Lerp(_draggableObject.transform.position, transfPos.position, 2 * UnityEngine.Time.deltaTime);
                _draggableObject.transform.rotation = UnityEngine.Quaternion.RotateTowards(_draggableObject.transform.rotation, transfPos.rotation, 2 * UnityEngine.Time.deltaTime);
            }
            yield return new WaitForSeconds(0.03f);
        }
        while (!(_draggableObject.transform.position.Equals(transfPos.position) && _draggableObject.transform.rotation.Equals(transfPos.rotation)));
        _isDragAvailable = true;
    }

    private void SetSceneState(HippoGrandsKitchenMomState state)
    {
        _sceneState = state;
    }

    private void Awake()
    {
        _carrot1.SetActive(false);
        _carrot2.SetActive(false);
        _pie.SetActive(false);

        _sceneState = (HippoGrandsKitchenMomState)(-1);

        DOVirtual.DelayedCall(AudioController.PlaySound("hp-146"), delegate
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-147"), delegate
            {
                DOVirtual.DelayedCall(AudioController.PlaySound("hp-148"), delegate
                {
                    DOVirtual.DelayedCall(AudioController.PlaySound("hp-149"), delegate
                    {
                        DOVirtual.DelayedCall(AudioController.PlaySound("hp-150"), delegate
                        {
                            _sceneState = HippoGrandsKitchenMomState.STATE_OPEN_OVEN;
                        });
                    });
                });
            });
        });
    }

    // Use this for initialization
    void Start () {
	
	}
	
    private Vector3 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    private void ResetDrag()
    {
        _isDragAvailable = true;
    }

    // Update is called once per frame
    void Update() {

        switch (_sceneState)
        {
            case HippoGrandsKitchenMomState.STATE_OPEN_OVEN:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _ovenDoor.name)
                        {
                            _ovenDoor.GetComponent<Collider2D>().enabled = false;
                            _ovenDoor.GetComponent<Animator>().Play("open");
                            AudioController.PlaySound("hp-151");
                            _sceneState = HippoGrandsKitchenMomState.STATE_DRAG_PIE;
                        }
                    }
                }
                break;
            case HippoGrandsKitchenMomState.STATE_DRAG_PIE:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _draggablePie.name)
                        {
                            _draggableObject = _draggablePie;
                            _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = 200;
                            _currentMousePos = Input.mousePosition;
                            _draggablePie.GetComponentInChildren<ParticleSystem>().Play();
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        _draggableObject = null;
                        if (colliderTouched.name == "karzina")
                        {
                            _grandmom.GetComponent<Animator>().SetTrigger("rightItem");
                            _draggablePie.GetComponent<SpriteRenderer>().sortingOrder = 19;
                            _draggablePie.transform.SetParent(_pie.transform.parent);
                            _draggablePie.transform.DOMove(_pie.transform.position, 1, false);
                            _draggablePie.transform.DOScale(_pie.transform.localScale, 1);
                            _draggablePie.GetComponent<Collider2D>().enabled = false;

                            DOVirtual.DelayedCall(AudioController.PlaySound("hp-152"), delegate
                            {
                                DOVirtual.DelayedCall(AudioController.PlaySound("hp-153"), delegate
                                {
                                    DOVirtual.DelayedCall(AudioController.PlaySound("hp-154"), delegate
                                    {
                                        //DOVirtual.DelayedCall(AudioController.PlaySound("hp-155"), delegate
                                        //{
                                            _sceneState = HippoGrandsKitchenMomState.STATE_OPEN_FRIDGE;
                                        //});
                                    });
                                });
                            });
                                    
                        }
                        else
                        {
                            _draggablePie.transform.DOMove(_draggablePie.transform.parent.position, 1, false);
                            _draggablePie.transform.DOScale(_draggablePie.transform.parent.localScale, 1);
                            _draggablePie.GetComponentInChildren<ParticleSystem>().Stop();
                        }
                    }
                    else
                    {
                        _draggableObject = null;
                        _draggablePie.transform.DOMove(_draggablePie.transform.parent.position, 1, false);
                        _draggablePie.transform.DOScale(_draggablePie.transform.parent.localScale, 1);
                        _draggablePie.GetComponentInChildren<ParticleSystem>().Stop();
                    }
                }
                break;
            case HippoGrandsKitchenMomState.STATE_OPEN_FRIDGE:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _fridgeDoor.name)
                        {
                            AudioController.PlaySound("hp-156");
                            _grandmom.GetComponent<Animator>().SetTrigger("rightItem");
                            _fridgeDoor.GetComponent<Animator>().Play("open", 0, 0);
                            _fridgeDoor.GetComponent<Animator>().SetBool("isOpen", true);
                            _fridgeDoor.GetComponent<Collider2D>().enabled = false;
                            _sceneState = HippoGrandsKitchenMomState.STATE_DRAG_CARROTS;
                        }
                    }
                }
                break;
            case HippoGrandsKitchenMomState.STATE_DRAG_CARROTS:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("vegetable_"))
                        {
                            if (!colliderTouched.name.Contains("morkovka"))
                            {
                                AudioController.PlaySound("hp-15" + Random.Range(6, 8).ToString());
                            }
                            _draggableObject = colliderTouched.gameObject;
                            _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = 200;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "karzina" && _draggableObject.name.Contains("morkovka"))
                        {
                            if (_carrotsTaken < 1)
                            {
                                _carrotsTaken++;
                                AudioController.PlaySound("hp-159");
                                _grandmom.GetComponent<Animator>().SetTrigger("rightItem");
                                _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = 19;
                                _draggableObject.transform.SetParent(_carrot1.transform.parent);
                                _draggableObject.transform.DOMove(_carrot1.transform.position, 1, false);
                                _draggableObject.transform.DOScale(_carrot1.transform.localScale, 1);
                                _draggableObject.GetComponent<Collider2D>().enabled = false;
                                _draggableObject = null;
                            }
                            else
                            {
                                _carrotsTaken++;

                                _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = 18;
                                _draggableObject.transform.SetParent(_carrot2.transform.parent);
                                _draggableObject.transform.DOMove(_carrot2.transform.position, 1, false);
                                _draggableObject.transform.DOScale(_carrot2.transform.localScale, 1);
                                _draggableObject.GetComponent<Collider2D>().enabled = false;
                                _draggableObject = null;

                                _fridgeDoor.GetComponent<Animator>().Play("close", 0, 0);
                                AudioController.PlaySound("hp-160");
                                StartCoroutine("WaitToChangeScene");

                                _fridgeDoor.GetComponent<Animator>().SetBool("isOpen", false);
                                _sceneState = HippoGrandsKitchenMomState.STATE_WALK_OUT;
                            }

                        }
                        else
                        {
                            _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = -46;
                            _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 1, false);
                            _draggableObject.transform.DOScale(_draggableObject.transform.parent.localScale, 1);
                            _draggableObject = null;
                        }
                    }
                    else
                    {
                        _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = -46;
                        _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 1, false);
                        _draggableObject.transform.DOScale(_draggableObject.transform.parent.localScale, 1);
                        _draggableObject = null;
                    }
                }
                break;
            case HippoGrandsKitchenMomState.STATE_WALK_OUT:
                break;
        }
    }

    private IEnumerator WaitToChangeScene()
    {
        yield return new WaitForSeconds(2);
        _grandmom.transform.DOMove(new Vector3(6.51f, -2.39f, 0), 6).SetEase(Ease.Linear);
        _grandmom.GetComponent<Animator>().SetBool("finished", true);
        DOVirtual.DelayedCall(3, delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsSaloonMakingJuice); });
    }
}
