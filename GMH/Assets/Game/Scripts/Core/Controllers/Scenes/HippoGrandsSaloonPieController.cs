﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HippoGrandsSaloonPieController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-173"), delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsBedroomFather); });	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
