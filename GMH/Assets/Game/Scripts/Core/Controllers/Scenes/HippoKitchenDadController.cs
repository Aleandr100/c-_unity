﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;

public class HippoKitchenDadController : MonoBehaviour {

    [SerializeField]
    private GameObject _dadHippo,
                       _momHippo,
                       _coffeeMachine;

    [SerializeField]
    private Transform _startTransform,
                      _chairTransform,
                      _endTransform,
                      _pieDadTableTransform,
                      _pieDadHandTransform,
                      _cupDadHandTransform,
                      _cupDadTableTransform,
                      _cupDadCoffeeMakerTransform;

    private bool _coffeeMaking,
                 _coffeeGiven;

    private GameObject _draggableObject;

    private Vector3 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private HippoKitchenDadState _sceneState;
    
    // Use this for initialization
    void Start () {
        _sceneState = HippoKitchenDadState.STATE_DAD_WALKS_IN;
        _dadHippo.transform.position = _startTransform.position;
        _dadHippo.transform.DOMove(_chairTransform.position, 4).SetEase(Ease.Linear).OnComplete(delegate {
            _dadHippo.GetComponent<Animator>().SetTrigger("sit");

            _momHippo.GetComponent<Animator>().SetBool("isTalking", true);
            DOVirtual.DelayedCall(AudioController.PlaySound("mh-2"), delegate
            {
                _momHippo.GetComponent<Animator>().SetBool("isTalking", false);
                _dadHippo.GetComponent<Animator>().SetBool("isTalking", true);
                DOVirtual.DelayedCall(AudioController.PlaySound("dh-6"), delegate
                {
                    _dadHippo.GetComponent<Animator>().SetBool("isTalking", false);

                    DOVirtual.DelayedCall(AudioController.PlaySound("hp-75"), delegate
                    {
                        AudioController.PlaySound("hp-76");
                        _sceneState = HippoKitchenDadState.STATE_CUP_TAKING;
                    });
                });
            });
        });
    }
	
	// Update is called once per frame
	void Update () {
	    switch (_sceneState)
        {
            case HippoKitchenDadState.STATE_DAD_WALKS_IN:
                break;
            case HippoKitchenDadState.STATE_CUP_TAKING:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("cup_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _coffeeMachine.name)
                        {
                            if (_draggableObject.name == "cup_dad")
                            {
                                _draggableObject.transform.SetParent(_cupDadCoffeeMakerTransform);
                                _sceneState = HippoKitchenDadState.STATE_MAKING_COFFEE;
                                _draggableObject.transform.DOMove(_cupDadCoffeeMakerTransform.position, 0.5f);
                                _draggableObject = null;
                                return;
                            }
                            else
                            {
                                AudioController.PlaySound(Random.Range(0, 3) == 0 ? "hp-31" : (Random.Range(0, 2) == 0 ? "hp-77" : "hp-78"));
                            }
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;
            case HippoKitchenDadState.STATE_MAKING_COFFEE:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _coffeeMachine.name && !_coffeeMaking)
                        {
                            _coffeeMachine.GetComponent<BoxCollider2D>().enabled = false;
                            _coffeeMaking = true;
                            foreach (ParticleSystem ps in _coffeeMachine.GetComponentsInChildren<ParticleSystem>())
                            {
                                ps.Play();
                            }

                            DOVirtual.DelayedCall(3, delegate
                            {
                                foreach (ParticleSystem ps in _coffeeMachine.GetComponentsInChildren<ParticleSystem>())
                                {
                                    ps.Stop();
                                }
                                AudioController.PlaySound("hp-79");
                                _sceneState = HippoKitchenDadState.STATE_CUP_BRINGING;
                            });
                        }
                    }
                }
                break;
            case HippoKitchenDadState.STATE_CUP_BRINGING:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("cup_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _dadHippo.name && !_coffeeGiven)
                        {
                            _coffeeGiven = true;

                            _draggableObject.transform.DOMove(_cupDadTableTransform.position, 0.5f);
                            _draggableObject.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                            _dadHippo.GetComponent<Animator>().SetTrigger("drink");
                            _cupDadTableTransform.gameObject.SetActive(true);
                            _cupDadTableTransform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                            _cupDadTableTransform.GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
                            DOVirtual.DelayedCall(0.95f, delegate
                            {
                                _cupDadTableTransform.gameObject.SetActive(false);
                            });

                            DOVirtual.DelayedCall(3.15f, delegate {
                                //_cupDadHandTransform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                                _cupDadTableTransform.gameObject.SetActive(true);
                                _dadHippo.GetComponent<Animator>().SetBool("isTalking", true);

                                //_cupDadTableTransform.GetComponent<SpriteRenderer>().DOFade(1, 0.5f).OnComplete(delegate {
                                //    _cupDadTableTransform.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                                //});

                                _cupDadHandTransform.GetComponent<SpriteRenderer>().DOFade(1, 0.5f).OnComplete(delegate {
                                    DOVirtual.DelayedCall(AudioController.PlaySound("dh-7"), delegate
                                    {
                                        DOVirtual.DelayedCall(AudioController.PlaySound("dh-8"), delegate
                                        {
                                            _dadHippo.GetComponent<Animator>().SetBool("isTalking", false);
                                            _momHippo.GetComponent<Animator>().SetBool("isTalking", true);
                                            DOVirtual.DelayedCall(AudioController.PlaySound("mh-3"), delegate
                                            {
                                                _momHippo.GetComponent<Animator>().SetBool("isTalking", false);
                                                AudioController.PlaySound("hp-80");
                                                _sceneState = HippoKitchenDadState.STATE_OPENING_OVEN;
                                            });
                                        });
                                    });
                                });
                                _draggableObject = null;
                                return;
                            });
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;
            case HippoKitchenDadState.STATE_OPENING_OVEN:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "duhowkaDoor")
                        {
                            _sceneState = HippoKitchenDadState.STATE_PIE_BRINGING;
                            AudioController.PlaySound("hp-82");
                            colliderTouched.GetComponent<Animator>().SetTrigger("open");
                            colliderTouched.GetComponent<BoxCollider2D>().enabled = false;
                        }
                    }
                }
                break;
            case HippoKitchenDadState.STATE_PIE_BRINGING:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("pirog"))
                        {
                            _currentMousePos = Input.mousePosition;
                            _draggableObject = colliderTouched.transform.GetChild(0).gameObject;
                            var sr = _draggableObject.GetComponent<SpriteRenderer>();
                            sr.sortingOrder = 75;
                            sr.color = new Color(1, 1, 1, 0);
                            sr.DOFade(1, 0.5f);
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    if (_draggableObject.GetComponent<Collider2D>()) _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (_draggableObject.GetComponent<Collider2D>()) _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _dadHippo.name)
                        {
                            if (_draggableObject.name == "PirogPartKursor")
                            {
                                _draggableObject.transform.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                                _draggableObject.transform.parent.GetChild(1).gameObject.SetActive(false);
                                _draggableObject.transform.parent.GetChild(2).gameObject.SetActive(true);
                                _pieDadTableTransform.gameObject.SetActive(true);
                                _pieDadTableTransform.transform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                                _pieDadTableTransform.transform.GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
                                _sceneState = HippoKitchenDadState.STATE_END;


                                _draggableObject.transform.DOMove(_pieDadTableTransform.position, 0.5f);
                                _draggableObject.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);

                                _dadHippo.GetComponent<Animator>().SetTrigger("eat");

                                DOVirtual.DelayedCall(0.95f, delegate
                                {
                                    _pieDadTableTransform.gameObject.SetActive(false);
                                });

                                DOVirtual.DelayedCall(5, delegate {
                                    _dadHippo.GetComponent<Animator>().SetBool("isTalking", true);
                                    DOVirtual.DelayedCall(AudioController.PlaySound("dh-9"), delegate
                                    {
                                        DOVirtual.DelayedCall(AudioController.PlaySound("dh-10"), delegate
                                        {
                                            _dadHippo.GetComponent<Animator>().SetBool("isTalking", false);
                                            DOVirtual.DelayedCall(0.5f, delegate
                                            {
                                                var scale = _dadHippo.transform.localScale;
                                                scale.x *= -1;
                                                _dadHippo.transform.localScale = scale;

                                                _dadHippo.GetComponent<Animator>().SetTrigger("walk");
                                                _dadHippo.transform.DOMove(_endTransform.position, 3.5f).SetEase(Ease.Linear);
                                                DOVirtual.DelayedCall(4, delegate
                                                {
                                                    Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_1_JIM_AND_HIPPI;
                                                    PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu);
                                                });
                                            });
                                        });
                                    });
                                });

                                

                                _draggableObject = null;
                                return;
                            }
                            else
                            {

                            }
                        }
                    }

                    var sr = _draggableObject.GetComponent<SpriteRenderer>();
                    sr.DOFade(0, 0.5f);
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;
            case HippoKitchenDadState.STATE_END:
                //Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_1_JIM_AND_HIPPI;
                //DG.Tweening.DOVirtual.DelayedCall(AudioController.PlaySound("mh-2"), delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoKitchenDad); });
                break;
        }
	}
}
