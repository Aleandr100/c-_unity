﻿using AppCore;
using UnityEngine;

namespace Game
{

    public class HippoBathroomMomController : ALevel
    {
        private Enumerators.HippoBathroomMomState _sceneState;

        private GameObject _momBefore,
                           _momBeforePrefab,

                           _momAfter,
                           _momAfterPrefab,

                           _nightie,
                           _nightiePrefab,

                           _hand,
                           _handPrefab,

                           _mouseClickable,
                           _mouseClickablePrefab,

                           _bubbleSystemPrefab,

                           _blind,

                           _cabinet,

                           _balloonPrefab,
                           _balloon,

                           _mochalka,

                           _draggableObject,

                           _flyingObject;

        private Transform _helperHand;

        private ParticleSystem _waterHotSystem,
                               _steamSystem;

        private Enumerators.Shampoo _momChosenShampoo;

        private float _momSpeed = 3f,
                      _objectFlySpeed = 6f,
                      _showerDelay = 15,
                      _takeOffNightieDelay = 2,
                      _closeBlindDelay = 1.5f,
                      _handTakeDelay = 1;

        private Vector3[] _momPositions = new Vector3[]
        {
            new Vector3(-13.23f, -4.0451f),
            new Vector3(-3.82f, -4.0451f),
            new Vector3(6.68f, -1.93f)
        };
        
        private bool _bathrobeTaken = false,
                     _towelTaken = false,
                     _cabinetOpen = false,
                     _canTakeTowelOrBathrobe = true,
                     _canDrag = true;

        private BubbleSystem _bubbleSystem;

        override public void ChangeSceneState(int state)
        {
            Reset();
            Enumerators.HippoBathroomMomState receivedState = (Enumerators.HippoBathroomMomState)state;
            switch (receivedState)
            {
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SHAMPOO:
                    SetActionableParts("showerGel", "Ruka");
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SPONGE:
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_TAKING_SHOWER:
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKS_TOWEL_AND_BATHROBE:
                    SetActionableParts("showerItem_", "Ruka");
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_WALKS_OUT:
                    break;
            }
            _sceneState = receivedState;
        }

        private void ShowHand(bool isShowing)
        {
            _hand.GetComponent<Animator>().Play(isShowing ? "ask" : "take");
        }

        private void ShowBallon(bool isShowing)
        {
            _balloon.GetComponent<Animator>().Play(isShowing ? "show" : "hide", 0, 0);

            var container = _balloon.transform.Find("cloud_base/staffContaner");

            for (int i = 0; i < container.childCount; i++)
            {
                container.GetChild(i).gameObject.SetActive(false);
            }


            switch (_sceneState)
            {
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SHAMPOO:

                    var shampo = container.transform.Find(_momChosenShampoo.ToString());
                    shampo.gameObject.SetActive(true);
                    break;

                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SPONGE:

                    var sponge = container.transform.Find("mochalka");
                    sponge.gameObject.SetActive(true);
                    break;

                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKS_TOWEL_AND_BATHROBE:
                    var towel = container.transform.Find("towelMama");
                    var bathrobe = container.transform.Find("mamaHalat");
                    towel.gameObject.SetActive(!_towelTaken);
                    bathrobe.gameObject.SetActive(!_bathrobeTaken);
                    break;
            }
        }

        #region scene_methods

        private void OnBlindClosed()
        {

            _nightie.SetActive(true);
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { AskForShampoo(); }, null, Time.time, Time.time + _takeOffNightieDelay);
        }

        private void TakeOffNightie(object[] args)
        {
            ServiceLocator.GetService<IDebugController>().Log("Mom took off the nightie");
            _blind.SetActive(true);
            _blind.GetComponent<Animator>().Play("close");
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { OnBlindClosed(); }, null, Time.time, Time.time + 1);
        }

        private void AskForShampoo()
        {
            GenerateRandomShampoo();

            string sndShampooName = "";

            switch (_momChosenShampoo)
            {
                case Enumerators.Shampoo.showerGelGreen:
                    sndShampooName = "hp-20";
                    break;
                case Enumerators.Shampoo.showerGelRed:
                    sndShampooName = "hp-21";
                    break;
                case Enumerators.Shampoo.showerGelYellow:
                    sndShampooName = "hp-19";
                    break;
            }

            DG.Tweening.DOVirtual.DelayedCall(AudioController.PlaySound("hp-18"), delegate { AudioController.PlaySound(sndShampooName); });

            _hand.SetActive(true);
            ShowHand(true);

            //ServiceLocator.GetService<IDebugController>().Log("Mom asked for" + _momChosenShampoo + "shampoo");

            ChangeSceneState((int)Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SHAMPOO);

            _balloon.SetActive(true);
            ShowBallon(true);
        }

        private void GenerateRandomShampoo()
        {
            _momChosenShampoo = (Enumerators.Shampoo)Random.Range(0, Extensions.GetEnumSize<Enumerators.Shampoo>());
        }

        private void CheckShampoo()
        {
            if (takenObject.name == _momChosenShampoo.ToString())
            {
                GameObject linkToTakenObj = takenObject;
                Extensions.MoveObjectToPointByPosition(_helperHand.transform.position, takenObject, _objectFlySpeed, delegate { TakeShampoo(linkToTakenObj); });
            }
            else
            {
                ThrowDraggableObject();
            }
        }

        private void CheckSponge()
        {
            if (takenObject.name == "mochalka")
            {
                GameObject linkToTakenObj = takenObject;
                Extensions.MoveObjectToPointByPosition(_helperHand.transform.position, takenObject, _objectFlySpeed, delegate { TakeSponge(linkToTakenObj); });
            }
            else
            {
                ThrowDraggableObject();
            }
        }

        void TakeTowelOrBathrobe()
        {
            if (takenObject.name.Contains("showerItem_"))
            {
                takenObject.transform.SetParent(_helperHand);
                if (takenObject.name.Equals("showerItem_towelMama")) _towelTaken = true;
                else if (takenObject.name.Equals("showerItem_mamaHalat")) _bathrobeTaken = true;
                Extensions.MoveObjectToPointByPosition(_helperHand.transform.position, takenObject, _objectFlySpeed);
                ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { OnTimeHideHand(); }, null, Time.time, Time.time + 0.5f);
            }
            else
            {
                ThrowDraggableObject();
            }
        }

        void OnTimeHideHand()
        {
            ServiceLocator.GetService<ITimeController>().RemoveTimer( delegate { OnTimeHideHand(); });
            ShowHand(false);
            ShowBallon(false);
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { AskAgainOrNot(); }, null, Time.time, Time.time + 1);
        }

        void AskAgainOrNot()
        {
            HideItemsInHand();
            Reset();
            if (!_bathrobeTaken || !_towelTaken)
            {
                if (!_bathrobeTaken)
                {
                    AudioController.PlaySound("hp-26");
                }
                else if (!_towelTaken)
                {
                    AudioController.PlaySound("hp-25");
                }
                ShowHand(true);
                ShowBallon(true);
            }
            else
            {
                ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { OnAllTaken(); }, null, Time.time, Time.time + 2);
            }
        }

        private void OnAllTaken()
        {
            _blind.GetComponent<Animator>().Play("open");
            _hand.SetActive(false);
            _momAfter.SetActive(true);
            _momBefore.SetActive(false);
            _momAfter.GetComponent<Animator>().Play("walk");

            Vector3[] positionsBackwards = { _momPositions[2], _momPositions[1], _momPositions[0] };

            Extensions.MoveCharacterByPoolOfDotsForward(positionsBackwards, _momAfter, 2, _momSpeed, delegate { ChangeScene(); });
        }

        private void ChangeScene()
        {
            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoKitchenMom);
        }

        private void FinishShower()
        {
            _bubbleSystem.Stop();
            _waterHotSystem.Stop();
            _steamSystem.Stop();
            ChangeSceneState((int)Enumerators.HippoBathroomMomState.STATE_MOM_ASKS_TOWEL_AND_BATHROBE);
            AudioController.PlaySound("hp-24");
            ShowHand(true);
            ShowBallon(true);
        }

        private void TakeShampoo(GameObject shampoo)
        {
            shampoo.transform.SetParent(_helperHand.transform);
            ShowHand(false);
            ShowBallon(false);

            ChangeSceneState((int)Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SPONGE);
            SetActionableParts("skaff", "");

            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { AskForSponge(); }, null, Time.time, Time.time + 2);
        }

        private void TakeSponge(GameObject sponge)
        {
            sponge.transform.SetParent(_helperHand.transform);
            ShowHand(false);
            ShowBallon(false);
            _cabinet.GetComponent<Animator>().Play("close", 0, 0);

            ChangeSceneState((int)Enumerators.HippoBathroomMomState.STATE_MOM_TAKING_SHOWER);

            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { HideItemsInHand(); }, null, Time.time, Time.time + 1);
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { TakeShower(); }, null, Time.time, Time.time + 2);
        }

        private void AskForSponge()
        {
            DG.Tweening.DOVirtual.DelayedCall(AudioController.PlaySound("hp-22"), delegate { AudioController.PlaySound("hp-23"); });
            HideItemsInHand();
            ShowHand(true);
            ShowBallon(true);
        }

        private void TakeShower()
        {
            _bubbleSystem.Play();
            _steamSystem.Play();
            _waterHotSystem.Play();

            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { FinishShower(); }, null, Time.time, Time.time + 15);
        }
        
        private void StartBubbles(object[] args)
        {
            _bubbleSystem.Play();
        }

        private void EndBubbles(object[] args)
        {
            _bubbleSystem.Stop();
        }

        private void HideItemsInHand()
        {
            for (int i = 0; i < _helperHand.transform.childCount; i++)
            {
                _helperHand.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        #endregion

        override public void Awake() {
            _sceneState = Enumerators.HippoBathroomMomState.STATE_MOM_WALKS_IN;

            var loaded = Resources.Load("Game/Prefabs/HippoBathroomMom/bgBathroom") as GameObject;

            var instance = GameObject.Instantiate(loaded).transform;

            _bubbleSystemPrefab = Resources.Load("Game/Prefabs/HippoBathroomMom/BubbleSystem") as GameObject;

            _bubbleSystem = GameObject.Instantiate(_bubbleSystemPrefab).GetComponent<BubbleSystem>();

            _momBefore = instance.Find("Hippo_mamaShower").gameObject;
            _nightie = instance.Find("nightie").gameObject;
            _hand = instance.Find("Ruka").gameObject;
            _momAfter = instance.Find("mom_after").gameObject;
            _balloon = instance.Find("cloudWish").gameObject;

            _mochalka = instance.Find("mochalka").gameObject;
            _waterHotSystem = instance.Find("waterHot").GetComponent<ParticleSystem>();
            _steamSystem = instance.Find("steam").GetComponent<ParticleSystem>();
            _blind = instance.Find("storka").gameObject;
            _cabinet = instance.Find("skaff").gameObject;
            _helperHand = _hand.transform.Find("a_0013/Helper_ObjectHandler");

            _balloon.SetActive(false);
            _nightie.SetActive(false);
            _hand.SetActive(false);
            _mochalka.SetActive(false);
        }

        override public void Start()
        {
            _momBefore.transform.position = _momPositions[0];


            ServiceLocator.GetService<IDebugController>().Log("mom walk");

            Extensions.MoveCharacterByPoolOfDotsForward(_momPositions, _momBefore, 0, _momSpeed, delegate { TakeOffNightie(null); });

            _momBefore.GetComponent<Animator>().Play("walk");

            AudioController.PlaySound("hp-17");
        }

        override public void Update()
        {
            UpdateMosePos();
        }

        #region mouseEventHandlers
        override public void OnTouchDown()
        {
            switch (_sceneState)
            {
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SHAMPOO:
                    TakeObject(true);
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SPONGE:
                    if (!_cabinetOpen) TakeObject(false, OpenCupboard);
                    else
                    {
                        TakeObject(false);
                    }
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_TAKING_SHOWER:
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKS_TOWEL_AND_BATHROBE:
                    TakeObject(true);
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_WALKS_OUT:
                    break;
            }
        }

        

        void OpenCupboard()
        {
            if (takenObject.name == _cabinet.name)
            {
                _cabinetOpen = true;
                _cabinet.GetComponent<Animator>().Play("open", 0, 0);
                ReleaseObject();
                _mochalka.SetActive(true);
                _cabinet.GetComponent<BoxCollider2D>().enabled = false;
                SetActionableParts("mochalka", "Ruka");
            } 
        }

        override public void OnTouch()
        {
            DragObject();
        }

        override public void OnTouchUp()
        {
            switch (_sceneState)
            {
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SHAMPOO:
                    ReleaseObject(CheckShampoo);
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKING_SPONGE:
                    ReleaseObject(CheckSponge);
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_TAKING_SHOWER:
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_ASKS_TOWEL_AND_BATHROBE:
                    ReleaseObject(TakeTowelOrBathrobe);
                    break;
                case Enumerators.HippoBathroomMomState.STATE_MOM_WALKS_OUT:
                    break;
            }
        }

        #endregion
    }
}
