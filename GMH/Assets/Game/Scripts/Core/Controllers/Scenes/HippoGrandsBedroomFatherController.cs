﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HippoGrandsBedroomFatherController : MonoBehaviour {

    [SerializeField]
    private ParticleSystem _smellLong,
                           _smellShort;

    [SerializeField]
    private Animator _grandDadAnimator,
                     _windowAnimator;

    [SerializeField]
    private GameObject _blanket,
                       _blanketDad;

    private Vector3 _endPos = new Vector3(-11.49f, -5.03f, 0);

    private bool _windowOpen = true;

    void CloseWindow()
    {
        _windowOpen = true;
        _windowAnimator.SetTrigger("close");
        _smellLong.Play();
        _smellShort.Stop();

        DOVirtual.DelayedCall(9, GetUp);
    }

    void GetUp()
    {
        _grandDadAnimator.SetTrigger("wakeup");
        float time = AudioController.PlaySound("hp-177");
        DOVirtual.DelayedCall(4 >= time ? 4 : time, Walk);
    }

    void Walk()
    {
        //_blanketDad.transform.SetParent(null, true);
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-178"), delegate
        {
            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsSaloonGranddadEating);
        });
        _blanketDad.SetActive(false);
        _blanket.SetActive(true);
        _grandDadAnimator.SetTrigger("walk");
        _grandDadAnimator.transform.DOMove(_endPos, 4).SetEase(Ease.Linear);
    }

	// Use this for initialization
	void Start () {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-174"), delegate
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-175"), delegate
            {
                _windowOpen = false;
            });
        });
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && !_windowOpen)
        {

            Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

            if (colliderTouched)
            {
                if (colliderTouched.name.Contains("windowDed"))
                {
                    CloseWindow();
                }
            }
        }
	}
}
