﻿using UnityEngine;
using System.Collections;
using Game.Enumerators;
using UnityEngine.UI;
using DG.Tweening;

public class HippoBadroomJimController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _curtain;

    [SerializeField]
    private GameObject _openedWindow,
                       _jimChar,
                       _sunRayCast,
                       _staticBlanket,
                       _activeBlanket,
                       _jimProgress,
                       _workProgress;

    private bool swipeOff       = true,
                 _moveCurtain   = false,
                 _startProgress = false;

    private int _curtainIndex,
                _countCurtainOpened = 0,
                _jumpCount = 0;

    private Vector3 _startPos,
                    _endPos,
                    _currentSwipe,
                    _tempScaleJimProgress;

    private float _time,
                  _addToProgress = 0.25f;

    private Collider2D _colliderTouched;

    private HippoBadroomJimState _curr_state;

    private void Start()
    {
        _curr_state = HippoBadroomJimState.NONE;

        _jimProgress.SetActive(false);
        _tempScaleJimProgress = _jimProgress.transform.localScale;
        _jimProgress.transform.localScale = Vector2.zero;

        if (AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
        {
            swipeOff = false;
        }

        DOVirtual.DelayedCall(0.5f, delegate { _time = AudioController.PlaySound("hp-83"); }).OnComplete(delegate
        {
            DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-84"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { _curr_state = HippoBadroomJimState.SATATE_MOVE_CURTAIN; });
            });
        });
    }

    private void Update()
    {
        if (_moveCurtain)
        {
            MoveCurtain();
        }

        if (_startProgress)
        {
            _workProgress.GetComponent<Image>().fillAmount -= 0.0004f;
        }

        if (UnityEngine.Input.GetMouseButtonDown(0))
        {
            OnTouchDown();
        }
        else if (UnityEngine.Input.GetMouseButton(0))
        {
            OnTouch();
        }
        else if (UnityEngine.Input.GetMouseButtonUp(0))
        {
            OnTouchUp();
        }
    }

    private void OnTouchDown()
    {
        Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _colliderTouched = Physics2D.OverlapPoint(touchToWorld);

        if (_colliderTouched != null)
        {
            switch (_curr_state)
            {
                case HippoBadroomJimState.SATATE_MOVE_CURTAIN:                    
                    if (swipeOff)
                    {
                        if (_colliderTouched.name == "curtain_r")
                        {
                            _moveCurtain = true;
                            _curtainIndex = 0;
                        }
                        else if (_colliderTouched.name == "curtain_l")
                        {
                            _moveCurtain = true;
                            _curtainIndex = 1;
                        }
                    }
                    else
                    {
                        _startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    }
                    break;
                case HippoBadroomJimState.SATATE_MOVE_OPEN_WINDOW:
                    if (_colliderTouched.name == "window")
                    {
                        _openedWindow.GetComponent<Animator>().enabled = true;

                        _sunRayCast.GetComponent<Animator>().SetTrigger("Start_ray_cast");

                        _openedWindow.GetComponent<Collider2D>().enabled = false;

                        DOVirtual.DelayedCall(0.5f, delegate { _jimChar.GetComponent<Animator>().SetTrigger("Hippo_sun_reaction"); }).OnComplete(delegate 
                        {                            
                            DOVirtual.DelayedCall(0.5f, delegate { _time = AudioController.PlaySound("hp-86"); }).OnComplete(delegate
                            {
                                DOVirtual.DelayedCall(_time, delegate { JimWakeUp(); });
                            });
                        });                                               
                    }
                    break;
                case HippoBadroomJimState.SATATE_JUMP_JIM_ON_BED:
                    if (_colliderTouched.name == "odeyalkoAnim2")
                    {
                        _activeBlanket.GetComponent<Collider2D>().enabled = false;
                        _jimChar.GetComponent<Animator>().SetTrigger("Jim_jump");

                        _colliderTouched.enabled = false;
                        StartCoroutine(WaitForJumpAnimEnd());
                    }
                    break;
            }
        }
    }

    private void MoveCurtain()
    {
        _curtain[_curtainIndex].transform.localScale = new Vector3(Mathf.Lerp(_curtain[_curtainIndex].transform.localScale.x, 0.2f, 0.1f), 1, 1);

        if (_curtain[_curtainIndex].transform.localScale.x < 0.25f)
        {
            _countCurtainOpened++;
            _moveCurtain = false;

            _curtain[_curtainIndex].GetComponent<Collider2D>().enabled = false;

            if (_countCurtainOpened == 2)
            {
                _time =  AudioController.PlaySound("hp-85");
                DOVirtual.DelayedCall(_time, delegate 
                {
                    _openedWindow.GetComponent<Collider2D>().enabled = true;
                    _curr_state = HippoBadroomJimState.SATATE_MOVE_OPEN_WINDOW;
                });
            }
        }
    }

    private void OnTouch()
    {
        /**/
    }

    private void OnTouchUp()
    {
        if (!swipeOff)
        {
            _endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _currentSwipe = new Vector2(_endPos.x - _startPos.x, _endPos.y - _startPos.y);
            _currentSwipe.Normalize();

            if (_colliderTouched != null)
            {
                switch (_curr_state)
                {
                    case HippoBadroomJimState.SATATE_MOVE_CURTAIN:
                        if (_currentSwipe.x > 0f && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                        {
                            if (_colliderTouched.name == "curtain_r")
                            {
                                _moveCurtain = true;
                                _curtainIndex = 0;
                            }
                        }
                        else if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                        {
                            if (_colliderTouched.name == "curtain_l")
                            {
                                _moveCurtain = true;
                                _curtainIndex = 1;
                            }
                        }
                        break;
                }   
            }
        }
    }

    private void JimWakeUp()
    {        
        _jimChar.GetComponent<Animator>().SetTrigger("Jim_wake_up");
        DOVirtual.DelayedCall(2.2f, delegate { _staticBlanket.SetActive(false); });

        DOVirtual.DelayedCall(2.5f, delegate { _time = AudioController.PlaySound("hp-87"); }).OnComplete(delegate
        {
            DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-88"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate 
                {
                    _jimProgress.SetActive(true);
                    _jimProgress.transform.DOScale(_tempScaleJimProgress, 0.5f);

                    DOVirtual.DelayedCall(0.5f, delegate 
                    {
                        _activeBlanket.GetComponent<Collider2D>().enabled = true;
                        _curr_state = HippoBadroomJimState.SATATE_JUMP_JIM_ON_BED;
                        _startProgress = true;
                    });                    
                });
            });
        });
    }

    private void addToProgressJim()
    {
        _workProgress.GetComponent<Image>().fillAmount += _addToProgress;
    }

    private void JimSayNo()
    {
        _jimChar.GetComponent<Animator>().SetTrigger("Jim_say_no");

        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-90"); }).OnComplete(delegate
        {
            DOVirtual.DelayedCall(_time, delegate { JimSayYes(); });
        });
    }

    private void JimSayYes()
    {
        _jimChar.GetComponent<Animator>().SetTrigger("Jim_say_yes");
        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-91"); }).OnComplete(delegate
        {
            DOVirtual.DelayedCall(_time, delegate { JimsGoDawn(); });
        });
    }

    private void JimsGoDawn()
    {
        _jimChar.GetComponent<Animator>().SetTrigger("Jim_go_dawn");

        DOVirtual.DelayedCall(0.7f, delegate { _time = AudioController.PlaySound("hp-92"); }).OnComplete(delegate 
        {
            DOVirtual.DelayedCall(_time, delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBadroomPeppa); });
        });
    }

    private IEnumerator WaitForJumpAnimEnd()
    {
        Animator animator = _jimChar.GetComponent<Animator>();
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("DgimJump") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        DOVirtual.DelayedCall(0.6f, delegate
        {
            addToProgressJim();
            if (_workProgress.GetComponent<Image>().fillAmount >= 0.9f)
            {
                _startProgress = false;
                _jimProgress.transform.DOScale(Vector2.zero, 0.5f).OnComplete(delegate
                {
                    _jimProgress.SetActive(false);
                });

                _curr_state = HippoBadroomJimState.NONE;
                DOVirtual.DelayedCall(1.5f, delegate { _time = AudioController.PlaySound("hp-89"); }).OnComplete(delegate
                {
                    DOVirtual.DelayedCall(_time, delegate { JimSayNo(); });
                });
            }
            else _activeBlanket.GetComponent<Collider2D>().enabled = true;
        });         
    }
}
