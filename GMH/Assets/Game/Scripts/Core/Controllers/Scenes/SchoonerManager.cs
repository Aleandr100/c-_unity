﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace GoodMorningHippo.Managers
{
    public class SchoonerManager : MonoBehaviour
    {
        public static SchoonerManager Instance;
        
        [SerializeField] private Animator _mech_anim;
        [SerializeField] private Animator _cuckoo_anim;

        [SerializeField] private List<Transform> _ropes_edge;
        [SerializeField] private List<LineRenderer> _ropes;

        [SerializeField] Transform _transf_mary;
        [SerializeField] SpriteRenderer[] _hide_items;

        private readonly string _itemSL = "Hide";

        private float _time;

        private bool _is_moved_mary = false;

        public bool is_moved_mary {
            get {
                return _is_moved_mary;
            }
            set {
                _is_moved_mary = value;
            }
        }

        void Awake()
        {
            Instance = this;
        }

        void Start()
        {
            DrawRopes();

            _time = AudioController.PlaySound("hp-205");
            DOVirtual.DelayedCall(_time, delegate { StartCoroutine(WaitForEndCuckooAnim()); });            
        }

        void Update()
        {
            if (!is_moved_mary)
                DrawRopes();
        }

        public void DrawRopes()
        {
            int target_pivot = 4;

            for (int i = 0; i < _ropes.Count - 2; i++)
            {
                _ropes[i].SetPosition(0, _ropes_edge[i].position);
                _ropes[i].SetPosition(1, _ropes_edge[target_pivot].position);
            }

            target_pivot++;

            for (int i = 2; i < _ropes.Count; i++)
            {
                _ropes[i].SetPosition(0, _ropes_edge[i].position);
                _ropes[i].SetPosition(1, _ropes_edge[target_pivot].position);
            }
        }

        private IEnumerator WaitForEndCuckooAnim()
        {
            while (_cuckoo_anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
                yield return null;

            _cuckoo_anim.enabled = false;

            _mech_anim.SetTrigger("Mech_move");

            StartCoroutine(WaitForEndMechAnim());
        }

        private IEnumerator WaitForEndMechAnim()
        {
            yield return new WaitForSeconds(0.05f);
            while (_mech_anim.GetCurrentAnimatorStateInfo(0).IsName("mech_move") && (_mech_anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.35f))
                yield return null;

            StartCoroutine(WaitForJumpAnim());

            is_moved_mary = true;
        }

        private void ChangeLayers()
        {
            foreach (SpriteRenderer renderer in _hide_items)
            {
                renderer.sortingLayerName = _itemSL;
            }

            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.RaccoonMaryShallow);
        }

        private IEnumerator WaitForJumpAnim()
        {
            while (true)
            {
                if (!_mech_anim.GetCurrentAnimatorStateInfo(0).IsName("jump"))
                    yield return null;
                else
                {
                    while (_mech_anim.GetCurrentAnimatorStateInfo(0).IsName("jump") && (_mech_anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f))
                        yield return null;

                    ChangeLayers();
                    break;
                }
            }
        }
    }
}

