﻿using UnityEngine;
using System.Collections.Generic;
using GoodMorningHippo.Objects;
using System.Collections;
using DG.Tweening;

namespace GoodMorningHippo.Managers
{
    public class ShallowManager : MonoBehaviour
    {
        public static ShallowManager Instance;

        [SerializeField]
        private GameObject _crab,
                           _jellyfish;

        private float _rightPosX,
                      _leftPosX,
                      _xCrabSpeed = 0.25f,
                      _targetCrabPos,
                      _jellyfishSpeed = 0.08f;

        private const int COUNT_CATCHED_FISH = 7;     

        private int _count_catched_item;
        private bool _is_stop_catched;

        private int _active_fish_num;

        private float _time;

        public int CatchedItems {
            set {
                _count_catched_item += value;
            }
        }

        public bool IsStopCatched {
            get {
                return _is_stop_catched;
            }
        }

        public bool StartCatching{ get; set; }

        private void Awake()
        {
            Instance = this;

            _rightPosX = Random.Range(-0.95f, 1.05f);
            _leftPosX  = Random.Range(-2.15f, -4.55f);

            _targetCrabPos = _rightPosX;
        }

        private void Start()
        {
            _count_catched_item = 0;
            _is_stop_catched = false;

            DOVirtual.DelayedCall(0.5f, delegate { _time = AudioController.PlaySound("hp-208"); }).OnComplete(delegate 
            {
                DOVirtual.DelayedCall(_time, delegate { StartCatching = true; });
            });
        }

        private void Update()
        {
            _crab.transform.position = Vector2.MoveTowards(_crab.transform.position, new Vector2(_targetCrabPos, _crab.transform.position.y), _xCrabSpeed * Time.deltaTime);

            _jellyfish.transform.position = Vector2.MoveTowards(_jellyfish.transform.position, new Vector2(2.65f, _jellyfish.transform.position.y), _jellyfishSpeed * Time.deltaTime);

            if (_targetCrabPos == _rightPosX)
            {
                if (_crab.transform.position.x >= _targetCrabPos)
                {
                    _leftPosX = Random.Range(-2.15f, -4.55f);
                    _targetCrabPos = _leftPosX;
                }
            }
            else if (_targetCrabPos == _leftPosX)
            {
                if (_crab.transform.position.x <= _targetCrabPos)
                {
                    _rightPosX = Random.Range(-0.95f, 1.05f);
                    _targetCrabPos = _rightPosX;
                }
            }                         

            if (_count_catched_item == COUNT_CATCHED_FISH)
            {
                _is_stop_catched = true;
            }
        }        
    }      
}
