﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Game.Enumerators;
using AppCore;

public class HippoKitchenKidsEatingController : MonoBehaviour {

    [SerializeField]
    private Animator _hippi,
                     _jim,
                     _hippoMom;

    [SerializeField]
    private GameObject _jimCloudWishes,
                       _hippiCloudWishes,
                       _cake,
                       _baozi,
                       _soup,
                       _spaghetti;

    private IList<KitchenFood> _foodLeftJim,
                              _foodLeftHippi;

    private GameObject _draggableObject;

    private bool _canEatJim = false,
                 _canEatHippi = false,
                 _changeLevelCalled = false;

    [SerializeField]
    private Transform _jimOutTransform,
                      _jimChairTransform,
                      _hippiStaffContainerTransform,
                      _jimStaffContainerTransform,
                      _hippiOutTransform,
                      _hippiChairTransform;

    private KitchenFood _jimWish,
                        _hippoWish;

    private int _foodEatCountJim = 3,
                _foodEatCountHippi = 3;

    private void Awake()
    {
        _foodLeftHippi = new List<KitchenFood>();
        _foodLeftJim = new List<KitchenFood>();

        _jimCloudWishes.SetActive(false);
        _hippiCloudWishes.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        var allFood = new List<KitchenFood>();

        for (int i = 0; i < Extensions.GetEnumSize<KitchenFood>(); i++)
        {
            allFood.Add((KitchenFood)i);
        }

        while (allFood.Count > 0) {
            if (allFood.Count > 0)
                _foodLeftHippi.Add(allFood.GetRandomAndRemove());
            else
                break;
            if (allFood.Count > 0)
                _foodLeftJim.Add(allFood.GetRandomAndRemove());
            else
                break;
        }

        _jim.Play("walk", 0, 0);
        _hippi.Play("walk", 0, 0);
        
        _jim.transform.position = _jimOutTransform.position;
        
        _jim.transform.DOMove(_jimChairTransform.position, 2.2f).SetEase(Ease.Linear).OnComplete(delegate
        {
            _jim.SetTrigger("sit_idle");
        });

        _hippi.transform.position = _hippiOutTransform.position;

        DOVirtual.DelayedCall(AudioController.PlaySound("hp-131"), delegate {
            _hippoMom.GetComponent<Animator>().SetBool("isTalking", true);
            DOVirtual.DelayedCall(AudioController.PlaySound("mh-4"), delegate {
                _hippoMom.GetComponent<Animator>().SetBool("isTalking", false);
                DOVirtual.DelayedCall(AudioController.PlaySound("hp-132"), delegate
                {
                    Wish(_hippi);
                    _canEatHippi = true;

                    Wish(_jim);
                    _canEatJim = true;
                });
            });
        });

        _hippi.transform.DOMove(_hippiChairTransform.position, 2.5f).SetEase(Ease.Linear).OnComplete(delegate
        {
            _hippi.SetTrigger("sit_idle");
        });

        
    }

    float FoodEatAction(KitchenFood foodTaken, Animator charAnimator)
    {
        switch (foodTaken)
        {
            case KitchenFood.FOOD_BAOZI:
                charAnimator.Play("eating_baozi");
                break;
            case KitchenFood.FOOD_CAKE:
                charAnimator.Play("eating_cake");
                break;
            case KitchenFood.FOOD_SOUP:
                charAnimator.Play("eating_soup1");
                break;
            case KitchenFood.FOOD_SPAGHETTI:
                charAnimator.Play("eating_spaghetti");
                break;
        }

        float actionLength = charAnimator.GetCurrentAnimatorClipInfo(0).Length;

        return actionLength;
    }

    void Wish(Animator charAnimator)
    {
        if (charAnimator == _hippi)
        {
            _hippiCloudWishes.SetActive(true);
            _hippoWish = _foodLeftHippi.GetRandomAndRemove();
            
            foreach (Transform child in _hippiStaffContainerTransform)
            {
                child.gameObject.SetActive(_hippoWish.ToString().ToLower() == child.name);
            }

            _hippiCloudWishes.GetComponent<Animator>().Play("show", 0, 0);
        }
        else
        {
            _jimCloudWishes.SetActive(true);
            _jimWish = _foodLeftJim.GetRandomAndRemove();

            foreach (Transform child in _jimStaffContainerTransform)
            {
                child.gameObject.SetActive(_jimWish.ToString().ToLower() == child.name);
            }

            _jimCloudWishes.GetComponent<Animator>().Play("show", 0, 0);
        }
    }

    void CheckChildrenFed()
    {
        if (_foodLeftHippi.Count <= 0 && _foodLeftJim.Count <= 0 && !_changeLevelCalled)
        {
            _changeLevelCalled = true;
            AudioController.ReleaseStreams();
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-133"), delegate
            {
                DOVirtual.DelayedCall(AudioController.PlaySound("mh-5"), delegate
                {
                    Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_1_LYALYA;
                    PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu);
                });
            });
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

            if (colliderTouched)
            {
                if (colliderTouched.name.ToLower().Contains("food_"))
                {
                    _draggableObject = colliderTouched.gameObject;

                    var foodTouched = colliderTouched.name.ToLower().ToEnum<KitchenFood>();

                    if (foodTouched == _jimWish && _canEatJim)
                    {
                        _canEatJim = false;
                        _jimCloudWishes.GetComponent<Animator>().Play("hide", 0, 0);
                        FoodEatAction(foodTouched, _jim);
                        DOVirtual.DelayedCall(Random.Range(3.5f, 4.5f), delegate
                        {
                            if (_foodLeftJim.Count > 0)
                            {
                                _canEatJim = true;
                                Wish(_jim);
                            }
                            else
                            {
                                CheckChildrenFed() ;
                                _jim.SetTrigger("walk");
                                var scale = _jim.transform.localScale;
                                scale.x *= -1;
                                _jim.transform.localScale = scale;
                                _jim.transform.DOMove(_jimOutTransform.position, 3);
                            }
                        });
                    }
                    if (foodTouched == _hippoWish && _canEatHippi)
                    {
                        _canEatHippi = false;
                        _hippiCloudWishes.GetComponent<Animator>().Play("hide", 0, 0);
                        FoodEatAction(foodTouched, _hippi);
                        DOVirtual.DelayedCall(Random.Range(3.5f, 4.5f), delegate
                        {
                            if (_foodLeftHippi.Count > 0) {
                                _canEatHippi = true;
                                Wish(_hippi);
                            }
                            else
                            {
                                CheckChildrenFed();
                                _hippi.SetTrigger("walk");
                                var scale = _hippi.transform.localScale;
                                scale.x *= -1;
                                _hippi.transform.localScale = scale;
                                DOVirtual.DelayedCall(0.3f, delegate
                                {
                                    _hippi.transform.DOMove(_hippiOutTransform.position, 3);
                                });
                            }
                        });
                    }
                }
            }
        }
    }
}
