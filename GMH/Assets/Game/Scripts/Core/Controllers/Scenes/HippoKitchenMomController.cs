﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;

public class HippoKitchenMomController : MonoBehaviour {

    [SerializeField]
    private GameObject _momHippo,
                       _coffeeMachine; 

    [SerializeField]
    private Transform _startTransform,
                      _windowTransform,
                      _chairTransform,
                      _cupMamaHandTransform,
                      _cupMamaCoffeeMakerTransform;

    private GameObject _draggableObject;

    private bool _windowOpen = false,
                 _coffeeMaking = false;

    private Vector3 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    private Vector2 _prevMousePos,
                    _currentMousePos;

    [SerializeField]
    private Animator _windowAnimator;

    private HippoKitchenMomState _sceneState;

	// Use this for initialization
	void Start () {
        _sceneState = HippoKitchenMomState.STATE_MOM_WALKS_IN;

        _momHippo.transform.position = _startTransform.position;

        _momHippo.GetComponent<Animator>().SetBool("isWalking", true);

        _momHippo.transform.DOMove(_windowTransform.position, 3).SetEase(Ease.Linear).OnComplete(delegate
        {
            _momHippo.GetComponent<Animator>().SetBool("isWalking", false);
        });

        DOVirtual.DelayedCall(AudioController.PlaySound("hp-27"), delegate
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-28"), delegate
            {
                _sceneState = HippoKitchenMomState.STATE_OPENING_WINDOW;
            });
        });
    }
	
	// Update is called once per frame
	void Update () {
	    switch (_sceneState)
        {
            case HippoKitchenMomState.STATE_MOM_WALKS_IN:
                break;
            case HippoKitchenMomState.STATE_OPENING_WINDOW:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("window") && !_windowOpen)
                        {
                            _windowOpen = true;
                            _windowAnimator.Play("open");
                            _windowAnimator.GetComponentInChildren<ParticleSystem>().Play();

                            _momHippo.GetComponent<Animator>().SetBool("isWalking", true);

                            _momHippo.transform.DOMove(_chairTransform.position, 1.5f).SetEase(Ease.Linear).OnComplete(delegate
                            {
                                _sceneState = HippoKitchenMomState.STATE_CUP_TAKING;
                                DOVirtual.DelayedCall(AudioController.PlaySound("hp-29"), delegate
                                {
                                    AudioController.PlaySound("hp-30");
                                });
                               
                                _momHippo.GetComponent<Animator>().SetBool("isWalking", false);
                            });
                        }
                    }
                }
                break;
            case HippoKitchenMomState.STATE_CUP_TAKING:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("cup_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _coffeeMachine.name)
                        {
                            if (_draggableObject.name == "cup_mama")
                            {
                                _draggableObject.transform.SetParent(_cupMamaCoffeeMakerTransform);
                                _sceneState = HippoKitchenMomState.STATE_MAKING_COFFEE;
                                _momHippo.GetComponent<Animator>().SetTrigger("waiting");
                                _draggableObject.transform.DOMove(_cupMamaCoffeeMakerTransform.position, 0.5f).OnComplete(delegate
                                {
                                    AudioController.PlaySound("hp-34");
                                });
                                _draggableObject = null;
                                return;
                            }
                            else
                            {
                                AudioController.PlaySound("hp-3" + Random.Range(1,4));
                            }
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;
            case HippoKitchenMomState.STATE_MAKING_COFFEE:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _coffeeMachine.name && !_coffeeMaking)
                        {
                            _coffeeMaking = true;
                            foreach (ParticleSystem ps in _coffeeMachine.GetComponentsInChildren<ParticleSystem>())
                            {
                                ps.Play();
                            }

                            DOVirtual.DelayedCall(3, delegate
                            {
                                foreach (ParticleSystem ps in _coffeeMachine.GetComponentsInChildren<ParticleSystem>())
                                {
                                    AudioController.PlaySound("hp-35");
                                    ps.Stop();
                                }
                                _sceneState = HippoKitchenMomState.STATE_CUP_BRINGING;
                            });
                        }
                    }
                }
                break;
            case HippoKitchenMomState.STATE_CUP_BRINGING:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("cup_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    _draggableObject.transform.position += GetMouseDeltaWorld();
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _momHippo.name)
                        {
                            
                            _draggableObject.transform.DOMove(_cupMamaHandTransform.position, 0.5f);
                            _draggableObject.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                            _momHippo.GetComponent<Animator>().SetTrigger("drink");
                            _cupMamaHandTransform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                            _cupMamaHandTransform.GetComponent<SpriteRenderer>().DOFade(1, 0.5f).OnComplete(delegate {
                                DOVirtual.DelayedCall(AudioController.PlaySound("hp-36"), delegate
                                {
                                    Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_1_DAD;
                                    PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu);
                                });
                            });
                            _draggableObject = null;
                            return;
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);
                    _draggableObject = null;
                }
                break;
        }
	}
}
