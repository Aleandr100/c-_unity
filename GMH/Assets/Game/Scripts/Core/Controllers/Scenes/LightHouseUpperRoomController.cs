﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LightHouseUpperRoomController : MonoBehaviour
{

    [SerializeField]
    GameObject _granddad;

    [SerializeField]
    SpriteRenderer _web1,
                   _web2,
                   _web3;

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private Vector3 _window1 = new Vector3(0, 0, -10),
                    _window2 = new Vector3(17.8f, 0, -10),
                    _window3 = new Vector3(35.46f, 0, -10);

    private Vector3 _dadPosStart = new Vector3(-12.97f, -3.505f, 10),
                    _cameraCenterPos = new Vector3(-4.45f, -3.505f, 10),
                    _walkStairs1 = new Vector3(4.23f, -3.505f, 10),
                    _walkStairs2 = new Vector3(11.45f, 0.69f, 10);

    private void Awake()
    {
        Camera.main.transform.position = _window1;

        _granddad.transform.position = _dadPosStart;

        _granddad.GetComponent<Animator>().SetBool("isWalking", true);
        _granddad.transform.DOLocalMove(_cameraCenterPos, 2).SetEase(Ease.Linear).OnComplete(StopAndSayAyaya);
    }

    private void StopAndSayAyaya()
    {
        _granddad.GetComponent<Animator>().SetBool("isWalking", false);
        DOVirtual.DelayedCall(AudioController.PlaySound("gh-1"), delegate
        {
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-192"), delegate
            {
                AudioController.PlaySound("hp-194");
            });
        });
        _granddad.GetComponent<Animator>().SetTrigger("ayaya");
    }

    protected Vector2 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _currentMousePos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            _prevMousePos = _currentMousePos;
            _currentMousePos = Input.mousePosition;

            Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

            if (colliderTouched)
            {
                if (colliderTouched.name.Contains("bg_"))
                {
                    if (Camera.main.transform.position.Equals(_window1) && _web1.gameObject.activeSelf)
                    {
                        if (AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                        {
                            var color = _web1.color;
                            Vector2 mouseDeltaWorld = GetMouseDeltaWorld();
                            color.a -= Mathf.Sqrt(mouseDeltaWorld.x * mouseDeltaWorld.x + mouseDeltaWorld.y * mouseDeltaWorld.y) * 0.05f;
                            _web1.color = color;

                            if (color.a <= 0)
                            {
                                _web1.gameObject.SetActive(false);
                                colliderTouched.GetComponentInChildren<ParticleSystem>().Play();
                                StartCoroutine("HahaAndGoSecond");
                            }
                        }
                        else
                        {
                            _web1.gameObject.SetActive(false);
                            colliderTouched.GetComponentInChildren<ParticleSystem>().Play();
                            StartCoroutine("HahaAndGoSecond");
                        }
                    }
                    else if (Camera.main.transform.position.Equals(_window2) && _web2.gameObject.activeSelf)
                    {
                        if (AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                        {
                            var color = _web2.color;
                            Vector2 mouseDeltaWorld = GetMouseDeltaWorld();
                            color.a -= Mathf.Sqrt(mouseDeltaWorld.x * mouseDeltaWorld.x + mouseDeltaWorld.y * mouseDeltaWorld.y) * 0.05f;
                            _web2.color = color;

                            if (color.a <= 0)
                            {
                                _web2.gameObject.SetActive(false);
                                colliderTouched.GetComponentInChildren<ParticleSystem>().Play();
                                StartCoroutine("HahaAndGoThird");
                            }
                        }
                        else
                        {
                            _web2.gameObject.SetActive(false);
                            colliderTouched.GetComponentInChildren<ParticleSystem>().Play();
                            StartCoroutine("HahaAndGoThird");
                        }
                    }
                    else if (Camera.main.transform.position.Equals(_window3) && _web3.gameObject.activeSelf)
                    {
                        if (AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                        {
                            var color = _web3.color;
                            Vector2 mouseDeltaWorld = GetMouseDeltaWorld();
                            color.a -= Mathf.Sqrt(mouseDeltaWorld.x * mouseDeltaWorld.x + mouseDeltaWorld.y * mouseDeltaWorld.y) * 0.05f;
                            _web3.color = color;

                            if (color.a <= 0)
                            {
                                _web3.gameObject.SetActive(false);
                                colliderTouched.GetComponentInChildren<ParticleSystem>().Play();
                                StartCoroutine("HahaAndWalkOut");
                            }
                        }
                        else
                        {
                            _web3.gameObject.SetActive(false);
                            colliderTouched.GetComponentInChildren<ParticleSystem>().Play();
                            StartCoroutine("HahaAndWalkOut");
                        }
                    }
                }
            }
        }
    }
    private IEnumerator HahaAndGoSecond()
    {
        AudioController.PlaySound("hp-195");
        _granddad.GetComponent<Animator>().SetTrigger("haha");
        yield return new WaitForSeconds(4);
        _granddad.GetComponent<Animator>().SetBool("isWalking", true);
        Camera.main.transform.DOMove(_window2, 4).SetEase(Ease.Linear).OnComplete(StopAndSayAyaya);
    }
    private IEnumerator HahaAndGoThird()
    {
        AudioController.PlaySound("hp-195");
        _granddad.GetComponent<Animator>().SetTrigger("haha");
        yield return new WaitForSeconds(4);
        _granddad.GetComponent<Animator>().SetBool("isWalking", true);
        Camera.main.transform.DOMove(_window3, 4).SetEase(Ease.Linear).OnComplete(StopAndSayAyaya);
    }
    private IEnumerator HahaAndWalkOut()
    {
        AudioController.PlaySound("hp-195");
        _granddad.GetComponent<Animator>().SetTrigger("haha");
        yield return new WaitForSeconds(4);
        _granddad.GetComponent<Animator>().SetBool("isWalking", true);
        _granddad.transform.DOLocalMove(_walkStairs1, 2f).SetEase(Ease.Linear).OnComplete(GetUpStairs);
    }

    private void GetUpStairs()
    {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-196"), delegate
        {
            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoGrandsLighthouseRoom);
        });
        _granddad.GetComponent<Animator>().SetTrigger("upstairs");
    }
}
