﻿using AppCore;
using UnityEngine;
using System.Collections.Generic;
using GMH.Interfaces;
using GMH.Objects;
using System.Linq;
using DG.Tweening;

namespace GMH.Controllers
{
    public class TestController : IHippoBadroomMomController
    {
        private GameObject _table_watch,
                             _table_watch_pref,

                             _digitalWatch,

                             _hippo_mom,
                             _hippo_mom_pref,

                             _slippers_pool,
                             _slippers_pool_pref,

                             _curr_slipper,
                             _colliders,
                             _movement_item,

                             _board,
                             _blanket,

                             _bad,
                             _bad_pref;

        private GameObject[] _slippers,
                             _targets_pos;

        private Animator _watch_anim,
                            _hippo_mather_anim;

        private Vector2 _last_target_pos;

        private Transform[] _legs_pos;

        private SpriteRenderer _curr_render;

        private Vector2 _offset,
                        _startPos,
                        _target_pos;

        private string _last_name_slipper;

        private string[] _randomVoice;

        private float _speed,
                      _move_back_speed,

                      _move_to_target_speed,
                      _mom_move_speed,
                      _time;

        private bool _is_active_controll,
                      _is_toch_active,

                      _is_dressed,
                      _moveToTarget;

        private int _count_drag_slippers,
                      _index_event,
                      _index_pos;

        private LayerMask touchInputMask;

        private List<SpriteRenderer> _sprite_slippers;
        private List<TimeController.OnTimerHandler> _call_backs;

        private Dictionary<Transform, Vector2> _start_pos_slipper;

        public void Awake()
        {
            _slippers = new GameObject[2];
            _legs_pos = new Transform[2];
            _randomVoice = new string[5] { "hp-5", "hp-6", "hp-7", "hp-8", "hp-9" };

            _table_watch_pref = Resources.Load("Game/Prefabs/HippoBadroomMom/table") as GameObject;
            _hippo_mom_pref = Resources.Load("Game/Prefabs/HippoBadroomMom/hippo_mom") as GameObject;
            _slippers_pool_pref = Resources.Load("Game/Prefabs/HippoBadroomMom/slippers") as GameObject;
            _bad_pref = Resources.Load("Game/Prefabs/HippoBadroomMom/bad") as GameObject;

            _table_watch = GameObject.Instantiate(_table_watch_pref);
            _hippo_mom = GameObject.Instantiate(_hippo_mom_pref);
            _slippers_pool = GameObject.Instantiate(_slippers_pool_pref);
            _bad = GameObject.Instantiate(_bad_pref);

            _digitalWatch = _table_watch.transform.Find("clockElektrBase").gameObject;
            _slippers[0] = _hippo_mom.transform.Find("Noga_1/slipper_container_L").gameObject;
            _slippers[1] = _hippo_mom.transform.Find("Noga_2/slipper_container_R").gameObject;
            _colliders = _hippo_mom.transform.Find("_colliders").gameObject;
            _blanket = _bad.transform.Find("blanket").gameObject;
            _board = _bad.transform.Find("board").gameObject;

            _legs_pos[0] = _hippo_mom.transform.Find("Noga_1/target_pos_l");
            _legs_pos[1] = _hippo_mom.transform.Find("Noga_2/target_pos_r");

            _watch_anim = _digitalWatch.GetComponent<Animator>();
            _hippo_mather_anim = _hippo_mom.GetComponent<Animator>();
        }

        public void Start()
        {
            _count_drag_slippers = 0;
            _last_target_pos = new Vector2(10.55f, -4.15f);

            touchInputMask = LayerMask.NameToLayer("Touchable");

            _last_name_slipper = "";

            _is_toch_active = false;
            _moveToTarget = false;
            _is_dressed = false;

            _curr_slipper = null;

            _mom_move_speed = 0.1f;
            _move_back_speed = 0.35f;
            _move_to_target_speed = 0.25f;

            _index_pos = 0;

            _is_active_controll = false;

            //DropeZone.OnDragItem += TargetHasBeenReached;

            InitSlippersSprite();

            InitStartPosSlippers();

            InitEventsActions();

            _watch_anim.SetTrigger("Set_first");

            //***********************************for_test***********************************
            //ServiceLocator.GetService<IAgeController>().age = AppCore.Enumerators.Age.TWO_PLUS;
            //***********************************for_test***********************************

            if (ServiceLocator.GetService<IAgeController>().age != AppCore.Enumerators.Age.FIVE_PLUS)
            {
                DisableCollider();
            }

            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(_call_backs.ElementAt(0), null, Time.time, Time.time + 3.5f);
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(_call_backs.ElementAt(1), null, Time.time, Time.time + 6.7f);
        }

        private void InitEventsActions()
        {
            _index_event = 0;

            _call_backs = new List<TimeController.OnTimerHandler>();

            _call_backs.Add(delegate {
                _hippo_mather_anim.SetTrigger("Wake_up");

                ServiceLocator.GetService<ITimeController>().RemoveTimer(_call_backs[0]);
            });

            _call_backs.Add(delegate {
                _watch_anim.SetTrigger("Push_button");

                DOVirtual.DelayedCall(1.9f, delegate { AudioController.PlaySound("hp-2"); }).OnComplete(delegate
                {
                    DOVirtual.DelayedCall(3.2f, delegate { AudioController.PlaySound("hp-3"); }).OnComplete(delegate
                    {
                        DOVirtual.DelayedCall(2.2f, delegate { AudioController.PlaySound("hp-4"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(2.4f, delegate { AudioController.PlaySound(_randomVoice[Random.Range(0, 4)]); }).OnComplete(delegate
                            {
                                DOVirtual.DelayedCall(1.9f, delegate { AudioController.PlaySound("hp-10"); }).OnComplete(delegate
                                {
                                    DOVirtual.DelayedCall(2.5f, delegate
                                    {
                                        _is_active_controll = true;
                                    });
                                });
                            });
                        });
                    });
                });

                ServiceLocator.GetService<ITimeController>().RemoveTimer(_call_backs[1]);

            });

            _call_backs.Add(delegate {
                _is_active_controll = true;

                ServiceLocator.GetService<ITimeController>().RemoveTimer(_call_backs[2]);
            });

            _call_backs.Add(delegate {
                _hippo_mather_anim.SetTrigger("Go_dawn");
                DOVirtual.DelayedCall(1.5f, delegate { AudioController.PlaySound("hp-16"); });
                _blanket.SetActive(true);

                ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(_call_backs[4], null, Time.time, Time.time + 2.2f);
            });

            _call_backs.Add(delegate {
                ChangeSortingLayer();

                _board.GetComponent<SpriteRenderer>().sortingOrder = 25;

                ServiceLocator.GetService<ITimeController>().RemoveTimer(_call_backs[4]);

                _target_pos = _last_target_pos;
                _speed = _mom_move_speed;
                _movement_item = _hippo_mom;

                _moveToTarget = true;
            });
        }

        //**********************last_changes**********************
        private void HippoMomWalkOut()   //when all slippers is dressed
        {
            _hippo_mather_anim.SetTrigger("Go_dawn");
            _blanket.SetActive(true);

            DOVirtual.DelayedCall(1.5f, delegate { _time = AudioController.PlaySound("hp-16"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate
                {
                    ChangeSortingLayer();
                    _board.GetComponent<SpriteRenderer>().sortingOrder = 25;
                });
            });
        }    
        //**********************last_changes**********************

        private void InitSlippersSprite()
        {
            _sprite_slippers = new List<SpriteRenderer>();
            foreach (Transform slpr in _slippers_pool.transform)
            {
                _sprite_slippers.Add(slpr.GetChild(0).GetComponent<SpriteRenderer>());
            }
        }

        private void InitStartPosSlippers()
        {
            _start_pos_slipper = new Dictionary<Transform, Vector2>();
            foreach (Transform slpr in _slippers_pool.transform)
            {
                _start_pos_slipper.Add(slpr, slpr.position);
            }
        }

        public void Update()
        {
            if (_moveToTarget)
            {
                if ((Vector2)_movement_item.transform.position != _target_pos)
                {
                    Extensions.MoveTo(_movement_item, _target_pos, _speed);
                }
                else
                {
                    if (ServiceLocator.GetService<IAgeController>().age != AppCore.Enumerators.Age.FIVE_PLUS)
                    {
                        TargetHasBeenReached(_movement_item.transform.GetChild(1).gameObject);
                    }

                    _moveToTarget = false;

                    if (_movement_item == _hippo_mom)
                    {
                        PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBathroomMom);
                    }
                    else
                    {
                        _curr_slipper = null;
                    }
                }
            }
        }

        private void DisableCollider()
        {
            CircleCollider2D[] colliders = _colliders.GetComponents<CircleCollider2D>();
            foreach (CircleCollider2D collider in colliders)
            {
                collider.enabled = false;
            }
        }

        private void ChangeSortingLayer()
        {
            foreach (Transform slipper in _slippers_pool.transform)
            {
                slipper.GetChild(1).GetComponent<SpriteRenderer>().sortingOrder = 30;
            }
        }

        private SpriteRenderer FindSprite(GameObject go)
        {
            SpriteRenderer spr_render = null;
            foreach (SpriteRenderer renderer in _sprite_slippers)
            {
                if (renderer.transform.parent.gameObject == go)
                {
                    spr_render = renderer;
                    return spr_render;
                }
            }

            return spr_render;
        }

        private void SetSprite(int index)
        {
            _slippers[index].GetComponent<SpriteRenderer>().sprite = _curr_render.sprite;
            _slippers[index].SetActive(true);
        }

        private void TargetHasBeenReached(GameObject go)
        {
            DropeZone.OnDragItem -= TargetHasBeenReached;

            GameObject slipper = go.transform.parent.gameObject;

            _curr_render = FindSprite(slipper);

            slipper.SetActive(false);

            if (_count_drag_slippers == 0)
            {
                _last_name_slipper = slipper.name;
                _count_drag_slippers++;

                SetSprite(0);
                _hippo_mather_anim.SetTrigger("Сorrect_slipper");

                AudioController.PlaySound("hp-14");

                ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(_call_backs.ElementAt(2), null, Time.time, Time.time + 0.7f);

                _is_dressed = true;
                _curr_slipper = null;
            }
            else
            {
                if (slipper.name == _last_name_slipper)
                {
                    SetSprite(1);
                    _hippo_mather_anim.SetTrigger("Сorrect_slipper");

                    AudioController.PlaySound("hp-15");

                    _call_backs.ElementAt(3)(null);

                    _is_active_controll = false;
                    _is_dressed = true;

                    _curr_slipper = null;
                }
                else
                {
                    slipper.gameObject.SetActive(true);

                    AudioController.PlaySound("hp-11");
                    _hippo_mather_anim.SetTrigger("Wrong_slipper");
                }
            }
        }

        private GameObject GetClickedObject()
        {
            GameObject target = null;

            Vector2 end_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hit = Physics2D.Raycast(end_pos, Vector2.zero, 20, 1 << touchInputMask);

            if (hit.collider != null)
            {
                target = hit.collider.gameObject;
            }

            return target;
        }

        public void OnTouchDown()
        {
            if (_is_active_controll)
            {
                GameObject go = GetClickedObject();

                if (go != null)
                {
                    DropeZone.OnDragItem += TargetHasBeenReached;

                    _curr_slipper = go.transform.parent.gameObject;

                    if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                    {
                        _startPos = _start_pos_slipper[_curr_slipper.transform];

                        var mouse_pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                        _offset = _curr_slipper.transform.position - Camera.main.ScreenToWorldPoint(mouse_pos);
                    }
                    else
                    {
                        if (_count_drag_slippers != 0)
                        {
                            if (_curr_slipper.name == _last_name_slipper)
                            {
                                _moveToTarget = true;

                                _movement_item = _curr_slipper;
                                _target_pos = _legs_pos[_index_pos].position;
                            }
                            else
                            {
                                AudioController.PlaySound("hp-11");
                                _hippo_mather_anim.SetTrigger("Wrong_slipper");
                            }
                        }
                        else
                        {
                            _moveToTarget = true;
                            _is_active_controll = false;

                            _movement_item = _curr_slipper;
                            _speed = _move_to_target_speed;
                            _target_pos = _legs_pos[_index_pos].position;

                            _index_pos++;
                        }
                    }
                }
            }
        }

        public void OnTouch()
        {
            if (_is_active_controll && _curr_slipper)
            {
                if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                {
                    var cur_screen_space = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                    var cur_pos = (Vector2)Camera.main.ScreenToWorldPoint(cur_screen_space) + _offset;

                    _curr_slipper.transform.position = cur_pos;
                }
            }
        }

        public void OnTouchUp()
        {
            if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
            {
                if (!_is_dressed && _curr_slipper)
                {
                    _moveToTarget = true;

                    _speed = _move_back_speed;
                    _movement_item = _curr_slipper;
                    _target_pos = _startPos;
                }
                else
                {
                    _is_dressed = false;
                }
            }
        }
    }
}
