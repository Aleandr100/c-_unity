﻿using UnityEngine;
using System.Collections;
using Game.Enumerators;
using UnityEngine.UI;
using DG.Tweening;

public class HippoBadroomPeppaController : MonoBehaviour {

    [SerializeField]
    private GameObject _blanket,
                       _jimChar,
                       _peppaChar,
                       _mucicCenter,
                       _openedWindow,
                       _camera,
                       _photik,
                       _stataticBlanket,
                       _activeBlanket,
                       _jimProgress,
                       _workProgress;

    [SerializeField]
    private GameObject[] _curtain;

    [SerializeField]
    private ParticleSystem musicEffect;

    private Collider2D _colliderTouched;

    private float _time = 0,
                  _addToProgress = 0.35f;

    private int _shakeCount = 0,
                _exercises  = 0,
                _curtainIndex,
                _countCurtainOpened = 0;

    private bool _stopTalking  = false,
                 _startToTilts = false,
                 _startSwipe   = false,
                 _moveCurtain  = false,
                 _startProgress = false;

    private Vector3 _startPos,
                    _endPos,
                    _currentSwipe,
                    _tempScaleJimProgress;

    private Vector3[] _jimWayPointsIn = new Vector3[]
    {
            new Vector3(-3.91f, -2.33f,  0),
            new Vector3(-0.8f,  -1.999f, 0)
    };

    private Vector3[] _jimWayPointsOut = new Vector3[]
    {
            new Vector3(0.01f, -2.43f,  0),
            new Vector3(1.6f,  -2.7f,   0),
            new Vector3(6.47f, -2.746f, 0)
    };

    private Vector3[] _peppaWayPointsTakePhoto = new Vector3[]
    {
            new Vector3(0.32f,  -2.34f, 0),
            new Vector3(-0.43f, -1.9f,  0),
            new Vector3(-1.05f, -1.4f,  0)
    };

    private HippoBadroomPeppaState _curr_state;
    private HippoBadroomPeppaState _tiltState;

    private void Start()
    {
        _curr_state = HippoBadroomPeppaState.NONE;

        _jimProgress.SetActive(false);
        _tempScaleJimProgress = _jimProgress.transform.localScale;
        _jimProgress.transform.localScale = Vector2.zero;

        _jimChar.transform.DOPath(_jimWayPointsIn, 3.5f).OnComplete(delegate
        {
            _jimChar.GetComponent<Animator>().SetTrigger("Jim_idle");
        });

        DOVirtual.DelayedCall(2.5f, delegate { _time = AudioController.PlaySound("hp-93"); }).OnComplete(delegate
        {
            DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-94"); }).OnComplete(delegate
            {
                _jimProgress.SetActive(true);
                _jimProgress.transform.DOScale(_tempScaleJimProgress, 0.5f);

                DOVirtual.DelayedCall(_time, delegate
                {
                    _startProgress = true;
                    _blanket.GetComponent<Collider2D>().enabled = true;
                    _curr_state = HippoBadroomPeppaState.STATE_JIM_SHAKE_BED;                    
                });
            });
        });
    }

    private void Update()
    {
        if (_moveCurtain)
        {
            MoveCurtain();
        }

        if (_startProgress)
        {
            _workProgress.GetComponent<Image>().fillAmount -= 0.0004f;
        }

        if (UnityEngine.Input.GetMouseButtonDown(0))
        {
            OnTouchDown();
        }
        else if (UnityEngine.Input.GetMouseButton(0))
        {
            OnTouch();
        }
        else if (UnityEngine.Input.GetMouseButtonUp(0))
        {
            OnTouchUp();
        }
    }

    private void OnTouchDown()
    {
        Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _colliderTouched = Physics2D.OverlapPoint(touchToWorld);

        if (_colliderTouched != null)
        {
            switch (_curr_state)
            {
                case HippoBadroomPeppaState.STATE_JIM_SHAKE_BED:
                    if (_colliderTouched.name == "odeyalko1")
                    {
                        _jimChar.GetComponent<Animator>().SetTrigger("Jim_start_shake");
                       
                        _blanket.GetComponent<Collider2D>().enabled = false;
                        StartCoroutine(WaitForJumpAnimEnd());
                    }
                    break;
                case HippoBadroomPeppaState.STATE_PEPPA_MUSIC_ENABLED:
                    if (_colliderTouched.name == "musCentre")
                    {
                        musicEffect.Play();
                        _mucicCenter.GetComponent<Animator>().enabled = true;
                        _mucicCenter.GetComponent<Collider2D>().enabled = false;

                        _time = AudioController.PlaySound("hp-103");
                        DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-104"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate
                            {
                                _curr_state = HippoBadroomPeppaState.STATE_PEPPA_TILTS_START;
                                _startSwipe = true;
                                _tiltState = HippoBadroomPeppaState.STATE_CHARS_TILT_LEFT;
                            });
                        });
                    }
                    break;
                case HippoBadroomPeppaState.STATE_PEPPA_TILTS_START:                    
                    _startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    break;
                case HippoBadroomPeppaState.STATE_MOVE_CURTAIN:
                    if (!_startSwipe)
                    {
                        if (_colliderTouched.name == "curtain_r")
                        {
                            _moveCurtain = true;
                            _curtainIndex = 0;
                        }
                        else if (_colliderTouched.name == "curtain_l")
                        {
                            _moveCurtain = true;
                            _curtainIndex = 1;
                        }
                    }
                    else
                    {
                        _startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    }
                    break;
                case HippoBadroomPeppaState.SATATE_MOVE_OPEN_WINDOW:
                    if (_colliderTouched.name == "window")
                    {
                        _openedWindow.GetComponent<Animator>().enabled = true;
                        _time = AudioController.PlaySound("hp-123");

                        DOVirtual.DelayedCall(_time, delegate 
                        {
                            
                            _peppaChar.GetComponent<Animator>().SetTrigger("Take_camera");
                            DOVirtual.DelayedCall(0.3f, delegate { _camera.SetActive(false); }).OnComplete(delegate 
                            {
                                StartCoroutine(WaitForTakeCameraPeppaAnimEnd());
                            });                                                                                  
                        });
                    }
                    break;
            }
        }
    }

    private void addToProgressJim()
    {
        _workProgress.GetComponent<Image>().fillAmount += _addToProgress;
    }

    private IEnumerator WaitForTakeCameraPeppaAnimEnd()
    {
        yield return new WaitForSeconds(0.5f);
        Animator animator = _peppaChar.GetComponent<Animator>();
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("GippiTakeCamera") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        _peppaChar.transform.DOPath(_peppaWayPointsTakePhoto, 2.7f).OnComplete(delegate
        {
            _photik.SetActive(true);
            _peppaChar.GetComponent<Animator>().SetTrigger("Peppa_idle");

            DOVirtual.DelayedCall(0.3f, delegate 
            {
                float xScale = _peppaChar.transform.localScale.x * (-1);
                _peppaChar.transform.localScale = new Vector2(xScale, _peppaChar.transform.localScale.y);
            });
        });
    }

    private void OnTouch()
    {
        /**/
    }

    private void OnTouchUp()
    {
        if (_startSwipe)
        {
            _endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _currentSwipe = new Vector2(_endPos.x - _startPos.x, _endPos.y - _startPos.y);
            _currentSwipe.Normalize();

            switch (_tiltState)
            {               
                case HippoBadroomPeppaState.STATE_CHARS_TILT_LEFT:
                    if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                    {
                        _jimChar.GetComponent<Animator>().SetTrigger("Tilt_to_left");
                        _peppaChar.GetComponent<Animator>().SetTrigger("Tilt_to_left");

                        _exercises++;
                    }
                    if (_exercises == 4)
                    {
                        _tiltState = HippoBadroomPeppaState.NONE;
                        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-106"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate { AudioController.PlaySound("hp-107"); }).OnComplete(delegate
                            {
                                _tiltState = HippoBadroomPeppaState.STATE_CHARS_TILT_RIGHT;
                                _exercises = 0;
                            });
                        });
                    }
                    break;

                case HippoBadroomPeppaState.STATE_CHARS_TILT_RIGHT:
                    if (_currentSwipe.x > 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                    {
                        _jimChar.GetComponent<Animator>().SetTrigger("Tilt_to_right");
                        _peppaChar.GetComponent<Animator>().SetTrigger("Tilt_to_right");

                        _exercises++;
                    }
                    if (_exercises == 4)
                    {
                        _tiltState = HippoBadroomPeppaState.NONE;
                        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-108"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate { AudioController.PlaySound("hp-109"); }).OnComplete(delegate
                            {
                                _tiltState = HippoBadroomPeppaState.STATE_CHARS_TILT_DAWN;
                                _exercises = 0;
                            });
                        });
                    }
                    break;

                case HippoBadroomPeppaState.STATE_CHARS_TILT_DAWN:
                    if (_currentSwipe.y < 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                    {
                        _jimChar.GetComponent<Animator>().SetTrigger("Tilt_to_dawn");
                        _peppaChar.GetComponent<Animator>().SetTrigger("Tilt_to_dawn");

                        _exercises++;
                    }
                    if (_exercises == 4)
                    {
                        _tiltState = HippoBadroomPeppaState.NONE;
                        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-110"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate { AudioController.PlaySound("hp-111"); }).OnComplete(delegate
                            {
                                _tiltState = HippoBadroomPeppaState.STATE_CHARS_TILT_UP;
                                _exercises = 0;
                            });
                        });
                    }
                    break;

                case HippoBadroomPeppaState.STATE_CHARS_TILT_UP:
                    if (_currentSwipe.y > 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                    {
                        _jimChar.GetComponent<Animator>().SetTrigger("Hippo_jump");
                        _peppaChar.GetComponent<Animator>().SetTrigger("Hippo_jump");

                        _exercises++;
                    }
                    if (_exercises == 4)
                    {
                        _tiltState = HippoBadroomPeppaState.NONE;
                        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-112"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate { AudioController.PlaySound("hp-113"); }).OnComplete(delegate
                            {
                                _tiltState = HippoBadroomPeppaState.STATE_CHARS_ETUDE;
                                _exercises = 0;
                            });
                        });
                    }
                    break;
                case HippoBadroomPeppaState.STATE_CHARS_ETUDE:
                    _jimChar.GetComponent<Animator>().SetTrigger("Make_swallow");
                    _peppaChar.GetComponent<Animator>().SetTrigger("Make_swallow");

                    _exercises++;
                    if (_exercises == 4)
                    {
                        _tiltState = HippoBadroomPeppaState.NONE;
                        DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-118"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate { AudioController.PlaySound("hp-119"); }).OnComplete(delegate
                            {
                                musicEffect.Stop();

                                DOVirtual.DelayedCall(2.5f, delegate {
                                    _mucicCenter.GetComponent<Animator>().enabled = false;
                                    _jimChar.GetComponent<Animator>().SetTrigger("Jim_run");
                                    _jimChar.transform.DOPath(_jimWayPointsOut, 2.5f).OnComplete(delegate
                                    {
                                        _peppaChar.GetComponent<Animator>().SetTrigger("Hippo_joy");
                                        DOVirtual.DelayedCall(0.7f, delegate { _time = AudioController.PlaySound("hp-120"); }).OnComplete(delegate
                                        {
                                            DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-121"); }).OnComplete(delegate
                                            {
                                                DOVirtual.DelayedCall(_time, delegate 
                                                {
                                                    _curr_state = HippoBadroomPeppaState.STATE_MOVE_CURTAIN;

                                                    if (AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.TWO_PLUS)
                                                    {
                                                        _startSwipe = false;
                                                    }
                                                    else
                                                    {
                                                        _tiltState = HippoBadroomPeppaState.STATE_MOVE_CURTAIN;
                                                    }
                                                });
                                            });
                                        });                                        
                                    });
                                });
                            });
                        });
                    }
                    break;
                case HippoBadroomPeppaState.STATE_MOVE_CURTAIN:
                    if (_colliderTouched != null)
                    {
                        if (_currentSwipe.x > 0f && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                        {
                            if (_colliderTouched.name == "curtain_r")
                            {
                                _moveCurtain = true;
                                _curtainIndex = 0;
                            }
                        }
                        else if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                        {
                            if (_colliderTouched.name == "curtain_l")
                            {
                                _moveCurtain = true;
                                _curtainIndex = 1;
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void MoveCurtain()
    {
        _curtain[_curtainIndex].transform.localScale = new Vector3(Mathf.Lerp(_curtain[_curtainIndex].transform.localScale.x, 0.2f, 0.1f), 1, 1);

        if (_curtain[_curtainIndex].transform.localScale.x < 0.25f)
        {
            _countCurtainOpened++;
            _moveCurtain = false;

            _curtain[_curtainIndex].GetComponent<Collider2D>().enabled = false;

            if (_countCurtainOpened == 2)
            {
                _time = AudioController.PlaySound("hp-122");
                DOVirtual.DelayedCall(_time, delegate
                {
                    _openedWindow.GetComponent<Collider2D>().enabled = true;
                    _curr_state = HippoBadroomPeppaState.SATATE_MOVE_OPEN_WINDOW;
                });
            }
        }
    }

    private void PeppaWakeUp()
    {
        _peppaChar.GetComponent<Animator>().SetTrigger("Peppa_wake_up");
        DOVirtual.DelayedCall(2.5f, delegate
        {
            _peppaChar.GetComponent<Animator>().SetTrigger("Peppa_idle_talk");
            StartCoroutine(WaitForTalkPeppaAnimEnd("hp-98"));

        });
    }

    private IEnumerator WaitForTalkPeppaAnimEnd(string sound)
    {
        yield return new WaitForSeconds(0.5f);
        Animator animator = _peppaChar.GetComponent<Animator>();
        AudioController.PlaySound(sound);
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("GippiTalk") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        if (_stopTalking)
        {
            _peppaChar.GetComponent<Animator>().SetTrigger("Peppa_stand_up");

            StartCoroutine(WaitForStandUpPeppaAnimEnd());

            DOVirtual.DelayedCall(1.2f, delegate { _jimChar.GetComponent<Animator>().SetTrigger("Jim_joy"); }).OnComplete(delegate 
            {
                DOVirtual.DelayedCall(1.2f, delegate { _time = AudioController.PlaySound("hp-100"); }).OnComplete(delegate 
                {
                    DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-101"); }).OnComplete(delegate
                    {
                        DOVirtual.DelayedCall(_time, delegate 
                        {
                            _mucicCenter.GetComponent<Collider2D>().enabled = true;
                            _curr_state = HippoBadroomPeppaState.STATE_PEPPA_MUSIC_ENABLED;
                        });
                    });
                });
            });            
        }
        else
        {
            _jimChar.GetComponent<Animator>().SetTrigger("Jim_idle_talk");
            StartCoroutine(WaitForTalkJimAnimEnd());
        }
    }

    private IEnumerator WaitForTalkJimAnimEnd()
    {
        yield return new WaitForSeconds(0.5f);
        Animator animator = _jimChar.GetComponent<Animator>();
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("JimIdleTalk") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        _stopTalking = true;
        _peppaChar.GetComponent<Animator>().SetTrigger("Peppa_idle_talk");
        StartCoroutine(WaitForTalkPeppaAnimEnd("hp-99"));
    }

    private IEnumerator WaitForStandUpPeppaAnimEnd()
    {
        yield return new WaitForSeconds(0.5f);
        Animator animator = _peppaChar.GetComponent<Animator>();
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("GippiStandUp") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.95f)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.2f)
            {
                _activeBlanket.GetComponent<SpriteRenderer>().sortingOrder = 0;
            }

            yield return null;
        }

        _stataticBlanket.SetActive(true);
    }

    private IEnumerator WaitForJumpAnimEnd()
    {
        Animator animator = _jimChar.GetComponent<Animator>();        
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("DgiStartShake") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
        {
            yield return null;
        }

        DOVirtual.DelayedCall(0.6f, delegate 
        {
            addToProgressJim();
            if (_workProgress.GetComponent<Image>().fillAmount >= 0.95f)
            {
                _startProgress = false;
                _jimProgress.transform.DOScale(Vector2.zero, 0.5f).OnComplete(delegate
                {
                    _jimProgress.SetActive(false);
                });

                _blanket.GetComponent<Collider2D>().enabled = false;

                _curr_state = HippoBadroomPeppaState.NONE;
                PeppaWakeUp();
            }
            else DOVirtual.DelayedCall(1.5f, delegate { _blanket.GetComponent<Collider2D>().enabled = true; });              
        });
    }
}
