﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;
public class HippoGrandsLighthouseRoomController : MonoBehaviour {
    [SerializeField]
    private GameObject _grandDad,
                       _richagDad,
                       _richagWorld,
                       _binocle,
                       _machine;

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private GameObject _draggableObject;

    private bool _isTurnedOff;

    private HippoGrandsLighthouseRoom _sceneState;

    private Vector3 _stopMachinePosition = new Vector3(1.25f, -3, 0),
                    _startPosition = new Vector3(12.23f, -3.93f, 0),
                    _endPosition = new Vector3(-6.42f, -3.37f, 0);

    protected Vector2 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    void RotateGrandpaAndStartTurning()
    {
        _grandDad.GetComponent<Animator>().SetBool("isWalking", false);
        var scale = _grandDad.transform.localScale;
        _grandDad.transform.localScale = new Vector3(-1 * scale.x, 1 * scale.y, 1 * scale.z);

        SetSceneState(HippoGrandsLighthouseRoom.STATE_TURN_OFF);
    }

    void StopAndSetTakingBinocle()
    {
        _grandDad.GetComponent<Animator>().SetBool("isWalking", false);
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-199"), delegate
        {
            AudioController.PlaySound("hp-200");
        });
        SetSceneState(HippoGrandsLighthouseRoom.STATE_GIVE_BINOCLE);
    }

    void RotateGrandpa()
    {
        var scale = _grandDad.transform.localScale;
        _grandDad.transform.localScale = new Vector3(-1 * scale.x, 1 * scale.y, 1 * scale.z);
    }

    void TurnOff()
    {
        _grandDad.GetComponent<Animator>().SetTrigger("offmachine");
    }

    private void SetSceneState(HippoGrandsLighthouseRoom sceneState)
    {
        _sceneState = sceneState;
    }

    void MoveToEndPosition()
    {
        RotateGrandpa();
        _grandDad.GetComponent<Animator>().SetBool("isWalking", true);
        _grandDad.transform.DOMove(_endPosition, 2).OnComplete(StopAndSetTakingBinocle).SetEase(Ease.Linear);
    }

    private void Awake()
    {
        _richagDad.SetActive(false);
        _grandDad.transform.position = _startPosition;
        RotateGrandpa();
        _grandDad.GetComponent<Animator>().SetBool("isWalking", true);
        _grandDad.transform.DOMove(_stopMachinePosition, 2).SetEase(Ease.Linear).OnComplete(RotateGrandpaAndStartTurning);
    }

    private void Start()
    {
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-197"), delegate
        {
            AudioController.PlaySound("hp-198");
        });
    }

    // Update is called once per frame
    void Update ()
    {
        switch (_sceneState)
        {
            case HippoGrandsLighthouseRoom.STATE_GRANDDAD_WALKS_IN:
                //Nothing xd
                break;
            case HippoGrandsLighthouseRoom.STATE_TURN_OFF:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "richagOn" && !_isTurnedOff)
                        {
                            _isTurnedOff = true;
                            _machine.GetComponent<Animator>().SetTrigger("turnoff");
                            _richagWorld.GetComponent<Animator>().enabled = true;
                            _grandDad.GetComponent<Animator>().SetTrigger("offmachine");
                            DOVirtual.DelayedCall(2, MoveToEndPosition);
                        }
                    }
                }
                break;
            case HippoGrandsLighthouseRoom.STATE_GIVE_BINOCLE:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("draggable_"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    Vector3 pos = GetMouseDeltaWorld();
                    _draggableObject.transform.position += pos;
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;


                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _grandDad.name)
                        {
                            if (_draggableObject.name.Contains("binocle"))
                            {
                                DOVirtual.DelayedCall(AudioController.PlaySound("hp-204"), delegate
                               {
                                   Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_3_MARY;
                                   PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu);
                               });
                                _grandDad.GetComponent<Animator>().SetBool("isWatching", true);
                                _draggableObject.SetActive(false);
                                _binocle.SetActive(true);
                                return;
                            }
                            else
                            {
                                AudioController.PlaySound("hp-203");
                            }
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);

                    _draggableObject = null;
                }
                break;
        }
	}
}
