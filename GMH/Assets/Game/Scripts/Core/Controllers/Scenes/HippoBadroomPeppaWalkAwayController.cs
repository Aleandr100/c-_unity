﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class HippoBadroomPeppaWalkAwayController : MonoBehaviour {

    [SerializeField]
    private GameObject _hippi;

    // Use this for initialization
    void Start()
    {
        //Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_1_LYALYA;
        DOVirtual.DelayedCall(AudioController.PlaySound("hp-129"),
            delegate
            {
                DOVirtual.DelayedCall(AudioController.PlaySound("hp-130"),
                delegate
                {
                    _hippi.GetComponent<Animator>().SetTrigger("walk");
                    _hippi.transform.DOMove(new Vector3(-4.5f, _hippi.transform.position.y, _hippi.transform.position.z), 4);
                    DOVirtual.DelayedCall(2, delegate
                    {
                        //PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoKitchenKidsEating);
                    });
                });
            });
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
