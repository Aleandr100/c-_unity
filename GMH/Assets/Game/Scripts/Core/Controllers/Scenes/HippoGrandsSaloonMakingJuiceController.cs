﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Game.Enumerators;

public class HippoGrandsSaloonMakingJuiceController : MonoBehaviour {

    [SerializeField]
    private GameObject _grandMom,
                       _splatsParent,
                       _cleanerCursor,
                       _cleanerStart,
                       _toBlenderHelper;
    [SerializeField]
    private Animator _blenderAnimator;

    private Vector2 _prevMousePos,
                    _currentMousePos;

    private int _carrotsTaken = 0,
                _splatsMade = 0;

    private bool _isCleanerTaken,
                 _cupTouched,
                 _blenderTouched;

    private HippoGrandsSaloonMakingJuiceState _sceneState;

    private GameObject _draggableObject;

    protected Vector2 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    // Use this for initialization
    void Start () {
        _sceneState = HippoGrandsSaloonMakingJuiceState.STATE_DRAG_CARROTS;
        AudioController.PlaySound("hp-161");
    }

    void TakeCarrot(GameObject turnableOffObject)
    {
        if (_carrotsTaken < 1)
        {
            turnableOffObject.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
            _carrotsTaken++;
            _blenderAnimator.SetInteger("carrotsTaken", 1);
        }
        else
        {
            turnableOffObject.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
            _blenderAnimator.SetInteger("carrotsTaken", 2);
            DOVirtual.DelayedCall(AudioController.PlaySound("hp-162"), delegate
            {
               DOVirtual.DelayedCall(AudioController.PlaySound("hp-163"), delegate
               {
                   _sceneState = HippoGrandsSaloonMakingJuiceState.STATE_TURNON_BLENDER;
               });
            });
        }
    }

    void MakeSplat()
    {
        if (_splatsMade < _splatsParent.transform.childCount)
        {
            var splat = _splatsParent.transform.GetChild(_splatsMade).GetChild(0);
            splat.transform.parent.gameObject.SetActive(true);
            splat.transform.position = _splatsParent.transform.position;
            splat.DOMove(splat.transform.parent.position, 0.3f).SetEase(Ease.InBounce);

            _splatsMade++;

            if (_splatsMade == _splatsParent.transform.childCount)
            {
                DOVirtual.DelayedCall(AudioController.PlaySound("hp-164"), delegate
                {
                    DOVirtual.DelayedCall(AudioController.PlaySound("hp-165"), delegate
                    {
                        _sceneState = HippoGrandsSaloonMakingJuiceState.STATE_CLEAN_TABLE;
                    });
                });
            }
        }
    }

    // Update is called once per frame
    void Update() {
        switch (_sceneState)
        {
            case HippoGrandsSaloonMakingJuiceState.STATE_DRAG_CARROTS:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("morkovka"))
                        {
                            _draggableObject = colliderTouched.gameObject;
                            _currentMousePos = Input.mousePosition;
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _draggableObject)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    Vector3 pos = GetMouseDeltaWorld();
                    _draggableObject.transform.position += pos;
                }
                if (Input.GetMouseButtonUp(0) && _draggableObject)
                {
                    _draggableObject.GetComponent<Collider2D>().enabled = false;

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    _draggableObject.GetComponent<Collider2D>().enabled = true;


                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "blender")
                        {
                            _draggableObject.GetComponent<Collider2D>().enabled = false;

                            _draggableObject.transform.DOMove(_toBlenderHelper.transform.position, 0.5f);
                            _draggableObject.transform.DORotate(_toBlenderHelper.transform.rotation.eulerAngles, 0.5f);

                            GameObject linkToDraggableObject = _draggableObject;
                            _draggableObject.transform.DOScale(_toBlenderHelper.transform.lossyScale, 0.5f).OnComplete(delegate { TakeCarrot(linkToDraggableObject); });

                            _draggableObject = null;
                            return;
                        }
                    }
                    _draggableObject.transform.DOMove(_draggableObject.transform.parent.position, 0.5f);

                    _draggableObject = null;
                }
                break;
            case HippoGrandsSaloonMakingJuiceState.STATE_TURNON_BLENDER:

                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "blender")
                        {
                            _blenderAnimator.SetTrigger("makeJuice");

                            DOVirtual.DelayedCall(0.5f, MakeSplat);
                            DOVirtual.DelayedCall(1f, MakeSplat);
                            DOVirtual.DelayedCall(1.7f, MakeSplat);
                            DOVirtual.DelayedCall(2.5f, MakeSplat);
                        }
                    }
                }
                break;
            case HippoGrandsSaloonMakingJuiceState.STATE_CLEAN_TABLE:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == _cleanerStart.name)
                        {
                            _isCleanerTaken = true;
                            UpdateCleaners();
                        }
                    }
                }
                if (Input.GetMouseButton(0) && _isCleanerTaken)
                {
                    _prevMousePos = _currentMousePos;
                    _currentMousePos = Input.mousePosition;

                    var camPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    camPos.z = 0;
                    _cleanerCursor.transform.position = Vector3.Lerp(_cleanerCursor.transform.position, camPos, 0.3f);

                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name.Contains("pyatna_"))
                        {
                            var sr = colliderTouched.GetComponent<SpriteRenderer>();
                            if (true)//AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                            {
                                var color = sr.color;
                                Vector2 mouseDeltaWorld = GetMouseDeltaWorld();
                                color.a -= Mathf.Sqrt(mouseDeltaWorld.x * mouseDeltaWorld.x + mouseDeltaWorld.y * mouseDeltaWorld.y) * 1f;
                                sr.color = color;

                                if (color.a <= 0.5)
                                {
                                    sr.GetComponent<Collider2D>().enabled = false;
                                    _splatsMade--;
                                    sr.DOFade(0, 0.5f);
                                    sr.GetComponentInChildren<ParticleSystem>().Play();
                                    AudioController.PlaySound("hp-16" + Random.Range(6, 9));
                                }
                            }
                            else
                            {
                                sr.GetComponent<Collider2D>().enabled = false;
                                _splatsMade--;
                                sr.DOFade(0, 0.5f);
                                sr.GetComponentInChildren<ParticleSystem>().Play();
                                AudioController.PlaySound("hp-16" + Random.Range(6, 9));
                            }

                            if (_splatsMade <= 0)
                            {
                                DOVirtual.DelayedCall(AudioController.PlaySound("hp-169"), delegate
                                {
                                    DOVirtual.DelayedCall(AudioController.PlaySound("hp-170"), delegate
                                    {
                                        DOVirtual.DelayedCall(AudioController.PlaySound("hp-171"), delegate
                                        {
                                            _isCleanerTaken = false;
                                            _cleanerCursor.transform.DOMove(_cleanerStart.transform.position, 0.5f).OnComplete(UpdateCleaners);
                                            _sceneState = HippoGrandsSaloonMakingJuiceState.STATE_FILL_CUP;
                                        });
                                    });
                                });
                            }
                        }
                    }
                }
                else
                {
                    _cleanerCursor.transform.position = Vector3.Lerp(_cleanerCursor.transform.position, _cleanerStart.transform.position, 0.3f);
                }
                if (Input.GetMouseButtonUp(0))
                {
                    _isCleanerTaken = false;
                    _cleanerCursor.transform.DOMove(_cleanerStart.transform.position, 0.5f).OnComplete(UpdateCleaners);
                }
                break;
            case HippoGrandsSaloonMakingJuiceState.STATE_FILL_CUP:
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Collider2D colliderTouched = Physics2D.OverlapPoint(touchToWorld);

                    if (colliderTouched)
                    {
                        if (colliderTouched.name == "blender")
                        {
                            _blenderTouched = true;
                        }
                        if (colliderTouched.name == "krugka")
                        {
                            _cupTouched = true;
                        }

                        if (_cupTouched && _blenderTouched)
                        {
                            Game.MainMenuSceneController.gameState = Game.Data.GameState.STATE_LOCATION_2_GRANDDAD;
                            DOVirtual.DelayedCall(AudioController.PlaySound("hp-172"), delegate { PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu); });
                            _grandMom.GetComponent<Animator>().SetTrigger("haha");
                            DOVirtual.DelayedCall(5, WalkOutTheRoom);
                            _blenderAnimator.SetTrigger("fillCup");
                        }
                    }
                }
                break;
        }
    }

    void WalkOutTheRoom()
    {
        var scale = _grandMom.transform.localScale;
        scale.x *= -1;
        _grandMom.transform.localScale = scale;
        _grandMom.GetComponent<Animator>().SetBool("finished", true);
        _grandMom.transform.DOMove(new Vector3(-7.35f, -2.6f, 0), 2).SetEase(Ease.Linear);
    }

    void UpdateCleaners()
    {
        _cleanerCursor.GetComponent<SpriteRenderer>().DOFade(_isCleanerTaken ? 1 : 0, 0.3f);
        _cleanerStart.GetComponent<SpriteRenderer>().DOFade(_isCleanerTaken ? 0 : 1, 0.3f);
    }
}
