﻿using AppCore;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GMH.Interfaces;
using UnityEngine.UI;
using GMH.Objects;
using GMH.Base;
using Game.Enumerators;
using DG.Tweening;

namespace GMH.Controllers
{
    public class HippoBadroomDadController : ABaseLevel
    {
        private HippoBadroomDadState _levelState;

        private GameObject _digitalWatch,

                           _cuckooMove,
                           _cuckooDamage,
                           _hippoDad,
                           _oldClock,
                           _blanketColl,

                           _clockShadow,
                           _closeBlanket,

                           _bodyColl,
                           _curr_slipper,
                           _movement_item,
                           _slippers_pool,
                           _legsColl,
        //****************last_changes****************
                           _dadProgress,
                           _workProgress;
        //****************last_changes****************

        private const int COUNT_OF_CLOCKS = 3;

        private Transform _slippersTransf;

        private bool _enabledControll = false,
                     _beatSlippers    = true,

                     _moveToTarget    = false,
                     _targetReached   = false,
                     _oldClockTouched = false,
                     _allClocksOn     = false,

                     _firstTouchedClocks = false,
                     _clockGotDamage     = false,
                     _cuckooIsOn         = true,
                     _checkTouched       = false,

                     _decreaseNormalizedTime = false,
                     _upIsAvailable          = false,
                     _endOfWorkOut           = false,

                     _is_active_controll = false,
                     _is_dressed         = false,
                     _firstTouch         = true,
        //****************last_changes****************
                     _startProgress = false,
                     _atOnce        = false,
                     _callAtOnce    = false;
        //****************last_changes****************

        private int _countTouchClocks = 0,
                    _weightUpCount = 0,
                    _count_drag_slippers = 0,
                    _index_pos = 0,
                    _countTouchedClock = 0,
                    _currIndexClip = 0;

        private float _targetPosX = 5.5f,
                      _posToBeat = 1.05f,
                      _speed = 0.045f,

                      _normalizedTime = 0f,
                      _decreaseValue = 0.15f,
                      _move_to_target_speed = 0.15f,
                      _dad_move_speed = 0.035f,
                      _move_back_speed = 0.35f,
                      _time;

        private Transform[] _legs_pos;

        private const string _weightUpAnim    = "weight_up",
                             _twoWeightUpAnim = "two_weight_up";

        private string _currWeightUpAnim    = _weightUpAnim,
                       _last_name_slipper   = "",
                       _currTouchedClockTag = " ";

        private string[] _currClipName;

        private Vector2[] _oldClockPos = new Vector2[]
        {
            new Vector2(6.1f,  -0.83f ),
            new Vector2(6.12f, -2.425f)
        };

        private Vector3[] _dadPositions = new Vector3[]
        {
            new Vector3(1.39f, -2.19f, 0),
            new Vector3(3.27f, -2.73f, 0)
        };

        private Vector3[] _dadOutWayPoint = new Vector3[]
        {
            new Vector3(5.51f, -3.23f, 0),
            new Vector3(9.06f, -3.23f, 0)
        };

        private Vector2 _startPos,
                        _target_pos,
                        _last_target_pos,
        //****************last_changes****************
                        _tempScaleDadProgress;
        //****************last_changes****************

        private SpriteRenderer _curr_render;

        private GameObject[] _slippers,
                             _weights;

        private List<SpriteRenderer>           _sprite_slippers;
        private Dictionary<Transform, Vector2> _start_pos_slipper;       

        private SlipperShotSystem _shotSystem;

        override public void Awake()
        {
            var loaded = Resources.Load("Game/Prefabs/HippoBadroomDad/bgBedroom") as GameObject;

            var instance = GameObject.Instantiate(loaded).transform;
            
            _cuckooDamage = instance.Find("cuckoo")                    .gameObject;
            _cuckooMove   = instance.Find("cuckoo/clockCucucshka")     .gameObject;
            _hippoDad     = instance.Find("hippo_dad")                 .gameObject;            
            _digitalWatch = instance.Find("table/digitalWatch")        .gameObject;
            _oldClock     = instance.Find("old_clock/old_clock")       .gameObject;
            _clockShadow  = instance.Find("old_clock/old_clock/shadow").gameObject;
            _closeBlanket = instance.Find("closeBlanket")              .gameObject;

            _bodyColl     = instance.Find("hippo_dad/Telo/a_0006/papaPigam").gameObject;
            _blanketColl  = _hippoDad.transform.Find("colliders")           .gameObject;

            _weights = new GameObject[2];
            _weights[0] = instance.Find("weights/weight_red")   .gameObject;
            _weights[1] = instance.Find("weights/weight_yellow").gameObject;

            _slippersTransf = instance.Find("slipperPool").transform;

            //******************************last_changes******************************
            _dadProgress  = instance.Find("Canvas/jimProgressBar").gameObject;
            _workProgress = instance.Find("Canvas/jimProgressBar/workProgress").gameObject;
            //******************************last_changes******************************

            _legs_pos = new Transform[2];
            _slippers = new GameObject[2];

            _legs_pos[0] = _hippoDad.transform.Find("Noga_1/target_pos_l");
            _legs_pos[1] = _hippoDad.transform.Find("Noga_2/target_pos_r");

            _slippers[0] = _hippoDad.transform.Find("Noga_1/tapokContaner1").gameObject;
            _slippers[1] = _hippoDad.transform.Find("Noga_2/tapokContaner2").gameObject;

            _slippers_pool = instance.Find("Slippers")                      .gameObject;
            _legsColl      = _hippoDad.transform.Find("legs_collider")      .gameObject;
        }

        override public void Start()
        {
            _levelState = HippoBadroomDadState.STATE_DAD_SHOT_SLIPPERS;
            
            _touchInputMask = LayerMask.NameToLayer("Touchable");

            _shotSystem  = new SlipperShotSystem(_slippersTransf, _blanketColl);

            //**********************last_changes**********************
            _dadProgress.SetActive(false);
            _tempScaleDadProgress = _dadProgress.transform.localScale;
            _dadProgress.transform.localScale = Vector2.zero;
            //**********************last_changes**********************

            _digitalWatch.GetComponent<Animator>().SetTrigger("Set_second");

            //******************************for_debug******************************
            ServiceLocator.GetService<IAgeController>().age = AppCore.Enumerators.Age.FIVE_PLUS;
            //******************************for_debug******************************

            _currClipName = new string[2] { "hp-50", "hp-51"};

            DropeZone.OnDragItem += TargetHasBeenReached;

            InitSlippersSprite(); InitStartPosSlippers();
            _last_target_pos = new Vector2(10.55f, -4.15f);

            foreach (Transform slipper in _slippersTransf)
            {
                slipper.GetComponent<SlipperMove>().OnBackToPoolHandler += _shotSystem.OnBackToPoolHandler;
                slipper.GetComponent<SlipperMove>().OnHitItemHandler    += OnHitItemHandler;
            }

            _time = AudioController.PlaySound("hp-37");
            DOVirtual.DelayedCall(_time, delegate { SingleThrowSlipper(); });
        }

        override public void Update()
        {
            if (_decreaseNormalizedTime)
            {
                if (_normalizedTime >= 1f)
                {
                    _weightUpCount++;                   
                    _upIsAvailable = false;
                    
                    if (_weightUpCount == 1)
                    {
                        //**************************last_changes**************************
                        _workProgress.GetComponent<Image>().fillAmount = 0f;
                        _startProgress = false;

                        if (_currWeightUpAnim == _twoWeightUpAnim)
                        {
                            _dadProgress.transform.DOScale(Vector2.zero, 0.5f).OnComplete(delegate
                            {
                                _dadProgress.SetActive(false);
                            });
                        }
                        //**************************last_changes**************************

                        _decreaseNormalizedTime = false;
                        DadDropWeight();
                    }
                    else
                    {
                        _decreaseValue = 0.45f;
                    }
                }

                _normalizedTime -= _decreaseValue * Time.deltaTime;
                if (_normalizedTime < 0)
                {
                    _normalizedTime = 0f;
                    _decreaseValue  = 0.15f;

                    _decreaseNormalizedTime = false;                   
                    _upIsAvailable    = true;
                }

                _hippoDad.GetComponent<Animator>().Play(_currWeightUpAnim, 0, _normalizedTime);
            }

            //**************************last_changes**************************
            if (_startProgress)
            {
                _workProgress.GetComponent<Image>().fillAmount -= 0.0022f;                   
            }
            //**************************last_changes**************************

            if (_moveToTarget)
            {
                if ((Vector2)_movement_item.transform.position != _target_pos)
                {
                    Extensions.MoveTo(_movement_item, _target_pos, _speed);
                }
                else
                {
                    if (ServiceLocator.GetService<IAgeController>().age != AppCore.Enumerators.Age.FIVE_PLUS)
                    {
                        TargetHasBeenReached(_movement_item.transform.GetChild(1).gameObject);
                    }

                    _moveToTarget = false;

                    if (_movement_item == _hippoDad)
                    {
                        /**/
                    }
                    else
                    {
                        _curr_slipper = null;
                    }
                }
            }
        }

        override public void ChangeLevelState(int state)
        {
            HippoBadroomDadState receivedState = (HippoBadroomDadState)state;
            switch (receivedState)
            {
                case HippoBadroomDadState.STATE_NEED_TO_MOVE_CLOCK:
                    SetExpectedItemTag("Old_clock");
                    break;
                case HippoBadroomDadState.STATE_NEED_TOUCHED_CUCKOO:
                    SetExpectedItemTag("Cuckoo");
                    break;
                case HippoBadroomDadState.STATE_NEED_TOUCHED_WATCH:
                    SetExpectedItemTag("Digital_watch");
                    break;
                case HippoBadroomDadState.STATE_NEED_TOUCH_DAD_BODY:
                    SetExpectedItemTag("Hippo_body");
                    break;
            }
            _levelState = receivedState;
        }

        private void SingleThrowSlipper()
        {
            _hippoDad.GetComponent<Animator>().SetTrigger("Shot_once");
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { HitByDigitalWatch(); }, null, Time.time, Time.time + 1.4f);
        }

        private void HitByDigitalWatch()
        {
            _digitalWatch.GetComponent<Animator>().SetTrigger("Get_damage");
            DOVirtual.DelayedCall(0.8f, delegate { _time = AudioController.PlaySound("hp-38"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { CuckooStartMove(); });
            });
        }

        private void CuckooStartMove()
        {
            _cuckooMove.GetComponent<Animator>().SetTrigger("Start_move");
            _time = AudioController.PlaySound("hp-39");
            DOVirtual.DelayedCall(_time, delegate { DadOpenEyes(); });
        }

        private void DadOpenEyes()
        {
            _hippoDad.GetComponent<Animator>().SetTrigger("Open_eyes");
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { _shotSystem.ShotSlipper(_cuckooIsOn); }, null, Time.time, Time.time + 0.85f);
        }

        private void OldClockStartRing()
        {
            _clockGotDamage = false;
            _oldClock.GetComponent<Animator>().SetTrigger("Clock_ring");
            ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { DadOpenEyes(); }, null, Time.time, Time.time + 0.85f);
        }

        private void OnHitItemHandler(Collider2D collider, GameObject slipper)
        {
            if (collider.tag == "Cuckoo" && !_clockGotDamage)
            {                
                _clockGotDamage = true;
                CuckooHit();
            }
            else if (collider.tag == "Old_clock" && !_clockGotDamage)
            {
                _clockGotDamage = true;
                OldClockHit();
            }
            else
            {
                if (collider.tag == "Hippo_dad")
                {                    
                    HippoDadHit();
                }
                else
                {
                    if (!_shotSystem.isLastShot) _blanketColl.SetActive(false);
                    slipper.GetComponent<SlipperMove>().ColliderDisable();
                }
            }
        }

        private void CuckooHit()
        {
            _cuckooIsOn   = false;            
            _cuckooMove.GetComponent  <Animator>().SetTrigger("Stop_move"  );                                    
            _cuckooDamage.GetComponent<Animator>().SetTrigger("Take_damage");

            DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-40"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-41"); }).OnComplete(delegate 
                {
                    _shotSystem.hitByBlanket = false;
                    _shotSystem.countShotSlippers = 1;
                    _shotSystem.numberOfShots = 0;

                    DOVirtual.DelayedCall(_time, delegate { OldClockStartRing(); });
                });
            });            
        }

        private void OldClockHit()
        {            
            _beatSlippers = false;
            _oldClock.GetComponent<Animator>().SetTrigger("Clock_fall");
            ChangeLevelState((int)HippoBadroomDadState.NONE);

            DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-42"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-43"); }).OnComplete(delegate 
                {
                    DOVirtual.DelayedCall(_time, delegate { EndOfShotSlippers(); });
                });
            });
        }

        private void HippoDadHit()
        {
            _shotSystem.hitByBlanket = true;
            _shotSystem.countShotSlippers = 2;

            _hippoDad.GetComponent<Animator>().SetTrigger("Hippo_hit");
            DOVirtual.DelayedCall(1.5f, delegate { _shotSystem.ShotSlipper(_cuckooIsOn); });
        }

        private void EndOfShotSlippers()
        {
            _oldClock.layer = 9;
            _shotSystem.DisableActiveSlippers();
            _enabledControll = true;
            
            SetDragActivity();
            SetItemPositions(_oldClockPos[0], _oldClockPos[1]);
            ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TO_MOVE_CLOCK);
        }

        private void ResetOldClockState()
        {
            _clockShadow.SetActive(true);
            _oldClock.transform.eulerAngles = new Vector3(0, 0, -170f);
            ResetTargetPos();
        }

        private void TouchOnOldClock()
        {
            if (_touchedItem.tag == "Old_clock")
            {
                _oldClock.transform.eulerAngles = Vector3.zero;
                _oldClock.GetComponent<Animator>().enabled = false;
                _clockShadow.SetActive(false);
            }            
        }

        private void PutOldClockOnTable()
        {
            _isDragable = false;

            ChangeLevelState((int)HippoBadroomDadState.NONE);

            _oldClock.GetComponent<Animator>().enabled = true;
            _oldClock.GetComponent<Animator>().SetTrigger("Clock_idle");

            DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-44"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCHED_CLOCK); });
            });            
        }
        
        private void OldClockTouched()
        {
            ChangeLevelState((int)HippoBadroomDadState.NONE);

            if (!_checkTouched)
            {
                _cuckooDamage.layer = 9;
                _digitalWatch.layer = 9;

                _hippoDad.GetComponent<Animator>().SetTrigger("Fidgeting" );
                _oldClock.GetComponent<Animator>().SetTrigger("Clock_ring");                                
            }

            DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-45"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-46"); }).OnComplete(delegate 
                {
                    DOVirtual.DelayedCall(_time, delegate
                    {
                        ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK);
                        SetExpectedItemTag("Cuckoo");
                    });
                });
            });            
        }
        
        private void CuckooTouched()
        {
            if (!_checkTouched)
                _cuckooMove.GetComponent<Animator>().SetTrigger("Start_move");                                
            else
            {
                _cuckooMove.GetComponent<Animator>().SetTrigger("Stop_move");
                _cuckooDamage.GetComponent<Collider2D>().enabled = false;
            }                            
        }
       
        private void WatchTouched()
        {
            if (!_checkTouched)
            {
                _digitalWatch.GetComponent<Animator>().SetTrigger("Watch_on"  );
                ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCHED_CLOCK);
                SetExpectedItemTag("Old_clock");

                _checkTouched = true;                
            }
            else
            {
                _hippoDad.GetComponent    <Animator>().SetTrigger("Wake_up");
                _digitalWatch.GetComponent<Animator>().enabled = false;
                _closeBlanket.SetActive(true);
                ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(delegate { MoveDadToCenterBedroom(); }, null, Time.time, Time.time + 1.7f);
            }
        }

        private void MoveDadToCenterBedroom()
        {
            _hippoDad.GetComponent<Animator>().SetTrigger("Hippo_walk");
            Extensions.MoveCharacterByPoolOfDotsForward((Vector3[])_dadPositions, _hippoDad, 0, 1.2f, delegate { HippoDadReachedCenter(); });
        }

        private void HippoDadReachedCenter()
        {
            _weights[0].SetActive(false);
            _hippoDad.GetComponent<Animator>().SetTrigger("Take_weight");

            //**********************************last_changes**********************************
            _time = AudioController.PlaySound("hp-54");
            DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-55"); }).OnComplete(delegate
            {
                DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-56"); }).OnComplete(delegate
                {
                    DOVirtual.DelayedCall(_time, delegate { WeightUpByTouch(); });
                });
            });
            //**********************************last_changes**********************************
        }

        private void WeightUpByTouch()
        {
            _dadProgress.SetActive(true);
            _dadProgress.transform.DOScale(_tempScaleDadProgress, 0.5f);

            ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_DAD_BODY);

            //***************last_changes***************
            //_startProgress = true;
            //***************last_changes***************

            _bodyColl.GetComponent<Collider2D>().enabled = true;
            _hippoDad.GetComponent<Animator>().speed = 0;
            _upIsAvailable = true;
        }

        private void BodyTochedNow()
        {
            if (_upIsAvailable)
            {
                _decreaseNormalizedTime = true;

                //***************last_changes***************
                _startProgress = true;
                //***************last_changes***************

                _hippoDad.GetComponent<Animator>().Play(_currWeightUpAnim, 0, _normalizedTime);

                //**********************************last_changes**********************************
                _workProgress.GetComponent<Image>().fillAmount += 0.047f;
                //**********************************last_changes**********************************

                _normalizedTime += 0.05f;
            }
        }
        
        private void DadDropWeight()
        {
            _hippoDad.GetComponent<Animator>().speed = 1;
            _hippoDad.GetComponent<Animator>().SetTrigger("Drop_weight");

            if (!_endOfWorkOut)
            {
                DOVirtual.DelayedCall(0.5f, delegate { _time = AudioController.PlaySound("hp-62"); }).OnComplete(delegate
                {
                    DOVirtual.DelayedCall(_time, delegate { _hippoDad.GetComponent<Animator>().SetTrigger("Take_two_weights"); _weights[1].SetActive(false); });
                    DOVirtual.DelayedCall(_time, delegate { TakeAlotOfWeights(); });


                    //DOVirtual.DelayedCall(5f, delegate { _time = AudioController.PlaySound("hp-63"); }).OnComplete(delegate
                    //{
                    //    DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-64"); }).OnComplete(delegate
                    //    {
                    //        DOVirtual.DelayedCall(_time, delegate { TakeAlotOfWeights(); });
                    //    });
                    //});
                });
            }
            else
            {
                DOVirtual.DelayedCall(6.5f, delegate { _time = AudioController.PlaySound("hp-65"); }).OnComplete(delegate 
                {
                    DOVirtual.DelayedCall(_time, delegate { DadDressing(); });
                });                
            }
        }

        private void TakeAlotOfWeights()
        {
            _time = AudioController.PlaySound("hp-63");
            DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-64"); }).OnComplete(delegate 
            {
                DOVirtual.DelayedCall(_time, delegate
                {                  
                    _normalizedTime = 0f;
                    _currWeightUpAnim = _twoWeightUpAnim;
                    _hippoDad.GetComponent<Animator>().speed = 0;
                    _weightUpCount = 0;
                    _endOfWorkOut = true;
                    _upIsAvailable = true;
                });
            });            
        }

        private void DadDressing()
        {
            ChangeLevelState((int)HippoBadroomDadState.STATE_DRESSING_DAD_SLIPPER);

            _bodyColl.GetComponent<Collider2D>().enabled = false;

            _time = AudioController.PlaySound("hp-66");

            DOVirtual.DelayedCall(_time, delegate
            {
                _hippoDad.GetComponent<Animator>().SetTrigger("Look_at_feet");
                DOVirtual.DelayedCall(1.5f, delegate
                {
                    _time = AudioController.PlaySound("hp-67");
                    DOVirtual.DelayedCall(_time, delegate 
                    {
                        _legsColl.SetActive(true);
                        _is_active_controll = true;
                    });                    
                });
            });                       
        }

        private void InitSlippersSprite()
        {
            _sprite_slippers = new List<SpriteRenderer>();
            foreach (Transform slpr in _slippers_pool.transform)
            {
                _sprite_slippers.Add(slpr.GetChild(0).GetComponent<SpriteRenderer>());
            }
        }

        private void InitStartPosSlippers()
        {
            _start_pos_slipper = new Dictionary<Transform, Vector2>();
            foreach (Transform slpr in _slippers_pool.transform)
            {
                _start_pos_slipper.Add(slpr, slpr.position);
            }
        }

        private SpriteRenderer FindSprite(GameObject go)
        {
            SpriteRenderer spr_render = null;
            foreach (SpriteRenderer renderer in _sprite_slippers)
            {
                if (renderer.transform.parent.gameObject == go)
                {
                    spr_render = renderer;
                    return spr_render;
                }
            }

            return spr_render;
        }

        private void SetSprite(int index)
        {
            _slippers[index].GetComponent<SpriteRenderer>().sprite = _curr_render.sprite;
            _slippers[index].SetActive(true);
        }

        private void SomeMethod()
        {
            _target_pos    = _last_target_pos;
            _speed         = _dad_move_speed;
            _movement_item = _hippoDad;

            _moveToTarget = true;
        }

        private void TargetHasBeenReached(GameObject go)
        {
            if (!_callAtOnce)
            {
                _callAtOnce = true;
                DropeZone.OnDragItem -= TargetHasBeenReached;

                GameObject slipper = go.transform.parent.gameObject;

                _curr_render = FindSprite(slipper);

                slipper.SetActive(false);

                if (_count_drag_slippers == 0)
                {
                    _last_name_slipper = slipper.name;
                    _count_drag_slippers++;

                    SetSprite(0);

                    _time = AudioController.PlaySound("hp-14");
                    DOVirtual.DelayedCall(_time, delegate { _is_active_controll = true; });

                    _is_dressed = true;
                    _curr_slipper = null;
                }
                else
                {
                    if (slipper.name == _last_name_slipper)
                    {
                        SetSprite(1);

                        Debug.Log("Сorrect_slipper");

                        AudioController.PlaySound("hp-15");

                        _legsColl.SetActive(false);

                        _hippoDad.GetComponent<Animator>().SetTrigger("Hippo_walk_out");
                        _hippoDad.transform.DOPath(_dadOutWayPoint, 5.5f).OnComplete(delegate {

                            //***************************Load_next_scene_here***************************
                            PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.HippoBathroomDad);
                            //***************************Load_next_scene_here***************************
                        });

                        _is_active_controll = false;
                        _is_dressed = true;

                        _curr_slipper = null;
                    }
                    else
                    {
                        slipper.gameObject.SetActive(true);

                        AudioController.PlaySound("hp-11");
                    }
                }
            }


            
        }

        override public void OnTouchDown()
        {
            switch (_levelState)
            {
                case HippoBadroomDadState.STATE_DAD_SHOT_SLIPPERS:
                    TakeItem();
                    break;
                case HippoBadroomDadState.STATE_NEED_TO_MOVE_CLOCK:
                    TakeItem(TouchOnOldClock);
                    break;
                case HippoBadroomDadState.STATE_NEED_TOUCHED_CLOCK:
                    TakeItem(OldClockTouched);
                    break;                
                case HippoBadroomDadState.STATE_NEED_TOUCH_DAD_BODY:
                    TakeItem(BodyTochedNow);
                    break;
            }

            if (_levelState == HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK)
            {
                GameObject go = GetClickedObject();

                if (go != null)
                {
                    if (_firstTouch)
                    {
                        if (go.tag == "Cuckoo")
                        {
                            _cuckooMove.GetComponent<Animator>().SetTrigger("Start_move");
                            _countTouchedClock++;

                            if (_currTouchedClockTag == " ")
                            {
                                _currTouchedClockTag = "Cuckoo";
                                ChangeLevelState((int)HippoBadroomDadState.NONE);
                                DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-47"); }).OnComplete(delegate 
                                {
                                    DOVirtual.DelayedCall(_time, delegate
                                    {
                                        ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK);
                                    });
                                });
                            }
                        }
                        else if (go.tag == "Digital_watch")
                        {
                            _digitalWatch.GetComponent<Animator>().SetTrigger("Watch_on");
                            _countTouchedClock++;

                            if (_currTouchedClockTag == " ")
                            {
                                _currTouchedClockTag = "Digital_watch";
                                ChangeLevelState((int)HippoBadroomDadState.NONE);
                                DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-48"); }).OnComplete(delegate
                                {
                                    DOVirtual.DelayedCall(_time, delegate
                                    {
                                        ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK);
                                    });
                                });
                            }
                        }

                        if (_countTouchedClock == 2)
                        {
                            _hippoDad.GetComponent<Animator>().SetTrigger("Close_ears");
                            ChangeLevelState((int)HippoBadroomDadState.NONE);
                            DOVirtual.DelayedCall(0.5f, delegate { _time = AudioController.PlaySound("hp-49"); }).OnComplete(delegate 
                            {
                                DOVirtual.DelayedCall(_time, delegate
                                {
                                    ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK);
                                });
                            });

                            _countTouchedClock = 0;
                            _firstTouch = false;                            
                        }
                    }
                    else
                    {                                            
                        ChangeLevelState((int)HippoBadroomDadState.NONE);
                        if (go.tag == "Cuckoo")
                        {
                            _cuckooMove.GetComponent<Animator>().SetTrigger("Stop_move");
                            _cuckooDamage.GetComponent<Collider2D>().enabled = false;
                            _countTouchedClock++; _currIndexClip++;

                            if (_currIndexClip < 3)
                            {
                                _time = AudioController.PlaySound(_currClipName[_currIndexClip-1]);
                                DOVirtual.DelayedCall(_time, delegate { ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK); });
                            }
                        }
                        else if (go.tag == "Digital_watch")
                        {
                            _digitalWatch.GetComponent<Animator>().enabled = false;
                            _countTouchedClock++; _currIndexClip++;

                            if (_currIndexClip < 3)
                            {
                                _time = AudioController.PlaySound(_currClipName[_currIndexClip-1]);
                                DOVirtual.DelayedCall(_time, delegate { ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK); });
                            }
                        }
                        else if (go.tag == "Old_clock")
                        {
                            _oldClock.GetComponent<Animator>().SetTrigger("Stop_ring");
                            _countTouchedClock++; _currIndexClip++;
                            if (_currIndexClip < 3)
                            {
                                _time = AudioController.PlaySound(_currClipName[_currIndexClip-1]);
                                DOVirtual.DelayedCall(_time, delegate { ChangeLevelState((int)HippoBadroomDadState.STATE_NEED_TOUCH_TO_CLOCK); });
                            }
                        }

                        if (_countTouchedClock == 3)
                        {
                            _closeBlanket.SetActive(true);
                            _hippoDad.GetComponent<Animator>().SetTrigger("Wake_up");
                            DOVirtual.DelayedCall(0.3f, delegate { _time = AudioController.PlaySound("hp-52"); }).OnComplete(delegate
                            {
                                DOVirtual.DelayedCall(_time, delegate { _time = AudioController.PlaySound("hp-53"); }).OnComplete(delegate
                                {
                                    DOVirtual.DelayedCall(_time, delegate { MoveDadToCenterBedroom(); });
                                });
                            });
                        }
                    }                    
                }
            }

            if (_levelState == HippoBadroomDadState.STATE_DRESSING_DAD_SLIPPER)
            {
                if (_is_active_controll)
                {
                    GameObject go = GetClickedObject();

                    if (go != null)
                    {
                        DropeZone.OnDragItem += TargetHasBeenReached;
                        //**********************last_changes**********************
                        _callAtOnce = false;
                        //**********************last_changes**********************
                        _curr_slipper = go.transform.parent.gameObject;

                        if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                        {
                            _startPos = _start_pos_slipper[_curr_slipper.transform];

                            var mouse_pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                            _offset = _curr_slipper.transform.position - Camera.main.ScreenToWorldPoint(mouse_pos);
                        }
                        else
                        {
                            if (_count_drag_slippers != 0)
                            {
                                if (_curr_slipper.name == _last_name_slipper)
                                {
                                    _moveToTarget = true;

                                    _movement_item = _curr_slipper;
                                    _target_pos = _legs_pos[_index_pos].position;
                                }
                                else
                                {
                                    Debug.Log("Wrong_slipper");

                                    AudioController.PlaySound("hp-11");
                                }
                            }
                            else
                            {
                                _moveToTarget = true;
                                _is_active_controll = false;

                                _movement_item = _curr_slipper;
                                _speed         = _move_to_target_speed;
                                _target_pos    = _legs_pos[_index_pos].position;

                                _index_pos++;
                            }
                        }
                    }
                }
            }
        }

        override public void OnTouch()
        {
            DragItem();

            if (_is_active_controll && _curr_slipper)
            {
                if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                {
                    var cur_screen_space = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                    var cur_pos = (Vector2)Camera.main.ScreenToWorldPoint(cur_screen_space) + _offset;

                    _curr_slipper.transform.position = cur_pos;
                }
            }
        }

        override public void OnTouchUp()
        {
            switch (_levelState)
            {
                case HippoBadroomDadState.STATE_DAD_SHOT_SLIPPERS:
                    ReleaseItem(delegate { _shotSystem.BeatSlipper(_touchedItem); });
                    break;
                case HippoBadroomDadState.STATE_NEED_TO_MOVE_CLOCK:
                    if (!_isDragable) ReleaseItem(delegate { MoveSelectedItem(PutOldClockOnTable); });
                    else ReleaseItem(PutOldClockOnTable, ResetOldClockState);
                    break;                
            }

            if (_levelState == HippoBadroomDadState.STATE_DRESSING_DAD_SLIPPER)
            {
                if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
                {
                    if (!_is_dressed && _curr_slipper)
                    {
                        _moveToTarget = true;

                        _speed         = _move_back_speed;
                        _movement_item = _curr_slipper;
                        _target_pos    = _startPos;
                    }
                    else
                    {
                        _is_dressed = false;
                    }
                }
            }
        }
    }
}
