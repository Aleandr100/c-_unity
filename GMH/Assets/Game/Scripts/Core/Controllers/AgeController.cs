﻿using UnityEngine;
using System;

namespace AppCore
{
    public class AgeController : IAgeController
    {

        private Enumerators.Age _age;

        public Enumerators.Age age
        {
            get { return _age; }
            set
            {
                if (Enum.GetNames(typeof(Enumerators.Age)).Length > (int)value)
                    _age = value;
                else
                {
                    Debug.LogError("Age not valid. (Out of enum range exception)");
                }
            }
        }

        public void Init()
        {
            _age = Enumerators.Age.FIVE_PLUS; // default age
        }

        public void Update()
        {
           
        }
    }

}