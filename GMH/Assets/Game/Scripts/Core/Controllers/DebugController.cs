﻿using UnityEngine;

namespace AppCore
{
    public class DebugController : IDebugController
    {
        public void Error(string msg)
        {
            Debug.LogError(msg);
        }

        public void Init()
        {
        }

        public void Log(string msg)
        {
#if UNITY_EDITOR
            Debug.Log(msg);
#endif
        }

        public void Pause()
        {
#if UNITY_EDITOR
            Debug.Break();
#endif
        }

        public void Warning(string msg)
        {
#if UNITY_EDITOR
            Debug.LogWarning(msg);
#endif
        }

        void IService.Update()
        {

        }
    }
}
