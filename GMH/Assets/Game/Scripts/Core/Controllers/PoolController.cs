﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AppCore
{
    public class PoolController
    {
        private Transform pool_transf;

        private List<GameObject> _slprs_pool;
        private List<GameObject> _slprs_active;

        public PoolController(Transform transf_place)
        {
            pool_transf = transf_place;

            _slprs_active = new List<GameObject>();

            _slprs_pool = new List<GameObject>();
            foreach (Transform slipper in pool_transf)
            {
                _slprs_pool.Add(slipper.gameObject);
            }
        }

        public GameObject GetFishFromPool()
        {
            int rand = Random.Range(0, _slprs_pool.Count);

            if (!_slprs_pool.Count.Equals(0))
            {
                GameObject slipper = _slprs_pool.ElementAt(rand);

                _slprs_pool.Remove(slipper);
                _slprs_active.Add(slipper);

                _slprs_active[_slprs_active.Count - 1].SetActive(true);

                return _slprs_active[_slprs_active.Count - 1];
            }
            return null;
        }

        public void BackSlipperToPool(GameObject slipper)
        {
            _slprs_active.Remove(slipper);
            _slprs_pool.Add(slipper);

            slipper.transform.position = Vector2.zero;

            slipper.SetActive(false);
        }

        public void BackToPoolActiveItems()
        {
            foreach (Transform slipper in pool_transf)
            {
                if (slipper.gameObject.activeSelf)
                {
                    BackSlipperToPool(slipper.gameObject);
                }
            }
        }
    }
}
