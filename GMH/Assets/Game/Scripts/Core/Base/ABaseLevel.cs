﻿using AppCore;
using UnityEngine;
using System.Collections;
using System;

namespace GMH.Base
{
    public abstract class ABaseLevel : ILevel
    {
        protected GameObject _touchedItem;

        private float _itemFlySpeed = 2.5f;

        protected Vector2 _offset,
                         _itemStartPos,
                         _itemOldPos,
                         _taragetItemPos;

        private string _expectedItemTag = "";

        protected bool _isDragable;

        protected LayerMask _touchInputMask;

        protected void SetItemPositions(Vector2 targetPos, Vector2 oldPos)
        {
            _itemStartPos   = targetPos;
            _taragetItemPos = targetPos;
            _itemOldPos     = oldPos;
        }

        protected void ResetTargetPos()
        {
            _taragetItemPos = _itemStartPos;
        }

        protected void SetExpectedItemTag(string expectedTag)
        {
            _expectedItemTag = expectedTag;
        }

        protected void SetDragActivity()
        {
            if (ServiceLocator.GetService<IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
            {
                _isDragable = true;                
            }
        }

        private void CheckReachedTraget(Action OnSucceededAction = null, Action OnFailedAction = null)
        {
            if (Vector2.Distance(_touchedItem.transform.position, _taragetItemPos) < 0.25f)
            {
                _touchedItem.transform.position = _taragetItemPos;

                if (OnSucceededAction != null) OnSucceededAction();
            }
            else
            {
                _taragetItemPos = _itemOldPos;

                MoveSelectedItem(OnFailedAction);
            }
        }

        protected GameObject GetClickedObject()
        {
            GameObject target = null;

            Vector2 end_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hit = Physics2D.Raycast(end_pos, Vector2.zero, 20, 1 << _touchInputMask);

            if (hit.collider != null)
            {
                target = hit.collider.gameObject;
            }

            return target;
        }
       
        protected void SetOffset()
        {
            var mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            _offset = _touchedItem.transform.position - Camera.main.ScreenToWorldPoint(mousePos);
        }

        protected void DragItem()
        {
            if (_touchedItem && _isDragable)
            {
                var cur_screen_space = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                var cur_pos = (Vector2)Camera.main.ScreenToWorldPoint(cur_screen_space) + _offset;

                _touchedItem.transform.position = cur_pos;
            }
        }

        protected void TakeItem(System.Action OnSucceededAction = null)
        {
            GameObject touchedItem = GetClickedObject();

            if (touchedItem != null)
            {
                _touchedItem = touchedItem;

                if (_touchedItem.tag.Contains(_expectedItemTag))
                {
                    if (_isDragable) SetOffset();
                    if (OnSucceededAction != null) OnSucceededAction();
                }
            }
        }

        protected void ReleaseItem(Action OnSucceededAction = null, Action OnFailedAction = null)
        {
            if (_touchedItem)
            {
                if (_isDragable)
                    CheckReachedTraget(OnSucceededAction, OnFailedAction);
                else if (OnSucceededAction != null) OnSucceededAction();
            }

            _touchedItem = null;
        }

        protected void MoveSelectedItem(Action OnReachedTarget = null)
        {
            Extensions.MoveObjectToPointByPosition(_taragetItemPos, _touchedItem, _itemFlySpeed, delegate { OnReachedTarget(); });
        }

        #region abstract
        public abstract void ChangeLevelState(int state);
        public abstract void Awake();
        public abstract void Start();
        public abstract void Update();

        public abstract void OnTouchDown();
        public abstract void OnTouch();
        public abstract void OnTouchUp();
        #endregion
    }
}
