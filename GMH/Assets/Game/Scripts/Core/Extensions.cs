﻿using UnityEngine;
using System.Collections;
using System;

namespace AppCore
{
    public struct Range
    {
        public float min,
            max;

        public Range(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }

    public static class Extensions
    {
        public static int GetEnumSize<EnumType>()
        {
            return System.Enum.GetNames(typeof(EnumType)).Length;
        }

        public static T GetRandomAndRemove<T>(this System.Collections.Generic.IList<T> list)
        {
            int index = UnityEngine.Random.Range(0, list.Count);
            T returnable = list[index];
            list.RemoveAt(index);
            return returnable;
        }

        public static int GetEnumSize(this System.Enum enumValue)
        {
            return System.Enum.GetNames(enumValue.GetType()).Length;
        }

        public static void MoveObjectToPointByPosition ( UnityEngine.Vector3 position,
            UnityEngine.GameObject gameObject,
            float objectFlySpeed,
            TimeController.MonoMethodHandler
            timerHandler = null)
        {
            object[] args = new object[] { position, gameObject, objectFlySpeed, timerHandler };
            ServiceLocator.GetService<ITimeController>().AddEndlessTimer( MoveObjectToPointByPosition, args, UnityEngine.Time.time );
        }

        public static void MoveObjectToPointByTransformWithRotation(UnityEngine.Transform transf,
            UnityEngine.GameObject gameObject,
            float objectFlySpeed,
            float rotationSpeed,
            TimeController.MonoMethodHandler
            timerHandler = null)
        {
            object[] args = new object[] { transf, gameObject, objectFlySpeed, rotationSpeed, timerHandler };
            ServiceLocator.GetService<ITimeController>().AddEndlessTimer(MoveObjectToPointByTransformWithRotation, args, UnityEngine.Time.time);
        }

        static private void MoveObjectToPointByPosition(object[] args)
        {
            UnityEngine.Vector3 position = (UnityEngine.Vector3)args[0];
            UnityEngine.GameObject go = (UnityEngine.GameObject)args[1];
            float objectFlySpeed = (float)args[2];
            TimeController.MonoMethodHandler timerHandler = (TimeController.MonoMethodHandler)args[3];


            if (UnityEngine.Vector3.SqrMagnitude(go.transform.position - position) < 0.02f)
            {
                go.transform.position = position;
            }
            else
            {
                go.transform.position = UnityEngine.Vector3.Lerp(go.transform.position, position, objectFlySpeed * UnityEngine.Time.deltaTime);
            }
            if (go.transform.position.Equals(position))
            {
                if (timerHandler != null)
                {
                    timerHandler();
                }
                ServiceLocator.GetService<ITimeController>().RemoveTimer(MoveObjectToPointByPosition);
            }
        }

        static private void MoveObjectToPointByTransformWithRotation(object[] args)
        {
            UnityEngine.Transform transf = (UnityEngine.Transform)args[0];
            UnityEngine.GameObject go = (UnityEngine.GameObject)args[1];
            float objectFlySpeed = (float)args[2];
            float rotationSpeed = (float)args[3];
            TimeController.MonoMethodHandler timerHandler = (TimeController.MonoMethodHandler)args[4];


            if (UnityEngine.Vector3.SqrMagnitude(go.transform.position - transf.position) < 0.01f && UnityEngine.Mathf.Abs(go.transform.rotation.z - transf.rotation.z) < 1f)
            {
                go.transform.position = transf.position;
                go.transform.rotation = transf.rotation;
            }
            else
            {
                go.transform.position = UnityEngine.Vector3.Lerp(go.transform.position, transf.position, objectFlySpeed * UnityEngine.Time.deltaTime);
                go.transform.rotation = UnityEngine.Quaternion.RotateTowards(go.transform.rotation, transf.rotation, rotationSpeed * UnityEngine.Time.deltaTime);
            }
            if (go.transform.position.Equals(transf.position) && go.transform.rotation.Equals(transf.rotation))
            {
                if (timerHandler != null)
                {
                    timerHandler();
                }
                ServiceLocator.GetService<ITimeController>().RemoveTimer(MoveObjectToPointByTransformWithRotation);
            }
        }
        

        static public void SetCharacterAnimation(UnityEngine.GameObject character, Game.Enumerators.CharacterAnimation characterAnimation)
        {
            character.GetComponent<UnityEngine.Animator>().Play(characterAnimation.ToString(), 0, 0);
        }

        static public void MoveCharacterByPoolOfDotsForward(UnityEngine.Vector3[] positions,
                                                            UnityEngine.GameObject go,
                                                            int iteration,
                                                            float speed,
                                                            TimeController.MonoMethodHandler handler
                                                            )
        {
            object[] args = new object[] { positions, go, iteration, speed, handler };

            ServiceLocator.GetService<ITimeController>().AddEndlessTimer(MoveCharacterByPoolOfDotsForward, args, UnityEngine.Time.time);
        }

        static private void MoveCharacterByPoolOfDotsForward(object[] args)
        {
            UnityEngine.Vector3[] positions = (UnityEngine.Vector3[])args[0];
            UnityEngine.GameObject go = (UnityEngine.GameObject)args[1];
            int iteration = (int)args[2];
            float speed = (float)args[3];
            TimeController.MonoMethodHandler handler = (TimeController.MonoMethodHandler)args[4];

            if (go.transform.position.Equals(positions[iteration]))
            {
                ServiceLocator.GetService<ITimeController>().RemoveTimer(MoveCharacterByPoolOfDotsForward);
                if (iteration < positions.Length - 1)
                {
                    iteration++;

                    args[0] = positions;
                    args[1] = go;
                    args[2] = iteration;

                    ServiceLocator.GetService<ITimeController>().AddEndlessTimer(MoveCharacterByPoolOfDotsForward, args, UnityEngine.Time.time);
                }
                else
                {
                    if (handler != null) handler();
                }
            }
            else
            {
                go.transform.position = UnityEngine.Vector3.MoveTowards(go.transform.position, positions[iteration], speed * UnityEngine.Time.deltaTime);
            }
        }

        static public void MoveTo(GameObject go, Vector2 target, float speed)
        {
            go.transform.position = Vector2.MoveTowards(go.transform.position, target, speed);
        }        
    }
}