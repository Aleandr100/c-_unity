﻿using UnityEngine;
using UnityEngine.UI;


public class ToggleButton : MonoBehaviour
{

    protected ButtonClickSound
        clicker;
    protected Toggle
        toggle;

    void Awake ()
    {
        toggle = GetComponent<Toggle> ( );
        toggle.onValueChanged.AddListener ( OnValueChanged );
        clicker = GetComponent<ButtonClickSound> ( );
    }

    virtual protected void OnValueChanged (bool val)
    {

    }

    protected void SetToggle (bool param)
    {
        EnableClicker ( false );
        toggle.isOn = param;
        EnableClicker ( true );
    }

    void EnableClicker (bool param)
    {
        if (clicker != null)
        {
            clicker.enabled = param;
        }
    }
}
