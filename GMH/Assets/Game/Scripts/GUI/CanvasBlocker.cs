﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CanvasBlocker : MonoBehaviour,IPointerClickHandler {

    public delegate void Callback (bool locked);
    public delegate void ActionCallback ();

    public static event Callback OnCanvasLocked;
    public static event ActionCallback OnClick;

    
    void OnEnable ()
    {
        if (OnCanvasLocked != null)
        {
            OnCanvasLocked ( true );
        }
    }
    
    void OnDisable ()
    {
        if (OnCanvasLocked != null)
        {
            OnCanvasLocked ( true );
        }
    }

    void Action ()
    {
        if (OnClick != null)
        {
            OnClick ( );
        }
    }

    public void OnPointerClick (PointerEventData eventData)
    {
        Action (  );
    }
}
