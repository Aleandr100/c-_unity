﻿
public class LangToggle : ToggleButton {

    public Languages.Language
        lang;

    protected override void OnValueChanged (bool param)
    {
        if (param)
        {
            SetLang ( );
        }
    }



    void SetLang ()
    {
        Languages.SetLanguage ( lang );
    }


    void Start ()
    {
            if (Languages.GetLanguage ( ) == lang)
            {
                SetToggle(true);
            }
    }
    
}
