﻿
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public class PlayerPrefsHelper : MonoBehaviour {

    [MenuItem("HelpTools/Clear all player prefs")]
    public static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}

#endif
