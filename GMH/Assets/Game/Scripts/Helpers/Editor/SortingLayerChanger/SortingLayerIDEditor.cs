using System;
using UnityEngine;
using UnityEditor;

namespace UnityToolbag
{
	[CustomEditor(typeof(SortingLayerID))]
	public class SortingLayerIDEditor : Editor
	{
		Vector3 tmp;
		int x;
		bool zOrder;
		enum orderType
		{
			ByZ,
			BySortingOrder,
		}

		public override void OnInspectorGUI ()
		{
			// Get the renderer from the target object
			Renderer[] renderers = (target as SortingLayerID).gameObject.GetComponentsInChildren<Renderer> (true);

			// If there is no renderer, we can't do anything
			if (renderers.Length == 0) {
				EditorGUILayout.HelpBox ("SortingLayerExposed must be added to a game object that has a renderer.", MessageType.Error);
				return;
			}
			bool 
			differentOrd = false,
			differentZ = false;
			int ord = renderers [0].sortingOrder;
			float z = renderers [0].transform.localPosition.z;
			foreach (Renderer item in renderers) {
				if (item.sortingOrder != ord)
					differentOrd = true;
				if (item.transform.localPosition.z != z)
					differentZ = true;
			}
			if (differentOrd && !differentZ) 
				zOrder = false;
			if (!differentOrd && differentZ) 
				zOrder = true;

			bool res = EditorGUILayout.Toggle ("Order by z", zOrder);



			if (res != zOrder) {
				zOrder = res;
				foreach (Renderer item in renderers) {
					if (zOrder) {
						tmp = item.transform.localPosition;
						tmp.z = -1 * (item.sortingOrder / 10f);
						item.transform.localPosition = tmp;
						item.sortingOrder = 0;
					} else {

						tmp = item.transform.localPosition;
						item.sortingOrder = (int)Mathf.Abs (tmp.z * 10f);
						tmp.z = 0;
						item.transform.localPosition = tmp;
					}
				} 

			}


			int 
			maxOrder = renderers [0].sortingOrder,
			minOrder = renderers [0].sortingOrder;

			foreach (Renderer item in renderers) {
				if (item.sortingOrder > maxOrder)
					maxOrder = item.sortingOrder;
				if (item.sortingOrder < minOrder)
					minOrder = item.sortingOrder;
			}



			EditorGUILayout.HelpBox ("SortingLayer " + renderers [0].sortingLayerID + 
				"\nMin sortingOrder " + minOrder +
				"\nMax sortingOrder " + maxOrder, MessageType.Info);


			var sortingLayerNames = SortingLayerHelper.sortingLayerNames;

			// If we have the sorting layers array, we can make a nice dropdown. For stability's sake, if the array is null
			// we just use our old logic. This makes sure the script works in some fashion even if Unity changes the name of
			// that internal field we reflected.
//			if (sortingLayerNames != null) {
			// Look up the layer name using the current layer ID
			string oldName = SortingLayerHelper.GetSortingLayerNameFromID (renderers [0].sortingLayerID);

			// Use the name to look up our array index into the names list
			int oldLayerIndex = Array.IndexOf (sortingLayerNames, oldName);

			// Show the popup for the names
			int newLayerIndex = EditorGUILayout.Popup ("Sorting Layer", oldLayerIndex, sortingLayerNames);
			foreach (Renderer item in renderers) {
				Undo.RecordObject (item, "Edit Sorting Order");
				item.sortingLayerID = SortingLayerHelper.GetSortingLayerIDForIndex (newLayerIndex);
				EditorUtility.SetDirty (item);
			}



		}

	}
}
