﻿#define NEAT_PLUG
//#define GOOGLE_ADS


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using DG.Tweening;


#if GOOGLE_ADS
using GoogleMobileAds.Api;
#endif

/* ChangeLog:
 * Updated Spine package (http://ru.esotericsoftware.com/forum/Noteworthy-Spine-Unity-Topics-5924) (use Spine.Unity and Spine.Unity.Modules  (http://ru.esotericsoftware.com/forum/Spine-Unity-namespaces-6025))
 * New GoogleAdsSDK (fixed bugs)
 * Scenes are loaded by string. Its not necessary now to keep same order of scenes in enum
 * Banners wont be repositioned untill splashes will end
 * Added extra check before banner reposition to avoid its reload call (ManagerGoogle)
 * Added push scene to no-ads list
 * Added platform directives to support different platforms
 * Removed Transition kit with shaders
 * Pannel -> Panel
 * 1.1.3
 * Added mediation networks: Chartboost, UnityAds, InMobi, MobFox
 * Added RateMePlugin
 * Added Promo.v11
 * Updated ManagerGoogle
 * Updated AdmobManager
 * Added PlayServicesResolver
 * Added EditorTools for Textures and manifest
 * Modified Audio toggle, LangPannel scripts
 * Added hide promo OnSettingsVisible
 * 1.1.4
 * Fixed reposition of banner (didn't turned back to previous position): modified ManagerGoogle and AdmobManager 
 * 1.1.5
 * Moved Reposition small banner to event OnInterClosed
 * Moved SceneLoader to namespace PSV_Prototype
 * 1.1.6
 * Fixed not calling OnInterClosed when Interstitial is not allowed to show
 * 1.1.7
 * Added support of FirebaseAnalytics
 * 1.1.8
 * Fixed Firebase LogEvent(SelectContent)
 */

//Version 1.1.7 (21.11.2016)

namespace PSV_Prototype
{


    public class SceneLoader :MonoBehaviour
    {
        public static SceneLoader Instance;



        public enum TransitionMethod
        {
            Default,    //do not select this (service item)
            Tween,      //using animathion of Canvas Image
            None,       //using siple SceneManager.LoadScene()
        }


        private Scenes
            _target_scene;


        public bool
            splash_transition = true;   //uncheck this to leave all scenes in Hierarchy and keep all necessary Instances created without switching to splashes

        private bool
            in_transition = false;      //determines if we can start another transitrion (switching between few scenes in a row not allowed)

        public List<Scenes>
            splash_scenes = new List<Scenes> ( );   //list here all scenes that should be shown at the start of the game with some short lifetime

        public Scenes
            first_scene = Scenes.MainMenu,       //scene to load after splashes (usually MainMenu)
            push_scene = Scenes.Push;        //scene where push service is set

        public float
            splash_duration = 2f,       //splash screen lifetime
            transition_duration = 1f;   //transition animation duration

        public TransitionMethod
            transition_method = TransitionMethod.Tween;             //how to switch between scenes

        public GameObject
            loading_screen,             //prefab of Canvas and text item with "Loading" label which will appare befor Interstitial will be shown
            fadeScreen;                 //link to object with Image with black texture which will fade over screen

        private UnityEngine.UI.Image
            fade_image;                 //Image component of fade_screen



        void Awake ()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad ( gameObject );
                fade_image = fadeScreen.GetComponent<UnityEngine.UI.Image> ( );

                if (transition_method == TransitionMethod.Default)  //fixing if method haven't been selected properly
                {
                    transition_method = TransitionMethod.Tween;
                }
            }
        }

        void OnEnable ()
        {
            ManagerGoogle.OnInterstitialShown += ShowLoadingScreen;
            ManagerGoogle.OnInterstitialClosed += OnInterClosed;
        }


        void OnDisable ()
        {
            ManagerGoogle.OnInterstitialShown -= ShowLoadingScreen;
            ManagerGoogle.OnInterstitialClosed -= OnInterClosed;
        }


        


        void Start ()
        {
#if !UNITY_EDITOR
        //for those, who forgot to put this trigger true before build
        splash_transition = true;
#endif
            if (splash_transition)
            {
                StartCoroutine ( SplashSequence ( ) );
            }

            AnalyticsManager.LogEvent ( AnalyticsEvents.StartApplication );
        }


        private void ShowRateUs (Scenes current_scene, Scenes target_scene)
        {
            if (target_scene != current_scene && SceneLoaderSettings.rate_me_after_end.Contains ( current_scene ))
            {
                RateMePlugin.RateMeModule.Instance.ShowDialogue ( true );
            }
        }


        #region ADS
        private void ShowBigBanner (Scenes current_scene)
        {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
            bool inter_shown = false;
            if (!SceneLoaderSettings.not_allowed_interstitial.Contains ( current_scene ))
            {
                inter_shown = ManagerGoogle.Instance.ShowFullscreenBanner ( );
            }
            if (!inter_shown)
            {
                OnInterClosed ( );
            }
#endif
        }

        private void RepositionBanner (Scenes target_scene)
        {
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
            bool show_banner = !SceneLoaderSettings.not_allowed_small_banner.Contains ( target_scene );

            if (show_banner)
            {
#if GOOGLE_ADS
            AdPosition target_pos = small_banner_default_pos;
#else
                AdmobAd.AdLayout target_pos = SceneLoaderSettings.small_banner_default_pos;
#endif
                if (SceneLoaderSettings.small_banner_override.ContainsKey ( target_scene ))
                {
                    target_pos = SceneLoaderSettings.small_banner_override [target_scene];
                }
                ManagerGoogle.Instance.RepositionSmallBanner ( target_pos );
                ManagerGoogle.Instance.ShowSmallBanner ( );
            }
            else
            {
                ManagerGoogle.Instance.HideSmallBanner ( );
            }
#endif
        }

        #endregion

        void CompleteTransition ()
        {
            in_transition = false;
        }


        public void ShowLoadingScreen ()
        {
            if (loading_screen != null)
            {
                Instantiate ( loading_screen );
            }
        }


        private void OnScreenObscured (Scenes target_scene)    //occurs when screen is frosen (hidden)
        {
            Scenes current_scene = SceneManager.GetActiveScene ( ).name.ToEnum<Scenes> ( );
            AudioController.ReleaseStreams ( );
            ShowBigBanner ( current_scene );
            ShowRateUs ( current_scene, target_scene );
            //RepositionBanner ( target_scene ); //moved to event OnInterClosed()
        }

        void OnInterClosed ()               //occurs when user closes interstitial
        {
            //Debug.Log ( "OnInterCLosed" );
            RepositionBanner ( _target_scene );
        }

        private void LoadScene (Scenes scene_to_load)   //generic way of loading scene without animation
        {
            string target = scene_to_load.ToString ( );
            OnScreenObscured ( scene_to_load );
            SceneManager.LoadScene ( target );
            StartCoroutine ( WaitForTransitionComplete ( target ) );
        }

        void OnApplicationQuit ()
        {
            PlayerPrefs.Save ( );
            AnalyticsManager.LogEvent ( AnalyticsEvents.CloseApplication );
        }


        IEnumerator SplashSequence ()   //coroutine for showing splash scenes with certain delay
        {
            for (int i = 0; i < splash_scenes.Count; i++)
            {
                SwitchToScene ( splash_scenes [i] );
                yield return new WaitForSeconds ( splash_duration );
            }
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
            SwitchToScene ( PsvPushService.Instance.AreNewsAvailable ( ) ? push_scene : first_scene );
#else
        SwitchToScene ( first_scene );
#endif
        }



        public bool IsLoadingLevel ()
        {
            return in_transition;
        }


        public void SwitchToScene (Scenes target_scene, TransitionMethod method = TransitionMethod.Default, float override_duration = -1)
        {
            if (!IsLoadingLevel ( ))
            {
                in_transition = true;

                _target_scene = target_scene;

                if (method == TransitionMethod.Default)
                    method = transition_method;


                float duration = override_duration < 0 ? transition_duration : override_duration;

                AnalyticsManager.LogEvent ( AnalyticsEvents.LogScreen, target_scene.ToString ( ) );


                switch (method)
                {
                    case TransitionMethod.None:
                        {
                            LoadScene ( target_scene );
                            break;
                        }
                    case TransitionMethod.Tween:
                        {
                            StartTransition ( target_scene, fade_image, duration );
                            break;
                        }
                }
            }
        }


        #region TweenTransition
        public static void SceneTransition (bool open, TweenCallback callback, UnityEngine.UI.Image fade_image, float fadeTime)
        {
            if (open)
            {
                TweenCallback fadeCallback = delegate
                {
                    fade_image.gameObject.SetActive ( false );
                    if (callback != null)
                    {
                        callback ( );
                    }
                };
                fade_image.DOFade ( 0, fadeTime ).OnComplete ( fadeCallback ).SetUpdate ( true );
            }
            else
            {
                fade_image.gameObject.SetActive ( true );
                fade_image.DOFade ( 1, fadeTime ).OnComplete ( callback ).SetUpdate ( true );
            }
        }


        public void StartTransition (Scenes scene_to_load, UnityEngine.UI.Image fade_image, float transition_duration)
        {
            string target = scene_to_load.ToString ( );
            if (fade_image != null)
            {
                TweenCallback StartLayout = delegate
                {
                    SceneManager.LoadScene ( target );
                    StartCoroutine ( WaitForTransitionComplete ( target, transition_duration, fade_image ) );
                };

                SceneTransition ( false, delegate
                {
                    OnScreenObscured ( scene_to_load );
                    StartLayout ( );
                }, fade_image, transition_duration );
            }
            else
            {
                Debug.Log ( "Fade image null" );
            }
        }


        IEnumerator WaitForTransitionComplete (string level, float transition_duration, UnityEngine.UI.Image fade_image)
        {

            while (SceneManager.GetActiveScene ( ).name != level)
                yield return null;
            SceneTransition ( true, delegate
            {
                CompleteTransition ( );
            }, fade_image, transition_duration );
        }


        IEnumerator WaitForTransitionComplete (string level)
        {
            while (SceneManager.GetActiveScene ( ).name != level)
                yield return null;
            CompleteTransition ( );
        }


        #endregion
    }
}