﻿using UnityEngine;

public class Initialiser :MonoBehaviour
{

    public bool
        dont_sleep = false;





    void Awake ()
    {
#if UNITY_EDITOR
        //ClearPlayerPrefs ( );
#endif
        if (dont_sleep)
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }


        GameSettings.UpdateSettings ( );
        AudioController.InitStreams ( gameObject );
        Languages.Init ( );
        //LocalizationManager.Init ( ); //uncomment if need localisastion (unnecessary resources load will slow down process)
        DG.Tweening.DOTween.Init ( true, true, DG.Tweening.LogBehaviour.Verbose ).SetCapacity ( 200, 10 );



    }



    void ClearPlayerPrefs ()
    {
        PlayerPrefs.DeleteAll ( );
        Debug.Log ( "Warning: player prefs cleared" );
    }


}
