﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EateryKitchenManager : MonoBehaviour {

    static public EateryKitchenManager Instance { get; private set; }

    static public bool isCuttingActive
    {
        get
        {
            return OnClickBasket.basketsCount == 0;
        }
    }

    public GameObject noriContainer;

    private List<GameObject> foodParts = new List<GameObject>();

    private void Awake()
    {
        Instance = this;

        foreach (Transform transformChild in noriContainer.transform)
            foodParts.Add(transformChild.gameObject);

        foreach (Transform transformChild in noriContainer.transform)
            transformChild.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPosition.z = Camera.main.transform.position.z;

            RaycastHit2D rayCastResult = Physics2D.Raycast(mouseWorldPosition, new Vector3(0, 0, 0), 0.0f);

            if (rayCastResult)
                if (rayCastResult.transform.GetComponent<OnClickBasket>())
                {
                    rayCastResult.transform.GetComponent<OnClickBasket>().Throw();
                }
        }
    }

    public void ShowCuttedFood(string objectName)
    {
        for (int i = 0; i < foodParts.Count; i++)
        {
            if (objectName.Contains(foodParts[i].name) && !foodParts[i].activeInHierarchy)
            {
                StartCoroutine("PlayAnimFood", foodParts[i]);
                foodParts.RemoveAt(i);
                return;
            }
        }
    }

    private IEnumerator PlayAnimFood(GameObject go)
    {
        yield return new WaitForSeconds(1f);
        go.gameObject.SetActive(true);

    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("click");
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D collider2d = Physics2D.OverlapPoint(pos);
            if (collider2d)
            {
                if (collider2d.transform.GetComponent<ChangeDirectionCollider>())
                {
                   // collider2d.transform.gameObject.SetActive(false);
                }
            }
        }
    }
}
