﻿using UnityEngine;
using System.Collections;

public class GameEnums : MonoBehaviour {

	public enum CharacterAnimations
    {
        walk_footOUT,
        sad,
        idle,
        turnOffSwitcher,
    }

    public enum Age {
        TWO,
        FIVE,
    }

    public enum EateryKitchenFood
    {
        fish,
        onion,
        lettuce,
        tomato,
        pepper,
    }
}
