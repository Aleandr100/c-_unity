﻿using UnityEngine;
using System.Collections;
using GoodMorningHippo.Objects;
using GoodMorningHippo.Managers;
using System.Collections.Generic;
using System.Linq;

namespace GoodMorningHippo.Objects
{
    public class FishPoolControll : MonoBehaviour
    {
        [SerializeField]
        private GameObject _splash;

        public static FishPoolControll Instance;

        private List<FishMoveRand> _fish_pool;
        private List<FishMoveRand> fish_active;

        private Transform _holder_transform;

        private float _last_spawn;
        private float _delta_spawn;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            _delta_spawn = 2.2f;
            _holder_transform = transform;

            fish_active = new List<FishMoveRand>();

            _fish_pool = new List<FishMoveRand>();
            foreach (Transform fish in _holder_transform)
            {
                _fish_pool.Add(fish.GetComponent<FishMoveRand>());
            }
        }

        private void Update()
        {
            if (ShallowManager.Instance.StartCatching)
            {
                if (!ShallowManager.Instance.IsStopCatched)
                {
                    if (Time.time - _last_spawn > _delta_spawn)
                    {
                        float start_x_pos, x_speed, start_y_pos;

                        start_x_pos = Random.Range(-2.83f, -0.48f);
                        x_speed     = Random.Range(-1.95f,  1.95f);
                        start_y_pos = Random.Range(-2.25f, -1.25f);

                        FishMoveRand fish = GetFishFromPool();

                        _splash.transform.position = new Vector2(start_x_pos, start_y_pos);

                        _splash.GetComponent<Animator>().SetTrigger("Water_splash");

                        if (fish != null)
                        {
                            fish.LaunchFish(Random.Range(2.75f, 3.55f), x_speed, start_x_pos, start_y_pos);
                        }

                        _last_spawn = Time.time;
                    }
                }
            }                        
        }

        private FishMoveRand GetFishFromPool()
        {
            int rand = Random.Range(0, _fish_pool.Count);

            if (!_fish_pool.Count.Equals(0))
            {
                FishMoveRand fish = _fish_pool.ElementAt(rand);

                _fish_pool.Remove(fish);
                fish_active.Add(fish);
                fish_active[fish_active.Count - 1].gameObject.SetActive(true);

                return fish_active[fish_active.Count - 1];
            }
            return null;
        }

        public void BackFishToPool(FishMoveRand fish)
        {
            fish_active.Remove(fish);
            _fish_pool.Add(fish);
            fish.gameObject.SetActive(false);
        }

        public void RemoveFromPool(FishMoveRand fish)
        {
            fish_active.Remove(fish);
        }        
    }
}

