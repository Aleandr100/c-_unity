﻿using UnityEngine;
using System.Collections;
using System;

namespace GoodMorningHippo.Objects
{
    public class FishMoveRand : MonoBehaviour
    {
        private GameObject _splash;

        private const float GRAVITY = 2.0f;

        private float _vertical_velocity;
        private float _speed,
                      _startScaleX;
        private int _sign;

        private GameObject _colliders;

        private int _direction;

        public bool isActive { set; get; }
        public bool isMoved { set; get; }

        public void Awake()
        {
            _colliders = transform.GetChild(1).gameObject;
            _sign = Math.Sign(transform.localScale.x);

            _startScaleX = transform.localScale.x;

            var loaded = Resources.Load("Game/Prefabs/RaccoonMaryShallow/waterSplash") as GameObject;

            _splash = GameObject.Instantiate(loaded).gameObject;
        }

        private void Update()
        {
            Movement();
        }

        public void LaunchFish(float verticalVelocity, float xSpeed, float xStart, float yStart)
        {
            isActive = true;
            _speed = xSpeed;

            isMoved = true;

            if (xSpeed < 0)
            {
                _direction = 1;
                Flip();
            }
            else
            {
                _direction = (-1);
            }

            _vertical_velocity = verticalVelocity;
            transform.position = new Vector3(xStart, yStart, 0f);
        }

        public void ColliderOff()
        {
            CircleCollider2D[] colliders = _colliders.GetComponents<CircleCollider2D>();
            foreach (CircleCollider2D collider in colliders)
            {
                collider.enabled = false;
            }
        }

        private void Movement()
        {
            if (!isActive)
                return;

            if (isMoved)
            {
                float last_pos_y = transform.position.y;
                _vertical_velocity -= GRAVITY * Time.deltaTime;

                SetRotation();

                transform.position += new Vector3(_speed, _vertical_velocity, 0) * Time.deltaTime;

                if (transform.position.y < -2.35f)
                {
                    _splash.transform.position = new Vector2(transform.position.x, transform.position.y);
                    _splash.GetComponent<Animator>().SetTrigger("Water_splash");

                    isActive = false;
                    if (Math.Sign(transform.localScale.y) != _sign)
                    {
                        Flip();
                    }
                    
                    transform.eulerAngles = Vector3.forward * 90f;
                    transform.localScale = new Vector2(_startScaleX, transform.localScale.y);

                    FishPoolControll.Instance.BackFishToPool(this);
                }
            }
        }

        private void SetRotation()
        {
            Vector2 next_pos = new Vector2(transform.position.x + _speed, transform.position.y + _vertical_velocity);

            transform.LookAt(next_pos);
            float angle = 0f - transform.eulerAngles.x;

            if (next_pos.x < transform.position.x)
            {
                angle *= -1f;
            }

            transform.localRotation = Quaternion.Euler(0, 0, angle);
        }

        private void Flip()
        {
            Vector3 theScale = transform.localScale;

            theScale.x *= -1;
            transform.localScale = theScale;
        }        
    }
}

