﻿using AppCore;
using UnityEngine;
using System.Collections;
using GoodMorningHippo.Managers;
using GMH.Controllers;
using System;

namespace GMH.Objects
{
    public class SlipperMove : MonoBehaviour
    {        
        public delegate void BackToPoolEvent(GameObject slipper                     );
        public delegate void HitItemEvent   (Collider2D collider, GameObject slipper);                        

        public event HitItemEvent    OnHitItemHandler;
        public event BackToPoolEvent OnBackToPoolHandler;

        private Vector2 _startPos;

        private float _fadeTime,
                      _targetPosX;

        private bool _fadeSlipper;

        private SpriteRenderer _currSprite;
        
        public bool isNotActive { get; set; }

        private void Awake()
        {
            isNotActive = false;
            _fadeSlipper = true;

            _targetPosX = 5.48f;
            _fadeTime   = 0.25f;

            _startPos = new Vector2(-0.59f, 0.22f);

            transform.position = _startPos;
            _currSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();            
        }

        private void OnEnable()
        {
            Color color = _currSprite.material.color;
            color.a = 1f;
            _currSprite.material.color = color;

            _fadeSlipper = true;
            transform.GetChild(1).GetComponent<Collider2D>().enabled = true;
        }

        public void LaunchSlipper(float x_speed, float target_y_pos)
        {
            transform.position = _startPos;
            Vector2 target_pos = new Vector2(_targetPosX, target_y_pos);

            transform.LookAt(target_pos);
            float angle = -90f - transform.eulerAngles.x;

            if (target_pos.x < transform.position.x)
            {
                angle *= -1f;
            }

            transform.localRotation = Quaternion.Euler(0, 0, angle);
        }

        public void AddForceToRigid(Vector2 target_pos, float x_speed)
        {
            GetComponent<Rigidbody2D>().AddForce((target_pos - (Vector2)transform.position).normalized * x_speed, ForceMode2D.Impulse);
        }

        private IEnumerator FadeTo(float value, float time, Action call_back)
        {
            float alpha = _currSprite.material.color.a;
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / time)
            {
                Color color = new Color(1f, 1f, 1f, Mathf.Lerp(alpha, value, t));
                _currSprite.material.color = color;
                yield return null;
            }

            call_back();
        }

        private void WhenFadeEnd(object[] args)
        {
            ServiceLocator.GetService<ITimeController>().RemoveTimer(WhenFadeEnd);

            StartCoroutine(FadeTo(0.0f, _fadeTime, () => {
                isNotActive = false;

                if (OnBackToPoolHandler != null)
                {
                    OnBackToPoolHandler(gameObject);
                }

                transform.position = _startPos;
            }));
        }

        public void ColliderDisable()
        {
            transform.GetChild(1).GetComponent<Collider2D>().enabled = false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (OnHitItemHandler != null)
            {
                OnHitItemHandler(collision.collider, gameObject);
            }

            if (_fadeSlipper)
            {
                ServiceLocator.GetService<ITimeController>().AddTimerSingleAction(WhenFadeEnd, null, Time.time, Time.time + 0.15f);

                _fadeSlipper = false;
            }
        }        
    }
}
