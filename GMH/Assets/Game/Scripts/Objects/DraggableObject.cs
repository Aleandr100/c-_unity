﻿using UnityEngine;
using System.Collections;

public class DraggableObject : MonoBehaviour {

    private GameObject _draggableObject;
    private Vector3 _startPosition;
    private Vector3 _lastMouseWorldPosition;

    private int _startOrder;

    public GameObject hand;

    static public DraggableObject instance;

    public bool interactible;

    // Use this for initialization
    void Awake () {
        instance = this;
	}

    // Update is called once per frame
    void Update()
    {
        if (interactible)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    //Debug.Log(hit.transform.name);

                    try
                    {
                        if (hit.transform.parent.GetComponent<DraggableObject>())
                        {
                            _startOrder = hit.transform.GetComponent<SpriteRenderer>().sortingOrder;
                            _draggableObject = hit.transform.gameObject;
                            _startPosition = hit.transform.position;
                            hit.transform.GetComponent<SpriteRenderer>().sortingOrder = 51;
                        }
                    }
                    catch
                    {
                        //
                    }
                }
            }
            else if (Input.GetMouseButton(0))
            {
                if (_draggableObject)
                {
                    Vector3 mouseToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mouseToWorld.z = _draggableObject.transform.position.z;
                    _draggableObject.transform.position = mouseToWorld;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (_draggableObject)
                {
                    if (_draggableObject.name == LighthouseRoom.instance.neededObject.name)
                    {
                        //Debug.Log("need object realeased");
                        _draggableObject.GetComponent<SpriteRenderer>().enabled = false;

                        RaycastHit hit;

                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(ray, out hit, 100.0f))
                        {
                            //Debug.Log("drop");
                            _draggableObject.transform.SetParent(hand.transform);
                            _draggableObject.transform.position = hand.transform.position;
                            interactible = false;
                        }
                        _draggableObject.GetComponent<SpriteRenderer>().enabled = true;
                        _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = 15;
                    }
                    else
                    {
                        _draggableObject.GetComponent<SpriteRenderer>().sortingOrder = _startOrder;
                        _draggableObject.transform.position = _startPosition;
                    }
                    _draggableObject = null;
                }
            }
        }
    }
}
