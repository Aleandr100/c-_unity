﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GoodMorningHippo.Managers;
using System.Linq;
using DG.Tweening;

namespace GoodMorningHippo.Objects
{
    public class FishFeedPool : MonoBehaviour
    {
        public static FishFeedPool Instance;

        [SerializeField] private List<GameObject> _fishes_prefabs;
        [SerializeField] private Transform _pool_transf;

        private const int SIZE_FISH_POOL = 60;

        private List<FishMoveOnPath> _fish;
        private List<FishMoveOnPath> _fishes_active;

        private float _last_spawn;
        private float _delta_spawn,
                      _timeToEmission = 1.5f,
                      _time;

        private bool _StopEmission = false;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            _delta_spawn = 0.1f;
            _fish          = new List<FishMoveOnPath>();
            _fishes_active = new List<FishMoveOnPath>();

            InitFishPool();
        }

        private void Update()
        {
            if (ShallowManager.Instance.IsStopCatched && !_StopEmission)
            {                
                if (Time.time - _last_spawn > _delta_spawn)
                {
                    GetFishFromPool().MoveFishOnPath();

                    _last_spawn = Time.time;

                    if (_timeToEmission < 0)
                    {
                        _StopEmission = true;

                        DOVirtual.DelayedCall(0.5f, delegate { _time = AudioController.PlaySound("hp-209"); }).OnComplete(delegate
                        {
                            DOVirtual.DelayedCall(_time, delegate { /*  Switch to next scene here */ });
                        });
                    }
                    else
                    {
                        _timeToEmission -= Time.deltaTime;
                    }
                }
            }            
        }

        private List<Vector2> SetStartPos()
        {
            List<Vector2> start_pos = new List<Vector2>();
            for (int index = 0; index < SIZE_FISH_POOL; index++)
            {
                float start_x, start_y;
                start_x = Random.Range(-5.75f, -1.65f);
                start_y = Random.Range(-2.56f, -0.66f);

                Vector2 position = new Vector2(start_x, start_y);

                start_pos.Add(position);
            }

            return start_pos;
        }

        private List<Vector2> SetTargetPos()
        {
            List<Vector2> target_pos = new List<Vector2>();
            for (int index = 0; index < SIZE_FISH_POOL; index++)
            {
                float target_x, target_y;
                target_x = Random.Range(-0.15f, 2.68f);                
                target_y = Random.Range(-3.46f, -2.74f);

                Vector2 position = new Vector2(target_x, target_y);

                target_pos.Add(position);
            }

            return target_pos;
        }

        private void InitFishPool()
        {
            List<Vector2> start_pos  = SetStartPos ();
            List<Vector2> traget_pos = SetTargetPos();

            for (int index = 0; index < SIZE_FISH_POOL; index++)
            {
                int rand, firing_angle;
                rand = Random.Range(0, _fishes_prefabs.Count);

                firing_angle = Random.Range(45, 65);

                GameObject fish = Instantiate(_fishes_prefabs[rand], _pool_transf.position, Quaternion.identity) as GameObject;                

                fish.GetComponent<FishMoveOnPath>().LaunchFish(start_pos[index], traget_pos[index], firing_angle);

                fish.transform.parent = _pool_transf;

                _fish.Add(fish.GetComponent<FishMoveOnPath>());

                fish.SetActive(false);
            }
        }

        private FishMoveOnPath GetFishFromPool()
        {
            int rand = Random.Range(0, _fish.Count);

            if (!_fish.Count.Equals(0))
            {
                FishMoveOnPath fish = _fish.ElementAt(rand);

                _fish.Remove(fish);
                _fishes_active.Add(fish);
                _fishes_active[_fishes_active.Count - 1].gameObject.SetActive(true);

                return _fishes_active[_fishes_active.Count - 1];
            }
            return null;
        }

        public void BackFishToPool(FishMoveOnPath curr_fish)
        {            
            _fishes_active.Remove(curr_fish);
            _fish.Add(curr_fish);

            curr_fish.gameObject.SetActive(false);
        }
    }
}
