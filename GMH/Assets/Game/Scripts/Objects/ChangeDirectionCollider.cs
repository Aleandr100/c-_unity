﻿using UnityEngine;
using System.Collections;

public class ChangeDirectionCollider : MonoBehaviour {

    private int _constantVelocity = 300;

    private bool _isActive = false;

    private void Update()
    {
        if (Vector2.SqrMagnitude(GetComponent<Rigidbody2D>().velocity) < 2)
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f) * _constantVelocity));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<Rigidbody2D>().AddForce(collision.contacts[0].normal * _constantVelocity);
    }

    private void Awake()
    {
        this.enabled = false;
    }

    public IEnumerator jumpOut()
    {
        yield return new WaitForSeconds(0.3f);
        transform.SetParent(null);
        GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 300);
        yield return new WaitForSeconds(0.2f);
        GetComponent<BoxCollider2D>().isTrigger = false;
    }

    private void OnMouseDown()
    {
        //gameObject.SetActive(false);
    }
}
