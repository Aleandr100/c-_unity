﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace GoodMorningHippo.Objects
{
    public class FishMoveOnPath : MonoBehaviour
    {
        private const int COUNT_OF_WAYPOINTS = 23;

        private float _firing_angle,
                      _minHeightToSplash;

        private GameObject _splash;

        private Vector2 _startPos;
        private Vector2 _target_pos;

        private Vector3[] _path;            

        public void LaunchFish(Vector2 start_pos, Vector2 target_pos, float angel)
        {
            var loaded = Resources.Load("Game/Prefabs/RaccoonMaryShallow/waterSplash") as GameObject;

            _splash = GameObject.Instantiate(loaded).gameObject;

            _path = new Vector3[COUNT_OF_WAYPOINTS];         

            _startPos   = new Vector2(start_pos.x,  start_pos.y );
            _target_pos = new Vector2(target_pos.x, target_pos.y);

            _firing_angle = angel;            

            _minHeightToSplash = Random.Range(-3.25f, -2.25f);

            CalculatePath();
        }

        private void CalculatePath()
        {
            float x_distance;
            x_distance = _target_pos.x - _startPos.x;

            float total_velocity;
            total_velocity = x_distance / (Mathf.Sin(2 * _firing_angle * Mathf.Deg2Rad) / Physics2D.gravity.magnitude);

            float x_velo, y_velo;
            x_velo = Mathf.Sqrt(total_velocity) * Mathf.Cos(_firing_angle * Mathf.Deg2Rad);
            y_velo = Mathf.Sqrt(total_velocity) * Mathf.Sin(_firing_angle * Mathf.Deg2Rad);

            float delta_time = 0.085f;

            for (int index = 0; index < COUNT_OF_WAYPOINTS; index++)
            {
                float dx, dy;
                dx = x_velo * delta_time;
                dy = y_velo * delta_time - Physics2D.gravity.magnitude * delta_time * delta_time * 0.5f;

                _path[index] = new Vector3(_startPos.x + dx, _startPos.y + dy, 0f);                

                delta_time += 0.085f;
            }
        }

        public void MoveFishOnPath()
        {            
            int curr_waypoint = 0;

            transform.position = _startPos;

            SetRotation(curr_waypoint);
            transform.DOPath(_path, 2.8f, PathType.CatmullRom, PathMode.Sidescroller2D).OnWaypointChange(delegate 
            {
                if (curr_waypoint < _path.Length - 1)
                {
                    curr_waypoint++;
                    SetRotation(curr_waypoint);

                    if (transform.position.y < _minHeightToSplash)
                    {
                        _splash.transform.position = new Vector2(transform.position.x, transform.position.y);
                        _splash.GetComponent<Animator>().SetTrigger("Water_splash");
                    }
                }
            }).OnComplete(delegate 
            {                
                FishFeedPool.Instance.BackFishToPool(this);
            });
        }

        private void SetRotation(int index)
        {
            transform.LookAt(_path[index]);
            float angle = 0f - transform.eulerAngles.x;

            if (_path[index].x < transform.position.x)
            {
                angle *= -1f;
            }

            transform.localRotation = Quaternion.Euler(0, 0, angle);
        }        
    }
}
