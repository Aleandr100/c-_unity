﻿using UnityEngine;
using GoodMorningHippo.Managers;
using System.Collections;

namespace GoodMorningHippo.Objects
{
    public class CameraFallow : MonoBehaviour
    {
        public GameObject player;

        private Vector3 offset;

        void Start()
        {
            offset = transform.position - player.transform.position;
        }

        void LateUpdate()
        {
            if (SchoonerManager.Instance.is_moved_mary)
            {
                transform.position = player.transform.position + offset;
            }
        }
    }
}

