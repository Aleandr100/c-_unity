﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

namespace GMH.Objects
{
    public class HippoDressingSystem
    {
        private GameObject _currSlipper,
                           _slippersContainer;

        //private GameObject[] _dressedSlippers;

        private Vector2 _startPos,
                        _target_pos;

        private int _countDressedSlippers,            
                    _legNamber;

        private string _lastNameSlipper;

        private bool _isDressed = false;   //???????????????????

        private float _time;

        private SpriteRenderer       _currRender;
        private List<SpriteRenderer> _spriteSlippers;

        private Dictionary<Transform, Vector2> _startPosSlipper;

        //**********************last_changes**********************
        List<GameObject> _dressedSlippers;
        List<Transform>  _legsPos;
        //**********************last_changes**********************

        /**/

        //**********************last_changes**********************
        public HippoDressingSystem(GameObject hippoCharacter, GameObject slprsContainer)
        {
            _dressedSlippers = new List<GameObject>();
            _legsPos         = new List<Transform>();

            _dressedSlippers[0] = hippoCharacter.transform.Find("Noga_1/slipper_container_L").gameObject;
            _dressedSlippers[1] = hippoCharacter.transform.Find("Noga_2/slipper_container_R").gameObject;

            _legsPos[0] = hippoCharacter.transform.Find("Noga_1/target_pos_l");
            _legsPos[1] = hippoCharacter.transform.Find("Noga_2/target_pos_r");

            _slippersContainer = slprsContainer;

            InitSlippersSprite();

            InitStartPosSlippers();
        }
        //**********************last_changes**********************

        private void InitSlippersSprite()
        {
            _spriteSlippers = new List<SpriteRenderer>();
            foreach (Transform slpr in _slippersContainer.transform)
            {
                _spriteSlippers.Add(slpr.GetChild(0).GetComponent<SpriteRenderer>());
            }
        }

        private void InitStartPosSlippers()
        {
            _startPosSlipper = new Dictionary<Transform, Vector2>();
            foreach (Transform slpr in _slippersContainer.transform)
            {
                _startPosSlipper.Add(slpr, slpr.position);
            }
        }

        private SpriteRenderer FindSprite(GameObject go)
        {
            SpriteRenderer sprRender = null;
            foreach (SpriteRenderer renderer in _spriteSlippers)
            {
                if (renderer.transform.parent.gameObject == go)
                {
                    sprRender = renderer;
                    return sprRender;
                }
            }

            return sprRender;
        }

        private void TargetHasBeenReached(GameObject go, GameObject hippoCharacter, Action OnSucceededAction)
        {
            GameObject slipper = go.transform.parent.gameObject;

            SpriteRenderer currSlipperRender = FindSprite(slipper);

            slipper.SetActive(false);

            if (_countDressedSlippers == 0)
            {
                _lastNameSlipper = slipper.name;
                _countDressedSlippers++;

                _dressedSlippers[0].GetComponent<SpriteRenderer>().sprite = currSlipperRender.sprite;

                if (hippoCharacter != null)
                {
                    hippoCharacter.GetComponent<Animator>().SetTrigger("Сorrect_slipper");
                }

                _time = AudioController.PlaySound("hp-14");

            }
            else
            {
                if (slipper.name == _lastNameSlipper)
                {
                    _dressedSlippers[1].GetComponent<SpriteRenderer>().sprite = currSlipperRender.sprite;

                    if (hippoCharacter != null)
                    {
                        hippoCharacter.GetComponent<Animator>().SetTrigger("Сorrect_slipper");
                    }

                    _time =  AudioController.PlaySound("hp-15");
                    DOVirtual.DelayedCall(_time, delegate 
                    {
                        if (OnSucceededAction != null)
                        {
                            OnSucceededAction();
                        }
                    });
                }
                else
                {
                    slipper.gameObject.SetActive(true);

                    AudioController.PlaySound("hp-11");
                    if (hippoCharacter != null)
                    {
                        hippoCharacter.GetComponent<Animator>().SetTrigger("Wrong_slipper");
                    }
                }
            }
        }
    }
}
