﻿using UnityEngine;
using System.Collections;

public class waterScript : MonoBehaviour {

    public static bool wasOpened = false;

    public GameObject _hippoDad;

    private void OnMouseDown()
    {
        if (transform.name == "smesitelCold")
        {
            transform.parent.GetComponent<Animator>().SetTrigger("cold");
            _hippoDad.GetComponent<Animator>().SetTrigger("openCold");

            StartCoroutine("SwitchToNextScene");
        }
        else if (transform.name == "smesitelHot")
        {
            transform.parent.GetComponent<Animator>().SetTrigger("hot");
        }
    }

    IEnumerator SwitchToNextScene()
    {
        yield return new WaitForSeconds(9);
        PSV_Prototype.SceneLoader.Instance.SwitchToScene(PSV_Prototype.Scenes.MainMenu);
    }
}
