﻿using UnityEngine;
using System.Collections;
using GoodMorningHippo.Objects;
using GoodMorningHippo.Managers;
using DG.Tweening;

namespace GoodMorningHippo.Objects
{
    public class ThrowHook : MonoBehaviour
    {
        [SerializeField]
        private GameObject _hook,
                           _rope,
                           _racoonMary;                            

        [SerializeField]
        private LayerMask _touch_input_mask;

        [SerializeField]
        private Transform _for_cathed_items;

        private GameObject _catched_item;

        private Vector2 _end_pos;
        private Vector2 _hook_start_pos;

        private GameObject _catched_fish;
        private bool _is_catched_fish,
                     _useWhip = false;

        private float _min_input_y;
        private float _speed = 15.5f,   //????????????????????????????????????????????????
                      _delay = 0.5f;

        private readonly Vector2 _vect_throw_on_bag = new Vector2(2.62f, -3.43f);
        private readonly float _delay_throw_on_bag = 0.7f;
        private readonly string _ropeSL = "Rope";

        private Transform _hook_transf;
        private LineRenderer _rope_LR;

        private HookStates _curr_hook_state;
        private ClickStates _curr_click_state;

        private enum ClickStates
        {
            CLICKED,
            THROW_HOOK,
            NONE,
        }

        private enum HookStates
        {
            MOVE_TARGET,
            CATCH_ITEM,
            MOVE_HALFWAY,
            MOVE_START,
            NONE,
        }

        private void Start()
        {
            _hook_transf = _hook.transform;

            _hook_start_pos = _hook.transform.position;

            _min_input_y = 2.15f;
            _curr_hook_state = HookStates.NONE;
            _curr_click_state = ClickStates.NONE;

            _rope_LR = _rope.GetComponent<LineRenderer>();
            _rope_LR.sortingLayerName = _ropeSL;

            HookControll.OnCatchItem += WhenCatchItem;

            _is_catched_fish = false;

            //Time.timeScale = 0.3f;
        }

        private void Update()
        {
            if (!ShallowManager.Instance.IsStopCatched)
            {
                CheckInput();

                if (_curr_hook_state != HookStates.NONE)
                {
                    //**********last_changes**********
                    _rope_LR.enabled = true;
                    //**********last_changes**********

                    MoveHook();
                    MoveRope();
                }
                else
                {
                    //********************last_changes********************
                    //_hook.transform.localPosition = Vector2.zero;
                    
                    //********************last_changes********************
                }
            }
        }

        //private IEnumerator WaitForWhipUseAnim()
        //{
        //    Animator animator = _racoonMary.GetComponent<Animator>();
        //    while (animator.GetCurrentAnimatorStateInfo(0).IsName("use_whip") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.4f)
        //    {
        //        yield return null;
        //    }
        //}

        private void CheckInput()
        {
            if (_curr_hook_state == HookStates.NONE)
            {
                if (_curr_click_state == ClickStates.NONE)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        var curScreenSpace = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                        Vector2 curMousePos = Camera.main.ScreenToWorldPoint(curScreenSpace);

                        if (curMousePos.x < 0.85f && curMousePos.x > -4.25)
                        {
                            _end_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                            RaycastHit2D[] hits = Physics2D.RaycastAll(_end_pos, Vector2.zero, 20, _touch_input_mask);
                            for (int index = 0; index < hits.Length; index++)
                            {
                                RaycastHit2D hit = hits[index];
                                if (hit.collider != null)
                                {
                                    _catched_fish = hit.transform.gameObject;
                                }
                            }

                            if (_end_pos.y < _min_input_y)
                            {
                                _curr_click_state = ClickStates.CLICKED;
                                ChangeHookState();
                            }
                        }                        
                    }
                }
                else if (_curr_click_state == ClickStates.THROW_HOOK)
                {
                    _curr_hook_state = HookStates.MOVE_TARGET;

                    //********************last_changes********************
                    //_racoonMary.GetComponent<Animator>().SetTrigger("Use_whip");
                    //********************last_changes********************
                }
            }
        }

        private void MoveHook()
        {
            print("HERE");

            //************************last_changes************************
            if (!_useWhip)
            {
                _racoonMary.GetComponent<Animator>().SetTrigger("Use_whip");
                _useWhip = true;
            }
            //************************last_changes************************

            DOVirtual.DelayedCall(_delay, delegate 
            {
                if (_catched_fish)
                { 
                    print("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
                    _hook.transform.GetChild(1).GetComponent<Collider2D>().enabled = true;
                    if (!_is_catched_fish)
                    {
                        _hook_transf.position = Vector2.MoveTowards(_hook_transf.position, _catched_fish.transform.position, Time.deltaTime * _speed);
                    }
                    print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                }
                else
                {
                    _hook_transf.position = Vector2.MoveTowards(_hook_transf.position, _end_pos, Time.deltaTime * _speed);
                }

                if (_curr_hook_state == HookStates.CATCH_ITEM)
                {
                    _end_pos = GetBeetweePosition();
                    _curr_hook_state = HookStates.MOVE_HALFWAY;
                }

                if ((Vector2)_hook_transf.position == _end_pos)
                {
                    if (_curr_hook_state == HookStates.MOVE_TARGET)
                    {
                        _end_pos = GetBeetweePosition();
                        _curr_hook_state = HookStates.MOVE_HALFWAY;
                    }
                    else if (_curr_hook_state == HookStates.MOVE_HALFWAY)
                    {
                        _end_pos = _hook_start_pos;
                        _curr_hook_state = HookStates.MOVE_START;
                    }
                    else if (_curr_hook_state == HookStates.MOVE_START)
                    {
                        _hook.transform.localPosition = Vector2.zero;   //??????????????????????

                        //**********last_changes**********
                        _rope_LR.enabled = false;
                        //**********last_changes**********

                        _useWhip = false;

                        _delay = 0.5f;

                        print("HERE");

                        _curr_click_state = ClickStates.NONE;
                        _curr_hook_state = HookStates.NONE;

                        _is_catched_fish = false;

                        ReactivatHookTrigger();
                        ThrowCatchedItemOnBag();
                    }
                }
            });            
        }

        private void ChangeHookState()
        {
            _curr_click_state = ClickStates.THROW_HOOK;
        }

        private void MoveRope()
        {
            _rope_LR.SetPosition(0, _rope_LR.gameObject.transform.position);
            _rope_LR.SetPosition(1, _hook_transf.position);
        }

        private Vector2 GetBeetweePosition()
        {
            Vector2 firstPos = _hook_start_pos;
            Vector2 secondPos = new Vector2(firstPos.x, _hook.transform.position.y);
            Vector2 betweenPos = (firstPos + secondPos) * 0.5f;

            return betweenPos;
        }

        private void WhenCatchItem(GameObject item)
        {
            _catched_item = item;
            _curr_hook_state = HookStates.CATCH_ITEM;

            _is_catched_fish = true;
            _catched_fish = null;                      

            if (_catched_item.CompareTag("Fish"))
            {
                /**/
            }
            else if (_catched_item.CompareTag("Star"))
            {
                /**/
            }
        }

        private void ThrowCatchedItemOnBag()
        {            
            if (_catched_item)
            {
                Transform _catched_transf = _catched_item.transform;                
                _catched_transf.SetParent(_for_cathed_items);
                _catched_transf.DOMove(_vect_throw_on_bag, _delay_throw_on_bag);
                _catched_transf.DOScale(new Vector2(_catched_transf.localScale.x / 2f, _catched_transf.localScale.y / 2f), _delay_throw_on_bag);

                ShallowManager.Instance.CatchedItems = 1;

                _catched_item = null;
            }
        }

        private void ReactivatHookTrigger()
        {
            var hook_trigger = _hook.GetComponent<HookControll>();
            if (hook_trigger)
            {
                hook_trigger.IsCatchItem = false;
                _hook.transform.position = _hook_start_pos;
            }
        }
    }
}

