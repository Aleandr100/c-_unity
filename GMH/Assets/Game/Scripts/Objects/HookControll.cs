﻿using UnityEngine;
using GoodMorningHippo.Managers;
using System.Collections;

namespace GoodMorningHippo.Objects
{
    public class HookControll : MonoBehaviour
    {
        public delegate void MyEvent(GameObject coll);
        public static event MyEvent OnCatchItem;

        [SerializeField] private Transform _for_items;

        private GameObject _catched_item;

        private bool _is_catch_item;

        public bool IsCatchItem {
            get {
                return _is_catch_item;
            }
            set {
                _is_catch_item = value;
            }
        }

        public GameObject CatchedItem {
            get {
                return _catched_item;
            }
            set {
                _catched_item = value;
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            transform.GetChild(1).GetComponent<Collider2D>().enabled = false;
            if (!IsCatchItem)
            {

                CatchItem(collider.gameObject);
            }
        }

        private void CatchItem(GameObject catched_item)
        {
            IsCatchItem = true;            

            CheckItemType(catched_item);
            SendEventCatchItem(CatchedItem);
        }

        private void CheckItemType(GameObject catched_item)
        {            
            _catched_item = catched_item.transform.parent.gameObject;
            _catched_item.transform.SetParent(_for_items);

            if (_catched_item.CompareTag("Fish"))
            {
                print("HERE");

                _catched_item.GetComponent<FishMoveRand>().isMoved = false;

                GameObject item = catched_item.transform.parent.gameObject;

                FishPoolControll.Instance.RemoveFromPool(item.GetComponent<FishMoveRand>());

                item.GetComponent<FishMoveRand>().ColliderOff();
            }
            else if (catched_item.CompareTag("Star"))
            {
                /**/
            }
        }

        //private void StopFishMove(GameObject collider)
        //{
        //    if (collider.CompareTag("Fish"))
        //    {
        //        var collFishMove = collider.GetComponent<FishMoveRand>();
        //        if (collFishMove.isActive)
        //        {
        //            collFishMove.isMoved = false;
        //            FishMoveRand fishComp = collider.transform.parent.GetComponent<FishMoveRand>();
        //            FishPoolControll.Instance.RemoveFromPool(fishComp);
        //        }
        //    }
        //}

        private void SendEventCatchItem(GameObject go)
        {
            if (OnCatchItem != null)
            {                
                OnCatchItem(go);
            }
        }
    }
}

