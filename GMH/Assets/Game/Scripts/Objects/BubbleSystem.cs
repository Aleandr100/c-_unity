﻿using UnityEngine;
using System.Collections.Generic;


public class BubbleSystem : MonoBehaviour {

    private List<Bubble> _bubbles,
                         _onScene;

    private AppCore.Range _timeRange = new AppCore.Range(0.1f, 0.3f),
                          _sizeRange = new AppCore.Range(0.5f, 0.9f),
                          _worldRangeX = new AppCore.Range(-1, 1),
                          _worldRangeY = new AppCore.Range(-2, 2),
                          _forceRange = new AppCore.Range(50, 220);

    private List<Sprite> _bubbleImages;

    private GameObject _bubblePrefab;
    
    private const int _POOL_SIZE = 15;

    private bool _isGenerating = false;
    
    public void Play()
    {
        _nextGenTime = Time.time;

        _isGenerating = true;
    }
    
    public void Stop()
    {
        _isGenerating = false;
    }

    private float _nextGenTime = 0;

    private void Awake()
    {
        _bubbles = new List<Bubble>();
        _onScene = new List<Bubble>();
        _bubbleImages = new List<Sprite>();
        
        _bubbleImages.AddRange( Resources.LoadAll<Sprite>("Game/Images/Bubbles") );
        _bubblePrefab = Resources.Load<GameObject>("Game/Prefabs/HippoBathroomMom/Bubble");

        for (int i = 0; i < _POOL_SIZE; i++)
        {
            var bubbleInstance = GameObject.Instantiate(_bubblePrefab);
            _bubbles.Add( bubbleInstance.GetComponent<Bubble>() );
            bubbleInstance.SetActive(false);
            bubbleInstance.GetComponent<Bubble>().bs = this;

            SetRandomBubbleImage(bubbleInstance);
        }
    }

    private void SetRandomBubbleImage(GameObject bubbleObject)
    {
        bubbleObject.GetComponent<SpriteRenderer>().sprite = _bubbleImages[Random.Range(0, _bubbleImages.Count)];
    }
    
    // Update is called once per frame
    private void Update () {
	    for (int i = 0; i < _onScene.Count; i++)
        {
            if (_onScene[i].transform.position.y > 6)
            {
                PutInPull(_onScene[i]);
            }
        }

        if (_isGenerating) CheckAndGenerate();
    }

    private void CheckAndGenerate()
    {
        if (Time.time >= _nextGenTime)
        {
            Spawn();

            _nextGenTime += Random.Range(_timeRange.min, _timeRange.max);
        }
    }

    public void PutInPull(Bubble bubble)
    {
        _onScene.Remove(bubble);

        _bubbles.Add(bubble);

        bubble.gameObject.SetActive(false);
    }

    public void Spawn()
    {
        if (_bubbles.Count > 0)
        {
            Bubble returnedBubble = _bubbles[0];

            _onScene.Add(returnedBubble);

            _bubbles.Remove(returnedBubble);

            returnedBubble.transform.position = transform.position;

            SetRandomBubbleImage(returnedBubble.gameObject);

            returnedBubble.transform.localScale = Vector3.one * Random.Range(_sizeRange.min, _sizeRange.max);

            returnedBubble.gameObject.SetActive(true);

            returnedBubble.GetComponent<Rigidbody2D>().AddForce(Vector2.left * Random.Range(_forceRange.min, _forceRange.max));
        }
    }
}
