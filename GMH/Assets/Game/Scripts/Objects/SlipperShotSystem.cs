﻿using AppCore;
using UnityEngine;
using System.Collections;

namespace GMH.Objects
{
    public class SlipperShotSystem
    {
        private const int MAX_NUMBERS_OF_SHOTS = 5;

        private int _countShotSlippers = 1;

        private float _targetPosX = 5.5f,
                      _posToBeat = 1.05f;

        private bool _disableOneSlipper = false;

        private GameObject _blanketColl;
        private PoolController _slippersPool;

        private Range _xSpeedRange = new Range(3.55f, 4.37f);

        public bool isLastShot   { get; set; }
        public bool hitByBlanket { get; set; }

        public int countShotSlippers { get; set; }
        public int numberOfShots     { get; set; }

        public SlipperShotSystem(Transform slippersTransf, GameObject objColl)
        {
            _blanketColl = objColl;
            countShotSlippers = 1;
            numberOfShots = 0;

            _slippersPool = new PoolController(slippersTransf);
        }

        public void ShotSlipper(bool cuckooIsOn)
        {
            ServiceLocator.GetService<ITimeController>().RemoveTimer(delegate { ShotSlipper(cuckooIsOn); });

            numberOfShots++;

            if (numberOfShots > MAX_NUMBERS_OF_SHOTS)
            {
                countShotSlippers = cuckooIsOn ? 1 : 4;

                if (!cuckooIsOn) isLastShot = true;

                _disableOneSlipper = true;
            }

            for (int index = 0; index < countShotSlippers; index++)
            {
                float xSpeed, targetPosY;

                xSpeed = Random.Range(_xSpeedRange.min, _xSpeedRange.max);
                targetPosY = SetTargetPosY(cuckooIsOn);

                SlipperMove slipper = _slippersPool.GetFishFromPool().GetComponent<SlipperMove>();

                if (_disableOneSlipper)
                {
                    slipper.isNotActive = true;
                    _disableOneSlipper = false;
                }

                if (slipper != null)
                {
                    Vector2 target_pos = new Vector2(_targetPosX, targetPosY);

                    slipper.LaunchSlipper(xSpeed, targetPosY);
                    slipper.AddForceToRigid(target_pos, xSpeed);
                }
            }
        }

        private float SetTargetPosY(bool cuckooIsOn)
        {
            return cuckooIsOn ? Random.Range(0.87f, 2.66f) : Random.Range(-1.27f, -0.51f);
        }

        public void BeatSlipper(GameObject item)
        {
            Transform slipperTransf;
            SlipperMove slipper = item.transform.parent.GetComponent<SlipperMove>();

            slipperTransf = slipper.transform;

            if (!slipper.isNotActive && slipperTransf.position.x > _posToBeat)
            {
                slipper.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

                float xTarget, yTarget, force;

                if (hitByBlanket)
                {
                    xTarget = Random.Range(-0.45f, 1.35f);
                    yTarget = Random.Range(-0.85f, 0.05f);

                    _blanketColl.SetActive(true);

                    if (!isLastShot)
                    {
                        hitByBlanket = false;
                    }
                }
                else
                {
                    xTarget = Random.Range(-2.25f, -1.28f);
                    yTarget = Random.Range( 0.25f,  1.25f);
                }

                force = hitByBlanket ? 6.1f : 5.5f;

                Vector2 target_pos = new Vector2(xTarget, yTarget);
                slipper.GetComponent<SlipperMove>().AddForceToRigid(target_pos, force);
            }
        }

        public void DisableActiveSlippers()
        {
            ServiceLocator.GetService<ITimeController>().RemoveTimer(delegate { DisableActiveSlippers(); });

            _slippersPool.BackToPoolActiveItems();
        }

        public void OnBackToPoolHandler(GameObject slipper)
        {
            _slippersPool.BackSlipperToPool(slipper);
        }
    }
}

