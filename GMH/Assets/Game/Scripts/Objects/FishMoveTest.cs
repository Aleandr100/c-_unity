﻿using UnityEngine;
using System.Collections;

namespace GoodMorningHippo.Objects
{
    public class FishMoveTest : MonoBehaviour
    {
        private Transform _transf;

        private Vector2 _startPos;
        private Vector2 _end_pos;

        private float _speed;
        private readonly float _speedMin = 0.9f;
        private readonly float _speedMax = 1.4f;

        private readonly float _minPosY = -1.85f;
        private readonly float _maxPosY = -0.95f;

        public enum States
        {
            NONE,
            MOVE_TO_POS,
            STOP
        }

        private States _curr_state = States.NONE;

        private void Start()
        {            
            _transf = transform;
            _startPos = transform.position;

            _end_pos = new Vector2(-9.35f, _transf.position.y);

            ChangeSpeed();
        }

        private void Update()
        {
            Movement();
        }

        private void Movement()
        {
            if (_curr_state != States.STOP)
            {
                _transf.localPosition = Vector2.MoveTowards(_transf.position, _end_pos, Time.deltaTime * _speed);
            }

            if ((Vector2)_transf.localPosition == _end_pos)
            {
                _curr_state = States.STOP;

                ResetFishParameters();

                _end_pos = _startPos;
            }
            
            if (_curr_state == States.STOP)
            {
                _curr_state = States.MOVE_TO_POS;

                _startPos = _transf.position;
            }
        }

        private void ChangeSpeed()
        {
            _speed = Random.Range(_speedMin, _speedMax);
        }

        private void ResetFishParameters()
        {
            ChangeSpeed();
            ChangeHeight();
            ChangeDirection();
        }

        private void ChangeHeight()
        {
            float y_pos = Random.Range(_minPosY, _maxPosY);
            _transf.localPosition = new Vector2(_transf.position.x, y_pos);
        }

        private void ChangeDirection()
        {
            Vector3 the_scale = _transf.localScale;

            the_scale.x *= -1;
            _transf.localScale = the_scale;
        }
    }
}
