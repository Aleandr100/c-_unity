﻿using UnityEngine;
using System.Collections;

public class BlindSwiper : MonoBehaviour {

    bool _isCatched,
         _swipedOff;

    Vector3 _startPosition,
            _startSize;

    float _touchMultiPly;

    float _currentMouseDelta;

    private Vector2 _worldSpriteSize;

    private Transform _modifiableTransform;

    private Vector2 _prevMousePos,
                        _currentMousePos;

    protected Vector2 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    protected Vector2 GetMouseDeltaScreen()
    {
        return _currentMousePos - _prevMousePos;
    }

    private void Awake()
    {
        _startPosition = transform.position;
        _startSize = transform.localScale;

        _modifiableTransform = transform.GetChild(0);

        var sr = _modifiableTransform.GetComponent<SpriteRenderer>();

        _worldSpriteSize = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

        Debug.Log(_worldSpriteSize);
    }

    // Update is called once per frame
    void Update () {
        _prevMousePos = _currentMousePos;
        _currentMousePos = Input.mousePosition;

        if (_isCatched)
        {
            _currentMouseDelta += GetMouseDeltaWorld().x;


            if (_currentMouseDelta > 0)
            {
                _modifiableTransform.position = _startPosition + _currentMouseDelta * Vector3.right;

                Debug.Log("swiping");

                _modifiableTransform.localScale = new Vector3((_worldSpriteSize.x / 2 - _currentMouseDelta) / (_worldSpriteSize.x / 2), 1, 1); /// (_worldSpriteSize.x / 2);

                if (_currentMouseDelta >= 1.6f)
                {
                    _isCatched = false;

                    Debug.Log("swipedOff");

                    Camera.main.GetComponent<Animator>().Play("back", 0, 0);
                    _swipedOff = true;

                    GetComponent<BoxCollider2D>().enabled = false;
                }
            }
            else if (_currentMouseDelta < -0.5f)
            {
                _isCatched = false;
            }
        }
        else if (!_swipedOff)
        {
            _modifiableTransform.position = new Vector3(Mathf.Lerp(_modifiableTransform.position.x, _startPosition.x, 6 * Time.deltaTime), _startPosition.y, _startPosition.z);
            _modifiableTransform.localScale = new Vector3(Mathf.Lerp(_modifiableTransform.localScale.x, 1, 6 * Time.deltaTime), 1, 1);
        }
        else if (_swipedOff)
        {
            _modifiableTransform.position = new Vector3(Mathf.Lerp(_modifiableTransform.position.x, _startPosition.x + _worldSpriteSize.x / 2, 4 * Time.deltaTime), _startPosition.y, _startPosition.z);
            _modifiableTransform.localScale = new Vector3(Mathf.Lerp(_modifiableTransform.localScale.x, 0.3f, 4 * Time.deltaTime), 1, 1);
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            OnMouseUp();
        }
    }

    private void OnMouseDown()
    {
        if (!_swipedOff)
        {
            Debug.Log("touched");
            if (AppCore.ServiceLocator.GetService<AppCore.IAgeController>().age == AppCore.Enumerators.Age.FIVE_PLUS)
            {
                _isCatched = true;
            }
            else
            {
                Camera.main.GetComponent<Animator>().Play("back", 0, 0);
                _swipedOff = true;
            }
            _currentMouseDelta = 0;
        }
    }

    private void OnMouseUp()
    {
        if (!_swipedOff)
        {
            Debug.Log("untouched");
            _isCatched = false;
        }
    }
}
