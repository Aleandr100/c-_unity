﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnClickBasket : MonoBehaviour {

    static private GameObject _maryCharacterGameObject;

    static public int basketsCount = 0;

    private bool _isThrown = false;
    private List<ChangeDirectionCollider> changeDirCol = new List<ChangeDirectionCollider>();

    public void Awake()
    {
        basketsCount++;
    }

    public void Throw()
    {
        if (_isThrown) return;
        _isThrown = true;
        basketsCount--;
        StartCoroutine("PlashkaToBackground", 0.4f);
        GetComponentInParent<Animator>().Play("basketExplode");
        
        changeDirCol.AddRange(GetComponentsInChildren<ChangeDirectionCollider>());

        foreach (ChangeDirectionCollider element in changeDirCol)
        {
            if (element.GetComponent<ChangeDirectionCollider>()) element.StartCoroutine("jumpOut");
        }
    }

    private IEnumerator PlashkaToBackground(float time)
    {
        yield return new WaitForSeconds(time);
        transform.parent.GetComponent<SpriteRenderer>().sortingOrder = 1;
        foreach (ChangeDirectionCollider element in changeDirCol)
        {
            if (element.GetComponent<ChangeDirectionCollider>()) element.GetComponent<ChangeDirectionCollider>().enabled = true;
        }
    }
    
}
