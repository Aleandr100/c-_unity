﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DontDestroyMe : MonoBehaviour {

    [SerializeField]
    private Button _buttonOpenScenes;

    [SerializeField]
    private GameObject _sceneList;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        _buttonOpenScenes.onClick.AddListener(HideOrShowSceneList);
    }

    void HideOrShowSceneList()
    {
        _sceneList.SetActive(!_sceneList.activeSelf);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
