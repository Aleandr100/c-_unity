﻿using UnityEngine;
using GoodMorningHippo.Objects;
using GoodMorningHippo.Managers;
using System.Collections;

namespace GMH.Objects
{   
    public class DropeZone : MonoBehaviour
    {
        public static DropeZone Instance;

        public delegate void OnDragEnd(GameObject collider);
        public static event OnDragEnd OnDragItem;

        private void Awake()
        {
            Instance = this;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            SendEventMovedItem(collider.gameObject);
        }

        public void SendEventMovedItem(GameObject go)
        {
            if (OnDragItem != null)
                OnDragItem(go);
        }
    }
}
