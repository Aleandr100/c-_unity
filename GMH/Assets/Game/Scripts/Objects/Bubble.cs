﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour {

    [HideInInspector]
    public BubbleSystem bs;

    private void OnMouseDown()
    {
        bs.PutInPull(this);
    }
}
