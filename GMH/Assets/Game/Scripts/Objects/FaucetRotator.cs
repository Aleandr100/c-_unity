﻿using UnityEngine;
using System.Collections;

public class FaucetRotator : MonoBehaviour {

    private float _angleToOpen = 270,
                  _rotationSpeed = 35,
                  _timeToOpen = 5,
                  _alreadyRotated,
                  _distanceForRotation;

    private bool _isCatched = false,
                 _isOpen = false;

    Vector3 _startPosition,
            _startSize;


    private Vector2 _worldSpriteSize,
                    _prevMousePos,
                    _currentMousePos,
                    _rotationMousePos;

    private Quaternion _startRotation;

    protected Vector2 GetMouseDeltaWorld()
    {
        return Camera.main.ScreenToWorldPoint(_currentMousePos) - Camera.main.ScreenToWorldPoint(_prevMousePos);
    }

    protected Vector2 GetMouseDeltaScreen()
    {
        return _currentMousePos - _prevMousePos;
    }

    private void SquaredMouseRotation()
    {
        if (_rotationMousePos.x >= 0 && _rotationMousePos.x < 1 && _rotationMousePos.y == 0)
        {
            float addToX = GetMouseDeltaWorld().x;
            
            _rotationMousePos += new Vector2(addToX > 0 ? addToX : 0, 0);
        }
    }

    private void Awake()
    {
        var sr = GetComponent<SpriteRenderer>();

        _worldSpriteSize = sr.sprite.rect.size / sr.sprite.pixelsPerUnit;

        _startRotation = transform.rotation;
    }
    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (!_isOpen)
        {
            Vector3 rotateDistance = _rotationSpeed * Vector3.forward * Time.deltaTime;

            if (_isCatched)
            {
                _alreadyRotated += rotateDistance.z;

                transform.Rotate(rotateDistance);

                if (_alreadyRotated >= _angleToOpen)
                {
                    transform.rotation = Quaternion.Euler(0, 0, _angleToOpen);
                    _isCatched = false;
                    _isOpen = true;
                }
            }
            else
            {
                if (_alreadyRotated > 0)
                {
                    _alreadyRotated -= rotateDistance.z;

                    transform.Rotate(-rotateDistance);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            }

        }
    }

    private void OnMouseDown()
    {
        if (!_isOpen)
        {
            _isCatched = true;
        }
    }

    private void OnMouseUp()
    {
        if (!_isOpen)
        {
            _isCatched = false;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            OnMouseUp();
        }
    }
}
