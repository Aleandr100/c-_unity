﻿using UnityEngine;
using System.Collections;

public class SwitcherLighthouseRoom : MonoBehaviour {

    public GameObject switcherOn,
                      switcherOff,
                      lightOn,
                      lightOff;

    public bool isActiveOnStart,
                isPlayedOnce,
                interactible;

    private bool _isOn;

    public static SwitcherLighthouseRoom instance;

    private void Awake()
    {
        instance = this;
    }

    public bool isOn
    {
        get { return _isOn; }
        set
        {
            _isOn = value;

            switcherOn.SetActive(value);
            switcherOff.SetActive(!value);

            lightOn.SetActive(value);
            lightOff.SetActive(!value);
        }
    }

    private void Start()
    {
        isOn = isActiveOnStart;
        if (isActiveOnStart)
        {
            Turn(true);
        }
        else
        {
            Turn(false);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                //Debug.Log(hit.transform.name);

                if (interactible)
                {
                    if (hit.transform.name == "switcher")
                    {
                        if (isPlayedOnce)
                        {
                            if (isActiveOnStart == isOn) LighthouseRoom.instance.TurnOffSwitcher();
                        }
                        else
                        {
                            LighthouseRoom.instance.TurnOffSwitcher();
                        }
                    }
                }
            }
        }
    }

    private void Turn(bool on)
    {
        isOn = on;
    }

    public void TurnState()
    {
        isOn = !isOn;
    }
}
