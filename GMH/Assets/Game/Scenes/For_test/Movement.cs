﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Movement : MonoBehaviour {

    [SerializeField] private Transform _target_point;
    [SerializeField] private float _firing_angle;
    //******************************for_test******************************
    [SerializeField] private GameObject _point_prefab;

    [SerializeField] private bool _use_physics;
    //******************************for_test******************************

    private const int COUNT_OF_WAYPOINTS = 25;

    private Vector3 _startPos;
    private Vector3[] _path;

    private float _gravity;
    //*****************for_test*****************
    private GameObject _point;
    private Rigidbody2D rigid_body2D;
    //*****************for_test*****************

    private bool _start_move;

    public bool StartMove {
        get {
            return _start_move;
        }
        set {
            _start_move = value;
        }
    }

    private void Start()
    {        
        _start_move = false;
        _startPos = transform.position;

        _path = new Vector3[COUNT_OF_WAYPOINTS];

        _gravity = 9.8f;
        //*************************for_test*************************
        rigid_body2D = GetComponent<Rigidbody2D>();

        _use_physics = false;
        //*************************for_test*************************
    }

    private void Update()
    {
        if (_start_move)
        {
            if (_use_physics)
            {
                //*****************for_test*****************
                rigid_body2D.isKinematic = false;
                MoveRigidBody();
                //*****************for_test*****************
            }
            else
            {
                CalculatePath();
                MoveObjectOnPath();

                //StartCoroutine(MovementOnPath());
            }
            
            _start_move = false;
        }
    }

    //******************************************for_test******************************************
    private void MoveRigidBody()
    {
        float x_distance;
        x_distance = _target_point.position.x - transform.position.x;

        float total_velocity;
        total_velocity = x_distance / (Mathf.Sin(2 * _firing_angle * Mathf.Deg2Rad) / Physics2D.gravity.magnitude);

        float x_velo, y_velo;
        x_velo = Mathf.Sqrt(total_velocity) * Mathf.Cos(_firing_angle * Mathf.Deg2Rad);
        y_velo = Mathf.Sqrt(total_velocity) * Mathf.Sin(_firing_angle * Mathf.Deg2Rad);

        rigid_body2D.velocity = new Vector2(x_velo, y_velo);
    }
    //******************************************for_test******************************************

    private void CalculatePath()
    {
        float x_distance;
        x_distance = _target_point.position.x - transform.position.x;

        float total_velocity;
        total_velocity = x_distance / (Mathf.Sin(2 * _firing_angle * Mathf.Deg2Rad) / Physics2D.gravity.magnitude);

        float x_velo, y_velo;
        x_velo = Mathf.Sqrt(total_velocity) * Mathf.Cos(_firing_angle * Mathf.Deg2Rad);
        y_velo = Mathf.Sqrt(total_velocity) * Mathf.Sin(_firing_angle * Mathf.Deg2Rad);

        float delta_time = 0.085f;
        
        for (int index = 0; index < COUNT_OF_WAYPOINTS; index++)
        {
            float dx, dy;
            dx = x_velo * delta_time;
            dy = y_velo * delta_time - Physics2D.gravity.magnitude * delta_time * delta_time * 0.5f;            

            _path[index] = new Vector3(_startPos.x + dx, _startPos.y + dy, 0f);

            //******************************************for_test******************************************
            _point = Instantiate(_point_prefab, _path[index], Quaternion.Euler(Vector2.zero)) as GameObject;
            //******************************************for_test******************************************

            delta_time += 0.085f;
        }
    }

    private IEnumerator MovementOnPath()
    {
        float x_distance;
        x_distance = _target_point.position.x - transform.position.x;

        float total_velocity;
        total_velocity = x_distance / (Mathf.Sin(2 * _firing_angle * Mathf.Deg2Rad) / Physics2D.gravity.magnitude);

        float x_velo, y_velo;
        x_velo = Mathf.Sqrt(total_velocity) * Mathf.Cos(_firing_angle * Mathf.Deg2Rad);
        y_velo = Mathf.Sqrt(total_velocity) * Mathf.Sin(_firing_angle * Mathf.Deg2Rad);

        float flight_duration = x_distance / x_velo;

        float elapse_time = 0f;

        while (elapse_time < flight_duration)
        {
            SetRotation(x_velo, y_velo, elapse_time);
            PointReached(x_velo, y_velo, elapse_time, () => {
                transform.Translate(x_velo * Time.deltaTime, (y_velo - (_gravity * elapse_time)) * Time.deltaTime, 0f);
            });
            
            elapse_time += Time.deltaTime;

            yield return null;
        }
    }

    private IEnumerator PointReached(float x_velo, float y_velo, float elapse_time, System.Action moveToPoint)
    {
        moveToPoint();

        Vector3 next_pos = new Vector3(transform.position.x + x_velo * Time.deltaTime, transform.position.y + (y_velo - (_gravity * elapse_time)) * Time.deltaTime, 0f);

        while (transform.position.x < next_pos.x)
        {
            yield return null;
        }
    }

    private void SetRotation(float x_velo, float y_velo, float elapse_time)
    {
        Vector2 next_pos = new Vector2(transform.position.x + x_velo * Time.deltaTime, transform.position.y + (y_velo - (_gravity * elapse_time)) * Time.deltaTime);

        transform.LookAt(next_pos);
        float angle = -90f - transform.eulerAngles.x;

        if (next_pos.x < transform.position.x)
        {
            angle *= -1f;
        }

        transform.localRotation = Quaternion.Euler(0, 0, angle);
    }

    private void SetRotation(int index)
    {
        transform.LookAt(_path[index]);
        float angle = -90f - transform.eulerAngles.x;

        if (_path[index].x < transform.position.x)
        {
            angle *= -1f;
        }

        transform.localRotation = Quaternion.Euler(0, 0, angle);
    }

    private void MoveObjectOnPath()
    {        
        int curr_waypoint = 0;

        SetRotation(curr_waypoint);
        transform.DOPath(_path, 1.9f, PathType.CatmullRom, PathMode.Sidescroller2D).OnWaypointChange(delegate {
            if (curr_waypoint < _path.Length - 1)
            {
                curr_waypoint++;
                SetRotation(curr_waypoint);
            }
        });
    }
}
