﻿using UnityEngine;
using System.Collections;

public class WhipMove : MonoBehaviour
{
    private Vector2 _offset;

    Collider2D _colliderTouched;

    private void Start()
    {
        /**/
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnTouchDown();
        }
        else if (Input.GetMouseButton(0))
        {
            OnTouch();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            OnTouchUp();
        }
    }

    private void OnTouchDown()
    {
        Vector2 touchToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _colliderTouched = Physics2D.OverlapPoint(touchToWorld);

        if (_colliderTouched != null)
        {
            var currMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            _offset = _colliderTouched.transform.position - Camera.main.ScreenToWorldPoint(currMousePos);
        }
            
    }

    private void OnTouch()
    {
        if (_colliderTouched != null)
        {
            var currScreenSpace = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            var cur_pos = (Vector2)Camera.main.ScreenToWorldPoint(currScreenSpace) + _offset;

            _colliderTouched.transform.position = cur_pos;
        }
    }

    private void OnTouchUp()
    {
        /**/
    }
}
