﻿using UnityEngine;
using System.Collections;

public class InputCheck : MonoBehaviour {

    [SerializeField] private LayerMask touchInputMask;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (gameObject == GetClickedObject())
            {
                GetComponent<Movement>().StartMove = true;
            }
        }
    }

    private GameObject GetClickedObject()
    {
        GameObject target = null;

        Vector2 end_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        RaycastHit2D[] hits = Physics2D.RaycastAll(end_pos, Vector2.zero, 20, touchInputMask);
        for (int index = 0; index < hits.Length; index++)
        {
            RaycastHit2D hit = hits[index];
            if (hit.collider != null)
            {
                target = hit.collider.gameObject;
            }
        }

        return target;
    }
}
