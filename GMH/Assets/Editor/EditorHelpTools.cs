using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.IO;
using System.Xml;


[InitializeOnLoad]
public class ManifestStartupCheck
{
    static ManifestStartupCheck ()
    {
        Debug.Log ( "Manifest checking" );
        EditorHelpTools.CheckPushActivities ( );
    }
}


public class EditorHelpTools :MonoBehaviour
{

    private static string unityNativeActivityName = "com.unity3d.player.UnityPlayerNativeActivity";
    private static string psv_push_reseiver_name = "com.psvstudio.pushservice.PSReceiver";
    private static string psv_push_reseiver_category = "android.intent.category.DEFAULT";
    private static string psv_push_boot_reseiver_name = "com.psvstudio.pushservice.PSBootReceiver";
    private static string psv_push_boot_receiver_action = "com.psvstudio.pushservice.PSBootReceiver";
    private static string psv_push_boot_receiver_poweron = "android.intent.action.QUICKBOOT_POWERON";
    private static string psv_push_boot_receiver_boot = "android.intent.action.BOOT_COMPLETED";

    private static string [] necessary_permissions = new string [] {
        "android.permission.RECEIVE_BOOT_COMPLETED",
        "android.permission.VIBRATE",
        "android.permission.INTERNET",
        "android.permission.ACCESS_WIFI_STATE",
        "android.permission.ACCESS_NETWORK_STATE",
        };






    public enum ManifestSection
    {
        All,
        Push_bundles,
        play_services,
    }


    [MenuItem ( "HelpTools/AndroidManifest/Check play-services" )]
    public static void CheckPlayServicesTheme ()
    {
        CheckAndFixManifest ( ManifestSection.play_services );
    }



    [MenuItem ( "HelpTools/AndroidManifest/Check Push activities" )]
    public static void CheckPushActivities ()
    {
        CheckAndFixManifest ( ManifestSection.Push_bundles );
    }

    public static void CheckAndFixManifest (ManifestSection section)
    {
        var outputFile = Path.Combine ( Application.dataPath, "Plugins/Android/AndroidManifest.xml" );
        if (!File.Exists ( outputFile ))
        {
            Debug.LogError ( "There is no Manifest file found at Assets/Plugins/Android/" );
            return;
        }

        XmlDocument doc = new XmlDocument ( );
        doc.Load ( outputFile );

        if (doc == null)
        {
            Debug.LogError ( "Couldn't load " + outputFile );
            return;
        }

        XmlNode manNode = FindChildNode ( doc, "manifest" );
        string ns = manNode.GetNamespaceOfPrefix ( "android" );

        XmlNode dict = FindChildNode ( manNode, "application" );

        if (dict == null)
        {
            Debug.LogError ( "Error parsing " + outputFile );
            return;
        }



        if (section == ManifestSection.All || section == ManifestSection.Push_bundles)
        {
            CheckMainActivityFilter ( doc, dict, ns );
            CheckPSReceiver ( doc, dict, ns );
            CheckPSBootReceiver ( doc, dict, ns );
            CheckPermissions ( doc, manNode, ns );
        }

        if (section == ManifestSection.All || section == ManifestSection.play_services)
        {
            //CheckMainActivityLibName ( doc, dict, ns );
            CheckGmsActivityTheme ( doc, dict, ns );
        }

        doc.Save ( outputFile );
    }



    private static void CheckGmsActivityTheme (XmlDocument doc, XmlNode application_node, string ns)
    {
        string
            gms_activity_attr = "name",
            gms_activity_val = "com.google.android.gms.ads.AdActivity",
            gms_cofig_attr = "configChanges",
            gms_theme_attr = "theme",
            gms_theme_val = "@android:style/Theme.Translucent",
            gms_config_val = "keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize",
            gms_ver_attr = "com.google.android.gms.version",
            gms_ver_val = "@integer/google_play_services_version";

        XmlElement main_activity = FindElementWithAndroidName ( "activity", gms_activity_attr, ns, gms_activity_val, application_node );
        if (main_activity == null)
        {
            Debug.LogWarning ( string.Format ( "You are nmissing declaration of activity {0}. Adding new record. Check if other manifests do not contain same record", gms_activity_val ) );
            main_activity = doc.CreateElement ( "activity" );
            main_activity.SetAttribute ( gms_activity_attr, ns, gms_activity_val );
            application_node.AppendChild ( main_activity );
        }

        XmlNode item_val = main_activity.GetAttributeNode ( gms_theme_attr, ns );
        if (item_val != null)
        {
            if (item_val.Value != gms_theme_val)
            {
                item_val.Value = gms_theme_val;
            }
        }
        else
        {
            main_activity.SetAttribute ( gms_theme_attr, ns, gms_theme_val );
        }


        item_val = main_activity.GetAttributeNode ( gms_cofig_attr, ns );
        if (item_val != null)
        {
            if (item_val.Value != gms_config_val)
            {
                item_val.Value = gms_config_val;
            }
        }
        else
        {
            main_activity.SetAttribute ( gms_cofig_attr, ns, gms_config_val );
        }

        XmlElement gms_meta = FindElementWithAndroidName ( "meta-data", "name", ns, gms_ver_attr, application_node );

        if (gms_meta == null)
        {
            Debug.LogWarning ( string.Format ( "You are nmissing declaration of activity {0}. Adding new record. Check if other manifests do not contain same record", gms_activity_val ) );
            gms_meta = doc.CreateElement ( "meta-data" );
            gms_meta.SetAttribute ( "name", ns, gms_ver_attr );
            application_node.AppendChild ( gms_meta );
        }

        XmlNode ver_val = gms_meta.GetAttributeNode ( "value", ns );
        if (ver_val != null)
        {
            if (ver_val.Value != gms_ver_val)
            {
                ver_val.Value = gms_ver_val;
            }
        }
        else
        {
            gms_meta.SetAttribute ( "value", ns, gms_ver_val );

        }

    }


    private static void CheckMainActivityLibName (XmlDocument doc, XmlNode application_node, string ns)
    {
        string
            app_lib_name = "android.app.lib_name",
            app_lib_val = "unity";

        XmlElement main_activity = FindElementWithAndroidName ( "activity", "name", ns, unityNativeActivityName, application_node );
        if (main_activity == null)
        {
            Debug.LogError ( string.Format ( "You are nmissing declaration of activity {0}", unityNativeActivityName ) );
            return;
        }

        XmlElement meta_lib = FindElementWithAndroidName ( "meta-data", "name", ns, app_lib_name, main_activity );

        if (meta_lib == null)
        {
            meta_lib = doc.CreateElement ( "meta-data" );
            meta_lib.SetAttribute ( "name", ns, app_lib_name );
            main_activity.AppendChild ( meta_lib );
        }

        string meta_val = meta_lib.GetAttribute ( "vale", ns );
        if (meta_val != app_lib_val)
        {
            meta_lib.SetAttribute ( "value", ns, app_lib_val );
        }
    }


    private static void CheckPermissions (XmlDocument doc, XmlNode manifest_node, string ns)
    {
        for (int i = 0; i < necessary_permissions.Length; i++)
        {
            XmlElement permission = FindElementWithAndroidName ( "uses-permission", "name", ns, necessary_permissions [i], manifest_node );
            if (permission == null)
            {
                permission = doc.CreateElement ( "uses-permission" );
                permission.SetAttribute ( "name", ns, necessary_permissions [i] );
                manifest_node.AppendChild ( permission );
            }
        }
    }


    private static void CheckMainActivityFilter (XmlDocument doc, XmlNode application_node, string ns)
    {
        XmlElement main_activity = FindElementWithAndroidName ( "activity", "name", ns, unityNativeActivityName, application_node );
        if (main_activity == null)
        {
            Debug.LogError ( string.Format ( "You are nmissing declaration of activity {0}", unityNativeActivityName ) );
            return;
        }
        XmlNodeList filters = main_activity.GetElementsByTagName ( "intent-filter" );

        XmlElement filter = null;

        XmlElement action = null;
        XmlElement category = null;

        if (filters.Count > 0)
        {
            for (int i = 0; i < filters.Count; i++)
            {
                filter = filters [i] as XmlElement;
                action = FindElementWithAndroidName ( "action", "name", ns, Application.bundleIdentifier, filter );
                category = FindElementWithAndroidName ( "category", "name", ns, psv_push_reseiver_category, filter );
                if (action != null || category != null)
                {
                    XmlNodeList other_actions = filter.GetElementsByTagName ( "action" );
                    for (int a = 0; a < other_actions.Count; a++)
                    {
                        //XmlElement other_action = other_actions [a] as XmlElement;
                        //string val = other_action.GetAttribute ( "name", ns );
                        string val = other_actions [a].Attributes.GetNamedItem ( "name", ns ).Value;
                        if (val != Application.bundleIdentifier)
                        {
                            if (EditorUtility.DisplayDialog ( "Push manifest check", "Found " + val + " in " + unityNativeActivityName + " intent-filter. Want to delete it?", "Delete", "Keep" ))
                            {
                                filter.RemoveChild ( other_actions [a] );
                            }
                        }
                    }
                    break;
                }
                else
                {
                    filter = null;
                }
            }
        }

        if (filter == null)
        {
            filter = doc.CreateElement ( "intent-filter" );
            main_activity.AppendChild ( filter );
        }

        if (action == null)
        {
            action = doc.CreateElement ( "action" );
            action.SetAttribute ( "name", ns, Application.bundleIdentifier );
            filter.AppendChild ( action );
        }

        if (category == null)
        {
            category = doc.CreateElement ( "category" );
            filter.AppendChild ( category );
        }
        if (category.GetAttribute ( "name", ns ) != psv_push_reseiver_category)
        {
            category.SetAttribute ( "name", ns, psv_push_reseiver_category );
        }
    }


    private static void CheckPSBootReceiver (XmlDocument doc, XmlNode application_node, string ns)
    {
        XmlElement PS_BootReceiverElement = FindElementWithAndroidName ( "receiver", "name", ns, psv_push_boot_reseiver_name, application_node );
        if (PS_BootReceiverElement == null)
        {
            Debug.Log ( string.Format ( "creating receiver {0}", psv_push_boot_reseiver_name ) );
            PS_BootReceiverElement = doc.CreateElement ( "receiver" );
            PS_BootReceiverElement.SetAttribute ( "name", ns, psv_push_boot_reseiver_name );
            application_node.AppendChild ( PS_BootReceiverElement );
        }

        XmlNodeList PS_ReceiverFilters = PS_BootReceiverElement.GetElementsByTagName ( "intent-filter" );


        XmlElement PS_BootReceiver_filter = null;
        XmlElement PS_BootReceiver_action_com = null;
        XmlElement PS_BootReceiver_action_poweron = null;
        XmlElement PS_BootReceiver_action_boot = null;
        XmlElement PS_BootReceiver_category = null;

        if (PS_ReceiverFilters.Count > 0)
        {
            if (PS_ReceiverFilters.Count > 1)
            {
                Debug.LogError ( psv_push_boot_reseiver_name + " has more then one intent-filter" );
                return;
            }

            PS_BootReceiver_filter = PS_ReceiverFilters [0] as XmlElement;

            PS_BootReceiver_action_com = FindElementWithAndroidName ( "action", "name", ns, psv_push_boot_receiver_action, PS_BootReceiver_filter );
            PS_BootReceiver_action_poweron = FindElementWithAndroidName ( "action", "name", ns, psv_push_boot_receiver_poweron, PS_BootReceiver_filter );
            PS_BootReceiver_action_boot = FindElementWithAndroidName ( "action", "name", ns, psv_push_boot_receiver_boot, PS_BootReceiver_filter );

            PS_BootReceiver_category = FindElementWithAndroidName ( "category", "name", ns, psv_push_reseiver_category, PS_BootReceiver_filter );
        }
        else
        {
            PS_BootReceiver_filter = doc.CreateElement ( "intent-filter" );
            PS_BootReceiverElement.AppendChild ( PS_BootReceiver_filter );
        }

        if (PS_BootReceiver_action_com == null)
        {
            PS_BootReceiver_action_com = doc.CreateElement ( "action" );
            PS_BootReceiver_filter.AppendChild ( PS_BootReceiver_action_com );
        }
        if (PS_BootReceiver_action_com.GetAttribute ( "name", ns ) != psv_push_boot_receiver_action)
        {
            PS_BootReceiver_action_com.SetAttribute ( "name", ns, psv_push_boot_receiver_action );
        }


        if (PS_BootReceiver_action_boot == null)
        {
            PS_BootReceiver_action_boot = doc.CreateElement ( "action" );
            PS_BootReceiver_filter.AppendChild ( PS_BootReceiver_action_boot );
        }
        if (PS_BootReceiver_action_boot.GetAttribute ( "name", ns ) != psv_push_boot_receiver_boot)
        {
            PS_BootReceiver_action_boot.SetAttribute ( "name", ns, psv_push_boot_receiver_boot );
        }

        if (PS_BootReceiver_action_poweron == null)
        {
            PS_BootReceiver_action_poweron = doc.CreateElement ( "action" );
            PS_BootReceiver_filter.AppendChild ( PS_BootReceiver_action_poweron );
        }
        if (PS_BootReceiver_action_poweron.GetAttribute ( "name", ns ) != psv_push_boot_receiver_poweron)
        {
            PS_BootReceiver_action_poweron.SetAttribute ( "name", ns, psv_push_boot_receiver_poweron );
        }

        if (PS_BootReceiver_category == null)
        {
            PS_BootReceiver_category = doc.CreateElement ( "category" );
            PS_BootReceiver_filter.AppendChild ( PS_BootReceiver_category );
        }
        if (PS_BootReceiver_category.GetAttribute ( "name", ns ) != psv_push_reseiver_category)
        {
            PS_BootReceiver_category.SetAttribute ( "name", ns, psv_push_reseiver_category );
        }
    }



    private static void CheckPSReceiver (XmlDocument doc, XmlNode application_node, string ns)
    {
        XmlElement PS_ReceiverElement = FindElementWithAndroidName ( "receiver", "name", ns, psv_push_reseiver_name, application_node );
        if (PS_ReceiverElement == null)
        {
            Debug.Log ( string.Format ( "creating receiver {0}", unityNativeActivityName ) );
            PS_ReceiverElement = doc.CreateElement ( "receiver" );
            PS_ReceiverElement.SetAttribute ( "name", ns, psv_push_reseiver_name );
            application_node.AppendChild ( PS_ReceiverElement );
        }

        XmlNodeList PS_ReceiverFilters = PS_ReceiverElement.GetElementsByTagName ( "intent-filter" );


        XmlElement PS_Receiver_filter = null;
        XmlElement PS_Receiver_action = null;
        XmlElement PS_Receiver_category = null;

        if (PS_ReceiverFilters.Count > 0)
        {
            if (PS_ReceiverFilters.Count > 1)
            {
                Debug.LogError ( psv_push_reseiver_name + " has more then one intent-filter" );
                return;
            }
            PS_Receiver_filter = PS_ReceiverFilters [0] as XmlElement;




            if (PS_Receiver_action == null)
            {
                XmlNodeList action_list = PS_Receiver_filter.GetElementsByTagName ( "action" );
                if (action_list.Count > 0)
                {
                    if (action_list.Count > 1)
                    {
                        Debug.LogError ( psv_push_reseiver_name + " has more then one action" );
                        return;
                    }
                    else
                    {
                        PS_Receiver_action = action_list [0] as XmlElement;
                    }
                }
            }


            PS_Receiver_category = FindElementWithAndroidName ( "category", "name", ns, psv_push_reseiver_category, PS_Receiver_filter );
        }
        else
        {
            PS_Receiver_filter = doc.CreateElement ( "intent-filter" );
            PS_ReceiverElement.AppendChild ( PS_Receiver_filter );
        }

        if (PS_Receiver_action == null)
        {
            PS_Receiver_action = doc.CreateElement ( "action" );
            PS_Receiver_filter.AppendChild ( PS_Receiver_action );
        }
        string action_name = "PSService." + Application.bundleIdentifier;

        if (PS_Receiver_action.GetAttribute ( "name", ns ) != action_name)
        {
            PS_Receiver_action.SetAttribute ( "name", ns, action_name );
        }

        if (PS_Receiver_category == null)
        {
            PS_Receiver_category = doc.CreateElement ( "category" );
            PS_Receiver_filter.AppendChild ( PS_Receiver_category );
        }
        if (PS_Receiver_category.GetAttribute ( "name", ns ) != psv_push_reseiver_category)
        {
            PS_Receiver_category.SetAttribute ( "name", ns, psv_push_reseiver_category );
        }
    }


    private static XmlNode FindChildNode (XmlNode parent, string name)
    {
        XmlNode curr = parent.FirstChild;
        while (curr != null)
        {
            if (curr.Name.Equals ( name ))
            {
                return curr;
            }
            curr = curr.NextSibling;
        }
        return null;
    }

    private static XmlElement FindElementWithAndroidName (string name, string androidName, string ns, string value, XmlNode parent)
    {
        var curr = parent.FirstChild;
        while (curr != null)
        {
            if (curr.Name.Equals ( name ) && curr is XmlElement && ((XmlElement) curr).GetAttribute ( androidName, ns ) == value)
            {
                return curr as XmlElement;
            }
            curr = curr.NextSibling;
        }
        return null;
    }

}



//based on this https://gist.github.com/yuandra/8264869
public class ChangeTextureImportSettings :ScriptableObject
{
    static int currentMaxTextureSize;
    static TextureImporterFormat currentTIFormat;
    static string logTitle = "ChangeTextureImportSettings. ";



    [MenuItem ( "HelpTools/Set optimised texture params" )]
    static void SetOptimalParamsWithout2048 ()
    {
        SelectedProcessWithOptimalSettings ( );
    }

    [MenuItem ( "HelpTools/Set optimised texture params +2048" )]
    static void SetOptimalParams ()
    {
        SelectedProcessWithOptimalSettings ( true );
    }



    static void SelectedProcessWithOptimalSettings (bool include_2048 = false)
    {
        int processingTexturesNumber;
        Object [] originalSelection = Selection.objects;
        Object [] textures = GetSelectedTextures ( );
        Selection.objects = new Object [0]; //Clear selection (for correct data representation on GUI)
        processingTexturesNumber = textures.Length;
        if (processingTexturesNumber == 0)
        {
            Debug.LogWarning ( logTitle + "Nothing to do. Please select objects/folders with 2d textures in Project tab" );
            return;
        }
        AssetDatabase.StartAssetEditing ( );
        foreach (Texture2D texture in textures)
        {
            string path = AssetDatabase.GetAssetPath ( texture );
            TextureImporter textureImporter = AssetImporter.GetAtPath ( path ) as TextureImporter;
            if (textureImporter != null)
            {
                textureImporter.mipmapEnabled = false;
                textureImporter.maxTextureSize = GetOptimalSize ( textureImporter, include_2048 );
                textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
                textureImporter.filterMode = FilterMode.Bilinear;
                //apply changes
                AssetDatabase.ImportAsset ( path, ImportAssetOptions.ForceUpdate );
            }
            else
            {
                processingTexturesNumber--;
                Debug.LogError ( logTitle + "Processing failed at " + path );
            }
        }
        AssetDatabase.StopAssetEditing ( );
        Selection.objects = originalSelection; //Restore selection
        Debug.Log ( logTitle + "Textures processed: " + processingTexturesNumber );
    }



    static int GetOptimalSize (TextureImporter importer, bool include_2048)
    {
        int [] sizes = new int [] { 32, 64, 128, 256, 512, 1024, 2048 };
        int res = 1024;
        int width, height;
        GetImageSize ( importer, out width, out height );
        int max_size = Mathf.Max ( width, height );
        for (int i = 0; i < sizes.Length; i++)
        {
            int limit = include_2048 ? 2048 : 1024;
            if (sizes [i] == limit)
            {
                res = sizes [i];
                break;
            }
            if (max_size <= sizes [i])
            {
                res = sizes [i];
                break;
            }
        }
        return res;
    }


    public static void GetImageSize (TextureImporter importer, out int width, out int height)
    {
        object [] args = new object [2] { 0, 0 };
        MethodInfo mi = typeof ( TextureImporter ).GetMethod ( "GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance );
        mi.Invoke ( importer, args );

        width = (int) args [0];
        height = (int) args [1];
    }

    static Object [] GetSelectedTextures ()
    {
        return Selection.GetFiltered ( typeof ( Texture2D ), SelectionMode.DeepAssets );
    }

}